var struct_member_slot =
[
    [ "ChangeRank", "struct_member_slot.html#a0169d043482656c99b25bab673ca37dc", null ],
    [ "SetMemberStats", "struct_member_slot.html#aeedaf3f343fd7debd0bd33d10fdc3562", null ],
    [ "SetOFFNOTE", "struct_member_slot.html#a1bae2519ebf4ac768d2983e5f6f3c592", null ],
    [ "SetPNOTE", "struct_member_slot.html#ab195d89611d65c4a397dd8f0a10ad282", null ],
    [ "UpdateLogoutTime", "struct_member_slot.html#a7a347eeb5cace354224e04fd09a76c2e", null ],
    [ "accountId", "struct_member_slot.html#a61e713f8dba8833b3fa10825da39ef1b", null ],
    [ "Class", "struct_member_slot.html#a7a81e75496ea4921bf1b90529a5723fb", null ],
    [ "guid", "struct_member_slot.html#a3d6c09aed98b33de93e14d888c9d7e5f", null ],
    [ "Level", "struct_member_slot.html#a0ae75333a5e9807b17450cf3c432b185", null ],
    [ "LogoutTime", "struct_member_slot.html#afff1253386f92fa3249651d627ec6ea4", null ],
    [ "Name", "struct_member_slot.html#af51c381461bc35600345867b8b2d7657", null ],
    [ "OFFnote", "struct_member_slot.html#a385cc41a94d4f32a8c16c339b02f0120", null ],
    [ "Pnote", "struct_member_slot.html#a63676246806379925c735f214dec1c63", null ],
    [ "RankId", "struct_member_slot.html#a788ca7602987691219756753e9795097", null ],
    [ "ZoneId", "struct_member_slot.html#abd1207a650231d4413023b427b145a26", null ]
];