var _loot_action_8cpp =
[
    [ "ProfessionSpells", "_loot_action_8cpp.html#a26d890ef51a8f0b6d41a8595d653c262", [
      [ "ALCHEMY", "_loot_action_8cpp.html#a26d890ef51a8f0b6d41a8595d653c262afd36cb5a714b18c3e0f8b8eb4197b0f6", null ],
      [ "BLACKSMITHING", "_loot_action_8cpp.html#a26d890ef51a8f0b6d41a8595d653c262a7a64f75ef55fb31e1491b39a1b69b628", null ],
      [ "COOKING", "_loot_action_8cpp.html#a26d890ef51a8f0b6d41a8595d653c262a57439ae43177e12da99d6a8ce0fac02f", null ],
      [ "ENCHANTING", "_loot_action_8cpp.html#a26d890ef51a8f0b6d41a8595d653c262a183cc3b46f3dc80988ef02d2304f142f", null ],
      [ "ENGINEERING", "_loot_action_8cpp.html#a26d890ef51a8f0b6d41a8595d653c262a3738df65761ce59b02eee3f2596fdd01", null ],
      [ "FIRST_AID", "_loot_action_8cpp.html#a26d890ef51a8f0b6d41a8595d653c262a2c1db64a29aef0ba96531d4eac1c67ad", null ],
      [ "FISHING", "_loot_action_8cpp.html#a26d890ef51a8f0b6d41a8595d653c262a5d77197491bc21a3636bbf6bd1315615", null ],
      [ "HERB_GATHERING", "_loot_action_8cpp.html#a26d890ef51a8f0b6d41a8595d653c262ab0f7b1d1edeec3b10cb7ebe7f051a48d", null ],
      [ "INSCRIPTION", "_loot_action_8cpp.html#a26d890ef51a8f0b6d41a8595d653c262a25182d273740e92f3a2c208bc0362798", null ],
      [ "JEWELCRAFTING", "_loot_action_8cpp.html#a26d890ef51a8f0b6d41a8595d653c262aa1968c5ea12f376acc80011e4b41c7ec", null ],
      [ "MINING", "_loot_action_8cpp.html#a26d890ef51a8f0b6d41a8595d653c262a68ecce034306c2e03aa6602e01eb6866", null ],
      [ "SKINNING", "_loot_action_8cpp.html#a26d890ef51a8f0b6d41a8595d653c262afbd9321fe7749f28abdbd1f736073546", null ],
      [ "TAILORING", "_loot_action_8cpp.html#a26d890ef51a8f0b6d41a8595d653c262aedae0cc5da42b028a2fe0cbd57192d27", null ]
    ] ]
];