var class_dungeon_persistent_state =
[
    [ "DungeonPersistentState", "class_dungeon_persistent_state.html#aff42c995de0f69db060ad794cdee018e", null ],
    [ "~DungeonPersistentState", "class_dungeon_persistent_state.html#a8a91789e908f3127a286d43699831415", null ],
    [ "AddGroup", "class_dungeon_persistent_state.html#a164361157ac83212e2efd096dc26cf00", null ],
    [ "AddPlayer", "class_dungeon_persistent_state.html#ab376e64a2bc6d14c2d0753bfc793800b", null ],
    [ "CanBeUnload", "class_dungeon_persistent_state.html#a2212d774d767f8b971e1b3e35e92ebb9", null ],
    [ "CanReset", "class_dungeon_persistent_state.html#a008a570fda1815371d6df970b05aa3b6", null ],
    [ "DeleteFromDB", "class_dungeon_persistent_state.html#ab84ac2ed70e61eac00d6313c0e5f176d", null ],
    [ "DeleteRespawnTimes", "class_dungeon_persistent_state.html#aa1237f4d38ca61f975f4629b75f0c591", null ],
    [ "GetGroupCount", "class_dungeon_persistent_state.html#a78db818b1fbc21be8c3c8add9e73107c", null ],
    [ "GetPlayerCount", "class_dungeon_persistent_state.html#a34b383782b0f48b5139967f395e66926", null ],
    [ "GetResetTime", "class_dungeon_persistent_state.html#accadc7839e0b6138385d99cb264f5a19", null ],
    [ "GetResetTimeForDB", "class_dungeon_persistent_state.html#a11c09b64911d58b8d7b2cca84f3f481f", null ],
    [ "GetSpawnedPoolData", "class_dungeon_persistent_state.html#a4bcee9bfeeacd0e7860d385438a323ed", null ],
    [ "GetTemplate", "class_dungeon_persistent_state.html#aa0fa68d031ded01ccc349220781eefb5", null ],
    [ "HasBounds", "class_dungeon_persistent_state.html#af5ca2f01065da778f59fc9a2550373fe", null ],
    [ "RemoveGroup", "class_dungeon_persistent_state.html#aa61deeed0d94946a151d02a192232a42", null ],
    [ "RemovePlayer", "class_dungeon_persistent_state.html#a817aec9f3debacf2db2826061ee50945", null ],
    [ "SaveToDB", "class_dungeon_persistent_state.html#a2ee896b41aa9fac3c62791c6e2aee9c1", null ],
    [ "SetCanReset", "class_dungeon_persistent_state.html#a4dfe1cc2adde98e4b801704e59d4105f", null ],
    [ "SetResetTime", "class_dungeon_persistent_state.html#ae6acb4a52dfe2b52195ae2ce935dbde7", null ],
    [ "UnbindThisState", "class_dungeon_persistent_state.html#a2f562e2b353f9a82c15e27085482016d", null ]
];