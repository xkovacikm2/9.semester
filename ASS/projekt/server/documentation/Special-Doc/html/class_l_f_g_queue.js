var class_l_f_g_queue =
[
    [ "LFGQueue", "class_l_f_g_queue.html#a1a1c30803b017659618123913dfcb1d0", null ],
    [ "~LFGQueue", "class_l_f_g_queue.html#a17817f6ef04934f339a2e24b3dc3f633", null ],
    [ "AddToQueue", "class_l_f_g_queue.html#ac22ce1f0bc0c64e89715db5a23f71b7f", null ],
    [ "CalculateRoles", "class_l_f_g_queue.html#ab6b82261764dcb4164d7df4d4cee207e", null ],
    [ "canPerformRole", "class_l_f_g_queue.html#ab591312ffb2296e59547d4730be54704", null ],
    [ "FindRoleToGroup", "class_l_f_g_queue.html#a26417bbe0fbd14f6d8de7b4889a321f9", null ],
    [ "getPriority", "class_l_f_g_queue.html#a29506812d8cc84a5fc77a22fc2917a6b", null ],
    [ "RemoveGroupFromQueue", "class_l_f_g_queue.html#aca7b5c609b55b0f929fb8f87476489ac", null ],
    [ "RemovePlayerFromQueue", "class_l_f_g_queue.html#a6996e642adf9bf3fc2619ab2c8b93a2c", null ],
    [ "RestoreOfflinePlayer", "class_l_f_g_queue.html#ad787f1df34087d5d30679aef1f292174", null ],
    [ "Update", "class_l_f_g_queue.html#ae1c12238a92644978f0d157977b9c5ac", null ],
    [ "UpdateGroup", "class_l_f_g_queue.html#ae342f6e90704711f34099e1eb8139e99", null ]
];