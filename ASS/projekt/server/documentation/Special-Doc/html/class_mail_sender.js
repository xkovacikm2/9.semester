var class_mail_sender =
[
    [ "MailSender", "class_mail_sender.html#ac7d7125dee1a75dbd65325e2cd5a0320", null ],
    [ "MailSender", "class_mail_sender.html#a16466924bf75ea8203ba27c0f3e6188b", null ],
    [ "MailSender", "group__mailing.html#ga735cfef6950357644d6965978dde0309", null ],
    [ "MailSender", "group__mailing.html#ga9ddda06cf5ecc55efdc4525353471c5b", null ],
    [ "GetMailMessageType", "class_mail_sender.html#a64c99a49ec01d2226b6bf740edb380fb", null ],
    [ "GetSenderId", "class_mail_sender.html#a03145c3fdc5bbabc64019e63e121132b", null ],
    [ "GetStationery", "class_mail_sender.html#a6a4446e52e20ad7ab4817bf024b38631", null ]
];