var _game_event_mgr_8h =
[
    [ "GameEventData", "struct_game_event_data.html", "struct_game_event_data" ],
    [ "GameEventCreatureData", "struct_game_event_creature_data.html", "struct_game_event_creature_data" ],
    [ "GameEventMail", "struct_game_event_mail.html", "struct_game_event_mail" ],
    [ "GameEventMgr", "class_game_event_mgr.html", "class_game_event_mgr" ],
    [ "max_ge_check_delay", "_game_event_mgr_8h.html#adaa73d920934a951856f7ce003ce2342", null ],
    [ "sGameEventMgr", "_game_event_mgr_8h.html#a3c1c916d94272dfe845b9cc5e8fc292a", null ],
    [ "GameEventCreatureDataPair", "_game_event_mgr_8h.html#ab53f12c9d5d7704cbbe67368cd50b8a4", null ],
    [ "IsHolidayActive", "_game_event_mgr_8h.html#a429db26781d2673aa2f031fc1327ffaf", null ]
];