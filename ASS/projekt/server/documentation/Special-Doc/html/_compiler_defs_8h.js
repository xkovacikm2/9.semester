var _compiler_defs_8h =
[
    [ "COMPILER_BORLAND", "_compiler_defs_8h.html#a9a21493e6a621887b45cd2fb3aafd184", null ],
    [ "COMPILER_CLANG", "_compiler_defs_8h.html#a6b57fb2a693b8ae06b593c7614cb0c7f", null ],
    [ "COMPILER_GNU", "_compiler_defs_8h.html#a5abd10a3e8c16656454ac5c9d09ff311", null ],
    [ "COMPILER_HAS_CPP11_SUPPORT", "_compiler_defs_8h.html#a8414bd0bfd15ec7e8ad8ad8d53ffd7cd", null ],
    [ "COMPILER_INTEL", "_compiler_defs_8h.html#a4160d22912425b5e9bcf0d0d6f848c0d", null ],
    [ "COMPILER_MICROSOFT", "_compiler_defs_8h.html#a9577811554138dce241ce6abf0138f45", null ],
    [ "PLATFORM", "_compiler_defs_8h.html#a1fa4f1561216be34f745f32aaa38d943", null ],
    [ "PLATFORM_APPLE", "_compiler_defs_8h.html#a1800cbdd58d8544f473a5f29690e38fb", null ],
    [ "PLATFORM_INTEL", "_compiler_defs_8h.html#a11c6e2491d7a05528d5b456cc82b50e7", null ],
    [ "PLATFORM_UNIX", "_compiler_defs_8h.html#affc7221af60296a2254e861682bbfedc", null ],
    [ "PLATFORM_WINDOWS", "_compiler_defs_8h.html#a20cd3c4775f1897fb5658d2dc61382c3", null ]
];