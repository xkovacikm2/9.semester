var struct_quest_locale =
[
    [ "QuestLocale", "struct_quest_locale.html#a887d474a470ee0278fb4b465a7ad9547", null ],
    [ "Details", "struct_quest_locale.html#afd23d37c92f2b28e1c125bba0d670497", null ],
    [ "EndText", "struct_quest_locale.html#afc0c37b07e3c2c26b8d9c838b2178f5a", null ],
    [ "Objectives", "struct_quest_locale.html#ae1ecea0f255f34b547302bfc2c0b6ad4", null ],
    [ "ObjectiveText", "struct_quest_locale.html#ae41f7f50ab68e5783ceec37e6f8ff5b0", null ],
    [ "OfferRewardText", "struct_quest_locale.html#aaba6d07625232a77e3457454f7d4fa77", null ],
    [ "RequestItemsText", "struct_quest_locale.html#a764d2092084454844e59c484fb637017", null ],
    [ "Title", "struct_quest_locale.html#ab75b38241e31b9137c1d85334b3a3f30", null ]
];