var class_roll =
[
    [ "PlayerVote", "class_roll.html#a3e55479126cc28dace09cc20db3a3372", null ],
    [ "Roll", "class_roll.html#af7fb549aa3e96d4d0067bc2998c65f1e", null ],
    [ "~Roll", "class_roll.html#a843879734a9c86a97d7f26b672f37f2a", null ],
    [ "getLoot", "class_roll.html#ae6c032632332b0ba8bb13fc6e28d298f", null ],
    [ "setLoot", "class_roll.html#a34869ee15d87708859cd51262ad160fc", null ],
    [ "targetObjectBuildLink", "class_roll.html#aeca8cf7223c06998e0419ab929fdcd7a", null ],
    [ "itemid", "class_roll.html#a7b40118e2d489177a268301636b9994f", null ],
    [ "itemRandomPropId", "class_roll.html#a5154a19624a6e0d9657a697f9b235491", null ],
    [ "itemSlot", "class_roll.html#a6e170a8296949cd76baa7992f30a61d6", null ],
    [ "lootedTargetGUID", "class_roll.html#a52d24457e8c98f6bd133ec2b34352682", null ],
    [ "playerVote", "class_roll.html#aff97626c4d749bf0acbd165f289b5a57", null ],
    [ "totalGreed", "class_roll.html#a6187dd03a275f9e51d489709c9cf6d13", null ],
    [ "totalNeed", "class_roll.html#ae02ba19cfd317143e839cd4a3751a3eb", null ],
    [ "totalPass", "class_roll.html#aeeac3fb83c410a3d256c0cbf45f48e95", null ],
    [ "totalPlayersRolling", "class_roll.html#a6a6ea6885bc68272520c018a83b2106f", null ]
];