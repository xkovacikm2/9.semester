var struct_group_queue_info =
[
    [ "BgTypeId", "struct_group_queue_info.html#a87216d26be429b52e99cdb08cf107378", null ],
    [ "GroupTeam", "struct_group_queue_info.html#a1aa520727910d5791a8e4411ce43d8e2", null ],
    [ "IsInvitedToBGInstanceGUID", "struct_group_queue_info.html#a0261df1066e01602f475e9a08e142883", null ],
    [ "JoinTime", "struct_group_queue_info.html#a44939a98ff1ec940e42e79faf31a2e9a", null ],
    [ "Players", "struct_group_queue_info.html#a4a66f6018ad94e674d24e5bc919425d6", null ],
    [ "RemoveInviteTime", "struct_group_queue_info.html#aefe5994744194e7db9110fb36fa9c4bb", null ]
];