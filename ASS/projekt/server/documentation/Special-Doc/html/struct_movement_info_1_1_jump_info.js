var struct_movement_info_1_1_jump_info =
[
    [ "JumpInfo", "struct_movement_info_1_1_jump_info.html#a9221e7374f53512cc99c87ba47ff9c65", null ],
    [ "cosAngle", "struct_movement_info_1_1_jump_info.html#a37f49fc816f37dff8c1947f4de7f04d5", null ],
    [ "sinAngle", "struct_movement_info_1_1_jump_info.html#af2a184b7c8e7f324607a0b7468bcb8f6", null ],
    [ "velocity", "struct_movement_info_1_1_jump_info.html#af1df29ed0d0c0d96bbbaa71647670510", null ],
    [ "xyspeed", "struct_movement_info_1_1_jump_info.html#a6fada60d677d89580c3a718d42a75d17", null ]
];