var class_hostile_reference =
[
    [ "HostileReference", "class_hostile_reference.html#a81061e54790bc6de829d9ac984a297dd", null ],
    [ "addThreat", "class_hostile_reference.html#a1a586a26293ba30cd31cd015ed3b8886", null ],
    [ "addThreatPercent", "class_hostile_reference.html#ad62b7cc8fc6d7aff9c94065d99fb6a15", null ],
    [ "getTempThreatModifyer", "class_hostile_reference.html#aaf1e0272fd75f17da7e7cba5445f1219", null ],
    [ "getThreat", "class_hostile_reference.html#ad2c43f7b67ead66c0b9ba4bea7763142", null ],
    [ "getUnitGuid", "class_hostile_reference.html#afc2d88b9c30d61f6ad7601dfdcf52fc6", null ],
    [ "isAccessable", "class_hostile_reference.html#a68c1c769815d0e3b9506e48b0f549e9f", null ],
    [ "isOnline", "class_hostile_reference.html#a041f8627b7e4f1bdc703effe67f59609", null ],
    [ "next", "class_hostile_reference.html#a73a83b7eacec5274b4b7edd754032f07", null ],
    [ "operator==", "class_hostile_reference.html#a5816411d7f4ebbec125fadd5450a06ab", null ],
    [ "removeReference", "class_hostile_reference.html#aa30a4bca79e58f1bf6f9c64f5092dc89", null ],
    [ "resetTempThreat", "class_hostile_reference.html#af52027732c37abc084c7ae1b7c7ce162", null ],
    [ "setAccessibleState", "class_hostile_reference.html#a35344c936880eef5b4902497eb7a6b28", null ],
    [ "setOnlineOfflineState", "class_hostile_reference.html#ad3de44df40ef79c8dc8c2e4da9882daf", null ],
    [ "setTempThreat", "class_hostile_reference.html#af8dca101c96ea85e86e361693a210ebd", null ],
    [ "setThreat", "class_hostile_reference.html#af37d9a78814dbab69456b77410dcd9f4", null ],
    [ "sourceObjectDestroyLink", "class_hostile_reference.html#a8c708097d72791957ffc93f4d0fcb742", null ],
    [ "targetObjectBuildLink", "class_hostile_reference.html#a6a14c74aa1fa05efdc330a7570c99910", null ],
    [ "targetObjectDestroyLink", "class_hostile_reference.html#a2fbcd4b88a86e22e3cf805cf199b1fd7", null ],
    [ "updateOnlineStatus", "class_hostile_reference.html#a0046c9cb12992bbd97ef2780ce9810e5", null ]
];