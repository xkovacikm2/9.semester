var _n_p_c_handler_8h =
[
    [ "PageText", "struct_page_text.html", "struct_page_text" ],
    [ "PageTextLocale", "struct_page_text_locale.html", "struct_page_text_locale" ],
    [ "NpcTextLocale", "struct_npc_text_locale.html", "struct_npc_text_locale" ],
    [ "QEmote", "struct_q_emote.html", "struct_q_emote" ],
    [ "GossipTextOption", "struct_gossip_text_option.html", "struct_gossip_text_option" ],
    [ "GossipText", "struct_gossip_text.html", "struct_gossip_text" ],
    [ "MAX_GOSSIP_TEXT_OPTIONS", "_n_p_c_handler_8h.html#a114c9636000e930cd1dc1ef7e444814f", null ]
];