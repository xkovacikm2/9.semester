var struct_mmap_tile_header =
[
    [ "MmapTileHeader", "struct_mmap_tile_header.html#a0165f9a4caee4e4759ea261dfbac08a7", null ],
    [ "dtVersion", "struct_mmap_tile_header.html#a34d3ed8c617139653176454d7fa7934e", null ],
    [ "mmapMagic", "struct_mmap_tile_header.html#ace0ec88460c97f1438de2d0e6a9d7774", null ],
    [ "mmapVersion", "struct_mmap_tile_header.html#a5cc3cd873419fe8aeb479d3187584410", null ],
    [ "size", "struct_mmap_tile_header.html#a69f3284a4a56b1108792ba56c250bf97", null ],
    [ "usesLiquids", "struct_mmap_tile_header.html#a40d4a28a25a09bc0e7f1e070b629b1af", null ]
];