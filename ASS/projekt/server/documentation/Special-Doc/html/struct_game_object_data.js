var struct_game_object_data =
[
    [ "animprogress", "struct_game_object_data.html#aadec9e3088063532fd879fdaad3c3d74", null ],
    [ "go_state", "struct_game_object_data.html#ae8c523cb24a55fcde5d8caac3f82dfd5", null ],
    [ "id", "struct_game_object_data.html#ae12c8c5e7b3c848a57960e4c51d76e84", null ],
    [ "mapid", "struct_game_object_data.html#abcd9d31433df021c098f831a8267b33e", null ],
    [ "orientation", "struct_game_object_data.html#a2ac5b28981ef5b4bb4b5ccf52cc83f98", null ],
    [ "posX", "struct_game_object_data.html#abe0de5925bbdc047e1b0fc3baff039b1", null ],
    [ "posY", "struct_game_object_data.html#adfb0111d7183eefd893e0cce7691a08e", null ],
    [ "posZ", "struct_game_object_data.html#a2b0581c5879b16ae7a4d471aa23d178c", null ],
    [ "rotation0", "struct_game_object_data.html#a5d64850a133f77c496558b236621ef06", null ],
    [ "rotation1", "struct_game_object_data.html#acf3b9a660b59c58b7fd9105c3a4e7951", null ],
    [ "rotation2", "struct_game_object_data.html#a3c12fbe2c783fcce1cd720bd005a87ee", null ],
    [ "rotation3", "struct_game_object_data.html#a5747827b4b272b88de1ff99bdd455c5f", null ],
    [ "spawntimesecs", "struct_game_object_data.html#a6afeb167bb136c3ddabcc350007788db", null ]
];