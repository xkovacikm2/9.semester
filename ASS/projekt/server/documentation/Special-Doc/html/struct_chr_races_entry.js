var struct_chr_races_entry =
[
    [ "CinematicSequence", "struct_chr_races_entry.html#a33a01b1c26b21706d9898af246f2a0df", null ],
    [ "FactionID", "struct_chr_races_entry.html#a5c66801f2f89aeb847699a2dd87486c7", null ],
    [ "model_f", "struct_chr_races_entry.html#a4807227fd863e247b18d29db0ea8236a", null ],
    [ "model_m", "struct_chr_races_entry.html#a50b5409352d7fe3e6f5919d83c9a0eae", null ],
    [ "name", "struct_chr_races_entry.html#a224842595c9dc5b077e4de121087a001", null ],
    [ "RaceID", "struct_chr_races_entry.html#a853f3262af3408cf2791e7cd08ec8d32", null ],
    [ "startingTaxiMask", "struct_chr_races_entry.html#aa92f7221dc225dcbdd86a51424625d4c", null ],
    [ "TeamID", "struct_chr_races_entry.html#addd21f5fbf6868e7cf2cd3c54c7172a1", null ]
];