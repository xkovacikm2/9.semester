var struct_do_spell_proc_event =
[
    [ "DoSpellProcEvent", "struct_do_spell_proc_event.html#aff7ed3ffbf36425619ec5e4702ab7286", null ],
    [ "AddEntry", "struct_do_spell_proc_event.html#a3695a577aa9428a2f4e583b5beab213c", null ],
    [ "HasEntry", "struct_do_spell_proc_event.html#a4d526a061180881e20b6ad40f515470a", null ],
    [ "IsValidCustomRank", "struct_do_spell_proc_event.html#a02cad684fe1c552abbe0f652dacdc376", null ],
    [ "operator()", "struct_do_spell_proc_event.html#a41631d5742e49c20ae65e5626838d6c3", null ],
    [ "SetStateToEntry", "struct_do_spell_proc_event.html#aa2f952bd10d65cd05e796f3dbcb48131", null ],
    [ "TableName", "struct_do_spell_proc_event.html#a1bbce0086641dc9d6f4cd97162e19ab6", null ],
    [ "count", "struct_do_spell_proc_event.html#af9b9d5cf6f56226d19b73ee8f5f07133", null ],
    [ "customProc", "struct_do_spell_proc_event.html#abb295d3f7f6c901f9114cfc4dda013cf", null ],
    [ "spe_map", "struct_do_spell_proc_event.html#a09fd3fa20e411499399e41255ca24eec", null ],
    [ "state", "struct_do_spell_proc_event.html#aaebc804be0407538320e56adb6e37242", null ]
];