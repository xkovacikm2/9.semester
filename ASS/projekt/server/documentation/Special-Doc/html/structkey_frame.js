var structkey_frame =
[
    [ "keyFrame", "structkey_frame.html#a7fa5d43e52b5edf6bf617d85baca4400", null ],
    [ "distFromPrev", "structkey_frame.html#a8a2780ab14c59fa1809762555567d665", null ],
    [ "distSinceStop", "structkey_frame.html#a05db56afb48882bff401b11e6aec7b0c", null ],
    [ "distUntilStop", "structkey_frame.html#a6ca701193a5f3ed0b838633ee2e8e4af", null ],
    [ "node", "structkey_frame.html#a0ccb95181de36302c2572624567867e0", null ],
    [ "tFrom", "structkey_frame.html#ac47728fafa155eb9b5e6432eac695a46", null ],
    [ "tTo", "structkey_frame.html#a6e53687d956cb59ce14b64fc88bfea15", null ]
];