var _d_b_c_enums_8h =
[
    [ "DEFAULT_MAX_LEVEL", "_d_b_c_enums_8h.html#af0724337c52ef108a88542412eb75d00", null ],
    [ "MAX_EFFECT_INDEX", "_d_b_c_enums_8h.html#a038867f3f1d822b38a6cad2411c8006a", null ],
    [ "MAX_LEVEL", "_d_b_c_enums_8h.html#a5bb4257ca9fa4bfcf9391b7895b97761", null ],
    [ "STRONG_MAX_LEVEL", "_d_b_c_enums_8h.html#aa7ac8b40a5627e3180ed713ac250a619", null ],
    [ "AbilitySkillFlags", "_d_b_c_enums_8h.html#a8e45942d7ac6906acbdb3cc3a39518ed", [
      [ "ABILITY_SKILL_NONTRAINABLE", "_d_b_c_enums_8h.html#a8e45942d7ac6906acbdb3cc3a39518edac7ef7ad484121825785284341d327911", null ]
    ] ],
    [ "AbilytyLearnType", "_d_b_c_enums_8h.html#a5af614298393d3986bae9de58785ba60", [
      [ "ABILITY_LEARNED_ON_GET_PROFESSION_SKILL", "_d_b_c_enums_8h.html#a5af614298393d3986bae9de58785ba60a9168fde478aa1b380752e8286a7df9a4", null ],
      [ "ABILITY_LEARNED_ON_GET_RACE_OR_CLASS_SKILL", "_d_b_c_enums_8h.html#a5af614298393d3986bae9de58785ba60abfbb14dbf586003cfb77df0ab7251757", null ]
    ] ],
    [ "AreaFlags", "_d_b_c_enums_8h.html#ab8e24c5cd27834059bb62c245412fe72", [
      [ "AREA_FLAG_SNOW", "_d_b_c_enums_8h.html#ab8e24c5cd27834059bb62c245412fe72a81a1c93967cc55190e20730cb3c92c7f", null ],
      [ "AREA_FLAG_UNK1", "_d_b_c_enums_8h.html#ab8e24c5cd27834059bb62c245412fe72a2fc6333348169ce9d6b2a126ec9468b0", null ],
      [ "AREA_FLAG_UNK2", "_d_b_c_enums_8h.html#ab8e24c5cd27834059bb62c245412fe72abc4dac5990f6431079a4d4020cdbd238", null ],
      [ "AREA_FLAG_SLAVE_CAPITAL", "_d_b_c_enums_8h.html#ab8e24c5cd27834059bb62c245412fe72a51c51d95656bf277c0c85fb40d8dee67", null ],
      [ "AREA_FLAG_UNK3", "_d_b_c_enums_8h.html#ab8e24c5cd27834059bb62c245412fe72a934cff687a4089975e0cc3b31caff3fa", null ],
      [ "AREA_FLAG_SLAVE_CAPITAL2", "_d_b_c_enums_8h.html#ab8e24c5cd27834059bb62c245412fe72ad6b552dd4ca2d293e91bd14fd173c8d5", null ],
      [ "AREA_FLAG_DUEL", "_d_b_c_enums_8h.html#ab8e24c5cd27834059bb62c245412fe72acd5dc11e2214b026961882dbac310f45", null ],
      [ "AREA_FLAG_ARENA", "_d_b_c_enums_8h.html#ab8e24c5cd27834059bb62c245412fe72a06ec648a29d24477de05dbe75ffa0eb8", null ],
      [ "AREA_FLAG_CAPITAL", "_d_b_c_enums_8h.html#ab8e24c5cd27834059bb62c245412fe72a0b0e611c76bde798e51beb87dcb1bce4", null ],
      [ "AREA_FLAG_CITY", "_d_b_c_enums_8h.html#ab8e24c5cd27834059bb62c245412fe72a0439703a070189dc2aecdd38edaea968", null ]
    ] ],
    [ "AreaTeams", "_d_b_c_enums_8h.html#aa599e16e43c105cc36321b195aa6d056", [
      [ "AREATEAM_NONE", "_d_b_c_enums_8h.html#aa599e16e43c105cc36321b195aa6d056a44a7b2b1ceb222b00a0569cbf7e92d9c", null ],
      [ "AREATEAM_ALLY", "_d_b_c_enums_8h.html#aa599e16e43c105cc36321b195aa6d056a4bd773583eaf1112dc44ba1fb20253c7", null ],
      [ "AREATEAM_HORDE", "_d_b_c_enums_8h.html#aa599e16e43c105cc36321b195aa6d056a37b8548d0d6f20f7452da146dd4758c9", null ]
    ] ],
    [ "FactionMasks", "_d_b_c_enums_8h.html#a45861e7675b910e0a8729e6f60dcea49", [
      [ "FACTION_MASK_PLAYER", "_d_b_c_enums_8h.html#a45861e7675b910e0a8729e6f60dcea49af1089cd14cd54de38950846cdde850a8", null ],
      [ "FACTION_MASK_ALLIANCE", "_d_b_c_enums_8h.html#a45861e7675b910e0a8729e6f60dcea49a0e795396d2d184563503fc7572b6152f", null ],
      [ "FACTION_MASK_HORDE", "_d_b_c_enums_8h.html#a45861e7675b910e0a8729e6f60dcea49a9cd730fd208bced2c786f1c194bf5f57", null ],
      [ "FACTION_MASK_MONSTER", "_d_b_c_enums_8h.html#a45861e7675b910e0a8729e6f60dcea49a5a6f97624057bd51be5c204709448368", null ]
    ] ],
    [ "FactionTemplateFlags", "_d_b_c_enums_8h.html#aea9f1d659ec336b7f3975ea1e135f0b0", [
      [ "FACTION_TEMPLATE_FLAG_PVP", "_d_b_c_enums_8h.html#aea9f1d659ec336b7f3975ea1e135f0b0a99f876ecc992919175f0c393129c680a", null ],
      [ "FACTION_TEMPLATE_FLAG_CONTESTED_GUARD", "_d_b_c_enums_8h.html#aea9f1d659ec336b7f3975ea1e135f0b0aa085ee82616f80970703ee9d0dbfb045", null ]
    ] ],
    [ "ItemEnchantmentType", "_d_b_c_enums_8h.html#a364650a1412c9ed62462ed0b0574e14a", [
      [ "ITEM_ENCHANTMENT_TYPE_NONE", "_d_b_c_enums_8h.html#a364650a1412c9ed62462ed0b0574e14aaa138d025a73b3bb8747273f638bedd76", null ],
      [ "ITEM_ENCHANTMENT_TYPE_COMBAT_SPELL", "_d_b_c_enums_8h.html#a364650a1412c9ed62462ed0b0574e14aa62d4592c03e7f422a7508d714a0468e7", null ],
      [ "ITEM_ENCHANTMENT_TYPE_DAMAGE", "_d_b_c_enums_8h.html#a364650a1412c9ed62462ed0b0574e14aa2d078d78c76b36fc8257d3e31ce6259f", null ],
      [ "ITEM_ENCHANTMENT_TYPE_EQUIP_SPELL", "_d_b_c_enums_8h.html#a364650a1412c9ed62462ed0b0574e14aacd3f7f47a7b616e831315abc671ed04a", null ],
      [ "ITEM_ENCHANTMENT_TYPE_RESISTANCE", "_d_b_c_enums_8h.html#a364650a1412c9ed62462ed0b0574e14aa73d8555b8fe2db7c033389a63b74ca5c", null ],
      [ "ITEM_ENCHANTMENT_TYPE_STAT", "_d_b_c_enums_8h.html#a364650a1412c9ed62462ed0b0574e14aa67fe9b7b093ea4e2d3e3252209c82fd2", null ],
      [ "ITEM_ENCHANTMENT_TYPE_TOTEM", "_d_b_c_enums_8h.html#a364650a1412c9ed62462ed0b0574e14aacf506de0fb5a484f8ec83eafe2fa005a", null ]
    ] ],
    [ "MapTypes", "_d_b_c_enums_8h.html#a28c252fd678ff644cdad7b1b835f24c9", [
      [ "MAP_COMMON", "_d_b_c_enums_8h.html#a28c252fd678ff644cdad7b1b835f24c9a319c961bd0442d8a6cc838e6dad28175", null ],
      [ "MAP_INSTANCE", "_d_b_c_enums_8h.html#a28c252fd678ff644cdad7b1b835f24c9ad30ca726d1f53514633e18d10c09e665", null ],
      [ "MAP_RAID", "_d_b_c_enums_8h.html#a28c252fd678ff644cdad7b1b835f24c9ae33c109f713bf214b65d889e5d7fd2fe", null ],
      [ "MAP_BATTLEGROUND", "_d_b_c_enums_8h.html#a28c252fd678ff644cdad7b1b835f24c9aa9a058d60db14ffc3f1cc5ef8339ff14", null ]
    ] ],
    [ "SpellCastTargetFlags", "_d_b_c_enums_8h.html#a3c11f0d0cd9689554b34ccbc325da54f", [
      [ "TARGET_FLAG_SELF", "_d_b_c_enums_8h.html#a3c11f0d0cd9689554b34ccbc325da54fafe9a52eb16cad63aeb4202ecae8e2c18", null ],
      [ "TARGET_FLAG_UNUSED1", "_d_b_c_enums_8h.html#a3c11f0d0cd9689554b34ccbc325da54fa733998ed5a1a732583b1448dd043aa61", null ],
      [ "TARGET_FLAG_UNIT", "_d_b_c_enums_8h.html#a3c11f0d0cd9689554b34ccbc325da54fa9bdbe71c2e8d869741084e7d1b9c0e7e", null ],
      [ "TARGET_FLAG_UNUSED2", "_d_b_c_enums_8h.html#a3c11f0d0cd9689554b34ccbc325da54fa50883b541bcaf3bcfd8fb91ab5157260", null ],
      [ "TARGET_FLAG_UNUSED3", "_d_b_c_enums_8h.html#a3c11f0d0cd9689554b34ccbc325da54fa176434a09aae6d61428b392a62d5d73b", null ],
      [ "TARGET_FLAG_ITEM", "_d_b_c_enums_8h.html#a3c11f0d0cd9689554b34ccbc325da54fa39c1b2ff3bdce8072d074f15c6c2af49", null ],
      [ "TARGET_FLAG_SOURCE_LOCATION", "_d_b_c_enums_8h.html#a3c11f0d0cd9689554b34ccbc325da54fa76062c8af712eecc1b87da7a458a86eb", null ],
      [ "TARGET_FLAG_DEST_LOCATION", "_d_b_c_enums_8h.html#a3c11f0d0cd9689554b34ccbc325da54fa6743615e8912aefb100fe3f804c162be", null ],
      [ "TARGET_FLAG_OBJECT_UNK", "_d_b_c_enums_8h.html#a3c11f0d0cd9689554b34ccbc325da54fa2ecde1db065b930443d9f155b352a268", null ],
      [ "TARGET_FLAG_UNIT_UNK", "_d_b_c_enums_8h.html#a3c11f0d0cd9689554b34ccbc325da54fa2893324f657b6c9cae5b353223354c3d", null ],
      [ "TARGET_FLAG_PVP_CORPSE", "_d_b_c_enums_8h.html#a3c11f0d0cd9689554b34ccbc325da54fadc951eda4e24c0f16a7a7ea9ed27d017", null ],
      [ "TARGET_FLAG_UNIT_CORPSE", "_d_b_c_enums_8h.html#a3c11f0d0cd9689554b34ccbc325da54fa048bfe66d6b588078e1abd0ddeff555e", null ],
      [ "TARGET_FLAG_OBJECT", "_d_b_c_enums_8h.html#a3c11f0d0cd9689554b34ccbc325da54fae89a6bec789f7a30229ad400520c325a", null ],
      [ "TARGET_FLAG_TRADE_ITEM", "_d_b_c_enums_8h.html#a3c11f0d0cd9689554b34ccbc325da54fa03c8f27ac662b23e30c405f800e13da4", null ],
      [ "TARGET_FLAG_STRING", "_d_b_c_enums_8h.html#a3c11f0d0cd9689554b34ccbc325da54fa24d43cf0043fec76138893870d0f33e4", null ],
      [ "TARGET_FLAG_GAMEOBJECT_ITEM", "_d_b_c_enums_8h.html#a3c11f0d0cd9689554b34ccbc325da54fa57bdcad724ff2971c1476f51034d2418", null ],
      [ "TARGET_FLAG_CORPSE", "_d_b_c_enums_8h.html#a3c11f0d0cd9689554b34ccbc325da54fac5d96b2542ae80aa4e010eb2a0090ffc", null ],
      [ "TARGET_FLAG_UNK2", "_d_b_c_enums_8h.html#a3c11f0d0cd9689554b34ccbc325da54faaedd4f2f1ddaac062285334cab69c981", null ]
    ] ],
    [ "SpellEffectIndex", "_d_b_c_enums_8h.html#a6be7938b7bea685e9f021da7acb6d9b6", [
      [ "EFFECT_INDEX_0", "_d_b_c_enums_8h.html#a6be7938b7bea685e9f021da7acb6d9b6a6eb7ddf7003f3af330b407084e1d88b1", null ],
      [ "EFFECT_INDEX_1", "_d_b_c_enums_8h.html#a6be7938b7bea685e9f021da7acb6d9b6a2515e06020940e666b94acc8835521db", null ],
      [ "EFFECT_INDEX_2", "_d_b_c_enums_8h.html#a6be7938b7bea685e9f021da7acb6d9b6a1c69b2509a4774a2c523cacb45edc840", null ]
    ] ],
    [ "SpellFamily", "_d_b_c_enums_8h.html#ad49a8709b09d0f77d54e799b71495e1c", [
      [ "SPELLFAMILY_GENERIC", "_d_b_c_enums_8h.html#ad49a8709b09d0f77d54e799b71495e1ca0ebbb6206eb92cdc484b416eb6f41743", null ],
      [ "SPELLFAMILY_ENVIRONMENT", "_d_b_c_enums_8h.html#ad49a8709b09d0f77d54e799b71495e1caa8d6f056b36f4e6fe9dd497f49d35708", null ],
      [ "SPELLFAMILY_MAGE", "_d_b_c_enums_8h.html#ad49a8709b09d0f77d54e799b71495e1ca0e4b88db8be01ea5d62cc08d07b28aae", null ],
      [ "SPELLFAMILY_WARRIOR", "_d_b_c_enums_8h.html#ad49a8709b09d0f77d54e799b71495e1ca52acbcf14a96b5ed01b8a43052a26a11", null ],
      [ "SPELLFAMILY_WARLOCK", "_d_b_c_enums_8h.html#ad49a8709b09d0f77d54e799b71495e1ca0a134bc75d430da10f6c1df4977c29cf", null ],
      [ "SPELLFAMILY_PRIEST", "_d_b_c_enums_8h.html#ad49a8709b09d0f77d54e799b71495e1ca04f59ec569a753b4e47a0a230782f207", null ],
      [ "SPELLFAMILY_DRUID", "_d_b_c_enums_8h.html#ad49a8709b09d0f77d54e799b71495e1cac3788ce010075b8bd4e6083638d8cd40", null ],
      [ "SPELLFAMILY_ROGUE", "_d_b_c_enums_8h.html#ad49a8709b09d0f77d54e799b71495e1ca9160b2e3a87a908af25c9a594295c0c1", null ],
      [ "SPELLFAMILY_HUNTER", "_d_b_c_enums_8h.html#ad49a8709b09d0f77d54e799b71495e1ca66df9b34a3b922a8242d3dfb74494026", null ],
      [ "SPELLFAMILY_PALADIN", "_d_b_c_enums_8h.html#ad49a8709b09d0f77d54e799b71495e1ca573f9ad8924e7632156291378448e742", null ],
      [ "SPELLFAMILY_SHAMAN", "_d_b_c_enums_8h.html#ad49a8709b09d0f77d54e799b71495e1ca9c8a9c6e102c760263471a93e58e80ea", null ],
      [ "SPELLFAMILY_POTION", "_d_b_c_enums_8h.html#ad49a8709b09d0f77d54e799b71495e1ca84336097f9e363a9b6a4d7d4839c1b54", null ]
    ] ]
];