var _move_map_shared_defines_8h =
[
    [ "MmapTileHeader", "struct_mmap_tile_header.html", "struct_mmap_tile_header" ],
    [ "MMAP_MAGIC", "_move_map_shared_defines_8h.html#af7a9a5e840ab3ad1c160e832500016d1", null ],
    [ "MMAP_VERSION", "_move_map_shared_defines_8h.html#aea71555fdd0af33ccc21c76c67574421", null ],
    [ "NavTerrain", "_move_map_shared_defines_8h.html#a8a4d0b36dd46d60b691b894fb8c1c5db", [
      [ "NAV_EMPTY", "_move_map_shared_defines_8h.html#a8a4d0b36dd46d60b691b894fb8c1c5dba7e7367494e3519c95f1b756726fec9a5", null ],
      [ "NAV_GROUND", "_move_map_shared_defines_8h.html#a8a4d0b36dd46d60b691b894fb8c1c5dbac2c74d40490a8ea42121c00a8249bd98", null ],
      [ "NAV_MAGMA", "_move_map_shared_defines_8h.html#a8a4d0b36dd46d60b691b894fb8c1c5dba9ea5d0550a08be7bb291557f1fc4f670", null ],
      [ "NAV_SLIME", "_move_map_shared_defines_8h.html#a8a4d0b36dd46d60b691b894fb8c1c5dbad892c321d39d49cdb700eb27fc249f75", null ],
      [ "NAV_WATER", "_move_map_shared_defines_8h.html#a8a4d0b36dd46d60b691b894fb8c1c5dbae258322a4a7d7928a89ac183ecf137d1", null ],
      [ "NAV_UNUSED1", "_move_map_shared_defines_8h.html#a8a4d0b36dd46d60b691b894fb8c1c5dba55b6c76a11113bfd585b61366a3f6d03", null ],
      [ "NAV_UNUSED2", "_move_map_shared_defines_8h.html#a8a4d0b36dd46d60b691b894fb8c1c5dba4c61900269be6dee2e26cbeb4e09d0bb", null ],
      [ "NAV_UNUSED3", "_move_map_shared_defines_8h.html#a8a4d0b36dd46d60b691b894fb8c1c5dba3055dd71ec523facb0cba2976316d530", null ],
      [ "NAV_UNUSED4", "_move_map_shared_defines_8h.html#a8a4d0b36dd46d60b691b894fb8c1c5dba94351935a3c7389b4667144d53923d56", null ]
    ] ]
];