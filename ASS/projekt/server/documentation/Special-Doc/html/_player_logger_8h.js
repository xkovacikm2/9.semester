var _player_logger_8h =
[
    [ "PlayerLogBase", "struct_player_log_base.html", "struct_player_log_base" ],
    [ "PlayerLogDamage", "struct_player_log_damage.html", "struct_player_log_damage" ],
    [ "PlayerLogLooting", "struct_player_log_looting.html", "struct_player_log_looting" ],
    [ "PlayerLogTrading", "struct_player_log_trading.html", "struct_player_log_trading" ],
    [ "PlayerLogKilling", "struct_player_log_killing.html", "struct_player_log_killing" ],
    [ "PlayerLogPosition", "struct_player_log_position.html", "struct_player_log_position" ],
    [ "PlayerLogProgress", "struct_player_log_progress.html", "struct_player_log_progress" ],
    [ "PlayerLogger", "class_player_logger.html", "class_player_logger" ],
    [ "MAX_PLAYER_LOG_ENTITIES", "_player_logger_8h.html#a873b959e5344dafcd960931f5e8431a1", null ],
    [ "PLAYER_LOGMASK_ANYTHING", "_player_logger_8h.html#a3d2127ed909504fbb7c383076a335476", null ],
    [ "PlayerLogBaseType", "_player_logger_8h.html#a74f77999cdf7015ceb1cc2dce264ec3d", null ],
    [ "LootSourceType", "_player_logger_8h.html#a421d488ff1b7698af8727debf20478de", [
      [ "LOOTSOURCE_CREATURE", "_player_logger_8h.html#a421d488ff1b7698af8727debf20478dea665b0b1f32d1aba880fcc76f0174cbbb", null ],
      [ "LOOTSOURCE_GAMEOBJECT", "_player_logger_8h.html#a421d488ff1b7698af8727debf20478dea6c5d705e1b8089516e91e2b4a07817ad", null ],
      [ "LOOTSOURCE_SPELL", "_player_logger_8h.html#a421d488ff1b7698af8727debf20478dea1d21d5d798c9f747f6f989863ce8214b", null ],
      [ "LOOTSOURCE_VENDOR", "_player_logger_8h.html#a421d488ff1b7698af8727debf20478dea056dbfb55e28861cd965b188c4953b8b", null ],
      [ "LOOTSOURCE_LETTER", "_player_logger_8h.html#a421d488ff1b7698af8727debf20478deaa0e8e9877fabd28f0e8039054995cd06", null ]
    ] ],
    [ "PlayerLogEntity", "_player_logger_8h.html#af77069239426eebf52cb50852354d6a6", [
      [ "PLAYER_LOG_DAMAGE_GET", "_player_logger_8h.html#af77069239426eebf52cb50852354d6a6a96f98e76acf49e34e5490f5b9fc9256b", null ],
      [ "PLAYER_LOG_DAMAGE_DONE", "_player_logger_8h.html#af77069239426eebf52cb50852354d6a6acbe77e54ec767cc3b16301834eb8782f", null ],
      [ "PLAYER_LOG_LOOTING", "_player_logger_8h.html#af77069239426eebf52cb50852354d6a6acdf0ceacae590e12920a815b786e854d", null ],
      [ "PLAYER_LOG_TRADE", "_player_logger_8h.html#af77069239426eebf52cb50852354d6a6a73dd370e6d49286b7e7cb7559965c2e8", null ],
      [ "PLAYER_LOG_KILL", "_player_logger_8h.html#af77069239426eebf52cb50852354d6a6a82d5a88ee76c4f894aabd2c0c22894e0", null ],
      [ "PLAYER_LOG_POSITION", "_player_logger_8h.html#af77069239426eebf52cb50852354d6a6a7de9090441b1345e97d013a4e5e700bd", null ],
      [ "PLAYER_LOG_PROGRESS", "_player_logger_8h.html#af77069239426eebf52cb50852354d6a6a4e6a502fa2c22cb00ef18488cdb431d0", null ]
    ] ],
    [ "PlayerLogMask", "_player_logger_8h.html#a8e75460fafe3ba89805fa0453da690a0", [
      [ "PLAYER_LOGMASK_DAMAGE_GET", "_player_logger_8h.html#a8e75460fafe3ba89805fa0453da690a0af7751d6737fa0521a51f24feaec418a4", null ],
      [ "PLAYER_LOGMASK_DAMAGE_DONE", "_player_logger_8h.html#a8e75460fafe3ba89805fa0453da690a0a08c805ef00e662ef3e75c2066ef34ea8", null ],
      [ "PLAYER_LOGMASK_LOOTING", "_player_logger_8h.html#a8e75460fafe3ba89805fa0453da690a0a3da8b6b38d8ef5348a073c77cdaa797d", null ],
      [ "PLAYER_LOGMASK_TRADE", "_player_logger_8h.html#a8e75460fafe3ba89805fa0453da690a0acbf1ef68094d7f5a88a1de76d495b56c", null ],
      [ "PLAYER_LOGMASK_KILL", "_player_logger_8h.html#a8e75460fafe3ba89805fa0453da690a0ae1920300df6fa801f83e5f4b7b1a49f8", null ],
      [ "PLAYER_LOGMASK_POSITION", "_player_logger_8h.html#a8e75460fafe3ba89805fa0453da690a0a059f171fc27ab40cfde027e5b62bac7f", null ],
      [ "PLAYER_LOGMASK_PROGRESS", "_player_logger_8h.html#a8e75460fafe3ba89805fa0453da690a0a4fad26323d71b1cad66f2f162d6588ef", null ]
    ] ],
    [ "ProgressType", "_player_logger_8h.html#a0a558d538b8742adf8b1251f2848ba75", [
      [ "PROGRESS_LEVEL", "_player_logger_8h.html#a0a558d538b8742adf8b1251f2848ba75a4b7b07aa3192128023ca5e658616bf02", null ],
      [ "PROGRESS_REPUTATION", "_player_logger_8h.html#a0a558d538b8742adf8b1251f2848ba75aa4eba32ffad0efc3e3c464bf44158956", null ],
      [ "PROGRESS_BOSS_KILL", "_player_logger_8h.html#a0a558d538b8742adf8b1251f2848ba75a3df4271bdbc739bc313fe5a5bac64a7c", null ],
      [ "PROGRESS_PET_LEVEL", "_player_logger_8h.html#a0a558d538b8742adf8b1251f2848ba75a664b5c281d969c2f66363c73b948c69c", null ]
    ] ]
];