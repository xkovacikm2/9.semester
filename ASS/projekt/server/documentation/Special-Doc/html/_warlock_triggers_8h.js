var _warlock_triggers_8h =
[
    [ "DemonArmorTrigger", "classai_1_1_demon_armor_trigger.html", "classai_1_1_demon_armor_trigger" ],
    [ "SpellstoneTrigger", "classai_1_1_spellstone_trigger.html", "classai_1_1_spellstone_trigger" ],
    [ "CorruptionOnAttackerTrigger", "classai_1_1_corruption_on_attacker_trigger.html", "classai_1_1_corruption_on_attacker_trigger" ],
    [ "ShadowTranceTrigger", "classai_1_1_shadow_trance_trigger.html", "classai_1_1_shadow_trance_trigger" ],
    [ "BacklashTrigger", "classai_1_1_backlash_trigger.html", "classai_1_1_backlash_trigger" ],
    [ "BanishTrigger", "classai_1_1_banish_trigger.html", "classai_1_1_banish_trigger" ],
    [ "WarlockConjuredItemTrigger", "classai_1_1_warlock_conjured_item_trigger.html", "classai_1_1_warlock_conjured_item_trigger" ],
    [ "HasSpellstoneTrigger", "classai_1_1_has_spellstone_trigger.html", "classai_1_1_has_spellstone_trigger" ],
    [ "HasFirestoneTrigger", "classai_1_1_has_firestone_trigger.html", "classai_1_1_has_firestone_trigger" ],
    [ "HasHealthstoneTrigger", "classai_1_1_has_healthstone_trigger.html", "classai_1_1_has_healthstone_trigger" ],
    [ "FearTrigger", "classai_1_1_fear_trigger.html", "classai_1_1_fear_trigger" ],
    [ "DEBUFF_TRIGGER", "_warlock_triggers_8h.html#ae118b639c6f86d7a5af94e31f0a6437e", null ],
    [ "DEBUFF_TRIGGER", "_warlock_triggers_8h.html#a53d8840a9d5bf4d1435c0e8ff3c9fa65", null ],
    [ "DEBUFF_TRIGGER", "_warlock_triggers_8h.html#a4d69b06fed70cac3451de9748ad20570", null ]
];