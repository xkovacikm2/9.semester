var class_object_registry =
[
    [ "RegistryMapType", "class_object_registry.html#aa5b8c7c433020423915c00e6f377f810", null ],
    [ "GetRegisteredItems", "class_object_registry.html#ae3933d28a86f607e457b7b24689c047b", null ],
    [ "GetRegisteredItems", "class_object_registry.html#a7bc6262d5eec35cab2743864e58c1d94", null ],
    [ "GetRegistryItem", "class_object_registry.html#ad0c5b2fbcc81625755765ae04540f2de", null ],
    [ "HasItem", "class_object_registry.html#aedb06258c39c158185edf7ab48485f31", null ],
    [ "InsertItem", "class_object_registry.html#ab39253451b4d3c807984ae608e95883d", null ],
    [ "RemoveItem", "class_object_registry.html#a63d1dfd20325bbe4fd141cd9a90d2f10", null ],
    [ "MaNGOS::OperatorNew< ObjectRegistry< T, Key > >", "class_object_registry.html#a75f7e291342f70bde7c8f849707a2211", null ]
];