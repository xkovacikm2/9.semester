var dir_9c0cc4a17caf618cdef1d4ae699cde02 =
[
    [ "FollowerReference.cpp", "_follower_reference_8cpp.html", null ],
    [ "FollowerReference.h", "_follower_reference_8h.html", [
      [ "FollowerReference", "class_follower_reference.html", "class_follower_reference" ]
    ] ],
    [ "FollowerRefManager.h", "_follower_ref_manager_8h.html", [
      [ "FollowerRefManager", "class_follower_ref_manager.html", null ]
    ] ],
    [ "GroupReference.cpp", "_group_reference_8cpp.html", null ],
    [ "GroupReference.h", "_group_reference_8h.html", [
      [ "GroupReference", "class_group_reference.html", "class_group_reference" ]
    ] ],
    [ "GroupRefManager.h", "_group_ref_manager_8h.html", [
      [ "GroupRefManager", "class_group_ref_manager.html", "class_group_ref_manager" ]
    ] ],
    [ "HostileRefManager.cpp", "_hostile_ref_manager_8cpp.html", null ],
    [ "HostileRefManager.h", "_hostile_ref_manager_8h.html", [
      [ "HostileRefManager", "class_hostile_ref_manager.html", "class_hostile_ref_manager" ]
    ] ],
    [ "MapReference.h", "_map_reference_8h.html", [
      [ "MapReference", "class_map_reference.html", "class_map_reference" ]
    ] ],
    [ "MapRefManager.h", "_map_ref_manager_8h.html", [
      [ "MapRefManager", "class_map_ref_manager.html", "class_map_ref_manager" ]
    ] ],
    [ "ThreatManager.cpp", "_threat_manager_8cpp.html", "_threat_manager_8cpp" ],
    [ "ThreatManager.h", "_threat_manager_8h.html", "_threat_manager_8h" ]
];