var _n_grid_8h =
[
    [ "GridInfo", "class_grid_info.html", "class_grid_info" ],
    [ "NGrid", "class_n_grid.html", "class_n_grid" ],
    [ "grid_state_t", "_n_grid_8h.html#a32c12f012f7ccd847bea5ec25560fe8b", [
      [ "GRID_STATE_INVALID", "_n_grid_8h.html#a32c12f012f7ccd847bea5ec25560fe8ba4f79947fb278f229b6d8fb1a86547a95", null ],
      [ "GRID_STATE_ACTIVE", "_n_grid_8h.html#a32c12f012f7ccd847bea5ec25560fe8bab205eb0d5b1044796728b09c3dd6e783", null ],
      [ "GRID_STATE_IDLE", "_n_grid_8h.html#a32c12f012f7ccd847bea5ec25560fe8ba5945c348b5de04475d11ea3bbcaacb03", null ],
      [ "GRID_STATE_REMOVAL", "_n_grid_8h.html#a32c12f012f7ccd847bea5ec25560fe8ba1a08174492eddab695a4fc5498b037e1", null ],
      [ "MAX_GRID_STATE", "_n_grid_8h.html#a32c12f012f7ccd847bea5ec25560fe8baed0aa8ae99c3b46e758755de2f607986", null ]
    ] ]
];