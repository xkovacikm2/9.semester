var struct_spell_modifier =
[
    [ "SpellModifier", "struct_spell_modifier.html#a865629ebc327cdb047bb1192babaac9d", null ],
    [ "SpellModifier", "struct_spell_modifier.html#abf35522fa51eb896b75c1b3e2dd2d444", null ],
    [ "SpellModifier", "struct_spell_modifier.html#aa08ba52c4fcb09b61f2b6e4d39786e4f", null ],
    [ "SpellModifier", "struct_spell_modifier.html#aa1f690f5c1835afbb84b9cc7ad143fc1", null ],
    [ "SpellModifier", "struct_spell_modifier.html#af6945299740bb47322bc5da417513402", null ],
    [ "isAffectedOnSpell", "struct_spell_modifier.html#ae5b95bade6b50c931440a2e71218f3be", null ],
    [ "charges", "struct_spell_modifier.html#a1aaf71299c2dd27b58a049d252efdee8", null ],
    [ "lastAffected", "struct_spell_modifier.html#a4d09fe36c5a183ae1437233c23a16908", null ],
    [ "mask", "struct_spell_modifier.html#adf6cef44bd7c9676be950118bc9cc6c6", null ],
    [ "op", "struct_spell_modifier.html#a21d017f0c8d3108a0eb5f48798103356", null ],
    [ "spellId", "struct_spell_modifier.html#a465803efab27b2ba0a49c6571b65b908", null ],
    [ "type", "struct_spell_modifier.html#a22f64b9c021f5a87f30f01ef117ebb0b", null ],
    [ "value", "struct_spell_modifier.html#a31742662ba8112c9e9b972897a928e57", null ]
];