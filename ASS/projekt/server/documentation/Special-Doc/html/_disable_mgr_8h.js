var _disable_mgr_8h =
[
    [ "DisableType", "_disable_mgr_8h.html#affa2a0d7ee61f6e1bc7c6cb7d2c1b64c", [
      [ "DISABLE_TYPE_SPELL", "_disable_mgr_8h.html#affa2a0d7ee61f6e1bc7c6cb7d2c1b64ca0f70e5147b74bccf96a1ff907ae92c27", null ],
      [ "DISABLE_TYPE_QUEST", "_disable_mgr_8h.html#affa2a0d7ee61f6e1bc7c6cb7d2c1b64ca53c0091b82a6fc9bc32393f75af1e49e", null ],
      [ "DISABLE_TYPE_MAP", "_disable_mgr_8h.html#affa2a0d7ee61f6e1bc7c6cb7d2c1b64ca7401630edc869fdab991560043243d9c", null ],
      [ "DISABLE_TYPE_BATTLEGROUND", "_disable_mgr_8h.html#affa2a0d7ee61f6e1bc7c6cb7d2c1b64ca9f1b696021b5e159000a608fdfb2e96a", null ],
      [ "DISABLE_TYPE_ACHIEVEMENT_CRITERIA", "_disable_mgr_8h.html#affa2a0d7ee61f6e1bc7c6cb7d2c1b64caa8a9feebca9f0cf6b1c70617fc0687d1", null ],
      [ "DISABLE_TYPE_OUTDOORPVP", "_disable_mgr_8h.html#affa2a0d7ee61f6e1bc7c6cb7d2c1b64caf73f51beb622b9096abd95821ac52296", null ],
      [ "DISABLE_TYPE_VMAP", "_disable_mgr_8h.html#affa2a0d7ee61f6e1bc7c6cb7d2c1b64ca3aa1423c0ff6356f4e839a98f97a4aff", null ],
      [ "DISABLE_TYPE_MMAP", "_disable_mgr_8h.html#affa2a0d7ee61f6e1bc7c6cb7d2c1b64ca2e857f14ca7ab95a238d10358186d993", null ],
      [ "DISABLE_TYPE_CREATURE_SPAWN", "_disable_mgr_8h.html#affa2a0d7ee61f6e1bc7c6cb7d2c1b64ca50493f7f7928a1de111e90f23e9a7e8c", null ],
      [ "DISABLE_TYPE_GAMEOBJECT_SPAWN", "_disable_mgr_8h.html#affa2a0d7ee61f6e1bc7c6cb7d2c1b64ca45d6684e11e004eb2a926a34e4d2ce9d", null ],
      [ "DISABLE_TYPE_ITEM_DROP", "_disable_mgr_8h.html#affa2a0d7ee61f6e1bc7c6cb7d2c1b64ca65c5f255fdd16157306688cb10ccb983", null ],
      [ "MAX_DISABLE_TYPES", "_disable_mgr_8h.html#affa2a0d7ee61f6e1bc7c6cb7d2c1b64ca377f1ee93641f0ea11503063c285bc9f", null ]
    ] ],
    [ "SpawnDisableTypes", "_disable_mgr_8h.html#afadaa203ac801791622488468049c802", [
      [ "SPAWN_DISABLE_CHECK_GUID", "_disable_mgr_8h.html#afadaa203ac801791622488468049c802a641ad40999cfe1d5b707ae9211cdef9f", null ]
    ] ],
    [ "SpellDisableTypes", "_disable_mgr_8h.html#a7b9e8ca1dcd13802fa528f955deaefd7", [
      [ "SPELL_DISABLE_PLAYER", "_disable_mgr_8h.html#a7b9e8ca1dcd13802fa528f955deaefd7aad7fc2c20248b5ccc6c7af46c0627a41", null ],
      [ "SPELL_DISABLE_CREATURE", "_disable_mgr_8h.html#a7b9e8ca1dcd13802fa528f955deaefd7a1af9f400d91b09580b35048cb1ffa707", null ],
      [ "SPELL_DISABLE_PET", "_disable_mgr_8h.html#a7b9e8ca1dcd13802fa528f955deaefd7a69cb584bf80eea477b207020b21810e4", null ],
      [ "SPELL_DISABLE_DEPRECATED_SPELL", "_disable_mgr_8h.html#a7b9e8ca1dcd13802fa528f955deaefd7abf8d2121280a5024433b90335617ad2b", null ],
      [ "SPELL_DISABLE_MAP", "_disable_mgr_8h.html#a7b9e8ca1dcd13802fa528f955deaefd7a814538087543158616640736f8511484", null ],
      [ "SPELL_DISABLE_AREA", "_disable_mgr_8h.html#a7b9e8ca1dcd13802fa528f955deaefd7af0b7760e1bbbca07ae256ffe853a0268", null ],
      [ "SPELL_DISABLE_LOS", "_disable_mgr_8h.html#a7b9e8ca1dcd13802fa528f955deaefd7a1a78c3d652ddec71707bf22ec83df437", null ],
      [ "MAX_SPELL_DISABLE_TYPE", "_disable_mgr_8h.html#a7b9e8ca1dcd13802fa528f955deaefd7ac556c5fec4ec5e1fb62f6f2a5904926d", null ]
    ] ],
    [ "CheckQuestDisables", "_disable_mgr_8h.html#a2f9fa5dadf6f2da5f01651d37d996377", null ],
    [ "IsDisabledFor", "_disable_mgr_8h.html#af4034a78efc086f320d945bfefbead32", null ],
    [ "IsPathfindingEnabled", "_disable_mgr_8h.html#a2fb2cc0c789502e1fa3384d07b07d29f", null ],
    [ "IsVMAPDisabledFor", "_disable_mgr_8h.html#a38b182e1836b2197303b2809006f5148", null ],
    [ "LoadDisables", "_disable_mgr_8h.html#adb464165caf6cd436499fa84969eeaf8", null ]
];