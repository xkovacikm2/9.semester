var class_movement_info =
[
    [ "JumpInfo", "struct_movement_info_1_1_jump_info.html", "struct_movement_info_1_1_jump_info" ],
    [ "MovementInfo", "class_movement_info.html#a809340998de55a9ffa895f7b27e1f2c3", null ],
    [ "AddMovementFlag", "class_movement_info.html#a58d0d10895ca4e57a3786fadb3d28801", null ],
    [ "ChangeOrientation", "class_movement_info.html#a99a960e12901bd17e6caeaa5fd070c63", null ],
    [ "ChangePosition", "class_movement_info.html#ab43e1ed13d6f3f648a64bd08d610603d", null ],
    [ "ClearTransportData", "class_movement_info.html#aa163ec16a7a02f69beed214cd6803e2c", null ],
    [ "GetFallTime", "class_movement_info.html#a91f8b6010e20388e09502414a9e9175a", null ],
    [ "GetJumpInfo", "class_movement_info.html#a29045481e443221a26ec280c7d93813e", null ],
    [ "GetMovementFlags", "class_movement_info.html#a3983d49ce0d4e786f9d49faed3b63368", null ],
    [ "GetPos", "class_movement_info.html#a97f26c16cc44d0cfa4514f60221193c6", null ],
    [ "GetTime", "class_movement_info.html#aa39eb0d00c83513c58fb6bc02de6d02a", null ],
    [ "GetTransportGuid", "class_movement_info.html#a1e390edb63f0e0513096e118c5500ab8", null ],
    [ "GetTransportPos", "class_movement_info.html#abf2309d96a01a3bae3a10dc23b54194e", null ],
    [ "GetTransportTime", "class_movement_info.html#ad9a4d8e5f2ac1ca619c76226b78c8a31", null ],
    [ "HasMovementFlag", "class_movement_info.html#af0b95ddc9ba7d602a37c310fea0bda18", null ],
    [ "Read", "class_movement_info.html#a88f54e4da5b6b76b2682d2a5b31d95b5", null ],
    [ "RemoveMovementFlag", "class_movement_info.html#add7028402acf458ca19c20879f9f0acb", null ],
    [ "SetMovementFlags", "class_movement_info.html#a6301810c32e9c1f4422bc3d3ae890593", null ],
    [ "SetTransportData", "class_movement_info.html#ae264262022e9d21c61adf1f7af0015fc", null ],
    [ "UpdateTime", "class_movement_info.html#a0b42045f62de341993bde604bb52a88b", null ],
    [ "Write", "class_movement_info.html#a115b3c1a33936bb5e110a4f8debefab3", null ]
];