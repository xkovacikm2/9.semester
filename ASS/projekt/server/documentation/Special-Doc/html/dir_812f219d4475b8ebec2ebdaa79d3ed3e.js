var dir_812f219d4475b8ebec2ebdaa79d3ed3e =
[
    [ "actions", "dir_c2e9f8fa78e9669d50fce0ccbaab85fe.html", "dir_c2e9f8fa78e9669d50fce0ccbaab85fe" ],
    [ "druid", "dir_4f1b0ef0289e4ff1aca03935565f461d.html", "dir_4f1b0ef0289e4ff1aca03935565f461d" ],
    [ "generic", "dir_05b318874d7d32bf8abf1851f83f4fdc.html", "dir_05b318874d7d32bf8abf1851f83f4fdc" ],
    [ "hunter", "dir_49e1d7b0fd1e711e535564c6e608c4a8.html", "dir_49e1d7b0fd1e711e535564c6e608c4a8" ],
    [ "mage", "dir_9c02e9bf594b5a83ee5d53a0d777cfb0.html", "dir_9c02e9bf594b5a83ee5d53a0d777cfb0" ],
    [ "paladin", "dir_f301022e1aba199a194511d5d172bb2a.html", "dir_f301022e1aba199a194511d5d172bb2a" ],
    [ "priest", "dir_e4951c08c1bf6a3a67b1085355e2e55f.html", "dir_e4951c08c1bf6a3a67b1085355e2e55f" ],
    [ "rogue", "dir_8b94250c24fae658ea9adc82d318d255.html", "dir_8b94250c24fae658ea9adc82d318d255" ],
    [ "shaman", "dir_62aedd113fb900a53d1af192539901ce.html", "dir_62aedd113fb900a53d1af192539901ce" ],
    [ "triggers", "dir_8d7cb7c192d5df63d14ca6d72c687c4a.html", "dir_8d7cb7c192d5df63d14ca6d72c687c4a" ],
    [ "values", "dir_1cdaa8868c9e42d49eda639e1a8e34f3.html", "dir_1cdaa8868c9e42d49eda639e1a8e34f3" ],
    [ "warlock", "dir_997579ee45276ed3a91ae4402b67abf3.html", "dir_997579ee45276ed3a91ae4402b67abf3" ],
    [ "warrior", "dir_efd51433fdbae5bde3101409eddf657b.html", "dir_efd51433fdbae5bde3101409eddf657b" ],
    [ "Action.cpp", "_action_8cpp.html", null ],
    [ "Action.h", "_action_8h.html", "_action_8h" ],
    [ "ActionBasket.cpp", "_action_basket_8cpp.html", null ],
    [ "ActionBasket.h", "_action_basket_8h.html", null ],
    [ "AiObject.cpp", "_ai_object_8cpp.html", null ],
    [ "AiObject.h", "_ai_object_8h.html", [
      [ "AiObject", "classai_1_1_ai_object.html", "classai_1_1_ai_object" ],
      [ "AiNamedObject", "classai_1_1_ai_named_object.html", "classai_1_1_ai_named_object" ]
    ] ],
    [ "AiObjectContext.cpp", "_ai_object_context_8cpp.html", null ],
    [ "AiObjectContext.h", "_ai_object_context_8h.html", [
      [ "AiObjectContext", "classai_1_1_ai_object_context.html", "classai_1_1_ai_object_context" ]
    ] ],
    [ "Engine.cpp", "_engine_8cpp.html", null ],
    [ "Engine.h", "_engine_8h.html", "_engine_8h" ],
    [ "Event.cpp", "_event_8cpp.html", null ],
    [ "Event.h", "_event_8h.html", [
      [ "Event", "classai_1_1_event.html", "classai_1_1_event" ]
    ] ],
    [ "ExternalEventHelper.h", "_external_event_helper_8h.html", [
      [ "ExternalEventHelper", "classai_1_1_external_event_helper.html", "classai_1_1_external_event_helper" ]
    ] ],
    [ "ItemVisitors.h", "_item_visitors_8h.html", "_item_visitors_8h" ],
    [ "Multiplier.cpp", "_multiplier_8cpp.html", null ],
    [ "Multiplier.h", "_multiplier_8h.html", [
      [ "Multiplier", "classai_1_1_multiplier.html", "classai_1_1_multiplier" ]
    ] ],
    [ "NamedObjectContext.h", "_named_object_context_8h.html", [
      [ "Qualified", "classai_1_1_qualified.html", "classai_1_1_qualified" ],
      [ "NamedObjectFactory", "classai_1_1_named_object_factory.html", "classai_1_1_named_object_factory" ],
      [ "NamedObjectContext", "classai_1_1_named_object_context.html", "classai_1_1_named_object_context" ],
      [ "NamedObjectContextList", "classai_1_1_named_object_context_list.html", "classai_1_1_named_object_context_list" ],
      [ "NamedObjectFactoryList", "classai_1_1_named_object_factory_list.html", "classai_1_1_named_object_factory_list" ]
    ] ],
    [ "PassiveMultiplier.cpp", "_passive_multiplier_8cpp.html", null ],
    [ "PassiveMultiplier.h", "_passive_multiplier_8h.html", [
      [ "PassiveMultiplier", "classai_1_1_passive_multiplier.html", "classai_1_1_passive_multiplier" ]
    ] ],
    [ "Queue.cpp", "_queue_8cpp.html", null ],
    [ "Queue.h", "_queue_8h.html", [
      [ "Queue", "classai_1_1_queue.html", "classai_1_1_queue" ]
    ] ],
    [ "Strategy.cpp", "_strategy_8cpp.html", [
      [ "ActionNodeFactoryInternal", "class_action_node_factory_internal.html", "class_action_node_factory_internal" ]
    ] ],
    [ "Strategy.h", "_strategy_8h.html", "_strategy_8h" ],
    [ "StrategyContext.h", "_strategy_context_8h.html", [
      [ "StrategyContext", "classai_1_1_strategy_context.html", "classai_1_1_strategy_context" ],
      [ "MovementStrategyContext", "classai_1_1_movement_strategy_context.html", "classai_1_1_movement_strategy_context" ],
      [ "AssistStrategyContext", "classai_1_1_assist_strategy_context.html", "classai_1_1_assist_strategy_context" ],
      [ "QuestStrategyContext", "classai_1_1_quest_strategy_context.html", "classai_1_1_quest_strategy_context" ]
    ] ],
    [ "Trigger.cpp", "_trigger_8cpp.html", null ],
    [ "Trigger.h", "_trigger_8h.html", "_trigger_8h" ],
    [ "Value.cpp", "_value_8cpp.html", null ],
    [ "Value.h", "_value_8h.html", [
      [ "UntypedValue", "classai_1_1_untyped_value.html", "classai_1_1_untyped_value" ],
      [ "Value", "classai_1_1_value.html", "classai_1_1_value" ],
      [ "CalculatedValue", "classai_1_1_calculated_value.html", "classai_1_1_calculated_value" ],
      [ "Uint8CalculatedValue", "classai_1_1_uint8_calculated_value.html", "classai_1_1_uint8_calculated_value" ],
      [ "Uint32CalculatedValue", "classai_1_1_uint32_calculated_value.html", "classai_1_1_uint32_calculated_value" ],
      [ "FloatCalculatedValue", "classai_1_1_float_calculated_value.html", "classai_1_1_float_calculated_value" ],
      [ "BoolCalculatedValue", "classai_1_1_bool_calculated_value.html", "classai_1_1_bool_calculated_value" ],
      [ "UnitCalculatedValue", "classai_1_1_unit_calculated_value.html", "classai_1_1_unit_calculated_value" ],
      [ "ObjectGuidListCalculatedValue", "classai_1_1_object_guid_list_calculated_value.html", "classai_1_1_object_guid_list_calculated_value" ],
      [ "ManualSetValue", "classai_1_1_manual_set_value.html", "classai_1_1_manual_set_value" ],
      [ "UnitManualSetValue", "classai_1_1_unit_manual_set_value.html", "classai_1_1_unit_manual_set_value" ]
    ] ]
];