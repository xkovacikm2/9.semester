var struct_b_g_data =
[
    [ "BGData", "struct_b_g_data.html#ab900bdb1414875ab04dba5bbb514ecc7", null ],
    [ "bgAfkReportedCount", "struct_b_g_data.html#ab7374e7b8fc6607dd6b4f04385e80ca3", null ],
    [ "bgAfkReportedTimer", "struct_b_g_data.html#a51f8f4e05dc0224982af9a61d2dfae36", null ],
    [ "bgAfkReporter", "struct_b_g_data.html#a988ea871fee6952c2ed7f3ec0740b170", null ],
    [ "bgInstanceID", "struct_b_g_data.html#a8eff26fd78b5763501a3f5460de4e551", null ],
    [ "bgTeam", "struct_b_g_data.html#a01c9e6c38ed1fe7fa89ccea6d3ae4351", null ],
    [ "bgTypeID", "struct_b_g_data.html#a3d89edb954339cdef72ca09dff40f279", null ],
    [ "joinPos", "struct_b_g_data.html#aa03e7ee93bfe40e0c220551d99d8564f", null ],
    [ "m_needSave", "struct_b_g_data.html#a8b2ca98425e227c04db966fb595723e3", null ]
];