var hierarchy =
[
    [ "MaNGOS::_Callback< Class, ParamType1, ParamType2, ParamType3, ParamType4 >", "class_ma_n_g_o_s_1_1___callback.html", [
      [ "MaNGOS::_ICallback< _Callback< Class, ParamType1, ParamType2, ParamType3, ParamType4 > >", "class_ma_n_g_o_s_1_1___i_callback.html", [
        [ "MaNGOS::Callback< Class, ParamType1, ParamType2, ParamType3, ParamType4 >", "class_ma_n_g_o_s_1_1_callback.html", null ]
      ] ]
    ] ],
    [ "MaNGOS::_Callback< Class >", "class_ma_n_g_o_s_1_1___callback_3_01_class_01_4.html", [
      [ "MaNGOS::_ICallback< _Callback< Class > >", "class_ma_n_g_o_s_1_1___i_callback.html", [
        [ "MaNGOS::Callback< Class >", "class_ma_n_g_o_s_1_1_callback_3_01_class_01_4.html", null ]
      ] ]
    ] ],
    [ "MaNGOS::_Callback< Class, ParamType1 >", "class_ma_n_g_o_s_1_1___callback_3_01_class_00_01_param_type1_01_4.html", [
      [ "MaNGOS::_ICallback< _Callback< Class, ParamType1 > >", "class_ma_n_g_o_s_1_1___i_callback.html", [
        [ "MaNGOS::Callback< Class, ParamType1 >", "class_ma_n_g_o_s_1_1_callback_3_01_class_00_01_param_type1_01_4.html", null ]
      ] ]
    ] ],
    [ "MaNGOS::_Callback< Class, ParamType1, ParamType2 >", "class_ma_n_g_o_s_1_1___callback_3_01_class_00_01_param_type1_00_01_param_type2_01_4.html", [
      [ "MaNGOS::_ICallback< _Callback< Class, ParamType1, ParamType2 > >", "class_ma_n_g_o_s_1_1___i_callback.html", [
        [ "MaNGOS::Callback< Class, ParamType1, ParamType2 >", "class_ma_n_g_o_s_1_1_callback_3_01_class_00_01_param_type1_00_01_param_type2_01_4.html", null ]
      ] ]
    ] ],
    [ "MaNGOS::_Callback< Class, ParamType1, ParamType2, ParamType3 >", "class_ma_n_g_o_s_1_1___callback_3_01_class_00_01_param_type1_00_01_param_type2_00_01_param_type3_01_4.html", [
      [ "MaNGOS::_ICallback< _Callback< Class, ParamType1, ParamType2, ParamType3 > >", "class_ma_n_g_o_s_1_1___i_callback.html", [
        [ "MaNGOS::Callback< Class, ParamType1, ParamType2, ParamType3 >", "class_ma_n_g_o_s_1_1_callback_3_01_class_00_01_param_type1_00_01_param_type2_00_01_param_type3_01_4.html", null ]
      ] ]
    ] ],
    [ "MaNGOS::_Callback< Class, QueryResult * >", "class_ma_n_g_o_s_1_1___callback.html", [
      [ "MaNGOS::_IQueryCallback< _Callback< Class, QueryResult *> >", "class_ma_n_g_o_s_1_1___i_query_callback.html", [
        [ "MaNGOS::QueryCallback< Class >", "class_ma_n_g_o_s_1_1_query_callback_3_01_class_01_4.html", null ]
      ] ]
    ] ],
    [ "MaNGOS::_Callback< Class, QueryResult *, ParamType1 >", "class_ma_n_g_o_s_1_1___callback.html", [
      [ "MaNGOS::_IQueryCallback< _Callback< Class, QueryResult *, ParamType1 > >", "class_ma_n_g_o_s_1_1___i_query_callback.html", [
        [ "MaNGOS::QueryCallback< Class, ParamType1 >", "class_ma_n_g_o_s_1_1_query_callback_3_01_class_00_01_param_type1_01_4.html", null ]
      ] ]
    ] ],
    [ "MaNGOS::_Callback< Class, QueryResult *, ParamType1, ParamType2 >", "class_ma_n_g_o_s_1_1___callback.html", [
      [ "MaNGOS::_IQueryCallback< _Callback< Class, QueryResult *, ParamType1, ParamType2 > >", "class_ma_n_g_o_s_1_1___i_query_callback.html", [
        [ "MaNGOS::QueryCallback< Class, ParamType1, ParamType2 >", "class_ma_n_g_o_s_1_1_query_callback_3_01_class_00_01_param_type1_00_01_param_type2_01_4.html", null ]
      ] ]
    ] ],
    [ "MaNGOS::_Callback< Class, QueryResult *, ParamType1, ParamType2, ParamType3 >", "class_ma_n_g_o_s_1_1___callback.html", [
      [ "MaNGOS::_IQueryCallback< _Callback< Class, QueryResult *, ParamType1, ParamType2, ParamType3 > >", "class_ma_n_g_o_s_1_1___i_query_callback.html", [
        [ "MaNGOS::QueryCallback< Class, ParamType1, ParamType2, ParamType3 >", "class_ma_n_g_o_s_1_1_query_callback.html", null ]
      ] ]
    ] ],
    [ "_Damage", "struct___damage.html", null ],
    [ "_ItemStat", "struct___item_stat.html", null ],
    [ "MaNGOS::_SCallback< ParamType1, ParamType2, ParamType3, ParamType4 >", "class_ma_n_g_o_s_1_1___s_callback.html", null ],
    [ "MaNGOS::_SCallback< ParamType1 >", "class_ma_n_g_o_s_1_1___s_callback_3_01_param_type1_01_4.html", null ],
    [ "MaNGOS::_SCallback< ParamType1, ParamType2 >", "class_ma_n_g_o_s_1_1___s_callback_3_01_param_type1_00_01_param_type2_01_4.html", null ],
    [ "MaNGOS::_SCallback< ParamType1, ParamType2, ParamType3 >", "class_ma_n_g_o_s_1_1___s_callback_3_01_param_type1_00_01_param_type2_00_01_param_type3_01_4.html", null ],
    [ "MaNGOS::_SCallback< QueryResult * >", "class_ma_n_g_o_s_1_1___s_callback.html", [
      [ "MaNGOS::_IQueryCallback< _SCallback< QueryResult *> >", "class_ma_n_g_o_s_1_1___i_query_callback.html", [
        [ "MaNGOS::SQueryCallback<>", "class_ma_n_g_o_s_1_1_s_query_callback_3_4.html", null ]
      ] ]
    ] ],
    [ "MaNGOS::_SCallback< QueryResult *, ParamType1 >", "class_ma_n_g_o_s_1_1___s_callback.html", [
      [ "MaNGOS::_IQueryCallback< _SCallback< QueryResult *, ParamType1 > >", "class_ma_n_g_o_s_1_1___i_query_callback.html", [
        [ "MaNGOS::SQueryCallback< ParamType1 >", "class_ma_n_g_o_s_1_1_s_query_callback_3_01_param_type1_01_4.html", null ]
      ] ]
    ] ],
    [ "MaNGOS::_SCallback< QueryResult *, ParamType1, ParamType2 >", "class_ma_n_g_o_s_1_1___s_callback.html", [
      [ "MaNGOS::_IQueryCallback< _SCallback< QueryResult *, ParamType1, ParamType2 > >", "class_ma_n_g_o_s_1_1___i_query_callback.html", [
        [ "MaNGOS::SQueryCallback< ParamType1, ParamType2 >", "class_ma_n_g_o_s_1_1_s_query_callback_3_01_param_type1_00_01_param_type2_01_4.html", null ]
      ] ]
    ] ],
    [ "MaNGOS::_SCallback< QueryResult *, ParamType1, ParamType2, ParamType3 >", "class_ma_n_g_o_s_1_1___s_callback.html", [
      [ "MaNGOS::_IQueryCallback< _SCallback< QueryResult *, ParamType1, ParamType2, ParamType3 > >", "class_ma_n_g_o_s_1_1___i_query_callback.html", [
        [ "MaNGOS::SQueryCallback< ParamType1, ParamType2, ParamType3 >", "class_ma_n_g_o_s_1_1_s_query_callback.html", null ]
      ] ]
    ] ],
    [ "MaNGOS::_SCallback<>", "class_ma_n_g_o_s_1_1___s_callback_3_4.html", null ],
    [ "_Socket", "struct___socket.html", null ],
    [ "_Spell", "struct___spell.html", null ],
    [ "AABound", "struct_a_a_bound.html", null ],
    [ "AccountMgr", "class_account_mgr.html", null ],
    [ "ACE_Method_Request", null, [
      [ "MapUpdateRequest", "class_map_update_request.html", null ]
    ] ],
    [ "ACE_Svc_Handler", null, [
      [ "RASocket", "class_r_a_socket.html", null ]
    ] ],
    [ "ACE_Task", null, [
      [ "AhbotThread", "class_ahbot_thread.html", null ],
      [ "SoapPool", "class_soap_pool.html", null ]
    ] ],
    [ "ACE_Task_Base", null, [
      [ "AntiFreezeThread", "class_anti_freeze_thread.html", null ],
      [ "CliThread", "class_cli_thread.html", null ],
      [ "DelayExecutor", "class_delay_executor.html", null ],
      [ "RAThread", "class_r_a_thread.html", null ],
      [ "SoapThread", "class_soap_thread.html", null ],
      [ "WorldSocketMgr", "class_world_socket_mgr.html", null ],
      [ "WorldThread", "class_world_thread.html", null ]
    ] ],
    [ "ai::ActionBasket", "classai_1_1_action_basket.html", null ],
    [ "ActionButton", "struct_action_button.html", null ],
    [ "ai::ActionExecutionListener", "classai_1_1_action_execution_listener.html", [
      [ "ai::ActionExecutionListeners", "classai_1_1_action_execution_listeners.html", null ]
    ] ],
    [ "ai::ActionNode", "classai_1_1_action_node.html", null ],
    [ "AddCreatureToRemoveListInMapsWorker", "struct_add_creature_to_remove_list_in_maps_worker.html", null ],
    [ "AddGameObjectToRemoveListInMapsWorker", "struct_add_game_object_to_remove_list_in_maps_worker.html", null ],
    [ "AddonHandler", "class_addon_handler.html", null ],
    [ "AHB_Buyer_Config", "struct_a_h_b___buyer___config.html", null ],
    [ "AHB_Seller_Config", "class_a_h_b___seller___config.html", null ],
    [ "ahbot::AhBot", "classahbot_1_1_ah_bot.html", null ],
    [ "AhBotConfig", "class_ah_bot_config.html", null ],
    [ "AiFactory", "class_ai_factory.html", null ],
    [ "MaNGOS::AllCreaturesOfEntryInRangeCheck", "class_ma_n_g_o_s_1_1_all_creatures_of_entry_in_range_check.html", null ],
    [ "MaNGOS::AllSpecificUnitsInGameObjectRangeDo", "class_ma_n_g_o_s_1_1_all_specific_units_in_game_object_range_do.html", null ],
    [ "MaNGOS::AnyAoETargetUnitInObjectRangeCheck", "class_ma_n_g_o_s_1_1_any_ao_e_target_unit_in_object_range_check.html", null ],
    [ "MaNGOS::AnyAoEVisibleTargetUnitInObjectRangeCheck", "class_ma_n_g_o_s_1_1_any_ao_e_visible_target_unit_in_object_range_check.html", null ],
    [ "MaNGOS::AnyAssistCreatureInRangeCheck", "class_ma_n_g_o_s_1_1_any_assist_creature_in_range_check.html", null ],
    [ "MaNGOS::AnyDeadUnitCheck", "class_ma_n_g_o_s_1_1_any_dead_unit_check.html", null ],
    [ "AnyDeadUnitInObjectRangeCheck", "class_any_dead_unit_in_object_range_check.html", null ],
    [ "MaNGOS::AnyFriendlyUnitInObjectRangeCheck", "class_ma_n_g_o_s_1_1_any_friendly_unit_in_object_range_check.html", null ],
    [ "AnyGameObjectInObjectRangeCheck", "class_any_game_object_in_object_range_check.html", null ],
    [ "MaNGOS::AnyPlayerInCapturePointRange", "class_ma_n_g_o_s_1_1_any_player_in_capture_point_range.html", null ],
    [ "MaNGOS::AnyPlayerInObjectRangeCheck", "class_ma_n_g_o_s_1_1_any_player_in_object_range_check.html", null ],
    [ "MaNGOS::AnyPlayerInObjectRangeWithAuraCheck", "class_ma_n_g_o_s_1_1_any_player_in_object_range_with_aura_check.html", null ],
    [ "MaNGOS::AnySpecificUnitInGameObjectRangeCheck", "class_ma_n_g_o_s_1_1_any_specific_unit_in_game_object_range_check.html", null ],
    [ "MaNGOS::AnyStealthedCheck", "class_ma_n_g_o_s_1_1_any_stealthed_check.html", null ],
    [ "MaNGOS::AnyUnfriendlyUnitInObjectRangeCheck", "class_ma_n_g_o_s_1_1_any_unfriendly_unit_in_object_range_check.html", null ],
    [ "MaNGOS::AnyUnfriendlyVisibleUnitInObjectRangeCheck", "class_ma_n_g_o_s_1_1_any_unfriendly_visible_unit_in_object_range_check.html", null ],
    [ "MaNGOS::AnyUnitInObjectRangeCheck", "class_ma_n_g_o_s_1_1_any_unit_in_object_range_check.html", null ],
    [ "ARC4", "class_a_r_c4.html", null ],
    [ "VMAP::AreaInfo", "struct_v_m_a_p_1_1_area_info.html", null ],
    [ "VMAP::AreaInfoCallback", "class_v_m_a_p_1_1_area_info_callback.html", null ],
    [ "Areas", "struct_areas.html", null ],
    [ "AreaTableEntry", "struct_area_table_entry.html", null ],
    [ "AreaTrigger", "struct_area_trigger.html", null ],
    [ "AreaTriggerEntry", "struct_area_trigger_entry.html", null ],
    [ "AuctionBotAgent", "class_auction_bot_agent.html", [
      [ "AuctionBotBuyer", "class_auction_bot_buyer.html", null ],
      [ "AuctionBotSeller", "class_auction_bot_seller.html", null ]
    ] ],
    [ "AuctionBotConfig", "class_auction_bot_config.html", null ],
    [ "AuctionEntry", "struct_auction_entry.html", null ],
    [ "AuctionHouseBot", "class_auction_house_bot.html", null ],
    [ "AuctionHouseBotStatusInfoPerType", "struct_auction_house_bot_status_info_per_type.html", null ],
    [ "AuctionHouseEntry", "struct_auction_house_entry.html", null ],
    [ "AuctionHouseMgr", "class_auction_house_mgr.html", null ],
    [ "AuctionHouseObject", "class_auction_house_object.html", null ],
    [ "Aura", "class_aura.html", [
      [ "AreaAura", "class_area_aura.html", null ],
      [ "PersistentAreaAura", "class_persistent_area_aura.html", null ],
      [ "SingleEnemyTargetAura", "class_single_enemy_target_aura.html", null ]
    ] ],
    [ "AuthCrypt", "class_auth_crypt.html", null ],
    [ "BankBagSlotPricesEntry", "struct_bank_bag_slot_prices_entry.html", null ],
    [ "BankBagSlotProcesEntry", "struct_bank_bag_slot_proces_entry.html", null ],
    [ "BarGoLink", "class_bar_go_link.html", null ],
    [ "BasicEvent", "class_basic_event.html", [
      [ "AiDelayEventAround", "class_ai_delay_event_around.html", null ],
      [ "BGQueueInviteEvent", "class_b_g_queue_invite_event.html", null ],
      [ "BGQueueRemoveEvent", "class_b_g_queue_remove_event.html", null ],
      [ "ForcedDespawnDelayEvent", "class_forced_despawn_delay_event.html", null ],
      [ "RelocationNotifyEvent", "class_relocation_notify_event.html", null ],
      [ "SpellEvent", "class_spell_event.html", null ]
    ] ],
    [ "BattleGround", "class_battle_ground.html", [
      [ "BattleGroundAB", "class_battle_ground_a_b.html", null ],
      [ "BattleGroundAV", "class_battle_ground_a_v.html", null ],
      [ "BattleGroundWS", "class_battle_ground_w_s.html", null ]
    ] ],
    [ "MaNGOS::BattleGround2ChatBuilder", "class_ma_n_g_o_s_1_1_battle_ground2_chat_builder.html", null ],
    [ "MaNGOS::BattleGround2YellBuilder", "class_ma_n_g_o_s_1_1_battle_ground2_yell_builder.html", null ],
    [ "MaNGOS::BattleGroundChatBuilder", "class_ma_n_g_o_s_1_1_battle_ground_chat_builder.html", null ],
    [ "BattleGroundEventIdx", "struct_battle_ground_event_idx.html", null ],
    [ "BattleGroundMgr", "class_battle_ground_mgr.html", null ],
    [ "BattleGroundObjectInfo", "struct_battle_ground_object_info.html", null ],
    [ "BattleGroundPlayer", "struct_battle_ground_player.html", null ],
    [ "BattleGroundQueue", "class_battle_ground_queue.html", null ],
    [ "BattleGroundScore", "class_battle_ground_score.html", [
      [ "BattleGroundABScore", "class_battle_ground_a_b_score.html", null ],
      [ "BattleGroundAVScore", "class_battle_ground_a_v_score.html", null ],
      [ "BattleGroundWGScore", "class_battle_ground_w_g_score.html", null ]
    ] ],
    [ "MaNGOS::BattleGroundYellBuilder", "class_ma_n_g_o_s_1_1_battle_ground_yell_builder.html", null ],
    [ "BG_AB_BannerTimer", "struct_b_g___a_b___banner_timer.html", null ],
    [ "BG_AV_NodeInfo", "struct_b_g___a_v___node_info.html", null ],
    [ "Player::BgBattleGroundQueueID_Rec", "struct_player_1_1_bg_battle_ground_queue_i_d___rec.html", null ],
    [ "BGData", "struct_b_g_data.html", null ],
    [ "BigNumber", "class_big_number.html", null ],
    [ "BIH", "class_b_i_h.html", null ],
    [ "BIHWrap< T, BoundsFunc >", "class_b_i_h_wrap.html", null ],
    [ "binary_function", null, [
      [ "ChainHealingOrder", "struct_chain_healing_order.html", null ],
      [ "TargetDistanceOrderNear", "struct_target_distance_order_near.html", null ]
    ] ],
    [ "BoundsTrait< GameObjectModel >", "struct_bounds_trait_3_01_game_object_model_01_4.html", null ],
    [ "BoundsTrait< VMAP::GroupModel >", "struct_bounds_trait_3_01_v_m_a_p_1_1_group_model_01_4.html", null ],
    [ "BoundsTrait< VMAP::ModelSpawn * >", "struct_bounds_trait_3_01_v_m_a_p_1_1_model_spawn_01_5_01_4.html", null ],
    [ "BIH::buildData", "struct_b_i_h_1_1build_data.html", null ],
    [ "BIH::BuildStats", "class_b_i_h_1_1_build_stats.html", null ],
    [ "BuyerAuctionEval", "struct_buyer_auction_eval.html", null ],
    [ "BuyerItemInfo", "struct_buyer_item_info.html", null ],
    [ "ByteBuffer", "class_byte_buffer.html", [
      [ "WorldPacket", "class_world_packet.html", null ]
    ] ],
    [ "ByteBufferException", "class_byte_buffer_exception.html", null ],
    [ "CalcDamageInfo", "struct_calc_damage_info.html", null ],
    [ "MaNGOS::CallOfHelpCreatureInRangeDo", "class_ma_n_g_o_s_1_1_call_of_help_creature_in_range_do.html", null ],
    [ "Camera", "class_camera.html", null ],
    [ "MaNGOS::CameraDistWorker< Do >", "struct_ma_n_g_o_s_1_1_camera_dist_worker.html", null ],
    [ "MaNGOS::CannibalizeObjectCheck", "class_ma_n_g_o_s_1_1_cannibalize_object_check.html", null ],
    [ "CapturePointSlider", "struct_capture_point_slider.html", null ],
    [ "ahbot::Category", "classahbot_1_1_category.html", [
      [ "ahbot::Consumable", "classahbot_1_1_consumable.html", [
        [ "ahbot::Alchemy", "classahbot_1_1_alchemy.html", null ],
        [ "ahbot::Bandage", "classahbot_1_1_bandage.html", null ],
        [ "ahbot::Food", "classahbot_1_1_food.html", null ],
        [ "ahbot::OtherConsumable", "classahbot_1_1_other_consumable.html", null ],
        [ "ahbot::Scroll", "classahbot_1_1_scroll.html", null ]
      ] ],
      [ "ahbot::Container", "classahbot_1_1_container.html", null ],
      [ "ahbot::Equip", "classahbot_1_1_equip.html", null ],
      [ "ahbot::Other", "classahbot_1_1_other.html", null ],
      [ "ahbot::Projectile", "classahbot_1_1_projectile.html", null ],
      [ "ahbot::QualityCategoryWrapper", "classahbot_1_1_quality_category_wrapper.html", null ],
      [ "ahbot::Quest", "classahbot_1_1_quest.html", null ],
      [ "ahbot::Quiver", "classahbot_1_1_quiver.html", null ],
      [ "ahbot::Reagent", "classahbot_1_1_reagent.html", null ],
      [ "ahbot::Recipe", "classahbot_1_1_recipe.html", null ],
      [ "ahbot::Trade", "classahbot_1_1_trade.html", [
        [ "ahbot::Engineering", "classahbot_1_1_engineering.html", null ],
        [ "ahbot::OtherTrade", "classahbot_1_1_other_trade.html", null ]
      ] ]
    ] ],
    [ "ahbot::CategoryList", "classahbot_1_1_category_list.html", null ],
    [ "RegularGrid2D< T, Node, NodeCreatorFunc, PositionFunc >::Cell", "struct_regular_grid2_d_1_1_cell.html", null ],
    [ "Cell", "struct_cell.html", null ],
    [ "CellArea", "struct_cell_area.html", null ],
    [ "CellObjectGuids", "struct_cell_object_guids.html", null ],
    [ "Channel", "class_channel.html", null ],
    [ "ChannelMgr", "class_channel_mgr.html", [
      [ "AllianceChannelMgr", "class_alliance_channel_mgr.html", null ],
      [ "HordeChannelMgr", "class_horde_channel_mgr.html", null ]
    ] ],
    [ "CharacterHandler", "class_character_handler.html", null ],
    [ "CharmInfo", "struct_charm_info.html", null ],
    [ "CharStartOutfitEntry", "struct_char_start_outfit_entry.html", null ],
    [ "ChatChannelsEntry", "struct_chat_channels_entry.html", null ],
    [ "ChatCommand", "class_chat_command.html", null ],
    [ "ChatCommandHolder", "class_chat_command_holder.html", null ],
    [ "ChatHandler", "class_chat_handler.html", [
      [ "CliHandler", "class_cli_handler.html", null ],
      [ "PlayerbotChatHandler", "class_playerbot_chat_handler.html", null ]
    ] ],
    [ "ChrClassesEntry", "struct_chr_classes_entry.html", null ],
    [ "ChrRacesEntry", "struct_chr_races_entry.html", null ],
    [ "CinematicSequencesEntry", "struct_cinematic_sequences_entry.html", null ],
    [ "ClassFamilyMask", "struct_class_family_mask.html", null ],
    [ "MaNGOS::ClassLevelLockable< T, MUTEX >", "class_ma_n_g_o_s_1_1_class_level_lockable.html", null ],
    [ "CleanDamage", "struct_clean_damage.html", null ],
    [ "CliCommandHolder", "struct_cli_command_holder.html", null ],
    [ "ClientPktHeader", "struct_client_pkt_header.html", null ],
    [ "ClientWardenModule", "struct_client_warden_module.html", null ],
    [ "CombatStopWithPetsHelper", "struct_combat_stop_with_pets_helper.html", null ],
    [ "Movement::CommonInitializer", "struct_movement_1_1_common_initializer.html", null ],
    [ "ConditionEntry", "struct_condition_entry.html", null ],
    [ "Config", "class_config.html", null ],
    [ "ContainerMapList< OBJECT >", "struct_container_map_list.html", null ],
    [ "ContainerMapList< GRID_OBJECT_TYPES >", "struct_container_map_list.html", null ],
    [ "ContainerMapList< H >", "struct_container_map_list.html", null ],
    [ "ContainerMapList< OBJECT_TYPES >", "struct_container_map_list.html", null ],
    [ "ContainerMapList< T >", "struct_container_map_list.html", null ],
    [ "ContainerMapList< TypeList< H, T > >", "struct_container_map_list_3_01_type_list_3_01_h_00_01_t_01_4_01_4.html", null ],
    [ "ContainerMapList< TypeNull >", "struct_container_map_list_3_01_type_null_01_4.html", null ],
    [ "ContainerMapList< WORLD_OBJECT_TYPES >", "struct_container_map_list.html", null ],
    [ "ContainerUnorderedMap< OBJECT, KEY_TYPE >", "struct_container_unordered_map.html", null ],
    [ "ContainerUnorderedMap< AllMapStoredObjectTypes, ObjectGuid >", "struct_container_unordered_map.html", null ],
    [ "ContainerUnorderedMap< H, KEY_TYPE >", "struct_container_unordered_map.html", null ],
    [ "ContainerUnorderedMap< OBJECT_TYPES, KEY_TYPE >", "struct_container_unordered_map.html", null ],
    [ "ContainerUnorderedMap< T, KEY_TYPE >", "struct_container_unordered_map.html", null ],
    [ "ContainerUnorderedMap< TypeList< H, T >, KEY_TYPE >", "struct_container_unordered_map_3_01_type_list_3_01_h_00_01_t_01_4_00_01_k_e_y___t_y_p_e_01_4.html", null ],
    [ "ContainerUnorderedMap< TypeNull, KEY_TYPE >", "struct_container_unordered_map_3_01_type_null_00_01_k_e_y___t_y_p_e_01_4.html", null ],
    [ "CoordPair< LIMIT >", "struct_coord_pair.html", null ],
    [ "CoordPair< MAX_NUMBER_OF_GRIDS >", "struct_coord_pair.html", null ],
    [ "CoordPair< TOTAL_NUMBER_OF_CELLS_PER_MAP >", "struct_coord_pair.html", null ],
    [ "Movement::counter< T, limit >", "class_movement_1_1counter.html", null ],
    [ "MaNGOS::CreateOnCallBack< T, CALL_BACK >", "class_ma_n_g_o_s_1_1_create_on_call_back.html", null ],
    [ "MaNGOS::CreateUsingMalloc< T >", "class_ma_n_g_o_s_1_1_create_using_malloc.html", null ],
    [ "CreatureAI", "class_creature_a_i.html", [
      [ "AggressorAI", "class_aggressor_a_i.html", null ],
      [ "CreatureEventAI", "class_creature_event_a_i.html", null ],
      [ "GuardAI", "class_guard_a_i.html", null ],
      [ "NullCreatureAI", "class_null_creature_a_i.html", null ],
      [ "PetAI", "class_pet_a_i.html", null ],
      [ "ReactorAI", "class_reactor_a_i.html", null ],
      [ "TotemAI", "class_totem_a_i.html", null ]
    ] ],
    [ "CreatureClassLvlStats", "struct_creature_class_lvl_stats.html", null ],
    [ "CreatureCreatePos", "struct_creature_create_pos.html", null ],
    [ "CreatureData", "struct_creature_data.html", null ],
    [ "CreatureDataAddon", "struct_creature_data_addon.html", null ],
    [ "CreatureDisplayInfoEntry", "struct_creature_display_info_entry.html", null ],
    [ "CreatureDisplayInfoExtraEntry", "struct_creature_display_info_extra_entry.html", null ],
    [ "CreatureEventAI_Action", "struct_creature_event_a_i___action.html", null ],
    [ "CreatureEventAI_Event", "struct_creature_event_a_i___event.html", null ],
    [ "CreatureEventAI_Summon", "struct_creature_event_a_i___summon.html", null ],
    [ "CreatureEventAIHolder", "struct_creature_event_a_i_holder.html", null ],
    [ "CreatureEventAIMgr", "class_creature_event_a_i_mgr.html", null ],
    [ "CreatureFamilyEntry", "struct_creature_family_entry.html", null ],
    [ "CreatureInfo", "struct_creature_info.html", null ],
    [ "MaNGOS::CreatureLastSearcher< Check >", "struct_ma_n_g_o_s_1_1_creature_last_searcher.html", null ],
    [ "CreatureLinkingHolder", "class_creature_linking_holder.html", null ],
    [ "CreatureLinkingInfo", "struct_creature_linking_info.html", null ],
    [ "CreatureLinkingMgr", "class_creature_linking_mgr.html", null ],
    [ "MaNGOS::CreatureListSearcher< Check >", "struct_ma_n_g_o_s_1_1_creature_list_searcher.html", null ],
    [ "CreatureLocale", "struct_creature_locale.html", null ],
    [ "CreatureModelInfo", "struct_creature_model_info.html", null ],
    [ "MaNGOS::CreatureRelocationNotifier", "struct_ma_n_g_o_s_1_1_creature_relocation_notifier.html", null ],
    [ "CreatureRespawnDeleteWorker", "struct_creature_respawn_delete_worker.html", null ],
    [ "MaNGOS::CreatureSearcher< Check >", "struct_ma_n_g_o_s_1_1_creature_searcher.html", null ],
    [ "CreatureSpellDataEntry", "struct_creature_spell_data_entry.html", null ],
    [ "CreatureTemplateSpells", "struct_creature_template_spells.html", null ],
    [ "CreatureTypeEntry", "struct_creature_type_entry.html", null ],
    [ "MaNGOS::CreatureWorker< Do >", "struct_ma_n_g_o_s_1_1_creature_worker.html", null ],
    [ "Database", "class_database.html", [
      [ "DatabaseMysql", "class_database_mysql.html", null ],
      [ "DatabasePostgre", "class_database_postgre.html", null ]
    ] ],
    [ "DBCFileLoader", "class_d_b_c_file_loader.html", null ],
    [ "DBCStorage< T >", "class_d_b_c_storage.html", null ],
    [ "DBVersion", "struct_d_b_version.html", null ],
    [ "ChatHandler::DeletedInfo", "struct_chat_handler_1_1_deleted_info.html", null ],
    [ "DespawnPoolInMapsWorker", "struct_despawn_pool_in_maps_worker.html", null ],
    [ "DiminishingReturn", "struct_diminishing_return.html", null ],
    [ "DoPlayerLearnSpell", "struct_do_player_learn_spell.html", null ],
    [ "DoSpellBonuses", "struct_do_spell_bonuses.html", null ],
    [ "DoSpellProcEvent", "struct_do_spell_proc_event.html", null ],
    [ "DoSpellProcItemEnchant", "struct_do_spell_proc_item_enchant.html", null ],
    [ "DoSpellThreat", "struct_do_spell_threat.html", null ],
    [ "DuelInfo", "struct_duel_info.html", null ],
    [ "DumpTable", "struct_dump_table.html", null ],
    [ "DungeonResetEvent", "struct_dungeon_reset_event.html", null ],
    [ "DungeonResetScheduler", "class_dungeon_reset_scheduler.html", null ],
    [ "DurabilityCostsEntry", "struct_durability_costs_entry.html", null ],
    [ "DurabilityQualityEntry", "struct_durability_quality_entry.html", null ],
    [ "DynamicMapTree", "class_dynamic_map_tree.html", null ],
    [ "MaNGOS::DynamicObjectUpdater", "struct_ma_n_g_o_s_1_1_dynamic_object_updater.html", null ],
    [ "DynamicTreeIntersectionCallback", "struct_dynamic_tree_intersection_callback.html", null ],
    [ "DynamicTreeIntersectionCallback_WithLogger", "struct_dynamic_tree_intersection_callback___with_logger.html", null ],
    [ "MaNGOS::EmoteChatBuilder", "class_ma_n_g_o_s_1_1_emote_chat_builder.html", null ],
    [ "EmotesEntry", "struct_emotes_entry.html", null ],
    [ "EmotesTextEntry", "struct_emotes_text_entry.html", null ],
    [ "EnchantDuration", "struct_enchant_duration.html", null ],
    [ "EnchStoreItem", "struct_ench_store_item.html", null ],
    [ "EquipmentInfo", "struct_equipment_info.html", null ],
    [ "EquipmentInfoItem", "struct_equipment_info_item.html", null ],
    [ "EquipmentInfoRaw", "struct_equipment_info_raw.html", null ],
    [ "ai::Event", "classai_1_1_event.html", null ],
    [ "BattleGround::EventObjects", "struct_battle_ground_1_1_event_objects.html", null ],
    [ "EventProcessor", "class_event_processor.html", null ],
    [ "ai::ExternalEventHelper", "classai_1_1_external_event_helper.html", null ],
    [ "Movement::FacingInfo", "union_movement_1_1_facing_info.html", null ],
    [ "FactionEntry", "struct_faction_entry.html", null ],
    [ "FactionState", "struct_faction_state.html", null ],
    [ "FactionTemplateEntry", "struct_faction_template_entry.html", null ],
    [ "FactoryHolder< T, Key >", "class_factory_holder.html", null ],
    [ "FactoryHolder< CreatureAI >", "class_factory_holder.html", [
      [ "SelectableAI", "struct_selectable_a_i.html", [
        [ "CreatureAIFactory< REAL_AI >", "struct_creature_a_i_factory.html", null ]
      ] ]
    ] ],
    [ "FactoryHolder< MovementGenerator, MovementGeneratorType >", "class_factory_holder.html", [
      [ "SelectableMovement", "struct_selectable_movement.html", [
        [ "MovementGeneratorFactory< REAL_MOVEMENT >", "struct_movement_generator_factory.html", null ]
      ] ]
    ] ],
    [ "Movement::FallInitializer", "struct_movement_1_1_fall_initializer.html", null ],
    [ "Field", "class_field.html", null ],
    [ "FindCreatureData", "class_find_creature_data.html", null ],
    [ "FindGOData", "class_find_g_o_data.html", null ],
    [ "ai::FindPlayerPredicate", "classai_1_1_find_player_predicate.html", [
      [ "FindDeadPlayer", "class_find_dead_player.html", null ],
      [ "PartyMemberToDispelPredicate", "class_party_member_to_dispel_predicate.html", null ],
      [ "PlayerWithoutAuraPredicate", "class_player_without_aura_predicate.html", null ]
    ] ],
    [ "ai::FindTargetStrategy", "classai_1_1_find_target_strategy.html", [
      [ "FindCurrentCcTargetStrategy", "class_find_current_cc_target_strategy.html", null ],
      [ "FindEnemyPlayerStrategy", "class_find_enemy_player_strategy.html", null ],
      [ "FindLeastHpTargetStrategy", "class_find_least_hp_target_strategy.html", null ],
      [ "FindTargetForCcStrategy", "class_find_target_for_cc_strategy.html", null ],
      [ "FindTargetForDpsStrategy", "class_find_target_for_dps_strategy.html", null ],
      [ "FindTargetForTankStrategy", "class_find_target_for_tank_strategy.html", null ]
    ] ],
    [ "ai::FleeManager", "classai_1_1_flee_manager.html", null ],
    [ "ai::FleePoint", "classai_1_1_flee_point.html", null ],
    [ "friend_", "structfriend__.html", null ],
    [ "FriendInfo", "struct_friend_info.html", null ],
    [ "MaNGOS::FriendlyCCedInRangeCheck", "class_ma_n_g_o_s_1_1_friendly_c_ced_in_range_check.html", null ],
    [ "MaNGOS::FriendlyMissingBuffInRangeCheck", "class_ma_n_g_o_s_1_1_friendly_missing_buff_in_range_check.html", null ],
    [ "GameEventCreatureData", "struct_game_event_creature_data.html", null ],
    [ "GameEventData", "struct_game_event_data.html", null ],
    [ "GameEventMail", "struct_game_event_mail.html", null ],
    [ "GameEventMgr", "class_game_event_mgr.html", null ],
    [ "GameEventUpdateCreatureDataInMapsWorker", "struct_game_event_update_creature_data_in_maps_worker.html", null ],
    [ "MaNGOS::GameObjectByGuidInRangeCheck", "class_ma_n_g_o_s_1_1_game_object_by_guid_in_range_check.html", null ],
    [ "GameObjectData", "struct_game_object_data.html", null ],
    [ "GameObjectDisplayInfoEntry", "struct_game_object_display_info_entry.html", null ],
    [ "MaNGOS::GameObjectEntryInPosRangeCheck", "class_ma_n_g_o_s_1_1_game_object_entry_in_pos_range_check.html", null ],
    [ "MaNGOS::GameObjectFocusCheck", "class_ma_n_g_o_s_1_1_game_object_focus_check.html", null ],
    [ "GameObjectInfo", "struct_game_object_info.html", null ],
    [ "MaNGOS::GameObjectLastSearcher< Check >", "struct_ma_n_g_o_s_1_1_game_object_last_searcher.html", null ],
    [ "MaNGOS::GameObjectListSearcher< Check >", "struct_ma_n_g_o_s_1_1_game_object_list_searcher.html", null ],
    [ "GameObjectLocale", "struct_game_object_locale.html", null ],
    [ "GameObjectModel", "class_game_object_model.html", null ],
    [ "GameobjectModelData", "struct_gameobject_model_data.html", null ],
    [ "GameObjectRespawnDeleteWorker", "struct_game_object_respawn_delete_worker.html", null ],
    [ "MaNGOS::GameObjectSearcher< Check >", "struct_ma_n_g_o_s_1_1_game_object_searcher.html", null ],
    [ "GameTele", "struct_game_tele.html", null ],
    [ "MaNGOS::GeneralLock< MUTEX >", "class_ma_n_g_o_s_1_1_general_lock.html", null ],
    [ "GlobalCooldown", "struct_global_cooldown.html", null ],
    [ "GlobalCooldownMgr", "class_global_cooldown_mgr.html", null ],
    [ "VMAP::GModelRayCallback", "struct_v_m_a_p_1_1_g_model_ray_callback.html", null ],
    [ "GMTicket", "class_g_m_ticket.html", null ],
    [ "GMTicketMgr", "class_g_m_ticket_mgr.html", null ],
    [ "GossipMenu", "class_gossip_menu.html", null ],
    [ "GossipMenuItem", "struct_gossip_menu_item.html", null ],
    [ "GossipMenuItemData", "struct_gossip_menu_item_data.html", null ],
    [ "GossipMenuItems", "struct_gossip_menu_items.html", null ],
    [ "GossipMenuItemsLocale", "struct_gossip_menu_items_locale.html", null ],
    [ "GossipMenus", "struct_gossip_menus.html", null ],
    [ "GossipText", "struct_gossip_text.html", null ],
    [ "GossipTextOption", "struct_gossip_text_option.html", null ],
    [ "Spell::GOTargetInfo", "struct_spell_1_1_g_o_target_info.html", null ],
    [ "GraveYardData", "struct_grave_yard_data.html", null ],
    [ "Grid< ACTIVE_OBJECT, WORLD_OBJECT_TYPES, GRID_OBJECT_TYPES >", "class_grid.html", null ],
    [ "GridInfo", "class_grid_info.html", null ],
    [ "GridLoader< ACTIVE_OBJECT, WORLD_OBJECT_TYPES, GRID_OBJECT_TYPES >", "class_grid_loader.html", null ],
    [ "GridMap", "class_grid_map.html", null ],
    [ "GridMapAreaHeader", "struct_grid_map_area_header.html", null ],
    [ "GridMapFileHeader", "struct_grid_map_file_header.html", null ],
    [ "GridMapHeightHeader", "struct_grid_map_height_header.html", null ],
    [ "GridMapLiquidData", "struct_grid_map_liquid_data.html", null ],
    [ "GridMapLiquidHeader", "struct_grid_map_liquid_header.html", null ],
    [ "GridState", "class_grid_state.html", [
      [ "ActiveState", "class_active_state.html", null ],
      [ "IdleState", "class_idle_state.html", null ],
      [ "InvalidState", "class_invalid_state.html", null ],
      [ "RemovalState", "class_removal_state.html", null ]
    ] ],
    [ "Group", "class_group.html", null ],
    [ "VMAP::GroupModel", "class_v_m_a_p_1_1_group_model.html", null ],
    [ "VMAP::GroupModel_Raw", "struct_v_m_a_p_1_1_group_model___raw.html", null ],
    [ "GroupQueueInfo", "struct_group_queue_info.html", null ],
    [ "Guild", "class_guild.html", null ],
    [ "GuildEventLogEntry", "struct_guild_event_log_entry.html", null ],
    [ "GuildMgr", "class_guild_mgr.html", null ],
    [ "hash< ObjectGuid >", "classhash_3_01_object_guid_01_4.html", null ],
    [ "HashMapHolder< T >", "class_hash_map_holder.html", null ],
    [ "HashTrait< GameObjectModel >", "struct_hash_trait_3_01_game_object_model_01_4.html", null ],
    [ "HMACSHA1", "class_h_m_a_c_s_h_a1.html", null ],
    [ "HomeMovementGenerator< T >", "class_home_movement_generator.html", null ],
    [ "HonorCP", "struct_honor_c_p.html", null ],
    [ "HonorRankInfo", "struct_honor_rank_info.html", null ],
    [ "HonorScores", "struct_honor_scores.html", null ],
    [ "HonorStanding", "class_honor_standing.html", null ],
    [ "MaNGOS::ICallback", "class_ma_n_g_o_s_1_1_i_callback.html", [
      [ "MaNGOS::_ICallback< _Callback< Class > >", "class_ma_n_g_o_s_1_1___i_callback.html", null ],
      [ "MaNGOS::_ICallback< _Callback< Class, ParamType1 > >", "class_ma_n_g_o_s_1_1___i_callback.html", null ],
      [ "MaNGOS::_ICallback< _Callback< Class, ParamType1, ParamType2 > >", "class_ma_n_g_o_s_1_1___i_callback.html", null ],
      [ "MaNGOS::_ICallback< _Callback< Class, ParamType1, ParamType2, ParamType3 > >", "class_ma_n_g_o_s_1_1___i_callback.html", null ],
      [ "MaNGOS::_ICallback< _Callback< Class, ParamType1, ParamType2, ParamType3, ParamType4 > >", "class_ma_n_g_o_s_1_1___i_callback.html", null ],
      [ "MaNGOS::_ICallback< CB >", "class_ma_n_g_o_s_1_1___i_callback.html", null ]
    ] ],
    [ "IdGenerator< T >", "class_id_generator.html", null ],
    [ "IdGenerator< uint32 >", "class_id_generator.html", null ],
    [ "MaNGOS::InAttackDistanceFromAnyHostileCreatureCheck", "class_ma_n_g_o_s_1_1_in_attack_distance_from_any_hostile_creature_check.html", null ],
    [ "InstanceData", "class_instance_data.html", null ],
    [ "InstanceGroupBind", "struct_instance_group_bind.html", null ],
    [ "InstancePlayerBind", "struct_instance_player_bind.html", null ],
    [ "InstanceTemplate", "struct_instance_template.html", null ],
    [ "IntervalTimer", "class_interval_timer.html", null ],
    [ "MaNGOS::IQueryCallback", "class_ma_n_g_o_s_1_1_i_query_callback.html", [
      [ "MaNGOS::_IQueryCallback< _Callback< Class, QueryResult *, ParamType1 > >", "class_ma_n_g_o_s_1_1___i_query_callback.html", null ],
      [ "MaNGOS::_IQueryCallback< _Callback< Class, QueryResult *, ParamType1, ParamType2 > >", "class_ma_n_g_o_s_1_1___i_query_callback.html", null ],
      [ "MaNGOS::_IQueryCallback< _Callback< Class, QueryResult *, ParamType1, ParamType2, ParamType3 > >", "class_ma_n_g_o_s_1_1___i_query_callback.html", null ],
      [ "MaNGOS::_IQueryCallback< _Callback< Class, QueryResult *> >", "class_ma_n_g_o_s_1_1___i_query_callback.html", null ],
      [ "MaNGOS::_IQueryCallback< _SCallback< QueryResult *, ParamType1 > >", "class_ma_n_g_o_s_1_1___i_query_callback.html", null ],
      [ "MaNGOS::_IQueryCallback< _SCallback< QueryResult *, ParamType1, ParamType2 > >", "class_ma_n_g_o_s_1_1___i_query_callback.html", null ],
      [ "MaNGOS::_IQueryCallback< _SCallback< QueryResult *, ParamType1, ParamType2, ParamType3 > >", "class_ma_n_g_o_s_1_1___i_query_callback.html", null ],
      [ "MaNGOS::_IQueryCallback< _SCallback< QueryResult *> >", "class_ma_n_g_o_s_1_1___i_query_callback.html", null ],
      [ "MaNGOS::_IQueryCallback< CB >", "class_ma_n_g_o_s_1_1___i_query_callback.html", null ]
    ] ],
    [ "IsAttackingPlayerHelper", "struct_is_attacking_player_helper.html", null ],
    [ "ahbot::ItemBag", "classahbot_1_1_item_bag.html", [
      [ "ahbot::AvailableItemsBag", "classahbot_1_1_available_items_bag.html", null ],
      [ "ahbot::InAuctionItemsBag", "classahbot_1_1_in_auction_items_bag.html", null ]
    ] ],
    [ "ItemBagFamilyEntry", "struct_item_bag_family_entry.html", null ],
    [ "ItemClassEntry", "struct_item_class_entry.html", null ],
    [ "ItemLocale", "struct_item_locale.html", null ],
    [ "ItemPosCount", "struct_item_pos_count.html", null ],
    [ "ItemPrototype", "struct_item_prototype.html", null ],
    [ "ItemRandomPropertiesEntry", "struct_item_random_properties_entry.html", null ],
    [ "ItemRequiredTarget", "struct_item_required_target.html", null ],
    [ "ItemSetEffect", "struct_item_set_effect.html", null ],
    [ "ItemSetEntry", "struct_item_set_entry.html", null ],
    [ "Spell::ItemTargetInfo", "struct_spell_1_1_item_target_info.html", null ],
    [ "ai::IterateItemsVisitor", "classai_1_1_iterate_items_visitor.html", [
      [ "ai::FindItemsByQualityVisitor", "classai_1_1_find_items_by_quality_visitor.html", [
        [ "ai::FindItemsToTradeByQualityVisitor", "classai_1_1_find_items_to_trade_by_quality_visitor.html", null ]
      ] ],
      [ "ai::FindItemsToTradeByClassVisitor", "classai_1_1_find_items_to_trade_by_class_visitor.html", null ],
      [ "ai::FindItemVisitor", "classai_1_1_find_item_visitor.html", [
        [ "ai::FindItemByIdVisitor", "classai_1_1_find_item_by_id_visitor.html", null ],
        [ "ai::FindUsableItemVisitor", "classai_1_1_find_usable_item_visitor.html", [
          [ "ai::FindUsableNamedItemVisitor", "classai_1_1_find_usable_named_item_visitor.html", null ],
          [ "FindFoodVisitor", "class_find_food_visitor.html", null ],
          [ "FindPotionVisitor", "class_find_potion_visitor.html", null ]
        ] ]
      ] ],
      [ "ai::ItemCountByQuality", "classai_1_1_item_count_by_quality.html", null ],
      [ "ai::ListItemsVisitor", "classai_1_1_list_items_visitor.html", null ],
      [ "ai::QueryItemCountVisitor", "classai_1_1_query_item_count_visitor.html", [
        [ "ai::QueryNamedItemCountVisitor", "classai_1_1_query_named_item_count_visitor.html", null ]
      ] ],
      [ "DestroyItemsVisitor", "class_destroy_items_visitor.html", null ],
      [ "FindBuffVisitor", "class_find_buff_visitor.html", null ],
      [ "FindTradeItemsVisitor", "class_find_trade_items_visitor.html", null ],
      [ "SellItemsVisitor", "class_sell_items_visitor.html", [
        [ "SellGrayItemsVisitor", "class_sell_gray_items_visitor.html", null ]
      ] ]
    ] ],
    [ "LinkedListHead::Iterator< _Ty >", "class_linked_list_head_1_1_iterator.html", null ],
    [ "LinkedListHead::Iterator< MapReference >", "class_linked_list_head_1_1_iterator.html", null ],
    [ "VMAP::IVMapManager", "class_v_m_a_p_1_1_i_v_map_manager.html", [
      [ "VMAP::VMapManager2", "class_v_m_a_p_1_1_v_map_manager2.html", null ]
    ] ],
    [ "MovementInfo::JumpInfo", "struct_movement_info_1_1_jump_info.html", null ],
    [ "keyData", "structkey_data.html", null ],
    [ "keyFrame", "structkey_frame.html", null ],
    [ "LanguageDesc", "struct_language_desc.html", null ],
    [ "ai::LastMovement", "classai_1_1_last_movement.html", null ],
    [ "ai::LastSpellCast", "classai_1_1_last_spell_cast.html", null ],
    [ "ai::LazyCalculatedValue< TValue, TOwner >", "classai_1_1_lazy_calculated_value.html", null ],
    [ "LFGGroupQueueInfo", "struct_l_f_g_group_queue_info.html", null ],
    [ "LFGPlayerQueueInfo", "struct_l_f_g_player_queue_info.html", null ],
    [ "LFGQueue", "class_l_f_g_queue.html", null ],
    [ "LinkedListElement", "class_linked_list_element.html", [
      [ "Reference< TO, FROM >", "class_reference.html", [
        [ "GridReference< Camera >", "class_grid_reference.html", null ],
        [ "GridReference< Corpse >", "class_grid_reference.html", null ],
        [ "GridReference< Creature >", "class_grid_reference.html", null ],
        [ "GridReference< DynamicObject >", "class_grid_reference.html", null ],
        [ "GridReference< GameObject >", "class_grid_reference.html", null ],
        [ "GridReference< NGrid< N, ACTIVE_OBJECT, WORLD_OBJECT_TYPES, GRID_OBJECT_TYPES > >", "class_grid_reference.html", null ],
        [ "GridReference< Player >", "class_grid_reference.html", null ]
      ] ],
      [ "Reference< GridRefManager< OBJECT >, OBJECT >", "class_reference.html", [
        [ "GridReference< OBJECT >", "class_grid_reference.html", null ]
      ] ],
      [ "Reference< Group, Player >", "class_reference.html", [
        [ "GroupReference", "class_group_reference.html", null ]
      ] ],
      [ "Reference< Loot, LootValidatorRef >", "class_reference.html", [
        [ "LootValidatorRef", "class_loot_validator_ref.html", [
          [ "Roll", "class_roll.html", null ]
        ] ]
      ] ],
      [ "Reference< Map, Player >", "class_reference.html", [
        [ "MapReference", "class_map_reference.html", null ]
      ] ],
      [ "Reference< Unit, TargetedMovementGeneratorBase >", "class_reference.html", [
        [ "FollowerReference", "class_follower_reference.html", null ]
      ] ],
      [ "Reference< Unit, ThreatManager >", "class_reference.html", [
        [ "HostileReference", "class_hostile_reference.html", null ]
      ] ]
    ] ],
    [ "LinkedListHead", "class_linked_list_head.html", [
      [ "RefManager< TO, FROM >", "class_ref_manager.html", null ],
      [ "RefManager< GridRefManager< GRID_OBJECT_TYPES >, GRID_OBJECT_TYPES >", "class_ref_manager.html", [
        [ "GridRefManager< GRID_OBJECT_TYPES >", "class_grid_ref_manager.html", null ]
      ] ],
      [ "RefManager< GridRefManager< H >, H >", "class_ref_manager.html", [
        [ "GridRefManager< H >", "class_grid_ref_manager.html", null ]
      ] ],
      [ "RefManager< GridRefManager< NGridType >, NGridType >", "class_ref_manager.html", [
        [ "GridRefManager< NGridType >", "class_grid_ref_manager.html", [
          [ "Map", "class_map.html", [
            [ "BattleGroundMap", "class_battle_ground_map.html", null ],
            [ "DungeonMap", "class_dungeon_map.html", null ],
            [ "WorldMap", "class_world_map.html", null ]
          ] ]
        ] ]
      ] ],
      [ "RefManager< GridRefManager< OBJECT >, OBJECT >", "class_ref_manager.html", [
        [ "GridRefManager< OBJECT >", "class_grid_ref_manager.html", null ]
      ] ],
      [ "RefManager< GridRefManager< OBJECT_TYPES >, OBJECT_TYPES >", "class_ref_manager.html", [
        [ "GridRefManager< OBJECT_TYPES >", "class_grid_ref_manager.html", null ]
      ] ],
      [ "RefManager< GridRefManager< T >, T >", "class_ref_manager.html", [
        [ "GridRefManager< T >", "class_grid_ref_manager.html", null ]
      ] ],
      [ "RefManager< GridRefManager< WORLD_OBJECT_TYPES >, WORLD_OBJECT_TYPES >", "class_ref_manager.html", [
        [ "GridRefManager< WORLD_OBJECT_TYPES >", "class_grid_ref_manager.html", null ]
      ] ],
      [ "RefManager< Group, Player >", "class_ref_manager.html", [
        [ "GroupRefManager", "class_group_ref_manager.html", null ]
      ] ],
      [ "RefManager< Loot, LootValidatorRef >", "class_ref_manager.html", [
        [ "LootValidatorRefManager", "class_loot_validator_ref_manager.html", null ]
      ] ],
      [ "RefManager< Map, Player >", "class_ref_manager.html", [
        [ "MapRefManager", "class_map_ref_manager.html", null ]
      ] ],
      [ "RefManager< Unit, TargetedMovementGeneratorBase >", "class_ref_manager.html", [
        [ "FollowerRefManager", "class_follower_ref_manager.html", null ]
      ] ],
      [ "RefManager< Unit, ThreatManager >", "class_ref_manager.html", [
        [ "HostileRefManager", "class_hostile_ref_manager.html", null ]
      ] ]
    ] ],
    [ "LiquidTypeEntry", "struct_liquid_type_entry.html", null ],
    [ "LocaleNameStr", "struct_locale_name_str.html", null ],
    [ "MaNGOS::LocalizedPacketDo< Builder >", "class_ma_n_g_o_s_1_1_localized_packet_do.html", null ],
    [ "MaNGOS::LocalizedPacketListDo< Builder >", "class_ma_n_g_o_s_1_1_localized_packet_list_do.html", null ],
    [ "MaNGOS::LocalStaticCreation< T >", "class_ma_n_g_o_s_1_1_local_static_creation.html", null ],
    [ "VMAP::LocationInfo", "struct_v_m_a_p_1_1_location_info.html", null ],
    [ "VMAP::LocationInfoCallback", "class_v_m_a_p_1_1_location_info_callback.html", null ],
    [ "MaNGOS::SingleThreaded< T >::Lock", "struct_ma_n_g_o_s_1_1_single_threaded_1_1_lock.html", null ],
    [ "MaNGOS::ObjectLevelLockable< T, MUTEX >::Lock", "class_ma_n_g_o_s_1_1_object_level_lockable_1_1_lock.html", null ],
    [ "MaNGOS::ClassLevelLockable< T, MUTEX >::Lock", "class_ma_n_g_o_s_1_1_class_level_lockable_1_1_lock.html", null ],
    [ "SqlConnection::Lock", "class_sql_connection_1_1_lock.html", null ],
    [ "ACE_Based::LockedQueue< T, LockType, StorageType >", "class_a_c_e___based_1_1_locked_queue.html", null ],
    [ "ACE_Based::LockedQueue< CliCommandHolder *, ACE_Thread_Mutex >", "class_a_c_e___based_1_1_locked_queue.html", null ],
    [ "ACE_Based::LockedQueue< MaNGOS::IQueryCallback *, ACE_Thread_Mutex >", "class_a_c_e___based_1_1_locked_queue.html", [
      [ "SqlResultQueue", "class_sql_result_queue.html", null ]
    ] ],
    [ "ACE_Based::LockedQueue< SqlOperation *, ACE_Thread_Mutex >", "class_a_c_e___based_1_1_locked_queue.html", null ],
    [ "ACE_Based::LockedQueue< WorldPacket *, ACE_Thread_Mutex >", "class_a_c_e___based_1_1_locked_queue.html", null ],
    [ "ACE_Based::LockedQueue< WorldSession *, ACE_Thread_Mutex >", "class_a_c_e___based_1_1_locked_queue.html", null ],
    [ "LockEntry", "struct_lock_entry.html", null ],
    [ "LogFilterData", "struct_log_filter_data.html", null ],
    [ "Loot", "struct_loot.html", null ],
    [ "LootTemplate::LootGroup", "class_loot_template_1_1_loot_group.html", null ],
    [ "LootItem", "struct_loot_item.html", null ],
    [ "ai::LootObject", "classai_1_1_loot_object.html", null ],
    [ "ai::LootObjectStack", "classai_1_1_loot_object_stack.html", null ],
    [ "LootStore", "class_loot_store.html", null ],
    [ "LootStoreItem", "struct_loot_store_item.html", null ],
    [ "ai::LootTarget", "classai_1_1_loot_target.html", null ],
    [ "LootTemplate", "class_loot_template.html", null ],
    [ "LootView", "struct_loot_view.html", null ],
    [ "Mail", "struct_mail.html", null ],
    [ "MailDraft", "class_mail_draft.html", null ],
    [ "MailItemInfo", "struct_mail_item_info.html", null ],
    [ "MailReceiver", "class_mail_receiver.html", null ],
    [ "MailSender", "class_mail_sender.html", null ],
    [ "MailTemplateEntry", "struct_mail_template_entry.html", null ],
    [ "VMAP::ManagedModel", "class_v_m_a_p_1_1_managed_model.html", null ],
    [ "MangosStringLocale", "struct_mangos_string_locale.html", null ],
    [ "MapCellObjectGuids", "struct_map_cell_object_guids.html", null ],
    [ "MapEntry", "struct_map_entry.html", null ],
    [ "MapID", "struct_map_i_d.html", null ],
    [ "MapPersistantStateResetWorker", "struct_map_persistant_state_reset_worker.html", null ],
    [ "MapPersistantStateWarnWorker", "struct_map_persistant_state_warn_worker.html", null ],
    [ "MapPersistentState", "class_map_persistent_state.html", [
      [ "BattleGroundPersistentState", "class_battle_ground_persistent_state.html", null ],
      [ "DungeonPersistentState", "class_dungeon_persistent_state.html", null ],
      [ "WorldPersistentState", "class_world_persistent_state.html", null ]
    ] ],
    [ "VMAP::MapRayCallback", "class_v_m_a_p_1_1_map_ray_callback.html", null ],
    [ "VMAP::MapSpawns", "struct_v_m_a_p_1_1_map_spawns.html", null ],
    [ "MapUpdater", "class_map_updater.html", null ],
    [ "MassMailerQueryHandler", "struct_mass_mailer_query_handler.html", null ],
    [ "MassMailMgr", "class_mass_mail_mgr.html", null ],
    [ "md5_state_s", "structmd5__state__s.html", null ],
    [ "MemberSlot", "struct_member_slot.html", null ],
    [ "Group::MemberSlot", "struct_group_1_1_member_slot.html", null ],
    [ "VMAP::MeshTriangle", "class_v_m_a_p_1_1_mesh_triangle.html", null ],
    [ "MaNGOS::MessageDeliverer", "struct_ma_n_g_o_s_1_1_message_deliverer.html", null ],
    [ "MaNGOS::MessageDelivererExcept", "struct_ma_n_g_o_s_1_1_message_deliverer_except.html", null ],
    [ "MaNGOS::MessageDistDeliverer", "struct_ma_n_g_o_s_1_1_message_dist_deliverer.html", null ],
    [ "ai::MinValueCalculator", "classai_1_1_min_value_calculator.html", null ],
    [ "MMAP::MMapData", "struct_m_m_a_p_1_1_m_map_data.html", null ],
    [ "MMAP::MMapFactory", "class_m_m_a_p_1_1_m_map_factory.html", null ],
    [ "MMAP::MMapManager", "class_m_m_a_p_1_1_m_map_manager.html", null ],
    [ "MmapTileHeader", "struct_mmap_tile_header.html", null ],
    [ "VMAP::ModelPosition", "class_v_m_a_p_1_1_model_position.html", null ],
    [ "VMAP::ModelSpawn", "class_v_m_a_p_1_1_model_spawn.html", [
      [ "VMAP::ModelInstance", "class_v_m_a_p_1_1_model_instance.html", null ]
    ] ],
    [ "Modifier", "struct_modifier.html", null ],
    [ "Module_79C0768D657977D697E10BAD956CCED1", "struct_module__79_c0768_d657977_d697_e10_b_a_d956_c_c_e_d1.html", null ],
    [ "MaNGOS::MonsterChatBuilder", "class_ma_n_g_o_s_1_1_monster_chat_builder.html", null ],
    [ "MaNGOS::MostHPMissingInRangeCheck", "class_ma_n_g_o_s_1_1_most_h_p_missing_in_range_check.html", null ],
    [ "MovementGenerator", "class_movement_generator.html", [
      [ "DistractMovementGenerator", "class_distract_movement_generator.html", [
        [ "AssistanceDistractMovementGenerator", "class_assistance_distract_movement_generator.html", null ]
      ] ],
      [ "EffectMovementGenerator", "class_effect_movement_generator.html", null ],
      [ "IdleMovementGenerator", "class_idle_movement_generator.html", null ],
      [ "MovementGeneratorMedium< T, D >", "class_movement_generator_medium.html", [
        [ "TargetedMovementGeneratorMedium< T, D >", "class_targeted_movement_generator_medium.html", null ]
      ] ],
      [ "MovementGeneratorMedium< Creature, FleeingMovementGenerator< Creature > >", "class_movement_generator_medium.html", [
        [ "FleeingMovementGenerator< Creature >", "class_fleeing_movement_generator.html", [
          [ "TimedFleeingMovementGenerator", "class_timed_fleeing_movement_generator.html", null ]
        ] ]
      ] ],
      [ "MovementGeneratorMedium< Creature, HomeMovementGenerator< Creature > >", "class_movement_generator_medium.html", [
        [ "HomeMovementGenerator< Creature >", "class_home_movement_generator_3_01_creature_01_4.html", null ]
      ] ],
      [ "MovementGeneratorMedium< Creature, PointMovementGenerator< Creature > >", "class_movement_generator_medium.html", [
        [ "PointMovementGenerator< Creature >", "class_point_movement_generator.html", [
          [ "AssistanceMovementGenerator", "class_assistance_movement_generator.html", null ],
          [ "FlyOrLandMovementGenerator", "class_fly_or_land_movement_generator.html", null ]
        ] ]
      ] ],
      [ "MovementGeneratorMedium< Creature, WaypointMovementGenerator< Creature > >", "class_movement_generator_medium.html", [
        [ "WaypointMovementGenerator< Creature >", "class_waypoint_movement_generator_3_01_creature_01_4.html", null ]
      ] ],
      [ "MovementGeneratorMedium< Player, FlightPathMovementGenerator >", "class_movement_generator_medium.html", [
        [ "FlightPathMovementGenerator", "class_flight_path_movement_generator.html", null ]
      ] ],
      [ "MovementGeneratorMedium< T, ChaseMovementGenerator< T > >", "class_movement_generator_medium.html", [
        [ "TargetedMovementGeneratorMedium< T, ChaseMovementGenerator< T > >", "class_targeted_movement_generator_medium.html", [
          [ "ChaseMovementGenerator< T >", "class_chase_movement_generator.html", null ]
        ] ]
      ] ],
      [ "MovementGeneratorMedium< T, ConfusedMovementGenerator< T > >", "class_movement_generator_medium.html", [
        [ "ConfusedMovementGenerator< T >", "class_confused_movement_generator.html", null ]
      ] ],
      [ "MovementGeneratorMedium< T, FleeingMovementGenerator< T > >", "class_movement_generator_medium.html", [
        [ "FleeingMovementGenerator< T >", "class_fleeing_movement_generator.html", null ]
      ] ],
      [ "MovementGeneratorMedium< T, FollowMovementGenerator< T > >", "class_movement_generator_medium.html", [
        [ "TargetedMovementGeneratorMedium< T, FollowMovementGenerator< T > >", "class_targeted_movement_generator_medium.html", [
          [ "FollowMovementGenerator< T >", "class_follow_movement_generator.html", null ]
        ] ]
      ] ],
      [ "MovementGeneratorMedium< T, PointMovementGenerator< T > >", "class_movement_generator_medium.html", [
        [ "PointMovementGenerator< T >", "class_point_movement_generator.html", null ]
      ] ],
      [ "MovementGeneratorMedium< T, RandomMovementGenerator< T > >", "class_movement_generator_medium.html", [
        [ "RandomMovementGenerator< T >", "class_random_movement_generator.html", null ]
      ] ]
    ] ],
    [ "MovementInfo", "class_movement_info.html", null ],
    [ "Movement::MoveSpline", "class_movement_1_1_move_spline.html", null ],
    [ "Movement::MoveSplineFlag", "class_movement_1_1_move_spline_flag.html", null ],
    [ "Movement::MoveSplineInit", "class_movement_1_1_move_spline_init.html", null ],
    [ "Movement::MoveSplineInitArgs", "struct_movement_1_1_move_spline_init_args.html", null ],
    [ "ai::NamedObjectContextList< T >", "classai_1_1_named_object_context_list.html", null ],
    [ "ai::NamedObjectContextList< ai::Action >", "classai_1_1_named_object_context_list.html", null ],
    [ "ai::NamedObjectContextList< ai::Strategy >", "classai_1_1_named_object_context_list.html", null ],
    [ "ai::NamedObjectContextList< ai::Trigger >", "classai_1_1_named_object_context_list.html", null ],
    [ "ai::NamedObjectContextList< ai::UntypedValue >", "classai_1_1_named_object_context_list.html", null ],
    [ "ai::NamedObjectFactory< T >", "classai_1_1_named_object_factory.html", [
      [ "ai::NamedObjectContext< T >", "classai_1_1_named_object_context.html", null ]
    ] ],
    [ "ai::NamedObjectFactory< Action >", "classai_1_1_named_object_factory.html", [
      [ "ai::NamedObjectContext< Action >", "classai_1_1_named_object_context.html", [
        [ "ai::ActionContext", "classai_1_1_action_context.html", null ],
        [ "ai::ChatActionContext", "classai_1_1_chat_action_context.html", null ],
        [ "ai::druid::AiObjectContextInternal", "classai_1_1druid_1_1_ai_object_context_internal.html", null ],
        [ "ai::hunter::AiObjectContextInternal", "classai_1_1hunter_1_1_ai_object_context_internal.html", null ],
        [ "ai::mage::AiObjectContextInternal", "classai_1_1mage_1_1_ai_object_context_internal.html", null ],
        [ "ai::paladin::AiObjectContextInternal", "classai_1_1paladin_1_1_ai_object_context_internal.html", null ],
        [ "ai::priest::AiObjectContextInternal", "classai_1_1priest_1_1_ai_object_context_internal.html", null ],
        [ "ai::rogue::AiObjectContextInternal", "classai_1_1rogue_1_1_ai_object_context_internal.html", null ],
        [ "ai::shaman::AiObjectContextInternal", "classai_1_1shaman_1_1_ai_object_context_internal.html", null ],
        [ "ai::warlock::AiObjectContextInternal", "classai_1_1warlock_1_1_ai_object_context_internal.html", null ],
        [ "ai::warrior::AiObjectContextInternal", "classai_1_1warrior_1_1_ai_object_context_internal.html", null ],
        [ "ai::WorldPacketActionContext", "classai_1_1_world_packet_action_context.html", null ]
      ] ]
    ] ],
    [ "ai::NamedObjectFactory< ActionNode >", "classai_1_1_named_object_factory.html", [
      [ "ActionNodeFactoryInternal", "class_action_node_factory_internal.html", null ],
      [ "ai::GenericPaladinStrategyActionNodeFactory", "classai_1_1_generic_paladin_strategy_action_node_factory.html", null ],
      [ "ai::GenericPriestStrategyActionNodeFactory", "classai_1_1_generic_priest_strategy_action_node_factory.html", null ],
      [ "ai::HolyPriestStrategyActionNodeFactory", "classai_1_1_holy_priest_strategy_action_node_factory.html", null ],
      [ "ai::PriestNonCombatStrategyActionNodeFactory", "classai_1_1_priest_non_combat_strategy_action_node_factory.html", null ],
      [ "ai::ShadowPriestStrategyActionNodeFactory", "classai_1_1_shadow_priest_strategy_action_node_factory.html", null ],
      [ "ai::ShapeshiftDruidStrategyActionNodeFactory", "classai_1_1_shapeshift_druid_strategy_action_node_factory.html", null ],
      [ "ArcaneMageStrategyActionNodeFactory", "class_arcane_mage_strategy_action_node_factory.html", null ],
      [ "BearTankDruidStrategyActionNodeFactory", "class_bear_tank_druid_strategy_action_node_factory.html", null ],
      [ "CasterDruidStrategyActionNodeFactory", "class_caster_druid_strategy_action_node_factory.html", null ],
      [ "CasterShamanStrategyActionNodeFactory", "class_caster_shaman_strategy_action_node_factory.html", null ],
      [ "CatDpsDruidStrategyActionNodeFactory", "class_cat_dps_druid_strategy_action_node_factory.html", null ],
      [ "ChatCommandActionNodeFactoryInternal", "class_chat_command_action_node_factory_internal.html", null ],
      [ "DpsHunterStrategyActionNodeFactory", "class_dps_hunter_strategy_action_node_factory.html", null ],
      [ "DpsPaladinStrategyActionNodeFactory", "class_dps_paladin_strategy_action_node_factory.html", null ],
      [ "DpsRogueStrategyActionNodeFactory", "class_dps_rogue_strategy_action_node_factory.html", null ],
      [ "DpsWarlockStrategyActionNodeFactory", "class_dps_warlock_strategy_action_node_factory.html", null ],
      [ "DpsWarriorStrategyActionNodeFactory", "class_dps_warrior_strategy_action_node_factory.html", null ],
      [ "FeralDruidStrategyActionNodeFactory", "class_feral_druid_strategy_action_node_factory.html", null ],
      [ "GenericDruidNonCombatStrategyActionNodeFactory", "class_generic_druid_non_combat_strategy_action_node_factory.html", null ],
      [ "GenericDruidStrategyActionNodeFactory", "class_generic_druid_strategy_action_node_factory.html", null ],
      [ "GenericHunterNonCombatStrategyActionNodeFactory", "class_generic_hunter_non_combat_strategy_action_node_factory.html", null ],
      [ "GenericHunterStrategyActionNodeFactory", "class_generic_hunter_strategy_action_node_factory.html", null ],
      [ "GenericMageNonCombatStrategyActionNodeFactory", "class_generic_mage_non_combat_strategy_action_node_factory.html", null ],
      [ "GenericMageStrategyActionNodeFactory", "class_generic_mage_strategy_action_node_factory.html", null ],
      [ "GenericShamanStrategyActionNodeFactory", "class_generic_shaman_strategy_action_node_factory.html", null ],
      [ "GenericWarlockNonCombatStrategyActionNodeFactory", "class_generic_warlock_non_combat_strategy_action_node_factory.html", null ],
      [ "GenericWarlockStrategyActionNodeFactory", "class_generic_warlock_strategy_action_node_factory.html", null ],
      [ "GenericWarlockStrategyActionNodeFactory", "class_generic_warlock_strategy_action_node_factory.html", null ],
      [ "GenericWarriorStrategyActionNodeFactory", "class_generic_warrior_strategy_action_node_factory.html", null ],
      [ "HealDruidStrategyActionNodeFactory", "class_heal_druid_strategy_action_node_factory.html", null ],
      [ "HealShamanStrategyActionNodeFactory", "class_heal_shaman_strategy_action_node_factory.html", null ],
      [ "MeleeShamanStrategyActionNodeFactory", "class_melee_shaman_strategy_action_node_factory.html", null ],
      [ "RacialsStrategyActionNodeFactory", "class_racials_strategy_action_node_factory.html", null ],
      [ "TankPaladinStrategyActionNodeFactory", "class_tank_paladin_strategy_action_node_factory.html", null ],
      [ "TankWarriorStrategyActionNodeFactory", "class_tank_warrior_strategy_action_node_factory.html", null ]
    ] ],
    [ "ai::NamedObjectFactory< ai::Action >", "classai_1_1_named_object_factory.html", [
      [ "ai::NamedObjectContext< ai::Action >", "classai_1_1_named_object_context.html", null ]
    ] ],
    [ "ai::NamedObjectFactory< Strategy >", "classai_1_1_named_object_factory.html", [
      [ "ai::NamedObjectContext< Strategy >", "classai_1_1_named_object_context.html", [
        [ "ai::AssistStrategyContext", "classai_1_1_assist_strategy_context.html", null ],
        [ "ai::druid::DruidStrategyFactoryInternal", "classai_1_1druid_1_1_druid_strategy_factory_internal.html", null ],
        [ "ai::druid::StrategyFactoryInternal", "classai_1_1druid_1_1_strategy_factory_internal.html", null ],
        [ "ai::hunter::BuffStrategyFactoryInternal", "classai_1_1hunter_1_1_buff_strategy_factory_internal.html", null ],
        [ "ai::hunter::StrategyFactoryInternal", "classai_1_1hunter_1_1_strategy_factory_internal.html", null ],
        [ "ai::mage::MageBuffStrategyFactoryInternal", "classai_1_1mage_1_1_mage_buff_strategy_factory_internal.html", null ],
        [ "ai::mage::MageStrategyFactoryInternal", "classai_1_1mage_1_1_mage_strategy_factory_internal.html", null ],
        [ "ai::mage::StrategyFactoryInternal", "classai_1_1mage_1_1_strategy_factory_internal.html", null ],
        [ "ai::MovementStrategyContext", "classai_1_1_movement_strategy_context.html", null ],
        [ "ai::paladin::BuffStrategyFactoryInternal", "classai_1_1paladin_1_1_buff_strategy_factory_internal.html", null ],
        [ "ai::paladin::CombatStrategyFactoryInternal", "classai_1_1paladin_1_1_combat_strategy_factory_internal.html", null ],
        [ "ai::paladin::ResistanceStrategyFactoryInternal", "classai_1_1paladin_1_1_resistance_strategy_factory_internal.html", null ],
        [ "ai::paladin::StrategyFactoryInternal", "classai_1_1paladin_1_1_strategy_factory_internal.html", null ],
        [ "ai::priest::CombatStrategyFactoryInternal", "classai_1_1priest_1_1_combat_strategy_factory_internal.html", null ],
        [ "ai::priest::StrategyFactoryInternal", "classai_1_1priest_1_1_strategy_factory_internal.html", null ],
        [ "ai::QuestStrategyContext", "classai_1_1_quest_strategy_context.html", null ],
        [ "ai::rogue::StrategyFactoryInternal", "classai_1_1rogue_1_1_strategy_factory_internal.html", null ],
        [ "ai::shaman::BuffStrategyFactoryInternal", "classai_1_1shaman_1_1_buff_strategy_factory_internal.html", null ],
        [ "ai::shaman::CombatStrategyFactoryInternal", "classai_1_1shaman_1_1_combat_strategy_factory_internal.html", null ],
        [ "ai::shaman::StrategyFactoryInternal", "classai_1_1shaman_1_1_strategy_factory_internal.html", null ],
        [ "ai::StrategyContext", "classai_1_1_strategy_context.html", null ],
        [ "ai::warlock::CombatStrategyFactoryInternal", "classai_1_1warlock_1_1_combat_strategy_factory_internal.html", null ],
        [ "ai::warlock::StrategyFactoryInternal", "classai_1_1warlock_1_1_strategy_factory_internal.html", null ],
        [ "ai::warrior::CombatStrategyFactoryInternal", "classai_1_1warrior_1_1_combat_strategy_factory_internal.html", null ],
        [ "ai::warrior::StrategyFactoryInternal", "classai_1_1warrior_1_1_strategy_factory_internal.html", null ]
      ] ]
    ] ],
    [ "ai::NamedObjectFactory< Trigger >", "classai_1_1_named_object_factory.html", [
      [ "ai::NamedObjectContext< Trigger >", "classai_1_1_named_object_context.html", [
        [ "ai::ChatTriggerContext", "classai_1_1_chat_trigger_context.html", null ],
        [ "ai::druid::TriggerFactoryInternal", "classai_1_1druid_1_1_trigger_factory_internal.html", null ],
        [ "ai::hunter::TriggerFactoryInternal", "classai_1_1hunter_1_1_trigger_factory_internal.html", null ],
        [ "ai::mage::TriggerFactoryInternal", "classai_1_1mage_1_1_trigger_factory_internal.html", null ],
        [ "ai::paladin::TriggerFactoryInternal", "classai_1_1paladin_1_1_trigger_factory_internal.html", null ],
        [ "ai::priest::TriggerFactoryInternal", "classai_1_1priest_1_1_trigger_factory_internal.html", null ],
        [ "ai::rogue::TriggerFactoryInternal", "classai_1_1rogue_1_1_trigger_factory_internal.html", null ],
        [ "ai::shaman::TriggerFactoryInternal", "classai_1_1shaman_1_1_trigger_factory_internal.html", null ],
        [ "ai::TriggerContext", "classai_1_1_trigger_context.html", null ],
        [ "ai::warlock::TriggerFactoryInternal", "classai_1_1warlock_1_1_trigger_factory_internal.html", null ],
        [ "ai::warrior::TriggerFactoryInternal", "classai_1_1warrior_1_1_trigger_factory_internal.html", null ],
        [ "ai::WorldPacketTriggerContext", "classai_1_1_world_packet_trigger_context.html", null ]
      ] ]
    ] ],
    [ "ai::NamedObjectFactory< UntypedValue >", "classai_1_1_named_object_factory.html", [
      [ "ai::NamedObjectContext< UntypedValue >", "classai_1_1_named_object_context.html", [
        [ "ai::ValueContext", "classai_1_1_value_context.html", null ]
      ] ]
    ] ],
    [ "ai::NamedObjectFactoryList< T >", "classai_1_1_named_object_factory_list.html", null ],
    [ "ai::NamedObjectFactoryList< ai::ActionNode >", "classai_1_1_named_object_factory_list.html", null ],
    [ "MaNGOS::NearestAssistCreatureInCreatureRangeCheck", "class_ma_n_g_o_s_1_1_nearest_assist_creature_in_creature_range_check.html", null ],
    [ "MaNGOS::NearestAttackableUnitInObjectRangeCheck", "class_ma_n_g_o_s_1_1_nearest_attackable_unit_in_object_range_check.html", null ],
    [ "MaNGOS::NearestCreatureEntryWithLiveStateInObjectRangeCheck", "class_ma_n_g_o_s_1_1_nearest_creature_entry_with_live_state_in_object_range_check.html", null ],
    [ "MaNGOS::NearestGameObjectEntryInObjectRangeCheck", "class_ma_n_g_o_s_1_1_nearest_game_object_entry_in_object_range_check.html", null ],
    [ "MaNGOS::NearestGameObjectEntryInPosRangeCheck", "class_ma_n_g_o_s_1_1_nearest_game_object_entry_in_pos_range_check.html", null ],
    [ "MaNGOS::NearestGameObjectFishingHoleCheck", "class_ma_n_g_o_s_1_1_nearest_game_object_fishing_hole_check.html", null ],
    [ "MaNGOS::NearUsedPosDo", "class_ma_n_g_o_s_1_1_near_used_pos_do.html", null ],
    [ "ai::NextAction", "classai_1_1_next_action.html", null ],
    [ "NGrid< N, ACTIVE_OBJECT, WORLD_OBJECT_TYPES, GRID_OBJECT_TYPES >", "class_n_grid.html", null ],
    [ "NoAttackersTrigger", null, [
      [ "ai::TankAoeTrigger", "classai_1_1_tank_aoe_trigger.html", null ]
    ] ],
    [ "NodeCreator< Node >", "struct_node_creator.html", null ],
    [ "NpcTextLocale", "struct_npc_text_locale.html", null ],
    [ "null", "structnull.html", null ],
    [ "Object", "class_object.html", [
      [ "Item", "class_item.html", [
        [ "Bag", "class_bag.html", null ]
      ] ],
      [ "WorldObject", "class_world_object.html", [
        [ "Corpse", "class_corpse.html", null ],
        [ "DynamicObject", "class_dynamic_object.html", null ],
        [ "GameObject", "class_game_object.html", [
          [ "Transport", "class_transport.html", [
            [ "GlobalTransport", "class_global_transport.html", null ],
            [ "LocalTransport", "class_local_transport.html", null ]
          ] ]
        ] ],
        [ "Unit", "class_unit.html", [
          [ "Creature", "class_creature.html", [
            [ "Pet", "class_pet.html", null ],
            [ "TemporarySummon", "class_temporary_summon.html", [
              [ "TemporarySummonWaypoint", "class_temporary_summon_waypoint.html", null ]
            ] ],
            [ "Totem", "class_totem.html", null ]
          ] ],
          [ "Player", "class_player.html", null ]
        ] ]
      ] ]
    ] ],
    [ "ObjectGridLoader", "class_object_grid_loader.html", null ],
    [ "ObjectGridRespawnMover", "class_object_grid_respawn_mover.html", null ],
    [ "ObjectGridStoper", "class_object_grid_stoper.html", null ],
    [ "ObjectGridUnloader", "class_object_grid_unloader.html", null ],
    [ "ObjectGuid", "class_object_guid.html", null ],
    [ "ObjectGuidGenerator< high >", "class_object_guid_generator.html", null ],
    [ "ObjectGuidGenerator< HIGHGUID_CORPSE >", "class_object_guid_generator.html", null ],
    [ "ObjectGuidGenerator< HIGHGUID_DYNAMICOBJECT >", "class_object_guid_generator.html", null ],
    [ "ObjectGuidGenerator< HIGHGUID_GAMEOBJECT >", "class_object_guid_generator.html", null ],
    [ "ObjectGuidGenerator< HIGHGUID_ITEM >", "class_object_guid_generator.html", null ],
    [ "ObjectGuidGenerator< HIGHGUID_PET >", "class_object_guid_generator.html", null ],
    [ "ObjectGuidGenerator< HIGHGUID_PLAYER >", "class_object_guid_generator.html", null ],
    [ "ObjectGuidGenerator< HIGHGUID_UNIT >", "class_object_guid_generator.html", null ],
    [ "MaNGOS::ObjectLevelLockable< T, MUTEX >", "class_ma_n_g_o_s_1_1_object_level_lockable.html", null ],
    [ "MaNGOS::ObjectLifeTime< T >", "class_ma_n_g_o_s_1_1_object_life_time.html", null ],
    [ "MaNGOS::ObjectMessageDeliverer", "struct_ma_n_g_o_s_1_1_object_message_deliverer.html", null ],
    [ "MaNGOS::ObjectMessageDistDeliverer", "struct_ma_n_g_o_s_1_1_object_message_dist_deliverer.html", null ],
    [ "ObjectMgr", "class_object_mgr.html", null ],
    [ "ObjectPosSelector", "struct_object_pos_selector.html", null ],
    [ "ObjectRegistry< T, Key >", "class_object_registry.html", null ],
    [ "MaNGOS::ObjectUpdater", "struct_ma_n_g_o_s_1_1_object_updater.html", null ],
    [ "ObjectWorldLoader", "class_object_world_loader.html", null ],
    [ "ObjectPosSelector::OccupiedArea", "struct_object_pos_selector_1_1_occupied_area.html", null ],
    [ "OpcodeHandler", "struct_opcode_handler.html", null ],
    [ "Opcodes", "class_opcodes.html", null ],
    [ "MaNGOS::OperatorNew< T >", "class_ma_n_g_o_s_1_1_operator_new.html", null ],
    [ "OutdoorPvP", "class_outdoor_pv_p.html", [
      [ "OutdoorPvPEP", "class_outdoor_pv_p_e_p.html", null ],
      [ "OutdoorPvPSI", "class_outdoor_pv_p_s_i.html", null ]
    ] ],
    [ "OutdoorPvPMgr", "class_outdoor_pv_p_mgr.html", null ],
    [ "PackedGuid", "class_packed_guid.html", null ],
    [ "PackedGuidReader", "struct_packed_guid_reader.html", null ],
    [ "Movement::PacketBuilder", "class_movement_1_1_packet_builder.html", null ],
    [ "PacketFilter", "class_packet_filter.html", [
      [ "MapSessionFilter", "class_map_session_filter.html", null ],
      [ "WorldSessionFilter", "class_world_session_filter.html", null ]
    ] ],
    [ "PacketHandlingHelper", "class_packet_handling_helper.html", null ],
    [ "PageText", "struct_page_text.html", null ],
    [ "PageTextLocale", "struct_page_text_locale.html", null ],
    [ "ai::PartyMemberActionNameSupport", "classai_1_1_party_member_action_name_support.html", [
      [ "ai::BuffOnPartyAction", "classai_1_1_buff_on_party_action.html", [
        [ "ai::CastArcaneIntellectOnPartyAction", "classai_1_1_cast_arcane_intellect_on_party_action.html", null ],
        [ "ai::CastBlessingOfKingsOnPartyAction", "classai_1_1_cast_blessing_of_kings_on_party_action.html", null ],
        [ "ai::CastBlessingOfMightOnPartyAction", "classai_1_1_cast_blessing_of_might_on_party_action.html", null ],
        [ "ai::CastBlessingOfSanctuaryOnPartyAction", "classai_1_1_cast_blessing_of_sanctuary_on_party_action.html", null ],
        [ "ai::CastBlessingOfWisdomOnPartyAction", "classai_1_1_cast_blessing_of_wisdom_on_party_action.html", null ],
        [ "ai::CastDivineSpiritOnPartyAction", "classai_1_1_cast_divine_spirit_on_party_action.html", null ],
        [ "ai::CastMarkOfTheWildOnPartyAction", "classai_1_1_cast_mark_of_the_wild_on_party_action.html", null ],
        [ "ai::CastPowerWordFortitudeOnPartyAction", "classai_1_1_cast_power_word_fortitude_on_party_action.html", null ],
        [ "ai::CastWaterBreathingOnPartyAction", "classai_1_1_cast_water_breathing_on_party_action.html", null ],
        [ "ai::CastWaterWalkingOnPartyAction", "classai_1_1_cast_water_walking_on_party_action.html", null ]
      ] ],
      [ "ai::CurePartyMemberAction", "classai_1_1_cure_party_member_action.html", [
        [ "ai::CastAbolishDiseaseOnPartyAction", "classai_1_1_cast_abolish_disease_on_party_action.html", null ],
        [ "ai::CastAbolishPoisonOnPartyAction", "classai_1_1_cast_abolish_poison_on_party_action.html", null ],
        [ "ai::CastCleanseDiseaseOnPartyAction", "classai_1_1_cast_cleanse_disease_on_party_action.html", null ],
        [ "ai::CastCleanseMagicOnPartyAction", "classai_1_1_cast_cleanse_magic_on_party_action.html", null ],
        [ "ai::CastCleansePoisonOnPartyAction", "classai_1_1_cast_cleanse_poison_on_party_action.html", null ],
        [ "ai::CastCureDiseaseOnPartyAction", "classai_1_1_cast_cure_disease_on_party_action.html", null ],
        [ "ai::CastCurePoisonOnPartyAction", "classai_1_1_cast_cure_poison_on_party_action.html", null ],
        [ "ai::CastDispelMagicOnPartyAction", "classai_1_1_cast_dispel_magic_on_party_action.html", null ],
        [ "ai::CastPurifyDiseaseOnPartyAction", "classai_1_1_cast_purify_disease_on_party_action.html", null ],
        [ "ai::CastPurifyPoisonOnPartyAction", "classai_1_1_cast_purify_poison_on_party_action.html", null ],
        [ "ai::CastRemoveCurseOnPartyAction", "classai_1_1_cast_remove_curse_on_party_action.html", null ]
      ] ],
      [ "ai::HealPartyMemberAction", "classai_1_1_heal_party_member_action.html", [
        [ "ai::CastDivineProtectionOnPartyAction", "classai_1_1_cast_divine_protection_on_party_action.html", null ],
        [ "ai::CastFlashHealOnPartyAction", "classai_1_1_cast_flash_heal_on_party_action.html", null ],
        [ "ai::CastFlashOfLightOnPartyAction", "classai_1_1_cast_flash_of_light_on_party_action.html", null ],
        [ "ai::CastGreaterHealOnPartyAction", "classai_1_1_cast_greater_heal_on_party_action.html", null ],
        [ "ai::CastHealingTouchOnPartyAction", "classai_1_1_cast_healing_touch_on_party_action.html", null ],
        [ "ai::CastHealingWaveOnPartyAction", "classai_1_1_cast_healing_wave_on_party_action.html", null ],
        [ "ai::CastHealOnPartyAction", "classai_1_1_cast_heal_on_party_action.html", null ],
        [ "ai::CastHolyLightOnPartyAction", "classai_1_1_cast_holy_light_on_party_action.html", null ],
        [ "ai::CastLayOnHandsOnPartyAction", "classai_1_1_cast_lay_on_hands_on_party_action.html", null ],
        [ "ai::CastLesserHealingWaveOnPartyAction", "classai_1_1_cast_lesser_healing_wave_on_party_action.html", null ],
        [ "ai::CastLesserHealOnPartyAction", "classai_1_1_cast_lesser_heal_on_party_action.html", null ],
        [ "ai::CastPowerWordShieldOnPartyAction", "classai_1_1_cast_power_word_shield_on_party_action.html", null ],
        [ "ai::CastRegrowthOnPartyAction", "classai_1_1_cast_regrowth_on_party_action.html", null ],
        [ "ai::CastRejuvenationOnPartyAction", "classai_1_1_cast_rejuvenation_on_party_action.html", null ],
        [ "ai::CastRenewOnPartyAction", "classai_1_1_cast_renew_on_party_action.html", null ]
      ] ]
    ] ],
    [ "Path< PathElem, PathNode >", "class_path.html", null ],
    [ "PathFinder", "class_path_finder.html", null ],
    [ "PathMovementBase< T, P >", "class_path_movement_base.html", null ],
    [ "PathMovementBase< Creature, WaypointPath const *>", "class_path_movement_base.html", [
      [ "WaypointMovementGenerator< Creature >", "class_waypoint_movement_generator_3_01_creature_01_4.html", null ]
    ] ],
    [ "PathMovementBase< Player, TaxiPathNodeList const *>", "class_path_movement_base.html", [
      [ "FlightPathMovementGenerator", "class_flight_path_movement_generator.html", null ]
    ] ],
    [ "PathNode", "struct_path_node.html", null ],
    [ "Permissible< T >", "class_permissible.html", null ],
    [ "Permissible< Creature >", "class_permissible.html", [
      [ "SelectableAI", "struct_selectable_a_i.html", null ]
    ] ],
    [ "PetAura", "class_pet_aura.html", null ],
    [ "PetCreateSpellEntry", "struct_pet_create_spell_entry.html", null ],
    [ "PetLevelInfo", "struct_pet_level_info.html", null ],
    [ "PetOwnerKilledUnitHelper", "struct_pet_owner_killed_unit_helper.html", null ],
    [ "PetSpell", "struct_pet_spell.html", null ],
    [ "PlaguelandsTowerBuff", "struct_plaguelands_tower_buff.html", null ],
    [ "PlaguelandsTowerEvent", "struct_plaguelands_tower_event.html", null ],
    [ "ai::PlayerbotAIAware", "classai_1_1_playerbot_a_i_aware.html", [
      [ "ai::AiObject", "classai_1_1_ai_object.html", [
        [ "ai::AiNamedObject", "classai_1_1_ai_named_object.html", [
          [ "ai::Action", "classai_1_1_action.html", [
            [ "ai::AcceptDuelAction", "classai_1_1_accept_duel_action.html", null ],
            [ "ai::AcceptInvitationAction", "classai_1_1_accept_invitation_action.html", null ],
            [ "ai::AcceptQuestShareAction", "classai_1_1_accept_quest_share_action.html", null ],
            [ "ai::AcceptResurrectAction", "classai_1_1_accept_resurrect_action.html", null ],
            [ "ai::AddAllLootAction", "classai_1_1_add_all_loot_action.html", [
              [ "ai::AddGatheringLootAction", "classai_1_1_add_gathering_loot_action.html", null ]
            ] ],
            [ "ai::AddLootAction", "classai_1_1_add_loot_action.html", null ],
            [ "ai::CastCustomSpellAction", "classai_1_1_cast_custom_spell_action.html", null ],
            [ "ai::CastRemoveShadowformAction", "classai_1_1_cast_remove_shadowform_action.html", null ],
            [ "ai::CastSpellAction", "classai_1_1_cast_spell_action.html", [
              [ "ai::CastArcaneMissilesAction", "classai_1_1_cast_arcane_missiles_action.html", null ],
              [ "ai::CastAuraSpellAction", "classai_1_1_cast_aura_spell_action.html", [
                [ "ai::CastBuffSpellAction", "classai_1_1_cast_buff_spell_action.html", [
                  [ "ai::BuffOnPartyAction", "classai_1_1_buff_on_party_action.html", null ],
                  [ "ai::CastArcaneIntellectAction", "classai_1_1_cast_arcane_intellect_action.html", null ],
                  [ "ai::CastArcaneTorrentAction", "classai_1_1_cast_arcane_torrent_action.html", null ],
                  [ "ai::CastAspectOfTheCheetahAction", "classai_1_1_cast_aspect_of_the_cheetah_action.html", null ],
                  [ "ai::CastAspectOfTheHawkAction", "classai_1_1_cast_aspect_of_the_hawk_action.html", null ],
                  [ "ai::CastAspectOfThePackAction", "classai_1_1_cast_aspect_of_the_pack_action.html", null ],
                  [ "ai::CastAspectOfTheWildAction", "classai_1_1_cast_aspect_of_the_wild_action.html", null ],
                  [ "ai::CastBanishAction", "classai_1_1_cast_banish_action.html", null ],
                  [ "ai::CastBarskinAction", "classai_1_1_cast_barskin_action.html", null ],
                  [ "ai::CastBattleShoutAction", "classai_1_1_cast_battle_shout_action.html", null ],
                  [ "ai::CastBattleStanceAction", "classai_1_1_cast_battle_stance_action.html", null ],
                  [ "ai::CastBearFormAction", "classai_1_1_cast_bear_form_action.html", null ],
                  [ "ai::CastBerserkAction", "classai_1_1_cast_berserk_action.html", null ],
                  [ "ai::CastBerserkerRageAction", "classai_1_1_cast_berserker_rage_action.html", null ],
                  [ "ai::CastBladeFlurryAction", "classai_1_1_cast_blade_flurry_action.html", null ],
                  [ "ai::CastBlessingOfKingsAction", "classai_1_1_cast_blessing_of_kings_action.html", null ],
                  [ "ai::CastBlessingOfMightAction", "classai_1_1_cast_blessing_of_might_action.html", null ],
                  [ "ai::CastBlessingOfSanctuaryAction", "classai_1_1_cast_blessing_of_sanctuary_action.html", null ],
                  [ "ai::CastBlessingOfWisdomAction", "classai_1_1_cast_blessing_of_wisdom_action.html", null ],
                  [ "ai::CastBloodlustAction", "classai_1_1_cast_bloodlust_action.html", null ],
                  [ "ai::CastBloodrageAction", "classai_1_1_cast_bloodrage_action.html", null ],
                  [ "ai::CastCallPetAction", "classai_1_1_cast_call_pet_action.html", null ],
                  [ "ai::CastCasterFormAction", "classai_1_1_cast_caster_form_action.html", null ],
                  [ "ai::CastCatFormAction", "classai_1_1_cast_cat_form_action.html", null ],
                  [ "ai::CastCombustionAction", "classai_1_1_cast_combustion_action.html", null ],
                  [ "ai::CastConcentrationAuraAction", "classai_1_1_cast_concentration_aura_action.html", null ],
                  [ "ai::CastCowerAction", "classai_1_1_cast_cower_action.html", null ],
                  [ "ai::CastCreateFirestoneAction", "classai_1_1_cast_create_firestone_action.html", null ],
                  [ "ai::CastCreateHealthstoneAction", "classai_1_1_cast_create_healthstone_action.html", null ],
                  [ "ai::CastCreateSpellstoneAction", "classai_1_1_cast_create_spellstone_action.html", null ],
                  [ "ai::CastDeathWishAction", "classai_1_1_cast_death_wish_action.html", null ],
                  [ "ai::CastDefensiveStanceAction", "classai_1_1_cast_defensive_stance_action.html", null ],
                  [ "ai::CastDemonArmorAction", "classai_1_1_cast_demon_armor_action.html", null ],
                  [ "ai::CastDemonSkinAction", "classai_1_1_cast_demon_skin_action.html", null ],
                  [ "ai::CastDevotionAuraAction", "classai_1_1_cast_devotion_aura_action.html", null ],
                  [ "ai::CastDireBearFormAction", "classai_1_1_cast_dire_bear_form_action.html", null ],
                  [ "ai::CastDivineProtectionAction", "classai_1_1_cast_divine_protection_action.html", null ],
                  [ "ai::CastDivineShieldAction", "classai_1_1_cast_divine_shield_action.html", null ],
                  [ "ai::CastDivineSpiritAction", "classai_1_1_cast_divine_spirit_action.html", null ],
                  [ "ai::CastEvasionAction", "classai_1_1_cast_evasion_action.html", null ],
                  [ "ai::CastFadeAction", "classai_1_1_cast_fade_action.html", null ],
                  [ "ai::CastFearOnCcAction", "classai_1_1_cast_fear_on_cc_action.html", null ],
                  [ "ai::CastFeignDeathAction", "classai_1_1_cast_feign_death_action.html", null ],
                  [ "ai::CastFeintAction", "classai_1_1_cast_feint_action.html", null ],
                  [ "ai::CastFireResistanceAuraAction", "classai_1_1_cast_fire_resistance_aura_action.html", null ],
                  [ "ai::CastFrostArmorAction", "classai_1_1_cast_frost_armor_action.html", null ],
                  [ "ai::CastFrostResistanceAuraAction", "classai_1_1_cast_frost_resistance_aura_action.html", null ],
                  [ "ai::CastHolyShieldAction", "classai_1_1_cast_holy_shield_action.html", null ],
                  [ "ai::CastIceArmorAction", "classai_1_1_cast_ice_armor_action.html", null ],
                  [ "ai::CastIceBlockAction", "classai_1_1_cast_ice_block_action.html", null ],
                  [ "ai::CastInnerFireAction", "classai_1_1_cast_inner_fire_action.html", null ],
                  [ "ai::CastLastStandAction", "classai_1_1_cast_last_stand_action.html", null ],
                  [ "ai::CastLightningShieldAction", "classai_1_1_cast_lightning_shield_action.html", null ],
                  [ "ai::CastMageArmorAction", "classai_1_1_cast_mage_armor_action.html", null ],
                  [ "ai::CastMarkOfTheWildAction", "classai_1_1_cast_mark_of_the_wild_action.html", null ],
                  [ "ai::CastMoonkinFormAction", "classai_1_1_cast_moonkin_form_action.html", null ],
                  [ "ai::CastNaturesGraspAction", "classai_1_1_cast_natures_grasp_action.html", null ],
                  [ "ai::CastPolymorphAction", "classai_1_1_cast_polymorph_action.html", null ],
                  [ "ai::CastPowerWordFortitudeAction", "classai_1_1_cast_power_word_fortitude_action.html", null ],
                  [ "ai::CastPowerWordShieldAction", "classai_1_1_cast_power_word_shield_action.html", null ],
                  [ "ai::CastRapidFireAction", "classai_1_1_cast_rapid_fire_action.html", null ],
                  [ "ai::CastRetributionAuraAction", "classai_1_1_cast_retribution_aura_action.html", null ],
                  [ "ai::CastRevivePetAction", "classai_1_1_cast_revive_pet_action.html", null ],
                  [ "ai::CastRighteousFuryAction", "classai_1_1_cast_righteous_fury_action.html", null ],
                  [ "ai::CastSealOfCommandAction", "classai_1_1_cast_seal_of_command_action.html", null ],
                  [ "ai::CastSealOfJusticeAction", "classai_1_1_cast_seal_of_justice_action.html", null ],
                  [ "ai::CastSealOfLightAction", "classai_1_1_cast_seal_of_light_action.html", null ],
                  [ "ai::CastSealOfRighteousnessAction", "classai_1_1_cast_seal_of_righteousness_action.html", null ],
                  [ "ai::CastSealOfWisdomAction", "classai_1_1_cast_seal_of_wisdom_action.html", null ],
                  [ "ai::CastShadowformAction", "classai_1_1_cast_shadowform_action.html", null ],
                  [ "ai::CastShadowResistanceAuraAction", "classai_1_1_cast_shadow_resistance_aura_action.html", null ],
                  [ "ai::CastShieldBlockAction", "classai_1_1_cast_shield_block_action.html", null ],
                  [ "ai::CastSprintAction", "classai_1_1_cast_sprint_action.html", null ],
                  [ "ai::CastSummonImpAction", "classai_1_1_cast_summon_imp_action.html", null ],
                  [ "ai::CastSummonVoidwalkerAction", "classai_1_1_cast_summon_voidwalker_action.html", null ],
                  [ "ai::CastThornsAction", "classai_1_1_cast_thorns_action.html", null ],
                  [ "ai::CastTigersFuryAction", "classai_1_1_cast_tigers_fury_action.html", null ],
                  [ "ai::CastTotemAction", "classai_1_1_cast_totem_action.html", [
                    [ "ai::CastCleansingTotemAction", "classai_1_1_cast_cleansing_totem_action.html", null ],
                    [ "ai::CastEarthbindTotemAction", "classai_1_1_cast_earthbind_totem_action.html", null ],
                    [ "ai::CastFlametongueTotemAction", "classai_1_1_cast_flametongue_totem_action.html", null ],
                    [ "ai::CastHealingStreamTotemAction", "classai_1_1_cast_healing_stream_totem_action.html", null ],
                    [ "ai::CastManaSpringTotemAction", "classai_1_1_cast_mana_spring_totem_action.html", null ],
                    [ "ai::CastManaTideTotemAction", "classai_1_1_cast_mana_tide_totem_action.html", null ],
                    [ "ai::CastSearingTotemAction", "classai_1_1_cast_searing_totem_action.html", null ],
                    [ "ai::CastStoneskinTotemAction", "classai_1_1_cast_stoneskin_totem_action.html", null ],
                    [ "ai::CastStrengthOfEarthTotemAction", "classai_1_1_cast_strength_of_earth_totem_action.html", null ],
                    [ "ai::CastWindfuryTotemAction", "classai_1_1_cast_windfury_totem_action.html", null ]
                  ] ],
                  [ "ai::CastTreeFormAction", "classai_1_1_cast_tree_form_action.html", null ],
                  [ "ai::CastTrueshotAuraAction", "classai_1_1_cast_trueshot_aura_action.html", null ],
                  [ "ai::CastVampiricEmbraceAction", "classai_1_1_cast_vampiric_embrace_action.html", null ],
                  [ "ai::CastVanishAction", "classai_1_1_cast_vanish_action.html", null ],
                  [ "ai::CastWaterBreathingAction", "classai_1_1_cast_water_breathing_action.html", null ],
                  [ "ai::CastWaterWalkingAction", "classai_1_1_cast_water_walking_action.html", null ]
                ] ],
                [ "ai::CastDebuffSpellAction", "classai_1_1_cast_debuff_spell_action.html", [
                  [ "ai::CastBlindAction", "classai_1_1_cast_blind_action.html", null ],
                  [ "ai::CastCorruptionAction", "classai_1_1_cast_corruption_action.html", null ],
                  [ "ai::CastCurseOfAgonyAction", "classai_1_1_cast_curse_of_agony_action.html", null ],
                  [ "ai::CastCurseOfWeaknessAction", "classai_1_1_cast_curse_of_weakness_action.html", null ],
                  [ "ai::CastDemoralizingRoarAction", "classai_1_1_cast_demoralizing_roar_action.html", null ],
                  [ "ai::CastDemoralizingShoutAction", "classai_1_1_cast_demoralizing_shout_action.html", null ],
                  [ "ai::CastEarthShockAction", "classai_1_1_cast_earth_shock_action.html", null ],
                  [ "ai::CastFearAction", "classai_1_1_cast_fear_action.html", null ],
                  [ "ai::CastFlameShockAction", "classai_1_1_cast_flame_shock_action.html", null ],
                  [ "ai::CastFreezingTrap", "classai_1_1_cast_freezing_trap.html", null ],
                  [ "ai::CastFrostShockAction", "classai_1_1_cast_frost_shock_action.html", null ],
                  [ "ai::CastImmolateAction", "classai_1_1_cast_immolate_action.html", null ],
                  [ "ai::CastInsectSwarmAction", "classai_1_1_cast_insect_swarm_action.html", null ],
                  [ "ai::CastMoonfireAction", "classai_1_1_cast_moonfire_action.html", null ],
                  [ "ai::CastPowerWordPainAction", "classai_1_1_cast_power_word_pain_action.html", null ],
                  [ "ai::CastRakeAction", "classai_1_1_cast_rake_action.html", null ]
                ] ],
                [ "ai::CastDebuffSpellOnAttackerAction", "classai_1_1_cast_debuff_spell_on_attacker_action.html", [
                  [ "ai::CastCorruptionOnAttackerAction", "classai_1_1_cast_corruption_on_attacker_action.html", null ],
                  [ "ai::CastPowerWordPainOnAttackerAction", "classai_1_1_cast_power_word_pain_on_attacker_action.html", null ],
                  [ "ai::CastRendOnAttackerAction", "classai_1_1_cast_rend_on_attacker_action.html", null ],
                  [ "ai::CastSerpentStingOnAttackerAction", "classai_1_1_cast_serpent_sting_on_attacker_action.html", null ]
                ] ],
                [ "ai::CastHealingSpellAction", "classai_1_1_cast_healing_spell_action.html", [
                  [ "ai::CastAoeHealSpellAction", "classai_1_1_cast_aoe_heal_spell_action.html", [
                    [ "ai::CastChainHealAction", "classai_1_1_cast_chain_heal_action.html", null ],
                    [ "ai::CastTranquilityAction", "classai_1_1_cast_tranquility_action.html", null ]
                  ] ],
                  [ "ai::CastFlashHealAction", "classai_1_1_cast_flash_heal_action.html", null ],
                  [ "ai::CastFlashOfLightAction", "classai_1_1_cast_flash_of_light_action.html", null ],
                  [ "ai::CastGiftOfTheNaaruAction", "classai_1_1_cast_gift_of_the_naaru_action.html", null ],
                  [ "ai::CastGreaterHealAction", "classai_1_1_cast_greater_heal_action.html", null ],
                  [ "ai::CastHealAction", "classai_1_1_cast_heal_action.html", null ],
                  [ "ai::CastHealingTouchAction", "classai_1_1_cast_healing_touch_action.html", null ],
                  [ "ai::CastHealingWaveAction", "classai_1_1_cast_healing_wave_action.html", null ],
                  [ "ai::CastHolyLightAction", "classai_1_1_cast_holy_light_action.html", null ],
                  [ "ai::CastLayOnHandsAction", "classai_1_1_cast_lay_on_hands_action.html", null ],
                  [ "ai::CastLesserHealAction", "classai_1_1_cast_lesser_heal_action.html", null ],
                  [ "ai::CastLesserHealingWaveAction", "classai_1_1_cast_lesser_healing_wave_action.html", null ],
                  [ "ai::CastLifeBloodAction", "classai_1_1_cast_life_blood_action.html", null ],
                  [ "ai::CastRegrowthAction", "classai_1_1_cast_regrowth_action.html", null ],
                  [ "ai::CastRejuvenationAction", "classai_1_1_cast_rejuvenation_action.html", null ],
                  [ "ai::CastRenewAction", "classai_1_1_cast_renew_action.html", null ],
                  [ "ai::HealPartyMemberAction", "classai_1_1_heal_party_member_action.html", null ]
                ] ],
                [ "ai::CastMendPetAction", "classai_1_1_cast_mend_pet_action.html", null ]
              ] ],
              [ "ai::CastBlastWaveAction", "classai_1_1_cast_blast_wave_action.html", null ],
              [ "ai::CastBlizzardAction", "classai_1_1_cast_blizzard_action.html", null ],
              [ "ai::CastChainLightningAction", "classai_1_1_cast_chain_lightning_action.html", null ],
              [ "ai::CastConflagrateAction", "classai_1_1_cast_conflagrate_action.html", null ],
              [ "ai::CastCureSpellAction", "classai_1_1_cast_cure_spell_action.html", [
                [ "ai::CastAbolishDiseaseAction", "classai_1_1_cast_abolish_disease_action.html", null ],
                [ "ai::CastAbolishPoisonAction", "classai_1_1_cast_abolish_poison_action.html", null ],
                [ "ai::CastCleanseDiseaseAction", "classai_1_1_cast_cleanse_disease_action.html", null ],
                [ "ai::CastCleanseMagicAction", "classai_1_1_cast_cleanse_magic_action.html", null ],
                [ "ai::CastCleansePoisonAction", "classai_1_1_cast_cleanse_poison_action.html", null ],
                [ "ai::CastCureDiseaseAction", "classai_1_1_cast_cure_disease_action.html", null ],
                [ "ai::CastCurePoisonAction", "classai_1_1_cast_cure_poison_action.html", null ],
                [ "ai::CastDispelMagicAction", "classai_1_1_cast_dispel_magic_action.html", null ],
                [ "ai::CastPurifyDiseaseAction", "classai_1_1_cast_purify_disease_action.html", null ],
                [ "ai::CastPurifyPoisonAction", "classai_1_1_cast_purify_poison_action.html", null ],
                [ "ai::CastRemoveCurseAction", "classai_1_1_cast_remove_curse_action.html", null ]
              ] ],
              [ "ai::CastDispelMagicOnTargetAction", "classai_1_1_cast_dispel_magic_on_target_action.html", null ],
              [ "ai::CastDistractAction", "classai_1_1_cast_distract_action.html", null ],
              [ "ai::CastDrainLifeAction", "classai_1_1_cast_drain_life_action.html", null ],
              [ "ai::CastDrainManaAction", "classai_1_1_cast_drain_mana_action.html", null ],
              [ "ai::CastDrainSoulAction", "classai_1_1_cast_drain_soul_action.html", null ],
              [ "ai::CastEnchantItemAction", "classai_1_1_cast_enchant_item_action.html", [
                [ "ai::CastFlametongueWeaponAction", "classai_1_1_cast_flametongue_weapon_action.html", null ],
                [ "ai::CastFrostbrandWeaponAction", "classai_1_1_cast_frostbrand_weapon_action.html", null ],
                [ "ai::CastRockbiterWeaponAction", "classai_1_1_cast_rockbiter_weapon_action.html", null ],
                [ "ai::CastWindfuryWeaponAction", "classai_1_1_cast_windfury_weapon_action.html", null ]
              ] ],
              [ "ai::CastEntanglingRootsAction", "classai_1_1_cast_entangling_roots_action.html", null ],
              [ "ai::CastEntanglingRootsCcAction", "classai_1_1_cast_entangling_roots_cc_action.html", null ],
              [ "ai::CastEvocationAction", "classai_1_1_cast_evocation_action.html", null ],
              [ "ai::CastFaerieFireAction", "classai_1_1_cast_faerie_fire_action.html", null ],
              [ "ai::CastFaerieFireFeralAction", "classai_1_1_cast_faerie_fire_feral_action.html", null ],
              [ "ai::CastFireballAction", "classai_1_1_cast_fireball_action.html", null ],
              [ "ai::CastFireBlastAction", "classai_1_1_cast_fire_blast_action.html", null ],
              [ "ai::CastFlamestrikeAction", "classai_1_1_cast_flamestrike_action.html", null ],
              [ "ai::CastFrostboltAction", "classai_1_1_cast_frostbolt_action.html", null ],
              [ "ai::CastFrostNovaAction", "classai_1_1_cast_frost_nova_action.html", null ],
              [ "ai::CastGrowlAction", "classai_1_1_cast_growl_action.html", null ],
              [ "ai::CastHibernateAction", "classai_1_1_cast_hibernate_action.html", null ],
              [ "ai::CastHurricaneAction", "classai_1_1_cast_hurricane_action.html", null ],
              [ "ai::CastInnervateAction", "classai_1_1_cast_innervate_action.html", null ],
              [ "ai::CastKickAction", "classai_1_1_cast_kick_action.html", null ],
              [ "ai::CastLifeTapAction", "classai_1_1_cast_life_tap_action.html", null ],
              [ "ai::CastLightningBoltAction", "classai_1_1_cast_lightning_bolt_action.html", null ],
              [ "ai::CastMeleeSpellAction", "classai_1_1_cast_melee_spell_action.html", [
                [ "ai::CastBashAction", "classai_1_1_cast_bash_action.html", null ],
                [ "ai::CastBattleMeleeSpellAction", "classai_1_1_cast_battle_melee_spell_action.html", [
                  [ "ai::CastOverpowerAction", "classai_1_1_cast_overpower_action.html", null ]
                ] ],
                [ "ai::CastBloodthirstAction", "classai_1_1_cast_bloodthirst_action.html", null ],
                [ "ai::CastCheapShotAction", "classai_1_1_cast_cheap_shot_action.html", null ],
                [ "ai::CastClawAction", "classai_1_1_cast_claw_action.html", null ],
                [ "ai::CastCleaveAction", "classai_1_1_cast_cleave_action.html", null ],
                [ "ai::CastComboAction", "classai_1_1_cast_combo_action.html", [
                  [ "ai::CastBackstabAction", "classai_1_1_cast_backstab_action.html", null ],
                  [ "ai::CastGougeAction", "classai_1_1_cast_gouge_action.html", null ],
                  [ "ai::CastMutilateAction", "classai_1_1_cast_mutilate_action.html", null ],
                  [ "ai::CastRiposteAction", "classai_1_1_cast_riposte_action.html", null ],
                  [ "ai::CastSinisterStrikeAction", "classai_1_1_cast_sinister_strike_action.html", null ]
                ] ],
                [ "ai::CastConsecrationAction", "classai_1_1_cast_consecration_action.html", null ],
                [ "ai::CastDefensiveMeleeSpellAction", "classai_1_1_cast_defensive_melee_spell_action.html", [
                  [ "ai::CastConcussionBlowAction", "classai_1_1_cast_concussion_blow_action.html", null ],
                  [ "ai::CastShieldWallAction", "classai_1_1_cast_shield_wall_action.html", null ]
                ] ],
                [ "ai::CastEviscerateAction", "classai_1_1_cast_eviscerate_action.html", null ],
                [ "ai::CastExecuteAction", "classai_1_1_cast_execute_action.html", null ],
                [ "ai::CastExposeArmorAction", "classai_1_1_cast_expose_armor_action.html", null ],
                [ "ai::CastFerociousBiteAction", "classai_1_1_cast_ferocious_bite_action.html", null ],
                [ "ai::CastGarroteAction", "classai_1_1_cast_garrote_action.html", null ],
                [ "ai::CastHammerOfJusticeAction", "classai_1_1_cast_hammer_of_justice_action.html", null ],
                [ "ai::CastHammerOfWrathAction", "classai_1_1_cast_hammer_of_wrath_action.html", null ],
                [ "ai::CastHamstringAction", "classai_1_1_cast_hamstring_action.html", null ],
                [ "ai::CastHeroicStrikeAction", "classai_1_1_cast_heroic_strike_action.html", null ],
                [ "ai::CastHolyWrathAction", "classai_1_1_cast_holy_wrath_action.html", null ],
                [ "ai::CastJudgementOfJusticeAction", "classai_1_1_cast_judgement_of_justice_action.html", null ],
                [ "ai::CastJudgementOfLightAction", "classai_1_1_cast_judgement_of_light_action.html", null ],
                [ "ai::CastJudgementOfWisdomAction", "classai_1_1_cast_judgement_of_wisdom_action.html", null ],
                [ "ai::CastKidneyShotAction", "classai_1_1_cast_kidney_shot_action.html", null ],
                [ "ai::CastLacerateAction", "classai_1_1_cast_lacerate_action.html", null ],
                [ "ai::CastMagmaTotemAction", "classai_1_1_cast_magma_totem_action.html", null ],
                [ "ai::CastMangleBearAction", "classai_1_1_cast_mangle_bear_action.html", null ],
                [ "ai::CastMangleCatAction", "classai_1_1_cast_mangle_cat_action.html", null ],
                [ "ai::CastMaulAction", "classai_1_1_cast_maul_action.html", null ],
                [ "ai::CastMockingBlowAction", "classai_1_1_cast_mocking_blow_action.html", null ],
                [ "ai::CastRipAction", "classai_1_1_cast_rip_action.html", null ],
                [ "ai::CastRuptureAction", "classai_1_1_cast_rupture_action.html", null ],
                [ "ai::CastSapAction", "classai_1_1_cast_sap_action.html", null ],
                [ "ai::CastShieldSlamAction", "classai_1_1_cast_shield_slam_action.html", null ],
                [ "ai::CastSlamAction", "classai_1_1_cast_slam_action.html", null ],
                [ "ai::CastSliceAndDiceAction", "classai_1_1_cast_slice_and_dice_action.html", null ],
                [ "ai::CastStormstrikeAction", "classai_1_1_cast_stormstrike_action.html", null ],
                [ "ai::CastSwipeAction", "classai_1_1_cast_swipe_action.html", null ],
                [ "ai::CastSwipeBearAction", "classai_1_1_cast_swipe_bear_action.html", null ],
                [ "ai::CastSwipeCatAction", "classai_1_1_cast_swipe_cat_action.html", null ],
                [ "ai::CastWingClipAction", "classai_1_1_cast_wing_clip_action.html", null ]
              ] ],
              [ "ai::CastPsychicScreamAction", "classai_1_1_cast_psychic_scream_action.html", null ],
              [ "ai::CastPurgeAction", "classai_1_1_cast_purge_action.html", null ],
              [ "ai::CastPyroblastAction", "classai_1_1_cast_pyroblast_action.html", null ],
              [ "ai::CastRainOfFireAction", "classai_1_1_cast_rain_of_fire_action.html", null ],
              [ "ai::CastReachTargetSpellAction", "classai_1_1_cast_reach_target_spell_action.html", [
                [ "ai::CastFeralChargeBearAction", "classai_1_1_cast_feral_charge_bear_action.html", null ],
                [ "ai::CastFeralChargeCatAction", "classai_1_1_cast_feral_charge_cat_action.html", null ]
              ] ],
              [ "ai::CastScorchAction", "classai_1_1_cast_scorch_action.html", null ],
              [ "ai::CastShootAction", "classai_1_1_cast_shoot_action.html", null ],
              [ "ai::CastSpellOnEnemyHealerAction", "classai_1_1_cast_spell_on_enemy_healer_action.html", [
                [ "ai::CastBashOnEnemyHealerAction", "classai_1_1_cast_bash_on_enemy_healer_action.html", null ],
                [ "ai::CastCounterspellOnEnemyHealerAction", "classai_1_1_cast_counterspell_on_enemy_healer_action.html", null ],
                [ "ai::CastHammerOfJusticeOnEnemyHealerAction", "classai_1_1_cast_hammer_of_justice_on_enemy_healer_action.html", null ],
                [ "ai::CastKickOnEnemyHealerAction", "classai_1_1_cast_kick_on_enemy_healer_action.html", null ],
                [ "ai::CastShieldBashOnEnemyHealerAction", "classai_1_1_cast_shield_bash_on_enemy_healer_action.html", null ]
              ] ],
              [ "ai::CastStarfireAction", "classai_1_1_cast_starfire_action.html", null ],
              [ "ai::CastTauntAction", "classai_1_1_cast_taunt_action.html", null ],
              [ "ai::CastWrathAction", "classai_1_1_cast_wrath_action.html", null ],
              [ "ai::CurePartyMemberAction", "classai_1_1_cure_party_member_action.html", null ],
              [ "ai::ResurrectPartyMemberAction", "classai_1_1_resurrect_party_member_action.html", [
                [ "ai::CastAncestralSpiritAction", "classai_1_1_cast_ancestral_spirit_action.html", null ],
                [ "ai::CastRebirthAction", "classai_1_1_cast_rebirth_action.html", null ],
                [ "ai::CastRedemptionAction", "classai_1_1_cast_redemption_action.html", null ],
                [ "ai::CastResurrectionAction", "classai_1_1_cast_resurrection_action.html", null ]
              ] ]
            ] ],
            [ "ai::ChangeChatAction", "classai_1_1_change_chat_action.html", null ],
            [ "ai::ChangeCombatStrategyAction", "classai_1_1_change_combat_strategy_action.html", null ],
            [ "ai::ChangeDeadStrategyAction", "classai_1_1_change_dead_strategy_action.html", null ],
            [ "ai::ChangeNonCombatStrategyAction", "classai_1_1_change_non_combat_strategy_action.html", null ],
            [ "ai::ChangeTalentsAction", "classai_1_1_change_talents_action.html", null ],
            [ "ai::CheckMountStateAction", "classai_1_1_check_mount_state_action.html", null ],
            [ "ai::DropQuestAction", "classai_1_1_drop_quest_action.html", null ],
            [ "ai::DropTargetAction", "classai_1_1_drop_target_action.html", null ],
            [ "ai::EmoteAction", "classai_1_1_emote_action.html", null ],
            [ "ai::FleeChatShortcutAction", "classai_1_1_flee_chat_shortcut_action.html", null ],
            [ "ai::FollowChatShortcutAction", "classai_1_1_follow_chat_shortcut_action.html", null ],
            [ "ai::GoawayChatShortcutAction", "classai_1_1_goaway_chat_shortcut_action.html", null ],
            [ "ai::GossipHelloAction", "classai_1_1_gossip_hello_action.html", null ],
            [ "ai::GrindChatShortcutAction", "classai_1_1_grind_chat_shortcut_action.html", null ],
            [ "ai::GuildAcceptAction", "classai_1_1_guild_accept_action.html", null ],
            [ "ai::HelpAction", "classai_1_1_help_action.html", null ],
            [ "ai::InventoryAction", "classai_1_1_inventory_action.html", [
              [ "ai::BankAction", "classai_1_1_bank_action.html", null ],
              [ "ai::BuffAction", "classai_1_1_buff_action.html", null ],
              [ "ai::BuyAction", "classai_1_1_buy_action.html", null ],
              [ "ai::DestroyItemAction", "classai_1_1_destroy_item_action.html", null ],
              [ "ai::EquipAction", "classai_1_1_equip_action.html", null ],
              [ "ai::GuildBankAction", "classai_1_1_guild_bank_action.html", null ],
              [ "ai::InventoryItemValueBase", "classai_1_1_inventory_item_value_base.html", [
                [ "ai::InventoryItemValue", "classai_1_1_inventory_item_value.html", null ],
                [ "ai::ItemCountValue", "classai_1_1_item_count_value.html", null ]
              ] ],
              [ "ai::QueryItemUsageAction", "classai_1_1_query_item_usage_action.html", [
                [ "ai::LootRollAction", "classai_1_1_loot_roll_action.html", null ],
                [ "ai::TradeStatusAction", "classai_1_1_trade_status_action.html", null ]
              ] ],
              [ "ai::RewardAction", "classai_1_1_reward_action.html", null ],
              [ "ai::SellAction", "classai_1_1_sell_action.html", null ],
              [ "ai::SuggestWhatToDoAction", "classai_1_1_suggest_what_to_do_action.html", null ],
              [ "ai::TellItemCountAction", "classai_1_1_tell_item_count_action.html", null ],
              [ "ai::TradeAction", "classai_1_1_trade_action.html", null ],
              [ "ai::UnequipAction", "classai_1_1_unequip_action.html", null ],
              [ "ai::WhoAction", "classai_1_1_who_action.html", null ],
              [ "PlayerbotFactory", "class_playerbot_factory.html", null ]
            ] ],
            [ "ai::InventoryChangeFailureAction", "classai_1_1_inventory_change_failure_action.html", null ],
            [ "ai::InviteToGroupAction", "classai_1_1_invite_to_group_action.html", null ],
            [ "ai::LeaveGroupAction", "classai_1_1_leave_group_action.html", [
              [ "ai::PartyCommandAction", "classai_1_1_party_command_action.html", null ],
              [ "ai::UninviteAction", "classai_1_1_uninvite_action.html", null ]
            ] ],
            [ "ai::ListQuestsAction", "classai_1_1_list_quests_action.html", null ],
            [ "ai::ListSpellsAction", "classai_1_1_list_spells_action.html", null ],
            [ "ai::LogLevelAction", "classai_1_1_log_level_action.html", null ],
            [ "ai::LootStrategyAction", "classai_1_1_loot_strategy_action.html", null ],
            [ "ai::MaxDpsChatShortcutAction", "classai_1_1_max_dps_chat_shortcut_action.html", null ],
            [ "ai::MovementAction", "classai_1_1_movement_action.html", [
              [ "ai::AreaTriggerAction", "classai_1_1_area_trigger_action.html", null ],
              [ "ai::AttackAction", "classai_1_1_attack_action.html", [
                [ "ai::AttackAnythingAction", "classai_1_1_attack_anything_action.html", null ],
                [ "ai::AttackDuelOpponentAction", "classai_1_1_attack_duel_opponent_action.html", null ],
                [ "ai::AttackEnemyPlayerAction", "classai_1_1_attack_enemy_player_action.html", null ],
                [ "ai::AttackLeastHpTargetAction", "classai_1_1_attack_least_hp_target_action.html", null ],
                [ "ai::AttackMyTargetAction", "classai_1_1_attack_my_target_action.html", null ],
                [ "ai::AttackRtiTargetAction", "classai_1_1_attack_rti_target_action.html", null ],
                [ "ai::DpsAssistAction", "classai_1_1_dps_assist_action.html", null ],
                [ "ai::MeleeAction", "classai_1_1_melee_action.html", null ],
                [ "ai::TankAssistAction", "classai_1_1_tank_assist_action.html", null ]
              ] ],
              [ "ai::FleeAction", "classai_1_1_flee_action.html", null ],
              [ "ai::FollowAction", "classai_1_1_follow_action.html", [
                [ "ai::FollowLineAction", "classai_1_1_follow_line_action.html", null ]
              ] ],
              [ "ai::FollowMasterAction", "classai_1_1_follow_master_action.html", null ],
              [ "ai::FollowMasterRandomAction", "classai_1_1_follow_master_random_action.html", null ],
              [ "ai::LootAction", "classai_1_1_loot_action.html", null ],
              [ "ai::MoveOutOfEnemyContactAction", "classai_1_1_move_out_of_enemy_contact_action.html", null ],
              [ "ai::MoveRandomAction", "classai_1_1_move_random_action.html", null ],
              [ "ai::MoveToLootAction", "classai_1_1_move_to_loot_action.html", null ],
              [ "ai::MoveToPositionAction", "classai_1_1_move_to_position_action.html", [
                [ "ai::GuardAction", "classai_1_1_guard_action.html", null ]
              ] ],
              [ "ai::OpenLootAction", "classai_1_1_open_loot_action.html", null ],
              [ "ai::OutOfReactRangeAction", "classai_1_1_out_of_react_range_action.html", null ],
              [ "ai::ReachAreaTriggerAction", "classai_1_1_reach_area_trigger_action.html", null ],
              [ "ai::ReachTargetAction", "classai_1_1_reach_target_action.html", [
                [ "ai::ReachMeleeAction", "classai_1_1_reach_melee_action.html", null ],
                [ "ai::ReachSpellAction", "classai_1_1_reach_spell_action.html", null ]
              ] ],
              [ "ai::RunAwayAction", "classai_1_1_run_away_action.html", null ],
              [ "ai::SetFacingTargetAction", "classai_1_1_set_facing_target_action.html", null ],
              [ "ai::SetHomeAction", "classai_1_1_set_home_action.html", null ],
              [ "ai::StayActionBase", "classai_1_1_stay_action_base.html", [
                [ "ai::StayAction", "classai_1_1_stay_action.html", null ],
                [ "ai::StayCircleAction", "classai_1_1_stay_circle_action.html", null ],
                [ "ai::StayCombatAction", "classai_1_1_stay_combat_action.html", null ],
                [ "ai::StayLineAction", "classai_1_1_stay_line_action.html", null ]
              ] ],
              [ "ai::StoreLootAction", "classai_1_1_store_loot_action.html", [
                [ "ai::LootRollAction", "classai_1_1_loot_roll_action.html", null ]
              ] ],
              [ "ai::SummonAction", "classai_1_1_summon_action.html", [
                [ "ai::UseMeetingStoneAction", "classai_1_1_use_meeting_stone_action.html", null ]
              ] ]
            ] ],
            [ "ai::PassLeadershipToMasterAction", "classai_1_1_pass_leadership_to_master_action.html", null ],
            [ "ai::PositionAction", "classai_1_1_position_action.html", null ],
            [ "ai::QueryQuestAction", "classai_1_1_query_quest_action.html", null ],
            [ "ai::QuestAction", "classai_1_1_quest_action.html", [
              [ "ai::AcceptAllQuestsAction", "classai_1_1_accept_all_quests_action.html", [
                [ "ai::AcceptQuestAction", "classai_1_1_accept_quest_action.html", null ]
              ] ],
              [ "ai::TalkToQuestGiverAction", "classai_1_1_talk_to_quest_giver_action.html", null ]
            ] ],
            [ "ai::QuestObjectiveCompletedAction", "classai_1_1_quest_objective_completed_action.html", null ],
            [ "ai::ReadyCheckAction", "classai_1_1_ready_check_action.html", [
              [ "ai::FinishReadyCheckAction", "classai_1_1_finish_ready_check_action.html", null ]
            ] ],
            [ "ai::ReleaseSpiritAction", "classai_1_1_release_spirit_action.html", null ],
            [ "ai::RememberTaxiAction", "classai_1_1_remember_taxi_action.html", null ],
            [ "ai::RepairAllAction", "classai_1_1_repair_all_action.html", null ],
            [ "ai::ResetAiAction", "classai_1_1_reset_ai_action.html", null ],
            [ "ai::ReviveFromCorpseAction", "classai_1_1_revive_from_corpse_action.html", null ],
            [ "ai::RtiAction", "classai_1_1_rti_action.html", null ],
            [ "ai::SaveManaAction", "classai_1_1_save_mana_action.html", null ],
            [ "ai::SecurityCheckAction", "classai_1_1_security_check_action.html", null ],
            [ "ai::SpiritHealerAction", "classai_1_1_spirit_healer_action.html", null ],
            [ "ai::StatsAction", "classai_1_1_stats_action.html", null ],
            [ "ai::StayChatShortcutAction", "classai_1_1_stay_chat_shortcut_action.html", null ],
            [ "ai::TankAttackChatShortcutAction", "classai_1_1_tank_attack_chat_shortcut_action.html", null ],
            [ "ai::TaxiAction", "classai_1_1_taxi_action.html", null ],
            [ "ai::TeleportAction", "classai_1_1_teleport_action.html", null ],
            [ "ai::TellAttackersAction", "classai_1_1_tell_attackers_action.html", null ],
            [ "ai::TellCastFailedAction", "classai_1_1_tell_cast_failed_action.html", null ],
            [ "ai::TellLosAction", "classai_1_1_tell_los_action.html", null ],
            [ "ai::TellMasterAction", "classai_1_1_tell_master_action.html", null ],
            [ "ai::TellReputationAction", "classai_1_1_tell_reputation_action.html", null ],
            [ "ai::TellSpellAction", "classai_1_1_tell_spell_action.html", null ],
            [ "ai::TellTargetAction", "classai_1_1_tell_target_action.html", null ],
            [ "ai::TrainerAction", "classai_1_1_trainer_action.html", null ],
            [ "ai::UseItemAction", "classai_1_1_use_item_action.html", [
              [ "ai::DrinkAction", "classai_1_1_drink_action.html", null ],
              [ "ai::EatAction", "classai_1_1_eat_action.html", null ],
              [ "ai::UseHealingPotion", "classai_1_1_use_healing_potion.html", null ],
              [ "ai::UseManaPotion", "classai_1_1_use_mana_potion.html", null ],
              [ "ai::UseSpellItemAction", "classai_1_1_use_spell_item_action.html", null ]
            ] ]
          ] ],
          [ "ai::Multiplier", "classai_1_1_multiplier.html", [
            [ "ai::CastTimeMultiplier", "classai_1_1_cast_time_multiplier.html", null ],
            [ "ai::ConserveManaMultiplier", "classai_1_1_conserve_mana_multiplier.html", null ],
            [ "ai::PassiveMultiplier", "classai_1_1_passive_multiplier.html", [
              [ "MagePullMultiplier", "class_mage_pull_multiplier.html", null ]
            ] ],
            [ "ai::SaveManaMultiplier", "classai_1_1_save_mana_multiplier.html", null ],
            [ "ai::ThreatMultiplier", "classai_1_1_threat_multiplier.html", null ]
          ] ],
          [ "ai::Trigger", "classai_1_1_trigger.html", [
            [ "ai::AndTrigger", "classai_1_1_and_trigger.html", null ],
            [ "ai::AoeHealTrigger", "classai_1_1_aoe_heal_trigger.html", null ],
            [ "ai::AttackerCountTrigger", "classai_1_1_attacker_count_trigger.html", [
              [ "ai::AoeTrigger", "classai_1_1_aoe_trigger.html", [
                [ "ai::HighAoeTrigger", "classai_1_1_high_aoe_trigger.html", null ],
                [ "ai::LightAoeTrigger", "classai_1_1_light_aoe_trigger.html", null ],
                [ "ai::MediumAoeTrigger", "classai_1_1_medium_aoe_trigger.html", null ]
              ] ],
              [ "ai::HasAttackersTrigger", "classai_1_1_has_attackers_trigger.html", null ],
              [ "ai::MyAttackerCountTrigger", "classai_1_1_my_attacker_count_trigger.html", [
                [ "ai::MediumThreatTrigger", "classai_1_1_medium_threat_trigger.html", null ]
              ] ]
            ] ],
            [ "ai::CanLootTrigger", "classai_1_1_can_loot_trigger.html", null ],
            [ "ai::ChatCommandTrigger", "classai_1_1_chat_command_trigger.html", null ],
            [ "ai::DeadTrigger", "classai_1_1_dead_trigger.html", null ],
            [ "ai::EnemyPlayerIsAttacking", "classai_1_1_enemy_player_is_attacking.html", null ],
            [ "ai::EnemyTooCloseForMeleeTrigger", "classai_1_1_enemy_too_close_for_melee_trigger.html", null ],
            [ "ai::EnemyTooCloseForSpellTrigger", "classai_1_1_enemy_too_close_for_spell_trigger.html", null ],
            [ "ai::FarFromCurrentLootTrigger", "classai_1_1_far_from_current_loot_trigger.html", null ],
            [ "ai::FarFromMasterTrigger", "classai_1_1_far_from_master_trigger.html", [
              [ "ai::OutOfReactRangeTrigger", "classai_1_1_out_of_react_range_trigger.html", null ]
            ] ],
            [ "ai::HasAggroTrigger", "classai_1_1_has_aggro_trigger.html", null ],
            [ "ai::HasAuraTrigger", "classai_1_1_has_aura_trigger.html", [
              [ "ai::ArtOfWarTrigger", "classai_1_1_art_of_war_trigger.html", null ],
              [ "ai::BacklashTrigger", "classai_1_1_backlash_trigger.html", null ],
              [ "ai::EclipseLunarTrigger", "classai_1_1_eclipse_lunar_trigger.html", null ],
              [ "ai::EclipseSolarTrigger", "classai_1_1_eclipse_solar_trigger.html", null ],
              [ "ai::HotStreakTrigger", "classai_1_1_hot_streak_trigger.html", null ],
              [ "ai::MaelstromWeaponTrigger", "classai_1_1_maelstrom_weapon_trigger.html", null ],
              [ "ai::MissileBarrageTrigger", "classai_1_1_missile_barrage_trigger.html", null ],
              [ "ai::ShadowTranceTrigger", "classai_1_1_shadow_trance_trigger.html", null ],
              [ "ai::SwordAndBoardTrigger", "classai_1_1_sword_and_board_trigger.html", null ],
              [ "ai::VictoryRushTrigger", "classai_1_1_victory_rush_trigger.html", null ]
            ] ],
            [ "ai::HasCcTargetTrigger", "classai_1_1_has_cc_target_trigger.html", [
              [ "ai::BanishTrigger", "classai_1_1_banish_trigger.html", null ],
              [ "ai::EntanglingRootsTrigger", "classai_1_1_entangling_roots_trigger.html", null ],
              [ "ai::FearTrigger", "classai_1_1_fear_trigger.html", null ],
              [ "ai::FreezingTrapTrigger", "classai_1_1_freezing_trap_trigger.html", null ],
              [ "ai::PolymorphTrigger", "classai_1_1_polymorph_trigger.html", null ]
            ] ],
            [ "ai::HasItemForSpellTrigger", "classai_1_1_has_item_for_spell_trigger.html", null ],
            [ "ai::HasNearestAddsTrigger", "classai_1_1_has_nearest_adds_trigger.html", null ],
            [ "ai::IsBehindTargetTrigger", "classai_1_1_is_behind_target_trigger.html", null ],
            [ "ai::IsNotFacingTargetTrigger", "classai_1_1_is_not_facing_target_trigger.html", null ],
            [ "ai::IsSwimmingTrigger", "classai_1_1_is_swimming_trigger.html", null ],
            [ "ai::ItemCountTrigger", "classai_1_1_item_count_trigger.html", [
              [ "ai::WarlockConjuredItemTrigger", "classai_1_1_warlock_conjured_item_trigger.html", [
                [ "ai::HasFirestoneTrigger", "classai_1_1_has_firestone_trigger.html", null ],
                [ "ai::HasHealthstoneTrigger", "classai_1_1_has_healthstone_trigger.html", null ],
                [ "ai::HasSpellstoneTrigger", "classai_1_1_has_spellstone_trigger.html", null ]
              ] ]
            ] ],
            [ "ai::LfgProposalActiveTrigger", "classai_1_1_lfg_proposal_active_trigger.html", null ],
            [ "ai::LootAvailableTrigger", "classai_1_1_loot_available_trigger.html", null ],
            [ "ai::LoseAggroTrigger", "classai_1_1_lose_aggro_trigger.html", null ],
            [ "ai::LowManaTrigger", "classai_1_1_low_mana_trigger.html", null ],
            [ "ai::MediumManaTrigger", "classai_1_1_medium_mana_trigger.html", null ],
            [ "ai::NoDrinkTrigger", "classai_1_1_no_drink_trigger.html", null ],
            [ "ai::NoFoodTrigger", "classai_1_1_no_food_trigger.html", null ],
            [ "ai::NoMovementTrigger", "classai_1_1_no_movement_trigger.html", null ],
            [ "ai::NoPetTrigger", "classai_1_1_no_pet_trigger.html", null ],
            [ "ai::NoPossibleTargetsTrigger", "classai_1_1_no_possible_targets_trigger.html", null ],
            [ "ai::NotLeastHpTargetActiveTrigger", "classai_1_1_not_least_hp_target_active_trigger.html", null ],
            [ "ai::OutOfRangeTrigger", "classai_1_1_out_of_range_trigger.html", [
              [ "ai::EnemyOutOfMeleeTrigger", "classai_1_1_enemy_out_of_melee_trigger.html", null ],
              [ "ai::EnemyOutOfSpellRangeTrigger", "classai_1_1_enemy_out_of_spell_range_trigger.html", null ],
              [ "ai::PartyMemberToHealOutOfSpellRangeTrigger", "classai_1_1_party_member_to_heal_out_of_spell_range_trigger.html", null ]
            ] ],
            [ "ai::PartyMemberDeadTrigger", "classai_1_1_party_member_dead_trigger.html", null ],
            [ "ai::RandomTrigger", "classai_1_1_random_trigger.html", [
              [ "ai::OftenTrigger", "classai_1_1_often_trigger.html", null ],
              [ "ai::SeldomTrigger", "classai_1_1_seldom_trigger.html", null ]
            ] ],
            [ "ai::SpellTrigger", "classai_1_1_spell_trigger.html", [
              [ "ai::BuffTrigger", "classai_1_1_buff_trigger.html", [
                [ "ai::ArcaneBlastTrigger", "classai_1_1_arcane_blast_trigger.html", null ],
                [ "ai::BearFormTrigger", "classai_1_1_bear_form_trigger.html", null ],
                [ "ai::BoostTrigger", "classai_1_1_boost_trigger.html", [
                  [ "ai::BloodlustTrigger", "classai_1_1_bloodlust_trigger.html", null ],
                  [ "ai::CombustionTrigger", "classai_1_1_combustion_trigger.html", null ],
                  [ "ai::DeathWishTrigger", "classai_1_1_death_wish_trigger.html", null ],
                  [ "ai::HeroismTrigger", "classai_1_1_heroism_trigger.html", null ],
                  [ "ai::IcyVeinsTrigger", "classai_1_1_icy_veins_trigger.html", null ],
                  [ "ai::NaturesGraspTrigger", "classai_1_1_natures_grasp_trigger.html", null ],
                  [ "ai::RapidFireTrigger", "classai_1_1_rapid_fire_trigger.html", null ],
                  [ "ai::TigersFuryTrigger", "classai_1_1_tigers_fury_trigger.html", null ]
                ] ],
                [ "ai::BuffOnPartyTrigger", "classai_1_1_buff_on_party_trigger.html", [
                  [ "ai::MarkOfTheWildOnPartyTrigger", "classai_1_1_mark_of_the_wild_on_party_trigger.html", null ],
                  [ "ai::WaterBreathingOnPartyTrigger", "classai_1_1_water_breathing_on_party_trigger.html", null ],
                  [ "ai::WaterWalkingOnPartyTrigger", "classai_1_1_water_walking_on_party_trigger.html", null ]
                ] ],
                [ "ai::CatFormTrigger", "classai_1_1_cat_form_trigger.html", null ],
                [ "ai::CrusaderAuraTrigger", "classai_1_1_crusader_aura_trigger.html", null ],
                [ "ai::DebuffTrigger", "classai_1_1_debuff_trigger.html", [
                  [ "ai::BlackArrowTrigger", "classai_1_1_black_arrow_trigger.html", null ],
                  [ "ai::BloodrageDebuffTrigger", "classai_1_1_bloodrage_debuff_trigger.html", null ],
                  [ "ai::DebuffOnAttackerTrigger", "classai_1_1_debuff_on_attacker_trigger.html", [
                    [ "ai::CorruptionOnAttackerTrigger", "classai_1_1_corruption_on_attacker_trigger.html", null ],
                    [ "ai::PowerWordPainOnAttackerTrigger", "classai_1_1_power_word_pain_on_attacker_trigger.html", null ],
                    [ "ai::RendDebuffOnAttackerTrigger", "classai_1_1_rend_debuff_on_attacker_trigger.html", null ],
                    [ "ai::SerpentStingOnAttackerTrigger", "classai_1_1_serpent_sting_on_attacker_trigger.html", null ]
                  ] ],
                  [ "ai::ExposeArmorTrigger", "classai_1_1_expose_armor_trigger.html", null ],
                  [ "ai::FaerieFireFeralTrigger", "classai_1_1_faerie_fire_feral_trigger.html", null ],
                  [ "ai::FaerieFireTrigger", "classai_1_1_faerie_fire_trigger.html", null ],
                  [ "ai::FireballTrigger", "classai_1_1_fireball_trigger.html", null ],
                  [ "ai::HuntersMarkTrigger", "classai_1_1_hunters_mark_trigger.html", null ],
                  [ "ai::InsectSwarmTrigger", "classai_1_1_insect_swarm_trigger.html", null ],
                  [ "ai::LivingBombTrigger", "classai_1_1_living_bomb_trigger.html", null ],
                  [ "ai::MoonfireTrigger", "classai_1_1_moonfire_trigger.html", null ],
                  [ "ai::PyroblastTrigger", "classai_1_1_pyroblast_trigger.html", null ],
                  [ "ai::RakeTrigger", "classai_1_1_rake_trigger.html", null ],
                  [ "ai::RuptureTrigger", "classai_1_1_rupture_trigger.html", null ],
                  [ "ai::ShockTrigger", "classai_1_1_shock_trigger.html", null ],
                  [ "ai::SnareTargetTrigger", "classai_1_1_snare_target_trigger.html", [
                    [ "ai::ConcussionBlowTrigger", "classai_1_1_concussion_blow_trigger.html", null ],
                    [ "ai::FrostShockSnareTrigger", "classai_1_1_frost_shock_snare_trigger.html", null ],
                    [ "ai::HammerOfJusticeSnareTrigger", "classai_1_1_hammer_of_justice_snare_trigger.html", null ],
                    [ "ai::HamstringTrigger", "classai_1_1_hamstring_trigger.html", null ]
                  ] ]
                ] ],
                [ "ai::DemonArmorTrigger", "classai_1_1_demon_armor_trigger.html", null ],
                [ "ai::DevotionAuraTrigger", "classai_1_1_devotion_aura_trigger.html", null ],
                [ "ai::FireResistanceAuraTrigger", "classai_1_1_fire_resistance_aura_trigger.html", null ],
                [ "ai::FrostResistanceAuraTrigger", "classai_1_1_frost_resistance_aura_trigger.html", null ],
                [ "ai::HunterAspectOfTheHawkTrigger", "classai_1_1_hunter_aspect_of_the_hawk_trigger.html", null ],
                [ "ai::HunterAspectOfThePackTrigger", "classai_1_1_hunter_aspect_of_the_pack_trigger.html", null ],
                [ "ai::HunterAspectOfTheViperTrigger", "classai_1_1_hunter_aspect_of_the_viper_trigger.html", null ],
                [ "ai::HunterAspectOfTheWildTrigger", "classai_1_1_hunter_aspect_of_the_wild_trigger.html", null ],
                [ "ai::LightningShieldTrigger", "classai_1_1_lightning_shield_trigger.html", null ],
                [ "ai::MageArmorTrigger", "classai_1_1_mage_armor_trigger.html", null ],
                [ "ai::MarkOfTheWildTrigger", "classai_1_1_mark_of_the_wild_trigger.html", null ],
                [ "ai::SealTrigger", "classai_1_1_seal_trigger.html", null ],
                [ "ai::ShadowformTrigger", "classai_1_1_shadowform_trigger.html", null ],
                [ "ai::ShadowResistanceAuraTrigger", "classai_1_1_shadow_resistance_aura_trigger.html", null ],
                [ "ai::ShamanWeaponTrigger", "classai_1_1_shaman_weapon_trigger.html", null ],
                [ "ai::SliceAndDiceTrigger", "classai_1_1_slice_and_dice_trigger.html", null ],
                [ "ai::SpellstoneTrigger", "classai_1_1_spellstone_trigger.html", null ],
                [ "ai::ThornsTrigger", "classai_1_1_thorns_trigger.html", null ],
                [ "ai::TreeFormTrigger", "classai_1_1_tree_form_trigger.html", null ],
                [ "ai::TrueshotAuraTrigger", "classai_1_1_trueshot_aura_trigger.html", null ],
                [ "ai::WaterBreathingTrigger", "classai_1_1_water_breathing_trigger.html", null ],
                [ "ai::WaterShieldTrigger", "classai_1_1_water_shield_trigger.html", null ],
                [ "ai::WaterWalkingTrigger", "classai_1_1_water_walking_trigger.html", null ]
              ] ],
              [ "ai::InterruptEnemyHealerTrigger", "classai_1_1_interrupt_enemy_healer_trigger.html", [
                [ "ai::BashInterruptEnemyHealerSpellTrigger", "classai_1_1_bash_interrupt_enemy_healer_spell_trigger.html", null ],
                [ "ai::CounterspellEnemyHealerTrigger", "classai_1_1_counterspell_enemy_healer_trigger.html", null ],
                [ "ai::HammerOfJusticeEnemyHealerTrigger", "classai_1_1_hammer_of_justice_enemy_healer_trigger.html", null ],
                [ "ai::KickInterruptEnemyHealerSpellTrigger", "classai_1_1_kick_interrupt_enemy_healer_spell_trigger.html", null ],
                [ "ai::ShieldBashInterruptEnemyHealerSpellTrigger", "classai_1_1_shield_bash_interrupt_enemy_healer_spell_trigger.html", null ],
                [ "ai::WindShearInterruptEnemyHealerSpellTrigger", "classai_1_1_wind_shear_interrupt_enemy_healer_spell_trigger.html", null ]
              ] ],
              [ "ai::InterruptSpellTrigger", "classai_1_1_interrupt_spell_trigger.html", [
                [ "ai::BashInterruptSpellTrigger", "classai_1_1_bash_interrupt_spell_trigger.html", null ],
                [ "ai::CounterspellInterruptSpellTrigger", "classai_1_1_counterspell_interrupt_spell_trigger.html", null ],
                [ "ai::HammerOfJusticeInterruptSpellTrigger", "classai_1_1_hammer_of_justice_interrupt_spell_trigger.html", null ],
                [ "ai::KickInterruptSpellTrigger", "classai_1_1_kick_interrupt_spell_trigger.html", null ],
                [ "ai::ShieldBashInterruptSpellTrigger", "classai_1_1_shield_bash_interrupt_spell_trigger.html", null ],
                [ "ai::WindShearInterruptSpellTrigger", "classai_1_1_wind_shear_interrupt_spell_trigger.html", null ]
              ] ],
              [ "ai::NeedCureTrigger", "classai_1_1_need_cure_trigger.html", [
                [ "ai::CleanseCureDiseaseTrigger", "classai_1_1_cleanse_cure_disease_trigger.html", null ],
                [ "ai::CleanseCureMagicTrigger", "classai_1_1_cleanse_cure_magic_trigger.html", null ],
                [ "ai::CleanseCurePoisonTrigger", "classai_1_1_cleanse_cure_poison_trigger.html", null ],
                [ "ai::CleanseSpiritCurseTrigger", "classai_1_1_cleanse_spirit_curse_trigger.html", null ],
                [ "ai::CleanseSpiritDiseaseTrigger", "classai_1_1_cleanse_spirit_disease_trigger.html", null ],
                [ "ai::CleanseSpiritPoisonTrigger", "classai_1_1_cleanse_spirit_poison_trigger.html", null ],
                [ "ai::CureDiseaseTrigger", "classai_1_1_cure_disease_trigger.html", null ],
                [ "ai::CurePoisonTrigger", "classai_1_1_cure_poison_trigger.html", null ],
                [ "ai::DispelMagicTrigger", "classai_1_1_dispel_magic_trigger.html", null ],
                [ "ai::PartyMemberNeedCureTrigger", "classai_1_1_party_member_need_cure_trigger.html", [
                  [ "ai::CleanseCurePartyMemberDiseaseTrigger", "classai_1_1_cleanse_cure_party_member_disease_trigger.html", null ],
                  [ "ai::CleanseCurePartyMemberMagicTrigger", "classai_1_1_cleanse_cure_party_member_magic_trigger.html", null ],
                  [ "ai::CleanseCurePartyMemberPoisonTrigger", "classai_1_1_cleanse_cure_party_member_poison_trigger.html", null ],
                  [ "ai::DispelMagicPartyMemberTrigger", "classai_1_1_dispel_magic_party_member_trigger.html", null ],
                  [ "ai::PartyMemberCleanseSpiritCurseTrigger", "classai_1_1_party_member_cleanse_spirit_curse_trigger.html", null ],
                  [ "ai::PartyMemberCleanseSpiritDiseaseTrigger", "classai_1_1_party_member_cleanse_spirit_disease_trigger.html", null ],
                  [ "ai::PartyMemberCleanseSpiritPoisonTrigger", "classai_1_1_party_member_cleanse_spirit_poison_trigger.html", null ],
                  [ "ai::PartyMemberCureDiseaseTrigger", "classai_1_1_party_member_cure_disease_trigger.html", null ],
                  [ "ai::PartyMemberCurePoisonTrigger", "classai_1_1_party_member_cure_poison_trigger.html", null ],
                  [ "ai::PartyMemberRemoveCurseTrigger", "classai_1_1_party_member_remove_curse_trigger.html", null ]
                ] ],
                [ "ai::RemoveCurseTrigger", "classai_1_1_remove_curse_trigger.html", null ],
                [ "ai::TargetAuraDispelTrigger", "classai_1_1_target_aura_dispel_trigger.html", [
                  [ "ai::PurgeTrigger", "classai_1_1_purge_trigger.html", null ],
                  [ "ai::SpellstealTrigger", "classai_1_1_spellsteal_trigger.html", null ]
                ] ]
              ] ],
              [ "ai::SpellCanBeCastTrigger", "classai_1_1_spell_can_be_cast_trigger.html", [
                [ "ai::RevengeAvailableTrigger", "classai_1_1_revenge_available_trigger.html", null ]
              ] ]
            ] ],
            [ "ai::StatAvailable", "classai_1_1_stat_available.html", [
              [ "ai::ComboPointsAvailableTrigger", "classai_1_1_combo_points_available_trigger.html", null ],
              [ "ai::EnergyAvailable", "classai_1_1_energy_available.html", [
                [ "ai::HighEnergyAvailableTrigger", "classai_1_1_high_energy_available_trigger.html", null ],
                [ "ai::LightEnergyAvailableTrigger", "classai_1_1_light_energy_available_trigger.html", null ],
                [ "ai::MediumEnergyAvailableTrigger", "classai_1_1_medium_energy_available_trigger.html", null ]
              ] ],
              [ "ai::RageAvailable", "classai_1_1_rage_available.html", [
                [ "ai::HighRageAvailableTrigger", "classai_1_1_high_rage_available_trigger.html", null ],
                [ "ai::LightRageAvailableTrigger", "classai_1_1_light_rage_available_trigger.html", null ],
                [ "ai::MediumRageAvailableTrigger", "classai_1_1_medium_rage_available_trigger.html", null ]
              ] ]
            ] ],
            [ "ai::TargetChangedTrigger", "classai_1_1_target_changed_trigger.html", null ],
            [ "ai::TargetInSightTrigger", "classai_1_1_target_in_sight_trigger.html", null ],
            [ "ai::TimerTrigger", "classai_1_1_timer_trigger.html", null ],
            [ "ai::TotemTrigger", "classai_1_1_totem_trigger.html", [
              [ "ai::FlametongueTotemTrigger", "classai_1_1_flametongue_totem_trigger.html", null ],
              [ "ai::MagmaTotemTrigger", "classai_1_1_magma_totem_trigger.html", null ],
              [ "ai::ManaSpringTotemTrigger", "classai_1_1_mana_spring_totem_trigger.html", null ],
              [ "ai::SearingTotemTrigger", "classai_1_1_searing_totem_trigger.html", null ],
              [ "ai::StrengthOfEarthTotemTrigger", "classai_1_1_strength_of_earth_totem_trigger.html", null ],
              [ "ai::WindfuryTotemTrigger", "classai_1_1_windfury_totem_trigger.html", null ]
            ] ],
            [ "ai::ValueInRangeTrigger", "classai_1_1_value_in_range_trigger.html", [
              [ "ai::HealthInRangeTrigger", "classai_1_1_health_in_range_trigger.html", [
                [ "ai::LowHealthTrigger", "classai_1_1_low_health_trigger.html", [
                  [ "ai::AlmostFullHealthTrigger", "classai_1_1_almost_full_health_trigger.html", null ],
                  [ "ai::CriticalHealthTrigger", "classai_1_1_critical_health_trigger.html", null ],
                  [ "ai::MediumHealthTrigger", "classai_1_1_medium_health_trigger.html", null ]
                ] ],
                [ "ai::PartyMemberLowHealthTrigger", "classai_1_1_party_member_low_health_trigger.html", [
                  [ "ai::PartyMemberAlmostFullHealthTrigger", "classai_1_1_party_member_almost_full_health_trigger.html", null ],
                  [ "ai::PartyMemberCriticalHealthTrigger", "classai_1_1_party_member_critical_health_trigger.html", null ],
                  [ "ai::PartyMemberMediumHealthTrigger", "classai_1_1_party_member_medium_health_trigger.html", null ]
                ] ],
                [ "ai::TargetLowHealthTrigger", "classai_1_1_target_low_health_trigger.html", [
                  [ "ai::TargetCriticalHealthTrigger", "classai_1_1_target_critical_health_trigger.html", null ]
                ] ]
              ] ]
            ] ],
            [ "ai::WithinAreaTrigger", "classai_1_1_within_area_trigger.html", null ],
            [ "ai::WorldPacketTrigger", "classai_1_1_world_packet_trigger.html", null ]
          ] ],
          [ "ai::UntypedValue", "classai_1_1_untyped_value.html", [
            [ "ai::CalculatedValue< T >", "classai_1_1_calculated_value.html", null ],
            [ "ai::ManualSetValue< T >", "classai_1_1_manual_set_value.html", null ],
            [ "ai::CalculatedValue< bool >", "classai_1_1_calculated_value.html", [
              [ "ai::BoolCalculatedValue", "classai_1_1_bool_calculated_value.html", [
                [ "ai::CanLootValue", "classai_1_1_can_loot_value.html", null ],
                [ "ai::HasAggroValue", "classai_1_1_has_aggro_value.html", null ],
                [ "ai::HasAvailableLootValue", "classai_1_1_has_available_loot_value.html", null ],
                [ "ai::HasManaValue", "classai_1_1_has_mana_value.html", null ],
                [ "ai::HasTotemValue", "classai_1_1_has_totem_value.html", null ],
                [ "ai::InvalidTargetValue", "classai_1_1_invalid_target_value.html", null ],
                [ "ai::IsBehindValue", "classai_1_1_is_behind_value.html", null ],
                [ "ai::IsDeadValue", "classai_1_1_is_dead_value.html", null ],
                [ "ai::IsFacingValue", "classai_1_1_is_facing_value.html", null ],
                [ "ai::IsInCombatValue", "classai_1_1_is_in_combat_value.html", null ],
                [ "ai::IsMountedValue", "classai_1_1_is_mounted_value.html", null ],
                [ "ai::IsMovingValue", "classai_1_1_is_moving_value.html", null ],
                [ "ai::IsSwimmingValue", "classai_1_1_is_swimming_value.html", null ],
                [ "ai::SpellCastUsefulValue", "classai_1_1_spell_cast_useful_value.html", null ]
              ] ]
            ] ],
            [ "ai::CalculatedValue< float >", "classai_1_1_calculated_value.html", [
              [ "ai::FloatCalculatedValue", "classai_1_1_float_calculated_value.html", [
                [ "ai::DistanceValue", "classai_1_1_distance_value.html", null ]
              ] ]
            ] ],
            [ "ai::CalculatedValue< Item *>", "classai_1_1_calculated_value.html", [
              [ "ai::ItemForSpellValue", "classai_1_1_item_for_spell_value.html", null ]
            ] ],
            [ "ai::CalculatedValue< ItemUsage >", "classai_1_1_calculated_value.html", [
              [ "ai::ItemUsageValue", "classai_1_1_item_usage_value.html", null ]
            ] ],
            [ "ai::CalculatedValue< list< Item *> >", "classai_1_1_calculated_value.html", [
              [ "ai::InventoryItemValue", "classai_1_1_inventory_item_value.html", null ]
            ] ],
            [ "ai::CalculatedValue< list< ObjectGuid > >", "classai_1_1_calculated_value.html", [
              [ "ai::ObjectGuidListCalculatedValue", "classai_1_1_object_guid_list_calculated_value.html", [
                [ "ai::AttackersValue", "classai_1_1_attackers_value.html", null ],
                [ "ai::NearestGameObjects", "classai_1_1_nearest_game_objects.html", null ],
                [ "ai::NearestUnitsValue", "classai_1_1_nearest_units_value.html", [
                  [ "ai::NearestCorpsesValue", "classai_1_1_nearest_corpses_value.html", null ],
                  [ "ai::NearestNpcsValue", "classai_1_1_nearest_npcs_value.html", null ],
                  [ "ai::PossibleTargetsValue", "classai_1_1_possible_targets_value.html", [
                    [ "ai::NearestAdsValue", "classai_1_1_nearest_ads_value.html", null ]
                  ] ]
                ] ]
              ] ]
            ] ],
            [ "ai::CalculatedValue< uint32 >", "classai_1_1_calculated_value.html", [
              [ "ai::SpellIdValue", "classai_1_1_spell_id_value.html", null ],
              [ "ai::Uint32CalculatedValue", "classai_1_1_uint32_calculated_value.html", null ]
            ] ],
            [ "ai::CalculatedValue< uint8 >", "classai_1_1_calculated_value.html", [
              [ "ai::Uint8CalculatedValue", "classai_1_1_uint8_calculated_value.html", [
                [ "ai::AoeHealValue", "classai_1_1_aoe_heal_value.html", null ],
                [ "ai::AttackerCountValue", "classai_1_1_attacker_count_value.html", null ],
                [ "ai::BagSpaceValue", "classai_1_1_bag_space_value.html", null ],
                [ "ai::BalancePercentValue", "classai_1_1_balance_percent_value.html", null ],
                [ "ai::ComboPointsValue", "classai_1_1_combo_points_value.html", null ],
                [ "ai::EnergyValue", "classai_1_1_energy_value.html", null ],
                [ "ai::HealthValue", "classai_1_1_health_value.html", null ],
                [ "ai::ItemCountValue", "classai_1_1_item_count_value.html", null ],
                [ "ai::ManaValue", "classai_1_1_mana_value.html", null ],
                [ "ai::MyAttackerCountValue", "classai_1_1_my_attacker_count_value.html", null ],
                [ "ai::RageValue", "classai_1_1_rage_value.html", null ],
                [ "ai::ThreatValue", "classai_1_1_threat_value.html", null ]
              ] ]
            ] ],
            [ "ai::CalculatedValue< Unit *>", "classai_1_1_calculated_value.html", [
              [ "ai::UnitCalculatedValue", "classai_1_1_unit_calculated_value.html", [
                [ "ai::AttackerWithoutAuraTargetValue", "classai_1_1_attacker_without_aura_target_value.html", null ],
                [ "ai::EnemyHealerTargetValue", "classai_1_1_enemy_healer_target_value.html", null ],
                [ "ai::LineTargetValue", "classai_1_1_line_target_value.html", null ],
                [ "ai::MasterTargetValue", "classai_1_1_master_target_value.html", null ],
                [ "ai::PartyMemberValue", "classai_1_1_party_member_value.html", [
                  [ "ai::PartyMemberToDispel", "classai_1_1_party_member_to_dispel.html", null ],
                  [ "ai::PartyMemberToHeal", "classai_1_1_party_member_to_heal.html", null ],
                  [ "ai::PartyMemberToResurrect", "classai_1_1_party_member_to_resurrect.html", null ],
                  [ "ai::PartyMemberWithoutAuraValue", "classai_1_1_party_member_without_aura_value.html", null ]
                ] ],
                [ "ai::PetTargetValue", "classai_1_1_pet_target_value.html", null ],
                [ "ai::RtiTargetValue", "classai_1_1_rti_target_value.html", null ],
                [ "ai::SelfTargetValue", "classai_1_1_self_target_value.html", null ],
                [ "ai::TargetValue", "classai_1_1_target_value.html", [
                  [ "ai::CcTargetValue", "classai_1_1_cc_target_value.html", null ],
                  [ "ai::CurrentCcTargetValue", "classai_1_1_current_cc_target_value.html", null ],
                  [ "ai::DpsTargetValue", "classai_1_1_dps_target_value.html", null ],
                  [ "ai::DuelTargetValue", "classai_1_1_duel_target_value.html", null ],
                  [ "ai::EnemyPlayerValue", "classai_1_1_enemy_player_value.html", null ],
                  [ "ai::GrindTargetValue", "classai_1_1_grind_target_value.html", null ],
                  [ "ai::LeastHpTargetValue", "classai_1_1_least_hp_target_value.html", null ],
                  [ "ai::TankTargetValue", "classai_1_1_tank_target_value.html", null ]
                ] ]
              ] ]
            ] ],
            [ "ai::ManualSetValue< ChatMsg >", "classai_1_1_manual_set_value.html", [
              [ "ai::ChatValue", "classai_1_1_chat_value.html", null ]
            ] ],
            [ "ai::ManualSetValue< double >", "classai_1_1_manual_set_value.html", [
              [ "ai::ManaSaveLevelValue", "classai_1_1_mana_save_level_value.html", null ]
            ] ],
            [ "ai::ManualSetValue< LastMovement &>", "classai_1_1_manual_set_value.html", [
              [ "ai::LastMovementValue", "classai_1_1_last_movement_value.html", null ]
            ] ],
            [ "ai::ManualSetValue< LastSpellCast &>", "classai_1_1_manual_set_value.html", [
              [ "ai::LastSpellCastValue", "classai_1_1_last_spell_cast_value.html", null ]
            ] ],
            [ "ai::ManualSetValue< LogLevel >", "classai_1_1_manual_set_value.html", [
              [ "ai::LogLevelValue", "classai_1_1_log_level_value.html", null ]
            ] ],
            [ "ai::ManualSetValue< LootObject >", "classai_1_1_manual_set_value.html", [
              [ "ai::LootTargetValue", "classai_1_1_loot_target_value.html", null ]
            ] ],
            [ "ai::ManualSetValue< LootObjectStack *>", "classai_1_1_manual_set_value.html", [
              [ "ai::AvailableLootValue", "classai_1_1_available_loot_value.html", null ]
            ] ],
            [ "ai::ManualSetValue< LootStrategy >", "classai_1_1_manual_set_value.html", [
              [ "ai::LootStrategyValue", "classai_1_1_loot_strategy_value.html", null ]
            ] ],
            [ "ai::ManualSetValue< Position &>", "classai_1_1_manual_set_value.html", [
              [ "ai::PositionValue", "classai_1_1_position_value.html", null ]
            ] ],
            [ "ai::ManualSetValue< set< uint32 > &>", "classai_1_1_manual_set_value.html", [
              [ "ai::AlwaysLootListValue", "classai_1_1_always_loot_list_value.html", null ]
            ] ],
            [ "ai::ManualSetValue< string >", "classai_1_1_manual_set_value.html", [
              [ "ai::RtiValue", "classai_1_1_rti_value.html", null ]
            ] ],
            [ "ai::ManualSetValue< time_t >", "classai_1_1_manual_set_value.html", [
              [ "ai::LastSpellCastTimeValue", "classai_1_1_last_spell_cast_time_value.html", null ]
            ] ],
            [ "ai::ManualSetValue< uint32 >", "classai_1_1_manual_set_value.html", [
              [ "ai::LfgProposalValue", "classai_1_1_lfg_proposal_value.html", null ]
            ] ],
            [ "ai::ManualSetValue< Unit *>", "classai_1_1_manual_set_value.html", [
              [ "ai::UnitManualSetValue", "classai_1_1_unit_manual_set_value.html", [
                [ "ai::CurrentTargetValue", "classai_1_1_current_target_value.html", null ]
              ] ]
            ] ]
          ] ]
        ] ]
      ] ],
      [ "ai::AiObjectContext", "classai_1_1_ai_object_context.html", [
        [ "ai::DruidAiObjectContext", "classai_1_1_druid_ai_object_context.html", null ],
        [ "ai::HunterAiObjectContext", "classai_1_1_hunter_ai_object_context.html", null ],
        [ "ai::MageAiObjectContext", "classai_1_1_mage_ai_object_context.html", null ],
        [ "ai::PaladinAiObjectContext", "classai_1_1_paladin_ai_object_context.html", null ],
        [ "ai::PriestAiObjectContext", "classai_1_1_priest_ai_object_context.html", null ],
        [ "ai::RogueAiObjectContext", "classai_1_1_rogue_ai_object_context.html", null ],
        [ "ai::ShamanAiObjectContext", "classai_1_1_shaman_ai_object_context.html", null ],
        [ "ai::WarlockAiObjectContext", "classai_1_1_warlock_ai_object_context.html", null ],
        [ "ai::WarriorAiObjectContext", "classai_1_1_warrior_ai_object_context.html", null ]
      ] ],
      [ "ai::ChatFilter", "classai_1_1_chat_filter.html", [
        [ "ai::CompositeChatFilter", "classai_1_1_composite_chat_filter.html", null ],
        [ "ClassChatFilter", "class_class_chat_filter.html", null ],
        [ "CombatTypeChatFilter", "class_combat_type_chat_filter.html", null ],
        [ "LevelChatFilter", "class_level_chat_filter.html", null ],
        [ "RtiChatFilter", "class_rti_chat_filter.html", null ],
        [ "StrategyChatFilter", "class_strategy_chat_filter.html", null ]
      ] ],
      [ "ai::ChatHelper", "classai_1_1_chat_helper.html", null ],
      [ "ai::Engine", "classai_1_1_engine.html", null ],
      [ "ai::Strategy", "classai_1_1_strategy.html", [
        [ "ai::CastTimeStrategy", "classai_1_1_cast_time_strategy.html", null ],
        [ "ai::CombatStrategy", "classai_1_1_combat_strategy.html", [
          [ "ai::CasterDruidAoeStrategy", "classai_1_1_caster_druid_aoe_strategy.html", null ],
          [ "ai::CasterDruidDebuffStrategy", "classai_1_1_caster_druid_debuff_strategy.html", null ],
          [ "ai::CatAoeDruidStrategy", "classai_1_1_cat_aoe_druid_strategy.html", null ],
          [ "ai::DpsAoeHunterStrategy", "classai_1_1_dps_aoe_hunter_strategy.html", null ],
          [ "ai::DpsAoeWarlockStrategy", "classai_1_1_dps_aoe_warlock_strategy.html", null ],
          [ "ai::DpsHunterDebuffStrategy", "classai_1_1_dps_hunter_debuff_strategy.html", null ],
          [ "ai::DpsWarlockDebuffStrategy", "classai_1_1_dps_warlock_debuff_strategy.html", null ],
          [ "ai::DpsWarrirorAoeStrategy", "classai_1_1_dps_warriror_aoe_strategy.html", null ],
          [ "ai::FireMageAoeStrategy", "classai_1_1_fire_mage_aoe_strategy.html", null ],
          [ "ai::FrostMageAoeStrategy", "classai_1_1_frost_mage_aoe_strategy.html", null ],
          [ "ai::GenericDruidStrategy", "classai_1_1_generic_druid_strategy.html", [
            [ "ai::CasterDruidStrategy", "classai_1_1_caster_druid_strategy.html", null ],
            [ "ai::FeralDruidStrategy", "classai_1_1_feral_druid_strategy.html", [
              [ "ai::BearTankDruidStrategy", "classai_1_1_bear_tank_druid_strategy.html", null ],
              [ "ai::CatDpsDruidStrategy", "classai_1_1_cat_dps_druid_strategy.html", null ]
            ] ],
            [ "ai::HealDruidStrategy", "classai_1_1_heal_druid_strategy.html", null ]
          ] ],
          [ "ai::GenericPriestStrategy", "classai_1_1_generic_priest_strategy.html", [
            [ "ai::HealPriestStrategy", "classai_1_1_heal_priest_strategy.html", [
              [ "ai::HolyPriestStrategy", "classai_1_1_holy_priest_strategy.html", null ]
            ] ],
            [ "ai::ShadowPriestStrategy", "classai_1_1_shadow_priest_strategy.html", null ]
          ] ],
          [ "ai::GenericShamanStrategy", "classai_1_1_generic_shaman_strategy.html", [
            [ "ai::CasterShamanStrategy", "classai_1_1_caster_shaman_strategy.html", null ],
            [ "ai::HealShamanStrategy", "classai_1_1_heal_shaman_strategy.html", null ],
            [ "ai::MeleeShamanStrategy", "classai_1_1_melee_shaman_strategy.html", null ],
            [ "ai::TotemsShamanStrategy", "classai_1_1_totems_shaman_strategy.html", null ]
          ] ],
          [ "ai::MeleeAoeShamanStrategy", "classai_1_1_melee_aoe_shaman_strategy.html", [
            [ "ai::CasterAoeShamanStrategy", "classai_1_1_caster_aoe_shaman_strategy.html", null ]
          ] ],
          [ "ai::MeleeCombatStrategy", "classai_1_1_melee_combat_strategy.html", [
            [ "ai::DpsRogueStrategy", "classai_1_1_dps_rogue_strategy.html", null ],
            [ "ai::GenericPaladinStrategy", "classai_1_1_generic_paladin_strategy.html", [
              [ "ai::DpsPaladinStrategy", "classai_1_1_dps_paladin_strategy.html", null ],
              [ "ai::TankPaladinStrategy", "classai_1_1_tank_paladin_strategy.html", null ]
            ] ],
            [ "ai::GenericWarriorStrategy", "classai_1_1_generic_warrior_strategy.html", [
              [ "ai::DpsWarriorStrategy", "classai_1_1_dps_warrior_strategy.html", null ],
              [ "ai::TankWarriorStrategy", "classai_1_1_tank_warrior_strategy.html", null ]
            ] ]
          ] ],
          [ "ai::RangedCombatStrategy", "classai_1_1_ranged_combat_strategy.html", [
            [ "ai::GenericHunterStrategy", "classai_1_1_generic_hunter_strategy.html", [
              [ "ai::DpsHunterStrategy", "classai_1_1_dps_hunter_strategy.html", null ]
            ] ],
            [ "ai::GenericMageStrategy", "classai_1_1_generic_mage_strategy.html", [
              [ "ai::ArcaneMageStrategy", "classai_1_1_arcane_mage_strategy.html", null ],
              [ "ai::FireMageStrategy", "classai_1_1_fire_mage_strategy.html", null ],
              [ "ai::FrostMageStrategy", "classai_1_1_frost_mage_strategy.html", null ]
            ] ],
            [ "ai::GenericWarlockStrategy", "classai_1_1_generic_warlock_strategy.html", [
              [ "ai::DpsWarlockStrategy", "classai_1_1_dps_warlock_strategy.html", null ],
              [ "ai::TankWarlockStrategy", "classai_1_1_tank_warlock_strategy.html", null ]
            ] ],
            [ "ai::PullStrategy", "classai_1_1_pull_strategy.html", null ]
          ] ],
          [ "ai::ShadowPriestAoeStrategy", "classai_1_1_shadow_priest_aoe_strategy.html", null ],
          [ "ai::ShadowPriestDebuffStrategy", "classai_1_1_shadow_priest_debuff_strategy.html", null ]
        ] ],
        [ "ai::ConserveManaStrategy", "classai_1_1_conserve_mana_strategy.html", null ],
        [ "ai::EmoteStrategy", "classai_1_1_emote_strategy.html", null ],
        [ "ai::FleeFromAddsStrategy", "classai_1_1_flee_from_adds_strategy.html", null ],
        [ "ai::FleeStrategy", "classai_1_1_flee_strategy.html", null ],
        [ "ai::GatherStrategy", "classai_1_1_gather_strategy.html", null ],
        [ "ai::KiteStrategy", "classai_1_1_kite_strategy.html", null ],
        [ "ai::LootNonCombatStrategy", "classai_1_1_loot_non_combat_strategy.html", null ],
        [ "ai::MageBuffDpsStrategy", "classai_1_1_mage_buff_dps_strategy.html", null ],
        [ "ai::MageBuffManaStrategy", "classai_1_1_mage_buff_mana_strategy.html", null ],
        [ "ai::NonCombatStrategy", "classai_1_1_non_combat_strategy.html", [
          [ "ai::AttackEnemyPlayersStrategy", "classai_1_1_attack_enemy_players_strategy.html", null ],
          [ "ai::AttackRtiStrategy", "classai_1_1_attack_rti_strategy.html", null ],
          [ "ai::AttackWeakStrategy", "classai_1_1_attack_weak_strategy.html", null ],
          [ "ai::DpsAoeStrategy", "classai_1_1_dps_aoe_strategy.html", null ],
          [ "ai::DpsAssistStrategy", "classai_1_1_dps_assist_strategy.html", null ],
          [ "ai::FollowLineStrategy", "classai_1_1_follow_line_strategy.html", null ],
          [ "ai::FollowMasterRandomStrategy", "classai_1_1_follow_master_random_strategy.html", null ],
          [ "ai::FollowMasterStrategy", "classai_1_1_follow_master_strategy.html", null ],
          [ "ai::GenericDruidNonCombatStrategy", "classai_1_1_generic_druid_non_combat_strategy.html", null ],
          [ "ai::GenericHunterNonCombatStrategy", "classai_1_1_generic_hunter_non_combat_strategy.html", null ],
          [ "ai::GenericMageNonCombatStrategy", "classai_1_1_generic_mage_non_combat_strategy.html", null ],
          [ "ai::GenericPaladinNonCombatStrategy", "classai_1_1_generic_paladin_non_combat_strategy.html", null ],
          [ "ai::GenericRogueNonCombatStrategy", "classai_1_1_generic_rogue_non_combat_strategy.html", null ],
          [ "ai::GenericWarlockNonCombatStrategy", "classai_1_1_generic_warlock_non_combat_strategy.html", null ],
          [ "ai::GenericWarriorNonCombatStrategy", "classai_1_1_generic_warrior_non_combat_strategy.html", null ],
          [ "ai::GrindingStrategy", "classai_1_1_grinding_strategy.html", null ],
          [ "ai::GuardStrategy", "classai_1_1_guard_strategy.html", null ],
          [ "ai::HunterBuffDpsStrategy", "classai_1_1_hunter_buff_dps_strategy.html", null ],
          [ "ai::HunterBuffManaStrategy", "classai_1_1_hunter_buff_mana_strategy.html", null ],
          [ "ai::HunterBuffSpeedStrategy", "classai_1_1_hunter_buff_speed_strategy.html", null ],
          [ "ai::HunterNatureResistanceStrategy", "classai_1_1_hunter_nature_resistance_strategy.html", null ],
          [ "ai::MoveRandomStrategy", "classai_1_1_move_random_strategy.html", null ],
          [ "ai::PriestNonCombatStrategy", "classai_1_1_priest_non_combat_strategy.html", null ],
          [ "ai::RunawayStrategy", "classai_1_1_runaway_strategy.html", null ],
          [ "ai::ShamanNonCombatStrategy", "classai_1_1_shaman_non_combat_strategy.html", null ],
          [ "ai::StayCircleStrategy", "classai_1_1_stay_circle_strategy.html", null ],
          [ "ai::StayCombatStrategy", "classai_1_1_stay_combat_strategy.html", null ],
          [ "ai::StayLineStrategy", "classai_1_1_stay_line_strategy.html", null ],
          [ "ai::StayStrategy", "classai_1_1_stay_strategy.html", null ],
          [ "ai::TankAoeStrategy", "classai_1_1_tank_aoe_strategy.html", null ],
          [ "ai::TankAssistStrategy", "classai_1_1_tank_assist_strategy.html", null ]
        ] ],
        [ "ai::PaladinBuffArmorStrategy", "classai_1_1_paladin_buff_armor_strategy.html", null ],
        [ "ai::PaladinBuffDpsStrategy", "classai_1_1_paladin_buff_dps_strategy.html", null ],
        [ "ai::PaladinBuffHealthStrategy", "classai_1_1_paladin_buff_health_strategy.html", null ],
        [ "ai::PaladinBuffManaStrategy", "classai_1_1_paladin_buff_mana_strategy.html", null ],
        [ "ai::PaladinBuffSpeedStrategy", "classai_1_1_paladin_buff_speed_strategy.html", null ],
        [ "ai::PaladinFireResistanceStrategy", "classai_1_1_paladin_fire_resistance_strategy.html", null ],
        [ "ai::PaladinFrostResistanceStrategy", "classai_1_1_paladin_frost_resistance_strategy.html", null ],
        [ "ai::PaladinShadowResistanceStrategy", "classai_1_1_paladin_shadow_resistance_strategy.html", null ],
        [ "ai::PassiveStrategy", "classai_1_1_passive_strategy.html", null ],
        [ "ai::PassTroughStrategy", "classai_1_1_pass_trough_strategy.html", [
          [ "ai::ChatCommandHandlerStrategy", "classai_1_1_chat_command_handler_strategy.html", null ],
          [ "ai::DeadStrategy", "classai_1_1_dead_strategy.html", null ],
          [ "ai::DuelStrategy", "classai_1_1_duel_strategy.html", null ],
          [ "ai::QuestStrategy", "classai_1_1_quest_strategy.html", [
            [ "ai::AcceptAllQuestsStrategy", "classai_1_1_accept_all_quests_strategy.html", null ],
            [ "ai::DefaultQuestStrategy", "classai_1_1_default_quest_strategy.html", null ]
          ] ],
          [ "ai::ReadyCheckStrategy", "classai_1_1_ready_check_strategy.html", null ],
          [ "ai::WorldPacketHandlerStrategy", "classai_1_1_world_packet_handler_strategy.html", null ]
        ] ],
        [ "ai::RacialsStrategy", "classai_1_1_racials_strategy.html", null ],
        [ "ai::ShamanBuffDpsStrategy", "classai_1_1_shaman_buff_dps_strategy.html", null ],
        [ "ai::ShamanBuffManaStrategy", "classai_1_1_shaman_buff_mana_strategy.html", null ],
        [ "ai::TellTargetStrategy", "classai_1_1_tell_target_strategy.html", null ],
        [ "ai::ThreatStrategy", "classai_1_1_threat_strategy.html", null ],
        [ "ai::UseFoodStrategy", "classai_1_1_use_food_strategy.html", null ],
        [ "ai::UsePotionsStrategy", "classai_1_1_use_potions_strategy.html", null ]
      ] ],
      [ "PartyMemberToDispelPredicate", "class_party_member_to_dispel_predicate.html", null ],
      [ "PlayerWithoutAuraPredicate", "class_player_without_aura_predicate.html", null ]
    ] ],
    [ "PlayerbotAIBase", "class_playerbot_a_i_base.html", [
      [ "PlayerbotAI", "class_playerbot_a_i.html", null ],
      [ "PlayerbotHolder", "class_playerbot_holder.html", [
        [ "PlayerbotMgr", "class_playerbot_mgr.html", null ],
        [ "RandomPlayerbotMgr", "class_random_playerbot_mgr.html", null ]
      ] ]
    ] ],
    [ "PlayerbotAIConfig", "class_playerbot_a_i_config.html", null ],
    [ "PlayerbotSecurity", "class_playerbot_security.html", null ],
    [ "PlayerClassInfo", "struct_player_class_info.html", null ],
    [ "PlayerClassLevelInfo", "struct_player_class_level_info.html", null ],
    [ "PlayerCondition", "class_player_condition.html", null ],
    [ "PlayerCreateInfoAction", "struct_player_create_info_action.html", null ],
    [ "PlayerCreateInfoItem", "struct_player_create_info_item.html", null ],
    [ "PlayerDump", "class_player_dump.html", [
      [ "PlayerDumpReader", "class_player_dump_reader.html", null ],
      [ "PlayerDumpWriter", "class_player_dump_writer.html", null ]
    ] ],
    [ "PlayerInfo", "struct_player_info.html", null ],
    [ "PlayerLevelInfo", "struct_player_level_info.html", null ],
    [ "MaNGOS::PlayerListSearcher< Check >", "struct_ma_n_g_o_s_1_1_player_list_searcher.html", null ],
    [ "PlayerLogBase", "struct_player_log_base.html", [
      [ "PlayerLogDamage", "struct_player_log_damage.html", null ],
      [ "PlayerLogKilling", "struct_player_log_killing.html", null ],
      [ "PlayerLogLooting", "struct_player_log_looting.html", null ],
      [ "PlayerLogPosition", "struct_player_log_position.html", [
        [ "PlayerLogProgress", "struct_player_log_progress.html", null ]
      ] ],
      [ "PlayerLogTrading", "struct_player_log_trading.html", null ]
    ] ],
    [ "PlayerLogger", "class_player_logger.html", null ],
    [ "PlayerMenu", "class_player_menu.html", null ],
    [ "PlayerQueueInfo", "struct_player_queue_info.html", null ],
    [ "MaNGOS::PlayerRelocationNotifier", "struct_ma_n_g_o_s_1_1_player_relocation_notifier.html", null ],
    [ "MaNGOS::PlayerSearcher< Check >", "struct_ma_n_g_o_s_1_1_player_searcher.html", null ],
    [ "PlayerSocial", "class_player_social.html", null ],
    [ "PlayerSpell", "struct_player_spell.html", null ],
    [ "PlayerTaxi", "class_player_taxi.html", null ],
    [ "MaNGOS::PlayerWorker< Do >", "struct_ma_n_g_o_s_1_1_player_worker.html", null ],
    [ "PointOfInterest", "struct_point_of_interest.html", null ],
    [ "PointOfInterestLocale", "struct_point_of_interest_locale.html", null ],
    [ "Pool", "class_pool.html", null ],
    [ "PoolGroup< T >", "class_pool_group.html", null ],
    [ "PoolManager", "class_pool_manager.html", null ],
    [ "PoolMapChecker", "struct_pool_map_checker.html", null ],
    [ "PoolObject", "struct_pool_object.html", null ],
    [ "PoolTemplateData", "struct_pool_template_data.html", null ],
    [ "Position", "struct_position.html", null ],
    [ "ai::Position", "classai_1_1_position.html", null ],
    [ "PositionTrait< GameObjectModel >", "struct_position_trait_3_01_game_object_model_01_4.html", null ],
    [ "ahbot::PricingStrategy", "classahbot_1_1_pricing_strategy.html", [
      [ "ahbot::BuyOnlyRarePricingStrategy", "classahbot_1_1_buy_only_rare_pricing_strategy.html", null ]
    ] ],
    [ "ahbot::PricingStrategyFactory", "classahbot_1_1_pricing_strategy_factory.html", null ],
    [ "ProcTriggeredData", "struct_proc_triggered_data.html", null ],
    [ "PvPInfo", "struct_pv_p_info.html", null ],
    [ "QEmote", "struct_q_emote.html", null ],
    [ "ai::Qualified", "classai_1_1_qualified.html", [
      [ "ai::AoeHealValue", "classai_1_1_aoe_heal_value.html", null ],
      [ "ai::AttackerCountValue", "classai_1_1_attacker_count_value.html", null ],
      [ "ai::AttackerWithoutAuraTargetValue", "classai_1_1_attacker_without_aura_target_value.html", null ],
      [ "ai::BalancePercentValue", "classai_1_1_balance_percent_value.html", null ],
      [ "ai::CcTargetValue", "classai_1_1_cc_target_value.html", null ],
      [ "ai::ComboPointsValue", "classai_1_1_combo_points_value.html", null ],
      [ "ai::CurrentCcTargetValue", "classai_1_1_current_cc_target_value.html", null ],
      [ "ai::DistanceValue", "classai_1_1_distance_value.html", null ],
      [ "ai::EnemyHealerTargetValue", "classai_1_1_enemy_healer_target_value.html", null ],
      [ "ai::EnergyValue", "classai_1_1_energy_value.html", null ],
      [ "ai::HasAggroValue", "classai_1_1_has_aggro_value.html", null ],
      [ "ai::HasManaValue", "classai_1_1_has_mana_value.html", null ],
      [ "ai::HasTotemValue", "classai_1_1_has_totem_value.html", null ],
      [ "ai::HealthValue", "classai_1_1_health_value.html", null ],
      [ "ai::InvalidTargetValue", "classai_1_1_invalid_target_value.html", null ],
      [ "ai::InventoryItemValue", "classai_1_1_inventory_item_value.html", null ],
      [ "ai::IsBehindValue", "classai_1_1_is_behind_value.html", null ],
      [ "ai::IsDeadValue", "classai_1_1_is_dead_value.html", null ],
      [ "ai::IsFacingValue", "classai_1_1_is_facing_value.html", null ],
      [ "ai::IsInCombatValue", "classai_1_1_is_in_combat_value.html", null ],
      [ "ai::IsMountedValue", "classai_1_1_is_mounted_value.html", null ],
      [ "ai::IsMovingValue", "classai_1_1_is_moving_value.html", null ],
      [ "ai::IsSwimmingValue", "classai_1_1_is_swimming_value.html", null ],
      [ "ai::ItemCountValue", "classai_1_1_item_count_value.html", null ],
      [ "ai::ItemForSpellValue", "classai_1_1_item_for_spell_value.html", null ],
      [ "ai::ItemUsageValue", "classai_1_1_item_usage_value.html", null ],
      [ "ai::LastSpellCastTimeValue", "classai_1_1_last_spell_cast_time_value.html", null ],
      [ "ai::ManaValue", "classai_1_1_mana_value.html", null ],
      [ "ai::MyAttackerCountValue", "classai_1_1_my_attacker_count_value.html", null ],
      [ "ai::PartyMemberToDispel", "classai_1_1_party_member_to_dispel.html", null ],
      [ "ai::PartyMemberWithoutAuraValue", "classai_1_1_party_member_without_aura_value.html", null ],
      [ "ai::PositionValue", "classai_1_1_position_value.html", null ],
      [ "ai::RageValue", "classai_1_1_rage_value.html", null ],
      [ "ai::SpellCastUsefulValue", "classai_1_1_spell_cast_useful_value.html", null ],
      [ "ai::SpellIdValue", "classai_1_1_spell_id_value.html", null ],
      [ "ai::ThreatValue", "classai_1_1_threat_value.html", null ]
    ] ],
    [ "QueryNamedResult", "class_query_named_result.html", null ],
    [ "QueryResult", "class_query_result.html", [
      [ "QueryResultMysql", "class_query_result_mysql.html", null ],
      [ "QueryResultPostgre", "class_query_result_postgre.html", null ]
    ] ],
    [ "Quest", "class_quest.html", null ],
    [ "QuestItem", "struct_quest_item.html", null ],
    [ "QuestLocale", "struct_quest_locale.html", null ],
    [ "QuestMenu", "class_quest_menu.html", null ],
    [ "QuestMenuItem", "struct_quest_menu_item.html", null ],
    [ "QuestSortEntry", "struct_quest_sort_entry.html", null ],
    [ "QuestStatusData", "struct_quest_status_data.html", null ],
    [ "ai::Queue", "classai_1_1_queue.html", null ],
    [ "RaceMaskName", "struct_race_mask_name.html", null ],
    [ "RandomArrayEntry", "struct_random_array_entry.html", null ],
    [ "RandomPlayerbotFactory", "class_random_playerbot_factory.html", null ],
    [ "ai::RangePair", "classai_1_1_range_pair.html", null ],
    [ "RankInfo", "struct_rank_info.html", null ],
    [ "ReapplyAffectedPassiveAurasHelper", "struct_reapply_affected_passive_auras_helper.html", null ],
    [ "DBCFileLoader::Record", "class_d_b_c_file_loader_1_1_record.html", null ],
    [ "Referencable< Countable >", "class_referencable.html", null ],
    [ "Referencable< AtomicLong >", "class_referencable.html", [
      [ "TerrainInfo", "class_terrain_info.html", null ]
    ] ],
    [ "RegularGrid2D< T, Node, NodeCreatorFunc, PositionFunc >", "class_regular_grid2_d.html", [
      [ "DynTreeImpl", "struct_dyn_tree_impl.html", null ]
    ] ],
    [ "rep", "structrep.html", null ],
    [ "RepRewardRate", "struct_rep_reward_rate.html", null ],
    [ "RepSpilloverTemplate", "struct_rep_spillover_template.html", null ],
    [ "ReputationMgr", "class_reputation_mgr.html", null ],
    [ "ReputationOnKillEntry", "struct_reputation_on_kill_entry.html", null ],
    [ "MaNGOS::RespawnDo", "class_ma_n_g_o_s_1_1_respawn_do.html", null ],
    [ "ACE_Based::Runnable", "class_a_c_e___based_1_1_runnable.html", [
      [ "SqlDelayThread", "class_sql_delay_thread.html", [
        [ "PGSQLDelayThread", "class_p_g_s_q_l_delay_thread.html", null ]
      ] ]
    ] ],
    [ "ScriptAction", "class_script_action.html", null ],
    [ "ScriptInfo", "struct_script_info.html", null ],
    [ "ScriptMgr", "class_script_mgr.html", null ],
    [ "SellerItemClassInfo", "struct_seller_item_class_info.html", null ],
    [ "SellerItemInfo", "struct_seller_item_info.html", null ],
    [ "ServerPktHeader", "struct_server_pkt_header.html", null ],
    [ "set", null, [
      [ "ai::LootTargetList", "classai_1_1_loot_target_list.html", null ]
    ] ],
    [ "SetGameMasterOffHelper", "struct_set_game_master_off_helper.html", null ],
    [ "SetGameMasterOnHelper", "struct_set_game_master_on_helper.html", null ],
    [ "SetPvPHelper", "struct_set_pv_p_helper.html", null ],
    [ "SetSpeedRateHelper", "struct_set_speed_rate_helper.html", null ],
    [ "sField", "structs_field.html", null ],
    [ "Sha1Hash", "class_sha1_hash.html", null ],
    [ "SHA1Randx", "class_s_h_a1_randx.html", null ],
    [ "ShortIntervalTimer", "class_short_interval_timer.html", null ],
    [ "ShortTimeTracker", "struct_short_time_tracker.html", null ],
    [ "SilithusSpawnLocation", "struct_silithus_spawn_location.html", null ],
    [ "MaNGOS::SingleThreaded< T >", "class_ma_n_g_o_s_1_1_single_threaded.html", null ],
    [ "MaNGOS::Singleton< T, ThreadingModel, CreatePolicy, LifeTimePolicy >", "class_ma_n_g_o_s_1_1_singleton.html", null ],
    [ "MaNGOS::Singleton< Log, MaNGOS::ClassLevelLockable< Log, ACE_Thread_Mutex > >", "class_ma_n_g_o_s_1_1_singleton.html", [
      [ "Log", "class_log.html", null ]
    ] ],
    [ "MaNGOS::Singleton< MapManager, MaNGOS::ClassLevelLockable< MapManager, ACE_Recursive_Thread_Mutex > >", "class_ma_n_g_o_s_1_1_singleton.html", [
      [ "MapManager", "class_map_manager.html", null ]
    ] ],
    [ "MaNGOS::Singleton< MapPersistentStateManager, MaNGOS::ClassLevelLockable< MapPersistentStateManager, ACE_Thread_Mutex > >", "class_ma_n_g_o_s_1_1_singleton.html", [
      [ "MapPersistentStateManager", "class_map_persistent_state_manager.html", null ]
    ] ],
    [ "MaNGOS::Singleton< ObjectAccessor, MaNGOS::ClassLevelLockable< ObjectAccessor, ACE_Thread_Mutex > >", "class_ma_n_g_o_s_1_1_singleton.html", [
      [ "ObjectAccessor", "class_object_accessor.html", null ]
    ] ],
    [ "MaNGOS::Singleton< TerrainManager, MaNGOS::ClassLevelLockable< TerrainManager, ACE_Thread_Mutex > >", "class_ma_n_g_o_s_1_1_singleton.html", [
      [ "TerrainManager", "class_terrain_manager.html", null ]
    ] ],
    [ "SkillLineAbilityEntry", "struct_skill_line_ability_entry.html", null ],
    [ "SkillLineEntry", "struct_skill_line_entry.html", null ],
    [ "SkillRaceClassInfoEntry", "struct_skill_race_class_info_entry.html", null ],
    [ "SkillStatusData", "struct_skill_status_data.html", null ],
    [ "SOAPCommand", "struct_s_o_a_p_command.html", null ],
    [ "SocialMgr", "class_social_mgr.html", null ],
    [ "SortByPricePredicate", "struct_sort_by_price_predicate.html", null ],
    [ "SoundEntriesEntry", "struct_sound_entries_entry.html", null ],
    [ "SpawnCreatureInMapsWorker", "struct_spawn_creature_in_maps_worker.html", null ],
    [ "SpawnedPoolData", "class_spawned_pool_data.html", null ],
    [ "SpawnGameObjectInMapsWorker", "struct_spawn_game_object_in_maps_worker.html", null ],
    [ "SpawnPoolInMapsWorker", "struct_spawn_pool_in_maps_worker.html", null ],
    [ "Spell", "class_spell.html", null ],
    [ "spell_cooldown_data", "structspell__cooldown__data.html", null ],
    [ "spell_data", "structspell__data.html", null ],
    [ "SpellArea", "struct_spell_area.html", null ],
    [ "SpellAuraHolder", "class_spell_aura_holder.html", null ],
    [ "SpellBonusEntry", "struct_spell_bonus_entry.html", null ],
    [ "SpellCastTargets", "class_spell_cast_targets.html", null ],
    [ "SpellCastTargetsReader", "struct_spell_cast_targets_reader.html", null ],
    [ "SpellCastTimesEntry", "struct_spell_cast_times_entry.html", null ],
    [ "SpellChainNode", "struct_spell_chain_node.html", null ],
    [ "SpellCooldown", "struct_spell_cooldown.html", null ],
    [ "SpellDurationEntry", "struct_spell_duration_entry.html", null ],
    [ "SpellEntry", "struct_spell_entry.html", null ],
    [ "ai::SpellEntryPredicate", "classai_1_1_spell_entry_predicate.html", [
      [ "IsTargetOfHealingSpell", "class_is_target_of_healing_spell.html", null ],
      [ "IsTargetOfResurrectSpell", "class_is_target_of_resurrect_spell.html", null ]
    ] ],
    [ "SpellFocusObjectEntry", "struct_spell_focus_object_entry.html", null ],
    [ "SpellImmune", "struct_spell_immune.html", null ],
    [ "SpellItemEnchantmentEntry", "struct_spell_item_enchantment_entry.html", null ],
    [ "SpellLearnSkillNode", "struct_spell_learn_skill_node.html", null ],
    [ "SpellLearnSpellNode", "struct_spell_learn_spell_node.html", null ],
    [ "SpellLinkedEntry", "struct_spell_linked_entry.html", null ],
    [ "SpellMgr", "class_spell_mgr.html", null ],
    [ "SpellModifier", "struct_spell_modifier.html", null ],
    [ "SpellNonMeleeDamage", "struct_spell_non_melee_damage.html", null ],
    [ "MaNGOS::SpellNotifierCreatureAndPlayer", "struct_ma_n_g_o_s_1_1_spell_notifier_creature_and_player.html", null ],
    [ "MaNGOS::SpellNotifierPlayer", "struct_ma_n_g_o_s_1_1_spell_notifier_player.html", null ],
    [ "SpellPeriodicAuraLogInfo", "struct_spell_periodic_aura_log_info.html", null ],
    [ "SpellProcEventEntry", "struct_spell_proc_event_entry.html", null ],
    [ "SpellRadiusEntry", "struct_spell_radius_entry.html", null ],
    [ "SpellRangeEntry", "struct_spell_range_entry.html", null ],
    [ "SpellRankHelper< EntryType, WorkerType, StorageType >", "struct_spell_rank_helper.html", null ],
    [ "SpellShapeshiftFormEntry", "struct_spell_shapeshift_form_entry.html", null ],
    [ "SpellTargetEntry", "struct_spell_target_entry.html", null ],
    [ "SpellTargetPosition", "struct_spell_target_position.html", null ],
    [ "SpellThreatEntry", "struct_spell_threat_entry.html", null ],
    [ "Movement::SplineBase", "class_movement_1_1_spline_base.html", [
      [ "Movement::Spline< length_type >", "class_movement_1_1_spline.html", null ],
      [ "Movement::Spline< int32 >", "class_movement_1_1_spline.html", null ]
    ] ],
    [ "SqlConnection", "class_sql_connection.html", [
      [ "MySQLConnection", "class_my_s_q_l_connection.html", null ],
      [ "PostgreSQLConnection", "class_postgre_s_q_l_connection.html", null ]
    ] ],
    [ "SQLMultiStorage::SQLMSIteratorBounds< T >", "class_s_q_l_multi_storage_1_1_s_q_l_m_s_iterator_bounds.html", null ],
    [ "SQLMultiStorage::SQLMultiSIterator< T >", "class_s_q_l_multi_storage_1_1_s_q_l_multi_s_iterator.html", null ],
    [ "SqlOperation", "class_sql_operation.html", [
      [ "SqlPlainRequest", "class_sql_plain_request.html", null ],
      [ "SqlPreparedRequest", "class_sql_prepared_request.html", null ],
      [ "SqlQuery", "class_sql_query.html", null ],
      [ "SqlQueryHolderEx", "class_sql_query_holder_ex.html", null ],
      [ "SqlTransaction", "class_sql_transaction.html", null ]
    ] ],
    [ "SqlPreparedStatement", "class_sql_prepared_statement.html", [
      [ "MySqlPreparedStatement", "class_my_sql_prepared_statement.html", null ],
      [ "SqlPlainPreparedStatement", "class_sql_plain_prepared_statement.html", null ]
    ] ],
    [ "SqlQueryHolder", "class_sql_query_holder.html", [
      [ "LoginQueryHolder", "class_login_query_holder.html", null ]
    ] ],
    [ "SQLStorageBase::SQLSIterator< T >", "class_s_q_l_storage_base_1_1_s_q_l_s_iterator.html", null ],
    [ "SqlStatement", "class_sql_statement.html", null ],
    [ "SqlStatementID", "class_sql_statement_i_d.html", null ],
    [ "SqlStmtField", "union_sql_stmt_field.html", null ],
    [ "SqlStmtFieldData", "class_sql_stmt_field_data.html", null ],
    [ "SqlStmtParameters", "class_sql_stmt_parameters.html", null ],
    [ "SQLStorageBase", "class_s_q_l_storage_base.html", [
      [ "SQLHashStorage", "class_s_q_l_hash_storage.html", null ],
      [ "SQLMultiStorage", "class_s_q_l_multi_storage.html", null ],
      [ "SQLStorage", "class_s_q_l_storage.html", null ]
    ] ],
    [ "SQLStorageLoaderBase< DerivedLoader, StorageClass >", "class_s_q_l_storage_loader_base.html", null ],
    [ "SQLStorageLoaderBase< SQLCreatureLoader, SQLStorage >", "class_s_q_l_storage_loader_base.html", [
      [ "SQLCreatureLoader", "struct_s_q_l_creature_loader.html", null ]
    ] ],
    [ "SQLStorageLoaderBase< SQLGameObjectLoader, SQLHashStorage >", "class_s_q_l_storage_loader_base.html", [
      [ "SQLGameObjectLoader", "struct_s_q_l_game_object_loader.html", null ]
    ] ],
    [ "SQLStorageLoaderBase< SQLHashStorageLoader, SQLHashStorage >", "class_s_q_l_storage_loader_base.html", [
      [ "SQLHashStorageLoader", "class_s_q_l_hash_storage_loader.html", null ]
    ] ],
    [ "SQLStorageLoaderBase< SQLInstanceLoader, SQLStorage >", "class_s_q_l_storage_loader_base.html", [
      [ "SQLInstanceLoader", "struct_s_q_l_instance_loader.html", null ]
    ] ],
    [ "SQLStorageLoaderBase< SQLItemLoader, SQLStorage >", "class_s_q_l_storage_loader_base.html", [
      [ "SQLItemLoader", "struct_s_q_l_item_loader.html", null ]
    ] ],
    [ "SQLStorageLoaderBase< SQLMultiStorageLoader, SQLMultiStorage >", "class_s_q_l_storage_loader_base.html", [
      [ "SQLMultiStorageLoader", "class_s_q_l_multi_storage_loader.html", null ]
    ] ],
    [ "SQLStorageLoaderBase< SQLStorageLoader, SQLStorage >", "class_s_q_l_storage_loader_base.html", [
      [ "SQLStorageLoader", "class_s_q_l_storage_loader.html", null ]
    ] ],
    [ "SQLStorageLoaderBase< SQLWorldLoader, SQLStorage >", "class_s_q_l_storage_loader_base.html", [
      [ "SQLWorldLoader", "struct_s_q_l_world_loader.html", null ]
    ] ],
    [ "StableSlotPricesEntry", "struct_stable_slot_prices_entry.html", null ],
    [ "stack", null, [
      [ "MotionMaster", "class_motion_master.html", null ]
    ] ],
    [ "BIH::StackNode", "struct_b_i_h_1_1_stack_node.html", null ],
    [ "VMAP::StaticMapTree", "class_v_m_a_p_1_1_static_map_tree.html", null ],
    [ "StaticMonsterChatBuilder", "class_static_monster_chat_builder.html", null ],
    [ "StopAttackFactionHelper", "struct_stop_attack_faction_helper.html", null ],
    [ "SYMBOL_INFO_PACKAGE", null, [
      [ "CSymbolInfoPackage", "struct_c_symbol_info_package.html", null ]
    ] ],
    [ "TalentEntry", "struct_talent_entry.html", null ],
    [ "TalentSpellPos", "struct_talent_spell_pos.html", null ],
    [ "TalentTabEntry", "struct_talent_tab_entry.html", null ],
    [ "TargetedMovementGeneratorBase", "class_targeted_movement_generator_base.html", [
      [ "TargetedMovementGeneratorMedium< T, D >", "class_targeted_movement_generator_medium.html", null ],
      [ "TargetedMovementGeneratorMedium< T, ChaseMovementGenerator< T > >", "class_targeted_movement_generator_medium.html", null ],
      [ "TargetedMovementGeneratorMedium< T, FollowMovementGenerator< T > >", "class_targeted_movement_generator_medium.html", null ]
    ] ],
    [ "Spell::TargetInfo", "struct_spell_1_1_target_info.html", null ],
    [ "TaxiNodesEntry", "struct_taxi_nodes_entry.html", null ],
    [ "TaxiPathBySourceAndDestination", "struct_taxi_path_by_source_and_destination.html", null ],
    [ "TaxiPathEntry", "struct_taxi_path_entry.html", null ],
    [ "TaxiPathNodeEntry", "struct_taxi_path_node_entry.html", null ],
    [ "TaxiPathNodePtr", "struct_taxi_path_node_ptr.html", null ],
    [ "TForm", null, [
      [ "TFrmMain", "class_t_frm_main.html", null ],
      [ "TFrmSearch", "class_t_frm_search.html", null ],
      [ "TFrmTitle", "class_t_frm_title.html", null ]
    ] ],
    [ "ACE_Based::Thread", "class_a_c_e___based_1_1_thread.html", null ],
    [ "Thread", null, [
      [ "mmap_extract.workerThread", "classmmap__extract_1_1worker_thread.html", null ]
    ] ],
    [ "ThreadingModel< T >", "class_threading_model_3_01_t_01_4.html", null ],
    [ "ACE_Based::ThreadPriority", "class_a_c_e___based_1_1_thread_priority.html", null ],
    [ "ThreatCalcHelper", "class_threat_calc_helper.html", null ],
    [ "ThreatContainer", "class_threat_container.html", null ],
    [ "ThreatManager", "class_threat_manager.html", null ],
    [ "VMAP::TileAssembler", "class_v_m_a_p_1_1_tile_assembler.html", null ],
    [ "TimeTracker", "struct_time_tracker.html", null ],
    [ "TradeData", "class_trade_data.html", null ],
    [ "TradeStatusInfo", "struct_trade_status_info.html", null ],
    [ "TrainerSpell", "struct_trainer_spell.html", null ],
    [ "TrainerSpellData", "struct_trainer_spell_data.html", null ],
    [ "Database::TransHelper", "class_database_1_1_trans_helper.html", null ],
    [ "VMAP::TriBoundFunc", "class_v_m_a_p_1_1_tri_bound_func.html", null ],
    [ "ai::TriggerNode", "classai_1_1_trigger_node.html", null ],
    [ "TThread", null, [
      [ "thOpenFile", "classth_open_file.html", null ]
    ] ],
    [ "TypeContainerVisitor< VISITOR, TYPE_CONTAINER >", "class_type_container_visitor.html", null ],
    [ "TypeList< HEAD, TAIL >", "struct_type_list.html", null ],
    [ "TypeMapContainer< OBJECT_TYPES >", "class_type_map_container.html", null ],
    [ "TypeMapContainer< GRID_OBJECT_TYPES >", "class_type_map_container.html", null ],
    [ "TypeMapContainer< WORLD_OBJECT_TYPES >", "class_type_map_container.html", null ],
    [ "TypePtr", "union_type_ptr.html", null ],
    [ "TypeUnorderedMapContainer< OBJECT_TYPES, KEY_TYPE >", "class_type_unordered_map_container.html", null ],
    [ "TypeUnorderedMapContainer< AllMapStoredObjectTypes, ObjectGuid >", "class_type_unordered_map_container.html", null ],
    [ "unary_function", null, [
      [ "ChainHealingFullHealth", "class_chain_healing_full_health.html", null ]
    ] ],
    [ "UnitActionBarEntry", "struct_unit_action_bar_entry.html", null ],
    [ "UnitBaseEvent", "class_unit_base_event.html", [
      [ "ThreatRefStatusChangeEvent", "class_threat_ref_status_change_event.html", [
        [ "ThreatManagerEvent", "class_threat_manager_event.html", null ]
      ] ]
    ] ],
    [ "MaNGOS::UnitByGuidInRangeCheck", "class_ma_n_g_o_s_1_1_unit_by_guid_in_range_check.html", null ],
    [ "MaNGOS::UnitLastSearcher< Check >", "struct_ma_n_g_o_s_1_1_unit_last_searcher.html", null ],
    [ "MaNGOS::UnitListSearcher< Check >", "struct_ma_n_g_o_s_1_1_unit_list_searcher.html", null ],
    [ "MaNGOS::UnitSearcher< Check >", "struct_ma_n_g_o_s_1_1_unit_searcher.html", null ],
    [ "MaNGOS::UnitWorker< Do >", "struct_ma_n_g_o_s_1_1_unit_worker.html", null ],
    [ "Unused< T >", "struct_unused.html", null ],
    [ "UpdateData", "class_update_data.html", null ],
    [ "WorldObject::UpdateHelper", "class_world_object_1_1_update_helper.html", null ],
    [ "UpdateMask", "class_update_mask.html", null ],
    [ "UpdatePoolInMapsWorker< T >", "struct_update_pool_in_maps_worker.html", null ],
    [ "ai::Value< T >", "classai_1_1_value.html", [
      [ "ai::CalculatedValue< T >", "classai_1_1_calculated_value.html", null ],
      [ "ai::ManualSetValue< T >", "classai_1_1_manual_set_value.html", null ]
    ] ],
    [ "ai::Value< bool >", "classai_1_1_value.html", [
      [ "ai::CalculatedValue< bool >", "classai_1_1_calculated_value.html", null ]
    ] ],
    [ "ai::Value< ChatMsg >", "classai_1_1_value.html", [
      [ "ai::ManualSetValue< ChatMsg >", "classai_1_1_manual_set_value.html", null ]
    ] ],
    [ "ai::Value< double >", "classai_1_1_value.html", [
      [ "ai::ManualSetValue< double >", "classai_1_1_manual_set_value.html", null ]
    ] ],
    [ "ai::Value< float >", "classai_1_1_value.html", [
      [ "ai::CalculatedValue< float >", "classai_1_1_calculated_value.html", null ]
    ] ],
    [ "ai::Value< Item * >", "classai_1_1_value.html", [
      [ "ai::CalculatedValue< Item *>", "classai_1_1_calculated_value.html", null ]
    ] ],
    [ "ai::Value< ItemUsage >", "classai_1_1_value.html", [
      [ "ai::CalculatedValue< ItemUsage >", "classai_1_1_calculated_value.html", null ]
    ] ],
    [ "ai::Value< LastMovement & >", "classai_1_1_value.html", [
      [ "ai::ManualSetValue< LastMovement &>", "classai_1_1_manual_set_value.html", null ]
    ] ],
    [ "ai::Value< LastSpellCast & >", "classai_1_1_value.html", [
      [ "ai::ManualSetValue< LastSpellCast &>", "classai_1_1_manual_set_value.html", null ]
    ] ],
    [ "ai::Value< list< Item * > >", "classai_1_1_value.html", [
      [ "ai::CalculatedValue< list< Item *> >", "classai_1_1_calculated_value.html", null ]
    ] ],
    [ "ai::Value< list< ObjectGuid > >", "classai_1_1_value.html", [
      [ "ai::CalculatedValue< list< ObjectGuid > >", "classai_1_1_calculated_value.html", null ]
    ] ],
    [ "ai::Value< LogLevel >", "classai_1_1_value.html", [
      [ "ai::ManualSetValue< LogLevel >", "classai_1_1_manual_set_value.html", null ]
    ] ],
    [ "ai::Value< LootObject >", "classai_1_1_value.html", [
      [ "ai::ManualSetValue< LootObject >", "classai_1_1_manual_set_value.html", null ]
    ] ],
    [ "ai::Value< LootObjectStack * >", "classai_1_1_value.html", [
      [ "ai::ManualSetValue< LootObjectStack *>", "classai_1_1_manual_set_value.html", null ]
    ] ],
    [ "ai::Value< LootStrategy >", "classai_1_1_value.html", [
      [ "ai::ManualSetValue< LootStrategy >", "classai_1_1_manual_set_value.html", null ]
    ] ],
    [ "ai::Value< Position & >", "classai_1_1_value.html", [
      [ "ai::ManualSetValue< Position &>", "classai_1_1_manual_set_value.html", null ]
    ] ],
    [ "ai::Value< set< uint32 > & >", "classai_1_1_value.html", [
      [ "ai::ManualSetValue< set< uint32 > &>", "classai_1_1_manual_set_value.html", null ]
    ] ],
    [ "ai::Value< string >", "classai_1_1_value.html", [
      [ "ai::ManualSetValue< string >", "classai_1_1_manual_set_value.html", null ]
    ] ],
    [ "ai::Value< time_t >", "classai_1_1_value.html", [
      [ "ai::ManualSetValue< time_t >", "classai_1_1_manual_set_value.html", null ]
    ] ],
    [ "ai::Value< uint32 >", "classai_1_1_value.html", [
      [ "ai::CalculatedValue< uint32 >", "classai_1_1_calculated_value.html", null ],
      [ "ai::ManualSetValue< uint32 >", "classai_1_1_manual_set_value.html", null ]
    ] ],
    [ "ai::Value< uint8 >", "classai_1_1_value.html", [
      [ "ai::CalculatedValue< uint8 >", "classai_1_1_calculated_value.html", null ]
    ] ],
    [ "ai::Value< Unit * >", "classai_1_1_value.html", [
      [ "ai::CalculatedValue< Unit *>", "classai_1_1_calculated_value.html", null ],
      [ "ai::ManualSetValue< Unit *>", "classai_1_1_manual_set_value.html", null ]
    ] ],
    [ "VendorItem", "struct_vendor_item.html", null ],
    [ "VendorItemCount", "struct_vendor_item_count.html", null ],
    [ "VendorItemData", "struct_vendor_item_data.html", null ],
    [ "ViewPoint", "class_view_point.html", null ],
    [ "MaNGOS::VisibleChangesNotifier", "struct_ma_n_g_o_s_1_1_visible_changes_notifier.html", null ],
    [ "MaNGOS::VisibleNotifier", "struct_ma_n_g_o_s_1_1_visible_notifier.html", null ],
    [ "VMAP::VMapFactory", "class_v_m_a_p_1_1_v_map_factory.html", null ],
    [ "Warden", "class_warden.html", [
      [ "WardenMac", "class_warden_mac.html", null ],
      [ "WardenWin", "class_warden_win.html", null ]
    ] ],
    [ "WardenCheck", "struct_warden_check.html", null ],
    [ "WardenCheckMgr", "class_warden_check_mgr.html", null ],
    [ "WardenCheckResult", "struct_warden_check_result.html", null ],
    [ "WardenHashRequest", "struct_warden_hash_request.html", null ],
    [ "WardenInitModuleRequest", "struct_warden_init_module_request.html", null ],
    [ "WardenModuleTransfer", "struct_warden_module_transfer.html", null ],
    [ "WardenModuleUse", "struct_warden_module_use.html", null ],
    [ "WatchDog", "struct_watch_dog.html", null ],
    [ "WaypointBehavior", "struct_waypoint_behavior.html", null ],
    [ "WaypointManager", "class_waypoint_manager.html", null ],
    [ "WaypointMovementGenerator< T >", "class_waypoint_movement_generator.html", null ],
    [ "WaypointNode", "struct_waypoint_node.html", null ],
    [ "Weather", "class_weather.html", null ],
    [ "WeatherMgr", "class_weather_mgr.html", null ],
    [ "WeatherSeasonChances", "struct_weather_season_chances.html", null ],
    [ "WeatherSystem", "class_weather_system.html", null ],
    [ "WeatherZoneChances", "struct_weather_zone_chances.html", null ],
    [ "WheatyExceptionReport", "class_wheaty_exception_report.html", null ],
    [ "WMOAreaTableEntry", "struct_w_m_o_area_table_entry.html", null ],
    [ "WMOAreaTableTripple", "struct_w_m_o_area_table_tripple.html", null ],
    [ "VMAP::WModelAreaCallback", "class_v_m_a_p_1_1_w_model_area_callback.html", null ],
    [ "VMAP::WModelRayCallBack", "struct_v_m_a_p_1_1_w_model_ray_call_back.html", null ],
    [ "VMAP::WmoLiquid", "class_v_m_a_p_1_1_wmo_liquid.html", null ],
    [ "VMAP::WMOLiquidHeader", "struct_v_m_a_p_1_1_w_m_o_liquid_header.html", null ],
    [ "World", "class_world.html", null ],
    [ "WorldHandler", null, [
      [ "WorldSocket", "class_world_socket.html", null ]
    ] ],
    [ "WorldLocation", "struct_world_location.html", null ],
    [ "WorldMapAreaEntry", "struct_world_map_area_entry.html", null ],
    [ "VMAP::WorldModel", "class_v_m_a_p_1_1_world_model.html", null ],
    [ "VMAP::WorldModel_Raw", "struct_v_m_a_p_1_1_world_model___raw.html", null ],
    [ "WorldObjectChangeAccumulator", "struct_world_object_change_accumulator.html", null ],
    [ "MaNGOS::WorldObjectLastSearcher< Check >", "struct_ma_n_g_o_s_1_1_world_object_last_searcher.html", null ],
    [ "MaNGOS::WorldObjectListSearcher< Check >", "struct_ma_n_g_o_s_1_1_world_object_list_searcher.html", null ],
    [ "MaNGOS::WorldObjectSearcher< Check >", "struct_ma_n_g_o_s_1_1_world_object_searcher.html", null ],
    [ "MaNGOS::WorldObjectWorker< Do >", "struct_ma_n_g_o_s_1_1_world_object_worker.html", null ],
    [ "WorldSafeLocsEntry", "struct_world_safe_locs_entry.html", null ],
    [ "WorldSession", "class_world_session.html", null ],
    [ "WorldStatePair", "struct_world_state_pair.html", null ],
    [ "WorldTimer", "class_world_timer.html", null ],
    [ "WorldUpdateCounter", "class_world_update_counter.html", null ],
    [ "MaNGOS::WorldWorldTextBuilder", "class_ma_n_g_o_s_1_1_world_world_text_builder.html", null ],
    [ "CB", null, [
      [ "MaNGOS::_ICallback< CB >", "class_ma_n_g_o_s_1_1___i_callback.html", null ],
      [ "MaNGOS::_IQueryCallback< CB >", "class_ma_n_g_o_s_1_1___i_query_callback.html", null ]
    ] ],
    [ "Vector3", null, [
      [ "Movement::Location", "struct_movement_1_1_location.html", null ]
    ] ]
];