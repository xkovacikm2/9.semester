var struct_seller_item_info =
[
    [ "SellerItemInfo", "struct_seller_item_info.html#ae65f3c24760076943f35a8f6b3205607", null ],
    [ "AmountOfItems", "struct_seller_item_info.html#a8821b607b3421c2ad3650ffc3541b14f", null ],
    [ "ItemClassInfos", "struct_seller_item_info.html#a059e693ddc8046935005cc8bb4b5a97d", null ],
    [ "MissItems", "struct_seller_item_info.html#a7137694b442b56145c171bf143e218e1", null ],
    [ "PriceRatio", "struct_seller_item_info.html#accabfcbde78dd98130d54990e3dd967d", null ]
];