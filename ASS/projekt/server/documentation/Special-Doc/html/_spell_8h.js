var _spell_8h =
[
    [ "SpellCastTargetsReader", "struct_spell_cast_targets_reader.html", "struct_spell_cast_targets_reader" ],
    [ "SpellCastTargets", "class_spell_cast_targets.html", "class_spell_cast_targets" ],
    [ "Spell", "class_spell.html", "class_spell" ],
    [ "TargetInfo", "struct_spell_1_1_target_info.html", "struct_spell_1_1_target_info" ],
    [ "GOTargetInfo", "struct_spell_1_1_g_o_target_info.html", "struct_spell_1_1_g_o_target_info" ],
    [ "ItemTargetInfo", "struct_spell_1_1_item_target_info.html", "struct_spell_1_1_item_target_info" ],
    [ "SpellNotifierPlayer", "struct_ma_n_g_o_s_1_1_spell_notifier_player.html", "struct_ma_n_g_o_s_1_1_spell_notifier_player" ],
    [ "SpellNotifierCreatureAndPlayer", "struct_ma_n_g_o_s_1_1_spell_notifier_creature_and_player.html", "struct_ma_n_g_o_s_1_1_spell_notifier_creature_and_player" ],
    [ "SpellEvent", "class_spell_event.html", "class_spell_event" ],
    [ "pEffect", "_spell_8h.html#abf8a42faff2e4c924eb6200d1243470e", null ],
    [ "SpellTargetTimeMap", "_spell_8h.html#a76c8e65c8907921fc5a906674fb29a66", null ],
    [ "ReplenishType", "_spell_8h.html#a4bad38c73aafb57f6b2fe2537e960689", [
      [ "REPLENISH_UNDEFINED", "_spell_8h.html#a4bad38c73aafb57f6b2fe2537e960689a44c042fdd280823a5550cb7174f18d3f", null ],
      [ "REPLENISH_HEALTH", "_spell_8h.html#a4bad38c73aafb57f6b2fe2537e960689a6d2fc80d3067aeeead272672c7252dd1", null ],
      [ "REPLENISH_MANA", "_spell_8h.html#a4bad38c73aafb57f6b2fe2537e960689a9530bed38b6b9790ca1f8353da844290", null ],
      [ "REPLENISH_RAGE", "_spell_8h.html#a4bad38c73aafb57f6b2fe2537e960689adc4cbd70d65521fe99ac679b75baa38f", null ]
    ] ],
    [ "SpellCastFlags", "_spell_8h.html#abed1822d323e37699c2c01a115a1d2f5", [
      [ "CAST_FLAG_NONE", "_spell_8h.html#abed1822d323e37699c2c01a115a1d2f5ae3bc67bf88cf15e2d611b9cc1137a3ca", null ],
      [ "CAST_FLAG_HIDDEN_COMBATLOG", "_spell_8h.html#abed1822d323e37699c2c01a115a1d2f5a8980c27eb74f67451b1a58348b6583a2", null ],
      [ "CAST_FLAG_UNKNOWN2", "_spell_8h.html#abed1822d323e37699c2c01a115a1d2f5af886177dd798d8295ddce2bfd2499d5b", null ],
      [ "CAST_FLAG_UNKNOWN3", "_spell_8h.html#abed1822d323e37699c2c01a115a1d2f5a37f9c6a7dd84cce30d470b485c7abc68", null ],
      [ "CAST_FLAG_UNKNOWN4", "_spell_8h.html#abed1822d323e37699c2c01a115a1d2f5a4d06695a426afaa30167f7c5bc480f9a", null ],
      [ "CAST_FLAG_UNKNOWN5", "_spell_8h.html#abed1822d323e37699c2c01a115a1d2f5af6a54533ea142ac477fca16950dde6ea", null ],
      [ "CAST_FLAG_AMMO", "_spell_8h.html#abed1822d323e37699c2c01a115a1d2f5a0c8ef953a07121a38ba7875e32720953", null ],
      [ "CAST_FLAG_UNKNOWN7", "_spell_8h.html#abed1822d323e37699c2c01a115a1d2f5a392ab315fc9125f98dbd806ceaf711b0", null ],
      [ "CAST_FLAG_UNKNOWN8", "_spell_8h.html#abed1822d323e37699c2c01a115a1d2f5a70a3a7c7f336d0d9428a1954934190a5", null ],
      [ "CAST_FLAG_UNKNOWN9", "_spell_8h.html#abed1822d323e37699c2c01a115a1d2f5a04f23993f9ac92c42034f26148fae84a", null ]
    ] ],
    [ "SpellNotifyPushType", "_spell_8h.html#a26481736169727a2aefce2cac71c5bee", [
      [ "PUSH_IN_FRONT", "_spell_8h.html#a26481736169727a2aefce2cac71c5beeaa66f2ff6a968146c87a0a6989774c0cf", null ],
      [ "PUSH_IN_FRONT_90", "_spell_8h.html#a26481736169727a2aefce2cac71c5beeac0d4b78061ea46c7ca0376ac8d275bf3", null ],
      [ "PUSH_IN_FRONT_15", "_spell_8h.html#a26481736169727a2aefce2cac71c5beeaf35c79e72deec1be632b9d6449a9b9f8", null ],
      [ "PUSH_IN_BACK", "_spell_8h.html#a26481736169727a2aefce2cac71c5beea632787f8356d03970fa934b7a08d7047", null ],
      [ "PUSH_SELF_CENTER", "_spell_8h.html#a26481736169727a2aefce2cac71c5beea1bb4ed5d94b5043026124678e75614f2", null ],
      [ "PUSH_DEST_CENTER", "_spell_8h.html#a26481736169727a2aefce2cac71c5beeaa2de4f65fdb6a9714d35ca0915d97e82", null ],
      [ "PUSH_TARGET_CENTER", "_spell_8h.html#a26481736169727a2aefce2cac71c5beeabb9c9e877dd5e758ed6738ea93caf4cd", null ]
    ] ],
    [ "SpellState", "_spell_8h.html#a821bd91389a0f4046989739b12e84f0a", [
      [ "SPELL_STATE_PREPARING", "_spell_8h.html#a821bd91389a0f4046989739b12e84f0aa626cba4f3a51caac32066af8b5e13da3", null ],
      [ "SPELL_STATE_CASTING", "_spell_8h.html#a821bd91389a0f4046989739b12e84f0aae175109189bc7f989a42de7f088cb6fa", null ],
      [ "SPELL_STATE_FINISHED", "_spell_8h.html#a821bd91389a0f4046989739b12e84f0aa620ae87eec478ce64981cb346d628a44", null ],
      [ "SPELL_STATE_DELAYED", "_spell_8h.html#a821bd91389a0f4046989739b12e84f0aa301d2895947dd39262654eb77e794452", null ]
    ] ],
    [ "SpellTargets", "_spell_8h.html#abdc7a5fee946eeffe8bfaad9e6b4b069", [
      [ "SPELL_TARGETS_HOSTILE", "_spell_8h.html#abdc7a5fee946eeffe8bfaad9e6b4b069a2d412a46564fc37f6f30b1aec6417254", null ],
      [ "SPELL_TARGETS_NOT_FRIENDLY", "_spell_8h.html#abdc7a5fee946eeffe8bfaad9e6b4b069a4a6912155589356ffd198e7e518f54f3", null ],
      [ "SPELL_TARGETS_NOT_HOSTILE", "_spell_8h.html#abdc7a5fee946eeffe8bfaad9e6b4b069a8a5da0cfa49e46cc2ab60816f57a3506", null ],
      [ "SPELL_TARGETS_FRIENDLY", "_spell_8h.html#abdc7a5fee946eeffe8bfaad9e6b4b069a8bc22901b14cb1fa3e7b5f748a880563", null ],
      [ "SPELL_TARGETS_AOE_DAMAGE", "_spell_8h.html#abdc7a5fee946eeffe8bfaad9e6b4b069abd964d7eb2650848f3d4933aa36db88d", null ],
      [ "SPELL_TARGETS_ALL", "_spell_8h.html#abdc7a5fee946eeffe8bfaad9e6b4b069a4fe3ea6f9fdd7dde6913ba898f1e11e3", null ]
    ] ],
    [ "IsQuestTameSpell", "_spell_8h.html#a9139404c5db8890c4fa92c1a7a8e68b1", null ],
    [ "operator<<", "_spell_8h.html#a6dfaac95bb7cec4a161b57cc94c1b20b", null ],
    [ "operator>>", "_spell_8h.html#a33b6289ab2f79031d618413a68fdb6aa", null ]
];