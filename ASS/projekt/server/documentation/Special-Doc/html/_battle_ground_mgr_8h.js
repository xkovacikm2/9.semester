var _battle_ground_mgr_8h =
[
    [ "PlayerQueueInfo", "struct_player_queue_info.html", "struct_player_queue_info" ],
    [ "GroupQueueInfo", "struct_group_queue_info.html", "struct_group_queue_info" ],
    [ "BattleGroundQueue", "class_battle_ground_queue.html", "class_battle_ground_queue" ],
    [ "BGQueueInviteEvent", "class_b_g_queue_invite_event.html", "class_b_g_queue_invite_event" ],
    [ "BGQueueRemoveEvent", "class_b_g_queue_remove_event.html", "class_b_g_queue_remove_event" ],
    [ "BattleGroundMgr", "class_battle_ground_mgr.html", "class_battle_ground_mgr" ],
    [ "BG_QUEUE_GROUP_TYPES_COUNT", "_battle_ground_mgr_8h.html#a5ea6ed78f78489fadc9d98c363702e0d", null ],
    [ "COUNT_OF_PLAYERS_TO_AVERAGE_WAIT_TIME", "_battle_ground_mgr_8h.html#acdd9d5422fecea59405438851a2d1f7c", null ],
    [ "sBattleGroundMgr", "_battle_ground_mgr_8h.html#ac3ad1a9b3606fb278830bb4b2094904e", null ],
    [ "BattleGroundSet", "_battle_ground_mgr_8h.html#a8800d1a450b5f61069d00506e62f0315", null ],
    [ "BattleMastersMap", "_battle_ground_mgr_8h.html#a07a5b459c475a48103ff26448b2d8ac2", null ],
    [ "BGFreeSlotQueueType", "_battle_ground_mgr_8h.html#abf15223bba680ec40365cfa89fdada79", null ],
    [ "CreatureBattleEventIndexesMap", "_battle_ground_mgr_8h.html#a6e8f922e23543b6495335ec48860db72", null ],
    [ "GameObjectBattleEventIndexesMap", "_battle_ground_mgr_8h.html#ae186de04a48531e6d0c1bc373d770fff", null ],
    [ "GroupQueueInfoPlayers", "_battle_ground_mgr_8h.html#a81dde25906c26e2f8d0140030aeaf482", null ],
    [ "BattleGroundGroupJoinStatus", "_battle_ground_mgr_8h.html#af21686a2632224380cce48b0962b98f5", [
      [ "BG_GROUPJOIN_DESERTERS", "_battle_ground_mgr_8h.html#af21686a2632224380cce48b0962b98f5a56a131b47a699009d16bd4312d97b6ae", null ],
      [ "BG_GROUPJOIN_FAILED", "_battle_ground_mgr_8h.html#af21686a2632224380cce48b0962b98f5a5208d576b1a940c48867f732dfbc02fb", null ]
    ] ],
    [ "BattleGroundQueueGroupTypes", "_battle_ground_mgr_8h.html#a3492a81fb67603f133535ba7d7c29ac7", [
      [ "BG_QUEUE_PREMADE_ALLIANCE", "_battle_ground_mgr_8h.html#a3492a81fb67603f133535ba7d7c29ac7a8f1f06d0124e05efcb390e0e9a29ddf4", null ],
      [ "BG_QUEUE_PREMADE_HORDE", "_battle_ground_mgr_8h.html#a3492a81fb67603f133535ba7d7c29ac7a39d0b398e1bcd59620eb0f8701139d1c", null ],
      [ "BG_QUEUE_NORMAL_ALLIANCE", "_battle_ground_mgr_8h.html#a3492a81fb67603f133535ba7d7c29ac7a4b31b889f93f89bfaacde424717c2697", null ],
      [ "BG_QUEUE_NORMAL_HORDE", "_battle_ground_mgr_8h.html#a3492a81fb67603f133535ba7d7c29ac7aea585c62a155542b2d643e25d554f15d", null ]
    ] ]
];