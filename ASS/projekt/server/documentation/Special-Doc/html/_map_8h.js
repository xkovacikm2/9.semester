var _map_8h =
[
    [ "InstanceTemplate", "struct_instance_template.html", "struct_instance_template" ],
    [ "Map", "class_map.html", "class_map" ],
    [ "WorldMap", "class_world_map.html", "class_world_map" ],
    [ "DungeonMap", "class_dungeon_map.html", "class_dungeon_map" ],
    [ "BattleGroundMap", "class_battle_ground_map.html", "class_battle_ground_map" ],
    [ "MIN_UNLOAD_DELAY", "_map_8h.html#a092c3d2c90fd46d14455e6ff2413dabb", null ]
];