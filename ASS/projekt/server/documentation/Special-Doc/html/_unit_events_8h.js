var _unit_events_8h =
[
    [ "UnitBaseEvent", "class_unit_base_event.html", "class_unit_base_event" ],
    [ "ThreatRefStatusChangeEvent", "class_threat_ref_status_change_event.html", "class_threat_ref_status_change_event" ],
    [ "ThreatManagerEvent", "class_threat_manager_event.html", "class_threat_manager_event" ],
    [ "UEV_ALL_EVENT_MASK", "_unit_events_8h.html#a99af7f8805c877a694d51ab4feb1e58b", null ],
    [ "UEV_THREAT_MANAGER_EVENT_MASK", "_unit_events_8h.html#a28d3aa8f148208fa65f5ef1af46913cc", null ],
    [ "UEV_THREAT_REF_EVENT_MASK", "_unit_events_8h.html#aca3c8db6dd274b113d9c3c7755ff5d7a", null ],
    [ "UnitThreatEventType", "_unit_events_8h.html#a0d3a1a8d294bb912afb0ed94576abcf3", [
      [ "UEV_THREAT_REF_ONLINE_STATUS", "_unit_events_8h.html#a0d3a1a8d294bb912afb0ed94576abcf3a77f8da111f54455d20c58ae0fd4156f9", null ],
      [ "UEV_THREAT_REF_THREAT_CHANGE", "_unit_events_8h.html#a0d3a1a8d294bb912afb0ed94576abcf3a5c74b5ccc46cdc399916b41b6d97bfb0", null ],
      [ "UEV_THREAT_REF_REMOVE_FROM_LIST", "_unit_events_8h.html#a0d3a1a8d294bb912afb0ed94576abcf3aa8b5a9d97c235e1f56e98ff4103b08d0", null ],
      [ "UEV_THREAT_REF_ASSECCIBLE_STATUS", "_unit_events_8h.html#a0d3a1a8d294bb912afb0ed94576abcf3abd7481924e1cb036704820e5be0407db", null ],
      [ "UEV_THREAT_SORT_LIST", "_unit_events_8h.html#a0d3a1a8d294bb912afb0ed94576abcf3a74092d5fa151b78ce0de1ddf3ed05865", null ],
      [ "UEV_THREAT_SET_NEXT_TARGET", "_unit_events_8h.html#a0d3a1a8d294bb912afb0ed94576abcf3ac45249081756ba40d712bd7e08626ed9", null ],
      [ "UEV_THREAT_VICTIM_CHANGED", "_unit_events_8h.html#a0d3a1a8d294bb912afb0ed94576abcf3af02166c466fbff650b908cd08c7e4669", null ]
    ] ]
];