var class_creature_linking_mgr =
[
    [ "CreatureLinkingMgr", "class_creature_linking_mgr.html#a4633792956a9887136b7dca46241069f", null ],
    [ "GetLinkedTriggerInformation", "group__npc__linking.html#ga7cb0f254d8c920bb3e252cf677ca79fc", null ],
    [ "GetLinkedTriggerInformation", "group__npc__linking.html#ga4bb9ace172e4c7d2f6a69e0781922fae", null ],
    [ "IsLinkedEventTrigger", "group__npc__linking.html#ga1bead7202530122a77e15c6d4e2761b3", null ],
    [ "IsLinkedMaster", "group__npc__linking.html#ga53abec816bb2e3ec2acc045d96603e44", null ],
    [ "IsSpawnedByLinkedMob", "group__npc__linking.html#gab2299e33cab49edca14c7c3a785ceb76", null ],
    [ "IsSpawnedByLinkedMob", "group__npc__linking.html#ga5fe947bdc3d31a49749913614705be33", null ],
    [ "LoadFromDB", "group__npc__linking.html#ga788201f3dbdde9ef6c7863c6b454e525", null ]
];