var struct_equipment_info_item =
[
    [ "Class", "struct_equipment_info_item.html#a270d2ea2ee9db07f3fa92ef35664bacb", null ],
    [ "DisplayID", "struct_equipment_info_item.html#aab24075f366f3c0916c338b56fa69db0", null ],
    [ "entry", "struct_equipment_info_item.html#a6a7e871ce596ce7bb6f64a223e47b9ec", null ],
    [ "InventoryType", "struct_equipment_info_item.html#a2884709ebccca0ad645d46087af93886", null ],
    [ "Material", "struct_equipment_info_item.html#a7ee5a772170a48ed59a0c750b40feed0", null ],
    [ "Sheath", "struct_equipment_info_item.html#aa1c67484fdf61ab160f51b4f62a80270", null ],
    [ "SubClass", "struct_equipment_info_item.html#a9c58e2b83b58ef0dae74b15a147c2f33", null ]
];