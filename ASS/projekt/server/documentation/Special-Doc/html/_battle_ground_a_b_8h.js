var _battle_ground_a_b_8h =
[
    [ "BG_AB_BannerTimer", "struct_b_g___a_b___banner_timer.html", "struct_b_g___a_b___banner_timer" ],
    [ "BattleGroundABScore", "class_battle_ground_a_b_score.html", "class_battle_ground_a_b_score" ],
    [ "BattleGroundAB", "class_battle_ground_a_b.html", "class_battle_ground_a_b" ],
    [ "AB_NORMAL_HONOR_INTERVAL", "_battle_ground_a_b_8h.html#a49e7f7f0f7a9f0a30ddf7fa8327279db", null ],
    [ "AB_NORMAL_REPUTATION_INTERVAL", "_battle_ground_a_b_8h.html#ac6e7a17355b03e7ba541d2d0d3e97c23", null ],
    [ "AB_WEEKEND_HONOR_INTERVAL", "_battle_ground_a_b_8h.html#a521741d000fe3fe420a598decc99757d", null ],
    [ "AB_WEEKEND_REPUTATION_INTERVAL", "_battle_ground_a_b_8h.html#a5563333f3ee5572e8cef437a6f8fe177", null ],
    [ "BG_AB_NODES_MAX", "_battle_ground_a_b_8h.html#aa58546b895d16d941ba1a7744f510b5a", null ],
    [ "BG_AB_Nodes", "_battle_ground_a_b_8h.html#a5883afd583529db00353140d7e0df0e8", [
      [ "BG_AB_NODE_STABLES", "_battle_ground_a_b_8h.html#a5883afd583529db00353140d7e0df0e8a1659f989b2c934ff7bbb18ddf3889f0f", null ],
      [ "BG_AB_NODE_BLACKSMITH", "_battle_ground_a_b_8h.html#a5883afd583529db00353140d7e0df0e8a0d2696d111c150a16896fc7a9b88af53", null ],
      [ "BG_AB_NODE_FARM", "_battle_ground_a_b_8h.html#a5883afd583529db00353140d7e0df0e8acbdce42b70229b7f8558d07e72159232", null ],
      [ "BG_AB_NODE_LUMBER_MILL", "_battle_ground_a_b_8h.html#a5883afd583529db00353140d7e0df0e8a4ba9970fd23b48756d7a9cb2774752d7", null ],
      [ "BG_AB_NODE_GOLD_MINE", "_battle_ground_a_b_8h.html#a5883afd583529db00353140d7e0df0e8af43583c1b4343a79b3d0a67deaac6aaa", null ],
      [ "BG_AB_NODES_ERROR", "_battle_ground_a_b_8h.html#a5883afd583529db00353140d7e0df0e8ab7bd6d04988585d55cb046bee357da13", null ]
    ] ],
    [ "BG_AB_NodeStatus", "_battle_ground_a_b_8h.html#a57b60dbaa9b05ef4b896e2b8ffebf130", [
      [ "BG_AB_NODE_TYPE_NEUTRAL", "_battle_ground_a_b_8h.html#a57b60dbaa9b05ef4b896e2b8ffebf130ab63ee89b82f2053b1bf42f647a77ee65", null ],
      [ "BG_AB_NODE_TYPE_CONTESTED", "_battle_ground_a_b_8h.html#a57b60dbaa9b05ef4b896e2b8ffebf130adbf00c634f3c4804f51deafcfb91fc82", null ],
      [ "BG_AB_NODE_STATUS_ALLY_CONTESTED", "_battle_ground_a_b_8h.html#a57b60dbaa9b05ef4b896e2b8ffebf130a11b9fdaafefa59b0717fb3c7a48d2db1", null ],
      [ "BG_AB_NODE_STATUS_HORDE_CONTESTED", "_battle_ground_a_b_8h.html#a57b60dbaa9b05ef4b896e2b8ffebf130aab0f7989996571b7596c15a5439d7be7", null ],
      [ "BG_AB_NODE_TYPE_OCCUPIED", "_battle_ground_a_b_8h.html#a57b60dbaa9b05ef4b896e2b8ffebf130a116568b3f890fbf76e2eedfc48d278c6", null ],
      [ "BG_AB_NODE_STATUS_ALLY_OCCUPIED", "_battle_ground_a_b_8h.html#a57b60dbaa9b05ef4b896e2b8ffebf130a468b2ba9d157fe9244da9027cf21c3e4", null ],
      [ "BG_AB_NODE_STATUS_HORDE_OCCUPIED", "_battle_ground_a_b_8h.html#a57b60dbaa9b05ef4b896e2b8ffebf130a5647376f7b315685f47525be14b1af97", null ]
    ] ],
    [ "BG_AB_Score", "_battle_ground_a_b_8h.html#af64af1ad6437b16ad565f1b957ad1889", [
      [ "BG_AB_WARNING_NEAR_VICTORY_SCORE", "_battle_ground_a_b_8h.html#af64af1ad6437b16ad565f1b957ad1889a3f2ba4902de6da31bbf923ed4dee5d9b", null ],
      [ "BG_AB_MAX_TEAM_SCORE", "_battle_ground_a_b_8h.html#af64af1ad6437b16ad565f1b957ad1889a83d800e593f1a661408cffc977d441ba", null ]
    ] ],
    [ "BG_AB_Sounds", "_battle_ground_a_b_8h.html#afa4330ec402735b2df3a043a6b018338", [
      [ "BG_AB_SOUND_NODE_CLAIMED", "_battle_ground_a_b_8h.html#afa4330ec402735b2df3a043a6b018338a82345118263a37caa33c0f0fa160ee58", null ],
      [ "BG_AB_SOUND_NODE_CAPTURED_ALLIANCE", "_battle_ground_a_b_8h.html#afa4330ec402735b2df3a043a6b018338adec8cd5886a496342cf2ade674f4edf6", null ],
      [ "BG_AB_SOUND_NODE_CAPTURED_HORDE", "_battle_ground_a_b_8h.html#afa4330ec402735b2df3a043a6b018338a294a068637e38497ebf3b514ea17c1f1", null ],
      [ "BG_AB_SOUND_NODE_ASSAULTED_ALLIANCE", "_battle_ground_a_b_8h.html#afa4330ec402735b2df3a043a6b018338a6619c65dabbb9b7b10de5cf313681b77", null ],
      [ "BG_AB_SOUND_NODE_ASSAULTED_HORDE", "_battle_ground_a_b_8h.html#afa4330ec402735b2df3a043a6b018338a6b49dfb9c85315c1a68adf6db98dc5fe", null ],
      [ "BG_AB_SOUND_NEAR_VICTORY", "_battle_ground_a_b_8h.html#afa4330ec402735b2df3a043a6b018338a66761cec31bffc96ca5916cf986b3341", null ]
    ] ],
    [ "BG_AB_Timers", "_battle_ground_a_b_8h.html#abb9d6bcfbed87044bd8a01c41a4ea74d", [
      [ "BG_AB_FLAG_CAPTURING_TIME", "_battle_ground_a_b_8h.html#abb9d6bcfbed87044bd8a01c41a4ea74da3de28f8d2bbeffeb1df9d791dda332db", null ]
    ] ],
    [ "BG_AB_WorldStates", "_battle_ground_a_b_8h.html#ae849ec0a797f15687258407024888c02", [
      [ "BG_AB_OP_OCCUPIED_BASES_HORDE", "_battle_ground_a_b_8h.html#ae849ec0a797f15687258407024888c02a3ad015982c6baf49277e16dbfac44150", null ],
      [ "BG_AB_OP_OCCUPIED_BASES_ALLY", "_battle_ground_a_b_8h.html#ae849ec0a797f15687258407024888c02ad2a640ea69dd05b09b1ef49fcd62ccfa", null ],
      [ "BG_AB_OP_RESOURCES_ALLY", "_battle_ground_a_b_8h.html#ae849ec0a797f15687258407024888c02a2bdd9f5197e31973507f447f3d2652f2", null ],
      [ "BG_AB_OP_RESOURCES_HORDE", "_battle_ground_a_b_8h.html#ae849ec0a797f15687258407024888c02a50866b1eabd65b69b345046dcd345e0b", null ],
      [ "BG_AB_OP_RESOURCES_MAX", "_battle_ground_a_b_8h.html#ae849ec0a797f15687258407024888c02a2ec77c16d7da678f5cb99206fabd8ef6", null ],
      [ "BG_AB_OP_RESOURCES_WARNING", "_battle_ground_a_b_8h.html#ae849ec0a797f15687258407024888c02acdfbef5593bc4d76444ac7a2c4fec289", null ]
    ] ],
    [ "BG_AB_BuffPositions", "_battle_ground_a_b_8h.html#a54a0697611ddf8a753bff84f4b273c54", null ],
    [ "BG_AB_GraveyardIds", "_battle_ground_a_b_8h.html#ae66da2b6eaf077c44490c1027d19a103", null ],
    [ "BG_AB_OP_NODEICONS", "_battle_ground_a_b_8h.html#a9efbad05f5d3938bae91d341e08e31f8", null ],
    [ "BG_AB_OP_NODESTATES", "_battle_ground_a_b_8h.html#acee2663161a0139ac7ed3ffe50fb2fd2", null ],
    [ "BG_AB_PerTickHonor", "_battle_ground_a_b_8h.html#ace00d915b3d1fb07a792d7dcc1c4e23e", null ],
    [ "BG_AB_TickIntervals", "_battle_ground_a_b_8h.html#a5b8798ae5665a3eefe01df49379ee58d", null ],
    [ "BG_AB_TickPoints", "_battle_ground_a_b_8h.html#a2fac7700e4075ae6945aca942c3d5034", null ],
    [ "BG_AB_WinMatchHonor", "_battle_ground_a_b_8h.html#a893c76d13c0b785b8de5b21d53a2768e", null ]
];