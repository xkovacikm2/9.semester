var class_spell =
[
    [ "GOTargetInfo", "struct_spell_1_1_g_o_target_info.html", "struct_spell_1_1_g_o_target_info" ],
    [ "ItemTargetInfo", "struct_spell_1_1_item_target_info.html", "struct_spell_1_1_item_target_info" ],
    [ "TargetInfo", "struct_spell_1_1_target_info.html", "struct_spell_1_1_target_info" ],
    [ "GOTargetList", "class_spell.html#af428f77d9eda4237542a7377a8a380fb", null ],
    [ "ItemTargetList", "class_spell.html#aad4acf5422ead2d643c6c6053317b82a", null ],
    [ "SpellInfoList", "class_spell.html#a005c4cf0cc4add41c42b4710b48acc6b", null ],
    [ "TargetList", "class_spell.html#ad09573137899b70c341c8c56198d4584", null ],
    [ "UnitList", "class_spell.html#af0814d910fab5aa813d07365e4120bae", null ],
    [ "Spell", "class_spell.html#a5cde8acc0b74b3d67fa8c2ba57a929ea", null ],
    [ "~Spell", "class_spell.html#ab00bfde34dc5dcdc6e1035608e14c067", null ],
    [ "_handle_finish_phase", "class_spell.html#a4678d2bf3013f3bef4c29e6b11245b3c", null ],
    [ "_handle_immediate_phase", "class_spell.html#a7e2f06d9f8d074db174bce10f2c5165e", null ],
    [ "AddGOTarget", "class_spell.html#a4e70acc177f89e2c4a59e647db20b228", null ],
    [ "AddGOTarget", "class_spell.html#ad3d3f641a54c1120c983a953f9ff44c9", null ],
    [ "AddItemTarget", "class_spell.html#a3177cb6d83604d83c59e88949f77cbac", null ],
    [ "AddPrecastSpell", "class_spell.html#a136f21422b9570b3573ad35e400de4a5", null ],
    [ "AddPrecastSpell", "class_spell.html#aee79a6cf1400ecc2a06a04db0359585c", null ],
    [ "AddTriggeredSpell", "class_spell.html#a564ed70ad4ac1567e58df40f8b8dd151", null ],
    [ "AddTriggeredSpell", "class_spell.html#af2e2b62fc427c19fa9e1dfff19534a1b", null ],
    [ "AddUnitTarget", "class_spell.html#a7e81b15777b1341f551af1fba797d1c3", null ],
    [ "AddUnitTarget", "class_spell.html#a22905f07256a67de192ce182e9c5abdc", null ],
    [ "CalculateDamage", "class_spell.html#a8f8bd6e45d95b9e754b27a07f2417d01", null ],
    [ "CanAutoCast", "class_spell.html#aff5477a54c2dfe98575d5ea12984b86f", null ],
    [ "cancel", "class_spell.html#aa50ac7bf59f5e8ff2afca8de8d29fb28", null ],
    [ "CancelGlobalCooldown", "class_spell.html#a6ad76e29c8c7a1f9242f9b59744d647c", null ],
    [ "CanOpenLock", "class_spell.html#af361f5b5f94a3bab5be215e028ff0ec8", null ],
    [ "CanTameUnit", "class_spell.html#a19fb60671d52766e4ca217da83c4b5d9", null ],
    [ "cast", "class_spell.html#a1c2c59eb34d0bd66300708cdedd83805", null ],
    [ "CastPreCastSpells", "class_spell.html#ae56cd0dc9a0fcece5365d59925da40f1", null ],
    [ "CastTriggerSpells", "class_spell.html#a704cd4f425ebf48be2bed2fd22b22571", null ],
    [ "CheckCast", "class_spell.html#ab2a42ecc2773f29d512625f1fb2ab17d", null ],
    [ "CheckCasterAuras", "class_spell.html#a7f10a0a0b2c0c522add0deecda8fa1b1", null ],
    [ "CheckItems", "class_spell.html#a516f3d4404f0ca2010d21d8c64d478ee", null ],
    [ "CheckPetCast", "class_spell.html#ab1dea568ff32c16850a58cf34aa5b5ea", null ],
    [ "CheckPower", "class_spell.html#a987ed6b9754005e13e2321b26708cc29", null ],
    [ "CheckRange", "class_spell.html#af29009de4460469d4a4157fa1eda8173", null ],
    [ "CheckTarget", "class_spell.html#a89082aa6f0f7250737058ec120ec1e58", null ],
    [ "CheckTargetCreatureType", "class_spell.html#ace3b37eed1982a9529fbaae71b10e044", null ],
    [ "CleanupTargetList", "class_spell.html#a6128294a5ab1a765cc51d55c31edb3f4", null ],
    [ "ClearCastItem", "class_spell.html#ad93507f6abc44a9a3334f3e9a5cc8825", null ],
    [ "Delayed", "class_spell.html#aa19f9a8629e5ff119c87c8308c8d08d6", null ],
    [ "DelayedChannel", "class_spell.html#ae9198fc52a22dbc82ad69b1c32d884e7", null ],
    [ "DoAllEffectOnTarget", "class_spell.html#afecdd65c73b267d8cb2b9525bed5f851", null ],
    [ "DoAllEffectOnTarget", "class_spell.html#ae31ef225c3a9ccdcfdc8c0db2b786dda", null ],
    [ "DoAllEffectOnTarget", "class_spell.html#a32e917a293fa4ebd41a1cd1914debaff", null ],
    [ "DoCreateItem", "class_spell.html#aca7666bd03d3be54c5f4574adf6cf1ef", null ],
    [ "DoSpellHitOnUnit", "class_spell.html#a5297563e2a7c0f47fd848804cf1af2da", null ],
    [ "EffectActivateObject", "class_spell.html#a84a5290c8f0c2156a6724bd5ecbfece4", null ],
    [ "EffectAddComboPoints", "class_spell.html#aabd43b832117731656ee05e5111adee0", null ],
    [ "EffectAddExtraAttacks", "class_spell.html#aff84afd6d6a3043ef46b0c7447df4dd9", null ],
    [ "EffectAddFarsight", "class_spell.html#ac1c4d7bda9e5e8c7f117ac21b62272cb", null ],
    [ "EffectAddHonor", "class_spell.html#ab73d468b77e1a669ff9d0bcabb479b8d", null ],
    [ "EffectApplyAreaAura", "class_spell.html#ac168037a37cd80a7a2bba02e946982e5", null ],
    [ "EffectApplyAura", "class_spell.html#a1cbe4c46ffd95db255567e58b1df30b4", null ],
    [ "EffectBind", "class_spell.html#a91cae9afc352c3203ee5830c0d761ece", null ],
    [ "EffectBlock", "class_spell.html#abf7e8ccc897ba73fbb09d960c91a912c", null ],
    [ "EffectCharge", "class_spell.html#a21fdf3cd53959925d4611830cbfb227f", null ],
    [ "EffectCreateItem", "class_spell.html#a58cb4d8ae4b4f0a0d32d257e119be111", null ],
    [ "EffectDestroyAllTotems", "class_spell.html#a9f66319645b0879a9cfd16f63d45e191", null ],
    [ "EffectDisEnchant", "class_spell.html#a50f13be135ade00d788e9e6b3d350fe8", null ],
    [ "EffectDismissPet", "class_spell.html#a39c70ef888aea55cbc50d80f12155858", null ],
    [ "EffectDispel", "class_spell.html#ab811e07345c7865ebd70243ec154635f", null ],
    [ "EffectDispelMechanic", "class_spell.html#a5051038e6416abda581022ba16f96e51", null ],
    [ "EffectDistract", "class_spell.html#aa6ab2e4009c8d826abea614f7c1a3779", null ],
    [ "EffectDualWield", "class_spell.html#a5c718efa22cae938dc35491277dd7d8c", null ],
    [ "EffectDuel", "class_spell.html#a103814e3870ec02b5ec22849ce444147", null ],
    [ "EffectDummy", "class_spell.html#a21cb6ac833e2b21caaa2cf79d165a8ff", null ],
    [ "EffectDurabilityDamage", "class_spell.html#a7c3e34de3b11cd6ff31382d2159d9716", null ],
    [ "EffectDurabilityDamagePCT", "class_spell.html#ae280ac34d3ccde438277ef04011a81f8", null ],
    [ "EffectEmpty", "class_spell.html#a123a4cef099f1bdf5c30a320948246d3", null ],
    [ "EffectEnchantHeldItem", "class_spell.html#a02124519ab6693e2c0fa204ec401aa8b", null ],
    [ "EffectEnchantItemPerm", "class_spell.html#a5470ec0effa2a81dd33390f0b228cac0", null ],
    [ "EffectEnchantItemTmp", "class_spell.html#a3ddd470431385ce8c02282d45fab29e6", null ],
    [ "EffectEnergize", "class_spell.html#ab3f31cce5910376f34bef92ef29e6287", null ],
    [ "EffectEnvironmentalDMG", "class_spell.html#af8a363ee8fc23d4fb7ff07ce1e0f777c", null ],
    [ "EffectFeedPet", "class_spell.html#ac058ff5f73387fb1ef91934f0597b3b3", null ],
    [ "EffectHeal", "class_spell.html#a264e896dbc6c814a6e882fe8f44a5755", null ],
    [ "EffectHealMaxHealth", "class_spell.html#a816feb47faddee463f7f769210b257e0", null ],
    [ "EffectHealMechanical", "class_spell.html#a1e8642fc6d0fe40244c27d674661ac47", null ],
    [ "EffectHealthLeech", "class_spell.html#a99dd2a9fb9e5d6c47de17bd3f929e846", null ],
    [ "EffectInebriate", "class_spell.html#a9277d1b7a516c639f73297e0e8ca189c", null ],
    [ "EffectInstaKill", "class_spell.html#ac634f979279e47897fc58b3c606e292a", null ],
    [ "EffectInterruptCast", "class_spell.html#a1045b77654aa1983bf0a841d6480e8e1", null ],
    [ "EffectKnockBack", "class_spell.html#a735596460498d9025b83d355c5559745", null ],
    [ "EffectLeapForward", "class_spell.html#a0c6343500f9c163b030eaeb5049e7118", null ],
    [ "EffectLearnPetSpell", "class_spell.html#ab7dbdc1e289dcf2569a7c3132310a937", null ],
    [ "EffectLearnSkill", "class_spell.html#a1c63cd65dabf5dcb8782af764080f451", null ],
    [ "EffectLearnSpell", "class_spell.html#a990173f7ed8ee4029dfada203009515b", null ],
    [ "EffectModifyThreatPercent", "class_spell.html#a4dd75fd8073f94f9b0de587323613079", null ],
    [ "EffectNULL", "class_spell.html#aa0485e72f733dc5abc39d7ca74515317", null ],
    [ "EffectOpenLock", "class_spell.html#a566fb9806cada38efcf1e30b20576e37", null ],
    [ "EffectParry", "class_spell.html#ad712861f30aa79f565f38be541909d85", null ],
    [ "EffectPersistentAA", "class_spell.html#a4a5c9812ab55bf23109397767d44371c", null ],
    [ "EffectPickPocket", "class_spell.html#ab534aacbb570ad0635d68b718ea8a67b", null ],
    [ "EffectPlayerPull", "class_spell.html#a0e7cd52ee73fe14ac0dc28754e79b642", null ],
    [ "EffectPlayMusic", "class_spell.html#aa583a76c5ed1523880664ea53fb6da3c", null ],
    [ "EffectPowerBurn", "class_spell.html#afe2d2ebe1e1bd4f0d4ac9c2c69528b56", null ],
    [ "EffectPowerDrain", "class_spell.html#a1c90f79bdbd63d0aa1d0588ab320ad18", null ],
    [ "EffectProficiency", "class_spell.html#aaabc3734e1f95b82d4a7e5032f5cacd6", null ],
    [ "EffectPull", "class_spell.html#a5c7b16ef4d23e0a7b13ca02ab9e4fdaf", null ],
    [ "EffectQuestComplete", "class_spell.html#a56fae6194d864ea20047c667c061b28f", null ],
    [ "EffectReputation", "class_spell.html#abffbd12a5d960deafca828749667433f", null ],
    [ "EffectResurrect", "class_spell.html#a10a8ac85b4d85b71e7dad0ef2d1e81cf", null ],
    [ "EffectResurrectNew", "class_spell.html#ae7006fd8f97f1a6d6544497d2dc4b8c9", null ],
    [ "EffectSanctuary", "class_spell.html#a30b4ecd3c9f98eeed8c0ca744eda3008", null ],
    [ "EffectSchoolDMG", "class_spell.html#ad81a48c51579c58a769a2e8354948b49", null ],
    [ "EffectScriptEffect", "class_spell.html#a9d7d74e9a801f44cc5c9aba7fd40b4fc", null ],
    [ "EffectSelfResurrect", "class_spell.html#a2cafc4581cfe2050b2bca5b36702266c", null ],
    [ "EffectSendEvent", "class_spell.html#a4260df7d04a1e10f861a855d4e464452", null ],
    [ "EffectSendTaxi", "class_spell.html#a02f8827a166f4618057e787342d29a20", null ],
    [ "EffectSkill", "class_spell.html#a9e18463a7c71d77ea38828a158728e11", null ],
    [ "EffectSkinning", "class_spell.html#ad23d34ccee1af842b845af828a25ab1e", null ],
    [ "EffectSkinPlayerCorpse", "class_spell.html#a36fb04bcd296ff8136cd9b3bf27abaa1", null ],
    [ "EffectSpiritHeal", "class_spell.html#a614362634586f79eb0b51eb72b4f7d40", null ],
    [ "EffectStuck", "class_spell.html#a650728e1116e440f1b571be3a1dd76c6", null ],
    [ "EffectSummon", "class_spell.html#a72513c785279fbe61e7f22e88201dae1", null ],
    [ "EffectSummonChangeItem", "class_spell.html#a319e0eae5f6d469a8af0fc041f4d00ed", null ],
    [ "EffectSummonCritter", "class_spell.html#a1731790497dc6f713cb22798e00587ca", null ],
    [ "EffectSummonDeadPet", "class_spell.html#a5796bf709be48f4c52ef94d7271393a8", null ],
    [ "EffectSummonDemon", "class_spell.html#a2020c408949f02183414c794ab1b2c04", null ],
    [ "EffectSummonGuardian", "class_spell.html#ad4ee52e24c64bf33908910c37fe6fc8b", null ],
    [ "EffectSummonObject", "class_spell.html#a99713d98d1f2e789d388cc9c045170bf", null ],
    [ "EffectSummonObjectWild", "class_spell.html#a056a473387c570142cebebb7c2b9d90f", null ],
    [ "EffectSummonPet", "class_spell.html#afabad26db282b5a380915b7d2b39b27d", null ],
    [ "EffectSummonPlayer", "class_spell.html#ae71a6c54e0d66f2c7746f9336a3e961a", null ],
    [ "EffectSummonPossessed", "class_spell.html#a17b567d02ecd29452ef06ffcc63ff57c", null ],
    [ "EffectSummonTotem", "class_spell.html#a4eaa95cabdc5303999da1209685e895d", null ],
    [ "EffectSummonWild", "class_spell.html#a70214b311bdf4a855b6f53f1bd56f958", null ],
    [ "EffectTameCreature", "class_spell.html#abad4fd1ce58d4398fc9125a9cef77ae8", null ],
    [ "EffectTaunt", "class_spell.html#a513bbc594d6ecba4dc08535e8c111507", null ],
    [ "EffectTeleportUnits", "class_spell.html#a082aa00e624e522fc3a9437c8b3f671f", null ],
    [ "EffectTeleUnitsFaceCaster", "class_spell.html#a0066ba26b4f96e95a4039b702c990b9b", null ],
    [ "EffectThreat", "class_spell.html#ae04f5f18ad33c0bd007908eaa8d13bb4", null ],
    [ "EffectTradeSkill", "class_spell.html#a613c300c57d67ff9383cdabb1571dd81", null ],
    [ "EffectTransmitted", "class_spell.html#a95e7a97b34ed239cc88ac9fe4de19028", null ],
    [ "EffectTriggerMissileSpell", "class_spell.html#a80a27ad6313528ac4301f541a6cfbce2", null ],
    [ "EffectTriggerSpell", "class_spell.html#aed3d7b061dbfed716f0382e39c585350", null ],
    [ "EffectUnused", "class_spell.html#a1fd95fccfdc725406be230b91e6f981c", null ],
    [ "EffectWeaponDmg", "class_spell.html#ac832affbda2f084d42e93eb681749a71", null ],
    [ "FillAreaTargets", "class_spell.html#a02a0949a687c30644ecca0f44dd62a06", null ],
    [ "FillRaidOrPartyTargets", "class_spell.html#a05d246b24ca65ef72c2b5e0d505a6220", null ],
    [ "FillTargetMap", "class_spell.html#aae8b05fa2c3fad700a3dca8c4d154e94", null ],
    [ "FindCorpseUsing", "class_spell.html#ac5331b13cbc4d1bc6ce1d68878f7a529", null ],
    [ "finish", "class_spell.html#af0af54631e2f159c5a70092bafac71c6", null ],
    [ "GetAffectiveCaster", "class_spell.html#a52c455dff0b1da39744da634d9f6be8d", null ],
    [ "GetAffectiveCasterObject", "class_spell.html#a8595ca62f33802afbbc63c35448ec52e", null ],
    [ "GetCastedTime", "class_spell.html#a4ff356afb1b818e68d665d987a38132d", null ],
    [ "GetCaster", "class_spell.html#a3bcd646cc44a9b80aa804f984c289cf3", null ],
    [ "GetCastingObject", "class_spell.html#a50a90dcb7f36854aff094c5b4fe45aa0", null ],
    [ "GetCastTime", "class_spell.html#a5fd7b2fa3f7559c425c367852e187846", null ],
    [ "GetCurrentContainer", "class_spell.html#a17951e3df50b5a84ff4e66e2c57c1477", null ],
    [ "GetDelayMoment", "class_spell.html#ad39149ba8f34185ed6c52c5782881250", null ],
    [ "GetDelayStart", "class_spell.html#acd106ca9dbe908ea1329c05d1fdbffc4", null ],
    [ "GetNextDelayAtDamageMsTime", "class_spell.html#a34c43475c3bd2fa182f6e9565fee82a5", null ],
    [ "GetPowerCost", "class_spell.html#aa17fd375e267fb7e66e526102953714d", null ],
    [ "GetPrefilledOrUnitTargetGuid", "class_spell.html#a5913ab670a72accabcbb635a83403504", null ],
    [ "GetSelfContainer", "class_spell.html#aa3e78eff4d477986216df1eae4d53353", null ],
    [ "GetSpellRangeAndRadius", "class_spell.html#aa71669017900ebcc58853ca522dd7ff7", null ],
    [ "getState", "class_spell.html#a6fea183fab3cd5f8946923c3d8909f5a", null ],
    [ "handle_delayed", "class_spell.html#a98e6d38fef38925272ed77fae54903b2", null ],
    [ "handle_immediate", "class_spell.html#a323570cf86a1b20a8e64a9db971cab8c", null ],
    [ "HandleDelayedSpellLaunch", "class_spell.html#a15d3c688199eb331ca97a8ead952aa3e", null ],
    [ "HandleEffects", "class_spell.html#a4cc01b16c2f009aff61d7e27b04b4b09", null ],
    [ "HandleThreatSpells", "class_spell.html#a68b7071f7708ddf46cb20e2e76e414ad", null ],
    [ "HasGlobalCooldown", "class_spell.html#ae732aefc7f491481aae3a033073fbfef", null ],
    [ "HaveTargetsForEffect", "class_spell.html#a8de82de2d1fc29d0874f2ad183f8f2a7", null ],
    [ "IgnoreItemRequirements", "class_spell.html#af84a8ccfbfa113458cd96927a0bd65d6", null ],
    [ "InitializeDamageMultipliers", "class_spell.html#ac99404294f7725f4bd29e70e015e3d47", null ],
    [ "IsAliveUnitPresentInTargetList", "class_spell.html#a389780258153b86d112f9628100d419a", null ],
    [ "IsAutoRepeat", "class_spell.html#a3378c5e643dbbde893606505c42817b5", null ],
    [ "IsChannelActive", "class_spell.html#aaf9581e80903c8fb7f96ea3e1d9e0173", null ],
    [ "IsDeletable", "class_spell.html#a6032f8f164df1d6fc2607e4ca5c35ce5", null ],
    [ "IsMeleeAttackResetSpell", "class_spell.html#a477d202997ee1650e937c3b6804f93e1", null ],
    [ "IsNeedSendToClient", "class_spell.html#a3ed06c7cdee02042bd42e23a21733bdd", null ],
    [ "IsNextMeleeSwingSpell", "class_spell.html#ab1df43b19e74ad30219358adffe098bb", null ],
    [ "IsRangedAttackResetSpell", "class_spell.html#a000cb9a620570de18a5a9acea6a8fb24", null ],
    [ "IsRangedSpell", "class_spell.html#ae13ca73b08aafc7636ee0449c186b0ab", null ],
    [ "IsTriggeredSpellWithRedundentCastTime", "class_spell.html#a13ecce110978c170c71f477e5abfcf02", null ],
    [ "prepare", "class_spell.html#a455eb6493c5ab7a7f8e7a62e008be955", null ],
    [ "prepareDataForTriggerSystem", "class_spell.html#a3708df24a12e888277bf67285487118e", null ],
    [ "ResetEffectDamageAndHeal", "class_spell.html#ac34348062658c54529e2d7f0e108e739", null ],
    [ "ReSetTimer", "class_spell.html#ab59bbf4428bcb938c7e9af5a6ee9812a", null ],
    [ "SendCastResult", "class_spell.html#ae98f311ace07d1cb60074ea1a9e91cc8", null ],
    [ "SendChannelStart", "class_spell.html#a1efb9631f004c31d06b0552c2edbc10f", null ],
    [ "SendChannelUpdate", "class_spell.html#a4c57115e0a422b49facecc75a6e25808", null ],
    [ "SendInterrupted", "class_spell.html#ac7ae05d39b08ef5b5672aaab10c0b129", null ],
    [ "SendLogExecute", "class_spell.html#a52dd22675529ac80245fef319af4bf91", null ],
    [ "SendLoot", "class_spell.html#a52debf9fc183a49d96925736578e4b21", null ],
    [ "SendResurrectRequest", "class_spell.html#adf73d7635455658b3a96095e59e20d68", null ],
    [ "SendSpellCooldown", "class_spell.html#a7e93f29ca09727fabcd6d4445029ca4a", null ],
    [ "SendSpellGo", "class_spell.html#a3244e8c8f24aa8fdaec7b236a9532724", null ],
    [ "SendSpellStart", "class_spell.html#a48b9d93136110a4bee3b3a482959becb", null ],
    [ "SetAutoRepeat", "class_spell.html#ab654508d3e73907a155286a047fa9b85", null ],
    [ "SetDelayStart", "class_spell.html#a9572cd3b12252c7c177b0f6151a1c57b", null ],
    [ "SetExecutedCurrently", "class_spell.html#a48371fc36b56594c7ec8df04292b195b", null ],
    [ "SetReferencedFromCurrent", "class_spell.html#ac84f3aeac1e0d86f97d3ed63c7881b48", null ],
    [ "SetSelfContainer", "class_spell.html#a86ab7e81f7a3d4984de41cb8bb445d0c", null ],
    [ "setState", "class_spell.html#a2778875470c79e6425a956ca0b89c3af", null ],
    [ "SetTargetMap", "class_spell.html#a77b2be8c463a272d5ca8b305d944fcd7", null ],
    [ "TakeAmmo", "class_spell.html#a5d48e255fcfcafa165137f39c73a6fcb", null ],
    [ "TakeCastItem", "class_spell.html#ad827f88458117266b82decb339b9ff88", null ],
    [ "TakePower", "class_spell.html#ab191335cf0379cfc4483c6404eebd285", null ],
    [ "TakeReagents", "class_spell.html#a39bddfec4b1d97d380972515a7d26314", null ],
    [ "TriggerGlobalCooldown", "class_spell.html#a613280e0bb3fb91996ef0b44b89d4a1a", null ],
    [ "update", "class_spell.html#acc435ce7ca5fc0510122e720a6aea580", null ],
    [ "UpdateOriginalCasterPointer", "class_spell.html#af4432b3a613d924a4bdc8f68e9c99dac", null ],
    [ "UpdatePointers", "class_spell.html#a00dbf3e8c4ec3aa165e7bddb7cfeecd8", null ],
    [ "WriteAmmoToPacket", "class_spell.html#ac3534e44fb4f5c7e20bfc3684f005601", null ],
    [ "WriteSpellGoTargets", "class_spell.html#a5658839715a1d19fa3925e89896a5f7d", null ],
    [ "MaNGOS::SpellNotifierCreatureAndPlayer", "class_spell.html#a78b27204d2371c25ec729076f43dda51", null ],
    [ "MaNGOS::SpellNotifierPlayer", "class_spell.html#a7c5b9e08c8d67ef0b8b97855f625dbcd", null ],
    [ "Unit::SetCurrentCastedSpell", "class_spell.html#a61c5e93627b85645d6a3b0a5af13d58a", null ],
    [ "damage", "class_spell.html#a33c6addb2f24e2bb07610004409c0686", null ],
    [ "focusObject", "class_spell.html#aeea039859c2f94a60300b6c744e907b9", null ],
    [ "gameObjTarget", "class_spell.html#a0e7f35f88f4bad9f5599c67fde08f98a", null ],
    [ "itemTarget", "class_spell.html#a6634a11a4bb1806304a33e3bc10b7c28", null ],
    [ "m_applyMultiplierMask", "class_spell.html#ac63ea9310b09ececfc1eac19133abce1", null ],
    [ "m_attackType", "class_spell.html#a538a5dba7f22c362a76d40252ae3dfd1", null ],
    [ "m_autoRepeat", "class_spell.html#af8e7a207583b68ff524b4372e995664a", null ],
    [ "m_canReflect", "class_spell.html#a10f7fb2e01864e22245b8053baf14377", null ],
    [ "m_canTrigger", "class_spell.html#a657688194c718b1e193bfa0b9caac645", null ],
    [ "m_caster", "class_spell.html#a84f13461f1f94ff882b111cd926b0b23", null ],
    [ "m_CastItem", "class_spell.html#ad211ab1945462c7724e025b88b0c2135", null ],
    [ "m_castOrientation", "class_spell.html#a7bdeaabf8fa127e4269e683c2bd76499", null ],
    [ "m_castPositionX", "class_spell.html#a72d630ee8147b4197d46503dd2c9ed34", null ],
    [ "m_castPositionY", "class_spell.html#a7a1b42fb496a662f928a3f3f2e738bcc", null ],
    [ "m_castPositionZ", "class_spell.html#a5e4e64d34f282231e10ec16596700432", null ],
    [ "m_casttime", "class_spell.html#a61b0b397250bb79024c8657e28323879", null ],
    [ "m_currentBasePoints", "class_spell.html#ac6c221b279935d9dfdf840f569f62954", null ],
    [ "m_damage", "class_spell.html#a69d7714c4cbd81d3dbe8e16cc8cb74a3", null ],
    [ "m_damageMultipliers", "class_spell.html#a775626fa420464863cbc05128381c5ba", null ],
    [ "m_delayAtDamageCount", "class_spell.html#a4b89533356cf0fe417a3e646c30e5a04", null ],
    [ "m_delayMoment", "class_spell.html#a504753412a8af2e23f05313bbb5b6e66", null ],
    [ "m_delayStart", "class_spell.html#a21ad65d1dbdf61469baa14a0e49887b6", null ],
    [ "m_diminishGroup", "class_spell.html#acd73c0fbf1411460ce187b5c81d3a40e", null ],
    [ "m_diminishLevel", "class_spell.html#a719f40445ed763c1af9b21bc93984771", null ],
    [ "m_duration", "class_spell.html#a0c1365668be6064fbd231e068c77686a", null ],
    [ "m_executedCurrently", "class_spell.html#a70b015d3289a781f9f7097e6581cadf5", null ],
    [ "m_healing", "class_spell.html#a947de48edc8fd21483f27a59e9ffcb35", null ],
    [ "m_healthLeech", "class_spell.html#aeea292e4bbbeb073f624dabda6420903", null ],
    [ "m_immediateHandled", "class_spell.html#a5802fff89b8e765fe49b4e22e5fbdcf9", null ],
    [ "m_IsTriggeredSpell", "class_spell.html#abdd0530fa6e4eca6c67f818cfa4817c2", null ],
    [ "m_needAliveTargetMask", "class_spell.html#adcb37c1f1904971b48304f6d18d48ee5", null ],
    [ "m_needSpellLog", "class_spell.html#af880d166ee53ab6614c4bd885fa6e466", null ],
    [ "m_negativeEffectMask", "class_spell.html#a3ca8244d8c6cdc28ca9e8af8553e168f", null ],
    [ "m_originalCaster", "class_spell.html#a15299304003f041f28de7a0fd992c735", null ],
    [ "m_originalCasterGUID", "class_spell.html#a5b4ebeb10ad576a529d0556064930297", null ],
    [ "m_powerCost", "class_spell.html#aa6e5caa381a17c50d7c128fb14421b9e", null ],
    [ "m_preCastSpells", "class_spell.html#a9bc6be667fcb649021e8c4b8d6a0cc1f", null ],
    [ "m_procAttacker", "class_spell.html#aecb2b2a5f569bd67fd97bc349250e5f5", null ],
    [ "m_procVictim", "class_spell.html#a132f7a222814fd8c2fc838626c7731d5", null ],
    [ "m_referencedFromCurrentSpell", "class_spell.html#a10c83ebca0cec17982c4e6a849ec668f", null ],
    [ "m_selfContainer", "class_spell.html#ac23942bcb7851e8514438121d8fa5e0d", null ],
    [ "m_spellAuraHolder", "class_spell.html#abdba7f18e2606cd1e2e8d8004cc2b30b", null ],
    [ "m_spellInfo", "class_spell.html#a3e313035ac36249f3be6bab84b9e6863", null ],
    [ "m_spellSchoolMask", "class_spell.html#aa1eef1b2b1d1aec52584b95b9217b6fa", null ],
    [ "m_spellState", "class_spell.html#a587554773427224df51cbce7a50b9d2c", null ],
    [ "m_targets", "class_spell.html#a22f76da795704e74f5447bbaebe02d9c", null ],
    [ "m_timer", "class_spell.html#a10c2de905868b3a6ce5346d27686f9f6", null ],
    [ "m_triggeredByAuraSpell", "class_spell.html#a13c8ce3196151883d5daf89a3f82bff9", null ],
    [ "m_triggeredBySpellInfo", "class_spell.html#ab3d3ccf4db3daa4de3bdfd7fe3371eae", null ],
    [ "m_TriggerSpells", "class_spell.html#ab74dcb259c27daf0a123cc30049d1b37", null ],
    [ "m_UniqueGOTargetInfo", "class_spell.html#a4eefd51eef23ff7d5914b6c041fab41d", null ],
    [ "m_UniqueItemInfo", "class_spell.html#a4ba53019f02abcaff8db6e21aaf4a78e", null ],
    [ "m_UniqueTargetInfo", "class_spell.html#a5467441e13fb75e103e32f4975528c41", null ],
    [ "unitTarget", "class_spell.html#aa160274138dbebf153c189d8f4cdb59c", null ]
];