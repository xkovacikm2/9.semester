var _i_v_map_manager_8h =
[
    [ "IVMapManager", "class_v_m_a_p_1_1_i_v_map_manager.html", "class_v_m_a_p_1_1_i_v_map_manager" ],
    [ "VMAP_INVALID_HEIGHT", "_i_v_map_manager_8h.html#aa3e862468079e7d1ca850fab429fb316", null ],
    [ "VMAP_INVALID_HEIGHT_VALUE", "_i_v_map_manager_8h.html#a6d0d846568e1fbb60e05aacfa9c1c73c", null ],
    [ "VMAPLoadResult", "_i_v_map_manager_8h.html#a67954b5178cc2a4af5fa17c7a864d188", [
      [ "VMAP_LOAD_RESULT_ERROR", "_i_v_map_manager_8h.html#a67954b5178cc2a4af5fa17c7a864d188a514fc6255627123117d881d4968bf663", null ],
      [ "VMAP_LOAD_RESULT_OK", "_i_v_map_manager_8h.html#a67954b5178cc2a4af5fa17c7a864d188a942b44b8a6db80f6d4ab19ebad17192b", null ],
      [ "VMAP_LOAD_RESULT_IGNORED", "_i_v_map_manager_8h.html#a67954b5178cc2a4af5fa17c7a864d188addac30ca7168c74f716b6923498e0165", null ]
    ] ]
];