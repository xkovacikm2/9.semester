var _pool_manager_8cpp =
[
    [ "PoolMapChecker", "struct_pool_map_checker.html", "struct_pool_map_checker" ],
    [ "SpawnPoolInMapsWorker", "struct_spawn_pool_in_maps_worker.html", "struct_spawn_pool_in_maps_worker" ],
    [ "DespawnPoolInMapsWorker", "struct_despawn_pool_in_maps_worker.html", "struct_despawn_pool_in_maps_worker" ],
    [ "UpdatePoolInMapsWorker", "struct_update_pool_in_maps_worker.html", "struct_update_pool_in_maps_worker" ],
    [ "INSTANTIATE_SINGLETON_1", "_pool_manager_8cpp.html#a7dca3402ea5b33d995897f0916b39909", null ]
];