var class_terrain_info =
[
    [ "TerrainInfo", "class_terrain_info.html#a766baa312815f64bce584105855110ca", null ],
    [ "~TerrainInfo", "class_terrain_info.html#a7b694d3fc2c743a10e59894d63cb0943", null ],
    [ "CleanUpGrids", "class_terrain_info.html#ab3d35c4b458e81566d37efc685110b12", null ],
    [ "GetAreaFlag", "class_terrain_info.html#a9862605fcf79281ab20b8e40fa71135f", null ],
    [ "GetAreaId", "class_terrain_info.html#a99aecf7e69cc57f2af1ef28ad48ed775", null ],
    [ "GetAreaInfo", "class_terrain_info.html#abfd77aa1b394ae877c0e643789433d56", null ],
    [ "GetHeightStatic", "class_terrain_info.html#a68727505260e954a7304404a9b780790", null ],
    [ "getLiquidStatus", "class_terrain_info.html#ad7e8015dc726ae292ecf8c81a3d5e0a7", null ],
    [ "GetMapId", "class_terrain_info.html#a40ba218439c0b25f41bf8ea3c4a39c3b", null ],
    [ "GetTerrainType", "class_terrain_info.html#ad53249ad33284641e4aca7e91f991c18", null ],
    [ "GetWaterLevel", "class_terrain_info.html#a05c1aa93058ee75bd48f137b7ec826bd", null ],
    [ "GetWaterOrGroundLevel", "class_terrain_info.html#a6080cb246a6aacaa17dbd7781b26908b", null ],
    [ "GetZoneAndAreaId", "class_terrain_info.html#accef84574836d234cd293a0841393a0d", null ],
    [ "GetZoneId", "class_terrain_info.html#a3487a73496a050a4c214e9d8dc8194fb", null ],
    [ "IsInWater", "class_terrain_info.html#a230dcf0440dfeac10b60eee329d7e219", null ],
    [ "IsOutdoors", "class_terrain_info.html#a88601853eed2beb56da806fea75059cf", null ],
    [ "IsSwimmable", "class_terrain_info.html#aeea12764d54cb16a91210722eb3fe6ac", null ],
    [ "IsUnderWater", "class_terrain_info.html#ae5fec72528305921b99c28bc91103af6", null ],
    [ "Load", "class_terrain_info.html#a8f709cfd59afbd11283eed38d02e175c", null ],
    [ "Unload", "class_terrain_info.html#ad3c18c8b9ce0a7e4223c1768d7075f99", null ],
    [ "Map", "class_terrain_info.html#ad2f32e921244459f7cc6d50355429cc6", null ]
];