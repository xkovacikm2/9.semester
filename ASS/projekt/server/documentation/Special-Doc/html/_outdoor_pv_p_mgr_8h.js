var _outdoor_pv_p_mgr_8h =
[
    [ "CapturePointSlider", "struct_capture_point_slider.html", "struct_capture_point_slider" ],
    [ "OutdoorPvPMgr", "class_outdoor_pv_p_mgr.html", "class_outdoor_pv_p_mgr" ],
    [ "sOutdoorPvPMgr", "_outdoor_pv_p_mgr_8h.html#a7f16e829e6dd717c8a6a20fd1aa783cc", null ],
    [ "CapturePointSliderMap", "_outdoor_pv_p_mgr_8h.html#ab3ea6d2052c992f3bd811bc60c9c4081", null ],
    [ "TIMER_OPVP_MGR_UPDATE", "_outdoor_pv_p_mgr_8h.html#a7ee8d0f117a79ca7eb1e0076a9182bcba764b734d933925f24971f1bee6fe0b18", null ],
    [ "OutdoorPvPTypes", "_outdoor_pv_p_mgr_8h.html#a56e7764289940901c2a230103f78adee", [
      [ "OPVP_ID_SI", "_outdoor_pv_p_mgr_8h.html#a56e7764289940901c2a230103f78adeead0567d95b6c07b93d6c9c84cce75851f", null ],
      [ "OPVP_ID_EP", "_outdoor_pv_p_mgr_8h.html#a56e7764289940901c2a230103f78adeea6d9e0ed7b779c9649a0e8f80be6a105e", null ],
      [ "MAX_OPVP_ID", "_outdoor_pv_p_mgr_8h.html#a56e7764289940901c2a230103f78adeeae532fb42157a8ad0f4dbe5b15df4de35", null ]
    ] ],
    [ "OutdoorPvPZones", "_outdoor_pv_p_mgr_8h.html#a8490f08a1cb6baaca1faa1e9559aa7b3", [
      [ "ZONE_ID_SILITHUS", "_outdoor_pv_p_mgr_8h.html#a8490f08a1cb6baaca1faa1e9559aa7b3aea0fd1d0ce8d641d93b747fba609863a", null ],
      [ "ZONE_ID_TEMPLE_OF_AQ", "_outdoor_pv_p_mgr_8h.html#a8490f08a1cb6baaca1faa1e9559aa7b3af6e0f35523e58a32b468bbad85b983f1", null ],
      [ "ZONE_ID_RUINS_OF_AQ", "_outdoor_pv_p_mgr_8h.html#a8490f08a1cb6baaca1faa1e9559aa7b3acbae3b65449bfe7c245bbbf739545606", null ],
      [ "ZONE_ID_GATES_OF_AQ", "_outdoor_pv_p_mgr_8h.html#a8490f08a1cb6baaca1faa1e9559aa7b3abcd096428f931d5be04c38f69b9d5bbb", null ],
      [ "ZONE_ID_EASTERN_PLAGUELANDS", "_outdoor_pv_p_mgr_8h.html#a8490f08a1cb6baaca1faa1e9559aa7b3a3d148e5f4ab75081c3ce917decee6f2b", null ],
      [ "ZONE_ID_STRATHOLME", "_outdoor_pv_p_mgr_8h.html#a8490f08a1cb6baaca1faa1e9559aa7b3a1ff2945a093e424e9f24bbf5286b441b", null ],
      [ "ZONE_ID_SCHOLOMANCE", "_outdoor_pv_p_mgr_8h.html#a8490f08a1cb6baaca1faa1e9559aa7b3ab1be4cf630f612c4b44753210a37d3a1", null ]
    ] ]
];