var _action_8h =
[
    [ "NextAction", "classai_1_1_next_action.html", "classai_1_1_next_action" ],
    [ "Action", "classai_1_1_action.html", "classai_1_1_action" ],
    [ "ActionNode", "classai_1_1_action_node.html", "classai_1_1_action_node" ],
    [ "ActionBasket", "classai_1_1_action_basket.html", "classai_1_1_action_basket" ],
    [ "AI_VALUE", "_action_8h.html#a76af574fe61f5e0ab2ddd23fd90731e9", null ],
    [ "AI_VALUE2", "_action_8h.html#af32b4e295752af14ac441a5dc3ab96d1", null ],
    [ "ActionThreatType", "_action_8h.html#abf72e2754253c763e15a3308600d3377", [
      [ "ACTION_THREAT_NONE", "_action_8h.html#abf72e2754253c763e15a3308600d3377a9da7d254633949197fb9629147b821bd", null ],
      [ "ACTION_THREAT_SINGLE", "_action_8h.html#abf72e2754253c763e15a3308600d3377a3b25cfe1db579039d7132b5cf6862873", null ],
      [ "ACTION_THREAT_AOE", "_action_8h.html#abf72e2754253c763e15a3308600d3377aa18c2fcd56b5d9e910031a1126d0d596", null ]
    ] ]
];