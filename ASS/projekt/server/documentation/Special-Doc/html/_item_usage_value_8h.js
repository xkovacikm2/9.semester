var _item_usage_value_8h =
[
    [ "ItemUsageValue", "classai_1_1_item_usage_value.html", "classai_1_1_item_usage_value" ],
    [ "ItemUsage", "_item_usage_value_8h.html#a172c04fa48bbe31b0249490bd366a640", [
      [ "ITEM_USAGE_NONE", "_item_usage_value_8h.html#a172c04fa48bbe31b0249490bd366a640a52d5c277d359176a7f2c2c7e129c7dc7", null ],
      [ "ITEM_USAGE_EQUIP", "_item_usage_value_8h.html#a172c04fa48bbe31b0249490bd366a640a46247ca147492e59ae2853b090864bcd", null ],
      [ "ITEM_USAGE_REPLACE", "_item_usage_value_8h.html#a172c04fa48bbe31b0249490bd366a640a804e844befed2be96dae8e7799a1488e", null ],
      [ "ITEM_USAGE_SKILL", "_item_usage_value_8h.html#a172c04fa48bbe31b0249490bd366a640a38d4f41d4fa0c380d5278d3b35d3c8fb", null ],
      [ "ITEM_USAGE_USE", "_item_usage_value_8h.html#a172c04fa48bbe31b0249490bd366a640a7cd64daad314557100647301877ea402", null ]
    ] ]
];