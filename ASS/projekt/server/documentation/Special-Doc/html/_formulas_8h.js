var _formulas_8h =
[
    [ "HonorScores", "struct_honor_scores.html", "struct_honor_scores" ],
    [ "XPColorChar", "_formulas_8h.html#afc54bbbcb5be759cb12031b094af3b4b", [
      [ "RED", "_formulas_8h.html#afc54bbbcb5be759cb12031b094af3b4ba27cb135627459d75a5c20c216555d4e3", null ],
      [ "ORANGE", "_formulas_8h.html#afc54bbbcb5be759cb12031b094af3b4bab7126adf14b849854163036d8383de58", null ],
      [ "YELLOW", "_formulas_8h.html#afc54bbbcb5be759cb12031b094af3b4ba18e5f6ffcb1269aa4c2b518d94cfa63c", null ],
      [ "GREEN", "_formulas_8h.html#afc54bbbcb5be759cb12031b094af3b4baffe35fb7eaaf93513e9365b28b65024f", null ],
      [ "GRAY", "_formulas_8h.html#afc54bbbcb5be759cb12031b094af3b4ba1267c1fee107cb7c6f4c3466eba6a949", null ]
    ] ],
    [ "BaseGain", "_formulas_8h.html#a0223f137e0c1d89da6f8f681e4307718", null ],
    [ "CalculateHonorRank", "_formulas_8h.html#ae5b8d0bd47c8303a96b4bd42df75f5ee", null ],
    [ "CalculateRankInfo", "_formulas_8h.html#ad8cb1a4ec4042f545700e1e6a802eb70", null ],
    [ "CalculateRpDecay", "_formulas_8h.html#a3023ca30d89263baa1a46eb4b0816c0f", null ],
    [ "CalculateRpEarning", "_formulas_8h.html#a5cfa78432eecff23658c2ecf265c12ad", null ],
    [ "DishonorableKillPoints", "_formulas_8h.html#aa12b8ee6bb14da40181971d7e5356f15", null ],
    [ "Gain", "_formulas_8h.html#a4109909b15f0ff73b4bd55fcd3adce30", null ],
    [ "GenerateScores", "_formulas_8h.html#aac649aa6555075b4c315739928c7b5fe", null ],
    [ "GetColorCode", "_formulas_8h.html#a45fe41286a35ad4547320a7fc8738c62", null ],
    [ "GetGrayLevel", "_formulas_8h.html#ae0ae11e67609d6cbd2690a2df4a40a79", null ],
    [ "GetZeroDifference", "_formulas_8h.html#a8063f95a964c35764b623405c23a2bf7", null ],
    [ "hk_honor_at_level", "_formulas_8h.html#a249974b98a142bca70c8c8d8e9e69a3e", null ],
    [ "HonorableKillPoints", "_formulas_8h.html#a96208b230cacf5816128bf6c6d1c9221", null ],
    [ "InitRankInfo", "_formulas_8h.html#ae25b69cfac8a3f2392c072c5a4732feb", null ],
    [ "xp_in_group_rate", "_formulas_8h.html#a14d79fe16f9372ac5efb43a8e4889fc7", null ]
];