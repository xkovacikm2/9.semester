var struct_spell_item_enchantment_entry =
[
    [ "amount", "struct_spell_item_enchantment_entry.html#a63be708daac7fe5432f6ff32b9a5d982", null ],
    [ "aura_id", "struct_spell_item_enchantment_entry.html#a5866d4e8ce373b6eb9af1d27e2db2c1b", null ],
    [ "description", "struct_spell_item_enchantment_entry.html#a2b55f4813c1425ef3de9707ea4226eef", null ],
    [ "ID", "struct_spell_item_enchantment_entry.html#af46fa8808024e75705b812b695bd1590", null ],
    [ "slot", "struct_spell_item_enchantment_entry.html#af64b13ce2e81d5be7e73763cd5c5e4b8", null ],
    [ "spellid", "struct_spell_item_enchantment_entry.html#ae56031830bf57999e20a046f177c51b0", null ],
    [ "type", "struct_spell_item_enchantment_entry.html#ac814e5c37b87098f57a652b9a92da846", null ]
];