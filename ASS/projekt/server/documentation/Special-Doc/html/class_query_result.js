var class_query_result =
[
    [ "QueryResult", "class_query_result.html#a95b372ad89f4cc7e0870f85d7fa79733", null ],
    [ "~QueryResult", "class_query_result.html#aa682f0a42e35567aebc44456e23b81e1", null ],
    [ "Fetch", "class_query_result.html#ae59bcb12d59b9a369953be91ddb5c446", null ],
    [ "GetFieldCount", "class_query_result.html#a3130f50aa6d5b726f6891a3fbed5bcc1", null ],
    [ "GetRowCount", "class_query_result.html#af92312138c98bbceeeddca74ae6029fe", null ],
    [ "NextRow", "class_query_result.html#a8f1f135d431fe357bf2b9373dc8d59be", null ],
    [ "operator[]", "class_query_result.html#afaa14f18582e5c0ad42841784b868db5", null ],
    [ "mCurrentRow", "class_query_result.html#aa099ee8afddd28503ddbc0c825643980", null ],
    [ "mFieldCount", "class_query_result.html#a2c18d53a2afa9e752d1f1dece3eaa3bf", null ],
    [ "mRowCount", "class_query_result.html#a0cb28f5d857d133e9b4346ffe0551e38", null ]
];