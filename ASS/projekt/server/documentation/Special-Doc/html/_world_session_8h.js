var _world_session_8h =
[
    [ "PartyOperation", "group__u2w.html#gaa4a0361b2172789575d5eac42ecb612a", [
      [ "PARTY_OP_INVITE", "group__u2w.html#ggaa4a0361b2172789575d5eac42ecb612aa8b30d82ca0b8d909d0445472f4003036", null ],
      [ "PARTY_OP_LEAVE", "group__u2w.html#ggaa4a0361b2172789575d5eac42ecb612aaf8c14598abfe7de9431f923cce924457", null ]
    ] ],
    [ "PartyResult", "group__u2w.html#gafafe9d186a337e0cf4a6fb7936200884", [
      [ "ERR_PARTY_RESULT_OK", "group__u2w.html#ggafafe9d186a337e0cf4a6fb7936200884aea6c07e7e38ca952fcb0ed0cf1000ae1", null ],
      [ "ERR_BAD_PLAYER_NAME_S", "group__u2w.html#ggafafe9d186a337e0cf4a6fb7936200884af8fdf599d0dbc104d9974037ade00c5a", null ],
      [ "ERR_TARGET_NOT_IN_GROUP_S", "group__u2w.html#ggafafe9d186a337e0cf4a6fb7936200884a728dfe8f063ee17267c3cbfc4b9ed133", null ],
      [ "ERR_GROUP_FULL", "group__u2w.html#ggafafe9d186a337e0cf4a6fb7936200884aef45f9e84fd5f65792e0d9bf3ce08479", null ],
      [ "ERR_ALREADY_IN_GROUP_S", "group__u2w.html#ggafafe9d186a337e0cf4a6fb7936200884a08991ba0009ce786f486afe56e53b44f", null ],
      [ "ERR_NOT_IN_GROUP", "group__u2w.html#ggafafe9d186a337e0cf4a6fb7936200884a5f08983db3f8d4487f8a8de2ff218a5e", null ],
      [ "ERR_NOT_LEADER", "group__u2w.html#ggafafe9d186a337e0cf4a6fb7936200884a5ae863688bad9108c0cad75e31212fd1", null ],
      [ "ERR_PLAYER_WRONG_FACTION", "group__u2w.html#ggafafe9d186a337e0cf4a6fb7936200884ad4c66acf2609b027add99ec3178377eb", null ],
      [ "ERR_IGNORING_YOU_S", "group__u2w.html#ggafafe9d186a337e0cf4a6fb7936200884a15b9fa3eec96dc56bec27e8c839e7081", null ]
    ] ],
    [ "TutorialDataState", "group__u2w.html#ga7495275bdc42e32296b74ef6a56458cd", [
      [ "TUTORIALDATA_UNCHANGED", "group__u2w.html#gga7495275bdc42e32296b74ef6a56458cdae22fb8e1a4961cafbaa60f407f0c19a1", null ],
      [ "TUTORIALDATA_CHANGED", "group__u2w.html#gga7495275bdc42e32296b74ef6a56458cda37f6a8f6501e693f0e2b2d832176f3cf", null ],
      [ "TUTORIALDATA_NEW", "group__u2w.html#gga7495275bdc42e32296b74ef6a56458cda3956b6779f02835881f27b44fe875653", null ]
    ] ]
];