var _character_database_cleaner_8h =
[
    [ "CleaningFlags", "_character_database_cleaner_8h.html#ad8ba5c50a4cd2d5193e35ccc83798c67", [
      [ "CLEANING_FLAG_SKILLS", "_character_database_cleaner_8h.html#ad8ba5c50a4cd2d5193e35ccc83798c67ac23b2648d4cf0fe8d9ff4cd96722a714", null ],
      [ "CLEANING_FLAG_SPELLS", "_character_database_cleaner_8h.html#ad8ba5c50a4cd2d5193e35ccc83798c67a39fa10b14e68e92b74ab6697d0e1d564", null ]
    ] ],
    [ "CheckUnique", "_character_database_cleaner_8h.html#ab98182e5f92f72090600314cc41f0d39", null ],
    [ "CleanCharacterSkills", "_character_database_cleaner_8h.html#ae8c5d4dc123b8a900254bcbc82c1ffbe", null ],
    [ "CleanCharacterSpell", "_character_database_cleaner_8h.html#aca5826d283031348e5d6ae6eb75ad605", null ],
    [ "CleanDatabase", "_character_database_cleaner_8h.html#a357c8a07f912c6efb55a13cc9bc089b9", null ],
    [ "SkillCheck", "_character_database_cleaner_8h.html#a0f72cf24646d1ece0eb793890840992d", null ],
    [ "SpellCheck", "_character_database_cleaner_8h.html#a02688f57778d28a7bcc4ca6cee2074ad", null ]
];