var struct_spell_bonus_entry =
[
    [ "ap_bonus", "struct_spell_bonus_entry.html#a98410b8bb89434cd7e1747462f8039dc", null ],
    [ "ap_dot_bonus", "struct_spell_bonus_entry.html#a09caeec47b138d9eff3539795416a09a", null ],
    [ "direct_damage", "struct_spell_bonus_entry.html#abab1af227a6c8e592b8a9bd15589535d", null ],
    [ "direct_damage_done", "struct_spell_bonus_entry.html#a12fbbca5900ad19ef4eab950b8edb17f", null ],
    [ "direct_damage_taken", "struct_spell_bonus_entry.html#ad4326b49dc12ee6edd93639bfc5f4f9e", null ],
    [ "dot_damage", "struct_spell_bonus_entry.html#a92491d8d1e61612d88c7c830d7912be5", null ],
    [ "one_hand_direct_damage", "struct_spell_bonus_entry.html#a684cc7e84a3b7de3ff9f1430ebe2cf16", null ],
    [ "one_hand_direct_damage_done", "struct_spell_bonus_entry.html#a4ab3d59752c69e10b06547bfb6115bca", null ],
    [ "one_hand_direct_damage_taken", "struct_spell_bonus_entry.html#ac2061818a1d073eb60cba5c919779976", null ],
    [ "two_hand_direct_damage", "struct_spell_bonus_entry.html#a0326030fd9459fe34410014bd63ced2a", null ],
    [ "two_hand_direct_damage_done", "struct_spell_bonus_entry.html#a5367fcbe3345af98307a54ee0dd33a8e", null ],
    [ "two_hand_direct_damage_taken", "struct_spell_bonus_entry.html#a74055a99b900d775b994485ce54f9a70", null ]
];