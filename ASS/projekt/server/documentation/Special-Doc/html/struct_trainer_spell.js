var struct_trainer_spell =
[
    [ "TrainerSpell", "struct_trainer_spell.html#a87a098774b32babfcf2000e3330a25b9", null ],
    [ "TrainerSpell", "struct_trainer_spell.html#a40f0f4d8ca745fb0dc4b52dfd21b548f", null ],
    [ "isProvidedReqLevel", "struct_trainer_spell.html#a409e87ab7b07890eb1d304186566bb93", null ],
    [ "reqLevel", "struct_trainer_spell.html#ac71afa7c180905ae4247011b2f40f929", null ],
    [ "reqSkill", "struct_trainer_spell.html#ac0a74c104994e36ba95703689ed92964", null ],
    [ "reqSkillValue", "struct_trainer_spell.html#a1187d182a512ae0f40af6ea36a4911db", null ],
    [ "spell", "struct_trainer_spell.html#af7649fcfa5e6ca77c7d0aa9ef474b44c", null ],
    [ "spellCost", "struct_trainer_spell.html#a9adb80199e22a0db5d4eb262d9edbfe8", null ]
];