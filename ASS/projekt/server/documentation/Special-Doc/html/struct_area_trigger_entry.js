var struct_area_trigger_entry =
[
    [ "box_orientation", "struct_area_trigger_entry.html#a027774a03c0b7a82d541c37a623e23ca", null ],
    [ "box_x", "struct_area_trigger_entry.html#a887ae2cd30d9d0774004167023611d8f", null ],
    [ "box_y", "struct_area_trigger_entry.html#ad786566cf84cd989bcb13af1d60a08b2", null ],
    [ "box_z", "struct_area_trigger_entry.html#ad0727c1aec0c89f7a7087c1b19da066e", null ],
    [ "id", "struct_area_trigger_entry.html#ae406e23b1086ebca3ba306d7165ab810", null ],
    [ "mapid", "struct_area_trigger_entry.html#a3b644051290c9070e3767ea6c9e83b94", null ],
    [ "radius", "struct_area_trigger_entry.html#aa209c2bbe03a89cb47fb9f452ad21c6e", null ],
    [ "x", "struct_area_trigger_entry.html#aed019df904179360dbfd80a5bc6840ef", null ],
    [ "y", "struct_area_trigger_entry.html#ae69118d452953883abac3e0540a82ecf", null ],
    [ "z", "struct_area_trigger_entry.html#aa2cb380a61834dec333ecbb5d6a73f61", null ]
];