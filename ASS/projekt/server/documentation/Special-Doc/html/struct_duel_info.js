var struct_duel_info =
[
    [ "DuelInfo", "struct_duel_info.html#ae5db676823aba9453d8a4408b0fadda3", null ],
    [ "initiator", "struct_duel_info.html#ab941c7a39e5b2f95a25ca096ddf5064b", null ],
    [ "opponent", "struct_duel_info.html#a3b032f1dea0693f9571bd1926056d9b5", null ],
    [ "outOfBound", "struct_duel_info.html#aa605083ad5170b1b28931b87e28b2350", null ],
    [ "startTime", "struct_duel_info.html#ae98d6579ccc51e4c40cedce0479edaf5", null ],
    [ "startTimer", "struct_duel_info.html#a9c74c722ada5fbb14b852f05ff887c5b", null ]
];