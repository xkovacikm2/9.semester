var struct_creature_class_lvl_stats =
[
    [ "BaseArmor", "struct_creature_class_lvl_stats.html#a5f504fbff1389ab6dbbbbca7916fa135", null ],
    [ "BaseDamage", "struct_creature_class_lvl_stats.html#ad33581aac4a1567d7eeff166de1755a4", null ],
    [ "BaseHealth", "struct_creature_class_lvl_stats.html#a8ee75f2d9e8af59698aed607a853ed65", null ],
    [ "BaseMana", "struct_creature_class_lvl_stats.html#aae95bebb39fa6c6ddab7091f9ee15a33", null ],
    [ "BaseMeleeAttackPower", "struct_creature_class_lvl_stats.html#a3dcb54cd3a18404e440abb4b66657c47", null ],
    [ "BaseRangedAttackPower", "struct_creature_class_lvl_stats.html#a7148f92d397190cf608a876aeef7fed8", null ]
];