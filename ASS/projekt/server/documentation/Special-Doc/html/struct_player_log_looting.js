var struct_player_log_looting =
[
    [ "PlayerLogLooting", "struct_player_log_looting.html#a00aa2122a4ac91487942839d5adad0b2", null ],
    [ "GetItemEntry", "struct_player_log_looting.html#af43c265e0dc7020bd296fc47af351602", null ],
    [ "GetLootSourceType", "struct_player_log_looting.html#a6ee334a303c1e59166405f916569ed5f", null ],
    [ "SetLootSourceType", "struct_player_log_looting.html#afed2d35690e7df79459bbd0339cc9ffe", null ],
    [ "droppedBy", "struct_player_log_looting.html#a6b1f85f0ade702940e7f21597122ea80", null ],
    [ "itemEntry", "struct_player_log_looting.html#a90942040e5b75d776b77a93ecf4ef58d", null ],
    [ "itemGuid", "struct_player_log_looting.html#acd8add0c4d9c659027707d237d346136", null ]
];