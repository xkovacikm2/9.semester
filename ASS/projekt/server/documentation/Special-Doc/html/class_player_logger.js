var class_player_logger =
[
    [ "PlayerLogger", "class_player_logger.html#a4c6b8e747bc359b54258f476e24ec2f7", null ],
    [ "~PlayerLogger", "class_player_logger.html#a79f0361e559f3eb0c0c83754ca87954b", null ],
    [ "CheckAndTruncate", "class_player_logger.html#ad869ad5e2f7c9ff8db866753ce5cb8c7", null ],
    [ "Clean", "class_player_logger.html#acc39c0e9c08df2585304f31b37c59e5c", null ],
    [ "Initialize", "class_player_logger.html#a08aba3aa158c792b13434cbecd1b4cf6", null ],
    [ "IsLoggingActive", "class_player_logger.html#a1c5183452a6b68896e554fb4accc0a05", null ],
    [ "IsLoggingActive", "class_player_logger.html#a7498b0de3f1404cbd525bef25fa82d50", null ],
    [ "LogDamage", "class_player_logger.html#a60c54e45318cca0782e9676d417ed6d2", null ],
    [ "LogKilling", "class_player_logger.html#a8d60222f565c7fb740b769e1395d8ece", null ],
    [ "LogLooting", "class_player_logger.html#a19d62733fcd863156fdca9aab6a8ab50", null ],
    [ "LogPosition", "class_player_logger.html#ad52dbb63720a3219b6219760e3716612", null ],
    [ "LogProgress", "class_player_logger.html#a0b7d4c8a4ac9ad98520aa40472eebbf8", null ],
    [ "LogTrading", "class_player_logger.html#ad4706c56942b416519c690346a98a546", null ],
    [ "SaveToDB", "class_player_logger.html#ab70381aa25de13945d9b918f8385f595", null ],
    [ "StartCombatLogging", "class_player_logger.html#a81a774961944fe8d9c52fe94eae18397", null ],
    [ "StartLogging", "class_player_logger.html#ad77083a71e5504ee041a7e7040a26782", null ],
    [ "Stop", "class_player_logger.html#af5962662bb04a0d80e244a53a87d59ae", null ]
];