var dir_8d7cb7c192d5df63d14ca6d72c687c4a =
[
    [ "ChatCommandTrigger.h", "_chat_command_trigger_8h.html", [
      [ "ChatCommandTrigger", "classai_1_1_chat_command_trigger.html", "classai_1_1_chat_command_trigger" ]
    ] ],
    [ "ChatTriggerContext.h", "_chat_trigger_context_8h.html", [
      [ "ChatTriggerContext", "classai_1_1_chat_trigger_context.html", "classai_1_1_chat_trigger_context" ]
    ] ],
    [ "CureTriggers.cpp", "_cure_triggers_8cpp.html", null ],
    [ "CureTriggers.h", "_cure_triggers_8h.html", [
      [ "NeedCureTrigger", "classai_1_1_need_cure_trigger.html", "classai_1_1_need_cure_trigger" ],
      [ "TargetAuraDispelTrigger", "classai_1_1_target_aura_dispel_trigger.html", "classai_1_1_target_aura_dispel_trigger" ],
      [ "PartyMemberNeedCureTrigger", "classai_1_1_party_member_need_cure_trigger.html", "classai_1_1_party_member_need_cure_trigger" ]
    ] ],
    [ "GenericTriggers.cpp", "_generic_triggers_8cpp.html", null ],
    [ "GenericTriggers.h", "_generic_triggers_8h.html", "_generic_triggers_8h" ],
    [ "HealthTriggers.cpp", "_health_triggers_8cpp.html", null ],
    [ "HealthTriggers.h", "_health_triggers_8h.html", [
      [ "ValueInRangeTrigger", "classai_1_1_value_in_range_trigger.html", "classai_1_1_value_in_range_trigger" ],
      [ "HealthInRangeTrigger", "classai_1_1_health_in_range_trigger.html", "classai_1_1_health_in_range_trigger" ],
      [ "LowHealthTrigger", "classai_1_1_low_health_trigger.html", "classai_1_1_low_health_trigger" ],
      [ "CriticalHealthTrigger", "classai_1_1_critical_health_trigger.html", "classai_1_1_critical_health_trigger" ],
      [ "MediumHealthTrigger", "classai_1_1_medium_health_trigger.html", "classai_1_1_medium_health_trigger" ],
      [ "AlmostFullHealthTrigger", "classai_1_1_almost_full_health_trigger.html", "classai_1_1_almost_full_health_trigger" ],
      [ "PartyMemberLowHealthTrigger", "classai_1_1_party_member_low_health_trigger.html", "classai_1_1_party_member_low_health_trigger" ],
      [ "PartyMemberCriticalHealthTrigger", "classai_1_1_party_member_critical_health_trigger.html", "classai_1_1_party_member_critical_health_trigger" ],
      [ "PartyMemberMediumHealthTrigger", "classai_1_1_party_member_medium_health_trigger.html", "classai_1_1_party_member_medium_health_trigger" ],
      [ "PartyMemberAlmostFullHealthTrigger", "classai_1_1_party_member_almost_full_health_trigger.html", "classai_1_1_party_member_almost_full_health_trigger" ],
      [ "TargetLowHealthTrigger", "classai_1_1_target_low_health_trigger.html", "classai_1_1_target_low_health_trigger" ],
      [ "TargetCriticalHealthTrigger", "classai_1_1_target_critical_health_trigger.html", "classai_1_1_target_critical_health_trigger" ],
      [ "PartyMemberDeadTrigger", "classai_1_1_party_member_dead_trigger.html", "classai_1_1_party_member_dead_trigger" ],
      [ "DeadTrigger", "classai_1_1_dead_trigger.html", "classai_1_1_dead_trigger" ],
      [ "AoeHealTrigger", "classai_1_1_aoe_heal_trigger.html", "classai_1_1_aoe_heal_trigger" ]
    ] ],
    [ "LfgTriggers.h", "_lfg_triggers_8h.html", [
      [ "LfgProposalActiveTrigger", "classai_1_1_lfg_proposal_active_trigger.html", "classai_1_1_lfg_proposal_active_trigger" ]
    ] ],
    [ "LootTriggers.cpp", "_loot_triggers_8cpp.html", null ],
    [ "LootTriggers.h", "_loot_triggers_8h.html", [
      [ "LootAvailableTrigger", "classai_1_1_loot_available_trigger.html", "classai_1_1_loot_available_trigger" ],
      [ "FarFromCurrentLootTrigger", "classai_1_1_far_from_current_loot_trigger.html", "classai_1_1_far_from_current_loot_trigger" ],
      [ "CanLootTrigger", "classai_1_1_can_loot_trigger.html", "classai_1_1_can_loot_trigger" ]
    ] ],
    [ "RangeTriggers.h", "_range_triggers_8h.html", [
      [ "EnemyTooCloseForSpellTrigger", "classai_1_1_enemy_too_close_for_spell_trigger.html", "classai_1_1_enemy_too_close_for_spell_trigger" ],
      [ "EnemyTooCloseForMeleeTrigger", "classai_1_1_enemy_too_close_for_melee_trigger.html", "classai_1_1_enemy_too_close_for_melee_trigger" ],
      [ "OutOfRangeTrigger", "classai_1_1_out_of_range_trigger.html", "classai_1_1_out_of_range_trigger" ],
      [ "EnemyOutOfMeleeTrigger", "classai_1_1_enemy_out_of_melee_trigger.html", "classai_1_1_enemy_out_of_melee_trigger" ],
      [ "EnemyOutOfSpellRangeTrigger", "classai_1_1_enemy_out_of_spell_range_trigger.html", "classai_1_1_enemy_out_of_spell_range_trigger" ],
      [ "PartyMemberToHealOutOfSpellRangeTrigger", "classai_1_1_party_member_to_heal_out_of_spell_range_trigger.html", "classai_1_1_party_member_to_heal_out_of_spell_range_trigger" ],
      [ "FarFromMasterTrigger", "classai_1_1_far_from_master_trigger.html", "classai_1_1_far_from_master_trigger" ],
      [ "OutOfReactRangeTrigger", "classai_1_1_out_of_react_range_trigger.html", "classai_1_1_out_of_react_range_trigger" ]
    ] ],
    [ "TriggerContext.h", "_trigger_context_8h.html", [
      [ "TriggerContext", "classai_1_1_trigger_context.html", "classai_1_1_trigger_context" ]
    ] ],
    [ "WithinAreaTrigger.h", "_within_area_trigger_8h.html", [
      [ "WithinAreaTrigger", "classai_1_1_within_area_trigger.html", "classai_1_1_within_area_trigger" ]
    ] ],
    [ "WorldPacketTrigger.h", "_world_packet_trigger_8h.html", [
      [ "WorldPacketTrigger", "classai_1_1_world_packet_trigger.html", "classai_1_1_world_packet_trigger" ]
    ] ],
    [ "WorldPacketTriggerContext.h", "_world_packet_trigger_context_8h.html", [
      [ "WorldPacketTriggerContext", "classai_1_1_world_packet_trigger_context.html", "classai_1_1_world_packet_trigger_context" ]
    ] ]
];