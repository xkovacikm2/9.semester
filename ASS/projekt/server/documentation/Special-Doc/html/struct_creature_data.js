var struct_creature_data =
[
    [ "GetObjectGuid", "struct_creature_data.html#aecc84f6197297b53d9748bdd6472d24c", null ],
    [ "curhealth", "struct_creature_data.html#a8d792fe1be63517b89beeb7dc16bc59e", null ],
    [ "curmana", "struct_creature_data.html#a9282b4137e892574d3326273c19b7333", null ],
    [ "currentwaypoint", "struct_creature_data.html#ac58e514339c9c60ce78876a7c1ef50f1", null ],
    [ "equipmentId", "struct_creature_data.html#a49d5a1eb1bc2486b3987235dcd7bd3e4", null ],
    [ "id", "struct_creature_data.html#a2e2c3a5008f3d5aba0b6903586d3f83e", null ],
    [ "is_dead", "struct_creature_data.html#adfad9c79af79f45b6ff28b989e6e9f05", null ],
    [ "mapid", "struct_creature_data.html#ad7b505ecbc54fd74d99d6d396b66ee84", null ],
    [ "modelid_override", "struct_creature_data.html#a8375e7ea69d80b467ff5faf3c5823f13", null ],
    [ "movementType", "struct_creature_data.html#a5574b3c7c3976325c72f2d49a91d1ef3", null ],
    [ "orientation", "struct_creature_data.html#a8e29f2f310f10644b21b86edce264cb6", null ],
    [ "posX", "struct_creature_data.html#ab73d154a173a40973b8211e2bee4ecba", null ],
    [ "posY", "struct_creature_data.html#a98fbc00a0254cb68fbe796c2b54aef50", null ],
    [ "posZ", "struct_creature_data.html#aee7aae90ca299d8ac163bb068fed3f6a", null ],
    [ "spawndist", "struct_creature_data.html#a7943ccbea454f305d000a4717f95b89e", null ],
    [ "spawntimesecs", "struct_creature_data.html#a1f0910de3138ea16397173c9821acf35", null ]
];