var class_spell_cast_targets =
[
    [ "SpellCastTargets", "class_spell_cast_targets.html#aae040bf9308a0ee63208cdb6cd15580b", null ],
    [ "~SpellCastTargets", "class_spell_cast_targets.html#ab3bf003f250e2d77bf1905e4c8b81244", null ],
    [ "getCorpseTargetGuid", "class_spell_cast_targets.html#a98eee6778ea2cc05bd0073affde7f37d", null ],
    [ "getDestination", "class_spell_cast_targets.html#a714e30a9388d264afdc0972bc0effa11", null ],
    [ "getGOTarget", "class_spell_cast_targets.html#a68372d026b0fbe7825ce7bccd0782094", null ],
    [ "getGOTargetGuid", "class_spell_cast_targets.html#ac2c7d09ec29b335bd23ea884c0c3615a", null ],
    [ "getItemTarget", "class_spell_cast_targets.html#a4e128d2ee02535f2020ceb4309dc8b09", null ],
    [ "getItemTargetEntry", "class_spell_cast_targets.html#a872c670896e98cb2ca74948e0639ba15", null ],
    [ "getItemTargetGuid", "class_spell_cast_targets.html#aebd9cd7110f9711cd2bd94e154920cd6", null ],
    [ "getSource", "class_spell_cast_targets.html#a31496bc8861793d11f83b216c94a81e8", null ],
    [ "getUnitTarget", "class_spell_cast_targets.html#a124b2fcc6764bd9bcd5e16d352e51c48", null ],
    [ "getUnitTargetGuid", "class_spell_cast_targets.html#ad566bd3a4d9d077fbcfdd1de2baab0d7", null ],
    [ "IsEmpty", "class_spell_cast_targets.html#aa6b954caac844b5b4d76e190a60ad8fa", null ],
    [ "operator=", "class_spell_cast_targets.html#a6f437a90f22aaf2145143897845a85b1", null ],
    [ "read", "class_spell_cast_targets.html#a524a7750c6c3fa3532d38cc94583b13d", null ],
    [ "ReadForCaster", "class_spell_cast_targets.html#a00d7096ba9db71ef7d839be063a7f879", null ],
    [ "setCorpseTarget", "class_spell_cast_targets.html#a5f5f01751fab6066b430d427466121c1", null ],
    [ "setDestination", "class_spell_cast_targets.html#a9e422f810ddd52b534f8b6f032989da1", null ],
    [ "setGOTarget", "class_spell_cast_targets.html#a85fac9724b195534fd0348be2937c331", null ],
    [ "setItemTarget", "class_spell_cast_targets.html#a5ddeb0e96775789e553286c9cc879c27", null ],
    [ "setSource", "class_spell_cast_targets.html#afa341e13427e2d645c72fb04c55b7922", null ],
    [ "setTradeItemTarget", "class_spell_cast_targets.html#af17d6161f1bf74aa8c13c30885dcdbeb", null ],
    [ "setUnitTarget", "class_spell_cast_targets.html#ab2f9a145d69bbf6620fbfa199c94fe55", null ],
    [ "Update", "class_spell_cast_targets.html#ac093bb14d2903e6833f9d7c6095b7d4c", null ],
    [ "updateTradeSlotItem", "class_spell_cast_targets.html#ac6ff9f0e2814bcdb4b2e36798724252e", null ],
    [ "write", "class_spell_cast_targets.html#a865d258a598ff7453e85649e527bc44b", null ],
    [ "m_destX", "class_spell_cast_targets.html#a125afa4e17d4bbf6b7a3e006cf4acebf", null ],
    [ "m_destY", "class_spell_cast_targets.html#a0b2faa1125a2b6f747ce7fb9ddabb20e", null ],
    [ "m_destZ", "class_spell_cast_targets.html#abe8c46bb1118b250bff27a914029117a", null ],
    [ "m_srcX", "class_spell_cast_targets.html#a5b1bf2eb5db04a8fa48263e4d76e1781", null ],
    [ "m_srcY", "class_spell_cast_targets.html#a7ca73238b1332afe0c8216b6023734fb", null ],
    [ "m_srcZ", "class_spell_cast_targets.html#aaecb596a393df431f72b55fb2f756f98", null ],
    [ "m_strTarget", "class_spell_cast_targets.html#a7980149cc7e517022ee042846ad7176a", null ],
    [ "m_targetMask", "class_spell_cast_targets.html#ad6cf5467200d517390acfb09802c1bd3", null ]
];