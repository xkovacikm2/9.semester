var struct_map_entry =
[
    [ "Instanceable", "struct_map_entry.html#af036a047dd6a68deb3dc61cff5bb6958", null ],
    [ "IsBattleGround", "struct_map_entry.html#ae2a8f800364af383c3ded478b825c37e", null ],
    [ "IsContinent", "struct_map_entry.html#a4e9a18b9335b79bb079b5255d24dd6d7", null ],
    [ "IsDungeon", "struct_map_entry.html#ad9902fc9b28ba9b555b45c0662aa0397", null ],
    [ "IsMountAllowed", "struct_map_entry.html#a72895024ae305e4ac680d028ed3b6c36", null ],
    [ "IsNonRaidDungeon", "struct_map_entry.html#a168041f8e1fa28e6c4e8922d3e6b9005", null ],
    [ "IsRaid", "struct_map_entry.html#adc49d2974929327b06a442012b0a07d1", null ],
    [ "linked_zone", "struct_map_entry.html#a617a23fd33a5853f20a7b1cbda6f6e3a", null ],
    [ "map_type", "struct_map_entry.html#adc919dc1d095da8c0f1624abfcca2cf6", null ],
    [ "MapID", "struct_map_entry.html#a6bf547a0414908596a7b37db8bd36719", null ],
    [ "multimap_id", "struct_map_entry.html#a3ffc45e361a42d043c31c7a6d94e8d46", null ],
    [ "name", "struct_map_entry.html#a448d68ff405b38ae4067bbd18b38927a", null ]
];