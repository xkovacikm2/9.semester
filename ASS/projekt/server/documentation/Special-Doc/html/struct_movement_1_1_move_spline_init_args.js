var struct_movement_1_1_move_spline_init_args =
[
    [ "MoveSplineInitArgs", "struct_movement_1_1_move_spline_init_args.html#a51679d8fbabd190af6b81a341d7b8429", null ],
    [ "Validate", "struct_movement_1_1_move_spline_init_args.html#a17beca23a7a7eaa298debb5e9638daca", null ],
    [ "facing", "struct_movement_1_1_move_spline_init_args.html#a8800fca5b50ab5123e92b28e921ec8b0", null ],
    [ "flags", "struct_movement_1_1_move_spline_init_args.html#ac4271ed0434fc8e7f7a70e8869d81a0e", null ],
    [ "path", "struct_movement_1_1_move_spline_init_args.html#a3857df1ab2d6dafef4a6d796e6593341", null ],
    [ "path_Idx_offset", "struct_movement_1_1_move_spline_init_args.html#a6b6aaf43685649798db46d7b409d3e32", null ],
    [ "splineId", "struct_movement_1_1_move_spline_init_args.html#ac8c84e828c23801a68524c9811f0336b", null ],
    [ "velocity", "struct_movement_1_1_move_spline_init_args.html#af042454593bfd4dc888c1700af437933", null ]
];