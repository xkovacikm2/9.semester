var class_player_social =
[
    [ "PlayerSocial", "class_player_social.html#ad505f01fad548b70cf070a86dce40278", null ],
    [ "~PlayerSocial", "class_player_social.html#ae25156d7ed5a918156264219ba01181e", null ],
    [ "AddToSocialList", "class_player_social.html#a13dc75b8da3c6dd4c90901286a69d339", null ],
    [ "GetNumberOfSocialsWithFlag", "class_player_social.html#a6c2d8fa0b62b803afe4d82bd8315b97b", null ],
    [ "HasFriend", "class_player_social.html#a83c71a1a1b794a0ae12d537b0337d5a0", null ],
    [ "HasIgnore", "class_player_social.html#a483db97730770e723815ca7930603c87", null ],
    [ "RemoveFromSocialList", "class_player_social.html#a53b902c68bebe29264931ca9622af819", null ],
    [ "SendFriendList", "class_player_social.html#a8a94bef37c20c36e2f10f89c48b9edf0", null ],
    [ "SendIgnoreList", "class_player_social.html#a7c3bb915e24d7e5d0e8bd8af21899d3f", null ],
    [ "SetFriendNote", "class_player_social.html#a95764850c24e5377f2586921aecf829a", null ],
    [ "SetPlayerGuid", "class_player_social.html#a9cdbe616bc8aa8bd9560602a2bc73736", null ],
    [ "SocialMgr", "class_player_social.html#ac90a13e772eca1f96d98f76cfc8bf396", null ]
];