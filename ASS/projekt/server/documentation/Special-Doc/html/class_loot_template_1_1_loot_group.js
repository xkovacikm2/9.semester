var class_loot_template_1_1_loot_group =
[
    [ "AddEntry", "class_loot_template_1_1_loot_group.html#a42cb6dce10cf3ef875abfe4724d03c12", null ],
    [ "CheckLootRefs", "class_loot_template_1_1_loot_group.html#ada8986e56029ac9addc8e02fd6be1e5e", null ],
    [ "HasQuestDrop", "class_loot_template_1_1_loot_group.html#ac5e141c75e11dcd423ea99fc6bba56e7", null ],
    [ "HasQuestDropForPlayer", "class_loot_template_1_1_loot_group.html#a686e61fc146233877d75dc4880e8bda1", null ],
    [ "HasSharedQuestDropForPlayer", "class_loot_template_1_1_loot_group.html#a57f9b56dc223324c348e435e083cc3d4", null ],
    [ "HasStartingQuestDropForPlayer", "class_loot_template_1_1_loot_group.html#a84e6a4e031f36f440313871e0c4aa3f1", null ],
    [ "Process", "class_loot_template_1_1_loot_group.html#a56eb1c80332a0df67214e156c101b1b0", null ],
    [ "RawTotalChance", "class_loot_template_1_1_loot_group.html#a5a4dfbbfaf6c6f87217c124f387d7951", null ],
    [ "TotalChance", "class_loot_template_1_1_loot_group.html#ab306f81dc00d214e0ec8ac3903eff42e", null ],
    [ "Verify", "class_loot_template_1_1_loot_group.html#a28d8ecba506c5ae1befda4858070cb4f", null ]
];