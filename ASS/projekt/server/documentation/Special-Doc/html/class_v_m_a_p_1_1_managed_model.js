var class_v_m_a_p_1_1_managed_model =
[
    [ "ManagedModel", "class_v_m_a_p_1_1_managed_model.html#ab641724be7e7791c2c95982acc13f3b0", null ],
    [ "decRefCount", "class_v_m_a_p_1_1_managed_model.html#a5a104acbc0661b19ba2a662dc669b73d", null ],
    [ "getModel", "class_v_m_a_p_1_1_managed_model.html#a34a9a48735d5af30b1afed5610bc52e5", null ],
    [ "incRefCount", "class_v_m_a_p_1_1_managed_model.html#aa18b99de7db1c0811bd877d45969d716", null ],
    [ "setModel", "class_v_m_a_p_1_1_managed_model.html#aa450ae6be22d507c36a746850629831d", null ],
    [ "iModel", "class_v_m_a_p_1_1_managed_model.html#a5e20d0157691bd6036247a83eea8d19b", null ],
    [ "iRefCount", "class_v_m_a_p_1_1_managed_model.html#afb30ad900a5979f98276324a1ab1c328", null ]
];