var struct_spell_non_melee_damage =
[
    [ "SpellNonMeleeDamage", "struct_spell_non_melee_damage.html#af9c0a5ae2423853c90e028d0f392a74f", null ],
    [ "absorb", "struct_spell_non_melee_damage.html#a485ee28c6a8cf32a22578e02633df75b", null ],
    [ "attacker", "struct_spell_non_melee_damage.html#a5248da0c1a8609479d681ae76a77d045", null ],
    [ "blocked", "struct_spell_non_melee_damage.html#af0cb3d2801e51d9236a6ff5185a43790", null ],
    [ "damage", "struct_spell_non_melee_damage.html#a47e66924ae869ce34bdf36a5ed50548b", null ],
    [ "HitInfo", "struct_spell_non_melee_damage.html#a3f99854d163cd672f881f721d5bc8a39", null ],
    [ "physicalLog", "struct_spell_non_melee_damage.html#a34318f4d92685c855972e6fd22089e03", null ],
    [ "resist", "struct_spell_non_melee_damage.html#ade20f4aab4a1945ccd6754cd71101fe2", null ],
    [ "school", "struct_spell_non_melee_damage.html#ae1244557fddebcf3ee9af266cbb8fb62", null ],
    [ "SpellID", "struct_spell_non_melee_damage.html#abd34d79bbe5078bf0d8ad2b809ba0b7c", null ],
    [ "target", "struct_spell_non_melee_damage.html#a324ea6c84e005487560de986dceffeba", null ],
    [ "unused", "struct_spell_non_melee_damage.html#ae991311d8290996e1c0c49f33c29c6b3", null ]
];