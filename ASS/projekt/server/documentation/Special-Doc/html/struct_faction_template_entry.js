var struct_faction_template_entry =
[
    [ "IsContestedGuardFaction", "struct_faction_template_entry.html#a52d5e1e5b01946a3d6d66158910e9f6d", null ],
    [ "IsFriendlyTo", "struct_faction_template_entry.html#ac70cef62b2ec646d593d8742a3c02f08", null ],
    [ "IsHostileTo", "struct_faction_template_entry.html#a1aadbf23c1c81a958665d96e5bf76093", null ],
    [ "IsHostileToPlayers", "struct_faction_template_entry.html#a349650c7af717e3c67392a0b9d1970e1", null ],
    [ "IsNeutralToAll", "struct_faction_template_entry.html#a99cb471bcc9071a3c152266caa4579ae", null ],
    [ "enemyFaction", "struct_faction_template_entry.html#a57038d5bcdc95f20a6fb463a6c466d63", null ],
    [ "faction", "struct_faction_template_entry.html#a2cb03ed38c15f6e6db7b556200282541", null ],
    [ "factionFlags", "struct_faction_template_entry.html#af20e4983d34c419b4a14c60d8049d9f4", null ],
    [ "friendFaction", "struct_faction_template_entry.html#aab5eb461444a72dcffedf21711f5281b", null ],
    [ "friendlyMask", "struct_faction_template_entry.html#a6842badfb5e7e0ad3a26d250426c64b6", null ],
    [ "hostileMask", "struct_faction_template_entry.html#ab395ab86c3e553ea4a9b1ae901c6e7cb", null ],
    [ "ID", "struct_faction_template_entry.html#a22657898486c71df39da27b968e405d5", null ],
    [ "ourMask", "struct_faction_template_entry.html#a822685376430441eeb85c24fd26473b0", null ]
];