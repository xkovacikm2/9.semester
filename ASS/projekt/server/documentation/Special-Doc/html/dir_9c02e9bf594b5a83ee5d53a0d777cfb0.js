var dir_9c02e9bf594b5a83ee5d53a0d777cfb0 =
[
    [ "ArcaneMageStrategy.cpp", "_arcane_mage_strategy_8cpp.html", [
      [ "ArcaneMageStrategyActionNodeFactory", "class_arcane_mage_strategy_action_node_factory.html", "class_arcane_mage_strategy_action_node_factory" ]
    ] ],
    [ "ArcaneMageStrategy.h", "_arcane_mage_strategy_8h.html", [
      [ "ArcaneMageStrategy", "classai_1_1_arcane_mage_strategy.html", "classai_1_1_arcane_mage_strategy" ]
    ] ],
    [ "FireMageStrategy.cpp", "_fire_mage_strategy_8cpp.html", null ],
    [ "FireMageStrategy.h", "_fire_mage_strategy_8h.html", [
      [ "FireMageStrategy", "classai_1_1_fire_mage_strategy.html", "classai_1_1_fire_mage_strategy" ],
      [ "FireMageAoeStrategy", "classai_1_1_fire_mage_aoe_strategy.html", "classai_1_1_fire_mage_aoe_strategy" ]
    ] ],
    [ "FrostMageStrategy.cpp", "_frost_mage_strategy_8cpp.html", null ],
    [ "FrostMageStrategy.h", "_frost_mage_strategy_8h.html", [
      [ "FrostMageStrategy", "classai_1_1_frost_mage_strategy.html", "classai_1_1_frost_mage_strategy" ],
      [ "FrostMageAoeStrategy", "classai_1_1_frost_mage_aoe_strategy.html", "classai_1_1_frost_mage_aoe_strategy" ]
    ] ],
    [ "GenericMageNonCombatStrategy.cpp", "_generic_mage_non_combat_strategy_8cpp.html", [
      [ "GenericMageNonCombatStrategyActionNodeFactory", "class_generic_mage_non_combat_strategy_action_node_factory.html", "class_generic_mage_non_combat_strategy_action_node_factory" ]
    ] ],
    [ "GenericMageNonCombatStrategy.h", "_generic_mage_non_combat_strategy_8h.html", [
      [ "GenericMageNonCombatStrategy", "classai_1_1_generic_mage_non_combat_strategy.html", "classai_1_1_generic_mage_non_combat_strategy" ],
      [ "MageBuffManaStrategy", "classai_1_1_mage_buff_mana_strategy.html", "classai_1_1_mage_buff_mana_strategy" ],
      [ "MageBuffDpsStrategy", "classai_1_1_mage_buff_dps_strategy.html", "classai_1_1_mage_buff_dps_strategy" ]
    ] ],
    [ "GenericMageStrategy.cpp", "_generic_mage_strategy_8cpp.html", [
      [ "GenericMageStrategyActionNodeFactory", "class_generic_mage_strategy_action_node_factory.html", "class_generic_mage_strategy_action_node_factory" ]
    ] ],
    [ "GenericMageStrategy.h", "_generic_mage_strategy_8h.html", [
      [ "GenericMageStrategy", "classai_1_1_generic_mage_strategy.html", "classai_1_1_generic_mage_strategy" ]
    ] ],
    [ "MageActions.cpp", "_mage_actions_8cpp.html", null ],
    [ "MageActions.h", "_mage_actions_8h.html", [
      [ "CastFireballAction", "classai_1_1_cast_fireball_action.html", "classai_1_1_cast_fireball_action" ],
      [ "CastScorchAction", "classai_1_1_cast_scorch_action.html", "classai_1_1_cast_scorch_action" ],
      [ "CastFireBlastAction", "classai_1_1_cast_fire_blast_action.html", "classai_1_1_cast_fire_blast_action" ],
      [ "CastArcaneMissilesAction", "classai_1_1_cast_arcane_missiles_action.html", "classai_1_1_cast_arcane_missiles_action" ],
      [ "CastPyroblastAction", "classai_1_1_cast_pyroblast_action.html", "classai_1_1_cast_pyroblast_action" ],
      [ "CastFlamestrikeAction", "classai_1_1_cast_flamestrike_action.html", "classai_1_1_cast_flamestrike_action" ],
      [ "CastFrostNovaAction", "classai_1_1_cast_frost_nova_action.html", "classai_1_1_cast_frost_nova_action" ],
      [ "CastFrostboltAction", "classai_1_1_cast_frostbolt_action.html", "classai_1_1_cast_frostbolt_action" ],
      [ "CastBlizzardAction", "classai_1_1_cast_blizzard_action.html", "classai_1_1_cast_blizzard_action" ],
      [ "CastArcaneIntellectAction", "classai_1_1_cast_arcane_intellect_action.html", "classai_1_1_cast_arcane_intellect_action" ],
      [ "CastArcaneIntellectOnPartyAction", "classai_1_1_cast_arcane_intellect_on_party_action.html", "classai_1_1_cast_arcane_intellect_on_party_action" ],
      [ "CastRemoveCurseAction", "classai_1_1_cast_remove_curse_action.html", "classai_1_1_cast_remove_curse_action" ],
      [ "CastCombustionAction", "classai_1_1_cast_combustion_action.html", "classai_1_1_cast_combustion_action" ],
      [ "CastRemoveCurseOnPartyAction", "classai_1_1_cast_remove_curse_on_party_action.html", "classai_1_1_cast_remove_curse_on_party_action" ],
      [ "CastIceBlockAction", "classai_1_1_cast_ice_block_action.html", "classai_1_1_cast_ice_block_action" ],
      [ "CastMageArmorAction", "classai_1_1_cast_mage_armor_action.html", "classai_1_1_cast_mage_armor_action" ],
      [ "CastIceArmorAction", "classai_1_1_cast_ice_armor_action.html", "classai_1_1_cast_ice_armor_action" ],
      [ "CastFrostArmorAction", "classai_1_1_cast_frost_armor_action.html", "classai_1_1_cast_frost_armor_action" ],
      [ "CastPolymorphAction", "classai_1_1_cast_polymorph_action.html", "classai_1_1_cast_polymorph_action" ],
      [ "CastBlastWaveAction", "classai_1_1_cast_blast_wave_action.html", "classai_1_1_cast_blast_wave_action" ],
      [ "CastEvocationAction", "classai_1_1_cast_evocation_action.html", "classai_1_1_cast_evocation_action" ],
      [ "CastCounterspellOnEnemyHealerAction", "classai_1_1_cast_counterspell_on_enemy_healer_action.html", "classai_1_1_cast_counterspell_on_enemy_healer_action" ]
    ] ],
    [ "MageAiObjectContext.cpp", "_mage_ai_object_context_8cpp.html", [
      [ "StrategyFactoryInternal", "classai_1_1mage_1_1_strategy_factory_internal.html", "classai_1_1mage_1_1_strategy_factory_internal" ],
      [ "MageStrategyFactoryInternal", "classai_1_1mage_1_1_mage_strategy_factory_internal.html", "classai_1_1mage_1_1_mage_strategy_factory_internal" ],
      [ "MageBuffStrategyFactoryInternal", "classai_1_1mage_1_1_mage_buff_strategy_factory_internal.html", "classai_1_1mage_1_1_mage_buff_strategy_factory_internal" ],
      [ "TriggerFactoryInternal", "classai_1_1mage_1_1_trigger_factory_internal.html", "classai_1_1mage_1_1_trigger_factory_internal" ],
      [ "AiObjectContextInternal", "classai_1_1mage_1_1_ai_object_context_internal.html", "classai_1_1mage_1_1_ai_object_context_internal" ]
    ] ],
    [ "MageAiObjectContext.h", "_mage_ai_object_context_8h.html", [
      [ "MageAiObjectContext", "classai_1_1_mage_ai_object_context.html", "classai_1_1_mage_ai_object_context" ]
    ] ],
    [ "MageMultipliers.cpp", "_mage_multipliers_8cpp.html", null ],
    [ "MageMultipliers.h", "_mage_multipliers_8h.html", null ],
    [ "MageTriggers.cpp", "_mage_triggers_8cpp.html", null ],
    [ "MageTriggers.h", "_mage_triggers_8h.html", [
      [ "MageArmorTrigger", "classai_1_1_mage_armor_trigger.html", "classai_1_1_mage_armor_trigger" ],
      [ "LivingBombTrigger", "classai_1_1_living_bomb_trigger.html", "classai_1_1_living_bomb_trigger" ],
      [ "FireballTrigger", "classai_1_1_fireball_trigger.html", "classai_1_1_fireball_trigger" ],
      [ "PyroblastTrigger", "classai_1_1_pyroblast_trigger.html", "classai_1_1_pyroblast_trigger" ],
      [ "HotStreakTrigger", "classai_1_1_hot_streak_trigger.html", "classai_1_1_hot_streak_trigger" ],
      [ "MissileBarrageTrigger", "classai_1_1_missile_barrage_trigger.html", "classai_1_1_missile_barrage_trigger" ],
      [ "ArcaneBlastTrigger", "classai_1_1_arcane_blast_trigger.html", "classai_1_1_arcane_blast_trigger" ],
      [ "CounterspellInterruptSpellTrigger", "classai_1_1_counterspell_interrupt_spell_trigger.html", "classai_1_1_counterspell_interrupt_spell_trigger" ],
      [ "CombustionTrigger", "classai_1_1_combustion_trigger.html", "classai_1_1_combustion_trigger" ],
      [ "IcyVeinsTrigger", "classai_1_1_icy_veins_trigger.html", "classai_1_1_icy_veins_trigger" ],
      [ "PolymorphTrigger", "classai_1_1_polymorph_trigger.html", "classai_1_1_polymorph_trigger" ],
      [ "RemoveCurseTrigger", "classai_1_1_remove_curse_trigger.html", "classai_1_1_remove_curse_trigger" ],
      [ "PartyMemberRemoveCurseTrigger", "classai_1_1_party_member_remove_curse_trigger.html", "classai_1_1_party_member_remove_curse_trigger" ],
      [ "SpellstealTrigger", "classai_1_1_spellsteal_trigger.html", "classai_1_1_spellsteal_trigger" ],
      [ "CounterspellEnemyHealerTrigger", "classai_1_1_counterspell_enemy_healer_trigger.html", "classai_1_1_counterspell_enemy_healer_trigger" ]
    ] ]
];