var _playerbot_a_i_8h =
[
    [ "PlayerbotChatHandler", "class_playerbot_chat_handler.html", "class_playerbot_chat_handler" ],
    [ "MinValueCalculator", "classai_1_1_min_value_calculator.html", "classai_1_1_min_value_calculator" ],
    [ "PacketHandlingHelper", "class_packet_handling_helper.html", "class_packet_handling_helper" ],
    [ "ChatCommandHolder", "class_chat_command_holder.html", "class_chat_command_holder" ],
    [ "PlayerbotAI", "class_playerbot_a_i.html", "class_playerbot_a_i" ],
    [ "BOT_STATE_MAX", "_playerbot_a_i_8h.html#ae6bca7c60760461f3d947592f108aa9d", null ],
    [ "BotState", "_playerbot_a_i_8h.html#a69e2e5b3c03363cf494a3a94d6a4afa7", [
      [ "BOT_STATE_COMBAT", "_playerbot_a_i_8h.html#a69e2e5b3c03363cf494a3a94d6a4afa7a4de8683af8123521cffd0e6b1812d936", null ],
      [ "BOT_STATE_NON_COMBAT", "_playerbot_a_i_8h.html#a69e2e5b3c03363cf494a3a94d6a4afa7a300b84f8924908fe55add6774d233a33", null ],
      [ "BOT_STATE_DEAD", "_playerbot_a_i_8h.html#a69e2e5b3c03363cf494a3a94d6a4afa7a86b0ae8152d741b848cc28b63b56b280", null ]
    ] ],
    [ "IsAlliance", "_playerbot_a_i_8h.html#a91552c43181172df8c73c54e453d8250", null ]
];