var _warden_8h =
[
    [ "WardenModuleUse", "struct_warden_module_use.html", "struct_warden_module_use" ],
    [ "WardenModuleTransfer", "struct_warden_module_transfer.html", "struct_warden_module_transfer" ],
    [ "WardenHashRequest", "struct_warden_hash_request.html", "struct_warden_hash_request" ],
    [ "ClientWardenModule", "struct_client_warden_module.html", "struct_client_warden_module" ],
    [ "Warden", "class_warden.html", "class_warden" ],
    [ "DEFAULT_CLIENT_BUILD", "_warden_8h.html#a15aff44ddae644ee2775ab148b2edcc3", null ],
    [ "Value", "_warden_8h.html#a664590632266b0fee9a252344978cf15", [
      [ "STATE_INITIAL", "_warden_8h.html#a664590632266b0fee9a252344978cf15a0385ff451e822a5b5b8d8c28ac3453bb", null ],
      [ "STATE_REQUESTED_MODULE", "_warden_8h.html#a664590632266b0fee9a252344978cf15a4b1a412e9ee9f566f3367274678b1516", null ],
      [ "STATE_SENT_MODULE", "_warden_8h.html#a664590632266b0fee9a252344978cf15a280c891318da252599eeb770c7e55867", null ],
      [ "STATE_REQUESTED_HASH", "_warden_8h.html#a664590632266b0fee9a252344978cf15a4a13453d4eea48a4b598be545ee5cafc", null ],
      [ "STATE_INITIALIZE_MODULE", "_warden_8h.html#a664590632266b0fee9a252344978cf15aba9607dfe5d98555b37fffac4511663a", null ],
      [ "STATE_REQUESTED_DATA", "_warden_8h.html#a664590632266b0fee9a252344978cf15ac3c487f1e8a7b3b84523657796645cb8", null ],
      [ "STATE_RESTING", "_warden_8h.html#a664590632266b0fee9a252344978cf15a68bd94ba8eed1532991ac7953ce79aab", null ]
    ] ],
    [ "WardenCheckType", "_warden_8h.html#a7598131f87414a66259189a0518ca6f2", [
      [ "MEM_CHECK", "_warden_8h.html#a7598131f87414a66259189a0518ca6f2aba449b452d425f871a9c01be028b5074", null ],
      [ "PAGE_CHECK_A", "_warden_8h.html#a7598131f87414a66259189a0518ca6f2a331b64b77a83e62b070746aab1ea995f", null ],
      [ "PAGE_CHECK_B", "_warden_8h.html#a7598131f87414a66259189a0518ca6f2a366d33d737ecb1b63b92e5c4fc13ed87", null ],
      [ "MPQ_CHECK", "_warden_8h.html#a7598131f87414a66259189a0518ca6f2ab22116b6a319b5c4ad88eaf90291090b", null ],
      [ "LUA_STR_CHECK", "_warden_8h.html#a7598131f87414a66259189a0518ca6f2af1774658e9887733caa18983d26d0d3a", null ],
      [ "DRIVER_CHECK", "_warden_8h.html#a7598131f87414a66259189a0518ca6f2a2b63e52f1c3146183a59fffa98919c93", null ],
      [ "TIMING_CHECK", "_warden_8h.html#a7598131f87414a66259189a0518ca6f2ad11a9e323f324f0c3b2b439fec521018", null ],
      [ "PROC_CHECK", "_warden_8h.html#a7598131f87414a66259189a0518ca6f2a19186939917116389af429c3c658293d", null ],
      [ "MODULE_CHECK", "_warden_8h.html#a7598131f87414a66259189a0518ca6f2adc2e09d27e8f622121d11ba931c4ff77", null ]
    ] ],
    [ "WardenOpcodes", "_warden_8h.html#a8d0de714c4a4cf7a125c6187a7d19e51", [
      [ "WARDEN_CMSG_MODULE_MISSING", "_warden_8h.html#a8d0de714c4a4cf7a125c6187a7d19e51a334622303a3ea504196a3807f5da22f4", null ],
      [ "WARDEN_CMSG_MODULE_OK", "_warden_8h.html#a8d0de714c4a4cf7a125c6187a7d19e51a7636fd5a47dd45d2bf69a4625d77939c", null ],
      [ "WARDEN_CMSG_CHEAT_CHECKS_RESULT", "_warden_8h.html#a8d0de714c4a4cf7a125c6187a7d19e51a713a20be2d2cf1fa7d46ee2b788bc6f5", null ],
      [ "WARDEN_CMSG_MEM_CHECKS_RESULT", "_warden_8h.html#a8d0de714c4a4cf7a125c6187a7d19e51adaac6c4f6f3ac87386438a5228ba617f", null ],
      [ "WARDEN_CMSG_HASH_RESULT", "_warden_8h.html#a8d0de714c4a4cf7a125c6187a7d19e51a51c0070c4347129060a6714ae0f483ed", null ],
      [ "WARDEN_CMSG_MODULE_FAILED", "_warden_8h.html#a8d0de714c4a4cf7a125c6187a7d19e51a5a2e35d214965eb7dccec345e261fadc", null ],
      [ "WARDEN_SMSG_MODULE_USE", "_warden_8h.html#a8d0de714c4a4cf7a125c6187a7d19e51ad8b63ed3f3ec8941563402a5eacf9d30", null ],
      [ "WARDEN_SMSG_MODULE_CACHE", "_warden_8h.html#a8d0de714c4a4cf7a125c6187a7d19e51aa12b4718a7515d81608ba46205625a11", null ],
      [ "WARDEN_SMSG_CHEAT_CHECKS_REQUEST", "_warden_8h.html#a8d0de714c4a4cf7a125c6187a7d19e51ae5eafc7b23b0d14c934e9eb48fa73709", null ],
      [ "WARDEN_SMSG_MODULE_INITIALIZE", "_warden_8h.html#a8d0de714c4a4cf7a125c6187a7d19e51aafdff6ee4abd8ad4511d527f09154da8", null ],
      [ "WARDEN_SMSG_MEM_CHECKS_REQUEST", "_warden_8h.html#a8d0de714c4a4cf7a125c6187a7d19e51a41d76534b91d6af9e1b3ca116c4eda82", null ],
      [ "WARDEN_SMSG_HASH_REQUEST", "_warden_8h.html#a8d0de714c4a4cf7a125c6187a7d19e51a69c3028f1b435c282c384008e49eb451", null ]
    ] ],
    [ "to_string", "_warden_8h.html#a20950c22673d2077cce98da774dffee7", null ]
];