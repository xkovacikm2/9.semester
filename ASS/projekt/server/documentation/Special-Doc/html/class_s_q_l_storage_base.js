var class_s_q_l_storage_base =
[
    [ "SQLSIterator", "class_s_q_l_storage_base_1_1_s_q_l_s_iterator.html", "class_s_q_l_storage_base_1_1_s_q_l_s_iterator" ],
    [ "SQLStorageBase", "class_s_q_l_storage_base.html#ac7d2ba0646863f4fc93ccd3894f86315", null ],
    [ "~SQLStorageBase", "class_s_q_l_storage_base.html#aaec1b1a43f3172270effc9a55a8408a2", null ],
    [ "EntryFieldName", "class_s_q_l_storage_base.html#a41cd480238b2c00a9e63a11726694b8a", null ],
    [ "Free", "class_s_q_l_storage_base.html#a6e6542eceb8690ce460cac9b91051e35", null ],
    [ "getDataBegin", "class_s_q_l_storage_base.html#a8b40f603e886470f83ddc00ffc16d5af", null ],
    [ "getDataEnd", "class_s_q_l_storage_base.html#aa6daf651e6d4aad8ee71e88584f560e9", null ],
    [ "GetDstFieldCount", "class_s_q_l_storage_base.html#a412076362104f5d1b4c4f77e80543bbd", null ],
    [ "GetDstFormat", "class_s_q_l_storage_base.html#a73062ad2239e8de4c38ecafd7f45ee63", null ],
    [ "GetDstFormat", "class_s_q_l_storage_base.html#aa68b2a5eee24dc60ed8ae1bc2a86c94e", null ],
    [ "GetMaxEntry", "class_s_q_l_storage_base.html#a56f27ea31311b62af2fa04d071142c5f", null ],
    [ "GetRecordCount", "class_s_q_l_storage_base.html#a5374514bffb40905fc8792effce785ad", null ],
    [ "GetRecordSize", "class_s_q_l_storage_base.html#aae8d24d21cb0d187d453e1461069a78d", null ],
    [ "GetSrcFieldCount", "class_s_q_l_storage_base.html#ab0dec9bcb1b0b0683344130e604b5c67", null ],
    [ "GetSrcFormat", "class_s_q_l_storage_base.html#aef2bc42dc4755377708d8e9a5d792f98", null ],
    [ "GetSrcFormat", "class_s_q_l_storage_base.html#afa098827fc0621d287906515943ae4a6", null ],
    [ "GetTableName", "class_s_q_l_storage_base.html#a6a89c04b282d7b27e9fee607a445822d", null ],
    [ "Initialize", "class_s_q_l_storage_base.html#aab6eb326f56e5e261dc0ec6a72abac04", null ],
    [ "JustCreatedRecord", "class_s_q_l_storage_base.html#ab18d6965048fc4d1950675ee04bc5b4e", null ],
    [ "prepareToLoad", "class_s_q_l_storage_base.html#aa94dcc1375f188203c0b3efbc80ea547", null ],
    [ "SQLStorageLoaderBase", "class_s_q_l_storage_base.html#a1543906b0ecd4eb00fa6b84a4c8b45ca", null ]
];