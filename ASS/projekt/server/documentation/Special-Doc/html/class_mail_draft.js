var class_mail_draft =
[
    [ "MailDraft", "class_mail_draft.html#a53596ca9ae0af0f03c315ed452443415", null ],
    [ "MailDraft", "class_mail_draft.html#a8a7a37890b6a280f2b08659e5e59b354", null ],
    [ "MailDraft", "class_mail_draft.html#acbf960edb0e18027c6d9b03078a78827", null ],
    [ "MailDraft", "group__mailing.html#gacb18bc965bc0234262806b01caaed801", null ],
    [ "AddItem", "group__mailing.html#gae0c2b4bac4dd8f2a8c53608c45cbb11a", null ],
    [ "CloneFrom", "group__mailing.html#gacf5946d8763375f56589ea42aef7f70c", null ],
    [ "GetBodyId", "class_mail_draft.html#a55cfeb3fdad89b1381da022353d981c6", null ],
    [ "GetCOD", "class_mail_draft.html#a2b4d799ab7ec945b60c07747981ad0a5", null ],
    [ "GetMailTemplateId", "class_mail_draft.html#ab5906ccf0213f8be241fbb177fef53bd", null ],
    [ "GetMoney", "class_mail_draft.html#a461ee77c1d85b07513d5f453c8c8507a", null ],
    [ "GetSubject", "class_mail_draft.html#a1d7aa2b22b316978d250470a89dfe762", null ],
    [ "SendMailTo", "group__mailing.html#gaf1f4156c2ab653e42f25cbfa38464afd", null ],
    [ "SendReturnToSender", "group__mailing.html#gab22dc74fdab62a16f236a4eaa77de25f", null ],
    [ "SetCOD", "class_mail_draft.html#a3c98b0cfe95e16a8554beb268fbf6bc0", null ],
    [ "SetMailTemplate", "class_mail_draft.html#a011367cbf5835bca6afd87b2214e4fa0", null ],
    [ "SetMoney", "class_mail_draft.html#ac1f6c0b9cd87f9b87080fc91369ef0ab", null ],
    [ "SetSubjectAndBody", "group__mailing.html#gad3454e2d4ab6f9ce54b8de86780f1207", null ],
    [ "SetSubjectAndBodyId", "class_mail_draft.html#a2b30032f6a68dc9da7d1e164f1614bd5", null ]
];