var class_field =
[
    [ "SimpleDataTypes", "class_field.html#a4e89220ea817d31932df6085b3d6d513", [
      [ "DB_TYPE_UNKNOWN", "class_field.html#a4e89220ea817d31932df6085b3d6d513a4440ef0b48b1ef81037360fa1e6a5819", null ],
      [ "DB_TYPE_STRING", "class_field.html#a4e89220ea817d31932df6085b3d6d513a5d6d9e7923f2b97eff423dd57b2182fe", null ],
      [ "DB_TYPE_INTEGER", "class_field.html#a4e89220ea817d31932df6085b3d6d513a5b4ed4109f05eccf78af783e6d1f8bc5", null ],
      [ "DB_TYPE_FLOAT", "class_field.html#a4e89220ea817d31932df6085b3d6d513a9fcd49516f10b7159ebf6c4a841b07c5", null ],
      [ "DB_TYPE_BOOL", "class_field.html#a4e89220ea817d31932df6085b3d6d513a4a6bd7ed5cf390f5f31dd9a9c080e6f8", null ]
    ] ],
    [ "Field", "class_field.html#a3e804c92273d9159f413f227b535c672", null ],
    [ "Field", "class_field.html#a142ecf35ae826aea685fb3a03b3a383f", null ],
    [ "~Field", "class_field.html#a45d6e6d09b8f8e46de62b40119d62c60", null ],
    [ "GetBool", "class_field.html#af5cd02c50067a95e3e6771d48bfbc92d", null ],
    [ "GetCppString", "class_field.html#ac3fe5bf9cdc61b5be66a4c79598cba5e", null ],
    [ "GetDouble", "class_field.html#a5ff6f51d43c166c486396e28f8366eac", null ],
    [ "GetFloat", "class_field.html#a8b05a34c3d0cb92a9d2c0f298aa11dea", null ],
    [ "GetInt16", "class_field.html#a78dc100d473c7e8e902e4eb28ed4b9de", null ],
    [ "GetInt32", "class_field.html#afc9e673b7e6607f5f52bf16c77a73e5e", null ],
    [ "GetInt64", "class_field.html#aeaba7d306722e6d3395bf4029fa5dcb0", null ],
    [ "GetInt8", "class_field.html#a714d8d2cb518f48e7ba65fc3176a5002", null ],
    [ "GetString", "class_field.html#ab8c1a7e73265f7b14457ab37cf20681c", null ],
    [ "GetType", "class_field.html#a1d108fa4fc5dd358ed96ddbce71850ed", null ],
    [ "GetUInt16", "class_field.html#a7bbb15bfb8f64f94b26c7cfffe83ac58", null ],
    [ "GetUInt32", "class_field.html#af71c97fd38da328f4d9590b112f859c4", null ],
    [ "GetUInt64", "class_field.html#a757dd8cac1b769fdb82d8a529bad9d5b", null ],
    [ "GetUInt8", "class_field.html#a090beafdf8b41555058a87d00443498a", null ],
    [ "IsNULL", "class_field.html#a7b037d8b24855c7bc06a4592d205a994", null ],
    [ "SetType", "class_field.html#a8490276a96ef132c75678fe4d2e4117d", null ],
    [ "SetValue", "class_field.html#aacf37547df8b1843d209f72718c371ba", null ]
];