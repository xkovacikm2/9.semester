var searchData=
[
  ['object_5fend',['OBJECT_END',['../_update_fields_8h.html#a3a7fe22e4a9448b53c4dc1fb45104e98a61c3cb66fef4709ef0e13388b1da3e27',1,'UpdateFields.h']]],
  ['object_5ffield_5fcreated_5fby',['OBJECT_FIELD_CREATED_BY',['../_update_fields_8h.html#aee9d327a38d3cfe074ba81a44817491fa8b5005617fee45461762adae1efc1577',1,'UpdateFields.h']]],
  ['object_5ffield_5fentry',['OBJECT_FIELD_ENTRY',['../_update_fields_8h.html#a3a7fe22e4a9448b53c4dc1fb45104e98a33c80ae0f59eb77bb0c60ad19614c3cc',1,'UpdateFields.h']]],
  ['object_5ffield_5fguid',['OBJECT_FIELD_GUID',['../_update_fields_8h.html#a3a7fe22e4a9448b53c4dc1fb45104e98a950d7d85da26690aa29ee92cd38c029a',1,'UpdateFields.h']]],
  ['object_5ffield_5fpadding',['OBJECT_FIELD_PADDING',['../_update_fields_8h.html#a3a7fe22e4a9448b53c4dc1fb45104e98ae676dc41ea4f052d9e8f8e9f759bf600',1,'UpdateFields.h']]],
  ['object_5ffield_5fscale_5fx',['OBJECT_FIELD_SCALE_X',['../_update_fields_8h.html#a3a7fe22e4a9448b53c4dc1fb45104e98a0dadb5eae03b1016a434daf8a85812b1',1,'UpdateFields.h']]],
  ['object_5ffield_5ftype',['OBJECT_FIELD_TYPE',['../_update_fields_8h.html#a3a7fe22e4a9448b53c4dc1fb45104e98aa2fef0c60d701512aecc588d1c45c274',1,'UpdateFields.h']]],
  ['obsolete_5fdrop_5fitem',['OBSOLETE_DROP_ITEM',['../group__u2w.html#ggabee94b75d47d66df99ee7dceb0362ba6a1eee202eb9ea4b3dac32bf36e077c3b4',1,'Opcodes.h']]],
  ['off_5fattack',['OFF_ATTACK',['../_shared_defines_8h.html#a2a35652af7129b6f139b0f7a2af1876aa052681ec165e5b5e3edcf2089607ae67',1,'SharedDefines.h']]],
  ['offhand_5fcrit_5fpercentage',['OFFHAND_CRIT_PERCENTAGE',['../group__game.html#gga938646605f1e70b8f1613bf9bdb78b4fa379fa05fe54b3ef17e6e9e2f159b9a8f',1,'Unit.h']]],
  ['opvp_5fid_5fep',['OPVP_ID_EP',['../_outdoor_pv_p_mgr_8h.html#a56e7764289940901c2a230103f78adeea6d9e0ed7b779c9649a0e8f80be6a105e',1,'OutdoorPvPMgr.h']]],
  ['opvp_5fid_5fsi',['OPVP_ID_SI',['../_outdoor_pv_p_mgr_8h.html#a56e7764289940901c2a230103f78adeead0567d95b6c07b93d6c9c84cce75851f',1,'OutdoorPvPMgr.h']]],
  ['orange',['ORANGE',['../namespace_ma_n_g_o_s_1_1_x_p.html#afc54bbbcb5be759cb12031b094af3b4bab7126adf14b849854163036d8383de58',1,'MaNGOS::XP']]],
  ['owner_5fpermission',['OWNER_PERMISSION',['../_loot_mgr_8h.html#a10c103da8c7dee2fd4e431c086a4d411a1750b08b9056ac1ab4f1115ed33ffa5f',1,'LootMgr.h']]]
];
