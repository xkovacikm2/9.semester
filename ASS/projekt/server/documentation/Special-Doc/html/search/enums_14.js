var searchData=
[
  ['unitauraflags',['UnitAuraFlags',['../group__game.html#ga2522c0ae23b027aa47991e325e436c9d',1,'Unit.h']]],
  ['unitbytes1_5fflags',['UnitBytes1_Flags',['../group__game.html#ga5675a5c1697c67bf33e72b41232b39ea',1,'Unit.h']]],
  ['unitbytes2_5fflags',['UnitBytes2_Flags',['../group__game.html#ga3aba4c05058efe3a2c3e41b06ef495fb',1,'Unit.h']]],
  ['unitdynflags',['UnitDynFlags',['../_shared_defines_8h.html#a6d2f7a5452237dadf8fd3aed70f44039',1,'SharedDefines.h']]],
  ['unitflags',['UnitFlags',['../group__game.html#gafaf72688a238d4ba16ba2ae1444cd552',1,'Unit.h']]],
  ['unitmodifiertype',['UnitModifierType',['../group__game.html#gaa21edecdaa2e077b2b87bbe5f2b2e7ee',1,'Unit.h']]],
  ['unitmods',['UnitMods',['../group__game.html#ga4c5336cd4be5f0162bd4874b0a1e5bde',1,'Unit.h']]],
  ['unitmovetype',['UnitMoveType',['../group__game.html#gaf91f7580a89722b7dc45a018bb46fce4',1,'Unit.h']]],
  ['unitstandstatetype',['UnitStandStateType',['../group__game.html#ga809d475cf0ffeaa1ddad86f73e24a7c1',1,'Unit.h']]],
  ['unitstate',['UnitState',['../group__game.html#gae51d2ecb71f7a5cb3882982a622c9544',1,'Unit.h']]],
  ['unitthreateventtype',['UnitThreatEventType',['../_unit_events_8h.html#a0d3a1a8d294bb912afb0ed94576abcf3',1,'UnitEvents.h']]],
  ['unitvisibility',['UnitVisibility',['../group__game.html#ga438f0aec316c954744ade1a737ec0a1d',1,'Unit.h']]],
  ['updatemaskcount',['UpdateMaskCount',['../class_update_mask.html#a667c2f210ee4a2417a2a3ba486d1332f',1,'UpdateMask']]],
  ['updateresult',['UpdateResult',['../class_movement_1_1_move_spline.html#a6fcd01e84f328619ce1a8dbbfa8143b0',1,'Movement::MoveSpline']]],
  ['usedareaside',['UsedAreaSide',['../_object_pos_selector_8h.html#a7364d69554fbce0c0fdd077fe7f5cfdc',1,'ObjectPosSelector.h']]]
];
