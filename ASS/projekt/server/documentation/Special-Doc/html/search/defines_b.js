var searchData=
[
  ['m_5fpi',['M_PI',['../_common_8h.html#ae71449b1cc6e6250b91f539153a7a0d3',1,'Common.h']]],
  ['m_5fpi_5ff',['M_PI_F',['../_common_8h.html#ab9c3c609e1d02430671de0a109410ac8',1,'Common.h']]],
  ['make_5fpair32',['MAKE_PAIR32',['../_common_8h.html#a614d8428709be2057ec890c19af56e05',1,'Common.h']]],
  ['make_5fpair64',['MAKE_PAIR64',['../_common_8h.html#a3729c6dc72a514e186c575d6a48f4e0a',1,'Common.h']]],
  ['make_5fskill_5fbonus',['MAKE_SKILL_BONUS',['../_player_8cpp.html#a92abd4a228d5facfb47326df8ac40d9e',1,'Player.cpp']]],
  ['make_5fskill_5fvalue',['MAKE_SKILL_VALUE',['../_player_8cpp.html#ae9b5b6063ec5d1c0d49342093f384733',1,'Player.cpp']]],
  ['mangos',['MANGOS',['../_shared_defines_8h.html#aceab3c27645b02780fe616cbf69e43e7',1,'SharedDefines.h']]],
  ['mangos_5fassert',['MANGOS_ASSERT',['../_errors_8h.html#a7b94d20194b84dde056465e12496d3f4',1,'Errors.h']]],
  ['mangos_5fbigendian',['MANGOS_BIGENDIAN',['../_define_8h.html#a0e013221d0c9158d6822d895360043ed',1,'Define.h']]],
  ['mangos_5fclose_5flibrary',['MANGOS_CLOSE_LIBRARY',['../_define_8h.html#ab841656a34f1d02e7b53105ca9928c3c',1,'Define.h']]],
  ['mangos_5fdll_5fspec',['MANGOS_DLL_SPEC',['../_define_8h.html#a5ae944080d5d77aab81cd723c8a7d17c',1,'Define.h']]],
  ['mangos_5fendian',['MANGOS_ENDIAN',['../_define_8h.html#ad947f61c9131201c0f0e0874f78fb854',1,'Define.h']]],
  ['mangos_5fexport',['MANGOS_EXPORT',['../_define_8h.html#ac2ca3badeee545ebad591cd1ea2405a0',1,'Define.h']]],
  ['mangos_5fget_5fproc_5faddr',['MANGOS_GET_PROC_ADDR',['../_define_8h.html#adfb802d61595170b983f1a4357af9af9',1,'Define.h']]],
  ['mangos_5fimport',['MANGOS_IMPORT',['../_define_8h.html#ac09c130c99fab0bc61bf5d5286c138df',1,'Define.h']]],
  ['mangos_5finline',['MANGOS_INLINE',['../_define_8h.html#a426502c01f341ae7ff9c6a87f9d52e4e',1,'Define.h']]],
  ['mangos_5flittleendian',['MANGOS_LITTLEENDIAN',['../_define_8h.html#a95cf7ffa446d971ce2eb95ee3c2d1df5',1,'Define.h']]],
  ['mangos_5fload_5flibrary',['MANGOS_LOAD_LIBRARY',['../_define_8h.html#a5ee9f9fa4ef02206206286bce2ef42ce',1,'Define.h']]],
  ['mangos_5fpath_5fmax',['MANGOS_PATH_MAX',['../_define_8h.html#ae723c562bb1d8446f2c5a638db2414d6',1,'Define.h']]],
  ['mangos_5fscript_5fname',['MANGOS_SCRIPT_NAME',['../_define_8h.html#a8a7ed2a2994441e14856cd944fd5dbd2',1,'Define.h']]],
  ['mangos_5fscript_5fprefix',['MANGOS_SCRIPT_PREFIX',['../_define_8h.html#a3e4f6b58c023374638cefc6139742b7b',1,'Define.h']]],
  ['mangos_5fscript_5fsuffix',['MANGOS_SCRIPT_SUFFIX',['../_define_8h.html#a11cedd047b4902c69c37a3665ae2addb',1,'Define.h']]],
  ['map_5fall_5fliquids',['MAP_ALL_LIQUIDS',['../_grid_map_8h.html#ab120de38f25fcadb7db17ec59fcae4e3',1,'GridMap.h']]],
  ['map_5farea_5fno_5farea',['MAP_AREA_NO_AREA',['../_grid_map_8h.html#a29bbacae053e66ee8b7fa97db3cbd127',1,'GridMap.h']]],
  ['map_5ffilename_5fextension2',['MAP_FILENAME_EXTENSION2',['../_v_map_manager2_8h.html#aad39c14e5fae9fd5141df154332ae6fd',1,'VMapManager2.h']]],
  ['map_5fhalfsize',['MAP_HALFSIZE',['../_grid_defines_8h.html#aa9d6737eb1c6d3c55c7707d0ad90d9f0',1,'GridDefines.h']]],
  ['map_5fheight_5fas_5fint16',['MAP_HEIGHT_AS_INT16',['../_grid_map_8h.html#aefa74fa33f3eba2d39b407c2fd2e8aba',1,'GridMap.h']]],
  ['map_5fheight_5fas_5fint8',['MAP_HEIGHT_AS_INT8',['../_grid_map_8h.html#a15a9245a53c00f1a8a89dd2d798cea9d',1,'GridMap.h']]],
  ['map_5fheight_5fno_5fheight',['MAP_HEIGHT_NO_HEIGHT',['../_grid_map_8h.html#aa5c2437972b2be5c5ff23a7ac04e2680',1,'GridMap.h']]],
  ['map_5fliquid_5fno_5fheight',['MAP_LIQUID_NO_HEIGHT',['../_grid_map_8h.html#a59daadc7c96729526e05982ec6600bb1',1,'GridMap.h']]],
  ['map_5fliquid_5fno_5ftype',['MAP_LIQUID_NO_TYPE',['../_grid_map_8h.html#a42b8b790a3d284e7116bcad1c391b8f9',1,'GridMap.h']]],
  ['map_5fliquid_5ftype_5fdark_5fwater',['MAP_LIQUID_TYPE_DARK_WATER',['../_grid_map_8h.html#a661f965d11d7da56890b64484d871441',1,'GridMap.h']]],
  ['map_5fliquid_5ftype_5fmagma',['MAP_LIQUID_TYPE_MAGMA',['../_grid_map_8h.html#a14e2a0a2291347b0ef877bc9d70f7384',1,'GridMap.h']]],
  ['map_5fliquid_5ftype_5fno_5fwater',['MAP_LIQUID_TYPE_NO_WATER',['../_grid_map_8h.html#a17d4c5ea32412f0fbe04a27f63d50e13',1,'GridMap.h']]],
  ['map_5fliquid_5ftype_5focean',['MAP_LIQUID_TYPE_OCEAN',['../_grid_map_8h.html#a6d257e0effb928fba7534eeee5de602d',1,'GridMap.h']]],
  ['map_5fliquid_5ftype_5fslime',['MAP_LIQUID_TYPE_SLIME',['../_grid_map_8h.html#aa56f76510545ed9c706b8fee3061b6cb',1,'GridMap.h']]],
  ['map_5fliquid_5ftype_5fwater',['MAP_LIQUID_TYPE_WATER',['../_grid_map_8h.html#a5ec110bd30ec47fcd087aaf5d572de28',1,'GridMap.h']]],
  ['map_5fliquid_5ftype_5fwmo_5fwater',['MAP_LIQUID_TYPE_WMO_WATER',['../_grid_map_8h.html#aa3f0e8c71e8df3160f681246fdd27002',1,'GridMap.h']]],
  ['map_5fresolution',['MAP_RESOLUTION',['../_grid_defines_8h.html#a350873099e339fcc394b9eefa121ed6e',1,'GridDefines.h']]],
  ['map_5fsize',['MAP_SIZE',['../_grid_defines_8h.html#a0a82fd70402bbdc2259248e735fecbca',1,'GridDefines.h']]],
  ['max_5faccount_5fstr',['MAX_ACCOUNT_STR',['../_account_mgr_8h.html#a134ebf22666a6525929447b2d869d400',1,'AccountMgr.h']]],
  ['max_5faction_5fbutton_5faction_5fvalue',['MAX_ACTION_BUTTON_ACTION_VALUE',['../_player_8h.html#aea4cac3aceaad70ea9b8397854b0128b',1,'Player.h']]],
  ['max_5faction_5fbuttons',['MAX_ACTION_BUTTONS',['../_player_8h.html#aa44542ab1037b2da52095a5c8e923bb2',1,'Player.h']]],
  ['max_5factions',['MAX_ACTIONS',['../_creature_event_a_i_8h.html#ab0cd711cc5dcd823de161216f3f5d313',1,'CreatureEventAI.h']]],
  ['max_5fattack',['MAX_ATTACK',['../_shared_defines_8h.html#a6df4c351d81376594e8df6a4e39ac9fe',1,'SharedDefines.h']]],
  ['max_5fauctions',['MAX_AUCTIONS',['../_ah_bot_8h.html#a54ccaca4dc510f64ea4a5033dd66829e',1,'AhBot.h']]],
  ['max_5fbag_5fsize',['MAX_BAG_SIZE',['../_bag_8h.html#a60a065b8fab298fb24dfd8f22f657d77',1,'Bag.h']]],
  ['max_5fbattleground_5fbrackets',['MAX_BATTLEGROUND_BRACKETS',['../_battle_ground_8h.html#a5201a522ecfb21f8b94bcab8b2ba2f8c',1,'BattleGround.h']]],
  ['max_5fbattleground_5fqueue_5ftypes',['MAX_BATTLEGROUND_QUEUE_TYPES',['../_battle_ground_8h.html#a43b2f4f5367b56c20d62a5794a85fcd5',1,'BattleGround.h']]],
  ['max_5fbattleground_5ftype_5fid',['MAX_BATTLEGROUND_TYPE_ID',['../_shared_defines_8h.html#add6e677d0cfd3f1eff02a4dd3b90b95c',1,'SharedDefines.h']]],
  ['max_5fbind_5ftype',['MAX_BIND_TYPE',['../_item_prototype_8h.html#ad481bb2c6de60d1bf303856b65393030',1,'ItemPrototype.h']]],
  ['max_5fcharter_5fname',['MAX_CHARTER_NAME',['../_object_mgr_8h.html#a2806b86f3c2ca37ebed1aa50d750f878',1,'ObjectMgr.h']]],
  ['max_5fchat_5fmsg_5ftype',['MAX_CHAT_MSG_TYPE',['../_shared_defines_8h.html#a5ee700eeb3fcc1390f2699f3c36d9607',1,'SharedDefines.h']]],
  ['max_5fclasses',['MAX_CLASSES',['../_shared_defines_8h.html#aa322a61b17e1d56852d53d5ef6a728c6',1,'SharedDefines.h']]],
  ['max_5fconnection_5fpool_5fsize',['MAX_CONNECTION_POOL_SIZE',['../_database_8cpp.html#afe60a46f0f1a922aea80fd169a76fe05',1,'Database.cpp']]],
  ['max_5fcorpse_5ftype',['MAX_CORPSE_TYPE',['../_corpse_8h.html#aec0512ffe7d6a8f7dd8330c334bb4fc3',1,'Corpse.h']]],
  ['max_5fcreature_5fai_5ftext_5fstring_5fid',['MAX_CREATURE_AI_TEXT_STRING_ID',['../_object_mgr_8h.html#abc7259895b38f933a190a4f5088829c6',1,'ObjectMgr.h']]],
  ['max_5fcreature_5fclass',['MAX_CREATURE_CLASS',['../_shared_defines_8h.html#a27035915cde38c379d1c02527ff95d61',1,'SharedDefines.h']]],
  ['max_5fcreature_5fmodel',['MAX_CREATURE_MODEL',['../_creature_8h.html#aedb9af27fc549b3c68e6673cb7f7c491',1,'Creature.h']]],
  ['max_5fcreature_5fspell_5fdata_5fslot',['MAX_CREATURE_SPELL_DATA_SLOT',['../_d_b_c_structure_8h.html#a84f97addb2541cd13960d71b99ac4f9c',1,'DBCStructure.h']]],
  ['max_5fdb_5fscript_5fstring_5fid',['MAX_DB_SCRIPT_STRING_ID',['../_object_mgr_8h.html#a6bd17904173069ddafc97475a6322d54',1,'ObjectMgr.h']]],
  ['max_5fdeath_5fcount',['MAX_DEATH_COUNT',['../_player_8cpp.html#aa01c39bcd3c3a1679c8dc86a647d0c8c',1,'Player.cpp']]],
  ['max_5fdps_5fcount',['MAX_DPS_COUNT',['../_l_f_g_mgr_8h.html#a959b984e754c2856bb87c47bd24a7d5d',1,'LFGMgr.h']]],
  ['max_5fdrunken',['MAX_DRUNKEN',['../_player_8h.html#aa54329cb1b1a81d0eeed65b6b11f7465',1,'Player.h']]],
  ['max_5feffect_5findex',['MAX_EFFECT_INDEX',['../_d_b_c_enums_8h.html#a038867f3f1d822b38a6cad2411c8006a',1,'DBCEnums.h']]],
  ['max_5fenchantment_5foffset',['MAX_ENCHANTMENT_OFFSET',['../_item_8h.html#a468d419c5aa228b6604358f9af3f15c6',1,'Item.h']]],
  ['max_5ffall_5fdistance',['MAX_FALL_DISTANCE',['../_grid_map_8h.html#a91e57a946b1b9303401fd9140f3cd13e',1,'GridMap.h']]],
  ['max_5fgameobject_5ftype',['MAX_GAMEOBJECT_TYPE',['../_shared_defines_8h.html#a6eae8e849308352a3006cfcdc0773fb9',1,'SharedDefines.h']]],
  ['max_5fge_5fcheck_5fdelay',['max_ge_check_delay',['../_game_event_mgr_8h.html#adaa73d920934a951856f7ce003ce2342',1,'GameEventMgr.h']]],
  ['max_5fgender',['MAX_GENDER',['../_shared_defines_8h.html#a4e17496e7798f79a7563f2e0672d3df4',1,'SharedDefines.h']]],
  ['max_5fgo_5fstate',['MAX_GO_STATE',['../_game_object_8h.html#ac148792c2982ecdddc593bf3871aa520',1,'GameObject.h']]],
  ['max_5fgossip_5ftext_5foptions',['MAX_GOSSIP_TEXT_OPTIONS',['../_n_p_c_handler_8h.html#a114c9636000e930cd1dc1ef7e444814f',1,'NPCHandler.h']]],
  ['max_5fgroup_5fsize',['MAX_GROUP_SIZE',['../_group_8h.html#a6ce981cf860637abfb692b61c5af0d3b',1,'Group.h']]],
  ['max_5fheight',['MAX_HEIGHT',['../_grid_map_8h.html#a9059fa76eb5e8e86f870405d63e72c4c',1,'GridMap.h']]],
  ['max_5finternal_5fplayer_5fname',['MAX_INTERNAL_PLAYER_NAME',['../_object_mgr_8h.html#ad432b0af08f147d309c03a036cd55380',1,'ObjectMgr.h']]],
  ['max_5finvtype',['MAX_INVTYPE',['../_item_prototype_8h.html#a702b6b8feb78b93f9bff8be0c6ba3043',1,'ItemPrototype.h']]],
  ['max_5fitem_5fclass',['MAX_ITEM_CLASS',['../_item_prototype_8h.html#a5608443419f5f9f8fcb53cca2d076f65',1,'ItemPrototype.h']]],
  ['max_5fitem_5fclass_5freserved_5f2',['MAX_ITEM_CLASS_RESERVED_2',['../_item_prototype_8h.html#afa01bc7fbb4cf2c1d06e34dcfc320935',1,'ItemPrototype.h']]],
  ['max_5fitem_5fclass_5freserved_5f3',['MAX_ITEM_CLASS_RESERVED_3',['../_item_prototype_8h.html#a2f0c4db9b5e47f7ad05209c7be0fe394',1,'ItemPrototype.h']]],
  ['max_5fitem_5fmod',['MAX_ITEM_MOD',['../_item_prototype_8h.html#af6071012b6b7e1cc95ef7f23e39ec508',1,'ItemPrototype.h']]],
  ['max_5fitem_5fproto_5fdamages',['MAX_ITEM_PROTO_DAMAGES',['../_item_prototype_8h.html#a857b44d18e4136e85a19074cfbda600b',1,'ItemPrototype.h']]],
  ['max_5fitem_5fproto_5fspells',['MAX_ITEM_PROTO_SPELLS',['../_item_prototype_8h.html#a9e238c2727e820f5207cccdb26e2ce95',1,'ItemPrototype.h']]],
  ['max_5fitem_5fproto_5fstats',['MAX_ITEM_PROTO_STATS',['../_item_prototype_8h.html#af192b935eb90a06b3e1f3ffd2b215d4d',1,'ItemPrototype.h']]],
  ['max_5fitem_5fquality',['MAX_ITEM_QUALITY',['../_shared_defines_8h.html#a8b7f3e9bd0473c61e8fd7bb42b24faaa',1,'SharedDefines.h']]],
  ['max_5fitem_5freq_5ftarget_5ftype',['MAX_ITEM_REQ_TARGET_TYPE',['../_item_8h.html#a46feea1e662a3f8cab146ae2111ac4f9',1,'Item.h']]],
  ['max_5fitem_5fspelltrigger',['MAX_ITEM_SPELLTRIGGER',['../_item_prototype_8h.html#af15c7bccd24864a77d750d3cbab3d69b',1,'ItemPrototype.h']]],
  ['max_5fitem_5fsubclass_5farmor',['MAX_ITEM_SUBCLASS_ARMOR',['../_item_prototype_8h.html#a5e3522824084a3eb818148f51174a3e5',1,'ItemPrototype.h']]],
  ['max_5fitem_5fsubclass_5fconsumable',['MAX_ITEM_SUBCLASS_CONSUMABLE',['../_item_prototype_8h.html#ab859d961c68d9e8bd15464d1231b4e32',1,'ItemPrototype.h']]],
  ['max_5fitem_5fsubclass_5fcontainer',['MAX_ITEM_SUBCLASS_CONTAINER',['../_item_prototype_8h.html#a87b85193a541dd2c2fc4c576b05cd659',1,'ItemPrototype.h']]],
  ['max_5fitem_5fsubclass_5fjunk',['MAX_ITEM_SUBCLASS_JUNK',['../_item_prototype_8h.html#a7b7b114a3fe7bbdc881d396d229eeec1',1,'ItemPrototype.h']]],
  ['max_5fitem_5fsubclass_5fkey',['MAX_ITEM_SUBCLASS_KEY',['../_item_prototype_8h.html#a15f1649d3ff8ddb9124d182a13540b18',1,'ItemPrototype.h']]],
  ['max_5fitem_5fsubclass_5fpermanent',['MAX_ITEM_SUBCLASS_PERMANENT',['../_item_prototype_8h.html#a58952d4634dfbca83aa78ec2764171be',1,'ItemPrototype.h']]],
  ['max_5fitem_5fsubclass_5fprojectile',['MAX_ITEM_SUBCLASS_PROJECTILE',['../_item_prototype_8h.html#aca9ae27814ce8e3f13e95b874c2c417f',1,'ItemPrototype.h']]],
  ['max_5fitem_5fsubclass_5fquest',['MAX_ITEM_SUBCLASS_QUEST',['../_item_prototype_8h.html#aceeff4283fbf0bce6e08e26b9ca550da',1,'ItemPrototype.h']]],
  ['max_5fitem_5fsubclass_5fquiver',['MAX_ITEM_SUBCLASS_QUIVER',['../_item_prototype_8h.html#ac1824506a4d571defbbb887eee5509ed',1,'ItemPrototype.h']]],
  ['max_5fitem_5fsubclass_5freagent',['MAX_ITEM_SUBCLASS_REAGENT',['../_item_prototype_8h.html#a51974df9fd71441575adbbd88b48da7c',1,'ItemPrototype.h']]],
  ['max_5fitem_5fsubclass_5frecipe',['MAX_ITEM_SUBCLASS_RECIPE',['../_item_prototype_8h.html#a14da81c054591b85fba0def5ee738316',1,'ItemPrototype.h']]],
  ['max_5fitem_5fsubclass_5freserved_5f1',['MAX_ITEM_SUBCLASS_RESERVED_1',['../_item_prototype_8h.html#a35875f52aa366dc68ecb7df58aa00389',1,'ItemPrototype.h']]],
  ['max_5fitem_5fsubclass_5ftrade_5fgoods',['MAX_ITEM_SUBCLASS_TRADE_GOODS',['../_item_prototype_8h.html#a6599b9d2042cfc2499df1e3163a676e3',1,'ItemPrototype.h']]],
  ['max_5fitem_5fsubclass_5fweapon',['MAX_ITEM_SUBCLASS_WEAPON',['../_item_prototype_8h.html#ad4143b84df046037bfee860169e703a7',1,'ItemPrototype.h']]],
  ['max_5fkill_5fcredit',['MAX_KILL_CREDIT',['../_creature_8h.html#a3f331d9e202385f679104cc20ef74c3d',1,'Creature.h']]],
  ['max_5flevel',['MAX_LEVEL',['../_d_b_c_enums_8h.html#a5bb4257ca9fa4bfcf9391b7895b97761',1,'DBCEnums.h']]],
  ['max_5flocale',['MAX_LOCALE',['../_common_8h.html#a61175fd946c693856e1ae4c6ebdcbbe9',1,'Common.h']]],
  ['max_5flock_5fcase',['MAX_LOCK_CASE',['../_d_b_c_structure_8h.html#a1ebc07de85011e32e1cb7a2f1637f092',1,'DBCStructure.h']]],
  ['max_5floot_5fobject_5fcount',['MAX_LOOT_OBJECT_COUNT',['../_loot_object_stack_8cpp.html#aa2365e0e3aa7bb187d301647cb50f781',1,'LootObjectStack.cpp']]],
  ['max_5fmangos_5fstring_5fid',['MAX_MANGOS_STRING_ID',['../_object_mgr_8h.html#a68c4a1fda36d4d93e84487f29606cf64',1,'ObjectMgr.h']]],
  ['max_5fmechanic',['MAX_MECHANIC',['../_shared_defines_8h.html#a2cd92d4b2bdf11d7c71cb5677af23f31',1,'SharedDefines.h']]],
  ['max_5fmoney_5famount',['MAX_MONEY_AMOUNT',['../_player_8h.html#aabbccefded1f0620ac64736f1c8224f9',1,'Player.h']]],
  ['max_5fnr_5floot_5fitems',['MAX_NR_LOOT_ITEMS',['../_loot_mgr_8h.html#aab54e459c8a6d83039755f8b6c82a5e9',1,'LootMgr.h']]],
  ['max_5fnr_5fquest_5fitems',['MAX_NR_QUEST_ITEMS',['../_loot_mgr_8h.html#a25b596c39e8f5b0f7c7baf80a14027da',1,'LootMgr.h']]],
  ['max_5fnumber_5fof_5fcells',['MAX_NUMBER_OF_CELLS',['../_grid_defines_8h.html#a2c6994ea025f427f1485c1c7aaee01fe',1,'GridDefines.h']]],
  ['max_5fnumber_5fof_5fgrids',['MAX_NUMBER_OF_GRIDS',['../_grid_defines_8h.html#a857252154f120575d940ffbdf63bace6',1,'GridDefines.h']]],
  ['max_5foutfit_5fitems',['MAX_OUTFIT_ITEMS',['../_d_b_c_structure_8h.html#ade532186d3fab018819b9718af66b6f8',1,'DBCStructure.h']]],
  ['max_5fpath_5flength',['MAX_PATH_LENGTH',['../_path_finder_8h.html#a9eb6992d76f02128388ae95c0415604a',1,'PathFinder.h']]],
  ['max_5fpet_5fdiet',['MAX_PET_DIET',['../_shared_defines_8h.html#a424f0b4e4e15f4bbc962a18ed30895db',1,'SharedDefines.h']]],
  ['max_5fpet_5fname',['MAX_PET_NAME',['../_object_mgr_8h.html#af2343fbe6e619aea69105837dc9d7a7f',1,'ObjectMgr.h']]],
  ['max_5fpet_5fstables',['MAX_PET_STABLES',['../_pet_8h.html#a4a834992e2dfccc5010094500092dfbe',1,'Pet.h']]],
  ['max_5fphase',['MAX_PHASE',['../_creature_event_a_i_8h.html#a15f996636bf968608c9c6214ffb050ba',1,'CreatureEventAI.h']]],
  ['max_5fplayed_5ftime_5findex',['MAX_PLAYED_TIME_INDEX',['../_player_8h.html#a95dadd52b33da56e706760fd70e83681',1,'Player.h']]],
  ['max_5fplayer_5flog_5fentities',['MAX_PLAYER_LOG_ENTITIES',['../_player_logger_8h.html#a873b959e5344dafcd960931f5e8431a1',1,'PlayerLogger.h']]],
  ['max_5fplayer_5fname',['MAX_PLAYER_NAME',['../_object_mgr_8h.html#a17e4584e6f22ba5f1b29f6c6db21fdfb',1,'ObjectMgr.h']]],
  ['max_5fplayer_5fsummon_5fdelay',['MAX_PLAYER_SUMMON_DELAY',['../_player_8h.html#ad061a6c6874af01a0dba9f5fe5ad9909',1,'Player.h']]],
  ['max_5fpoint_5fpath_5flength',['MAX_POINT_PATH_LENGTH',['../_path_finder_8h.html#ac4c3ae34e2d3733cc39fc1c207d4b814',1,'PathFinder.h']]],
  ['max_5fpowers',['MAX_POWERS',['../_shared_defines_8h.html#a78fcc691229c7bc57dff4fcb8d744f1b',1,'SharedDefines.h']]],
  ['max_5fquery_5flen',['MAX_QUERY_LEN',['../_database_8h.html#a5799c8932ad35c57edbd997ef75f8fce',1,'Database.h']]],
  ['max_5fquest_5flog_5fsize',['MAX_QUEST_LOG_SIZE',['../_quest_def_8h.html#a95d52fc66ae096a6bcac03b3cdccf3b8',1,'QuestDef.h']]],
  ['max_5fquest_5foffset',['MAX_QUEST_OFFSET',['../_player_8h.html#ab5c23006e50d6e278b50ca5a57e478d6',1,'Player.h']]],
  ['max_5fquiet_5fdistance',['MAX_QUIET_DISTANCE',['../_fleeing_movement_generator_8cpp.html#a9738c927d0a0b9da431824ab69a5988a',1,'FleeingMovementGenerator.cpp']]],
  ['max_5fraces',['MAX_RACES',['../_shared_defines_8h.html#a4f1557efbaca04e932287ce09a5b5206',1,'SharedDefines.h']]],
  ['max_5fraid_5fsize',['MAX_RAID_SIZE',['../_group_8h.html#a768d942fbb5e677e72365dd37c94551c',1,'Group.h']]],
  ['max_5fraid_5fsubgroups',['MAX_RAID_SUBGROUPS',['../_group_8h.html#afb58b37bfbd0e4d75c26cc40d4066a43',1,'Group.h']]],
  ['max_5freputation_5frank',['MAX_REPUTATION_RANK',['../_shared_defines_8h.html#a1a7eedbd325882888a3de6c19d38261a',1,'SharedDefines.h']]],
  ['max_5freset_5fevent_5ftype',['MAX_RESET_EVENT_TYPE',['../_map_persistent_state_mgr_8h.html#a0879886737bd6bd809130fca129cfeaf',1,'MapPersistentStateMgr.h']]],
  ['max_5fscript_5fflag_5fvalid',['MAX_SCRIPT_FLAG_VALID',['../_script_mgr_8h.html#a850eff72fb2f7a5f9cda0d5503228e0e',1,'ScriptMgr.h']]],
  ['max_5fsheathetype',['MAX_SHEATHETYPE',['../_shared_defines_8h.html#a5b0201c2562467d1f4b39abdd19b74e2',1,'SharedDefines.h']]],
  ['max_5fskill_5ftype',['MAX_SKILL_TYPE',['../_shared_defines_8h.html#a97abb2e37f990652334199396083aec1',1,'SharedDefines.h']]],
  ['max_5fspell_5fimmunity',['MAX_SPELL_IMMUNITY',['../_shared_defines_8h.html#adfaf18991f9a0e9422c4a1fbc0136679',1,'SharedDefines.h']]],
  ['max_5fspell_5freagents',['MAX_SPELL_REAGENTS',['../_d_b_c_structure_8h.html#a7dfd9b5e0adb5c5e3ebf3173f80fbcce',1,'DBCStructure.h']]],
  ['max_5fspell_5fschool',['MAX_SPELL_SCHOOL',['../_shared_defines_8h.html#ad42abd20ed172c2dfe884c635b234189',1,'SharedDefines.h']]],
  ['max_5fspell_5ftarget_5ftype',['MAX_SPELL_TARGET_TYPE',['../_spell_mgr_8h.html#aac877d83d1d2b22d02c52c695a5c6402',1,'SpellMgr.h']]],
  ['max_5fspell_5ftotems',['MAX_SPELL_TOTEMS',['../_d_b_c_structure_8h.html#aecf32f24cad55cf1076d863d70247894',1,'DBCStructure.h']]],
  ['max_5fspillover_5ffactions',['MAX_SPILLOVER_FACTIONS',['../_shared_defines_8h.html#afac307c17fe23879738ebb77d43ad4b7',1,'SharedDefines.h']]],
  ['max_5fstack_5fsize',['MAX_STACK_SIZE',['../_b_i_h_8h.html#accbb358028675c83675d8b34c386268d',1,'BIH.h']]],
  ['max_5fstats',['MAX_STATS',['../_shared_defines_8h.html#a0f8923f02eb7dfba43a426b8320354da',1,'SharedDefines.h']]],
  ['max_5fstealth_5fdetect_5frange',['MAX_STEALTH_DETECT_RANGE',['../_object_8h.html#af4d27c5076e6628eb126529284187868',1,'Object.h']]],
  ['max_5ftalent_5frank',['MAX_TALENT_RANK',['../_d_b_c_structure_8h.html#a8d6d5cac52d7a437e341e9f6ebc21a21',1,'DBCStructure.h']]],
  ['max_5ftext_5fid',['MAX_TEXT_ID',['../_script_mgr_8h.html#aecdb965589928a32a0a17790cc8872d8',1,'ScriptMgr.h']]],
  ['max_5ftimers',['MAX_TIMERS',['../_player_8h.html#a79fd3eac646964332ed33ec7dd40f208',1,'Player.h']]],
  ['max_5ftotem_5fslot',['MAX_TOTEM_SLOT',['../_shared_defines_8h.html#aa1b5c5e153101987fcbf9000907e2a81',1,'SharedDefines.h']]],
  ['max_5ftrainer_5ftype',['MAX_TRAINER_TYPE',['../_shared_defines_8h.html#ae4c3ac2deaad5a75f25ea1fc34dc3e0f',1,'SharedDefines.h']]],
  ['max_5ftype_5fid',['MAX_TYPE_ID',['../_object_guid_8h.html#a3744f20bdae8eedb2bb8d50f423ddec4',1,'ObjectGuid.h']]],
  ['max_5fvendor_5fitems',['MAX_VENDOR_ITEMS',['../_creature_8h.html#a44c3a813b92331e325ba5aae11ed72a3',1,'Creature.h']]],
  ['max_5fvirtual_5fitem_5fslot',['MAX_VIRTUAL_ITEM_SLOT',['../_creature_8h.html#a8f7d803e051a8bcb43a44f16b01f9c09',1,'Creature.h']]],
  ['max_5fvisibility_5fdistance',['MAX_VISIBILITY_DISTANCE',['../_object_8h.html#a195874db808f155d535260d5e4080059',1,'Object.h']]],
  ['max_5fvisible_5fitem_5foffset',['MAX_VISIBLE_ITEM_OFFSET',['../_item_8h.html#a918ebc309f37f0d89c91e17b371bf9ca',1,'Item.h']]],
  ['max_5fwaypoint_5ftext',['MAX_WAYPOINT_TEXT',['../_waypoint_manager_8h.html#ae3befe591071285b3477f7eb7cf53e4e',1,'WaypointManager.h']]],
  ['max_5fweather_5ftype',['MAX_WEATHER_TYPE',['../_shared_defines_8h.html#a998b287aec78d72633142b2331baf958',1,'SharedDefines.h']]],
  ['maxprioritynum',['MAXPRIORITYNUM',['../_threading_8h.html#adaa2d7355dffd6e7266818c98c6015a5',1,'Threading.h']]],
  ['mechanic_5fnot_5fremoved_5fby_5fshapeshift',['MECHANIC_NOT_REMOVED_BY_SHAPESHIFT',['../_shared_defines_8h.html#ae85cc8bf7167bf1fc4fc1b74d9624103',1,'SharedDefines.h']]],
  ['melee_5fbased_5ftrigger_5fmask',['MELEE_BASED_TRIGGER_MASK',['../_spell_mgr_8h.html#a324bcfedb9d45ed36bd7457067482db8',1,'SpellMgr.h']]],
  ['min_5fconnection_5fpool_5fsize',['MIN_CONNECTION_POOL_SIZE',['../_database_8cpp.html#a57de71aba0b5311388b5529131f858b9',1,'Database.cpp']]],
  ['min_5fcreature_5fai_5ftext_5fstring_5fid',['MIN_CREATURE_AI_TEXT_STRING_ID',['../_object_mgr_8h.html#ad2b46edf67e4634f0dc735cb4f3e8ce2',1,'ObjectMgr.h']]],
  ['min_5fdb_5fscript_5fstring_5fid',['MIN_DB_SCRIPT_STRING_ID',['../_object_mgr_8h.html#a5e40569259bd7c80cd4081eae4805549',1,'ObjectMgr.h']]],
  ['min_5fgrid_5fdelay',['MIN_GRID_DELAY',['../_grid_defines_8h.html#a204f91e3181b15c48673d54f4900f46c',1,'GridDefines.h']]],
  ['min_5fmangos_5fstring_5fid',['MIN_MANGOS_STRING_ID',['../_object_mgr_8h.html#a39828a8fbd783f1df15d881a191cb8fc',1,'ObjectMgr.h']]],
  ['min_5fmap_5fupdate_5fdelay',['MIN_MAP_UPDATE_DELAY',['../_grid_defines_8h.html#ae372d35aed08096c29f83d1f27eacf8c',1,'GridDefines.h']]],
  ['min_5fquiet_5fdistance',['MIN_QUIET_DISTANCE',['../_fleeing_movement_generator_8cpp.html#ad1ba65a06a9ac4ab2293efde8be35403',1,'FleeingMovementGenerator.cpp']]],
  ['min_5freputation_5frank',['MIN_REPUTATION_RANK',['../_shared_defines_8h.html#a37ec5c9991b7291bce7ffa92f82534e1',1,'SharedDefines.h']]],
  ['min_5funload_5fdelay',['MIN_UNLOAD_DELAY',['../_map_8h.html#a092c3d2c90fd46d14455e6ff2413dabb',1,'Map.h']]],
  ['mmap_5fmagic',['MMAP_MAGIC',['../_move_map_shared_defines_8h.html#af7a9a5e840ab3ad1c160e832500016d1',1,'MoveMapSharedDefines.h']]],
  ['mmap_5fversion',['MMAP_VERSION',['../_move_map_shared_defines_8h.html#aea71555fdd0af33ccc21c76c67574421',1,'MoveMapSharedDefines.h']]],
  ['movement_5fpacket_5ftime_5fdelay',['MOVEMENT_PACKET_TIME_DELAY',['../_movement_handler_8cpp.html#a69e254ae3a119a96053add6b9732296c',1,'MovementHandler.cpp']]],
  ['movement_5frandom_5fmmgen_5fchance_5fno_5fbreak',['MOVEMENT_RANDOM_MMGEN_CHANCE_NO_BREAK',['../_random_movement_generator_8h.html#adb0f58e92358bb511abfd586727923ea',1,'RandomMovementGenerator.h']]]
];
