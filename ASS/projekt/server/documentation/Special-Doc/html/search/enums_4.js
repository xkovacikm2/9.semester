var searchData=
[
  ['damageeffecttype',['DamageEffectType',['../_shared_defines_8h.html#aff59f402d05917ab1de0c5f6bf148ea4',1,'SharedDefines.h']]],
  ['damagetypetoschool',['DamageTypeToSchool',['../group__game.html#ga157a4e1cc0a8e5f03988c03cf4a0660d',1,'Unit.h']]],
  ['databasetypes',['DatabaseTypes',['../_database_8h.html#a55952e64a3ebe86dc0aba267b22fa8fa',1,'Database.h']]],
  ['dbscriptcommand',['DBScriptCommand',['../_script_mgr_8h.html#aada2386d0abc89492b5b091bff832bb2',1,'ScriptMgr.h']]],
  ['dbscripttype',['DBScriptType',['../_script_mgr_8h.html#a070a8cfe5add0f89ed18674bbb4ca286',1,'ScriptMgr.h']]],
  ['deathstate',['DeathState',['../group__game.html#ga671007e5c9d47b3ccb212f971e65baed',1,'Unit.h']]],
  ['denyreason',['DenyReason',['../_playerbot_security_8h.html#a3a5640e33ba384031babfbc84ef05cfb',1,'PlayerbotSecurity.h']]],
  ['diminishinggroup',['DiminishingGroup',['../_shared_defines_8h.html#a1d74b2169d9a060490d81975a3064f50',1,'SharedDefines.h']]],
  ['diminishinglevels',['DiminishingLevels',['../group__game.html#gaa8baff9cbdf61bbb483c1e7ae739f930',1,'Unit.h']]],
  ['diminishingreturnstype',['DiminishingReturnsType',['../_shared_defines_8h.html#acc1ff89f53e83d6c8146a6989a5cba5b',1,'SharedDefines.h']]],
  ['disabletype',['DisableType',['../_disable_mgr_8h.html#affa2a0d7ee61f6e1bc7c6cb7d2c1b64c',1,'DisableMgr.h']]],
  ['disabletypes',['DisableTypes',['../namespace_v_m_a_p.html#ae6fa86a61cff3bac928739b18ffb8c1b',1,'VMAP']]],
  ['dispeltype',['DispelType',['../_shared_defines_8h.html#ada2b5e66b650d7a63f70c06a746070d1',1,'SharedDefines.h']]],
  ['drunkenstate',['DrunkenState',['../_player_8h.html#adc8fb9638bd9c8f972163d22b9d870ed',1,'Player.h']]],
  ['duelcompletetype',['DuelCompleteType',['../_player_8h.html#a7847fd3865d67c4dbaa18b6110b7c395',1,'Player.h']]],
  ['dumpreturn',['DumpReturn',['../_player_dump_8h.html#a4471667403db9bc7532f371d28743866',1,'PlayerDump.h']]],
  ['dumptabletype',['DumpTableType',['../_player_dump_8h.html#ad4739d865080bb5590831b606365e7ef',1,'PlayerDump.h']]],
  ['dynamicobjecttype',['DynamicObjectType',['../_dynamic_object_8h.html#aee85c2f840289bcf773609333ee91131',1,'DynamicObject.h']]]
];
