var indexSectionsWithContent =
{
  0: "$_abcdefghijklmnopqrstuvwxyz~",
  1: "_abcdefghijklmnopqrstuvw",
  2: "abcdfgmvw",
  3: "abcdefghiklmnopqrstuvw",
  4: "_abcdefghijklmnopqrstuvwxyz~",
  5: "$_abcdefghijklmnopqrstuvwxyz",
  6: "abcdefghijlmnopqrstuvw",
  7: "_abcdefghiklmnopqrstuvwx",
  8: "abcdefghijklmnopqrstuvwyz",
  9: "abcdghilmoprstuvw",
  10: "_abcdefghilmnopqrstuvwxz",
  11: "dgmrstu",
  12: "acdeghimrst"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "related",
  10: "defines",
  11: "groups",
  12: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Friends",
  10: "Macros",
  11: "Modules",
  12: "Pages"
};

