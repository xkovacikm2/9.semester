var class_ref_manager =
[
    [ "iterator", "class_ref_manager.html#a1e38bf1c98ee6ec22ef4f7a8727f270f", null ],
    [ "RefManager", "class_ref_manager.html#a9d96a6c6121468b3a1777c7862217fe1", null ],
    [ "~RefManager", "class_ref_manager.html#abde1c4b11b940d480067af353786815e", null ],
    [ "begin", "class_ref_manager.html#a5ef23cb18eb8decf80f852d7a8f2f144", null ],
    [ "clearReferences", "class_ref_manager.html#a8f78004ac50323889a117ff98d48105e", null ],
    [ "end", "class_ref_manager.html#a0cca597b5efce2d60112fbba4d66df9a", null ],
    [ "getFirst", "class_ref_manager.html#a3d30413852279e56375a168fde80fb8d", null ],
    [ "getFirst", "class_ref_manager.html#a7127927b327617664fd2303952436263", null ],
    [ "getLast", "class_ref_manager.html#ab2601c0224127ef91363ebf24010720a", null ],
    [ "getLast", "class_ref_manager.html#a45f65d2fc1b615176a3f880695ed8b0c", null ],
    [ "rbegin", "class_ref_manager.html#a3bb57997710144e12e682944cb08c94e", null ],
    [ "rend", "class_ref_manager.html#afda867b782bf1cbae976e5511fb951ea", null ]
];