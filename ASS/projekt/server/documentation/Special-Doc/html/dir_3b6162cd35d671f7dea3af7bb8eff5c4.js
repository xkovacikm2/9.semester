var dir_3b6162cd35d671f7dea3af7bb8eff5c4 =
[
    [ "AggressorAI.cpp", "_aggressor_a_i_8cpp.html", null ],
    [ "AggressorAI.h", "_aggressor_a_i_8h.html", [
      [ "AggressorAI", "class_aggressor_a_i.html", "class_aggressor_a_i" ]
    ] ],
    [ "AuctionHouseMgr.cpp", "_auction_house_mgr_8cpp.html", "_auction_house_mgr_8cpp" ],
    [ "AuctionHouseMgr.h", "_auction_house_mgr_8h.html", "_auction_house_mgr_8h" ],
    [ "Bag.cpp", "_bag_8cpp.html", null ],
    [ "Bag.h", "_bag_8h.html", "_bag_8h" ],
    [ "Camera.cpp", "_camera_8cpp.html", null ],
    [ "Camera.h", "_camera_8h.html", [
      [ "Camera", "class_camera.html", "class_camera" ],
      [ "ViewPoint", "class_view_point.html", "class_view_point" ]
    ] ],
    [ "Corpse.cpp", "_corpse_8cpp.html", null ],
    [ "Corpse.h", "_corpse_8h.html", "_corpse_8h" ],
    [ "Creature.cpp", "_creature_8cpp.html", [
      [ "CreatureRespawnDeleteWorker", "struct_creature_respawn_delete_worker.html", "struct_creature_respawn_delete_worker" ],
      [ "AddCreatureToRemoveListInMapsWorker", "struct_add_creature_to_remove_list_in_maps_worker.html", "struct_add_creature_to_remove_list_in_maps_worker" ],
      [ "SpawnCreatureInMapsWorker", "struct_spawn_creature_in_maps_worker.html", "struct_spawn_creature_in_maps_worker" ]
    ] ],
    [ "Creature.h", "_creature_8h.html", "_creature_8h" ],
    [ "CreatureAI.cpp", "_creature_a_i_8cpp.html", [
      [ "AiDelayEventAround", "class_ai_delay_event_around.html", "class_ai_delay_event_around" ]
    ] ],
    [ "CreatureAI.h", "_creature_a_i_8h.html", "_creature_a_i_8h" ],
    [ "CreatureAIImpl.h", "_creature_a_i_impl_8h.html", null ],
    [ "CreatureAIRegistry.cpp", "_creature_a_i_registry_8cpp.html", "_creature_a_i_registry_8cpp" ],
    [ "CreatureAIRegistry.h", "_creature_a_i_registry_8h.html", "_creature_a_i_registry_8h" ],
    [ "CreatureAISelector.cpp", "_creature_a_i_selector_8cpp.html", "_creature_a_i_selector_8cpp" ],
    [ "CreatureAISelector.h", "_creature_a_i_selector_8h.html", "_creature_a_i_selector_8h" ],
    [ "CreatureEventAI.cpp", "_creature_event_a_i_8cpp.html", "_creature_event_a_i_8cpp" ],
    [ "CreatureEventAI.h", "_creature_event_a_i_8h.html", "_creature_event_a_i_8h" ],
    [ "CreatureEventAIMgr.cpp", "_creature_event_a_i_mgr_8cpp.html", "_creature_event_a_i_mgr_8cpp" ],
    [ "CreatureEventAIMgr.h", "_creature_event_a_i_mgr_8h.html", "_creature_event_a_i_mgr_8h" ],
    [ "DynamicObject.cpp", "_dynamic_object_8cpp.html", null ],
    [ "DynamicObject.h", "_dynamic_object_8h.html", "_dynamic_object_8h" ],
    [ "Formulas.h", "_formulas_8h.html", "_formulas_8h" ],
    [ "GameObject.cpp", "_game_object_8cpp.html", [
      [ "GameObjectRespawnDeleteWorker", "struct_game_object_respawn_delete_worker.html", "struct_game_object_respawn_delete_worker" ],
      [ "AddGameObjectToRemoveListInMapsWorker", "struct_add_game_object_to_remove_list_in_maps_worker.html", "struct_add_game_object_to_remove_list_in_maps_worker" ],
      [ "SpawnGameObjectInMapsWorker", "struct_spawn_game_object_in_maps_worker.html", "struct_spawn_game_object_in_maps_worker" ]
    ] ],
    [ "GameObject.h", "_game_object_8h.html", "_game_object_8h" ],
    [ "GMTicketMgr.cpp", "_g_m_ticket_mgr_8cpp.html", "_g_m_ticket_mgr_8cpp" ],
    [ "GMTicketMgr.h", "_g_m_ticket_mgr_8h.html", "_g_m_ticket_mgr_8h" ],
    [ "GuardAI.cpp", "_guard_a_i_8cpp.html", null ],
    [ "GuardAI.h", "_guard_a_i_8h.html", [
      [ "GuardAI", "class_guard_a_i.html", "class_guard_a_i" ]
    ] ],
    [ "Guild.cpp", "_guild_8cpp.html", null ],
    [ "Guild.h", "_guild_8h.html", "_guild_8h" ],
    [ "Item.cpp", "_item_8cpp.html", "_item_8cpp" ],
    [ "Item.h", "_item_8h.html", "_item_8h" ],
    [ "ItemEnchantmentMgr.cpp", "_item_enchantment_mgr_8cpp.html", "_item_enchantment_mgr_8cpp" ],
    [ "ItemEnchantmentMgr.h", "_item_enchantment_mgr_8h.html", "_item_enchantment_mgr_8h" ],
    [ "ItemPrototype.h", "_item_prototype_8h.html", "_item_prototype_8h" ],
    [ "LootMgr.cpp", "_loot_mgr_8cpp.html", "_loot_mgr_8cpp" ],
    [ "LootMgr.h", "_loot_mgr_8h.html", "_loot_mgr_8h" ],
    [ "NullCreatureAI.cpp", "_null_creature_a_i_8cpp.html", null ],
    [ "NullCreatureAI.h", "_null_creature_a_i_8h.html", [
      [ "NullCreatureAI", "class_null_creature_a_i.html", "class_null_creature_a_i" ]
    ] ],
    [ "Object.cpp", "_object_8cpp.html", "_object_8cpp" ],
    [ "Object.h", "_object_8h.html", "_object_8h" ],
    [ "ObjectAccessor.cpp", "_object_accessor_8cpp.html", "_object_accessor_8cpp" ],
    [ "ObjectAccessor.h", "_object_accessor_8h.html", "_object_accessor_8h" ],
    [ "ObjectGuid.cpp", "_object_guid_8cpp.html", "_object_guid_8cpp" ],
    [ "ObjectGuid.h", "_object_guid_8h.html", "_object_guid_8h" ],
    [ "ObjectMgr.cpp", "_object_mgr_8cpp.html", "_object_mgr_8cpp" ],
    [ "ObjectMgr.h", "_object_mgr_8h.html", "_object_mgr_8h" ],
    [ "ObjectPosSelector.cpp", "_object_pos_selector_8cpp.html", "_object_pos_selector_8cpp" ],
    [ "ObjectPosSelector.h", "_object_pos_selector_8h.html", "_object_pos_selector_8h" ],
    [ "Pet.cpp", "_pet_8cpp.html", "_pet_8cpp" ],
    [ "Pet.h", "_pet_8h.html", "_pet_8h" ],
    [ "PetAI.cpp", "_pet_a_i_8cpp.html", null ],
    [ "PetAI.h", "_pet_a_i_8h.html", [
      [ "PetAI", "class_pet_a_i.html", "class_pet_a_i" ]
    ] ],
    [ "Player.cpp", "_player_8cpp.html", "_player_8cpp" ],
    [ "Player.h", "_player_8h.html", "_player_8h" ],
    [ "PlayerLogger.cpp", "_player_logger_8cpp.html", null ],
    [ "PlayerLogger.h", "_player_logger_8h.html", "_player_logger_8h" ],
    [ "ReactorAI.cpp", "_reactor_a_i_8cpp.html", "_reactor_a_i_8cpp" ],
    [ "ReactorAI.h", "_reactor_a_i_8h.html", [
      [ "ReactorAI", "class_reactor_a_i.html", "class_reactor_a_i" ]
    ] ],
    [ "ReputationMgr.cpp", "_reputation_mgr_8cpp.html", [
      [ "rep", "structrep.html", "structrep" ]
    ] ],
    [ "ReputationMgr.h", "_reputation_mgr_8h.html", "_reputation_mgr_8h" ],
    [ "SocialMgr.cpp", "_social_mgr_8cpp.html", "_social_mgr_8cpp" ],
    [ "SocialMgr.h", "_social_mgr_8h.html", "_social_mgr_8h" ],
    [ "SpellMgr.cpp", "_spell_mgr_8cpp.html", "_spell_mgr_8cpp" ],
    [ "SpellMgr.h", "_spell_mgr_8h.html", "_spell_mgr_8h" ],
    [ "StatSystem.cpp", "_stat_system_8cpp.html", null ],
    [ "TemporarySummon.cpp", "_temporary_summon_8cpp.html", null ],
    [ "TemporarySummon.h", "_temporary_summon_8h.html", [
      [ "TemporarySummon", "class_temporary_summon.html", "class_temporary_summon" ],
      [ "TemporarySummonWaypoint", "class_temporary_summon_waypoint.html", "class_temporary_summon_waypoint" ]
    ] ],
    [ "Totem.cpp", "_totem_8cpp.html", null ],
    [ "Totem.h", "_totem_8h.html", "_totem_8h" ],
    [ "TotemAI.cpp", "_totem_a_i_8cpp.html", null ],
    [ "TotemAI.h", "_totem_a_i_8h.html", [
      [ "TotemAI", "class_totem_a_i.html", "class_totem_a_i" ]
    ] ],
    [ "Unit.cpp", "_unit_8cpp.html", "_unit_8cpp" ],
    [ "Unit.h", "_unit_8h.html", "_unit_8h" ],
    [ "UnitEvents.h", "_unit_events_8h.html", "_unit_events_8h" ],
    [ "UpdateFields.h", "_update_fields_8h.html", "_update_fields_8h" ],
    [ "UpdateMask.h", "_update_mask_8h.html", [
      [ "UpdateMask", "class_update_mask.html", "class_update_mask" ]
    ] ]
];