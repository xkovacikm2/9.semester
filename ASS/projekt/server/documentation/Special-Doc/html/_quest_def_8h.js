var _quest_def_8h =
[
    [ "QuestLocale", "struct_quest_locale.html", "struct_quest_locale" ],
    [ "Quest", "class_quest.html", "class_quest" ],
    [ "QuestStatusData", "struct_quest_status_data.html", "struct_quest_status_data" ],
    [ "MAX_QUEST_LOG_SIZE", "_quest_def_8h.html#a95d52fc66ae096a6bcac03b3cdccf3b8", null ],
    [ "QUEST_DEPLINK_COUNT", "_quest_def_8h.html#a1ad18081efbed90eca30ddba880ef519", null ],
    [ "QUEST_EMOTE_COUNT", "_quest_def_8h.html#a63297dd973bbf1f493882c44e7b32d75", null ],
    [ "QUEST_ITEM_OBJECTIVES_COUNT", "_quest_def_8h.html#a5b538720bd7de8c4468dc6d21b412f10", null ],
    [ "QUEST_OBJECTIVES_COUNT", "_quest_def_8h.html#a7dab6182ee1bbabf205ed30f9c2d6c8b", null ],
    [ "QUEST_REPUTATIONS_COUNT", "_quest_def_8h.html#aca7218f1a80ba33fb21a51073850202e", null ],
    [ "QUEST_REWARD_CHOICES_COUNT", "_quest_def_8h.html#ad2f1fa1fe4676a225f4c3319310907aa", null ],
    [ "QUEST_REWARDS_COUNT", "_quest_def_8h.html#a15eafb369f3711f92bd7ec296be6af15", null ],
    [ "QUEST_SOURCE_ITEM_IDS_COUNT", "_quest_def_8h.html#acf2c83159c7b7d9270380dafa7ab8d11", null ],
    [ "QUEST_SPECIAL_FLAG_DB_ALLOWED", "_quest_def_8h.html#a3c843969886ed19db29a6299b556b5c2", null ],
    [ "__QuestGiverStatus", "_quest_def_8h.html#a4f51c29ab964345e968b587a4b479faf", [
      [ "DIALOG_STATUS_NONE", "_quest_def_8h.html#a4f51c29ab964345e968b587a4b479fafa26affdf7d4a232a13a2e85bcfec3acac", null ],
      [ "DIALOG_STATUS_UNAVAILABLE", "_quest_def_8h.html#a4f51c29ab964345e968b587a4b479fafa23c6b78237061dac38ddfb39d974a3db", null ],
      [ "DIALOG_STATUS_CHAT", "_quest_def_8h.html#a4f51c29ab964345e968b587a4b479fafab7db1111d41f4e72f4c302f5d3773f59", null ],
      [ "DIALOG_STATUS_INCOMPLETE", "_quest_def_8h.html#a4f51c29ab964345e968b587a4b479fafaa4ccf7e90f00237c4edb4de98162be3a", null ],
      [ "DIALOG_STATUS_REWARD_REP", "_quest_def_8h.html#a4f51c29ab964345e968b587a4b479fafa8c007c4cb4d3909e264b15da0c678415", null ],
      [ "DIALOG_STATUS_AVAILABLE", "_quest_def_8h.html#a4f51c29ab964345e968b587a4b479fafa332bbf12a9f624eaf8304facf64520c2", null ],
      [ "DIALOG_STATUS_REWARD_OLD", "_quest_def_8h.html#a4f51c29ab964345e968b587a4b479fafaf33dfa588eeac6bb7c73f35d9250cf84", null ],
      [ "DIALOG_STATUS_REWARD2", "_quest_def_8h.html#a4f51c29ab964345e968b587a4b479fafa90d7515f30165efde72eca296e6506f6", null ],
      [ "DIALOG_STATUS_UNDEFINED", "_quest_def_8h.html#a4f51c29ab964345e968b587a4b479fafa804031523c2291f13f4477de490e7d6a", null ]
    ] ],
    [ "__QuestTradeSkill", "_quest_def_8h.html#ae2addccc323853a195d104f28292dc7e", [
      [ "QUEST_TRSKILL_NONE", "_quest_def_8h.html#ae2addccc323853a195d104f28292dc7ea93bf045ffdaf60a1fead61925f7ffe21", null ],
      [ "QUEST_TRSKILL_ALCHEMY", "_quest_def_8h.html#ae2addccc323853a195d104f28292dc7ea1126b552f6edd5b83b5f75677f17a187", null ],
      [ "QUEST_TRSKILL_BLACKSMITHING", "_quest_def_8h.html#ae2addccc323853a195d104f28292dc7ea519915a9d5405dbdf18effc84a0b3218", null ],
      [ "QUEST_TRSKILL_COOKING", "_quest_def_8h.html#ae2addccc323853a195d104f28292dc7ea8bdee5caba9821147650a73e33093c76", null ],
      [ "QUEST_TRSKILL_ENCHANTING", "_quest_def_8h.html#ae2addccc323853a195d104f28292dc7eadb3dfef23e8aff7d9e91e0d6ddc6b7d1", null ],
      [ "QUEST_TRSKILL_ENGINEERING", "_quest_def_8h.html#ae2addccc323853a195d104f28292dc7eaab43f4c6841032578ec09ee87ff279bf", null ],
      [ "QUEST_TRSKILL_FIRSTAID", "_quest_def_8h.html#ae2addccc323853a195d104f28292dc7eac2c8698c4a2f6fc698323b700ce7c1f4", null ],
      [ "QUEST_TRSKILL_HERBALISM", "_quest_def_8h.html#ae2addccc323853a195d104f28292dc7ea64623fa376a4cd5a26f6105b67f1e00c", null ],
      [ "QUEST_TRSKILL_LEATHERWORKING", "_quest_def_8h.html#ae2addccc323853a195d104f28292dc7eaf8423750a58d8e63bfb7ea95c6919aed", null ],
      [ "QUEST_TRSKILL_POISONS", "_quest_def_8h.html#ae2addccc323853a195d104f28292dc7eaffb007d66bf0c7f4c9329ccedd9ef888", null ],
      [ "QUEST_TRSKILL_TAILORING", "_quest_def_8h.html#ae2addccc323853a195d104f28292dc7ea576763d14de26f190cda1c78246d6b62", null ],
      [ "QUEST_TRSKILL_MINING", "_quest_def_8h.html#ae2addccc323853a195d104f28292dc7ea63ec13a1563a0768de6069e080fef312", null ],
      [ "QUEST_TRSKILL_FISHING", "_quest_def_8h.html#ae2addccc323853a195d104f28292dc7ea167e3be6af4a952cfb25fd0a635028d3", null ],
      [ "QUEST_TRSKILL_SKINNING", "_quest_def_8h.html#ae2addccc323853a195d104f28292dc7ea7fd107acbf05a6bccb2a8472775a5b64", null ]
    ] ],
    [ "QuestActor", "_quest_def_8h.html#a6f30b853e6cfd1572f94384177b67c73", [
      [ "QA_CREATURE", "_quest_def_8h.html#a6f30b853e6cfd1572f94384177b67c73adcd6e03e873b4d2aaad250a0c89848e4", null ],
      [ "QA_GAMEOBJECT", "_quest_def_8h.html#a6f30b853e6cfd1572f94384177b67c73af2f51bcfefe0f23f8665f0db540bae53", null ],
      [ "QA_AREATRIGGER", "_quest_def_8h.html#a6f30b853e6cfd1572f94384177b67c73a9a7fbd9b90c12442cfc882e75d6e0cc3", null ]
    ] ],
    [ "QuestFailedReasons", "_quest_def_8h.html#a7798b44dc1474f5c1fccbf5c59c968cb", [
      [ "INVALIDREASON_DONT_HAVE_REQ", "_quest_def_8h.html#a7798b44dc1474f5c1fccbf5c59c968cba067d8fff87d25ae4640a460349211252", null ],
      [ "INVALIDREASON_QUEST_FAILED_LOW_LEVEL", "_quest_def_8h.html#a7798b44dc1474f5c1fccbf5c59c968cba8c7847cc142b47d740a77d44a9a359f2", null ],
      [ "INVALIDREASON_QUEST_FAILED_REQS", "_quest_def_8h.html#a7798b44dc1474f5c1fccbf5c59c968cba8a8defa504c2ee84420c07a59b848587", null ],
      [ "INVALIDREASON_QUEST_FAILED_INVENTORY_FULL", "_quest_def_8h.html#a7798b44dc1474f5c1fccbf5c59c968cbaa2adf9f7790fa3e4c4c2f72960d72b31", null ],
      [ "INVALIDREASON_QUEST_FAILED_WRONG_RACE", "_quest_def_8h.html#a7798b44dc1474f5c1fccbf5c59c968cba6ccfc01ce1d8978fd6ff245df456842d", null ],
      [ "INVALIDREASON_QUEST_ONLY_ONE_TIMED", "_quest_def_8h.html#a7798b44dc1474f5c1fccbf5c59c968cba89932eb633b107826e9dbd9a01b20d87", null ],
      [ "INVALIDREASON_QUEST_ALREADY_ON", "_quest_def_8h.html#a7798b44dc1474f5c1fccbf5c59c968cba43ddb2705d2b8b1dadf2129725eb8e67", null ],
      [ "INVALIDREASON_QUEST_FAILED_DUPLICATE_ITEM", "_quest_def_8h.html#a7798b44dc1474f5c1fccbf5c59c968cbad8e4bd4d1cfa8f06a5687d16c3baad1a", null ],
      [ "INVALIDREASON_QUEST_FAILED_MISSING_ITEMS", "_quest_def_8h.html#a7798b44dc1474f5c1fccbf5c59c968cba5452dd7f33073451f35b28cfa0f516a5", null ],
      [ "INVALIDREASON_QUEST_FAILED_NOT_ENOUGH_MONEY", "_quest_def_8h.html#a7798b44dc1474f5c1fccbf5c59c968cba89e5220155ad05d0a11e8a0f9d32d3cb", null ]
    ] ],
    [ "QuestFlags", "_quest_def_8h.html#a881ebe30fbf0f088d54bfa959cefe47c", [
      [ "QUEST_FLAGS_NONE", "_quest_def_8h.html#a881ebe30fbf0f088d54bfa959cefe47ca20e759d47e01a7f13e5cdd8a64e8ee37", null ],
      [ "QUEST_FLAGS_STAY_ALIVE", "_quest_def_8h.html#a881ebe30fbf0f088d54bfa959cefe47caacbbd50733b9d85ad468a4edf4ba5c42", null ],
      [ "QUEST_FLAGS_PARTY_ACCEPT", "_quest_def_8h.html#a881ebe30fbf0f088d54bfa959cefe47ca05e7e7ed2a08121777ed61d1f76b7015", null ],
      [ "QUEST_FLAGS_EXPLORATION", "_quest_def_8h.html#a881ebe30fbf0f088d54bfa959cefe47cac2ea06e97bd4594fce08e84b91606d57", null ],
      [ "QUEST_FLAGS_SHARABLE", "_quest_def_8h.html#a881ebe30fbf0f088d54bfa959cefe47caad03215b00fda5ef5b3a3b55d0181582", null ],
      [ "QUEST_FLAGS_EPIC", "_quest_def_8h.html#a881ebe30fbf0f088d54bfa959cefe47ca4a2f2f1c95e6916bf67427685a662891", null ],
      [ "QUEST_FLAGS_RAID", "_quest_def_8h.html#a881ebe30fbf0f088d54bfa959cefe47cae058bd85af103f8537a68f56d7f67b69", null ],
      [ "QUEST_FLAGS_UNK2", "_quest_def_8h.html#a881ebe30fbf0f088d54bfa959cefe47ca19c678a87beb501ccbb3e4d334ecc1ca", null ],
      [ "QUEST_FLAGS_HIDDEN_REWARDS", "_quest_def_8h.html#a881ebe30fbf0f088d54bfa959cefe47ca516bd7b6d09994b8591abdfc6ac0c449", null ],
      [ "QUEST_FLAGS_AUTO_REWARDED", "_quest_def_8h.html#a881ebe30fbf0f088d54bfa959cefe47ca3e580fb8d9cefa21be5e12ff636cea46", null ]
    ] ],
    [ "QuestRole", "_quest_def_8h.html#ada336a34b18a9cd49137fae520f28f1d", [
      [ "QR_START", "_quest_def_8h.html#ada336a34b18a9cd49137fae520f28f1daf870edf38c534799685d983408ef201c", null ],
      [ "QR_END", "_quest_def_8h.html#ada336a34b18a9cd49137fae520f28f1da6c3c0006aa009661e32fd857e652ce7a", null ]
    ] ],
    [ "QuestShareMessages", "_quest_def_8h.html#a119d3789e37252cfbcbf7e7c7d7489ba", [
      [ "QUEST_PARTY_MSG_SHARING_QUEST", "_quest_def_8h.html#a119d3789e37252cfbcbf7e7c7d7489baab6f861a23c26725e4c2f299e225937c4", null ],
      [ "QUEST_PARTY_MSG_CANT_TAKE_QUEST", "_quest_def_8h.html#a119d3789e37252cfbcbf7e7c7d7489baa27a5d14218bd45dfdc4dca776189da32", null ],
      [ "QUEST_PARTY_MSG_ACCEPT_QUEST", "_quest_def_8h.html#a119d3789e37252cfbcbf7e7c7d7489baa6260d7bfc0d07ea9b2e090b00c36bf0d", null ],
      [ "QUEST_PARTY_MSG_DECLINE_QUEST", "_quest_def_8h.html#a119d3789e37252cfbcbf7e7c7d7489baae2367e7a54baeef7f8b830a16c187ecf", null ],
      [ "QUEST_PARTY_MSG_TOO_FAR", "_quest_def_8h.html#a119d3789e37252cfbcbf7e7c7d7489baa9b57122b041d7c3ce4bc1bd7dc69392a", null ],
      [ "QUEST_PARTY_MSG_BUSY", "_quest_def_8h.html#a119d3789e37252cfbcbf7e7c7d7489baa457f8498d07f0f0c1af0989763318608", null ],
      [ "QUEST_PARTY_MSG_LOG_FULL", "_quest_def_8h.html#a119d3789e37252cfbcbf7e7c7d7489baacb52a2d28b26ab2a7b66e91d5708b09d", null ],
      [ "QUEST_PARTY_MSG_HAVE_QUEST", "_quest_def_8h.html#a119d3789e37252cfbcbf7e7c7d7489baa92ac32c022b23307b7a9162f851c73d7", null ],
      [ "QUEST_PARTY_MSG_FINISH_QUEST", "_quest_def_8h.html#a119d3789e37252cfbcbf7e7c7d7489baa03c2b208ccff7ddb48194c798da4230d", null ]
    ] ],
    [ "QuestSpecialFlags", "_quest_def_8h.html#afa444be95c64570168d2ecd6732aa7ad", [
      [ "QUEST_SPECIAL_FLAG_REPEATABLE", "_quest_def_8h.html#afa444be95c64570168d2ecd6732aa7ada3ff06a8de95b6f4ab08e6469c3e6d3ad", null ],
      [ "QUEST_SPECIAL_FLAG_EXPLORATION_OR_EVENT", "_quest_def_8h.html#afa444be95c64570168d2ecd6732aa7ada110c8b71244638f662306eb8421cd787", null ],
      [ "QUEST_SPECIAL_FLAG_DELIVER", "_quest_def_8h.html#afa444be95c64570168d2ecd6732aa7ada088ddebe2c4949b190f140710a208ecb", null ],
      [ "QUEST_SPECIAL_FLAG_SPEAKTO", "_quest_def_8h.html#afa444be95c64570168d2ecd6732aa7ada9148e5404fe41dcbfaf523e853544611", null ],
      [ "QUEST_SPECIAL_FLAG_KILL_OR_CAST", "_quest_def_8h.html#afa444be95c64570168d2ecd6732aa7ada37fb0253a7fb6d3ff0d7b75ebcdb86ae", null ],
      [ "QUEST_SPECIAL_FLAG_TIMED", "_quest_def_8h.html#afa444be95c64570168d2ecd6732aa7adaa0b7d3e0ed0c26a3390436efa20aa7f8", null ]
    ] ],
    [ "QuestStatus", "_quest_def_8h.html#aafe449b7709f89517097b7c7bc764d31", [
      [ "QUEST_STATUS_NONE", "_quest_def_8h.html#aafe449b7709f89517097b7c7bc764d31abefdf5f7417012e5d2adedf1dc155f28", null ],
      [ "QUEST_STATUS_COMPLETE", "_quest_def_8h.html#aafe449b7709f89517097b7c7bc764d31ae1b2fe2d9de28cc70b59a4bdd5f0abed", null ],
      [ "QUEST_STATUS_UNAVAILABLE", "_quest_def_8h.html#aafe449b7709f89517097b7c7bc764d31a29e8b87caa56fa19f43a4c40494e0844", null ],
      [ "QUEST_STATUS_INCOMPLETE", "_quest_def_8h.html#aafe449b7709f89517097b7c7bc764d31a9d062f65bead287785d9b540987bb484", null ],
      [ "QUEST_STATUS_AVAILABLE", "_quest_def_8h.html#aafe449b7709f89517097b7c7bc764d31aa988ec4c4b38bbb20c136c7b93705e49", null ],
      [ "QUEST_STATUS_FAILED", "_quest_def_8h.html#aafe449b7709f89517097b7c7bc764d31a8e2ffb80a87da63793e4122d9b73cf8b", null ],
      [ "MAX_QUEST_STATUS", "_quest_def_8h.html#aafe449b7709f89517097b7c7bc764d31ac5861bd66a93cb8083339c969a26f6d4", null ]
    ] ],
    [ "QuestTypes", "_quest_def_8h.html#aa6addbfd7b811a56c18d8b67d5d61712", [
      [ "QUEST_TYPE_ELITE", "_quest_def_8h.html#aa6addbfd7b811a56c18d8b67d5d61712a21c876180271aeb1a7b15e611e398cb4", null ],
      [ "QUEST_TYPE_LIFE", "_quest_def_8h.html#aa6addbfd7b811a56c18d8b67d5d61712a5ce2ad48b3513d89973f9fdb5dcad924", null ],
      [ "QUEST_TYPE_PVP", "_quest_def_8h.html#aa6addbfd7b811a56c18d8b67d5d61712ab2ca1f5cd185e6dcdf89efe9dc3f90bd", null ],
      [ "QUEST_TYPE_RAID", "_quest_def_8h.html#aa6addbfd7b811a56c18d8b67d5d61712a0157b79207785039161f984f430db806", null ],
      [ "QUEST_TYPE_DUNGEON", "_quest_def_8h.html#aa6addbfd7b811a56c18d8b67d5d61712ae50c239acc02bbcab08684155ed1d310", null ],
      [ "QUEST_TYPE_WORLD_EVENT", "_quest_def_8h.html#aa6addbfd7b811a56c18d8b67d5d61712a4b9dddcbeaacaae5de66a72a13e32905", null ],
      [ "QUEST_TYPE_LEGENDARY", "_quest_def_8h.html#aa6addbfd7b811a56c18d8b67d5d61712ab6e521e4beb3fcda66c3b8ecbcb49b16", null ],
      [ "QUEST_TYPE_ESCORT", "_quest_def_8h.html#aa6addbfd7b811a56c18d8b67d5d61712a7d1fec8fe66a481a8c631fde02e9bb0a", null ]
    ] ],
    [ "QuestUpdateState", "_quest_def_8h.html#a6d0c6c243048329681393fc8a7aeb99b", [
      [ "QUEST_UNCHANGED", "_quest_def_8h.html#a6d0c6c243048329681393fc8a7aeb99ba49a6a9d4d94f7c4b4bd09eb08f44fbf3", null ],
      [ "QUEST_CHANGED", "_quest_def_8h.html#a6d0c6c243048329681393fc8a7aeb99ba94f08cb6258ab0f82686514128eb3a06", null ],
      [ "QUEST_NEW", "_quest_def_8h.html#a6d0c6c243048329681393fc8a7aeb99ba3e9ee1f9ce089029c82636739c35a3e0", null ]
    ] ]
];