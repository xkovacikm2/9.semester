var struct_player_info =
[
    [ "PlayerInfo", "struct_player_info.html#a05e9b9529ed8e4638b2bd2fb76b3abeb", null ],
    [ "action", "struct_player_info.html#a0f68412598a9741e9b5ce7a42c82d129", null ],
    [ "areaId", "struct_player_info.html#a60d086c6e6879e6c5d8b5a24c0ae7bf8", null ],
    [ "displayId_f", "struct_player_info.html#adf14854368c9351fe2fba3215194f2f9", null ],
    [ "displayId_m", "struct_player_info.html#a2ce7d9aa7fc52bd9768c75b88f88b256", null ],
    [ "item", "struct_player_info.html#ad76819034c64c0c691561eff683b7e57", null ],
    [ "levelInfo", "struct_player_info.html#abbff546bbe2cc0ea643c41456c1f0d9f", null ],
    [ "mapId", "struct_player_info.html#ad8d359374f74de40c1c591bb61976405", null ],
    [ "orientation", "struct_player_info.html#a041a37c356d50860cab9f1ced33a28d0", null ],
    [ "positionX", "struct_player_info.html#a55a076dc53e8c2d0b2ab11b1087ebccd", null ],
    [ "positionY", "struct_player_info.html#a7314ecd469357e2eb46ed3d133a5a7a0", null ],
    [ "positionZ", "struct_player_info.html#ad21a8dd3353d3f141bcc9af65e607aee", null ],
    [ "spell", "struct_player_info.html#ab9d7c06822c6e1ea4a6308fd32bd27f6", null ]
];