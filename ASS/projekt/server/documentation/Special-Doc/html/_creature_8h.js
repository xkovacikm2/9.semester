var _creature_8h =
[
    [ "CreatureInfo", "struct_creature_info.html", "struct_creature_info" ],
    [ "CreatureTemplateSpells", "struct_creature_template_spells.html", "struct_creature_template_spells" ],
    [ "EquipmentInfo", "struct_equipment_info.html", "struct_equipment_info" ],
    [ "EquipmentInfoItem", "struct_equipment_info_item.html", "struct_equipment_info_item" ],
    [ "EquipmentInfoRaw", "struct_equipment_info_raw.html", "struct_equipment_info_raw" ],
    [ "CreatureData", "struct_creature_data.html", "struct_creature_data" ],
    [ "CreatureDataAddon", "struct_creature_data_addon.html", "struct_creature_data_addon" ],
    [ "CreatureClassLvlStats", "struct_creature_class_lvl_stats.html", "struct_creature_class_lvl_stats" ],
    [ "CreatureModelInfo", "struct_creature_model_info.html", "struct_creature_model_info" ],
    [ "CreatureLocale", "struct_creature_locale.html", "struct_creature_locale" ],
    [ "GossipMenuItemsLocale", "struct_gossip_menu_items_locale.html", "struct_gossip_menu_items_locale" ],
    [ "PointOfInterestLocale", "struct_point_of_interest_locale.html", "struct_point_of_interest_locale" ],
    [ "VendorItem", "struct_vendor_item.html", "struct_vendor_item" ],
    [ "VendorItemData", "struct_vendor_item_data.html", "struct_vendor_item_data" ],
    [ "VendorItemCount", "struct_vendor_item_count.html", "struct_vendor_item_count" ],
    [ "TrainerSpell", "struct_trainer_spell.html", "struct_trainer_spell" ],
    [ "TrainerSpellData", "struct_trainer_spell_data.html", "struct_trainer_spell_data" ],
    [ "CreatureCreatePos", "struct_creature_create_pos.html", "struct_creature_create_pos" ],
    [ "Creature", "class_creature.html", "class_creature" ],
    [ "ForcedDespawnDelayEvent", "class_forced_despawn_delay_event.html", "class_forced_despawn_delay_event" ],
    [ "CREATURE_Z_ATTACK_RANGE", "_creature_8h.html#a91a03a879f1b4df44968ae2f9f22fb0d", null ],
    [ "MAX_CREATURE_MODEL", "_creature_8h.html#aedb9af27fc549b3c68e6673cb7f7c491", null ],
    [ "MAX_KILL_CREDIT", "_creature_8h.html#a3f331d9e202385f679104cc20ef74c3d", null ],
    [ "MAX_VENDOR_ITEMS", "_creature_8h.html#a44c3a813b92331e325ba5aae11ed72a3", null ],
    [ "MAX_VIRTUAL_ITEM_SLOT", "_creature_8h.html#a8f7d803e051a8bcb43a44f16b01f9c09", null ],
    [ "USE_DEFAULT_DATABASE_LEVEL", "_creature_8h.html#af359b23a6535a86f691a1f9490ddbf0d", null ],
    [ "CreatureSpellCooldowns", "_creature_8h.html#ac3425d3e0ea42c1c7ceee65ac9a91d57", null ],
    [ "TrainerSpellMap", "_creature_8h.html#a73ae2c890e2e0b456043962562156f71", null ],
    [ "VendorItemCounts", "_creature_8h.html#a545edaa9fa6e5172e0acaa6f686d8a6c", null ],
    [ "VendorItemList", "_creature_8h.html#ade72c22c956598b6544cf254bdf9e975", null ],
    [ "AttackingTarget", "_creature_8h.html#abd5ff2d3af268e1be207d465ac97f737", [
      [ "ATTACKING_TARGET_RANDOM", "_creature_8h.html#abd5ff2d3af268e1be207d465ac97f737adb3a0bbc7e5e594a7547284d1a41d848", null ],
      [ "ATTACKING_TARGET_TOPAGGRO", "_creature_8h.html#abd5ff2d3af268e1be207d465ac97f737ab18fdae2ce547ce798e7be8ac9c6e8cb", null ],
      [ "ATTACKING_TARGET_BOTTOMAGGRO", "_creature_8h.html#abd5ff2d3af268e1be207d465ac97f737ae6f3f124c1a1647daa16f65c0765132a", null ]
    ] ],
    [ "ChatType", "_creature_8h.html#ab474065b922c4091606228d4c785b15e", [
      [ "CHAT_TYPE_SAY", "_creature_8h.html#ab474065b922c4091606228d4c785b15ea259d4bad6e08f092ee9af5a87de80961", null ],
      [ "CHAT_TYPE_YELL", "_creature_8h.html#ab474065b922c4091606228d4c785b15eadc5980413f923977ff9f66e4966b005c", null ],
      [ "CHAT_TYPE_TEXT_EMOTE", "_creature_8h.html#ab474065b922c4091606228d4c785b15ea45c06648182a8ec2320d052aef7893b9", null ],
      [ "CHAT_TYPE_BOSS_EMOTE", "_creature_8h.html#ab474065b922c4091606228d4c785b15ea983164442279412488cf59d326d2c81d", null ],
      [ "CHAT_TYPE_WHISPER", "_creature_8h.html#ab474065b922c4091606228d4c785b15eac9530b47feeb0908379b96ae42b417c7", null ],
      [ "CHAT_TYPE_BOSS_WHISPER", "_creature_8h.html#ab474065b922c4091606228d4c785b15ea42bf788b7a507b711a9fe07b4c24bcc7", null ],
      [ "CHAT_TYPE_ZONE_YELL", "_creature_8h.html#ab474065b922c4091606228d4c785b15eac4409452bcef503c226a3480f043a82b", null ]
    ] ],
    [ "CreatureFlagsExtra", "_creature_8h.html#a509c490881d9ef2abce208236910f0a5", [
      [ "CREATURE_EXTRA_FLAG_INSTANCE_BIND", "_creature_8h.html#a509c490881d9ef2abce208236910f0a5abbc7417d902b630a19a7241c474ca7f3", null ],
      [ "CREATURE_EXTRA_FLAG_NO_AGGRO", "_creature_8h.html#a509c490881d9ef2abce208236910f0a5a8f495f9768290b5a7ca2eebeb85c5b42", null ],
      [ "CREATURE_EXTRA_FLAG_NO_PARRY", "_creature_8h.html#a509c490881d9ef2abce208236910f0a5a941ac3b597462158d90b843d1bf26f44", null ],
      [ "CREATURE_EXTRA_FLAG_NO_PARRY_HASTEN", "_creature_8h.html#a509c490881d9ef2abce208236910f0a5a31630150b56cc69dd421103be9598786", null ],
      [ "CREATURE_EXTRA_FLAG_NO_BLOCK", "_creature_8h.html#a509c490881d9ef2abce208236910f0a5aa66ea8dc193797b1ffbdab4e65d23b9a", null ],
      [ "CREATURE_EXTRA_FLAG_NO_CRUSH", "_creature_8h.html#a509c490881d9ef2abce208236910f0a5a6e4b34f969dc8feebe8ab4ee47ab7c12", null ],
      [ "CREATURE_EXTRA_FLAG_NO_XP_AT_KILL", "_creature_8h.html#a509c490881d9ef2abce208236910f0a5a04f977dcc07b0a2d9c8dc506f5439dcc", null ],
      [ "CREATURE_EXTRA_FLAG_INVISIBLE", "_creature_8h.html#a509c490881d9ef2abce208236910f0a5ac1f31402dafafeec1eb6194adc52b3d6", null ],
      [ "CREATURE_EXTRA_FLAG_NOT_TAUNTABLE", "_creature_8h.html#a509c490881d9ef2abce208236910f0a5a59fb0a7847f0f8a297776e4b22ccc95c", null ],
      [ "CREATURE_EXTRA_FLAG_AGGRO_ZONE", "_creature_8h.html#a509c490881d9ef2abce208236910f0a5a72d482faaad245cd956a43d44e11fe21", null ],
      [ "CREATURE_EXTRA_FLAG_GUARD", "_creature_8h.html#a509c490881d9ef2abce208236910f0a5a3e07b7eb885eb2855efc2404974d000f", null ],
      [ "CREATURE_EXTRA_FLAG_NO_CALL_ASSIST", "_creature_8h.html#a509c490881d9ef2abce208236910f0a5a7568c216449b6f09a575cd278b50bda7", null ],
      [ "CREATURE_EXTRA_FLAG_ACTIVE", "_creature_8h.html#a509c490881d9ef2abce208236910f0a5a6ddb81b9ff0eef694e892068b909865a", null ],
      [ "CREATURE_EXTRA_FLAG_MMAP_FORCE_ENABLE", "_creature_8h.html#a509c490881d9ef2abce208236910f0a5aee6840da1a82d19ba3171eb7a5f83a0b", null ],
      [ "CREATURE_EXTRA_FLAG_MMAP_FORCE_DISABLE", "_creature_8h.html#a509c490881d9ef2abce208236910f0a5a564ffe6bdd7cad3999b979d5e3b76e13", null ],
      [ "CREATURE_EXTRA_FLAG_WALK_IN_WATER", "_creature_8h.html#a509c490881d9ef2abce208236910f0a5a0a4b61051017c26bb6a0504137b6baf8", null ],
      [ "CREATURE_EXTRA_FLAG_HAVE_NO_SWIM_ANIMATION", "_creature_8h.html#a509c490881d9ef2abce208236910f0a5aac0e4b7251932bcfc202578070c70cba", null ]
    ] ],
    [ "CreatureSubtype", "_creature_8h.html#a0adc01a263d6c377feabd69772dd957a", [
      [ "CREATURE_SUBTYPE_GENERIC", "_creature_8h.html#a0adc01a263d6c377feabd69772dd957aae239f8d3bf9c8a04a7634480dcfc706a", null ],
      [ "CREATURE_SUBTYPE_PET", "_creature_8h.html#a0adc01a263d6c377feabd69772dd957aa9ec8abad4095c21592d208efdc4a866a", null ],
      [ "CREATURE_SUBTYPE_TOTEM", "_creature_8h.html#a0adc01a263d6c377feabd69772dd957aa11abf3812d92f6281b35613d0973b328", null ],
      [ "CREATURE_SUBTYPE_TEMPORARY_SUMMON", "_creature_8h.html#a0adc01a263d6c377feabd69772dd957aae767d4fabc9c275da21d4d819b98e358", null ]
    ] ],
    [ "InhabitTypeValues", "_creature_8h.html#aec385f62258c23b8c755b8e78a0e1873", [
      [ "INHABIT_GROUND", "_creature_8h.html#aec385f62258c23b8c755b8e78a0e1873a520d2ee7572e47cd40d4a81ec015a38b", null ],
      [ "INHABIT_WATER", "_creature_8h.html#aec385f62258c23b8c755b8e78a0e1873a3e409a2141cd78fb1bdd83d36c2388e7", null ],
      [ "INHABIT_AIR", "_creature_8h.html#aec385f62258c23b8c755b8e78a0e1873a2e939d2dbf075bfb7c86e3e971beb228", null ],
      [ "INHABIT_ANYWHERE", "_creature_8h.html#aec385f62258c23b8c755b8e78a0e1873ab9163dc24970d6c55bc79f344d177e15", null ]
    ] ],
    [ "RegenStatsFlags", "_creature_8h.html#aa24ce34a520f35e4ffc0c759b95c69cb", [
      [ "REGEN_FLAG_HEALTH", "_creature_8h.html#aa24ce34a520f35e4ffc0c759b95c69cba96beb69bd1a181d485c5fc047b1a67f7", null ],
      [ "REGEN_FLAG_POWER", "_creature_8h.html#aa24ce34a520f35e4ffc0c759b95c69cbaaafe0d30695923fcb4da5bf23e13f318", null ]
    ] ],
    [ "SelectFlags", "_creature_8h.html#a3e751bdcf1ad5d9f8b1f92f32e460ea5", [
      [ "SELECT_FLAG_IN_LOS", "_creature_8h.html#a3e751bdcf1ad5d9f8b1f92f32e460ea5a451f21c27cf8e231bd96f24fcb81e1f6", null ],
      [ "SELECT_FLAG_PLAYER", "_creature_8h.html#a3e751bdcf1ad5d9f8b1f92f32e460ea5a8dd8e7a85253a65a9119ef98f8850c72", null ],
      [ "SELECT_FLAG_POWER_MANA", "_creature_8h.html#a3e751bdcf1ad5d9f8b1f92f32e460ea5aa5d43959d2c93af344fe180e308f6ef4", null ],
      [ "SELECT_FLAG_POWER_RAGE", "_creature_8h.html#a3e751bdcf1ad5d9f8b1f92f32e460ea5af1106c5bd35813ccd0467e6635b59f9c", null ],
      [ "SELECT_FLAG_POWER_ENERGY", "_creature_8h.html#a3e751bdcf1ad5d9f8b1f92f32e460ea5a1e1dc18d10f597675683b3263cd349c2", null ],
      [ "SELECT_FLAG_IN_MELEE_RANGE", "_creature_8h.html#a3e751bdcf1ad5d9f8b1f92f32e460ea5adfdf78819effd2237c26a4cc3a08d8fd", null ],
      [ "SELECT_FLAG_NOT_IN_MELEE_RANGE", "_creature_8h.html#a3e751bdcf1ad5d9f8b1f92f32e460ea5a043e822247ee52c8a7679d82f859a148", null ]
    ] ],
    [ "SplineFlags", "_creature_8h.html#a3b388b7fbe309af507459a14b84e7896", [
      [ "SPLINEFLAG_WALKMODE", "_creature_8h.html#a3b388b7fbe309af507459a14b84e7896a279a5c8f7d90cf020ec144da59fdd1e0", null ],
      [ "SPLINEFLAG_FLYING", "_creature_8h.html#a3b388b7fbe309af507459a14b84e7896af8216e30d04dc8ac70594857ddcc6104", null ]
    ] ],
    [ "TemporaryFactionFlags", "_creature_8h.html#a446fa4b0844e5cf76f98961e6e90f183", [
      [ "TEMPFACTION_NONE", "_creature_8h.html#a446fa4b0844e5cf76f98961e6e90f183ad59f1ae7febaae05725b91537ffb84d2", null ],
      [ "TEMPFACTION_RESTORE_RESPAWN", "_creature_8h.html#a446fa4b0844e5cf76f98961e6e90f183a79e2a0afea33a5c6cf5c68a63498a236", null ],
      [ "TEMPFACTION_RESTORE_COMBAT_STOP", "_creature_8h.html#a446fa4b0844e5cf76f98961e6e90f183a0e3943f6fe23a0401dfa2787d7fc1a04", null ],
      [ "TEMPFACTION_RESTORE_REACH_HOME", "_creature_8h.html#a446fa4b0844e5cf76f98961e6e90f183a3a20da4406d33d3007a95beb9d59a1eb", null ],
      [ "TEMPFACTION_TOGGLE_NON_ATTACKABLE", "_creature_8h.html#a446fa4b0844e5cf76f98961e6e90f183a3f20b026668f9f866718a894055a871f", null ],
      [ "TEMPFACTION_TOGGLE_OOC_NOT_ATTACK", "_creature_8h.html#a446fa4b0844e5cf76f98961e6e90f183a4644d3b0086883aaac06775107280aba", null ],
      [ "TEMPFACTION_TOGGLE_PASSIVE", "_creature_8h.html#a446fa4b0844e5cf76f98961e6e90f183a3956ef36dadda4440b780a46fba060fb", null ],
      [ "TEMPFACTION_TOGGLE_PACIFIED", "_creature_8h.html#a446fa4b0844e5cf76f98961e6e90f183a5ee83d77107b3631310864b1c64af4fd", null ],
      [ "TEMPFACTION_TOGGLE_NOT_SELECTABLE", "_creature_8h.html#a446fa4b0844e5cf76f98961e6e90f183aaec2fc7e3255fee0d8cb2a779cb049e1", null ],
      [ "TEMPFACTION_ALL", "_creature_8h.html#a446fa4b0844e5cf76f98961e6e90f183a25eefd749ff853768b5e865bddb275c1", null ]
    ] ],
    [ "VirtualItemInfoByteOffset", "_creature_8h.html#a6a4fd7391745e6f6b5089b89003ff8c9", [
      [ "VIRTUAL_ITEM_INFO_0_OFFSET_CLASS", "_creature_8h.html#a6a4fd7391745e6f6b5089b89003ff8c9ac5b160f2819866bae7ab669072201933", null ],
      [ "VIRTUAL_ITEM_INFO_0_OFFSET_SUBCLASS", "_creature_8h.html#a6a4fd7391745e6f6b5089b89003ff8c9a696b2ad0cd77520488f5b84c56d187ee", null ],
      [ "VIRTUAL_ITEM_INFO_0_OFFSET_MATERIAL", "_creature_8h.html#a6a4fd7391745e6f6b5089b89003ff8c9a2b85d12c578765da555058969a14e4e0", null ],
      [ "VIRTUAL_ITEM_INFO_0_OFFSET_INVENTORYTYPE", "_creature_8h.html#a6a4fd7391745e6f6b5089b89003ff8c9a34c6bd10967002b690e3d29047edd8b7", null ],
      [ "VIRTUAL_ITEM_INFO_1_OFFSET_SHEATH", "_creature_8h.html#a6a4fd7391745e6f6b5089b89003ff8c9ad6d9bd776aef857e2fbc35531eba6f55", null ]
    ] ],
    [ "VirtualItemSlot", "_creature_8h.html#a27fa9a4414aee2cf8b94a6e9b4e5bdf6", [
      [ "VIRTUAL_ITEM_SLOT_0", "_creature_8h.html#a27fa9a4414aee2cf8b94a6e9b4e5bdf6afbeeb94d660c5808ec5a888b0fb1ee8d", null ],
      [ "VIRTUAL_ITEM_SLOT_1", "_creature_8h.html#a27fa9a4414aee2cf8b94a6e9b4e5bdf6a6e1124241b4d933306d866ef3495ff8a", null ],
      [ "VIRTUAL_ITEM_SLOT_2", "_creature_8h.html#a27fa9a4414aee2cf8b94a6e9b4e5bdf6a439d35234a9e700db37dfa3ac4daf092", null ]
    ] ]
];