var class_object =
[
    [ "~Object", "class_object.html#ae8f5483f459e46687bd01e6f9977afd3", null ],
    [ "Object", "class_object.html#a40860402e64d8008fb42329df7097cdb", null ],
    [ "_Create", "class_object.html#a67a8903f529af7e3ff6d296d59dd8405", null ],
    [ "_InitValues", "class_object.html#a746093971e9e14c965e504f5db7d4541", null ],
    [ "_ReCreate", "class_object.html#af72dac5ba1ca210e6a3ddff14e53a86d", null ],
    [ "_SetCreateBits", "class_object.html#a2abab1e974cd2e8d0b3cdb566e0b05b7", null ],
    [ "_SetUpdateBits", "class_object.html#ad5edc00b73065b986ac87e55018f5f36", null ],
    [ "AddToClientUpdateList", "class_object.html#a16db8416fa156b3973ba080a996f2765", null ],
    [ "AddToWorld", "class_object.html#a556f6c1d8ce43bcd015197e74996b9e8", null ],
    [ "ApplyModByteFlag", "class_object.html#acea9c3306a8f1a52e36835db02dd1a64", null ],
    [ "ApplyModFlag", "class_object.html#a882b48728ca660c0f6251bb595e8c67e", null ],
    [ "ApplyModFlag64", "class_object.html#a1d0aed1f0b94a4eeca8be2952189dc0d", null ],
    [ "ApplyModInt32Value", "class_object.html#a494b539a938499639eab3f8d9a0c3364", null ],
    [ "ApplyModPositiveFloatValue", "class_object.html#a4feda5e6ca74034f95bd463f44352854", null ],
    [ "ApplyModShortFlag", "class_object.html#a464b5b271b3eecc29f66b3a43d01eed5", null ],
    [ "ApplyModSignedFloatValue", "class_object.html#a00340fbc92fd28dfaa5e945a7f5e5a77", null ],
    [ "ApplyModUInt32Value", "class_object.html#a830bd94d6366efff27b887ec963c1dab", null ],
    [ "ApplyPercentModFloatValue", "class_object.html#aeff5b9b739f39137cb0c5d83a1a50457", null ],
    [ "BuildCreateUpdateBlockForPlayer", "class_object.html#a679880881a212915277221770ee9bcb3", null ],
    [ "BuildMovementUpdate", "class_object.html#ae56eecfa6533a611ef21d59489ccf54e", null ],
    [ "BuildOutOfRangeUpdateBlock", "class_object.html#ad591516e39981b3684a9274ce6152f3d", null ],
    [ "BuildUpdateData", "class_object.html#a250500b961c3e9700702e4765c7adb95", null ],
    [ "BuildUpdateDataForPlayer", "class_object.html#a17a8d4c6073a9e575524f0bd0c219665", null ],
    [ "BuildValuesUpdate", "class_object.html#a1aab0c5938c8e24fbdc0781242b46e95", null ],
    [ "BuildValuesUpdateBlockForPlayer", "class_object.html#aafb370f7a523b6a3b99cd8c427019752", null ],
    [ "ClearUpdateMask", "class_object.html#a8a75ada284806c5ee19fd53a48ba6924", null ],
    [ "DestroyForPlayer", "class_object.html#a6c6d7d12990aab480714a19cf4722922", null ],
    [ "GetByteValue", "class_object.html#a0958df7e77da48c2cfb6fad103a9ea44", null ],
    [ "GetEntry", "class_object.html#aa627fe2967ed0e0ed1d8aeeb4365a961", null ],
    [ "GetFloatValue", "class_object.html#ae70d868c6ec2c0d3849a48c6e75da5aa", null ],
    [ "GetGUIDLow", "class_object.html#a4777844356842a389621c1bad1433109", null ],
    [ "GetGuidStr", "class_object.html#a5c2f31b9fb0a083ff4caca3de93fac90", null ],
    [ "GetGuidValue", "class_object.html#adee3f37717608da2092b30f3bcc40b2d", null ],
    [ "GetInt32Value", "class_object.html#acbbf66178b2c9ec6d5d44d80b43abfb6", null ],
    [ "GetObjectGuid", "class_object.html#ae7e0c11556aeda48ab347a56b6b7bd6a", null ],
    [ "GetObjectScale", "class_object.html#a1ca2dc2f577c6c2b99f747cc08646090", null ],
    [ "GetPackGUID", "class_object.html#ad4b3f372b6700cd50e1eb7ef238cf3be", null ],
    [ "GetTypeId", "class_object.html#a7e985367f6a9997544bbc18253f3563d", null ],
    [ "GetUInt16Value", "class_object.html#a41c82ba1a547ce8bb6e9e1b98d189aba", null ],
    [ "GetUInt32Value", "class_object.html#aebc9e7589e372ab99544e474c07609f0", null ],
    [ "GetUInt64Value", "class_object.html#addb94a904bfd47c836d6eea75c052abf", null ],
    [ "GetValuesCount", "class_object.html#ae1019ecea5f6682ad417080e12ec9008", null ],
    [ "HasByteFlag", "class_object.html#ad837c14c5766b8c13f4a5ce2532b0657", null ],
    [ "HasFlag", "class_object.html#acf64820d3725a37cef2c63f0baa820e7", null ],
    [ "HasFlag64", "class_object.html#aebc9abb8dc78d8184bfbc5cd2a23eff0", null ],
    [ "HasInvolvedQuest", "class_object.html#a984ccc38ff20b245c3c1fb4d25ddfa5b", null ],
    [ "HasQuest", "class_object.html#a9c541a568a8d9f8cf678e0b13213581c", null ],
    [ "HasShortFlag", "class_object.html#a214e40ad041dcff83164c5af2499c9cf", null ],
    [ "IsInWorld", "class_object.html#a8acb91caf94322dc0289d3e60eb8e0e7", null ],
    [ "isType", "class_object.html#ab3fb8844a3549186ed265dd9a7b4b670", null ],
    [ "LoadValues", "class_object.html#ad7fae23d4b2e1bb3d8e1e016404ce12b", null ],
    [ "MarkFlagUpdateForClient", "class_object.html#ae1a87a495f1a3f611993701937617173", null ],
    [ "MarkForClientUpdate", "class_object.html#a5ef1f3184ea1c5cfbe48ab1123c46858", null ],
    [ "PrintEntryError", "class_object.html#a055191d03e423c609e22ada2e901ee94", null ],
    [ "PrintIndexError", "class_object.html#aad61e90207805a7ad4464969408c0f8a", null ],
    [ "RemoveByteFlag", "class_object.html#acef75a7f84673fda790861bd1b0b68b7", null ],
    [ "RemoveFlag", "class_object.html#a7e764fbd4b7eca84f80e36e900bca7d3", null ],
    [ "RemoveFlag64", "class_object.html#ab8a79526424a0a8b4719a8ecc2f54606", null ],
    [ "RemoveFromClientUpdateList", "class_object.html#aa1f2172b2ed8c05c6c1c62503a27252a", null ],
    [ "RemoveFromWorld", "class_object.html#a0c750f2c1b55c4584ff4d4b583ed0103", null ],
    [ "RemoveShortFlag", "class_object.html#ad98daf0fe4bd2d175e2d71cbbb375d77", null ],
    [ "SendCreateUpdateToPlayer", "class_object.html#a3cb21f2c9ad514d86674436cc47374b4", null ],
    [ "SendForcedObjectUpdate", "class_object.html#a60ad577129223b6c89a6afb089b6fc9a", null ],
    [ "SetAsNewObject", "class_object.html#a5474bafe68b4322dc8f39b34b7eb3279", null ],
    [ "SetByteFlag", "class_object.html#afe65b1ab32dbcd51890b754fb8b5e1b0", null ],
    [ "SetByteValue", "class_object.html#a0a29933a2da73d788a130f971511f2f5", null ],
    [ "SetEntry", "class_object.html#ad5ba4799de88341246bc5ff7dd4b2d36", null ],
    [ "SetFlag", "class_object.html#a79a7a80acdec46961b4d8aaead4ae631", null ],
    [ "SetFlag64", "class_object.html#a9a7b62d70508bd9f908bf06964c07a0a", null ],
    [ "SetFloatValue", "class_object.html#abc072fa242381b397e4d282e31bcb950", null ],
    [ "SetGuidValue", "class_object.html#a622b2dbdec79c7edf471a2514d8a222d", null ],
    [ "SetInt16Value", "class_object.html#a219c875817057a6189178a0d5b9d5a09", null ],
    [ "SetInt32Value", "class_object.html#a1dd144f55a02384903919242dbf17505", null ],
    [ "SetObjectScale", "class_object.html#ab1221153d59ef17ba4790cec60eaa9bf", null ],
    [ "SetShortFlag", "class_object.html#aead10b710f056a12d971de051df8f542", null ],
    [ "SetStatFloatValue", "class_object.html#ac7552adafc068a8e4435e969194677f5", null ],
    [ "SetStatInt32Value", "class_object.html#af01444effe1f5da6942ad6d597bd0e48", null ],
    [ "SetUInt16Value", "class_object.html#a2c1b0dfb53bdedb46f13733dcde3d216", null ],
    [ "SetUInt32Value", "class_object.html#adc7e9b205f35f9894ec9a4f0ec0871f9", null ],
    [ "SetUInt64Value", "class_object.html#a1c78e5310671694d3996f2046b638846", null ],
    [ "ToCorpse", "class_object.html#aaaf4f6d0e09304798982a4e46d3d4191", null ],
    [ "ToCorpse", "class_object.html#a46591888fc8a21a04b191a68b2c0592e", null ],
    [ "ToCreature", "class_object.html#a4a4bdac0d151f9193aa3581bda31193c", null ],
    [ "ToCreature", "class_object.html#ab64c8a37fa7115e9613c3173de4f7472", null ],
    [ "ToDynObject", "class_object.html#aae5d2cfe04bb2d3148d0ce8dc7b8ee1e", null ],
    [ "ToDynObject", "class_object.html#a4d78d22c00bc5e02449cd81aead14f74", null ],
    [ "ToGameObject", "class_object.html#ac29133995854ac3bf59c164a91d6efaa", null ],
    [ "ToGameObject", "class_object.html#aa8930eec77af6fc85137c71c49f8c47c", null ],
    [ "ToggleByteFlag", "class_object.html#af81f667d0207466b63cf4982e109eebc", null ],
    [ "ToggleFlag", "class_object.html#ae6d7d519332f6d82f25286070ed351b1", null ],
    [ "ToggleFlag64", "class_object.html#a06fad148a907831b5c3ee5e822b3a1b4", null ],
    [ "ToggleShortFlag", "class_object.html#ae3f12d460a605f9b084c2b523ffce801", null ],
    [ "ToPlayer", "class_object.html#ae635434021784608b659501867538c87", null ],
    [ "ToPlayer", "class_object.html#a01b46d194eb94d55c647f3b2f0ba38de", null ],
    [ "ToUnit", "class_object.html#ac7d5ffa5c25d3951457c9b0a3c90ce76", null ],
    [ "ToUnit", "class_object.html#a237793af1e5064d6cf45a94230a05517", null ],
    [ "UpdateUInt32Value", "class_object.html#a04f33aa76bc03e8b1b9337fc5363c46d", null ],
    [ "m_changedValues", "class_object.html#a90aee1be81d47cc91599f87a55dc93f6", null ],
    [ "m_floatValues", "class_object.html#ad4fdb092a9ea4f6f5fc082bb755b24bd", null ],
    [ "m_int32Values", "class_object.html#a0e3befcc1645cf61475f19ba9ca4afee", null ],
    [ "m_objectType", "class_object.html#a139043a10260048589bb9c61292df815", null ],
    [ "m_objectTypeId", "class_object.html#a9ed462c172c2a0ece92cd5546349a993", null ],
    [ "m_objectUpdated", "class_object.html#af2a0d1358ff1532ac3cdb78b357f2fd0", null ],
    [ "m_plrSpecificFlags", "class_object.html#aea2d48215ea18810aaaca274fd7e04e9", null ],
    [ "m_uint32Values", "class_object.html#a653cc6291a81ce05fcfe0c7c3dea8c3d", null ],
    [ "m_updateFlag", "class_object.html#a0a403bf9815c479aaa9f5c3b31686ba6", null ],
    [ "m_valuesCount", "class_object.html#a742999a5b0741f84b7dbd594dfba693a", null ]
];