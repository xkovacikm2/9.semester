var _database_8h =
[
    [ "SqlConnection", "class_sql_connection.html", "class_sql_connection" ],
    [ "Lock", "class_sql_connection_1_1_lock.html", "class_sql_connection_1_1_lock" ],
    [ "Database", "class_database.html", "class_database" ],
    [ "TransHelper", "class_database_1_1_trans_helper.html", "class_database_1_1_trans_helper" ],
    [ "MAX_QUERY_LEN", "_database_8h.html#a5799c8932ad35c57edbd997ef75f8fce", null ],
    [ "DatabaseTypes", "_database_8h.html#a55952e64a3ebe86dc0aba267b22fa8fa", [
      [ "DATABASE_WORLD", "_database_8h.html#a55952e64a3ebe86dc0aba267b22fa8faa1ceb4193ab67ccde79e26e55f7b04e37", null ],
      [ "DATABASE_REALMD", "_database_8h.html#a55952e64a3ebe86dc0aba267b22fa8faa8f4c9bd478c8be1bdb991d3380d96c1b", null ],
      [ "DATABASE_CHARACTER", "_database_8h.html#a55952e64a3ebe86dc0aba267b22fa8faa1f7fe9980e6f1a4235bac0b6039bda20", null ],
      [ "COUNT_DATABASES", "_database_8h.html#a55952e64a3ebe86dc0aba267b22fa8faa257b944f780eab662f18a659f5cad65a", null ]
    ] ]
];