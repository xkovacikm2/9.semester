var _instance_data_8h =
[
    [ "InstanceData", "class_instance_data.html", "class_instance_data" ],
    [ "InstanceConditionIDs", "_instance_data_8h.html#a837d842c85775c888d150f2382eca607", [
      [ "INSTANCE_CONDITION_ID_NORMAL_MODE", "_instance_data_8h.html#a837d842c85775c888d150f2382eca607ad8e94f24869703a69155b8a0a283b42c", null ],
      [ "INSTANCE_CONDITION_ID_HARD_MODE", "_instance_data_8h.html#a837d842c85775c888d150f2382eca607a6a2dfa3786223d1229be1882ca511af6", null ],
      [ "INSTANCE_CONDITION_ID_HARD_MODE_2", "_instance_data_8h.html#a837d842c85775c888d150f2382eca607af9bbe61898513353eabf2cc01025b5fe", null ],
      [ "INSTANCE_CONDITION_ID_HARD_MODE_3", "_instance_data_8h.html#a837d842c85775c888d150f2382eca607a3f7311879affd0261ccf5b24294465e4", null ],
      [ "INSTANCE_CONDITION_ID_HARD_MODE_4", "_instance_data_8h.html#a837d842c85775c888d150f2382eca607a9617ac37ce6798133d08cbe4426a0205", null ],
      [ "INSTANCE_CONDITION_ID_TEAM_HORDE", "_instance_data_8h.html#a837d842c85775c888d150f2382eca607a973d7e1ebd18bb1acbd0ef72146fad03", null ],
      [ "INSTANCE_CONDITION_ID_TEAM_ALLIANCE", "_instance_data_8h.html#a837d842c85775c888d150f2382eca607a8e948ad35253155c44719b2b05d7e19d", null ]
    ] ]
];