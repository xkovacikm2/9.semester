var struct_creature_family_entry =
[
    [ "ID", "struct_creature_family_entry.html#a84ba697182123cb3e7dd3ba07241e971", null ],
    [ "maxScale", "struct_creature_family_entry.html#a23ca0ae7c45790a24f58546970194abf", null ],
    [ "maxScaleLevel", "struct_creature_family_entry.html#ad5d722d74bc08d4de3edd54c2f24addb", null ],
    [ "minScale", "struct_creature_family_entry.html#a3eacdd8639c6dd25dbd3205a58860cf9", null ],
    [ "minScaleLevel", "struct_creature_family_entry.html#a49d9d7847cc4d54891d83166eb277740", null ],
    [ "Name", "struct_creature_family_entry.html#abb4ccf6a7d9dd32ffd55ef5c87e392f0", null ],
    [ "petFoodMask", "struct_creature_family_entry.html#a4d9dd8baa3f7981633248513bb25f4ca", null ],
    [ "skillLine", "struct_creature_family_entry.html#ad4498d40b31d18742ee4ac235cb58a2e", null ]
];