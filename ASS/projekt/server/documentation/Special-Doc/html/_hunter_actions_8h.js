var _hunter_actions_8h =
[
    [ "CastAspectOfTheHawkAction", "classai_1_1_cast_aspect_of_the_hawk_action.html", "classai_1_1_cast_aspect_of_the_hawk_action" ],
    [ "CastAspectOfTheWildAction", "classai_1_1_cast_aspect_of_the_wild_action.html", "classai_1_1_cast_aspect_of_the_wild_action" ],
    [ "CastAspectOfTheCheetahAction", "classai_1_1_cast_aspect_of_the_cheetah_action.html", "classai_1_1_cast_aspect_of_the_cheetah_action" ],
    [ "CastAspectOfThePackAction", "classai_1_1_cast_aspect_of_the_pack_action.html", "classai_1_1_cast_aspect_of_the_pack_action" ],
    [ "CastCallPetAction", "classai_1_1_cast_call_pet_action.html", "classai_1_1_cast_call_pet_action" ],
    [ "CastMendPetAction", "classai_1_1_cast_mend_pet_action.html", "classai_1_1_cast_mend_pet_action" ],
    [ "CastRevivePetAction", "classai_1_1_cast_revive_pet_action.html", "classai_1_1_cast_revive_pet_action" ],
    [ "CastTrueshotAuraAction", "classai_1_1_cast_trueshot_aura_action.html", "classai_1_1_cast_trueshot_aura_action" ],
    [ "CastFeignDeathAction", "classai_1_1_cast_feign_death_action.html", "classai_1_1_cast_feign_death_action" ],
    [ "CastRapidFireAction", "classai_1_1_cast_rapid_fire_action.html", "classai_1_1_cast_rapid_fire_action" ],
    [ "CastFreezingTrap", "classai_1_1_cast_freezing_trap.html", "classai_1_1_cast_freezing_trap" ],
    [ "CastWingClipAction", "classai_1_1_cast_wing_clip_action.html", "classai_1_1_cast_wing_clip_action" ],
    [ "CastSerpentStingOnAttackerAction", "classai_1_1_cast_serpent_sting_on_attacker_action.html", "classai_1_1_cast_serpent_sting_on_attacker_action" ],
    [ "isUseful", "_hunter_actions_8h.html#a6e0fdf21e7bb3ced0dfbfd0baf5bb6e5", null ]
];