var dir_62aedd113fb900a53d1af192539901ce =
[
    [ "CasterShamanStrategy.cpp", "_caster_shaman_strategy_8cpp.html", [
      [ "CasterShamanStrategyActionNodeFactory", "class_caster_shaman_strategy_action_node_factory.html", "class_caster_shaman_strategy_action_node_factory" ]
    ] ],
    [ "CasterShamanStrategy.h", "_caster_shaman_strategy_8h.html", [
      [ "CasterShamanStrategy", "classai_1_1_caster_shaman_strategy.html", "classai_1_1_caster_shaman_strategy" ],
      [ "CasterAoeShamanStrategy", "classai_1_1_caster_aoe_shaman_strategy.html", "classai_1_1_caster_aoe_shaman_strategy" ]
    ] ],
    [ "GenericShamanStrategy.cpp", "_generic_shaman_strategy_8cpp.html", [
      [ "GenericShamanStrategyActionNodeFactory", "class_generic_shaman_strategy_action_node_factory.html", "class_generic_shaman_strategy_action_node_factory" ]
    ] ],
    [ "GenericShamanStrategy.h", "_generic_shaman_strategy_8h.html", [
      [ "GenericShamanStrategy", "classai_1_1_generic_shaman_strategy.html", "classai_1_1_generic_shaman_strategy" ],
      [ "ShamanBuffDpsStrategy", "classai_1_1_shaman_buff_dps_strategy.html", "classai_1_1_shaman_buff_dps_strategy" ],
      [ "ShamanBuffManaStrategy", "classai_1_1_shaman_buff_mana_strategy.html", "classai_1_1_shaman_buff_mana_strategy" ]
    ] ],
    [ "HealShamanStrategy.cpp", "_heal_shaman_strategy_8cpp.html", [
      [ "HealShamanStrategyActionNodeFactory", "class_heal_shaman_strategy_action_node_factory.html", "class_heal_shaman_strategy_action_node_factory" ]
    ] ],
    [ "HealShamanStrategy.h", "_heal_shaman_strategy_8h.html", [
      [ "HealShamanStrategy", "classai_1_1_heal_shaman_strategy.html", "classai_1_1_heal_shaman_strategy" ]
    ] ],
    [ "MeleeShamanStrategy.cpp", "_melee_shaman_strategy_8cpp.html", [
      [ "MeleeShamanStrategyActionNodeFactory", "class_melee_shaman_strategy_action_node_factory.html", "class_melee_shaman_strategy_action_node_factory" ]
    ] ],
    [ "MeleeShamanStrategy.h", "_melee_shaman_strategy_8h.html", [
      [ "MeleeShamanStrategy", "classai_1_1_melee_shaman_strategy.html", "classai_1_1_melee_shaman_strategy" ],
      [ "MeleeAoeShamanStrategy", "classai_1_1_melee_aoe_shaman_strategy.html", "classai_1_1_melee_aoe_shaman_strategy" ]
    ] ],
    [ "ShamanActions.cpp", "_shaman_actions_8cpp.html", null ],
    [ "ShamanActions.h", "_shaman_actions_8h.html", [
      [ "CastLesserHealingWaveAction", "classai_1_1_cast_lesser_healing_wave_action.html", "classai_1_1_cast_lesser_healing_wave_action" ],
      [ "CastLesserHealingWaveOnPartyAction", "classai_1_1_cast_lesser_healing_wave_on_party_action.html", "classai_1_1_cast_lesser_healing_wave_on_party_action" ],
      [ "CastHealingWaveAction", "classai_1_1_cast_healing_wave_action.html", "classai_1_1_cast_healing_wave_action" ],
      [ "CastHealingWaveOnPartyAction", "classai_1_1_cast_healing_wave_on_party_action.html", "classai_1_1_cast_healing_wave_on_party_action" ],
      [ "CastChainHealAction", "classai_1_1_cast_chain_heal_action.html", "classai_1_1_cast_chain_heal_action" ],
      [ "CastLightningShieldAction", "classai_1_1_cast_lightning_shield_action.html", "classai_1_1_cast_lightning_shield_action" ],
      [ "CastRockbiterWeaponAction", "classai_1_1_cast_rockbiter_weapon_action.html", "classai_1_1_cast_rockbiter_weapon_action" ],
      [ "CastFlametongueWeaponAction", "classai_1_1_cast_flametongue_weapon_action.html", "classai_1_1_cast_flametongue_weapon_action" ],
      [ "CastFrostbrandWeaponAction", "classai_1_1_cast_frostbrand_weapon_action.html", "classai_1_1_cast_frostbrand_weapon_action" ],
      [ "CastWindfuryWeaponAction", "classai_1_1_cast_windfury_weapon_action.html", "classai_1_1_cast_windfury_weapon_action" ],
      [ "CastTotemAction", "classai_1_1_cast_totem_action.html", "classai_1_1_cast_totem_action" ],
      [ "CastStoneskinTotemAction", "classai_1_1_cast_stoneskin_totem_action.html", "classai_1_1_cast_stoneskin_totem_action" ],
      [ "CastEarthbindTotemAction", "classai_1_1_cast_earthbind_totem_action.html", "classai_1_1_cast_earthbind_totem_action" ],
      [ "CastStrengthOfEarthTotemAction", "classai_1_1_cast_strength_of_earth_totem_action.html", "classai_1_1_cast_strength_of_earth_totem_action" ],
      [ "CastManaSpringTotemAction", "classai_1_1_cast_mana_spring_totem_action.html", "classai_1_1_cast_mana_spring_totem_action" ],
      [ "CastManaTideTotemAction", "classai_1_1_cast_mana_tide_totem_action.html", "classai_1_1_cast_mana_tide_totem_action" ],
      [ "CastHealingStreamTotemAction", "classai_1_1_cast_healing_stream_totem_action.html", "classai_1_1_cast_healing_stream_totem_action" ],
      [ "CastCleansingTotemAction", "classai_1_1_cast_cleansing_totem_action.html", "classai_1_1_cast_cleansing_totem_action" ],
      [ "CastFlametongueTotemAction", "classai_1_1_cast_flametongue_totem_action.html", "classai_1_1_cast_flametongue_totem_action" ],
      [ "CastWindfuryTotemAction", "classai_1_1_cast_windfury_totem_action.html", "classai_1_1_cast_windfury_totem_action" ],
      [ "CastSearingTotemAction", "classai_1_1_cast_searing_totem_action.html", "classai_1_1_cast_searing_totem_action" ],
      [ "CastMagmaTotemAction", "classai_1_1_cast_magma_totem_action.html", "classai_1_1_cast_magma_totem_action" ],
      [ "CastAncestralSpiritAction", "classai_1_1_cast_ancestral_spirit_action.html", "classai_1_1_cast_ancestral_spirit_action" ],
      [ "CastPurgeAction", "classai_1_1_cast_purge_action.html", "classai_1_1_cast_purge_action" ],
      [ "CastStormstrikeAction", "classai_1_1_cast_stormstrike_action.html", "classai_1_1_cast_stormstrike_action" ],
      [ "CastWaterBreathingAction", "classai_1_1_cast_water_breathing_action.html", "classai_1_1_cast_water_breathing_action" ],
      [ "CastWaterWalkingAction", "classai_1_1_cast_water_walking_action.html", "classai_1_1_cast_water_walking_action" ],
      [ "CastWaterBreathingOnPartyAction", "classai_1_1_cast_water_breathing_on_party_action.html", "classai_1_1_cast_water_breathing_on_party_action" ],
      [ "CastWaterWalkingOnPartyAction", "classai_1_1_cast_water_walking_on_party_action.html", "classai_1_1_cast_water_walking_on_party_action" ],
      [ "CastFlameShockAction", "classai_1_1_cast_flame_shock_action.html", "classai_1_1_cast_flame_shock_action" ],
      [ "CastEarthShockAction", "classai_1_1_cast_earth_shock_action.html", "classai_1_1_cast_earth_shock_action" ],
      [ "CastFrostShockAction", "classai_1_1_cast_frost_shock_action.html", "classai_1_1_cast_frost_shock_action" ],
      [ "CastChainLightningAction", "classai_1_1_cast_chain_lightning_action.html", "classai_1_1_cast_chain_lightning_action" ],
      [ "CastLightningBoltAction", "classai_1_1_cast_lightning_bolt_action.html", "classai_1_1_cast_lightning_bolt_action" ],
      [ "CastBloodlustAction", "classai_1_1_cast_bloodlust_action.html", "classai_1_1_cast_bloodlust_action" ]
    ] ],
    [ "ShamanAiObjectContext.cpp", "_shaman_ai_object_context_8cpp.html", [
      [ "StrategyFactoryInternal", "classai_1_1shaman_1_1_strategy_factory_internal.html", "classai_1_1shaman_1_1_strategy_factory_internal" ],
      [ "BuffStrategyFactoryInternal", "classai_1_1shaman_1_1_buff_strategy_factory_internal.html", "classai_1_1shaman_1_1_buff_strategy_factory_internal" ],
      [ "CombatStrategyFactoryInternal", "classai_1_1shaman_1_1_combat_strategy_factory_internal.html", "classai_1_1shaman_1_1_combat_strategy_factory_internal" ],
      [ "TriggerFactoryInternal", "classai_1_1shaman_1_1_trigger_factory_internal.html", "classai_1_1shaman_1_1_trigger_factory_internal" ],
      [ "AiObjectContextInternal", "classai_1_1shaman_1_1_ai_object_context_internal.html", "classai_1_1shaman_1_1_ai_object_context_internal" ]
    ] ],
    [ "ShamanAiObjectContext.h", "_shaman_ai_object_context_8h.html", [
      [ "ShamanAiObjectContext", "classai_1_1_shaman_ai_object_context.html", "classai_1_1_shaman_ai_object_context" ]
    ] ],
    [ "ShamanMultipliers.cpp", "_shaman_multipliers_8cpp.html", null ],
    [ "ShamanMultipliers.h", "_shaman_multipliers_8h.html", null ],
    [ "ShamanNonCombatStrategy.cpp", "_shaman_non_combat_strategy_8cpp.html", null ],
    [ "ShamanNonCombatStrategy.h", "_shaman_non_combat_strategy_8h.html", [
      [ "ShamanNonCombatStrategy", "classai_1_1_shaman_non_combat_strategy.html", "classai_1_1_shaman_non_combat_strategy" ]
    ] ],
    [ "ShamanTriggers.cpp", "_shaman_triggers_8cpp.html", null ],
    [ "ShamanTriggers.h", "_shaman_triggers_8h.html", [
      [ "ShamanWeaponTrigger", "classai_1_1_shaman_weapon_trigger.html", "classai_1_1_shaman_weapon_trigger" ],
      [ "TotemTrigger", "classai_1_1_totem_trigger.html", "classai_1_1_totem_trigger" ],
      [ "WindfuryTotemTrigger", "classai_1_1_windfury_totem_trigger.html", "classai_1_1_windfury_totem_trigger" ],
      [ "ManaSpringTotemTrigger", "classai_1_1_mana_spring_totem_trigger.html", "classai_1_1_mana_spring_totem_trigger" ],
      [ "FlametongueTotemTrigger", "classai_1_1_flametongue_totem_trigger.html", "classai_1_1_flametongue_totem_trigger" ],
      [ "StrengthOfEarthTotemTrigger", "classai_1_1_strength_of_earth_totem_trigger.html", "classai_1_1_strength_of_earth_totem_trigger" ],
      [ "MagmaTotemTrigger", "classai_1_1_magma_totem_trigger.html", "classai_1_1_magma_totem_trigger" ],
      [ "SearingTotemTrigger", "classai_1_1_searing_totem_trigger.html", "classai_1_1_searing_totem_trigger" ],
      [ "WindShearInterruptSpellTrigger", "classai_1_1_wind_shear_interrupt_spell_trigger.html", "classai_1_1_wind_shear_interrupt_spell_trigger" ],
      [ "WaterShieldTrigger", "classai_1_1_water_shield_trigger.html", "classai_1_1_water_shield_trigger" ],
      [ "LightningShieldTrigger", "classai_1_1_lightning_shield_trigger.html", "classai_1_1_lightning_shield_trigger" ],
      [ "PurgeTrigger", "classai_1_1_purge_trigger.html", "classai_1_1_purge_trigger" ],
      [ "WaterWalkingTrigger", "classai_1_1_water_walking_trigger.html", "classai_1_1_water_walking_trigger" ],
      [ "WaterBreathingTrigger", "classai_1_1_water_breathing_trigger.html", "classai_1_1_water_breathing_trigger" ],
      [ "WaterWalkingOnPartyTrigger", "classai_1_1_water_walking_on_party_trigger.html", "classai_1_1_water_walking_on_party_trigger" ],
      [ "WaterBreathingOnPartyTrigger", "classai_1_1_water_breathing_on_party_trigger.html", "classai_1_1_water_breathing_on_party_trigger" ],
      [ "CleanseSpiritPoisonTrigger", "classai_1_1_cleanse_spirit_poison_trigger.html", "classai_1_1_cleanse_spirit_poison_trigger" ],
      [ "PartyMemberCleanseSpiritPoisonTrigger", "classai_1_1_party_member_cleanse_spirit_poison_trigger.html", "classai_1_1_party_member_cleanse_spirit_poison_trigger" ],
      [ "CleanseSpiritCurseTrigger", "classai_1_1_cleanse_spirit_curse_trigger.html", "classai_1_1_cleanse_spirit_curse_trigger" ],
      [ "PartyMemberCleanseSpiritCurseTrigger", "classai_1_1_party_member_cleanse_spirit_curse_trigger.html", "classai_1_1_party_member_cleanse_spirit_curse_trigger" ],
      [ "CleanseSpiritDiseaseTrigger", "classai_1_1_cleanse_spirit_disease_trigger.html", "classai_1_1_cleanse_spirit_disease_trigger" ],
      [ "PartyMemberCleanseSpiritDiseaseTrigger", "classai_1_1_party_member_cleanse_spirit_disease_trigger.html", "classai_1_1_party_member_cleanse_spirit_disease_trigger" ],
      [ "ShockTrigger", "classai_1_1_shock_trigger.html", "classai_1_1_shock_trigger" ],
      [ "FrostShockSnareTrigger", "classai_1_1_frost_shock_snare_trigger.html", "classai_1_1_frost_shock_snare_trigger" ],
      [ "HeroismTrigger", "classai_1_1_heroism_trigger.html", "classai_1_1_heroism_trigger" ],
      [ "BloodlustTrigger", "classai_1_1_bloodlust_trigger.html", "classai_1_1_bloodlust_trigger" ],
      [ "MaelstromWeaponTrigger", "classai_1_1_maelstrom_weapon_trigger.html", "classai_1_1_maelstrom_weapon_trigger" ],
      [ "WindShearInterruptEnemyHealerSpellTrigger", "classai_1_1_wind_shear_interrupt_enemy_healer_spell_trigger.html", "classai_1_1_wind_shear_interrupt_enemy_healer_spell_trigger" ]
    ] ],
    [ "TotemsShamanStrategy.cpp", "_totems_shaman_strategy_8cpp.html", null ],
    [ "TotemsShamanStrategy.h", "_totems_shaman_strategy_8h.html", [
      [ "TotemsShamanStrategy", "classai_1_1_totems_shaman_strategy.html", "classai_1_1_totems_shaman_strategy" ]
    ] ]
];