var _guild_8h =
[
    [ "GuildEventLogEntry", "struct_guild_event_log_entry.html", "struct_guild_event_log_entry" ],
    [ "MemberSlot", "struct_member_slot.html", "struct_member_slot" ],
    [ "RankInfo", "struct_rank_info.html", "struct_rank_info" ],
    [ "Guild", "class_guild.html", "class_guild" ],
    [ "GUILD_RANK_NONE", "_guild_8h.html#a81853da1a5c95815f2f1356ec9daa432", null ],
    [ "GUILD_RANKS_MAX_COUNT", "_guild_8h.html#a23789ae244454d7f56b055daa4bd6687", null ],
    [ "GUILD_RANKS_MIN_COUNT", "_guild_8h.html#a731fad16c34e163d19cf24d26faf0f57", null ],
    [ "CommandErrors", "_guild_8h.html#af4fd2b26207f5d30218890c7fc136744", [
      [ "ERR_PLAYER_NO_MORE_IN_GUILD", "_guild_8h.html#af4fd2b26207f5d30218890c7fc136744ad708e4b339836562080f733cf8d63d41", null ],
      [ "ERR_GUILD_INTERNAL", "_guild_8h.html#af4fd2b26207f5d30218890c7fc136744a8905fc6aff78718328bddfea91f8c880", null ],
      [ "ERR_ALREADY_IN_GUILD", "_guild_8h.html#af4fd2b26207f5d30218890c7fc136744aba29a04e7ed3817d55f075ed17acb297", null ],
      [ "ERR_ALREADY_IN_GUILD_S", "_guild_8h.html#af4fd2b26207f5d30218890c7fc136744a41d76fbbe087af601af21c19107330f4", null ],
      [ "ERR_INVITED_TO_GUILD", "_guild_8h.html#af4fd2b26207f5d30218890c7fc136744a71d0699daafed40ee8881fcb019a8e92", null ],
      [ "ERR_ALREADY_INVITED_TO_GUILD_S", "_guild_8h.html#af4fd2b26207f5d30218890c7fc136744acbe2f5ad90080b52ffbcc218810a316e", null ],
      [ "ERR_GUILD_NAME_INVALID", "_guild_8h.html#af4fd2b26207f5d30218890c7fc136744ae1e9b2619d7b16d241fbe4a59ee28ea5", null ],
      [ "ERR_GUILD_NAME_EXISTS_S", "_guild_8h.html#af4fd2b26207f5d30218890c7fc136744ab3ec7a09a685067777689f69d02c8dd0", null ],
      [ "ERR_GUILD_LEADER_LEAVE", "_guild_8h.html#af4fd2b26207f5d30218890c7fc136744a6506724d2f8b0fdb419acc3d201094f9", null ],
      [ "ERR_GUILD_PERMISSIONS", "_guild_8h.html#af4fd2b26207f5d30218890c7fc136744a12d6d5351242de2172f3c77069966373", null ],
      [ "ERR_GUILD_PLAYER_NOT_IN_GUILD", "_guild_8h.html#af4fd2b26207f5d30218890c7fc136744a4c1419689cc7acff5457878d6593deb8", null ],
      [ "ERR_GUILD_PLAYER_NOT_IN_GUILD_S", "_guild_8h.html#af4fd2b26207f5d30218890c7fc136744a5918718bb3ee88fe8a6133b9363ab634", null ],
      [ "ERR_GUILD_PLAYER_NOT_FOUND_S", "_guild_8h.html#af4fd2b26207f5d30218890c7fc136744a9360ca5ff657b8d7078acd51d586817c", null ],
      [ "ERR_GUILD_NOT_ALLIED", "_guild_8h.html#af4fd2b26207f5d30218890c7fc136744a8127617140b8da3540ee84783639f6c5", null ],
      [ "ERR_GUILD_RANK_TOO_HIGH_S", "_guild_8h.html#af4fd2b26207f5d30218890c7fc136744ac58f3c26d979598e350e1428300d0d68", null ],
      [ "ERR_GUILD_RANK_TOO_LOW_S", "_guild_8h.html#af4fd2b26207f5d30218890c7fc136744a93782a71094ed3ce9186034266661ffa", null ],
      [ "ERR_GUILD_RANKS_LOCKED", "_guild_8h.html#af4fd2b26207f5d30218890c7fc136744a722ac0b4b0b929b19cfd1c2f175d5bc2", null ],
      [ "ERR_GUILD_RANK_IN_USE", "_guild_8h.html#af4fd2b26207f5d30218890c7fc136744a6336fb11258178c0a9fcd0226139d854", null ],
      [ "ERR_GUILD_IGNORING_YOU_S", "_guild_8h.html#af4fd2b26207f5d30218890c7fc136744aa94f9be1954e58a003f5214838637044", null ],
      [ "ERR_GUILD_UNK20", "_guild_8h.html#af4fd2b26207f5d30218890c7fc136744a194d6c05df422d66b809a391025ab9a5", null ]
    ] ],
    [ "GuildDefaultRanks", "_guild_8h.html#a6534fcf172d7fbe4a762e0479601acd4", [
      [ "GR_GUILDMASTER", "_guild_8h.html#a6534fcf172d7fbe4a762e0479601acd4a349203b98139f80965ab3d7e3379c36d", null ],
      [ "GR_OFFICER", "_guild_8h.html#a6534fcf172d7fbe4a762e0479601acd4ab167d6658cd49c0909c6f435a7222b70", null ],
      [ "GR_VETERAN", "_guild_8h.html#a6534fcf172d7fbe4a762e0479601acd4ab18e872c32182264beb6659c203f4edd", null ],
      [ "GR_MEMBER", "_guild_8h.html#a6534fcf172d7fbe4a762e0479601acd4a2308a5860bcf26ee1f9e2e2cb84e3fd7", null ],
      [ "GR_INITIATE", "_guild_8h.html#a6534fcf172d7fbe4a762e0479601acd4a92f7ee434ade92ce3d0811d99d590ed5", null ]
    ] ],
    [ "GuildEmblem", "_guild_8h.html#a1b15db2ebc7408bd91ae195834c23568", [
      [ "ERR_GUILDEMBLEM_SUCCESS", "_guild_8h.html#a1b15db2ebc7408bd91ae195834c23568a862afc12072cb607f89b65dff6a4e1d0", null ],
      [ "ERR_GUILDEMBLEM_INVALID_TABARD_COLORS", "_guild_8h.html#a1b15db2ebc7408bd91ae195834c23568a438f7a4bf30acc47976689d07f397137", null ],
      [ "ERR_GUILDEMBLEM_NOGUILD", "_guild_8h.html#a1b15db2ebc7408bd91ae195834c23568aada1bda618d5cd4e62b78ead670f1214", null ],
      [ "ERR_GUILDEMBLEM_NOTGUILDMASTER", "_guild_8h.html#a1b15db2ebc7408bd91ae195834c23568a043b58ec4131734c7e45302a31f521db", null ],
      [ "ERR_GUILDEMBLEM_NOTENOUGHMONEY", "_guild_8h.html#a1b15db2ebc7408bd91ae195834c23568a81544caf6fd0c4f02da37f973178bdd0", null ],
      [ "ERR_GUILDEMBLEM_FAIL_NO_MESSAGE", "_guild_8h.html#a1b15db2ebc7408bd91ae195834c23568a11076194b40069e8160ade220c71cb7c", null ]
    ] ],
    [ "GuildEventLogTypes", "_guild_8h.html#aff19569a84ef44eea2acf4d6bcb86690", [
      [ "GUILD_EVENT_LOG_INVITE_PLAYER", "_guild_8h.html#aff19569a84ef44eea2acf4d6bcb86690a9a9cfccd8804c03c87ce7749d0eebd54", null ],
      [ "GUILD_EVENT_LOG_JOIN_GUILD", "_guild_8h.html#aff19569a84ef44eea2acf4d6bcb86690ae5477237a42236638c4c00f45b99ad8c", null ],
      [ "GUILD_EVENT_LOG_PROMOTE_PLAYER", "_guild_8h.html#aff19569a84ef44eea2acf4d6bcb86690a76587e8e52ae2fb7918f62b5542fe7d4", null ],
      [ "GUILD_EVENT_LOG_DEMOTE_PLAYER", "_guild_8h.html#aff19569a84ef44eea2acf4d6bcb86690abe7cbc394621d334e2933aa98d103434", null ],
      [ "GUILD_EVENT_LOG_UNINVITE_PLAYER", "_guild_8h.html#aff19569a84ef44eea2acf4d6bcb86690a7cf29d925b03cbedbf6f4a55b6a4db0e", null ],
      [ "GUILD_EVENT_LOG_LEAVE_GUILD", "_guild_8h.html#aff19569a84ef44eea2acf4d6bcb86690a0ec79d8832cc2bf0c1a715d8f702faff", null ]
    ] ],
    [ "GuildEvents", "_guild_8h.html#a06a0aa0a30a110150b9f76a3be9ee59d", [
      [ "GE_PROMOTION", "_guild_8h.html#a06a0aa0a30a110150b9f76a3be9ee59da72da68638000a7f33a2d5cba84f4a06f", null ],
      [ "GE_DEMOTION", "_guild_8h.html#a06a0aa0a30a110150b9f76a3be9ee59dae1bf3924ca8430f489b567e959cf63d2", null ],
      [ "GE_MOTD", "_guild_8h.html#a06a0aa0a30a110150b9f76a3be9ee59daab7edbe172dab4cd9f3a08e9d735421a", null ],
      [ "GE_JOINED", "_guild_8h.html#a06a0aa0a30a110150b9f76a3be9ee59da6649e0fbda75bed2ed56edaa163abd4d", null ],
      [ "GE_LEFT", "_guild_8h.html#a06a0aa0a30a110150b9f76a3be9ee59da0e9ffb6d5c2c9909a0eca1ede00ee816", null ],
      [ "GE_REMOVED", "_guild_8h.html#a06a0aa0a30a110150b9f76a3be9ee59da50fdd818113a0fffcd2ba9dc3bc29782", null ],
      [ "GE_LEADER_IS", "_guild_8h.html#a06a0aa0a30a110150b9f76a3be9ee59da25d37eb4d886fabe2c8495b238488a48", null ],
      [ "GE_LEADER_CHANGED", "_guild_8h.html#a06a0aa0a30a110150b9f76a3be9ee59dad4ba70858e9fb573d9bbd3b308ac6b54", null ],
      [ "GE_DISBANDED", "_guild_8h.html#a06a0aa0a30a110150b9f76a3be9ee59da9ef80195ef0577e240aa857f743ff2a0", null ],
      [ "GE_TABARDCHANGE", "_guild_8h.html#a06a0aa0a30a110150b9f76a3be9ee59da45306f2f4d9a144a27d85dd6f4d54911", null ],
      [ "GE_UNK1", "_guild_8h.html#a06a0aa0a30a110150b9f76a3be9ee59daf2c6156a99582537d7c84f60ca0d636f", null ],
      [ "GE_UNK2", "_guild_8h.html#a06a0aa0a30a110150b9f76a3be9ee59da55aab08c27b4422370909fc592b514d9", null ],
      [ "GE_SIGNED_ON", "_guild_8h.html#a06a0aa0a30a110150b9f76a3be9ee59dae1b52b8ecf858eb756e92de351ffecf4", null ],
      [ "GE_SIGNED_OFF", "_guild_8h.html#a06a0aa0a30a110150b9f76a3be9ee59daa42cdde19a25a9a63f9b3239641ff605", null ]
    ] ],
    [ "GuildRankRights", "_guild_8h.html#abe65a5c09114479eec37ae55f24aaaa7", [
      [ "GR_RIGHT_EMPTY", "_guild_8h.html#abe65a5c09114479eec37ae55f24aaaa7a0aaa2fbc124920eccb1f1495dc966aab", null ],
      [ "GR_RIGHT_GCHATLISTEN", "_guild_8h.html#abe65a5c09114479eec37ae55f24aaaa7a613b1d285b8e4bfd5b302d455a234e8b", null ],
      [ "GR_RIGHT_GCHATSPEAK", "_guild_8h.html#abe65a5c09114479eec37ae55f24aaaa7a31d51868ba86650a2177d9a9e55fb21e", null ],
      [ "GR_RIGHT_OFFCHATLISTEN", "_guild_8h.html#abe65a5c09114479eec37ae55f24aaaa7a5e1a484f0ba139d1a81e4ee542d91a9f", null ],
      [ "GR_RIGHT_OFFCHATSPEAK", "_guild_8h.html#abe65a5c09114479eec37ae55f24aaaa7a448d0ff1b08f751a9339dff1cd8b8e74", null ],
      [ "GR_RIGHT_PROMOTE", "_guild_8h.html#abe65a5c09114479eec37ae55f24aaaa7af89d8390d741e7b44ac5e8a3b253f6d8", null ],
      [ "GR_RIGHT_DEMOTE", "_guild_8h.html#abe65a5c09114479eec37ae55f24aaaa7af1b7986e010de928a797b01027a54281", null ],
      [ "GR_RIGHT_INVITE", "_guild_8h.html#abe65a5c09114479eec37ae55f24aaaa7adef53b5c69909e7c695d9237707c0da7", null ],
      [ "GR_RIGHT_REMOVE", "_guild_8h.html#abe65a5c09114479eec37ae55f24aaaa7aaa7ecd3d86acfd15c0da455338af9249", null ],
      [ "GR_RIGHT_SETMOTD", "_guild_8h.html#abe65a5c09114479eec37ae55f24aaaa7a7733a2d4555e5b4a35031df77c9ef5d8", null ],
      [ "GR_RIGHT_EPNOTE", "_guild_8h.html#abe65a5c09114479eec37ae55f24aaaa7a52e417f2751e39a4ec3e1315b0b3d532", null ],
      [ "GR_RIGHT_VIEWOFFNOTE", "_guild_8h.html#abe65a5c09114479eec37ae55f24aaaa7a9cee3982eb7862d62e7f00414c4703be", null ],
      [ "GR_RIGHT_EOFFNOTE", "_guild_8h.html#abe65a5c09114479eec37ae55f24aaaa7a2f3962665bd6827bf747293a361af469", null ],
      [ "GR_RIGHT_MODIFY_GUILD_INFO", "_guild_8h.html#abe65a5c09114479eec37ae55f24aaaa7a966fe8d17446997c93187cadac659f7e", null ],
      [ "GR_RIGHT_ALL", "_guild_8h.html#abe65a5c09114479eec37ae55f24aaaa7a4848a44a3683ba2200a6e6844dfcadee", null ]
    ] ],
    [ "PetitionSigns", "_guild_8h.html#a55e7b8d0bd00c4163cc1e3fa0b922dbc", [
      [ "PETITION_SIGN_OK", "_guild_8h.html#a55e7b8d0bd00c4163cc1e3fa0b922dbca5ce4a6092a209fa78694bbf846e8057f", null ],
      [ "PETITION_SIGN_ALREADY_SIGNED", "_guild_8h.html#a55e7b8d0bd00c4163cc1e3fa0b922dbca1687c7c5f4f3e28259bec2dd416543fc", null ],
      [ "PETITION_SIGN_ALREADY_IN_GUILD", "_guild_8h.html#a55e7b8d0bd00c4163cc1e3fa0b922dbca2bca52318633fc4ff08518ccb69f3fd2", null ],
      [ "PETITION_SIGN_CANT_SIGN_OWN", "_guild_8h.html#a55e7b8d0bd00c4163cc1e3fa0b922dbca52ade0f152f12607294a541cf2484625", null ],
      [ "PETITION_SIGN_NEED_MORE", "_guild_8h.html#a55e7b8d0bd00c4163cc1e3fa0b922dbcaf8cdb31b4c7d68c9355005c3911e1c8b", null ],
      [ "PETITION_SIGN_NOT_SERVER", "_guild_8h.html#a55e7b8d0bd00c4163cc1e3fa0b922dbcadd3f9a7e2db2c44c0d51801c83c45ab5", null ]
    ] ],
    [ "Typecommand", "_guild_8h.html#a523a552e936d34f9cc1ec0924e423e2c", [
      [ "GUILD_CREATE_S", "_guild_8h.html#a523a552e936d34f9cc1ec0924e423e2caa405b290db9afc354ee4782a9d72e39c", null ],
      [ "GUILD_INVITE_S", "_guild_8h.html#a523a552e936d34f9cc1ec0924e423e2ca10bb3eff65e47aa3839c69dddd5a4b2b", null ],
      [ "GUILD_QUIT_S", "_guild_8h.html#a523a552e936d34f9cc1ec0924e423e2ca0b2c6725eefa9961630ff763591d385d", null ],
      [ "GUILD_FOUNDER_S", "_guild_8h.html#a523a552e936d34f9cc1ec0924e423e2ca72bd027465ef5ed93c013030d67090ad", null ],
      [ "GUILD_UNK19", "_guild_8h.html#a523a552e936d34f9cc1ec0924e423e2ca8c8f2f7a15f8c72015f49777611e3f31", null ],
      [ "GUILD_UNK20", "_guild_8h.html#a523a552e936d34f9cc1ec0924e423e2ca60171c9c01f348cafede56b517fe0c2d", null ]
    ] ]
];