var _loot_object_stack_8h =
[
    [ "LootObject", "classai_1_1_loot_object.html", "classai_1_1_loot_object" ],
    [ "LootTarget", "classai_1_1_loot_target.html", "classai_1_1_loot_target" ],
    [ "LootTargetList", "classai_1_1_loot_target_list.html", "classai_1_1_loot_target_list" ],
    [ "LootObjectStack", "classai_1_1_loot_object_stack.html", "classai_1_1_loot_object_stack" ],
    [ "LootStrategy", "_loot_object_stack_8h.html#ac4473ef89d8b292f18779ebdc322f4e5", [
      [ "LOOTSTRATEGY_QUEST", "_loot_object_stack_8h.html#ac4473ef89d8b292f18779ebdc322f4e5a5c7f75b75026ab72cfcbdcef9f351284", null ],
      [ "LOOTSTRATEGY_SKILL", "_loot_object_stack_8h.html#ac4473ef89d8b292f18779ebdc322f4e5a9742ce385ac5d33c7c55fe6dd49fb208", null ],
      [ "LOOTSTRATEGY_GRAY", "_loot_object_stack_8h.html#ac4473ef89d8b292f18779ebdc322f4e5a2c014b833dc9f3c27c26d59846779a92", null ],
      [ "LOOTSTRATEGY_NORMAL", "_loot_object_stack_8h.html#ac4473ef89d8b292f18779ebdc322f4e5afcb6dca04fc89da02c9f8b6a808b7eb0", null ],
      [ "LOOTSTRATEGY_ALL", "_loot_object_stack_8h.html#ac4473ef89d8b292f18779ebdc322f4e5a9021de56fbecdbd426821f9d678b8f59", null ]
    ] ]
];