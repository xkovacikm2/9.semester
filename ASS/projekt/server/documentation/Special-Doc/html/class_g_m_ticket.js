var class_g_m_ticket =
[
    [ "GMTicket", "class_g_m_ticket.html#a812622dab681787bbdee9492ee5fde47", null ],
    [ "Close", "class_g_m_ticket.html#af12d92c607c1117308b8e7a38314fb3d", null ],
    [ "CloseByClient", "class_g_m_ticket.html#a109f05f6bb61ae426e33ccc8adeeec0b", null ],
    [ "CloseWithSurvey", "class_g_m_ticket.html#aa685929595ac818405f7f699ae4021e8", null ],
    [ "GetId", "class_g_m_ticket.html#aac6e49a3ab904264fe5577c0c7e51edb", null ],
    [ "GetLastUpdate", "class_g_m_ticket.html#ab5e256ff8404acc7a46ef42ef38534d8", null ],
    [ "GetPlayerGuid", "class_g_m_ticket.html#ab60537cb5db16eeb5667637034b6208d", null ],
    [ "GetResponse", "class_g_m_ticket.html#aa2780a46b1c7005ebd1606e6e3729620", null ],
    [ "GetText", "class_g_m_ticket.html#a9bf4820f84101e50169b0aa2d9844cab", null ],
    [ "HasResponse", "class_g_m_ticket.html#ac675c8548e9a4342bcd7973db1565cd3", null ],
    [ "Init", "class_g_m_ticket.html#acc1f286fc99769808f2b30b0ecb1c98b", null ],
    [ "SaveSurveyData", "class_g_m_ticket.html#a1de6acb62663224dedd07d8867721bb1", null ],
    [ "SetResponseText", "class_g_m_ticket.html#ad409a9fa9025700cef4a74710cda35ae", null ],
    [ "SetText", "class_g_m_ticket.html#ad5837071e9fe2796f8eb6d705ea80a3e", null ]
];