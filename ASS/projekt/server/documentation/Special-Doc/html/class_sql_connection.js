var class_sql_connection =
[
    [ "Lock", "class_sql_connection_1_1_lock.html", "class_sql_connection_1_1_lock" ],
    [ "~SqlConnection", "class_sql_connection.html#afecbefc8547bc255a9ab483ac74a50e8", null ],
    [ "SqlConnection", "class_sql_connection.html#affe22133a5d0749f478346b31e61de6f", null ],
    [ "BeginTransaction", "class_sql_connection.html#a8711530c28a2873c72323c792ccbb200", null ],
    [ "CommitTransaction", "class_sql_connection.html#a826e876465eef1cbdb099689bc4d642c", null ],
    [ "CreateStatement", "class_sql_connection.html#ac659acb48054fda6965abd718570f61c", null ],
    [ "DB", "class_sql_connection.html#a249c3ad129c1b8e6b850d6ef33cd495f", null ],
    [ "escape_string", "class_sql_connection.html#ae44ec51e1721a0fd2540bdb90b059651", null ],
    [ "Execute", "class_sql_connection.html#a0bee668558aeaf11610bde67de71e85f", null ],
    [ "ExecuteStmt", "class_sql_connection.html#a2c5de54855291515dbdfefac8741f67b", null ],
    [ "FreePreparedStatements", "class_sql_connection.html#ad7687b6bcdd78be27b189a22b792720b", null ],
    [ "GetStmt", "class_sql_connection.html#af1621a12224a1ab65fa172f69b06c770", null ],
    [ "Initialize", "class_sql_connection.html#aa8a902da410a16d72ea106adff47d7de", null ],
    [ "Query", "class_sql_connection.html#ad11061c8472ca191fd41b41ba6f509c7", null ],
    [ "QueryNamed", "class_sql_connection.html#aea3fc7b077aceeed175ff2fd180814a3", null ],
    [ "RollbackTransaction", "class_sql_connection.html#aad6f18270deacdddd4042ee98de7e336", null ],
    [ "m_db", "class_sql_connection.html#a6345bb926f304947b56f536ad75118f6", null ]
];