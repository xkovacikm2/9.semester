var dir_62566da35c34ff91fbff11e5d04fde87 =
[
    [ "strategy", "dir_812f219d4475b8ebec2ebdaa79d3ed3e.html", "dir_812f219d4475b8ebec2ebdaa79d3ed3e" ],
    [ "AiFactory.cpp", "_ai_factory_8cpp.html", null ],
    [ "AiFactory.h", "_ai_factory_8h.html", [
      [ "AiFactory", "class_ai_factory.html", null ]
    ] ],
    [ "ChatFilter.cpp", "_chat_filter_8cpp.html", [
      [ "StrategyChatFilter", "class_strategy_chat_filter.html", "class_strategy_chat_filter" ],
      [ "LevelChatFilter", "class_level_chat_filter.html", "class_level_chat_filter" ],
      [ "CombatTypeChatFilter", "class_combat_type_chat_filter.html", "class_combat_type_chat_filter" ],
      [ "RtiChatFilter", "class_rti_chat_filter.html", "class_rti_chat_filter" ],
      [ "ClassChatFilter", "class_class_chat_filter.html", "class_class_chat_filter" ]
    ] ],
    [ "ChatFilter.h", "_chat_filter_8h.html", [
      [ "ChatFilter", "classai_1_1_chat_filter.html", "classai_1_1_chat_filter" ],
      [ "CompositeChatFilter", "classai_1_1_composite_chat_filter.html", "classai_1_1_composite_chat_filter" ]
    ] ],
    [ "ChatHelper.cpp", "_chat_helper_8cpp.html", null ],
    [ "ChatHelper.h", "_chat_helper_8h.html", "_chat_helper_8h" ],
    [ "FleeManager.cpp", "_flee_manager_8cpp.html", null ],
    [ "FleeManager.h", "_flee_manager_8h.html", [
      [ "RangePair", "classai_1_1_range_pair.html", "classai_1_1_range_pair" ],
      [ "FleePoint", "classai_1_1_flee_point.html", "classai_1_1_flee_point" ],
      [ "FleeManager", "classai_1_1_flee_manager.html", "classai_1_1_flee_manager" ]
    ] ],
    [ "Helpers.cpp", "_helpers_8cpp.html", "_helpers_8cpp" ],
    [ "LazyCalculatedValue.h", "_lazy_calculated_value_8h.html", [
      [ "LazyCalculatedValue", "classai_1_1_lazy_calculated_value.html", "classai_1_1_lazy_calculated_value" ]
    ] ],
    [ "LootObjectStack.cpp", "_loot_object_stack_8cpp.html", "_loot_object_stack_8cpp" ],
    [ "LootObjectStack.h", "_loot_object_stack_8h.html", "_loot_object_stack_8h" ],
    [ "playerbot.h", "playerbot_8h.html", "playerbot_8h" ],
    [ "PlayerbotAI.cpp", "_playerbot_a_i_8cpp.html", "_playerbot_a_i_8cpp" ],
    [ "PlayerbotAI.h", "_playerbot_a_i_8h.html", "_playerbot_a_i_8h" ],
    [ "PlayerbotAIAware.h", "_playerbot_a_i_aware_8h.html", [
      [ "PlayerbotAIAware", "classai_1_1_playerbot_a_i_aware.html", "classai_1_1_playerbot_a_i_aware" ]
    ] ],
    [ "PlayerbotAIBase.cpp", "_playerbot_a_i_base_8cpp.html", null ],
    [ "PlayerbotAIBase.h", "_playerbot_a_i_base_8h.html", [
      [ "PlayerbotAIBase", "class_playerbot_a_i_base.html", "class_playerbot_a_i_base" ]
    ] ],
    [ "PlayerbotAIConfig.cpp", "_playerbot_a_i_config_8cpp.html", "_playerbot_a_i_config_8cpp" ],
    [ "PlayerbotAIConfig.h", "_playerbot_a_i_config_8h.html", "_playerbot_a_i_config_8h" ],
    [ "playerbotDefs.h", "playerbot_defs_8h.html", null ],
    [ "PlayerbotFactory.cpp", "_playerbot_factory_8cpp.html", [
      [ "DestroyItemsVisitor", "class_destroy_items_visitor.html", "class_destroy_items_visitor" ]
    ] ],
    [ "PlayerbotFactory.h", "_playerbot_factory_8h.html", [
      [ "PlayerbotFactory", "class_playerbot_factory.html", "class_playerbot_factory" ]
    ] ],
    [ "PlayerbotMgr.cpp", "_playerbot_mgr_8cpp.html", null ],
    [ "PlayerbotMgr.h", "_playerbot_mgr_8h.html", "_playerbot_mgr_8h" ],
    [ "PlayerbotSecurity.cpp", "_playerbot_security_8cpp.html", null ],
    [ "PlayerbotSecurity.h", "_playerbot_security_8h.html", "_playerbot_security_8h" ],
    [ "RandomPlayerbotFactory.cpp", "_random_playerbot_factory_8cpp.html", null ],
    [ "RandomPlayerbotFactory.h", "_random_playerbot_factory_8h.html", [
      [ "RandomPlayerbotFactory", "class_random_playerbot_factory.html", "class_random_playerbot_factory" ]
    ] ],
    [ "RandomPlayerbotMgr.cpp", "_random_playerbot_mgr_8cpp.html", "_random_playerbot_mgr_8cpp" ],
    [ "RandomPlayerbotMgr.h", "_random_playerbot_mgr_8h.html", "_random_playerbot_mgr_8h" ]
];