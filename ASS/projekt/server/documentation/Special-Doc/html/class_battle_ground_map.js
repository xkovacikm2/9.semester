var class_battle_ground_map =
[
    [ "BattleGroundMap", "class_battle_ground_map.html#a0df8391f762fd4e2c1086999a5aa8d96", null ],
    [ "~BattleGroundMap", "class_battle_ground_map.html#a00858bf30c9eb609bc1b6b51dfb5ebbc", null ],
    [ "Add", "class_battle_ground_map.html#a1677599508b7243a579b4eaae1f3b5a2", null ],
    [ "CanEnter", "class_battle_ground_map.html#ad81c5c3f474b59413b56bd5d73dc208f", null ],
    [ "GetBG", "class_battle_ground_map.html#a3bec70aece18329d75c5b14f93f34766", null ],
    [ "GetPersistanceState", "class_battle_ground_map.html#a16bba67b2281ff87ecc04023e1a485dc", null ],
    [ "GetScriptId", "class_battle_ground_map.html#a6b93a9bffd09093449d43b00ff6cae18", null ],
    [ "InitVisibilityDistance", "class_battle_ground_map.html#a9c89c387915281da06312ac9c0d94bd7", null ],
    [ "Remove", "class_battle_ground_map.html#a493ed36e8e42271e413cbe03e8aa11b0", null ],
    [ "SetBG", "class_battle_ground_map.html#a0c601e457e7a1007220d7d88fca8b5c1", null ],
    [ "SetUnload", "class_battle_ground_map.html#af960735455d030183f843a4c587b8037", null ],
    [ "UnloadAll", "class_battle_ground_map.html#a34b7af79a1157bc6bb1c107c1e48aabd", null ],
    [ "Update", "class_battle_ground_map.html#a1ee0acf038aaf24fb9d9c316c1a3b73f", null ]
];