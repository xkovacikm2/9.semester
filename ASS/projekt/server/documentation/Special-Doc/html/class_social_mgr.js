var class_social_mgr =
[
    [ "SocialMgr", "class_social_mgr.html#a3cff8262ce10d2e8c8c38dc834230545", null ],
    [ "~SocialMgr", "class_social_mgr.html#aef859e6c923a6ba4bb7ac0049f17463d", null ],
    [ "BroadcastToFriendListers", "class_social_mgr.html#a55b35ea17ebfea4ff31710186b87865f", null ],
    [ "GetFriendInfo", "class_social_mgr.html#a8566b501cbbd1a0a67d2291b19fd13f9", null ],
    [ "LoadFromDB", "class_social_mgr.html#aa54411704a4d1320611eca12747b3253", null ],
    [ "MakeFriendStatusPacket", "class_social_mgr.html#aa91c04640649648f2e9b96cd2f43bbb2", null ],
    [ "RemovePlayerSocial", "class_social_mgr.html#a837874c9e19252ea5920ae27f5640df1", null ],
    [ "SendFriendStatus", "class_social_mgr.html#a8824263f601c107ef84e487ca1b71bff", null ]
];