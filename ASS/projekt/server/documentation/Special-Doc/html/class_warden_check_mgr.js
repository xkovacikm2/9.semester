var class_warden_check_mgr =
[
    [ "GetWardenCheckIds", "class_warden_check_mgr.html#a830c0ba742f14354744423dd430b8c36", null ],
    [ "GetWardenDataById", "class_warden_check_mgr.html#a20d691e80d4607583f93ffbbf4b91df6", null ],
    [ "GetWardenResultById", "class_warden_check_mgr.html#abfd8b3e32f4691d42ef3d083e76ac1ef", null ],
    [ "LoadWardenChecks", "class_warden_check_mgr.html#ad02847547a06d98c582d09992dce9d11", null ],
    [ "LoadWardenOverrides", "class_warden_check_mgr.html#a0a82e118a8f8236fecf1b00de0e2071c", null ]
];