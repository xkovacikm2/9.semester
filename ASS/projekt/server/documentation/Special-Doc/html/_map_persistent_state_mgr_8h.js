var _map_persistent_state_mgr_8h =
[
    [ "MapCellObjectGuids", "struct_map_cell_object_guids.html", "struct_map_cell_object_guids" ],
    [ "MapPersistentState", "class_map_persistent_state.html", "class_map_persistent_state" ],
    [ "WorldPersistentState", "class_world_persistent_state.html", "class_world_persistent_state" ],
    [ "DungeonPersistentState", "class_dungeon_persistent_state.html", "class_dungeon_persistent_state" ],
    [ "BattleGroundPersistentState", "class_battle_ground_persistent_state.html", "class_battle_ground_persistent_state" ],
    [ "DungeonResetEvent", "struct_dungeon_reset_event.html", "struct_dungeon_reset_event" ],
    [ "DungeonResetScheduler", "class_dungeon_reset_scheduler.html", "class_dungeon_reset_scheduler" ],
    [ "MapPersistentStateManager", "class_map_persistent_state_manager.html", "class_map_persistent_state_manager" ],
    [ "MAX_RESET_EVENT_TYPE", "_map_persistent_state_mgr_8h.html#a0879886737bd6bd809130fca129cfeaf", null ],
    [ "sMapPersistentStateMgr", "_map_persistent_state_mgr_8h.html#a1f241a8cd8718e0856cdbbf764fb9549", null ],
    [ "CellGuidSet", "_map_persistent_state_mgr_8h.html#a0340f208223730c2c1b2e0c80455289e", null ],
    [ "MapCellObjectGuidsMap", "_map_persistent_state_mgr_8h.html#af8d3c37008052f7b4fe3711d65c96301", null ],
    [ "InstanceResetFailReason", "_map_persistent_state_mgr_8h.html#a64781e56718ff35a019e1feef1f4e730", [
      [ "INSTANCERESET_FAIL_GENERAL", "_map_persistent_state_mgr_8h.html#a64781e56718ff35a019e1feef1f4e730a3cb918c375e0883ff99bd3927dbd547c", null ],
      [ "INSTANCERESET_FAIL_OFFLINE", "_map_persistent_state_mgr_8h.html#a64781e56718ff35a019e1feef1f4e730a6401b5b77620cc36cf59138ca564a119", null ],
      [ "INSTANCERESET_FAIL_ZONING", "_map_persistent_state_mgr_8h.html#a64781e56718ff35a019e1feef1f4e730a2c5172535b50355a9b4744427b89318d", null ],
      [ "INSTANCERESET_FAIL_SILENTLY", "_map_persistent_state_mgr_8h.html#a64781e56718ff35a019e1feef1f4e730a1994168d84d7028e32940d485bfa9338", null ]
    ] ],
    [ "ResetEventType", "_map_persistent_state_mgr_8h.html#aa81f4a2587036d8ec779d3518bf6812d", [
      [ "RESET_EVENT_NORMAL_DUNGEON", "_map_persistent_state_mgr_8h.html#aa81f4a2587036d8ec779d3518bf6812daf0283fea5853f5202163b1ff6b76eca3", null ],
      [ "RESET_EVENT_INFORM_1", "_map_persistent_state_mgr_8h.html#aa81f4a2587036d8ec779d3518bf6812daab6edb17bbbeecb554bd35a5ae2a8af1", null ],
      [ "RESET_EVENT_INFORM_2", "_map_persistent_state_mgr_8h.html#aa81f4a2587036d8ec779d3518bf6812dafdf25c50ff76a8b4d7f1a35495c6288b", null ],
      [ "RESET_EVENT_INFORM_3", "_map_persistent_state_mgr_8h.html#aa81f4a2587036d8ec779d3518bf6812daf02f5c877339c52c6348fe87f5e4ca3a", null ],
      [ "RESET_EVENT_INFORM_LAST", "_map_persistent_state_mgr_8h.html#aa81f4a2587036d8ec779d3518bf6812dab8a8a4803c640de8071e913792f6381a", null ],
      [ "RESET_EVENT_FORCED_INFORM_1", "_map_persistent_state_mgr_8h.html#aa81f4a2587036d8ec779d3518bf6812daeb1f64f9b3eda3a3d0c19e4973096a60", null ],
      [ "RESET_EVENT_FORCED_INFORM_2", "_map_persistent_state_mgr_8h.html#aa81f4a2587036d8ec779d3518bf6812da6dd6c3bc82945ef06746bf77431c651a", null ],
      [ "RESET_EVENT_FORCED_INFORM_3", "_map_persistent_state_mgr_8h.html#aa81f4a2587036d8ec779d3518bf6812daa38dcdea97956e5634ecbf4f5cf70bbc", null ],
      [ "RESET_EVENT_FORCED_INFORM_LAST", "_map_persistent_state_mgr_8h.html#aa81f4a2587036d8ec779d3518bf6812da4ce0682e4adefcc51c6d2f527c58aa7c", null ]
    ] ]
];