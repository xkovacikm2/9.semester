var _v_map_manager2_8h =
[
    [ "ManagedModel", "class_v_m_a_p_1_1_managed_model.html", "class_v_m_a_p_1_1_managed_model" ],
    [ "VMapManager2", "class_v_m_a_p_1_1_v_map_manager2.html", "class_v_m_a_p_1_1_v_map_manager2" ],
    [ "FILENAMEBUFFER_SIZE", "_v_map_manager2_8h.html#aebe91d3f560a4221b135ef03b21a485f", null ],
    [ "MAP_FILENAME_EXTENSION2", "_v_map_manager2_8h.html#aad39c14e5fae9fd5141df154332ae6fd", null ],
    [ "InstanceTreeMap", "_v_map_manager2_8h.html#a61a2c6d4a5155c7b37ee0b54b81bccd4", null ],
    [ "ModelFileMap", "_v_map_manager2_8h.html#a7ac41653f01e4d106b969968245ca5a5", null ],
    [ "DisableTypes", "_v_map_manager2_8h.html#ae6fa86a61cff3bac928739b18ffb8c1b", [
      [ "VMAP_DISABLE_AREAFLAG", "_v_map_manager2_8h.html#ae6fa86a61cff3bac928739b18ffb8c1ba4fc4dc95e6df57c161e146d8da4b4a19", null ],
      [ "VMAP_DISABLE_HEIGHT", "_v_map_manager2_8h.html#ae6fa86a61cff3bac928739b18ffb8c1ba2f9e18e9b167ecc25202f607e29af926", null ],
      [ "VMAP_DISABLE_LOS", "_v_map_manager2_8h.html#ae6fa86a61cff3bac928739b18ffb8c1ba26f8933b26a6ce62d8bddafde8ebb108", null ],
      [ "VMAP_DISABLE_LIQUIDSTATUS", "_v_map_manager2_8h.html#ae6fa86a61cff3bac928739b18ffb8c1ba191004652b9fbcccb26c14ce81a94cec", null ]
    ] ]
];