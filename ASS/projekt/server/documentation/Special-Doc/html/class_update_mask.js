var class_update_mask =
[
    [ "ClientUpdateMaskType", "class_update_mask.html#a7638c508cfb0f5480eec2f9d650b0eb4", null ],
    [ "UpdateMaskCount", "class_update_mask.html#a667c2f210ee4a2417a2a3ba486d1332f", [
      [ "CLIENT_UPDATE_MASK_BITS", "class_update_mask.html#a667c2f210ee4a2417a2a3ba486d1332fac06e5fd027ae17cd4eaf7faa2580098b", null ]
    ] ],
    [ "UpdateMask", "class_update_mask.html#a84456333046da3f36f726ff54a7c13c8", null ],
    [ "UpdateMask", "class_update_mask.html#ae0cefda1282f8c29a6557e1c25e007de", null ],
    [ "~UpdateMask", "class_update_mask.html#a986a2b81e0b4f86486ccc455ff6ad79a", null ],
    [ "AppendToPacket", "class_update_mask.html#afe89109ca16f687072062408e311f793", null ],
    [ "Clear", "class_update_mask.html#a39cffd3822966011d83d7241ee3c5548", null ],
    [ "GetBit", "class_update_mask.html#a69f7c72de49699d5730f3a10d55f30e2", null ],
    [ "GetBlockCount", "class_update_mask.html#a5c0d2086346e4522e422add9622e1117", null ],
    [ "GetCount", "class_update_mask.html#a1f6e2b930533b303c173166bf3588613", null ],
    [ "operator &=", "class_update_mask.html#a79a6f2ddae159c11d177f407f723e7df", null ],
    [ "operator=", "class_update_mask.html#a47c8da9f3ebe924a4c8e63ade86f6c88", null ],
    [ "operator|", "class_update_mask.html#a487d62a6a039803da615d265a176f138", null ],
    [ "operator|=", "class_update_mask.html#ab8074107ff12f780973f388f7063b5c3", null ],
    [ "SetBit", "class_update_mask.html#af3390cfa31c39629968a871be2c763fc", null ],
    [ "SetCount", "class_update_mask.html#ad6ec4d5a2b6a85a2ec72b559efbdeb35", null ],
    [ "UnsetBit", "class_update_mask.html#aaac0596975c08d04736996c608eff29f", null ]
];