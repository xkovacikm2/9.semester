var struct_spell_rank_helper =
[
    [ "SpellRankHelper", "struct_spell_rank_helper.html#acada8bd0d391477a8bdfc59c55064341", null ],
    [ "FillHigherRanks", "struct_spell_rank_helper.html#af09375289bf8aa228e062f56d5df848a", null ],
    [ "RecordRank", "struct_spell_rank_helper.html#ae53f535aaf67ebbb048fb49e9feedb29", null ],
    [ "customRank", "struct_spell_rank_helper.html#ae70993f664dad15e4f8018c6e0937f0e", null ],
    [ "firstRankSpells", "struct_spell_rank_helper.html#a65b23cc5350d198dd7ce155842efa1b4", null ],
    [ "firstRankSpellsWithCustomRanks", "struct_spell_rank_helper.html#a289cc6461064615995aee9d5aa83d1d2", null ],
    [ "mgr", "struct_spell_rank_helper.html#a595ae682f491f3f880cede2d60390848", null ],
    [ "worker", "struct_spell_rank_helper.html#a785193628241474c3018820b1c6d072f", null ]
];