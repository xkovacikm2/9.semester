var _update_data_8h =
[
    [ "UpdateData", "class_update_data.html", "class_update_data" ],
    [ "ObjectUpdateFlags", "_update_data_8h.html#aca2f8502b06266b483a0d0280409fcdb", [
      [ "UPDATEFLAG_NONE", "_update_data_8h.html#aca2f8502b06266b483a0d0280409fcdbab3f4d8eafca6e04f6b445bedda939799", null ],
      [ "UPDATEFLAG_SELF", "_update_data_8h.html#aca2f8502b06266b483a0d0280409fcdbacb59b2f7a20c57e2dc11ad4a4a74195f", null ],
      [ "UPDATEFLAG_TRANSPORT", "_update_data_8h.html#aca2f8502b06266b483a0d0280409fcdba7b800b143915d7a9ea65020153cee3ca", null ],
      [ "UPDATEFLAG_FULLGUID", "_update_data_8h.html#aca2f8502b06266b483a0d0280409fcdba391091d73b2d47a6a8f75144bff27931", null ],
      [ "UPDATEFLAG_HIGHGUID", "_update_data_8h.html#aca2f8502b06266b483a0d0280409fcdba509d9019e3d069d0a3696451c21ed2e2", null ],
      [ "UPDATEFLAG_ALL", "_update_data_8h.html#aca2f8502b06266b483a0d0280409fcdba1e2183d7dce842eea2e5473ab5f85fa2", null ],
      [ "UPDATEFLAG_LIVING", "_update_data_8h.html#aca2f8502b06266b483a0d0280409fcdba0eedb2f7b118804bb3fe89fde52d7632", null ],
      [ "UPDATEFLAG_HAS_POSITION", "_update_data_8h.html#aca2f8502b06266b483a0d0280409fcdbac46d45c93cf6ea074da06b224936fcfc", null ]
    ] ],
    [ "ObjectUpdateType", "_update_data_8h.html#a38641959b0ec110c50ea41a10cb0a2f1", [
      [ "UPDATETYPE_VALUES", "_update_data_8h.html#a38641959b0ec110c50ea41a10cb0a2f1a1fc033f630eab16bbf5b7ce7938d7bef", null ],
      [ "UPDATETYPE_MOVEMENT", "_update_data_8h.html#a38641959b0ec110c50ea41a10cb0a2f1ac08838cdcba5218c53ea7745f5c0c370", null ],
      [ "UPDATETYPE_CREATE_OBJECT", "_update_data_8h.html#a38641959b0ec110c50ea41a10cb0a2f1a4dc60011a5f19dc6c7d8ef61f9b10e20", null ],
      [ "UPDATETYPE_CREATE_OBJECT2", "_update_data_8h.html#a38641959b0ec110c50ea41a10cb0a2f1af72ecfc1f4c6b5006f7985299cc2cd8f", null ],
      [ "UPDATETYPE_OUT_OF_RANGE_OBJECTS", "_update_data_8h.html#a38641959b0ec110c50ea41a10cb0a2f1a5e1317f5c975a9e3a19c37922f49e089", null ],
      [ "UPDATETYPE_NEAR_OBJECTS", "_update_data_8h.html#a38641959b0ec110c50ea41a10cb0a2f1a5017763e8c136985831c2ee1583795d5", null ]
    ] ]
];