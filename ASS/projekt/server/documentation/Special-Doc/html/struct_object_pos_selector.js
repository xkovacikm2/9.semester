var struct_object_pos_selector =
[
    [ "OccupiedArea", "struct_object_pos_selector_1_1_occupied_area.html", "struct_object_pos_selector_1_1_occupied_area" ],
    [ "UsedArea", "struct_object_pos_selector.html#ae2f0aa27ff8990fffc2a1fa8beeb9e26", null ],
    [ "UsedAreaList", "struct_object_pos_selector.html#a36d9b17b9651d820955cdd5a8dbad1ba", null ],
    [ "ObjectPosSelector", "struct_object_pos_selector.html#a827f509e4074adee592a01d2b74c64d9", null ],
    [ "AddUsedArea", "struct_object_pos_selector.html#a9d1e4e3c05f5aab23f03a808f6c2e37d", null ],
    [ "CheckAngle", "struct_object_pos_selector.html#a8f7f6433eb8de5b849988b965a60302a", null ],
    [ "CheckOriginalAngle", "struct_object_pos_selector.html#acbd8e59b3ef0ba863c4647cfbd0bed98", null ],
    [ "InitializeAngle", "struct_object_pos_selector.html#ac58836fb3671cf87b156072910b811c2", null ],
    [ "InitializeAngle", "struct_object_pos_selector.html#a5472e8b54ab8aadf72e5dfb90e88bbec", null ],
    [ "NextAngle", "struct_object_pos_selector.html#ae45835290af001233a7d76e1a5be6df0", null ],
    [ "NextSideAngle", "struct_object_pos_selector.html#a70b9812ad26f09eb56fe98c3bee054fd", null ],
    [ "NextUsedAngle", "struct_object_pos_selector.html#a779268da0c5b089c9752601543a0f856", null ],
    [ "m_centerX", "struct_object_pos_selector.html#a0d23545f60ed6e93689d463241df21b9", null ],
    [ "m_centerY", "struct_object_pos_selector.html#a23d7247f4f8804ac484a340f0bfc7836", null ],
    [ "m_nextUsedAreaItr", "struct_object_pos_selector.html#aae35577d2e3e6caa2ade888041f076d5", null ],
    [ "m_searchedForReqHAngle", "struct_object_pos_selector.html#a38eaeefc27f004de9822dff206847199", null ],
    [ "m_searcherDist", "struct_object_pos_selector.html#ae85c1635a2e9ca87606e701d1f874ae1", null ],
    [ "m_searchPosFor", "struct_object_pos_selector.html#a63dbce9e128c7c49dd35a37eae27e1fe", null ],
    [ "m_stepAngle", "struct_object_pos_selector.html#ae5d14ed3fa0cb75fbeb8a391299ce320", null ],
    [ "m_UsedAreaLists", "struct_object_pos_selector.html#a0d7ecfed7a87d88473d99954073a3626", null ]
];