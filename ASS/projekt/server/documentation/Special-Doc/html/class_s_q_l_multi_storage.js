var class_s_q_l_multi_storage =
[
    [ "SQLMSIteratorBounds", "class_s_q_l_multi_storage_1_1_s_q_l_m_s_iterator_bounds.html", "class_s_q_l_multi_storage_1_1_s_q_l_m_s_iterator_bounds" ],
    [ "SQLMultiSIterator", "class_s_q_l_multi_storage_1_1_s_q_l_multi_s_iterator.html", "class_s_q_l_multi_storage_1_1_s_q_l_multi_s_iterator" ],
    [ "SQLMultiStorage", "class_s_q_l_multi_storage.html#a8179f737d445dff15e90ba2a5f5ba469", null ],
    [ "SQLMultiStorage", "class_s_q_l_multi_storage.html#a910ed18f3aa5e19352dd983fb72f9eb1", null ],
    [ "~SQLMultiStorage", "class_s_q_l_multi_storage.html#a783a861a40c33bd496e27f1004a5e659", null ],
    [ "EraseEntry", "class_s_q_l_multi_storage.html#ac08a531e732971912b3c163f855d7a8c", null ],
    [ "Free", "class_s_q_l_multi_storage.html#a7e6e5e9405ebba231ef85a95e32d7857", null ],
    [ "getBounds", "class_s_q_l_multi_storage.html#a737fa961049b08dd90e981c45d40ff0b", null ],
    [ "JustCreatedRecord", "class_s_q_l_multi_storage.html#a19c918cf6388c68b0bb43ff478a6b2c7", null ],
    [ "Load", "class_s_q_l_multi_storage.html#a571c5f81a887818efec775c33c6a61a1", null ],
    [ "prepareToLoad", "class_s_q_l_multi_storage.html#a3d80d46899346bf0166845b079e61b6b", null ],
    [ "SQLMSIteratorBounds", "class_s_q_l_multi_storage.html#ac7e61a23a3e0dd2afa72dee9c606fcf7", null ],
    [ "SQLMultiSIterator", "class_s_q_l_multi_storage.html#abc85524c54cafe80b8652c1819fd461a", null ],
    [ "SQLStorageLoaderBase", "class_s_q_l_multi_storage.html#a1543906b0ecd4eb00fa6b84a4c8b45ca", null ]
];