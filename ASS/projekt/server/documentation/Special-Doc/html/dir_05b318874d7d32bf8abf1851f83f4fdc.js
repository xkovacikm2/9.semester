var dir_05b318874d7d32bf8abf1851f83f4fdc =
[
    [ "AttackEnemyPlayersStrategy.cpp", "_attack_enemy_players_strategy_8cpp.html", null ],
    [ "AttackEnemyPlayersStrategy.h", "_attack_enemy_players_strategy_8h.html", [
      [ "AttackEnemyPlayersStrategy", "classai_1_1_attack_enemy_players_strategy.html", "classai_1_1_attack_enemy_players_strategy" ]
    ] ],
    [ "AttackRtiStrategy.cpp", "_attack_rti_strategy_8cpp.html", null ],
    [ "AttackRtiStrategy.h", "_attack_rti_strategy_8h.html", [
      [ "AttackRtiStrategy", "classai_1_1_attack_rti_strategy.html", "classai_1_1_attack_rti_strategy" ]
    ] ],
    [ "AttackWeakStrategy.cpp", "_attack_weak_strategy_8cpp.html", null ],
    [ "AttackWeakStrategy.h", "_attack_weak_strategy_8h.html", [
      [ "AttackWeakStrategy", "classai_1_1_attack_weak_strategy.html", "classai_1_1_attack_weak_strategy" ]
    ] ],
    [ "CastTimeStrategy.cpp", "_cast_time_strategy_8cpp.html", null ],
    [ "CastTimeStrategy.h", "_cast_time_strategy_8h.html", [
      [ "CastTimeMultiplier", "classai_1_1_cast_time_multiplier.html", "classai_1_1_cast_time_multiplier" ],
      [ "CastTimeStrategy", "classai_1_1_cast_time_strategy.html", "classai_1_1_cast_time_strategy" ]
    ] ],
    [ "ChatCommandHandlerStrategy.cpp", "_chat_command_handler_strategy_8cpp.html", [
      [ "ChatCommandActionNodeFactoryInternal", "class_chat_command_action_node_factory_internal.html", "class_chat_command_action_node_factory_internal" ]
    ] ],
    [ "ChatCommandHandlerStrategy.h", "_chat_command_handler_strategy_8h.html", [
      [ "ChatCommandHandlerStrategy", "classai_1_1_chat_command_handler_strategy.html", "classai_1_1_chat_command_handler_strategy" ]
    ] ],
    [ "CombatStrategy.cpp", "_combat_strategy_8cpp.html", null ],
    [ "CombatStrategy.h", "_combat_strategy_8h.html", [
      [ "CombatStrategy", "classai_1_1_combat_strategy.html", "classai_1_1_combat_strategy" ]
    ] ],
    [ "ConserveManaStrategy.cpp", "_conserve_mana_strategy_8cpp.html", null ],
    [ "ConserveManaStrategy.h", "_conserve_mana_strategy_8h.html", [
      [ "ConserveManaMultiplier", "classai_1_1_conserve_mana_multiplier.html", "classai_1_1_conserve_mana_multiplier" ],
      [ "SaveManaMultiplier", "classai_1_1_save_mana_multiplier.html", "classai_1_1_save_mana_multiplier" ],
      [ "ConserveManaStrategy", "classai_1_1_conserve_mana_strategy.html", "classai_1_1_conserve_mana_strategy" ]
    ] ],
    [ "DeadStrategy.cpp", "_dead_strategy_8cpp.html", null ],
    [ "DeadStrategy.h", "_dead_strategy_8h.html", [
      [ "DeadStrategy", "classai_1_1_dead_strategy.html", "classai_1_1_dead_strategy" ]
    ] ],
    [ "DpsAoeStrategy.cpp", "_dps_aoe_strategy_8cpp.html", null ],
    [ "DpsAoeStrategy.h", "_dps_aoe_strategy_8h.html", [
      [ "DpsAoeStrategy", "classai_1_1_dps_aoe_strategy.html", "classai_1_1_dps_aoe_strategy" ]
    ] ],
    [ "DpsAssistStrategy.cpp", "_dps_assist_strategy_8cpp.html", null ],
    [ "DpsAssistStrategy.h", "_dps_assist_strategy_8h.html", [
      [ "DpsAssistStrategy", "classai_1_1_dps_assist_strategy.html", "classai_1_1_dps_assist_strategy" ]
    ] ],
    [ "DuelStrategy.cpp", "_duel_strategy_8cpp.html", null ],
    [ "DuelStrategy.h", "_duel_strategy_8h.html", [
      [ "DuelStrategy", "classai_1_1_duel_strategy.html", "classai_1_1_duel_strategy" ]
    ] ],
    [ "EmoteStrategy.cpp", "_emote_strategy_8cpp.html", null ],
    [ "EmoteStrategy.h", "_emote_strategy_8h.html", [
      [ "EmoteStrategy", "classai_1_1_emote_strategy.html", "classai_1_1_emote_strategy" ]
    ] ],
    [ "FleeStrategy.cpp", "_flee_strategy_8cpp.html", null ],
    [ "FleeStrategy.h", "_flee_strategy_8h.html", [
      [ "FleeStrategy", "classai_1_1_flee_strategy.html", "classai_1_1_flee_strategy" ],
      [ "FleeFromAddsStrategy", "classai_1_1_flee_from_adds_strategy.html", "classai_1_1_flee_from_adds_strategy" ]
    ] ],
    [ "FollowLineStrategy.cpp", "_follow_line_strategy_8cpp.html", null ],
    [ "FollowLineStrategy.h", "_follow_line_strategy_8h.html", [
      [ "FollowLineStrategy", "classai_1_1_follow_line_strategy.html", "classai_1_1_follow_line_strategy" ]
    ] ],
    [ "FollowMasterRandomStrategy.cpp", "_follow_master_random_strategy_8cpp.html", null ],
    [ "FollowMasterRandomStrategy.h", "_follow_master_random_strategy_8h.html", [
      [ "FollowMasterRandomStrategy", "classai_1_1_follow_master_random_strategy.html", "classai_1_1_follow_master_random_strategy" ]
    ] ],
    [ "FollowMasterStrategy.cpp", "_follow_master_strategy_8cpp.html", null ],
    [ "FollowMasterStrategy.h", "_follow_master_strategy_8h.html", [
      [ "FollowMasterStrategy", "classai_1_1_follow_master_strategy.html", "classai_1_1_follow_master_strategy" ]
    ] ],
    [ "GrindingStrategy.cpp", "_grinding_strategy_8cpp.html", null ],
    [ "GrindingStrategy.h", "_grinding_strategy_8h.html", [
      [ "GrindingStrategy", "classai_1_1_grinding_strategy.html", "classai_1_1_grinding_strategy" ]
    ] ],
    [ "GuardStrategy.cpp", "_guard_strategy_8cpp.html", null ],
    [ "GuardStrategy.h", "_guard_strategy_8h.html", [
      [ "GuardStrategy", "classai_1_1_guard_strategy.html", "classai_1_1_guard_strategy" ]
    ] ],
    [ "KiteStrategy.cpp", "_kite_strategy_8cpp.html", null ],
    [ "KiteStrategy.h", "_kite_strategy_8h.html", [
      [ "KiteStrategy", "classai_1_1_kite_strategy.html", "classai_1_1_kite_strategy" ]
    ] ],
    [ "LootNonCombatStrategy.cpp", "_loot_non_combat_strategy_8cpp.html", null ],
    [ "LootNonCombatStrategy.h", "_loot_non_combat_strategy_8h.html", [
      [ "LootNonCombatStrategy", "classai_1_1_loot_non_combat_strategy.html", "classai_1_1_loot_non_combat_strategy" ],
      [ "GatherStrategy", "classai_1_1_gather_strategy.html", "classai_1_1_gather_strategy" ]
    ] ],
    [ "MeleeCombatStrategy.cpp", "_melee_combat_strategy_8cpp.html", null ],
    [ "MeleeCombatStrategy.h", "_melee_combat_strategy_8h.html", [
      [ "MeleeCombatStrategy", "classai_1_1_melee_combat_strategy.html", "classai_1_1_melee_combat_strategy" ]
    ] ],
    [ "MoveRandomStrategy.cpp", "_move_random_strategy_8cpp.html", null ],
    [ "MoveRandomStrategy.h", "_move_random_strategy_8h.html", [
      [ "MoveRandomStrategy", "classai_1_1_move_random_strategy.html", "classai_1_1_move_random_strategy" ]
    ] ],
    [ "NonCombatStrategy.cpp", "_non_combat_strategy_8cpp.html", null ],
    [ "NonCombatStrategy.h", "_non_combat_strategy_8h.html", [
      [ "NonCombatStrategy", "classai_1_1_non_combat_strategy.html", "classai_1_1_non_combat_strategy" ]
    ] ],
    [ "PassiveStrategy.cpp", "_passive_strategy_8cpp.html", null ],
    [ "PassiveStrategy.h", "_passive_strategy_8h.html", [
      [ "PassiveStrategy", "classai_1_1_passive_strategy.html", "classai_1_1_passive_strategy" ]
    ] ],
    [ "PassTroughStrategy.h", "_pass_trough_strategy_8h.html", [
      [ "PassTroughStrategy", "classai_1_1_pass_trough_strategy.html", "classai_1_1_pass_trough_strategy" ]
    ] ],
    [ "PullStrategy.cpp", "_pull_strategy_8cpp.html", [
      [ "MagePullMultiplier", "class_mage_pull_multiplier.html", "class_mage_pull_multiplier" ]
    ] ],
    [ "PullStrategy.h", "_pull_strategy_8h.html", [
      [ "PullStrategy", "classai_1_1_pull_strategy.html", "classai_1_1_pull_strategy" ]
    ] ],
    [ "QuestStrategies.cpp", "_quest_strategies_8cpp.html", null ],
    [ "QuestStrategies.h", "_quest_strategies_8h.html", [
      [ "QuestStrategy", "classai_1_1_quest_strategy.html", "classai_1_1_quest_strategy" ],
      [ "DefaultQuestStrategy", "classai_1_1_default_quest_strategy.html", "classai_1_1_default_quest_strategy" ],
      [ "AcceptAllQuestsStrategy", "classai_1_1_accept_all_quests_strategy.html", "classai_1_1_accept_all_quests_strategy" ]
    ] ],
    [ "RacialsStrategy.cpp", "_racials_strategy_8cpp.html", [
      [ "RacialsStrategyActionNodeFactory", "class_racials_strategy_action_node_factory.html", "class_racials_strategy_action_node_factory" ]
    ] ],
    [ "RacialsStrategy.h", "_racials_strategy_8h.html", [
      [ "RacialsStrategy", "classai_1_1_racials_strategy.html", "classai_1_1_racials_strategy" ]
    ] ],
    [ "RangedCombatStrategy.cpp", "_ranged_combat_strategy_8cpp.html", null ],
    [ "RangedCombatStrategy.h", "_ranged_combat_strategy_8h.html", [
      [ "RangedCombatStrategy", "classai_1_1_ranged_combat_strategy.html", "classai_1_1_ranged_combat_strategy" ]
    ] ],
    [ "RunawayStrategy.cpp", "_runaway_strategy_8cpp.html", null ],
    [ "RunawayStrategy.h", "_runaway_strategy_8h.html", [
      [ "RunawayStrategy", "classai_1_1_runaway_strategy.html", "classai_1_1_runaway_strategy" ]
    ] ],
    [ "StayCircleStrategy.cpp", "_stay_circle_strategy_8cpp.html", null ],
    [ "StayCircleStrategy.h", "_stay_circle_strategy_8h.html", [
      [ "StayCircleStrategy", "classai_1_1_stay_circle_strategy.html", "classai_1_1_stay_circle_strategy" ]
    ] ],
    [ "StayCombatStrategy.cpp", "_stay_combat_strategy_8cpp.html", null ],
    [ "StayCombatStrategy.h", "_stay_combat_strategy_8h.html", [
      [ "StayCombatStrategy", "classai_1_1_stay_combat_strategy.html", "classai_1_1_stay_combat_strategy" ]
    ] ],
    [ "StayLineStrategy.cpp", "_stay_line_strategy_8cpp.html", null ],
    [ "StayLineStrategy.h", "_stay_line_strategy_8h.html", [
      [ "StayLineStrategy", "classai_1_1_stay_line_strategy.html", "classai_1_1_stay_line_strategy" ]
    ] ],
    [ "StayStrategy.cpp", "_stay_strategy_8cpp.html", null ],
    [ "StayStrategy.h", "_stay_strategy_8h.html", [
      [ "StayStrategy", "classai_1_1_stay_strategy.html", "classai_1_1_stay_strategy" ]
    ] ],
    [ "TankAoeStrategy.cpp", "_tank_aoe_strategy_8cpp.html", null ],
    [ "TankAoeStrategy.h", "_tank_aoe_strategy_8h.html", [
      [ "TankAoeStrategy", "classai_1_1_tank_aoe_strategy.html", "classai_1_1_tank_aoe_strategy" ]
    ] ],
    [ "TankAssistStrategy.cpp", "_tank_assist_strategy_8cpp.html", null ],
    [ "TankAssistStrategy.h", "_tank_assist_strategy_8h.html", [
      [ "TankAssistStrategy", "classai_1_1_tank_assist_strategy.html", "classai_1_1_tank_assist_strategy" ]
    ] ],
    [ "TellTargetStrategy.cpp", "_tell_target_strategy_8cpp.html", null ],
    [ "TellTargetStrategy.h", "_tell_target_strategy_8h.html", [
      [ "TellTargetStrategy", "classai_1_1_tell_target_strategy.html", "classai_1_1_tell_target_strategy" ]
    ] ],
    [ "ThreatStrategy.cpp", "_threat_strategy_8cpp.html", null ],
    [ "ThreatStrategy.h", "_threat_strategy_8h.html", [
      [ "ThreatMultiplier", "classai_1_1_threat_multiplier.html", "classai_1_1_threat_multiplier" ],
      [ "ThreatStrategy", "classai_1_1_threat_strategy.html", "classai_1_1_threat_strategy" ]
    ] ],
    [ "UseFoodStrategy.cpp", "_use_food_strategy_8cpp.html", null ],
    [ "UseFoodStrategy.h", "_use_food_strategy_8h.html", [
      [ "UseFoodStrategy", "classai_1_1_use_food_strategy.html", "classai_1_1_use_food_strategy" ]
    ] ],
    [ "UsePotionsStrategy.cpp", "_use_potions_strategy_8cpp.html", null ],
    [ "UsePotionsStrategy.h", "_use_potions_strategy_8h.html", [
      [ "UsePotionsStrategy", "classai_1_1_use_potions_strategy.html", "classai_1_1_use_potions_strategy" ]
    ] ],
    [ "WorldPacketHandlerStrategy.cpp", "_world_packet_handler_strategy_8cpp.html", null ],
    [ "WorldPacketHandlerStrategy.h", "_world_packet_handler_strategy_8h.html", [
      [ "WorldPacketHandlerStrategy", "classai_1_1_world_packet_handler_strategy.html", "classai_1_1_world_packet_handler_strategy" ],
      [ "ReadyCheckStrategy", "classai_1_1_ready_check_strategy.html", "classai_1_1_ready_check_strategy" ]
    ] ]
];