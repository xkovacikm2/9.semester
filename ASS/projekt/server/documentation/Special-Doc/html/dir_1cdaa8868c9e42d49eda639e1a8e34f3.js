var dir_1cdaa8868c9e42d49eda639e1a8e34f3 =
[
    [ "AlwaysLootListValue.h", "_always_loot_list_value_8h.html", [
      [ "AlwaysLootListValue", "classai_1_1_always_loot_list_value.html", "classai_1_1_always_loot_list_value" ]
    ] ],
    [ "AoeHealValues.cpp", "_aoe_heal_values_8cpp.html", null ],
    [ "AoeHealValues.h", "_aoe_heal_values_8h.html", [
      [ "AoeHealValue", "classai_1_1_aoe_heal_value.html", "classai_1_1_aoe_heal_value" ]
    ] ],
    [ "AttackerCountValues.cpp", "_attacker_count_values_8cpp.html", null ],
    [ "AttackerCountValues.h", "_attacker_count_values_8h.html", [
      [ "AttackerCountValue", "classai_1_1_attacker_count_value.html", "classai_1_1_attacker_count_value" ],
      [ "MyAttackerCountValue", "classai_1_1_my_attacker_count_value.html", "classai_1_1_my_attacker_count_value" ],
      [ "HasAggroValue", "classai_1_1_has_aggro_value.html", "classai_1_1_has_aggro_value" ],
      [ "BalancePercentValue", "classai_1_1_balance_percent_value.html", "classai_1_1_balance_percent_value" ]
    ] ],
    [ "AttackersValue.cpp", "_attackers_value_8cpp.html", null ],
    [ "AttackersValue.h", "_attackers_value_8h.html", [
      [ "AttackersValue", "classai_1_1_attackers_value.html", "classai_1_1_attackers_value" ]
    ] ],
    [ "AttackerWithoutAuraTargetValue.cpp", "_attacker_without_aura_target_value_8cpp.html", null ],
    [ "AttackerWithoutAuraTargetValue.h", "_attacker_without_aura_target_value_8h.html", [
      [ "AttackerWithoutAuraTargetValue", "classai_1_1_attacker_without_aura_target_value.html", "classai_1_1_attacker_without_aura_target_value" ]
    ] ],
    [ "AvailableLootValue.h", "_available_loot_value_8h.html", [
      [ "AvailableLootValue", "classai_1_1_available_loot_value.html", "classai_1_1_available_loot_value" ],
      [ "LootTargetValue", "classai_1_1_loot_target_value.html", "classai_1_1_loot_target_value" ],
      [ "CanLootValue", "classai_1_1_can_loot_value.html", "classai_1_1_can_loot_value" ]
    ] ],
    [ "CcTargetValue.cpp", "_cc_target_value_8cpp.html", [
      [ "FindTargetForCcStrategy", "class_find_target_for_cc_strategy.html", "class_find_target_for_cc_strategy" ]
    ] ],
    [ "CcTargetValue.h", "_cc_target_value_8h.html", [
      [ "CcTargetValue", "classai_1_1_cc_target_value.html", "classai_1_1_cc_target_value" ]
    ] ],
    [ "ChatValue.h", "_chat_value_8h.html", [
      [ "ChatValue", "classai_1_1_chat_value.html", "classai_1_1_chat_value" ]
    ] ],
    [ "CurrentCcTargetValue.cpp", "_current_cc_target_value_8cpp.html", [
      [ "FindCurrentCcTargetStrategy", "class_find_current_cc_target_strategy.html", "class_find_current_cc_target_strategy" ]
    ] ],
    [ "CurrentCcTargetValue.h", "_current_cc_target_value_8h.html", [
      [ "CurrentCcTargetValue", "classai_1_1_current_cc_target_value.html", "classai_1_1_current_cc_target_value" ]
    ] ],
    [ "CurrentTargetValue.cpp", "_current_target_value_8cpp.html", null ],
    [ "CurrentTargetValue.h", "_current_target_value_8h.html", [
      [ "CurrentTargetValue", "classai_1_1_current_target_value.html", "classai_1_1_current_target_value" ]
    ] ],
    [ "DistanceValue.h", "_distance_value_8h.html", [
      [ "DistanceValue", "classai_1_1_distance_value.html", "classai_1_1_distance_value" ]
    ] ],
    [ "DpsTargetValue.cpp", "_dps_target_value_8cpp.html", [
      [ "FindTargetForDpsStrategy", "class_find_target_for_dps_strategy.html", "class_find_target_for_dps_strategy" ]
    ] ],
    [ "DpsTargetValue.h", "_dps_target_value_8h.html", [
      [ "DpsTargetValue", "classai_1_1_dps_target_value.html", "classai_1_1_dps_target_value" ]
    ] ],
    [ "DuelTargetValue.cpp", "_duel_target_value_8cpp.html", null ],
    [ "DuelTargetValue.h", "_duel_target_value_8h.html", [
      [ "DuelTargetValue", "classai_1_1_duel_target_value.html", "classai_1_1_duel_target_value" ]
    ] ],
    [ "EnemyHealerTargetValue.cpp", "_enemy_healer_target_value_8cpp.html", null ],
    [ "EnemyHealerTargetValue.h", "_enemy_healer_target_value_8h.html", [
      [ "EnemyHealerTargetValue", "classai_1_1_enemy_healer_target_value.html", "classai_1_1_enemy_healer_target_value" ]
    ] ],
    [ "EnemyPlayerValue.cpp", "_enemy_player_value_8cpp.html", [
      [ "FindEnemyPlayerStrategy", "class_find_enemy_player_strategy.html", "class_find_enemy_player_strategy" ]
    ] ],
    [ "EnemyPlayerValue.h", "_enemy_player_value_8h.html", [
      [ "EnemyPlayerValue", "classai_1_1_enemy_player_value.html", "classai_1_1_enemy_player_value" ]
    ] ],
    [ "GrindTargetValue.cpp", "_grind_target_value_8cpp.html", null ],
    [ "GrindTargetValue.h", "_grind_target_value_8h.html", [
      [ "GrindTargetValue", "classai_1_1_grind_target_value.html", "classai_1_1_grind_target_value" ]
    ] ],
    [ "HasAvailableLootValue.h", "_has_available_loot_value_8h.html", [
      [ "HasAvailableLootValue", "classai_1_1_has_available_loot_value.html", "classai_1_1_has_available_loot_value" ]
    ] ],
    [ "HasTotemValue.h", "_has_totem_value_8h.html", [
      [ "HasTotemValue", "classai_1_1_has_totem_value.html", "classai_1_1_has_totem_value" ]
    ] ],
    [ "InvalidTargetValue.cpp", "_invalid_target_value_8cpp.html", null ],
    [ "InvalidTargetValue.h", "_invalid_target_value_8h.html", [
      [ "InvalidTargetValue", "classai_1_1_invalid_target_value.html", "classai_1_1_invalid_target_value" ]
    ] ],
    [ "IsBehindValue.h", "_is_behind_value_8h.html", [
      [ "IsBehindValue", "classai_1_1_is_behind_value.html", "classai_1_1_is_behind_value" ]
    ] ],
    [ "IsFacingValue.h", "_is_facing_value_8h.html", [
      [ "IsFacingValue", "classai_1_1_is_facing_value.html", "classai_1_1_is_facing_value" ]
    ] ],
    [ "IsMovingValue.h", "_is_moving_value_8h.html", [
      [ "IsMovingValue", "classai_1_1_is_moving_value.html", "classai_1_1_is_moving_value" ],
      [ "IsSwimmingValue", "classai_1_1_is_swimming_value.html", "classai_1_1_is_swimming_value" ]
    ] ],
    [ "ItemCountValue.cpp", "_item_count_value_8cpp.html", null ],
    [ "ItemCountValue.h", "_item_count_value_8h.html", [
      [ "InventoryItemValueBase", "classai_1_1_inventory_item_value_base.html", "classai_1_1_inventory_item_value_base" ],
      [ "ItemCountValue", "classai_1_1_item_count_value.html", "classai_1_1_item_count_value" ],
      [ "InventoryItemValue", "classai_1_1_inventory_item_value.html", "classai_1_1_inventory_item_value" ]
    ] ],
    [ "ItemForSpellValue.cpp", "_item_for_spell_value_8cpp.html", "_item_for_spell_value_8cpp" ],
    [ "ItemForSpellValue.h", "_item_for_spell_value_8h.html", [
      [ "ItemForSpellValue", "classai_1_1_item_for_spell_value.html", "classai_1_1_item_for_spell_value" ]
    ] ],
    [ "ItemUsageValue.cpp", "_item_usage_value_8cpp.html", null ],
    [ "ItemUsageValue.h", "_item_usage_value_8h.html", "_item_usage_value_8h" ],
    [ "LastMovementValue.h", "_last_movement_value_8h.html", [
      [ "LastMovement", "classai_1_1_last_movement.html", "classai_1_1_last_movement" ],
      [ "LastMovementValue", "classai_1_1_last_movement_value.html", "classai_1_1_last_movement_value" ]
    ] ],
    [ "LastSpellCastTimeValue.h", "_last_spell_cast_time_value_8h.html", [
      [ "LastSpellCastTimeValue", "classai_1_1_last_spell_cast_time_value.html", "classai_1_1_last_spell_cast_time_value" ]
    ] ],
    [ "LastSpellCastValue.h", "_last_spell_cast_value_8h.html", [
      [ "LastSpellCast", "classai_1_1_last_spell_cast.html", "classai_1_1_last_spell_cast" ],
      [ "LastSpellCastValue", "classai_1_1_last_spell_cast_value.html", "classai_1_1_last_spell_cast_value" ]
    ] ],
    [ "LeastHpTargetValue.cpp", "_least_hp_target_value_8cpp.html", [
      [ "FindLeastHpTargetStrategy", "class_find_least_hp_target_strategy.html", "class_find_least_hp_target_strategy" ]
    ] ],
    [ "LeastHpTargetValue.h", "_least_hp_target_value_8h.html", [
      [ "LeastHpTargetValue", "classai_1_1_least_hp_target_value.html", "classai_1_1_least_hp_target_value" ]
    ] ],
    [ "LfgValues.h", "_lfg_values_8h.html", [
      [ "LfgProposalValue", "classai_1_1_lfg_proposal_value.html", "classai_1_1_lfg_proposal_value" ]
    ] ],
    [ "LineTargetValue.cpp", "_line_target_value_8cpp.html", null ],
    [ "LineTargetValue.h", "_line_target_value_8h.html", [
      [ "LineTargetValue", "classai_1_1_line_target_value.html", "classai_1_1_line_target_value" ]
    ] ],
    [ "LogLevelValue.h", "_log_level_value_8h.html", [
      [ "LogLevelValue", "classai_1_1_log_level_value.html", "classai_1_1_log_level_value" ]
    ] ],
    [ "LootStrategyValue.h", "_loot_strategy_value_8h.html", [
      [ "LootStrategyValue", "classai_1_1_loot_strategy_value.html", "classai_1_1_loot_strategy_value" ]
    ] ],
    [ "ManaSaveLevelValue.h", "_mana_save_level_value_8h.html", [
      [ "ManaSaveLevelValue", "classai_1_1_mana_save_level_value.html", "classai_1_1_mana_save_level_value" ]
    ] ],
    [ "MasterTargetValue.h", "_master_target_value_8h.html", [
      [ "MasterTargetValue", "classai_1_1_master_target_value.html", "classai_1_1_master_target_value" ]
    ] ],
    [ "NearestAdsValue.cpp", "_nearest_ads_value_8cpp.html", null ],
    [ "NearestAdsValue.h", "_nearest_ads_value_8h.html", [
      [ "NearestAdsValue", "classai_1_1_nearest_ads_value.html", "classai_1_1_nearest_ads_value" ]
    ] ],
    [ "NearestCorpsesValue.cpp", "_nearest_corpses_value_8cpp.html", [
      [ "AnyDeadUnitInObjectRangeCheck", "class_any_dead_unit_in_object_range_check.html", "class_any_dead_unit_in_object_range_check" ]
    ] ],
    [ "NearestCorpsesValue.h", "_nearest_corpses_value_8h.html", [
      [ "NearestCorpsesValue", "classai_1_1_nearest_corpses_value.html", "classai_1_1_nearest_corpses_value" ]
    ] ],
    [ "NearestGameObjects.cpp", "_nearest_game_objects_8cpp.html", [
      [ "AnyGameObjectInObjectRangeCheck", "class_any_game_object_in_object_range_check.html", "class_any_game_object_in_object_range_check" ]
    ] ],
    [ "NearestGameObjects.h", "_nearest_game_objects_8h.html", [
      [ "NearestGameObjects", "classai_1_1_nearest_game_objects.html", "classai_1_1_nearest_game_objects" ]
    ] ],
    [ "NearestNpcsValue.cpp", "_nearest_npcs_value_8cpp.html", null ],
    [ "NearestNpcsValue.h", "_nearest_npcs_value_8h.html", [
      [ "NearestNpcsValue", "classai_1_1_nearest_npcs_value.html", "classai_1_1_nearest_npcs_value" ]
    ] ],
    [ "NearestUnitsValue.h", "_nearest_units_value_8h.html", [
      [ "NearestUnitsValue", "classai_1_1_nearest_units_value.html", "classai_1_1_nearest_units_value" ]
    ] ],
    [ "PartyMemberToDispel.cpp", "_party_member_to_dispel_8cpp.html", [
      [ "PartyMemberToDispelPredicate", "class_party_member_to_dispel_predicate.html", "class_party_member_to_dispel_predicate" ]
    ] ],
    [ "PartyMemberToDispel.h", "_party_member_to_dispel_8h.html", [
      [ "PartyMemberToDispel", "classai_1_1_party_member_to_dispel.html", "classai_1_1_party_member_to_dispel" ]
    ] ],
    [ "PartyMemberToHeal.cpp", "_party_member_to_heal_8cpp.html", [
      [ "IsTargetOfHealingSpell", "class_is_target_of_healing_spell.html", "class_is_target_of_healing_spell" ]
    ] ],
    [ "PartyMemberToHeal.h", "_party_member_to_heal_8h.html", [
      [ "PartyMemberToHeal", "classai_1_1_party_member_to_heal.html", "classai_1_1_party_member_to_heal" ]
    ] ],
    [ "PartyMemberToResurrect.cpp", "_party_member_to_resurrect_8cpp.html", [
      [ "IsTargetOfResurrectSpell", "class_is_target_of_resurrect_spell.html", "class_is_target_of_resurrect_spell" ],
      [ "FindDeadPlayer", "class_find_dead_player.html", "class_find_dead_player" ]
    ] ],
    [ "PartyMemberToResurrect.h", "_party_member_to_resurrect_8h.html", [
      [ "PartyMemberToResurrect", "classai_1_1_party_member_to_resurrect.html", "classai_1_1_party_member_to_resurrect" ]
    ] ],
    [ "PartyMemberValue.cpp", "_party_member_value_8cpp.html", null ],
    [ "PartyMemberValue.h", "_party_member_value_8h.html", [
      [ "FindPlayerPredicate", "classai_1_1_find_player_predicate.html", "classai_1_1_find_player_predicate" ],
      [ "SpellEntryPredicate", "classai_1_1_spell_entry_predicate.html", "classai_1_1_spell_entry_predicate" ],
      [ "PartyMemberValue", "classai_1_1_party_member_value.html", "classai_1_1_party_member_value" ]
    ] ],
    [ "PartyMemberWithoutAuraValue.cpp", "_party_member_without_aura_value_8cpp.html", [
      [ "PlayerWithoutAuraPredicate", "class_player_without_aura_predicate.html", "class_player_without_aura_predicate" ]
    ] ],
    [ "PartyMemberWithoutAuraValue.h", "_party_member_without_aura_value_8h.html", [
      [ "PartyMemberWithoutAuraValue", "classai_1_1_party_member_without_aura_value.html", "classai_1_1_party_member_without_aura_value" ]
    ] ],
    [ "PetTargetValue.h", "_pet_target_value_8h.html", [
      [ "PetTargetValue", "classai_1_1_pet_target_value.html", "classai_1_1_pet_target_value" ]
    ] ],
    [ "PositionValue.cpp", "_position_value_8cpp.html", null ],
    [ "PositionValue.h", "_position_value_8h.html", [
      [ "Position", "classai_1_1_position.html", "classai_1_1_position" ],
      [ "PositionValue", "classai_1_1_position_value.html", "classai_1_1_position_value" ]
    ] ],
    [ "PossibleTargetsValue.cpp", "_possible_targets_value_8cpp.html", null ],
    [ "PossibleTargetsValue.h", "_possible_targets_value_8h.html", [
      [ "PossibleTargetsValue", "classai_1_1_possible_targets_value.html", "classai_1_1_possible_targets_value" ]
    ] ],
    [ "RtiTargetValue.h", "_rti_target_value_8h.html", [
      [ "RtiTargetValue", "classai_1_1_rti_target_value.html", "classai_1_1_rti_target_value" ]
    ] ],
    [ "RtiValue.cpp", "_rti_value_8cpp.html", null ],
    [ "RtiValue.h", "_rti_value_8h.html", [
      [ "RtiValue", "classai_1_1_rti_value.html", "classai_1_1_rti_value" ]
    ] ],
    [ "SelfTargetValue.h", "_self_target_value_8h.html", [
      [ "SelfTargetValue", "classai_1_1_self_target_value.html", "classai_1_1_self_target_value" ]
    ] ],
    [ "SpellCastUsefulValue.cpp", "_spell_cast_useful_value_8cpp.html", null ],
    [ "SpellCastUsefulValue.h", "_spell_cast_useful_value_8h.html", [
      [ "SpellCastUsefulValue", "classai_1_1_spell_cast_useful_value.html", "classai_1_1_spell_cast_useful_value" ]
    ] ],
    [ "SpellIdValue.cpp", "_spell_id_value_8cpp.html", null ],
    [ "SpellIdValue.h", "_spell_id_value_8h.html", [
      [ "SpellIdValue", "classai_1_1_spell_id_value.html", "classai_1_1_spell_id_value" ]
    ] ],
    [ "StatsValues.cpp", "_stats_values_8cpp.html", null ],
    [ "StatsValues.h", "_stats_values_8h.html", [
      [ "HealthValue", "classai_1_1_health_value.html", "classai_1_1_health_value" ],
      [ "IsDeadValue", "classai_1_1_is_dead_value.html", "classai_1_1_is_dead_value" ],
      [ "RageValue", "classai_1_1_rage_value.html", "classai_1_1_rage_value" ],
      [ "EnergyValue", "classai_1_1_energy_value.html", "classai_1_1_energy_value" ],
      [ "ManaValue", "classai_1_1_mana_value.html", "classai_1_1_mana_value" ],
      [ "HasManaValue", "classai_1_1_has_mana_value.html", "classai_1_1_has_mana_value" ],
      [ "ComboPointsValue", "classai_1_1_combo_points_value.html", "classai_1_1_combo_points_value" ],
      [ "IsMountedValue", "classai_1_1_is_mounted_value.html", "classai_1_1_is_mounted_value" ],
      [ "IsInCombatValue", "classai_1_1_is_in_combat_value.html", "classai_1_1_is_in_combat_value" ],
      [ "BagSpaceValue", "classai_1_1_bag_space_value.html", "classai_1_1_bag_space_value" ]
    ] ],
    [ "TankTargetValue.cpp", "_tank_target_value_8cpp.html", [
      [ "FindTargetForTankStrategy", "class_find_target_for_tank_strategy.html", "class_find_target_for_tank_strategy" ]
    ] ],
    [ "TankTargetValue.h", "_tank_target_value_8h.html", [
      [ "TankTargetValue", "classai_1_1_tank_target_value.html", "classai_1_1_tank_target_value" ]
    ] ],
    [ "TargetValue.cpp", "_target_value_8cpp.html", null ],
    [ "TargetValue.h", "_target_value_8h.html", [
      [ "FindTargetStrategy", "classai_1_1_find_target_strategy.html", "classai_1_1_find_target_strategy" ],
      [ "TargetValue", "classai_1_1_target_value.html", "classai_1_1_target_value" ]
    ] ],
    [ "ThreatValues.cpp", "_threat_values_8cpp.html", null ],
    [ "ThreatValues.h", "_threat_values_8h.html", [
      [ "ThreatValue", "classai_1_1_threat_value.html", "classai_1_1_threat_value" ]
    ] ],
    [ "ValueContext.h", "_value_context_8h.html", [
      [ "ValueContext", "classai_1_1_value_context.html", "classai_1_1_value_context" ]
    ] ]
];