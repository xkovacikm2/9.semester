var struct_buyer_item_info =
[
    [ "BuyerItemInfo", "struct_buyer_item_info.html#aa4ff986f5e378878a348cdf94ce2257d", null ],
    [ "BidPrice", "struct_buyer_item_info.html#ab2a4cae310557cb5f4c7280f4a73f065", null ],
    [ "BuyPrice", "struct_buyer_item_info.html#ae3e9e8a98a92c649c6df3dc06e30c7f6", null ],
    [ "ItemCount", "struct_buyer_item_info.html#aa6e4362c7c206a3b750fdd4c6057b3e0", null ],
    [ "MinBidPrice", "struct_buyer_item_info.html#aca2fc65c918e6a3312440dda391f2b38", null ],
    [ "MinBuyPrice", "struct_buyer_item_info.html#aae812b9efc48199be2ab59058483b1b5", null ]
];