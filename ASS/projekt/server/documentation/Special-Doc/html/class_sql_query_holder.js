var class_sql_query_holder =
[
    [ "SqlQueryHolder", "class_sql_query_holder.html#a44a1266384721a9ab5b833145298646d", null ],
    [ "~SqlQueryHolder", "class_sql_query_holder.html#a52751aca17c51f89be356e9c0c8ad7d3", null ],
    [ "Execute", "class_sql_query_holder.html#a1d4309b29e609d7934e79681438754d3", null ],
    [ "GetResult", "class_sql_query_holder.html#a7e746be9e265f33222d288dccd2bc442", null ],
    [ "SetPQuery", "class_sql_query_holder.html#a856777eb4b0519adc52d7f1dac72fdd3", null ],
    [ "SetQuery", "class_sql_query_holder.html#ab77e12eff88e2225b1e9ac7939fc19b9", null ],
    [ "SetResult", "class_sql_query_holder.html#a0d9f7b30180cab32c51c3711e70ea92e", null ],
    [ "SetSize", "class_sql_query_holder.html#a8ad1b3829b242ecdc11e4eb192700411", null ],
    [ "SqlQueryHolderEx", "class_sql_query_holder.html#affc6be521c3d2b4190c0f4896fd4f47d", null ]
];