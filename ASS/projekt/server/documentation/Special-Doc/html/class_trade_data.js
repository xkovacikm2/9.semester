var class_trade_data =
[
    [ "TradeData", "class_trade_data.html#ac2bfab37acbdb9fd955d8f2ee8876b79", null ],
    [ "GetItem", "class_trade_data.html#a6c769042535c69e89bdc19234dd680e4", null ],
    [ "GetMoney", "class_trade_data.html#a54d3de8cb15ccc761d272295b19b7575", null ],
    [ "GetSpell", "class_trade_data.html#a99272f794179b17c50c722ae90ea78bf", null ],
    [ "GetSpellCastItem", "class_trade_data.html#a011acd6730c58aa75d3c27e793748afc", null ],
    [ "GetTrader", "class_trade_data.html#a1a377da3913a91af904c2a4b4ca3377e", null ],
    [ "GetTraderData", "class_trade_data.html#a91911ad105b24d6055a65b07ab6c269f", null ],
    [ "HasItem", "class_trade_data.html#a7ca3b06ef1d970f93e53ad81b364d76c", null ],
    [ "HasSpellCastItem", "class_trade_data.html#a27981abf9bddbfe87bf81836b57e0780", null ],
    [ "IsAccepted", "class_trade_data.html#a75b4970c1f26912bc24da528f226ca76", null ],
    [ "IsInAcceptProcess", "class_trade_data.html#abbae0b720c0b44a05f0625ebafd09cc8", null ],
    [ "SetAccepted", "class_trade_data.html#a9e48c5ee348c5a8d475c5e7b15058b16", null ],
    [ "SetInAcceptProcess", "class_trade_data.html#a4851016708b0e8336ec33c64ab41aa8a", null ],
    [ "SetItem", "class_trade_data.html#ab3eff66bb411bd22a11d8e29f7961507", null ],
    [ "SetMoney", "class_trade_data.html#a0dc996fdc9acab409833caff4e6b489b", null ],
    [ "SetSpell", "class_trade_data.html#acab3351d1ab080b8e5b5761774c53985", null ]
];