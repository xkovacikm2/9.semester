var struct_game_event_data =
[
    [ "GameEventData", "struct_game_event_data.html#a09a5543a4c89dc258ffe64f713b5b1b2", null ],
    [ "isValid", "struct_game_event_data.html#a7a91c9c9cc68593812b13f6efd2ea4ff", null ],
    [ "description", "struct_game_event_data.html#aebdadbaadbba367601674146a27a283f", null ],
    [ "end", "struct_game_event_data.html#a1bf3980d02cd0dc304b2ffbd5afb7b9f", null ],
    [ "holiday_id", "struct_game_event_data.html#aa13c2c2eacb9475e18c1d1370566e988", null ],
    [ "length", "struct_game_event_data.html#ac45297bad99ddb339b3aa18ec5912c1c", null ],
    [ "occurence", "struct_game_event_data.html#a04c813525fef572000608f32650565ff", null ],
    [ "start", "struct_game_event_data.html#ac4da43ddbb3b95f00589f49ed250092d", null ]
];