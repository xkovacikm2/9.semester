var struct_coord_pair =
[
    [ "CoordPair", "struct_coord_pair.html#a53b5f24cfdf13b246549a0c1543924c9", null ],
    [ "CoordPair", "struct_coord_pair.html#aab92ece0f01ebc00cdac4966e02fa8fd", null ],
    [ "normalize", "struct_coord_pair.html#a8fb2e10913a74b466bd7bea7bf63de97", null ],
    [ "operator!=", "struct_coord_pair.html#a9a7b8c06310cd99d2b76cd99cc4af2d4", null ],
    [ "operator+=", "struct_coord_pair.html#aed64e2c76b1ed8581d4009a17ddc3a0f", null ],
    [ "operator-=", "struct_coord_pair.html#a28835cb09b85655cd099ed8abe106348", null ],
    [ "operator<<", "struct_coord_pair.html#ab64e398f1b21e51c67fe7fc46939732d", null ],
    [ "operator=", "struct_coord_pair.html#a6b8377f9321df2930f640d3d475c4f65", null ],
    [ "operator==", "struct_coord_pair.html#a79f9f6ae0c49487be7cbea69396469e8", null ],
    [ "operator>>", "struct_coord_pair.html#a75ad1e090464dd098c809debf9a007b1", null ],
    [ "x_coord", "struct_coord_pair.html#a101dd71af6a2cba18140aea2e502ef8b", null ],
    [ "y_coord", "struct_coord_pair.html#aacae12844cc0c0d70e26b137ad28f700", null ]
];