var _strategy_8h =
[
    [ "Strategy", "classai_1_1_strategy.html", "classai_1_1_strategy" ],
    [ "ActionPriority", "_strategy_8h.html#a8f3bdf28cf2126b74ad6599b1162ca82", [
      [ "ACTION_IDLE", "_strategy_8h.html#a8f3bdf28cf2126b74ad6599b1162ca82a4167dbac51511dd9e63cf9360b2b0c2d", null ],
      [ "ACTION_NORMAL", "_strategy_8h.html#a8f3bdf28cf2126b74ad6599b1162ca82a4cf0ae2484ea173d922ce4587d1b5005", null ],
      [ "ACTION_HIGH", "_strategy_8h.html#a8f3bdf28cf2126b74ad6599b1162ca82a63542d8a6598e718556e0c184b060ddc", null ],
      [ "ACTION_MOVE", "_strategy_8h.html#a8f3bdf28cf2126b74ad6599b1162ca82a3df4faa13f20cf2f139c7ab81bfb68ff", null ],
      [ "ACTION_INTERRUPT", "_strategy_8h.html#a8f3bdf28cf2126b74ad6599b1162ca82a61b588275929e86bcf11f8030947b35e", null ],
      [ "ACTION_DISPEL", "_strategy_8h.html#a8f3bdf28cf2126b74ad6599b1162ca82a8e711f1b8443cc28d5ffc44249516ad9", null ],
      [ "ACTION_LIGHT_HEAL", "_strategy_8h.html#a8f3bdf28cf2126b74ad6599b1162ca82acf36a758f46baa13aeada81e28bf8bb0", null ],
      [ "ACTION_MEDIUM_HEAL", "_strategy_8h.html#a8f3bdf28cf2126b74ad6599b1162ca82a43e4a809309527ec61a02e56aed7ce95", null ],
      [ "ACTION_CRITICAL_HEAL", "_strategy_8h.html#a8f3bdf28cf2126b74ad6599b1162ca82a1c8524f0db885a87566c095dbd80c23b", null ],
      [ "ACTION_EMERGENCY", "_strategy_8h.html#a8f3bdf28cf2126b74ad6599b1162ca82a3f5edb284bd47f9bede7e00ed99da0e7", null ]
    ] ],
    [ "StrategyType", "_strategy_8h.html#a003cb912a241828c2692e5b0f41a9938", [
      [ "STRATEGY_TYPE_GENERIC", "_strategy_8h.html#a003cb912a241828c2692e5b0f41a9938a3a1b71b07bb9b97931219401e052696c", null ],
      [ "STRATEGY_TYPE_COMBAT", "_strategy_8h.html#a003cb912a241828c2692e5b0f41a9938a9e839a1d7fdab225a772572d4602fa40", null ],
      [ "STRATEGY_TYPE_NONCOMBAT", "_strategy_8h.html#a003cb912a241828c2692e5b0f41a9938a3ef8e0e56dae4f49d3feaca284c2d89d", null ],
      [ "STRATEGY_TYPE_TANK", "_strategy_8h.html#a003cb912a241828c2692e5b0f41a9938a4b3c38402b107274e8152a4c85de80db", null ],
      [ "STRATEGY_TYPE_DPS", "_strategy_8h.html#a003cb912a241828c2692e5b0f41a9938a594ddd44bf422f9fa1c08b8242dadbe8", null ],
      [ "STRATEGY_TYPE_HEAL", "_strategy_8h.html#a003cb912a241828c2692e5b0f41a9938a59a3f970277e21a5bd89c70de5dd7cce", null ],
      [ "STRATEGY_TYPE_RANGED", "_strategy_8h.html#a003cb912a241828c2692e5b0f41a9938a734adf1684686902fa0d76dd19c7881a", null ],
      [ "STRATEGY_TYPE_MELEE", "_strategy_8h.html#a003cb912a241828c2692e5b0f41a9938aed0b3c6b0767b59a2ae5f0ac14be2e59", null ]
    ] ]
];