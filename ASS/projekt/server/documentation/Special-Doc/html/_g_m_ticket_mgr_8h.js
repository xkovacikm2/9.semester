var _g_m_ticket_mgr_8h =
[
    [ "sTicketMgr", "group__game.html#ga20db6dadbe20767eb03327894588c7be", null ],
    [ "GMTicketIdMap", "group__game.html#ga792e084e719e382f43cb08d93c067fca", null ],
    [ "GMTicketMap", "group__game.html#ga74b5ac4f4c09c7edf0dc651a728916f3", null ],
    [ "GMTicketResponse", "_g_m_ticket_mgr_8h.html#adb4cde78a930d8f04d2f7ba4167a27d3", [
      [ "GMTICKET_RESPONSE_ALREADY_EXIST", "_g_m_ticket_mgr_8h.html#adb4cde78a930d8f04d2f7ba4167a27d3a51361b1b513a3edf01f314b6c8c14921", null ],
      [ "GMTICKET_RESPONSE_CREATE_SUCCESS", "_g_m_ticket_mgr_8h.html#adb4cde78a930d8f04d2f7ba4167a27d3a841b790fadbc9573dd25dc12bacc2780", null ],
      [ "GMTICKET_RESPONSE_CREATE_ERROR", "_g_m_ticket_mgr_8h.html#adb4cde78a930d8f04d2f7ba4167a27d3a08c10cb4c26d717228a5697ead5bbbc2", null ],
      [ "GMTICKET_RESPONSE_UPDATE_SUCCESS", "_g_m_ticket_mgr_8h.html#adb4cde78a930d8f04d2f7ba4167a27d3af30fa9cd9828f4254eac75bd01ca49f5", null ],
      [ "GMTICKET_RESPONSE_UPDATE_ERROR", "_g_m_ticket_mgr_8h.html#adb4cde78a930d8f04d2f7ba4167a27d3af4d4cdba2bdbb81d2ca25b1f4864e18f", null ],
      [ "GMTICKET_RESPONSE_TICKET_DELETED", "_g_m_ticket_mgr_8h.html#adb4cde78a930d8f04d2f7ba4167a27d3aa0b33c09a7035bef00e6d174383c0804", null ]
    ] ]
];