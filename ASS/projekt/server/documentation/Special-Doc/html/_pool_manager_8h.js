var _pool_manager_8h =
[
    [ "PoolTemplateData", "struct_pool_template_data.html", "struct_pool_template_data" ],
    [ "PoolObject", "struct_pool_object.html", "struct_pool_object" ],
    [ "Pool", "class_pool.html", null ],
    [ "SpawnedPoolData", "class_spawned_pool_data.html", "class_spawned_pool_data" ],
    [ "PoolGroup", "class_pool_group.html", "class_pool_group" ],
    [ "PoolManager", "class_pool_manager.html", "class_pool_manager" ],
    [ "sPoolMgr", "_pool_manager_8h.html#a9189dd9c6265b9e6d57104dd61a5fe85", null ],
    [ "PoolObjectList", "_pool_manager_8h.html#a4fec5569848f6814295ad96445f1b2ba", null ],
    [ "SpawnedPoolObjects", "_pool_manager_8h.html#ad45b03f314fc866d646ba28fffa87e0d", null ],
    [ "SpawnedPoolPools", "_pool_manager_8h.html#a1f65e6bff563b294e5348c2e77919275", null ]
];