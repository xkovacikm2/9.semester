var struct_item_prototype =
[
    [ "CanChangeEquipStateInCombat", "struct_item_prototype.html#a83bdc1e097f69ed6560c376bd7730c80", null ],
    [ "GetMaxStackSize", "struct_item_prototype.html#a79913f1362b580a8d20b52fc9152c292", null ],
    [ "IsArmorVellum", "struct_item_prototype.html#ac1f5f42da71291880b0744d7da16cd73", null ],
    [ "IsConjuredConsumable", "struct_item_prototype.html#a8ee1bd8676c4a02607eb02aa05718fe0", null ],
    [ "IsPotion", "struct_item_prototype.html#aae886316f6bca6f761b5a78891c39147", null ],
    [ "IsWeaponVellum", "struct_item_prototype.html#a7b24461090a54db8dd4bb74b1a7f983c", null ],
    [ "AllowableClass", "struct_item_prototype.html#a8bdbeed0ca2588d2a2f9e75a124d2d9f", null ],
    [ "AllowableRace", "struct_item_prototype.html#a6876582e3dfe49ea17c3de8c22d32da6", null ],
    [ "AmmoType", "struct_item_prototype.html#a341c1307b69555835f675989a731391c", null ],
    [ "ArcaneRes", "struct_item_prototype.html#a786b0a8ffe7a1baa78ff9aec904e1713", null ],
    [ "Area", "struct_item_prototype.html#a83736fc049a9fcb890ebc3afea8035fe", null ],
    [ "Armor", "struct_item_prototype.html#a3be7a8f762650086581c1249a1c28f03", null ],
    [ "BagFamily", "struct_item_prototype.html#aacedcbbee9793886477b8de13a2c453b", null ],
    [ "Block", "struct_item_prototype.html#a9f2d6a8fe96aabc3f68107b9af4ec1f6", null ],
    [ "Bonding", "struct_item_prototype.html#af04737c46ceae8af391f86124d595910", null ],
    [ "BuyCount", "struct_item_prototype.html#a11efff035f03fa8ecca86dc6465a1b18", null ],
    [ "BuyPrice", "struct_item_prototype.html#a0a38f8bfe3c274f1c4a2eb42a388b621", null ],
    [ "Class", "struct_item_prototype.html#a1818791c8035a7cd3cf441c79d34fb66", null ],
    [ "ContainerSlots", "struct_item_prototype.html#a6ae587f724d449f6d88c87f70fe736ce", null ],
    [ "Damage", "struct_item_prototype.html#a2fb87d40156ea20c541fff8a1a9b8dbc", null ],
    [ "Delay", "struct_item_prototype.html#ad2c37f1be4d37a8cff4aed9613b26dd3", null ],
    [ "Description", "struct_item_prototype.html#a5fa84d5f71012d5ab4abccbd80ab8c6d", null ],
    [ "DisenchantID", "struct_item_prototype.html#a7a22f8ff5b6866d5d6da4ed241029877", null ],
    [ "DisplayInfoID", "struct_item_prototype.html#a547d8fb983ba4c47d8803940a04fa521", null ],
    [ "Duration", "struct_item_prototype.html#ab140e47e48a6212ffea94e1b78b610a2", null ],
    [ "ExtraFlags", "struct_item_prototype.html#a7ef6d3f8ef9b09e856a4e5ff892e92ac", null ],
    [ "FireRes", "struct_item_prototype.html#a0e1a2a910859e1eca4ff5bd2ca4dbcc8", null ],
    [ "Flags", "struct_item_prototype.html#af7a886f8c92b50336befba9567265017", null ],
    [ "FoodType", "struct_item_prototype.html#a8b500820accc0beeb24cbdef8a8bf185", null ],
    [ "FrostRes", "struct_item_prototype.html#a0660af322701c3a61a10432ab89d1594", null ],
    [ "HolyRes", "struct_item_prototype.html#a25df82271693be75bc376b276f879d09", null ],
    [ "InventoryType", "struct_item_prototype.html#a912c943c6d40c3537c319eda5393604a", null ],
    [ "ItemId", "struct_item_prototype.html#a6b5f71419f275de1891c1464d72af61c", null ],
    [ "ItemLevel", "struct_item_prototype.html#a8eca0641dc694b14de2db0e23a341328", null ],
    [ "ItemSet", "struct_item_prototype.html#a5ddcc3b92f4c3727ab37e940be66c11a", null ],
    [ "ItemStat", "struct_item_prototype.html#ab3a0ee773ecee32859694112caf3a0fe", null ],
    [ "LanguageID", "struct_item_prototype.html#ac02ac49376134e5292fd57fc43193b9b", null ],
    [ "LockID", "struct_item_prototype.html#ad7bdce4f45615a65f952053a8740bdfc", null ],
    [ "Map", "struct_item_prototype.html#a9eaaa2c479fd2daccf2dad9c62c4d761", null ],
    [ "Material", "struct_item_prototype.html#ac43a174e15dfecb49c38ec67679d0056", null ],
    [ "MaxCount", "struct_item_prototype.html#a07443ac4eef65ec97ad27f8371ec9f0d", null ],
    [ "MaxDurability", "struct_item_prototype.html#a1734a8d08fc13cd9bf17cd86e0ffad88", null ],
    [ "MaxMoneyLoot", "struct_item_prototype.html#a9b47fd6b7deda2d32a111e78a9cbe821", null ],
    [ "MinMoneyLoot", "struct_item_prototype.html#a096794f35b1df72cd01cf2f0471f1b46", null ],
    [ "Name1", "struct_item_prototype.html#a1131aacd7736636d2a94b15855748e32", null ],
    [ "NatureRes", "struct_item_prototype.html#ad9231c58d95c3bc55c270777c0eefc7a", null ],
    [ "PageMaterial", "struct_item_prototype.html#a0701b7a5f53f69a76a50d8d071a95be5", null ],
    [ "PageText", "struct_item_prototype.html#a606496d473fb021e019a21cd1c2d2f86", null ],
    [ "Quality", "struct_item_prototype.html#af752a829222507c76f21aefd2d1cea0f", null ],
    [ "RandomProperty", "struct_item_prototype.html#a699b3cae664d2369387794da26b29ed3", null ],
    [ "RangedModRange", "struct_item_prototype.html#ab3b81f1f768a35ca12e663c9284e61c3", null ],
    [ "RequiredCityRank", "struct_item_prototype.html#a6bf3063dc67f2055cc6ce6afe6df7487", null ],
    [ "RequiredHonorRank", "struct_item_prototype.html#a25d0caa918b4479aa1f308e57ce6c89f", null ],
    [ "RequiredLevel", "struct_item_prototype.html#a96425f74ffa866a7a40707ed1bd462df", null ],
    [ "RequiredReputationFaction", "struct_item_prototype.html#abcefa6dacfd631403be91c8a76c94074", null ],
    [ "RequiredReputationRank", "struct_item_prototype.html#aaba50a6cb02b95957fc29d0f59830e24", null ],
    [ "RequiredSkill", "struct_item_prototype.html#a2bee0a9da3bd18ec0b51a51295f159d6", null ],
    [ "RequiredSkillRank", "struct_item_prototype.html#ad7684c702be4fdb4ce87d4a83092d215", null ],
    [ "RequiredSpell", "struct_item_prototype.html#a65d8751d828550b1f50c3d89c2f012ad", null ],
    [ "SellPrice", "struct_item_prototype.html#a00480054d61d98a985bf5fbfc46d52d7", null ],
    [ "ShadowRes", "struct_item_prototype.html#a420f3eb53d1eec4559e81adf0b5f8ad5", null ],
    [ "Sheath", "struct_item_prototype.html#ac3466639ee87f0d41428af5b819ab4f9", null ],
    [ "Spells", "struct_item_prototype.html#ae86c1831acf411538d41b9015630c326", null ],
    [ "Stackable", "struct_item_prototype.html#a57cb0d9af8df4bd4a67d886c98c59026", null ],
    [ "StartQuest", "struct_item_prototype.html#afbfa42d56f73fdb8602c6459cc5c7a4d", null ],
    [ "SubClass", "struct_item_prototype.html#ad09dd92ab1fcf2a86060fb36120459b3", null ]
];