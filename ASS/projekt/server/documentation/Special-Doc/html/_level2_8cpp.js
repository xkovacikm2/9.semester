var _level2_8cpp =
[
    [ "CreatureLinkType", "_level2_8cpp.html#ac3f9e35ebaff3f6f192e633f845331df", [
      [ "CREATURE_LINK_RAW", "_level2_8cpp.html#ac3f9e35ebaff3f6f192e633f845331dfa1c40202eb180fbc5868db2ebc58d93f2", null ],
      [ "CREATURE_LINK_GUID", "_level2_8cpp.html#ac3f9e35ebaff3f6f192e633f845331dfa45074cb023e9a96666609f1a854e1eb4", null ],
      [ "CREATURE_LINK_ENTRY", "_level2_8cpp.html#ac3f9e35ebaff3f6f192e633f845331dfa0424c87239a71a1ab05c44d5fd561a14", null ]
    ] ],
    [ "GameobjectLinkType", "_level2_8cpp.html#a050add9d6cf3d70cba03c05b324cf06f", [
      [ "GAMEOBJECT_LINK_RAW", "_level2_8cpp.html#a050add9d6cf3d70cba03c05b324cf06faf92853ea778e633c3d1ebdb58a53ac3f", null ],
      [ "GAMEOBJECT_LINK_GUID", "_level2_8cpp.html#a050add9d6cf3d70cba03c05b324cf06faa551f513479c6c4b30a21861aa09fff8", null ],
      [ "GAMEOBJECT_LINK_ENTRY", "_level2_8cpp.html#a050add9d6cf3d70cba03c05b324cf06fa7a69902e2f374d036a6d07039f8c3602", null ]
    ] ],
    [ "Helper_CreateWaypointFor", "_level2_8cpp.html#a66c3aaf0a686b78570e867e471c08d7a", null ],
    [ "UnsummonVisualWaypoints", "_level2_8cpp.html#acc4eeba81a3bcbeac0f9236be584b685", null ]
];