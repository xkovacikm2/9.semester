var _character_handler_8cpp =
[
    [ "LoginQueryHolder", "class_login_query_holder.html", "class_login_query_holder" ],
    [ "CharacterHandler", "class_character_handler.html", "class_character_handler" ],
    [ "CinematicsSkipMode", "_character_handler_8cpp.html#a5538fde73e2411a8b1e68584ca93f929", [
      [ "CINEMATICS_SKIP_NONE", "_character_handler_8cpp.html#a5538fde73e2411a8b1e68584ca93f929a34858259fd5de0fbaf13b4e3ac478ca9", null ],
      [ "CINEMATICS_SKIP_SAME_RACE", "_character_handler_8cpp.html#a5538fde73e2411a8b1e68584ca93f929a74a1e111318489dc9e5b74af3fbc8108", null ],
      [ "CINEMATICS_SKIP_ALL", "_character_handler_8cpp.html#a5538fde73e2411a8b1e68584ca93f929a709faaa3b56083f6a97cbbe0a7de854a", null ]
    ] ],
    [ "chrHandler", "_character_handler_8cpp.html#aa45ed215a0cf1f7284232430b9f9674d", null ]
];