var _battle_ground_w_s_8h =
[
    [ "BattleGroundWGScore", "class_battle_ground_w_g_score.html", "class_battle_ground_w_g_score" ],
    [ "BattleGroundWS", "class_battle_ground_w_s.html", "class_battle_ground_w_s" ],
    [ "BG_WS_FLAG_DROP_TIME", "_battle_ground_w_s_8h.html#a1602916911c09dff4cba87410438962f", null ],
    [ "BG_WS_FLAG_RESPAWN_TIME", "_battle_ground_w_s_8h.html#a147008a2a619eb65d562a789f9ec0a4b", null ],
    [ "BG_WS_MAX_TEAM_SCORE", "_battle_ground_w_s_8h.html#a5f989a7e419bbdb330f24d4a00a4e008", null ],
    [ "BG_WS_Events", "_battle_ground_w_s_8h.html#a6145482dfe4a795cbed7af3313c3599c", [
      [ "WS_EVENT_FLAG_A", "_battle_ground_w_s_8h.html#a6145482dfe4a795cbed7af3313c3599ca2ba1007de4af0de2d8ee044443c569fc", null ],
      [ "WS_EVENT_FLAG_H", "_battle_ground_w_s_8h.html#a6145482dfe4a795cbed7af3313c3599caf7333ef0c9b5985e6c6a2134c803553d", null ],
      [ "WS_EVENT_SPIRITGUIDES_SPAWN", "_battle_ground_w_s_8h.html#a6145482dfe4a795cbed7af3313c3599cabbd049e677414171fb15781a9edde6d6", null ]
    ] ],
    [ "BG_WS_FlagState", "_battle_ground_w_s_8h.html#a15d72f3f072bc0cabaf5f1bef80b64e0", [
      [ "BG_WS_FLAG_STATE_ON_BASE", "_battle_ground_w_s_8h.html#a15d72f3f072bc0cabaf5f1bef80b64e0afa30eb723a0561b597261f3fd8a12ade", null ],
      [ "BG_WS_FLAG_STATE_WAIT_RESPAWN", "_battle_ground_w_s_8h.html#a15d72f3f072bc0cabaf5f1bef80b64e0a8b6319a13f880b7f129cc942f66c45ca", null ],
      [ "BG_WS_FLAG_STATE_ON_PLAYER", "_battle_ground_w_s_8h.html#a15d72f3f072bc0cabaf5f1bef80b64e0a894046e91503d915c4d7f722ed988e6a", null ],
      [ "BG_WS_FLAG_STATE_ON_GROUND", "_battle_ground_w_s_8h.html#a15d72f3f072bc0cabaf5f1bef80b64e0a02eaf89f2607dfe9d7b222d7a142642d", null ]
    ] ],
    [ "BG_WS_Graveyards", "_battle_ground_w_s_8h.html#afb194929fa7fa28ba1362df96d98f48c", [
      [ "WS_GRAVEYARD_FLAGROOM_ALLIANCE", "_battle_ground_w_s_8h.html#afb194929fa7fa28ba1362df96d98f48ca0393e94e3ca8bdb8f88c188ae4651300", null ],
      [ "WS_GRAVEYARD_FLAGROOM_HORDE", "_battle_ground_w_s_8h.html#afb194929fa7fa28ba1362df96d98f48ca40a02f455e9bb74cef1f2955d86d2f7a", null ],
      [ "WS_GRAVEYARD_MAIN_ALLIANCE", "_battle_ground_w_s_8h.html#afb194929fa7fa28ba1362df96d98f48ca2f36e1a66bfe840de8d0b2c4f24c122d", null ],
      [ "WS_GRAVEYARD_MAIN_HORDE", "_battle_ground_w_s_8h.html#afb194929fa7fa28ba1362df96d98f48caabc7b651d7611a592759d4c5bd37cdfe", null ]
    ] ],
    [ "BG_WS_Sound", "_battle_ground_w_s_8h.html#a57475685e7e9485731450b3d705d5ca5", [
      [ "BG_WS_SOUND_FLAG_CAPTURED_ALLIANCE", "_battle_ground_w_s_8h.html#a57475685e7e9485731450b3d705d5ca5a997f8b8527dfbcb80664778885733896", null ],
      [ "BG_WS_SOUND_FLAG_CAPTURED_HORDE", "_battle_ground_w_s_8h.html#a57475685e7e9485731450b3d705d5ca5a32ffcf2c65e6cbacbfdb5790460cf65f", null ],
      [ "BG_WS_SOUND_FLAG_PLACED", "_battle_ground_w_s_8h.html#a57475685e7e9485731450b3d705d5ca5a308b9670204904fe7404572046075eb4", null ],
      [ "BG_WS_SOUND_FLAG_RETURNED", "_battle_ground_w_s_8h.html#a57475685e7e9485731450b3d705d5ca5abd9a65bbcd72bd56f8bdc9f8da47585a", null ],
      [ "BG_WS_SOUND_HORDE_FLAG_PICKED_UP", "_battle_ground_w_s_8h.html#a57475685e7e9485731450b3d705d5ca5aa29d2519be11c8f28b7e1d38c1b43a2f", null ],
      [ "BG_WS_SOUND_ALLIANCE_FLAG_PICKED_UP", "_battle_ground_w_s_8h.html#a57475685e7e9485731450b3d705d5ca5aae5caaadc49a60a2da8a1e9490c8f67f", null ],
      [ "BG_WS_SOUND_FLAGS_RESPAWNED", "_battle_ground_w_s_8h.html#a57475685e7e9485731450b3d705d5ca5a566491875872716060e137f006eaad5a", null ]
    ] ],
    [ "BG_WS_SpellId", "_battle_ground_w_s_8h.html#a3c368811f9c49f0276a9d115a8672d4b", [
      [ "BG_WS_SPELL_WARSONG_FLAG", "_battle_ground_w_s_8h.html#a3c368811f9c49f0276a9d115a8672d4ba3ff5e866002dc9c96e76b65bd922c110", null ],
      [ "BG_WS_SPELL_WARSONG_FLAG_DROPPED", "_battle_ground_w_s_8h.html#a3c368811f9c49f0276a9d115a8672d4bab8f74356870d6e130953b79269fa416a", null ],
      [ "BG_WS_SPELL_SILVERWING_FLAG", "_battle_ground_w_s_8h.html#a3c368811f9c49f0276a9d115a8672d4baba32f31f837fa712a15bc8f298005437", null ],
      [ "BG_WS_SPELL_SILVERWING_FLAG_DROPPED", "_battle_ground_w_s_8h.html#a3c368811f9c49f0276a9d115a8672d4ba553cd677ac7e0285223bea1c0ac3fcdf", null ]
    ] ],
    [ "BG_WS_WorldStates", "_battle_ground_w_s_8h.html#af4cae60fa452f9a1504325480cf4d6b6", [
      [ "BG_WS_FLAG_UNK_ALLIANCE", "_battle_ground_w_s_8h.html#af4cae60fa452f9a1504325480cf4d6b6a38ca3ea95fbb5b6a6fe34666ac4940c6", null ],
      [ "BG_WS_FLAG_UNK_HORDE", "_battle_ground_w_s_8h.html#af4cae60fa452f9a1504325480cf4d6b6a3ff589e295e52d6d03d07a428cbd89f3", null ],
      [ "BG_WS_FLAG_CAPTURES_ALLIANCE", "_battle_ground_w_s_8h.html#af4cae60fa452f9a1504325480cf4d6b6ae883ddf71516900be2d33631bdd92ad2", null ],
      [ "BG_WS_FLAG_CAPTURES_HORDE", "_battle_ground_w_s_8h.html#af4cae60fa452f9a1504325480cf4d6b6a9f7b1b3d5f9c877c281639e2ce27b0a8", null ],
      [ "BG_WS_FLAG_CAPTURES_MAX", "_battle_ground_w_s_8h.html#af4cae60fa452f9a1504325480cf4d6b6a53e88b178ab76de3a608b254901a9052", null ],
      [ "BG_WS_FLAG_STATE_HORDE", "_battle_ground_w_s_8h.html#af4cae60fa452f9a1504325480cf4d6b6a77055024580df28d74d7ccc5296dcb2e", null ],
      [ "BG_WS_FLAG_STATE_ALLIANCE", "_battle_ground_w_s_8h.html#af4cae60fa452f9a1504325480cf4d6b6a7956b3f40570a592d3d2fb921efa1d27", null ]
    ] ],
    [ "BG_WSG_FlagCapturedHonor", "_battle_ground_w_s_8h.html#a2462291cb6e5b65d1dab4daccdc9a3d3", null ],
    [ "BG_WSG_WinMatchHonor", "_battle_ground_w_s_8h.html#a461c6ebf3726c39f79754d7ad5b58f03", null ]
];