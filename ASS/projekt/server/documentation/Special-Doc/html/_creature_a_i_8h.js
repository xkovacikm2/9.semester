var _creature_a_i_8h =
[
    [ "CreatureAI", "class_creature_a_i.html", "class_creature_a_i" ],
    [ "SelectableAI", "struct_selectable_a_i.html", "struct_selectable_a_i" ],
    [ "CreatureAIFactory", "struct_creature_a_i_factory.html", "struct_creature_a_i_factory" ],
    [ "TIME_INTERVAL_LOOK", "_creature_a_i_8h.html#a8de08a341cdd207e19eef425c2f5f152", null ],
    [ "VISIBILITY_RANGE", "_creature_a_i_8h.html#a5548091cca58d569e4226b6915ec8cb2", null ],
    [ "CreatureAICreator", "_creature_a_i_8h.html#a092af9e866ccac7e3496a54e696bab27", null ],
    [ "CreatureAIRegistry", "_creature_a_i_8h.html#a37776974996e0e01328373654bf591fe", null ],
    [ "CreatureAIRepository", "_creature_a_i_8h.html#a8fa6df63944804af39e22a3653f9091e", null ],
    [ "AIEventType", "_creature_a_i_8h.html#a06206bdc666d7f723662f7ff6b90ad92", [
      [ "AI_EVENT_JUST_DIED", "_creature_a_i_8h.html#a06206bdc666d7f723662f7ff6b90ad92af41e9046d955857b4e1512127ae6f021", null ],
      [ "AI_EVENT_CRITICAL_HEALTH", "_creature_a_i_8h.html#a06206bdc666d7f723662f7ff6b90ad92ad688c1d6a1ee6834ddcf81be7c59f94a", null ],
      [ "AI_EVENT_LOST_HEALTH", "_creature_a_i_8h.html#a06206bdc666d7f723662f7ff6b90ad92af0984070268ab474df78886810b388ff", null ],
      [ "AI_EVENT_LOST_SOME_HEALTH", "_creature_a_i_8h.html#a06206bdc666d7f723662f7ff6b90ad92a7217d42901fd5e4b8c0fc56d25d0d9e4", null ],
      [ "AI_EVENT_GOT_FULL_HEALTH", "_creature_a_i_8h.html#a06206bdc666d7f723662f7ff6b90ad92a82377277d507724579b6f4d0ac97f927", null ],
      [ "AI_EVENT_CUSTOM_EVENTAI_A", "_creature_a_i_8h.html#a06206bdc666d7f723662f7ff6b90ad92a874183413fbe60fa21a16972acf3b65b", null ],
      [ "AI_EVENT_CUSTOM_EVENTAI_B", "_creature_a_i_8h.html#a06206bdc666d7f723662f7ff6b90ad92a20158609815a921fb649ac481c5b3fbd", null ],
      [ "AI_EVENT_GOT_CCED", "_creature_a_i_8h.html#a06206bdc666d7f723662f7ff6b90ad92a80c5b5d6bf25c75fde2ffb7c4ec50caf", null ],
      [ "MAXIMAL_AI_EVENT_EVENTAI", "_creature_a_i_8h.html#a06206bdc666d7f723662f7ff6b90ad92a1da328ae411c7a36e3dee16d2710898d", null ],
      [ "AI_EVENT_CALL_ASSISTANCE", "_creature_a_i_8h.html#a06206bdc666d7f723662f7ff6b90ad92aa7730cf991e2f4ca1ba6bb12c357aff9", null ],
      [ "AI_EVENT_START_ESCORT", "_creature_a_i_8h.html#a06206bdc666d7f723662f7ff6b90ad92a5dcfdf087c619ddfc9ef0afbe0275191", null ],
      [ "AI_EVENT_START_ESCORT_B", "_creature_a_i_8h.html#a06206bdc666d7f723662f7ff6b90ad92a9cb4edcac0b97c029b936fc019cc4107", null ],
      [ "AI_EVENT_START_EVENT", "_creature_a_i_8h.html#a06206bdc666d7f723662f7ff6b90ad92ab6e8f2ef2b1f6f2666afc6c4d9a58fc3", null ],
      [ "AI_EVENT_START_EVENT_A", "_creature_a_i_8h.html#a06206bdc666d7f723662f7ff6b90ad92a331b9b020ab71fd25a275804fab8f669", null ],
      [ "AI_EVENT_START_EVENT_B", "_creature_a_i_8h.html#a06206bdc666d7f723662f7ff6b90ad92a340765c81ea35ff8a126e63e574985f8", null ],
      [ "AI_EVENT_CUSTOM_A", "_creature_a_i_8h.html#a06206bdc666d7f723662f7ff6b90ad92ac018dfb4a21cef010d77c8746c61777c", null ],
      [ "AI_EVENT_CUSTOM_B", "_creature_a_i_8h.html#a06206bdc666d7f723662f7ff6b90ad92a80c95959bcfc5f0d56a5f7c4402a819e", null ],
      [ "AI_EVENT_CUSTOM_C", "_creature_a_i_8h.html#a06206bdc666d7f723662f7ff6b90ad92ac70cea1409919b9a33b06f38e7fdb0fe", null ],
      [ "AI_EVENT_CUSTOM_D", "_creature_a_i_8h.html#a06206bdc666d7f723662f7ff6b90ad92a62760c335438feb359a9f18ea2e8b295", null ],
      [ "AI_EVENT_CUSTOM_E", "_creature_a_i_8h.html#a06206bdc666d7f723662f7ff6b90ad92a522e85a772aa804cf6a78e8ea9bcd7bd", null ],
      [ "AI_EVENT_CUSTOM_F", "_creature_a_i_8h.html#a06206bdc666d7f723662f7ff6b90ad92a9b2c4b096472734094dcf4a857a9de00", null ]
    ] ],
    [ "CanCastResult", "_creature_a_i_8h.html#aa50449ff87fcca96bcdb3d3155ba0a8d", [
      [ "CAST_OK", "_creature_a_i_8h.html#aa50449ff87fcca96bcdb3d3155ba0a8da51eea970b1e8774ed15a60fa02e29d49", null ],
      [ "CAST_FAIL_IS_CASTING", "_creature_a_i_8h.html#aa50449ff87fcca96bcdb3d3155ba0a8daa605fb1ecf3fd77b7a565f6fd17a7634", null ],
      [ "CAST_FAIL_OTHER", "_creature_a_i_8h.html#aa50449ff87fcca96bcdb3d3155ba0a8da4fd8862289d11a6865476dd1527312e1", null ],
      [ "CAST_FAIL_TOO_FAR", "_creature_a_i_8h.html#aa50449ff87fcca96bcdb3d3155ba0a8da1a8c8e00ba64f32d39e53db36f3bcce0", null ],
      [ "CAST_FAIL_TOO_CLOSE", "_creature_a_i_8h.html#aa50449ff87fcca96bcdb3d3155ba0a8dacaed49a04dd86dab638a4813f3857f8e", null ],
      [ "CAST_FAIL_POWER", "_creature_a_i_8h.html#aa50449ff87fcca96bcdb3d3155ba0a8dad407b138672ff0e3eae1ad4ba9df4034", null ],
      [ "CAST_FAIL_STATE", "_creature_a_i_8h.html#aa50449ff87fcca96bcdb3d3155ba0a8da072e840f96bd53c5a61f7e2faddef18d", null ],
      [ "CAST_FAIL_TARGET_AURA", "_creature_a_i_8h.html#aa50449ff87fcca96bcdb3d3155ba0a8da9418a338d426a75e239e3eed341bfcb5", null ],
      [ "CAST_FAIL_NO_LOS", "_creature_a_i_8h.html#aa50449ff87fcca96bcdb3d3155ba0a8da5374da0c2b0bcb161084b35e2bee1093", null ],
      [ "CAST_FAIL_SILENCED", "_creature_a_i_8h.html#aa50449ff87fcca96bcdb3d3155ba0a8da85623664ac4c003e381d0ae1d52ce4a0", null ]
    ] ],
    [ "CastFlags", "_creature_a_i_8h.html#a561c3f921983946b556567b85aeab276", [
      [ "CAST_INTERRUPT_PREVIOUS", "_creature_a_i_8h.html#a561c3f921983946b556567b85aeab276a988a1a51bbd40239ab37f0e804683ed1", null ],
      [ "CAST_TRIGGERED", "_creature_a_i_8h.html#a561c3f921983946b556567b85aeab276accf3e130736d4a9eff74c956fdd417bb", null ],
      [ "CAST_FORCE_CAST", "_creature_a_i_8h.html#a561c3f921983946b556567b85aeab276a5dc40435eb825ce3b67e7067eb9f0e34", null ],
      [ "CAST_NO_MELEE_IF_OOM", "_creature_a_i_8h.html#a561c3f921983946b556567b85aeab276a297961b98edfffcdf5a01249dbea1258", null ],
      [ "CAST_FORCE_TARGET_SELF", "_creature_a_i_8h.html#a561c3f921983946b556567b85aeab276ac9f1838079c7312a0d7c829f4169175d", null ],
      [ "CAST_AURA_NOT_PRESENT", "_creature_a_i_8h.html#a561c3f921983946b556567b85aeab276acfe71b4aa87723eb89ae23c8375882be", null ]
    ] ],
    [ "CombatMovementFlags", "_creature_a_i_8h.html#ae9fb46d429732e3a5103c18ea8122161", [
      [ "CM_SCRIPT", "_creature_a_i_8h.html#ae9fb46d429732e3a5103c18ea8122161a2f759a516cb95faaad268c704cceec02", null ],
      [ "CM_SPELL", "_creature_a_i_8h.html#ae9fb46d429732e3a5103c18ea8122161a4c4dfd6619d826f4d742c572c518eff4", null ]
    ] ],
    [ "Permitions", "_creature_a_i_8h.html#aa6522cb6549e0e65065233f8a527e97f", [
      [ "PERMIT_BASE_NO", "_creature_a_i_8h.html#aa6522cb6549e0e65065233f8a527e97fa3ea8686bf6c00259f435df8f34da17c0", null ],
      [ "PERMIT_BASE_IDLE", "_creature_a_i_8h.html#aa6522cb6549e0e65065233f8a527e97faa30fb6bbae2f275b7c269df6b7bac2c0", null ],
      [ "PERMIT_BASE_REACTIVE", "_creature_a_i_8h.html#aa6522cb6549e0e65065233f8a527e97fa8781eb0d588151b618bd98b03423dc58", null ],
      [ "PERMIT_BASE_PROACTIVE", "_creature_a_i_8h.html#aa6522cb6549e0e65065233f8a527e97fa06a019c60b174cae59e682299512d78c", null ],
      [ "PERMIT_BASE_FACTION_SPECIFIC", "_creature_a_i_8h.html#aa6522cb6549e0e65065233f8a527e97fa9935ee057ed82483ff6a6363709db059", null ],
      [ "PERMIT_BASE_SPECIAL", "_creature_a_i_8h.html#aa6522cb6549e0e65065233f8a527e97fa01dc7d3add2acacf44721592378df415", null ]
    ] ]
];