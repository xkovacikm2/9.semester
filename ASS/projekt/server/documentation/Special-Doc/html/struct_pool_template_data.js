var struct_pool_template_data =
[
    [ "PoolTemplateData", "struct_pool_template_data.html#aa90577ea2689add61db9af73dafc406f", null ],
    [ "CanBeSpawnedAtMap", "struct_pool_template_data.html#add366f1fb0c0a5cae13376060c85aa18", null ],
    [ "AutoSpawn", "struct_pool_template_data.html#a86ddf390a0373de108db3dc5e9c684e5", null ],
    [ "description", "struct_pool_template_data.html#a65e8eb03b6317b0bd92ff664b9786a0c", null ],
    [ "mapEntry", "struct_pool_template_data.html#ad52a6adb896c31b736bffb4c69894fd8", null ],
    [ "MaxLimit", "struct_pool_template_data.html#a38ba12a67e99cabba2388a63f12a2b53", null ]
];