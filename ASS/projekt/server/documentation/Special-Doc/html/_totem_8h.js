var _totem_8h =
[
    [ "Totem", "class_totem.html", "class_totem" ],
    [ "TotemType", "_totem_8h.html#a65151889ab4b03cf02268cc7a7ff0ba3", [
      [ "TOTEM_PASSIVE", "_totem_8h.html#a65151889ab4b03cf02268cc7a7ff0ba3a63f155f3367aa262aae567b57c623069", null ],
      [ "TOTEM_ACTIVE", "_totem_8h.html#a65151889ab4b03cf02268cc7a7ff0ba3a3c3ecb49d653993abf86f289019e27b0", null ],
      [ "TOTEM_STATUE", "_totem_8h.html#a65151889ab4b03cf02268cc7a7ff0ba3ab0fbf32f7d371a34f276dfdf6552e300", null ]
    ] ]
];