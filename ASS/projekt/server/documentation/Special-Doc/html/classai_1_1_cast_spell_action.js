var classai_1_1_cast_spell_action =
[
    [ "CastSpellAction", "classai_1_1_cast_spell_action.html#a527d61396b286220148e6c4ea79d8cf9", null ],
    [ "Execute", "classai_1_1_cast_spell_action.html#a115dd4424ceb1331514e9e6c2e7a788f", null ],
    [ "getPrerequisites", "classai_1_1_cast_spell_action.html#a8303184aa1f375d84d20e44466bf2b7b", null ],
    [ "GetTargetName", "classai_1_1_cast_spell_action.html#a38c68ba2b230150d06403a9769fc0e59", null ],
    [ "getThreatType", "classai_1_1_cast_spell_action.html#a8cb570553c7c29033d4375cb33a49952", null ],
    [ "isPossible", "classai_1_1_cast_spell_action.html#aae99756077ada838f56a92e55cf8b98d", null ],
    [ "isUseful", "classai_1_1_cast_spell_action.html#a99f5b9947cefc7185d3fcd6f5b1f80c6", null ],
    [ "range", "classai_1_1_cast_spell_action.html#ae8eea09b4f676d04b28bc16db05e18e0", null ],
    [ "spell", "classai_1_1_cast_spell_action.html#a26dc47efeef561ea0f575064e5ab88bd", null ]
];