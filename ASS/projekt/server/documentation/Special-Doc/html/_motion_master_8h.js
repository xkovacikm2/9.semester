var _motion_master_8h =
[
    [ "MotionMaster", "class_motion_master.html", "class_motion_master" ],
    [ "VISUAL_WAYPOINT", "_motion_master_8h.html#af0544189c7fc748059c3075861102b40", null ],
    [ "MMCleanFlag", "_motion_master_8h.html#a1a14fe34f2e738c20bcc4e3c5cc71de5", [
      [ "MMCF_NONE", "_motion_master_8h.html#a1a14fe34f2e738c20bcc4e3c5cc71de5a94162d33d05261960508206156596b79", null ],
      [ "MMCF_UPDATE", "_motion_master_8h.html#a1a14fe34f2e738c20bcc4e3c5cc71de5ab78b5bb4f06ecc2246991dc5660faca5", null ],
      [ "MMCF_RESET", "_motion_master_8h.html#a1a14fe34f2e738c20bcc4e3c5cc71de5a52a6ffecbabca2148ffb0ad5ac3009f2", null ]
    ] ],
    [ "MovementGeneratorType", "_motion_master_8h.html#a4f12806b915efea564eae942207002f9", [
      [ "IDLE_MOTION_TYPE", "_motion_master_8h.html#a4f12806b915efea564eae942207002f9a35e951ebafcab180b4bd926e5760803e", null ],
      [ "RANDOM_MOTION_TYPE", "_motion_master_8h.html#a4f12806b915efea564eae942207002f9aec7cad6c9576df1fca0eb4fa073e5ff2", null ],
      [ "WAYPOINT_MOTION_TYPE", "_motion_master_8h.html#a4f12806b915efea564eae942207002f9a6158029efb794f0a586a62f35cef5ae9", null ],
      [ "MAX_DB_MOTION_TYPE", "_motion_master_8h.html#a4f12806b915efea564eae942207002f9a06257c74c322baaae86bc2bfd1dd359b", null ],
      [ "CONFUSED_MOTION_TYPE", "_motion_master_8h.html#a4f12806b915efea564eae942207002f9aaf0253f8d6a3f2a5219bcf9db3494611", null ],
      [ "CHASE_MOTION_TYPE", "_motion_master_8h.html#a4f12806b915efea564eae942207002f9a4a7484b1c685865cef43981b61d3a881", null ],
      [ "HOME_MOTION_TYPE", "_motion_master_8h.html#a4f12806b915efea564eae942207002f9ab50c687899070d507c7858b481807426", null ],
      [ "FLIGHT_MOTION_TYPE", "_motion_master_8h.html#a4f12806b915efea564eae942207002f9ad1cbffaeac9c9f09b8aeedbaadef54f4", null ],
      [ "POINT_MOTION_TYPE", "_motion_master_8h.html#a4f12806b915efea564eae942207002f9ad05105e19ca15912eceb8a1d856c7467", null ],
      [ "FLEEING_MOTION_TYPE", "_motion_master_8h.html#a4f12806b915efea564eae942207002f9a96bdbc5913c5eb73d6eb7b28bbe9b05e", null ],
      [ "DISTRACT_MOTION_TYPE", "_motion_master_8h.html#a4f12806b915efea564eae942207002f9a35aa056191b760ab18515a11f00f1759", null ],
      [ "ASSISTANCE_MOTION_TYPE", "_motion_master_8h.html#a4f12806b915efea564eae942207002f9a32a9419cbc2b947d521e0ea050020a9c", null ],
      [ "ASSISTANCE_DISTRACT_MOTION_TYPE", "_motion_master_8h.html#a4f12806b915efea564eae942207002f9aec81245314a6864c0e3561d3bfc04ec4", null ],
      [ "TIMED_FLEEING_MOTION_TYPE", "_motion_master_8h.html#a4f12806b915efea564eae942207002f9a0358690810ecf133811aa6b73bc086fc", null ],
      [ "FOLLOW_MOTION_TYPE", "_motion_master_8h.html#a4f12806b915efea564eae942207002f9ae99e76e1e80feb6d7ff6e1df01e98e9c", null ],
      [ "EFFECT_MOTION_TYPE", "_motion_master_8h.html#a4f12806b915efea564eae942207002f9af4917c7ba7d6e157bc3b8c59773cc6ac", null ],
      [ "EXTERNAL_WAYPOINT_MOVE", "_motion_master_8h.html#a4f12806b915efea564eae942207002f9a884a6148433799d4d735b275a745baab", null ],
      [ "EXTERNAL_WAYPOINT_MOVE_START", "_motion_master_8h.html#a4f12806b915efea564eae942207002f9aee40317c068554a93f0a2b7c83ac3617", null ],
      [ "EXTERNAL_WAYPOINT_FINISHED_LAST", "_motion_master_8h.html#a4f12806b915efea564eae942207002f9a22b1e104ae5d90a547eab8e55841bbb1", null ]
    ] ]
];