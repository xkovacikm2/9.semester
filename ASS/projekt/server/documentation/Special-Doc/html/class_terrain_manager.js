var class_terrain_manager =
[
    [ "GetAreaFlag", "class_terrain_manager.html#aa06aa1b86a64a0edf55d7996367aeec2", null ],
    [ "GetAreaId", "class_terrain_manager.html#a8de89ff029dc556c69843fca073c062e", null ],
    [ "GetZoneAndAreaId", "class_terrain_manager.html#a3e41840a1056e50f5c481ec0d67dfe44", null ],
    [ "GetZoneId", "class_terrain_manager.html#a49d840e87914fd4915093cc2eb6676a7", null ],
    [ "LoadTerrain", "class_terrain_manager.html#a30c165e8569c32dd6807dda75a0b5e62", null ],
    [ "UnloadAll", "class_terrain_manager.html#aef5b808546a33a322c475a0eb21ec2b2", null ],
    [ "UnloadTerrain", "class_terrain_manager.html#a0fde97177e9d6e46ec70b21709b9ae54", null ],
    [ "Update", "class_terrain_manager.html#affb8c5b67bb8ff88efc3611773de0fc6", null ],
    [ "MaNGOS::OperatorNew< TerrainManager >", "class_terrain_manager.html#a445192d688d655a7ed7ab120f93669f8", null ]
];