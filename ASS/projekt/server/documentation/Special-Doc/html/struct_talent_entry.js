var struct_talent_entry =
[
    [ "Col", "struct_talent_entry.html#a0bc589c5b1425d5e889cd290b805169e", null ],
    [ "DependsOn", "struct_talent_entry.html#a7b1aa1384777b8d94dceed3c387bb628", null ],
    [ "DependsOnRank", "struct_talent_entry.html#a608f232669981da3423b959479ca8683", null ],
    [ "DependsOnSpell", "struct_talent_entry.html#ac026743279b0eb468146cc992a179fc6", null ],
    [ "RankID", "struct_talent_entry.html#ab9216f97d2b7c7fe0ff4336b80869b88", null ],
    [ "Row", "struct_talent_entry.html#a136b889d2e8875b01fc7aa2be06847ac", null ],
    [ "TalentID", "struct_talent_entry.html#aa769574106b0e5afdbc7c71c51c3b9a2", null ],
    [ "TalentTab", "struct_talent_entry.html#acefe6f8c0a5b3893e2a9f828d500df68", null ]
];