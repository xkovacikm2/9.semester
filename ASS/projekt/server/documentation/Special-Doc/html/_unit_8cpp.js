var _unit_8cpp =
[
    [ "PetOwnerKilledUnitHelper", "struct_pet_owner_killed_unit_helper.html", "struct_pet_owner_killed_unit_helper" ],
    [ "CombatStopWithPetsHelper", "struct_combat_stop_with_pets_helper.html", "struct_combat_stop_with_pets_helper" ],
    [ "IsAttackingPlayerHelper", "struct_is_attacking_player_helper.html", "struct_is_attacking_player_helper" ],
    [ "SetSpeedRateHelper", "struct_set_speed_rate_helper.html", "struct_set_speed_rate_helper" ],
    [ "ProcTriggeredData", "struct_proc_triggered_data.html", "struct_proc_triggered_data" ],
    [ "SetPvPHelper", "struct_set_pv_p_helper.html", "struct_set_pv_p_helper" ],
    [ "StopAttackFactionHelper", "struct_stop_attack_faction_helper.html", "struct_stop_attack_faction_helper" ],
    [ "RelocationNotifyEvent", "class_relocation_notify_event.html", "class_relocation_notify_event" ],
    [ "ProcTriggeredList", "_unit_8cpp.html#af5ec5d5b1ba4d4454c1bdcbc139de758", null ],
    [ "RemoveSpellList", "_unit_8cpp.html#a440e206ac70ab8683e767afeb465da4c", null ],
    [ "createProcExtendMask", "group__game.html#gafb539370dad036ff97905efdbb4270c0", null ],
    [ "getMSTime", "_unit_8cpp.html#a4476c8e13ea790c3536f2749105f9cb9", null ],
    [ "baseMoveSpeed", "group__game.html#ga2ede1cce0416c74db2d4599bab36eb57", null ]
];