var _pet_8h =
[
    [ "PetSpell", "struct_pet_spell.html", "struct_pet_spell" ],
    [ "Pet", "class_pet.html", "class_pet" ],
    [ "ACTIVE_SPELLS_MAX", "_pet_8h.html#a2eba0c745572c0d75f7127597344e769", null ],
    [ "HAPPINESS_LEVEL_SIZE", "_pet_8h.html#a76712c47838e51c9fe44aa5043816d40", null ],
    [ "MAX_PET_STABLES", "_pet_8h.html#a4a834992e2dfccc5010094500092dfbe", null ],
    [ "PET_FOLLOW_ANGLE", "_pet_8h.html#a346068e848cb7777695f170d2c7958de", null ],
    [ "PET_FOLLOW_DIST", "_pet_8h.html#a83e96c6d024ceb5d1189baac7b823d5d", null ],
    [ "AutoSpellList", "_pet_8h.html#a85baf40bd776b1c20b66ba7ad78e0356", null ],
    [ "PetSpellMap", "_pet_8h.html#a9b2c3d313ce5c6c6ae166d57880495bd", null ],
    [ "TeachSpellMap", "_pet_8h.html#ab548fcd2156144f873742fefd36e1e52", null ],
    [ "ActionFeedback", "_pet_8h.html#aeb5551b3f37a2573e008d2d5d865359d", [
      [ "FEEDBACK_PET_NONE", "_pet_8h.html#aeb5551b3f37a2573e008d2d5d865359dae713d68bc1efefc5b315329adfca47ec", null ],
      [ "FEEDBACK_PET_DEAD", "_pet_8h.html#aeb5551b3f37a2573e008d2d5d865359da482a5e311921dbd1dfab7521309f18c1", null ],
      [ "FEEDBACK_NOTHING_TO_ATT", "_pet_8h.html#aeb5551b3f37a2573e008d2d5d865359da3706032c84734c5ac7a062f213a55c5d", null ],
      [ "FEEDBACK_CANT_ATT_TARGET", "_pet_8h.html#aeb5551b3f37a2573e008d2d5d865359da7ab57d203acda1a7175c48b188c1cbf4", null ],
      [ "FEEDBACK_NO_PATH_TO", "_pet_8h.html#aeb5551b3f37a2573e008d2d5d865359da5019ba56f7443c2eb8a31a3442b77a7e", null ]
    ] ],
    [ "HappinessState", "_pet_8h.html#addd416d6eeaa9cd92339aa437caedb3e", [
      [ "UNHAPPY", "_pet_8h.html#addd416d6eeaa9cd92339aa437caedb3eae373c6b52d4a8b45084f4a276fb6d6c5", null ],
      [ "CONTENT", "_pet_8h.html#addd416d6eeaa9cd92339aa437caedb3ea59ae17a687995046ac7025dcc53156e3", null ],
      [ "HAPPY", "_pet_8h.html#addd416d6eeaa9cd92339aa437caedb3ea5592cf91bd70788e64c412b3cc642e2c", null ]
    ] ],
    [ "LoyaltyLevel", "_pet_8h.html#afd206bef60bf6a74b2dfbbb0c7271555", [
      [ "REBELLIOUS", "_pet_8h.html#afd206bef60bf6a74b2dfbbb0c7271555a91b15e6e0beca48448532532831ad3dd", null ],
      [ "UNRULY", "_pet_8h.html#afd206bef60bf6a74b2dfbbb0c7271555aaa2d79194bf64e059901159fcfc43d8d", null ],
      [ "SUBMISSIVE", "_pet_8h.html#afd206bef60bf6a74b2dfbbb0c7271555ab1a934335c5785dd0badc03090f1361e", null ],
      [ "DEPENDABLE", "_pet_8h.html#afd206bef60bf6a74b2dfbbb0c7271555a11b61097560f4bd97861f776ea846ed9", null ],
      [ "FAITHFUL", "_pet_8h.html#afd206bef60bf6a74b2dfbbb0c7271555a3c458e9ef3804fa966744f66401513ef", null ],
      [ "BEST_FRIEND", "_pet_8h.html#afd206bef60bf6a74b2dfbbb0c7271555aa7a8a41aa6f8ab2fb877a2a3097878b5", null ]
    ] ],
    [ "PetDatabaseStatus", "_pet_8h.html#aca5e828743491ed810d832e371e5d289", [
      [ "PET_DB_NO_PET", "_pet_8h.html#aca5e828743491ed810d832e371e5d289a61ab4c325c98a91d84b69510e841e87e", null ],
      [ "PET_DB_DEAD", "_pet_8h.html#aca5e828743491ed810d832e371e5d289a4e3088a24e6441da9a6aece46c7f39c8", null ],
      [ "PET_DB_ALIVE", "_pet_8h.html#aca5e828743491ed810d832e371e5d289ad8836404e886c8b6f0a056ea4cd6d016", null ]
    ] ],
    [ "PetModeFlags", "_pet_8h.html#a484a86780410fb960031b81ec70edf06", [
      [ "PET_MODE_UNKNOWN_0", "_pet_8h.html#a484a86780410fb960031b81ec70edf06af6c4825efee55234ffeb0f673bf34991", null ],
      [ "PET_MODE_UNKNOWN_2", "_pet_8h.html#a484a86780410fb960031b81ec70edf06aeec6259c565c7190c19593b343bcc088", null ],
      [ "PET_MODE_DISABLE_ACTIONS", "_pet_8h.html#a484a86780410fb960031b81ec70edf06aa6d6597d8137d6e2d5ca91a28dca5afc", null ],
      [ "PET_MODE_DEFAULT", "_pet_8h.html#a484a86780410fb960031b81ec70edf06a541261899d040191b719300aa6710dcb", null ]
    ] ],
    [ "PetNameInvalidReason", "_pet_8h.html#ad776c249625bcf827889070cc5fb1c02", [
      [ "PET_NAME_SUCCESS", "_pet_8h.html#ad776c249625bcf827889070cc5fb1c02a876c4817f2f5601e4bbe4202ec30e31c", null ],
      [ "PET_NAME_INVALID", "_pet_8h.html#ad776c249625bcf827889070cc5fb1c02a2243c96cca5ce23e6c86911e675beaff", null ],
      [ "PET_NAME_NO_NAME", "_pet_8h.html#ad776c249625bcf827889070cc5fb1c02a1b5cac781e3e4bd51eb14fcc8e864f2a", null ],
      [ "PET_NAME_TOO_SHORT", "_pet_8h.html#ad776c249625bcf827889070cc5fb1c02ac08c847666b4c838fe88fb895e6dee06", null ],
      [ "PET_NAME_TOO_LONG", "_pet_8h.html#ad776c249625bcf827889070cc5fb1c02ae09f07fb824ab93181b0900029dbba9b", null ],
      [ "PET_NAME_MIXED_LANGUAGES", "_pet_8h.html#ad776c249625bcf827889070cc5fb1c02a9710fbd1a4a966af4effe52716bbb030", null ],
      [ "PET_NAME_PROFANE", "_pet_8h.html#ad776c249625bcf827889070cc5fb1c02af3946919a10939b5d07298c131b12806", null ],
      [ "PET_NAME_RESERVED", "_pet_8h.html#ad776c249625bcf827889070cc5fb1c02a72d261ed733ff58777ee84bface018e4", null ],
      [ "PET_NAME_THREE_CONSECUTIVE", "_pet_8h.html#ad776c249625bcf827889070cc5fb1c02a8eb1c04076f139095a968168a3896794", null ],
      [ "PET_NAME_INVALID_SPACE", "_pet_8h.html#ad776c249625bcf827889070cc5fb1c02aab8140db6cb39ad6fafdd6893a5bc422", null ],
      [ "PET_NAME_CONSECUTIVE_SPACES", "_pet_8h.html#ad776c249625bcf827889070cc5fb1c02a6679df0b27f925cacfb2c3bdf58ac925", null ],
      [ "PET_NAME_RUSSIAN_CONSECUTIVE_SILENT_CHARACTERS", "_pet_8h.html#ad776c249625bcf827889070cc5fb1c02a27be79123e481d8faa1eb3f2f72187a9", null ],
      [ "PET_NAME_RUSSIAN_SILENT_CHARACTER_AT_BEGINNING_OR_END", "_pet_8h.html#ad776c249625bcf827889070cc5fb1c02a4e3adfbe72e887324b967dc775300b8f", null ],
      [ "PET_NAME_DECLENSION_DOESNT_MATCH_BASE_NAME", "_pet_8h.html#ad776c249625bcf827889070cc5fb1c02ae65bd7e7d81212f26c8401e3c8097cd1", null ]
    ] ],
    [ "PetSaveMode", "_pet_8h.html#a006372e9693754ac0d923d8764ec55fd", [
      [ "PET_SAVE_AS_DELETED", "_pet_8h.html#a006372e9693754ac0d923d8764ec55fdae32b2ad103ad490f5f16a85f9a173dd1", null ],
      [ "PET_SAVE_AS_CURRENT", "_pet_8h.html#a006372e9693754ac0d923d8764ec55fdafd6db825b6f344f6f64ae53dc19cf503", null ],
      [ "PET_SAVE_FIRST_STABLE_SLOT", "_pet_8h.html#a006372e9693754ac0d923d8764ec55fda15e00cc5539d2c9254cb9b310ebcc712", null ],
      [ "PET_SAVE_LAST_STABLE_SLOT", "_pet_8h.html#a006372e9693754ac0d923d8764ec55fda20411938f7cc7a5140e3b7006d54d851", null ],
      [ "PET_SAVE_NOT_IN_SLOT", "_pet_8h.html#a006372e9693754ac0d923d8764ec55fdae55abbb64855962852c91fb8638ad6cd", null ],
      [ "PET_SAVE_REAGENTS", "_pet_8h.html#a006372e9693754ac0d923d8764ec55fdad4213e75267dd772a1811da6336e447a", null ]
    ] ],
    [ "PetSpellState", "_pet_8h.html#a4135b2b50486d91baa260c63a20f15f5", [
      [ "PETSPELL_UNCHANGED", "_pet_8h.html#a4135b2b50486d91baa260c63a20f15f5a67cfad7aa3b55a87a692db5c7eeac936", null ],
      [ "PETSPELL_CHANGED", "_pet_8h.html#a4135b2b50486d91baa260c63a20f15f5a9b8bacded9035aeabc92430edece787c", null ],
      [ "PETSPELL_NEW", "_pet_8h.html#a4135b2b50486d91baa260c63a20f15f5a12e005d42fccd3aadeac9008e95eb5b8", null ],
      [ "PETSPELL_REMOVED", "_pet_8h.html#a4135b2b50486d91baa260c63a20f15f5aa45b4f700fe69b9edae4fb979b988ffe", null ]
    ] ],
    [ "PetSpellType", "_pet_8h.html#ac12b13d4da0688112812b2c7976550ce", [
      [ "PETSPELL_NORMAL", "_pet_8h.html#ac12b13d4da0688112812b2c7976550ceabb53c3a4462fa50e5c8ad926465f63bb", null ],
      [ "PETSPELL_FAMILY", "_pet_8h.html#ac12b13d4da0688112812b2c7976550cead8f157e9e157c6042ec5fa09e17edafb", null ]
    ] ],
    [ "PetTalk", "_pet_8h.html#aa01f2535df94effa749383327aaea2b7", [
      [ "PET_TALK_SPECIAL_SPELL", "_pet_8h.html#aa01f2535df94effa749383327aaea2b7a44499e435b353774d3ec3ce0bdfea28d", null ],
      [ "PET_TALK_ATTACK", "_pet_8h.html#aa01f2535df94effa749383327aaea2b7a803c3c41784bfcad6b7dd2b15f4db7f7", null ]
    ] ],
    [ "PetType", "_pet_8h.html#aae4a343750f70898b0f6490e7db6a471", [
      [ "SUMMON_PET", "_pet_8h.html#aae4a343750f70898b0f6490e7db6a471a3167e28bcafa6bb3b3e2b2dc72557ecd", null ],
      [ "HUNTER_PET", "_pet_8h.html#aae4a343750f70898b0f6490e7db6a471a608ed771a0dfa608eeafada945298bc2", null ],
      [ "GUARDIAN_PET", "_pet_8h.html#aae4a343750f70898b0f6490e7db6a471afb3caeecb823c7b5ee717d5e24d5e879", null ],
      [ "MINI_PET", "_pet_8h.html#aae4a343750f70898b0f6490e7db6a471ae855adcbda374b2c654ae4e9a9bf2ce1", null ],
      [ "MAX_PET_TYPE", "_pet_8h.html#aae4a343750f70898b0f6490e7db6a471acb8090a59681ea6d194c6a00b73423ea", null ]
    ] ],
    [ "LevelStartLoyalty", "_pet_8h.html#a3fa9a92485805c7918668d40e98ed40a", null ],
    [ "LevelUpLoyalty", "_pet_8h.html#a7725b6aad466c066b22b75b543bca4aa", null ]
];