var _l_f_g_mgr_8h =
[
    [ "LFGPlayerQueueInfo", "struct_l_f_g_player_queue_info.html", "struct_l_f_g_player_queue_info" ],
    [ "LFGGroupQueueInfo", "struct_l_f_g_group_queue_info.html", "struct_l_f_g_group_queue_info" ],
    [ "LFGQueue", "class_l_f_g_queue.html", "class_l_f_g_queue" ],
    [ "MAX_DPS_COUNT", "_l_f_g_mgr_8h.html#a959b984e754c2856bb87c47bd24a7d5d", null ],
    [ "sLFGMgr", "_l_f_g_mgr_8h.html#a9876016115774ddaf27fca5cc9adb96e", null ],
    [ "ClassRoles", "_l_f_g_mgr_8h.html#afcc2c5ec2f8a80429f58fcf271257be0", [
      [ "LFG_ROLE_NONE", "_l_f_g_mgr_8h.html#afcc2c5ec2f8a80429f58fcf271257be0a9db69a599169323523f2ef381a9f1811", null ],
      [ "LFG_ROLE_TANK", "_l_f_g_mgr_8h.html#afcc2c5ec2f8a80429f58fcf271257be0aabd02672b9a70bc23c307281cb80fdd8", null ],
      [ "LFG_ROLE_HEALER", "_l_f_g_mgr_8h.html#afcc2c5ec2f8a80429f58fcf271257be0a2272ceed7a8bb01d61662c6e89ce3376", null ],
      [ "LFG_ROLE_DPS", "_l_f_g_mgr_8h.html#afcc2c5ec2f8a80429f58fcf271257be0abfba6d067eb32f0dcd3a8b007217fc42", null ]
    ] ],
    [ "GroupLeaveMethod", "_l_f_g_mgr_8h.html#aa9fa74629e470987ef406a6f80791f37", [
      [ "GROUP_CLIENT_LEAVE", "_l_f_g_mgr_8h.html#aa9fa74629e470987ef406a6f80791f37a9f26a56a7c7075a83ead352aad360e84", null ],
      [ "GROUP_SYSTEM_LEAVE", "_l_f_g_mgr_8h.html#aa9fa74629e470987ef406a6f80791f37a59632fc20cc3983e20bc26b0cb1d1a6f", null ]
    ] ],
    [ "PlayerLeaveMethod", "_l_f_g_mgr_8h.html#a12c15f737b24e629c590ccf7bfb3819f", [
      [ "PLAYER_CLIENT_LEAVE", "_l_f_g_mgr_8h.html#a12c15f737b24e629c590ccf7bfb3819fab6a8a78d764fb08a771c7bce3e06adf5", null ],
      [ "PLAYER_SYSTEM_LEAVE", "_l_f_g_mgr_8h.html#a12c15f737b24e629c590ccf7bfb3819fa2cb272c61060c155138202711b8f4ea0", null ]
    ] ],
    [ "RolesPriority", "_l_f_g_mgr_8h.html#aec605cd3d7ede4a0801d5ba76c772114", [
      [ "LFG_PRIORITY_NONE", "_l_f_g_mgr_8h.html#aec605cd3d7ede4a0801d5ba76c772114a189cf5f0efb0e7bc7ab6632d10394b63", null ],
      [ "LFG_PRIORITY_LOW", "_l_f_g_mgr_8h.html#aec605cd3d7ede4a0801d5ba76c772114adbe50f6b3a0d25b3747a23c63f796f21", null ],
      [ "LFG_PRIORITY_NORMAL", "_l_f_g_mgr_8h.html#aec605cd3d7ede4a0801d5ba76c772114a4fd96504d9c422f744fa2e1dabda2063", null ],
      [ "LFG_PRIORITY_HIGH", "_l_f_g_mgr_8h.html#aec605cd3d7ede4a0801d5ba76c772114a489bed77a2ff3bf9c33df0065b109f3d", null ]
    ] ]
];