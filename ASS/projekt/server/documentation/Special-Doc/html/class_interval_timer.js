var class_interval_timer =
[
    [ "IntervalTimer", "class_interval_timer.html#a016bc8a0d44fa120f4ac1803decc2dc3", null ],
    [ "GetCurrent", "class_interval_timer.html#a93a456d6ff6d74cc889517fc585626e8", null ],
    [ "GetInterval", "class_interval_timer.html#a986bec1dcb7325088517f6721636abcc", null ],
    [ "Passed", "class_interval_timer.html#aa830a407630b8f7a936fcaf838b0fe72", null ],
    [ "Reset", "class_interval_timer.html#ae243f18c4c3174b91fa01f6d08b04d4d", null ],
    [ "SetCurrent", "class_interval_timer.html#ab315f686c2777dc21c824d8674479f9f", null ],
    [ "SetInterval", "class_interval_timer.html#a28d95330f971d3a37aaf53e4f16aad4a", null ],
    [ "Update", "class_interval_timer.html#aa1f5b922ea2d95c49924a506ca37a676", null ]
];