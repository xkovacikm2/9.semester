var struct_friend_info =
[
    [ "FriendInfo", "struct_friend_info.html#a31272eeef0d6b9c98821f8ee7f379977", null ],
    [ "FriendInfo", "struct_friend_info.html#afa26b94ab71ffbe86fcc2e58494cd085", null ],
    [ "Area", "struct_friend_info.html#adf16884b55545ed9302dbb54d653867b", null ],
    [ "Class", "struct_friend_info.html#a71b7cf9ed2df6ce87cbc3117b6e015d8", null ],
    [ "Flags", "struct_friend_info.html#a67462a211bc900e0062bcbf65ed67604", null ],
    [ "Level", "struct_friend_info.html#a9ac0093577afe0f2f47112410682f32a", null ],
    [ "Status", "struct_friend_info.html#a380f11d65b34e1e598b7861987cecfe6", null ]
];