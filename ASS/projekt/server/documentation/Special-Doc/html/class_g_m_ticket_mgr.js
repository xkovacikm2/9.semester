var class_g_m_ticket_mgr =
[
    [ "GMTicketMgr", "class_g_m_ticket_mgr.html#a7420d490db1b04bbf21d6960ea529c5b", null ],
    [ "~GMTicketMgr", "class_g_m_ticket_mgr.html#aac399ca1486e9f15885be14f9e2e32b0", null ],
    [ "Create", "class_g_m_ticket_mgr.html#aecfbd4972a76ef85b5beca8bb4dab34d", null ],
    [ "Delete", "class_g_m_ticket_mgr.html#a9ba4c4d8f8f5de545f7a2901d30ad074", null ],
    [ "DeleteAll", "class_g_m_ticket_mgr.html#a46f96c67a6499b59f4dc9401f53f8a26", null ],
    [ "GetGMTicket", "class_g_m_ticket_mgr.html#ad90ec58ff890877a0b863f21c574064a", null ],
    [ "GetGMTicket", "class_g_m_ticket_mgr.html#a1aad8e4813c67c451cda9ea41f85d114", null ],
    [ "GetGMTicketByOrderPos", "class_g_m_ticket_mgr.html#a943f188c2cf43f4316d106a0c2982d2a", null ],
    [ "GetTicketCount", "class_g_m_ticket_mgr.html#aceffe7d84d6bd44a3f5c214fc0594035", null ],
    [ "LoadGMTickets", "class_g_m_ticket_mgr.html#a96b001ccd04767b99314c02854dae4a7", null ],
    [ "SetAcceptTickets", "class_g_m_ticket_mgr.html#ac5597ea8517597568ef56dde68c2e6bb", null ],
    [ "WillAcceptTickets", "class_g_m_ticket_mgr.html#afa339b566954e04510970288093dd4ce", null ]
];