var struct___spell =
[
    [ "SpellCategory", "struct___spell.html#ad170ffceb499879a4b0d638e9c922703", null ],
    [ "SpellCategoryCooldown", "struct___spell.html#a8a33274d71b410f24f110e9a5921ff18", null ],
    [ "SpellCharges", "struct___spell.html#a16909465a3a20dbab6e3ca2e3c807f17", null ],
    [ "SpellCooldown", "struct___spell.html#a8ef7106595ed25ce23ca2163258d769a", null ],
    [ "SpellId", "struct___spell.html#aed47853f0d0a19643f9667a8af115d4d", null ],
    [ "SpellPPMRate", "struct___spell.html#a5b208a667802e4c3766619c96d267287", null ],
    [ "SpellTrigger", "struct___spell.html#a39657ff9e561a3c86a671f7b3c78ef98", null ]
];