var struct_instance_template =
[
    [ "ghostEntranceMap", "struct_instance_template.html#a7b9f7d846da8f0f96163ce1fa69b183c", null ],
    [ "ghostEntranceX", "struct_instance_template.html#a464225ae3d47bd7010a26dbd23b49345", null ],
    [ "ghostEntranceY", "struct_instance_template.html#a4ef249553a6b84f262e256772f105d4a", null ],
    [ "levelMax", "struct_instance_template.html#abb52f9da39919323cbdcbdf1e736ec00", null ],
    [ "levelMin", "struct_instance_template.html#aa3fadb2df1c8094a50db302c6c8de15b", null ],
    [ "map", "struct_instance_template.html#a1133ebf01501e9677335565a1563ecc5", null ],
    [ "maxPlayers", "struct_instance_template.html#a79cf4c0527e6b22e8c54ffd3d2853e31", null ],
    [ "parent", "struct_instance_template.html#ac209506a6a86fe6217149bce77784585", null ],
    [ "reset_delay", "struct_instance_template.html#a5c3d35c593bfbea68aa4420e992c17cd", null ],
    [ "script_id", "struct_instance_template.html#ab092063f68d94ce5d42e1e2aa9bd7d01", null ]
];