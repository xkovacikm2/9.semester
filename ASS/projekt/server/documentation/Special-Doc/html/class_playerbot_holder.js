var class_playerbot_holder =
[
    [ "PlayerbotHolder", "class_playerbot_holder.html#a7422ce2f08d6c43317acf07937960f19", null ],
    [ "~PlayerbotHolder", "class_playerbot_holder.html#a8e6ee9eefb0afdf88748359e1b7d8421", null ],
    [ "AddPlayerBot", "class_playerbot_holder.html#ad97f375e65d508660ebaf5d8202c8484", null ],
    [ "GetAccountId", "class_playerbot_holder.html#a1a1ef2e990ed05162ebda61a373c5bf9", null ],
    [ "GetPlayerBot", "class_playerbot_holder.html#aace84e423ed0ae51f7cd11cb01efccae", null ],
    [ "GetPlayerBotsBegin", "class_playerbot_holder.html#a0e4cc5650fe1c2f289f5b163f2a0ec3e", null ],
    [ "GetPlayerBotsEnd", "class_playerbot_holder.html#a2077efbf0c1cfcd7eea58c925ec8ec88", null ],
    [ "HandlePlayerbotCommand", "class_playerbot_holder.html#a3458293aa0261c5b7ecec33036675906", null ],
    [ "LogoutAllBots", "class_playerbot_holder.html#a2c3c8bd0f8a0287c9951ce25790470a8", null ],
    [ "LogoutPlayerBot", "class_playerbot_holder.html#a59a32cebbd0ec3ade9e87637c3e853e7", null ],
    [ "OnBotLogin", "class_playerbot_holder.html#aee274a7ffdbc15277a25e79b95a15e9e", null ],
    [ "OnBotLoginInternal", "class_playerbot_holder.html#a0807351e95034735e9225e777be5f8d8", null ],
    [ "ProcessBotCommand", "class_playerbot_holder.html#af4cf952141a47fe47f9210cea604a4af", null ],
    [ "UpdateAIInternal", "class_playerbot_holder.html#ac7687836db28cf7cd3e8c3278340153f", null ],
    [ "UpdateSessions", "class_playerbot_holder.html#ac99e42d55bda8eeb512375e60be3660e", null ],
    [ "playerBots", "class_playerbot_holder.html#aa64bbac57e33dbe98561a7d674131664", null ]
];