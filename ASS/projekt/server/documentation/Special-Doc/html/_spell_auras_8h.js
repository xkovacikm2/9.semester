var _spell_auras_8h =
[
    [ "Modifier", "struct_modifier.html", "struct_modifier" ],
    [ "SpellAuraHolder", "class_spell_aura_holder.html", "class_spell_aura_holder" ],
    [ "Aura", "class_aura.html", "class_aura" ],
    [ "AreaAura", "class_area_aura.html", "class_area_aura" ],
    [ "PersistentAreaAura", "class_persistent_area_aura.html", "class_persistent_area_aura" ],
    [ "SingleEnemyTargetAura", "class_single_enemy_target_aura.html", "class_single_enemy_target_aura" ],
    [ "pAuraHandler", "_spell_auras_8h.html#a5111c0baac0c4b8f8aa6a77a50707403", null ],
    [ "CreateAura", "_spell_auras_8h.html#a0f4dc053d54d644360375398782de70e", null ],
    [ "CreateSpellAuraHolder", "_spell_auras_8h.html#aa25169065b80eceb02d582f20d6bee5c", null ]
];