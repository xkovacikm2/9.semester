var _playerbot_security_8h =
[
    [ "PlayerbotSecurity", "class_playerbot_security.html", "class_playerbot_security" ],
    [ "DenyReason", "_playerbot_security_8h.html#a3a5640e33ba384031babfbc84ef05cfb", [
      [ "PLAYERBOT_DENY_NONE", "_playerbot_security_8h.html#a3a5640e33ba384031babfbc84ef05cfba66df05cd4d6c6a4945f31cbcb43f3009", null ],
      [ "PLAYERBOT_DENY_LOW_LEVEL", "_playerbot_security_8h.html#a3a5640e33ba384031babfbc84ef05cfbaedac716ed9fb52b00a77d841555e61ec", null ],
      [ "PLAYERBOT_DENY_GEARSCORE", "_playerbot_security_8h.html#a3a5640e33ba384031babfbc84ef05cfbad335d21e8a897169881183b3ff89ae9e", null ],
      [ "PLAYERBOT_DENY_NOT_YOURS", "_playerbot_security_8h.html#a3a5640e33ba384031babfbc84ef05cfba45bf0632e60436c152c166d208fa96fc", null ],
      [ "PLAYERBOT_DENY_IS_BOT", "_playerbot_security_8h.html#a3a5640e33ba384031babfbc84ef05cfba2ea31b553000ae5edb6df554156094ae", null ],
      [ "PLAYERBOT_DENY_OPPOSING", "_playerbot_security_8h.html#a3a5640e33ba384031babfbc84ef05cfbaafc6f6e81b8584345f9f9417a56f5532", null ],
      [ "PLAYERBOT_DENY_DEAD", "_playerbot_security_8h.html#a3a5640e33ba384031babfbc84ef05cfba4f4fdcbfea57f221b577000341a10515", null ],
      [ "PLAYERBOT_DENY_FAR", "_playerbot_security_8h.html#a3a5640e33ba384031babfbc84ef05cfba0ea7fb8d0fa8670dd303710eec340f01", null ],
      [ "PLAYERBOT_DENY_INVITE", "_playerbot_security_8h.html#a3a5640e33ba384031babfbc84ef05cfba72e9d6e2630fdc93cb9731e37afa1672", null ],
      [ "PLAYERBOT_DENY_FULL_GROUP", "_playerbot_security_8h.html#a3a5640e33ba384031babfbc84ef05cfbacd5f7edb92a0e91033070f4452e2693f", null ]
    ] ],
    [ "PlayerbotSecurityLevel", "_playerbot_security_8h.html#a946cb7ac7e54d320cb7642a149c55c13", [
      [ "PLAYERBOT_SECURITY_DENY_ALL", "_playerbot_security_8h.html#a946cb7ac7e54d320cb7642a149c55c13ae05ad253da8e2f7d0de0db3fc1cee3cb", null ],
      [ "PLAYERBOT_SECURITY_TALK", "_playerbot_security_8h.html#a946cb7ac7e54d320cb7642a149c55c13adf7b51ae943d6a757af81d93ff3b25aa", null ],
      [ "PLAYERBOT_SECURITY_INVITE", "_playerbot_security_8h.html#a946cb7ac7e54d320cb7642a149c55c13ab10ab904f2bb486c657cd99524ff4b83", null ],
      [ "PLAYERBOT_SECURITY_ALLOW_ALL", "_playerbot_security_8h.html#a946cb7ac7e54d320cb7642a149c55c13a254efcefa4954373b8bc56e1f988c38f", null ]
    ] ]
];