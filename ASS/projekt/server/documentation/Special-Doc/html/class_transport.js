var class_transport =
[
    [ "Transport", "class_transport.html#a36928c178aa490c02aa25b4b12e2bf63", null ],
    [ "~Transport", "class_transport.html#a9071b156741912e4550f6c88467d2834", null ],
    [ "AddPassenger", "class_transport.html#a79a8db0e464176da51f3ea365d2b5690", null ],
    [ "DeleteFromDB", "class_transport.html#ab420262385903050d4906184b399deb8", null ],
    [ "GetPassengers", "class_transport.html#ac1d8bb6d350fd1e320f0aecdc3194d38", null ],
    [ "RemovePassenger", "class_transport.html#a9b4e89f7b703edcb4045161e563d3712", null ],
    [ "Update", "class_transport.html#a21fa90d1552a7f3c981d19bf5b3ff2f0", null ],
    [ "m_passengers", "class_transport.html#a1ff3e6bfe8abb7cff0612e7b9da5ec36", null ]
];