var struct_faction_entry =
[
    [ "GetIndexFitTo", "struct_faction_entry.html#a3896c3e55f4f8efbcadc0c3506310468", null ],
    [ "BaseRepClassMask", "struct_faction_entry.html#ae73655de567de3766fcb3bbfb7f9eed3", null ],
    [ "BaseRepRaceMask", "struct_faction_entry.html#aa8e1c08416d07882af7075fbb020ae5a", null ],
    [ "BaseRepValue", "struct_faction_entry.html#a52f0753d1e1e69a4994eb6b1eab6824c", null ],
    [ "ID", "struct_faction_entry.html#a1953f7a343201113dae80cdd97e1ac90", null ],
    [ "name", "struct_faction_entry.html#acd023107464ca31847d49229608d1d85", null ],
    [ "ReputationFlags", "struct_faction_entry.html#ae23a3c94554b2043928d6e1ea748e709", null ],
    [ "reputationListID", "struct_faction_entry.html#aee99121c2d7298e16540229923391aee", null ],
    [ "team", "struct_faction_entry.html#a6210fabf2688f26a0a0050f6c148485d", null ]
];