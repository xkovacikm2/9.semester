var struct_loot_item =
[
    [ "LootItem", "struct_loot_item.html#a35e11d01d30ca234c533f6bb0bdb6748", null ],
    [ "LootItem", "struct_loot_item.html#a2dd7e3a8dd5ceb1be0851a5677815c86", null ],
    [ "AllowedForPlayer", "struct_loot_item.html#a15cb066c24b649a9698120fd7adba2dd", null ],
    [ "GetSlotTypeForSharedLoot", "struct_loot_item.html#a4def94cd2014071b0b005e6dc57b7c3a", null ],
    [ "conditionId", "struct_loot_item.html#aa8780f7b1397a127535202add999dac5", null ],
    [ "count", "struct_loot_item.html#a8b08275cd4e1ebabc389fc2e933b1be5", null ],
    [ "freeforall", "struct_loot_item.html#ae9f49e27fd81c235fdc2c754918f55c4", null ],
    [ "is_blocked", "struct_loot_item.html#a0e1dbe84cef1c5f3111b08f25dbb859d", null ],
    [ "is_counted", "struct_loot_item.html#af50758cf8e8ad77e42197b13d849ae08", null ],
    [ "is_looted", "struct_loot_item.html#a9021fa353f7538706349702498a4e778", null ],
    [ "is_underthreshold", "struct_loot_item.html#a4d18c53f68ff454b7521e8afcab85b24", null ],
    [ "itemid", "struct_loot_item.html#a03aad2576badecebacfb0f4658fdbf4c", null ],
    [ "needs_quest", "struct_loot_item.html#a37ee3823a2144ed57f754d826fcbc093", null ],
    [ "randomPropertyId", "struct_loot_item.html#a665a75643f49ee93f469b7b7bc52a928", null ],
    [ "winner", "struct_loot_item.html#a66ca64880ece1a09f128e40da20bdeac", null ]
];