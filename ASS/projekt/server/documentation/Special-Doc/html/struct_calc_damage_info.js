var struct_calc_damage_info =
[
    [ "absorb", "struct_calc_damage_info.html#a7298afcc76b856c5427f5e123030a797", null ],
    [ "attacker", "struct_calc_damage_info.html#aebe14d91f54da0b25a07ff3d59df6ec2", null ],
    [ "attackType", "struct_calc_damage_info.html#afd18e43bd78a9144c02ba0493921c506", null ],
    [ "blocked_amount", "struct_calc_damage_info.html#a0ce1e1ff406264c7283979fe4fc70148", null ],
    [ "cleanDamage", "struct_calc_damage_info.html#a452ac9be1f6e9c10616a62a128573358", null ],
    [ "damage", "struct_calc_damage_info.html#ab95e1e4746b966f1301d997fdac99c94", null ],
    [ "damageSchoolMask", "struct_calc_damage_info.html#adf048564443d794b2b8077b242c28aae", null ],
    [ "HitInfo", "struct_calc_damage_info.html#ae42a8a3750633d94e661ac9d5a9aa8df", null ],
    [ "hitOutCome", "struct_calc_damage_info.html#a114dcabcb8bff10c2452cb27e75da115", null ],
    [ "procAttacker", "struct_calc_damage_info.html#a21bed657c7e3d2338f30fb5237884d5f", null ],
    [ "procEx", "struct_calc_damage_info.html#ad629b9aad4d88247ef4dfaadd79dc1c8", null ],
    [ "procVictim", "struct_calc_damage_info.html#a236b06649e715fab9dbddb074ef2f075", null ],
    [ "resist", "struct_calc_damage_info.html#ab57d348b500203649bf532405cd9ceec", null ],
    [ "target", "struct_calc_damage_info.html#af2976fa1245b3e3177dcbac464345829", null ],
    [ "TargetState", "struct_calc_damage_info.html#a73ef30ca87444da617b8c2e05b70e9fc", null ]
];