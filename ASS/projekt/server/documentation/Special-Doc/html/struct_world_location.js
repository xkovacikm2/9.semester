var struct_world_location =
[
    [ "WorldLocation", "struct_world_location.html#a3fe6f0c890e7159ebaba48f84f4df04e", null ],
    [ "WorldLocation", "struct_world_location.html#aef10d804709b3faaa97491c1833fdc0b", null ],
    [ "coord_x", "struct_world_location.html#a0d32a4df8019f9c99a14c5bea9b03163", null ],
    [ "coord_y", "struct_world_location.html#aefe6419a2a92bd402969b6d421b4625f", null ],
    [ "coord_z", "struct_world_location.html#afcd1abdc7162493b3926f1e72124a16b", null ],
    [ "mapid", "struct_world_location.html#af6d8a527e976d4a79811d8b1b7959cbe", null ],
    [ "orientation", "struct_world_location.html#a60ef8d67148d358a186ecdd93ad261b9", null ]
];