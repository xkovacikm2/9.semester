var struct_creature_data_addon =
[
    [ "auras", "struct_creature_data_addon.html#a242b0202a8780c7b1555f60a989cbc1c", null ],
    [ "bytes1", "struct_creature_data_addon.html#ac3803f2887cc9d021a75f24d6dea1bb3", null ],
    [ "emote", "struct_creature_data_addon.html#acc79afb5ef8075a39a7b4b10e80739fb", null ],
    [ "flags", "struct_creature_data_addon.html#a3f75e2fc754439545e5e2c78bef3e216", null ],
    [ "guidOrEntry", "struct_creature_data_addon.html#af3d7fe99c990a302a27d5e98719c5a2b", null ],
    [ "mount", "struct_creature_data_addon.html#af16db2e6020fa59e2c4466b444aebb95", null ],
    [ "move_flags", "struct_creature_data_addon.html#a5111d5b0760f9dbfdc19b9a440a83331", null ],
    [ "sheath_state", "struct_creature_data_addon.html#a0d463b05221d2646f9cbcca812d3d14c", null ]
];