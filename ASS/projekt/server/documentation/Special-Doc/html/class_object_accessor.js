var class_object_accessor =
[
    [ "Player2CorpsesMapType", "class_object_accessor.html#aadf0c23ece2eb5c0cf1b3897c4d9dd97", null ],
    [ "AddCorpse", "class_object_accessor.html#a3ba0c87308347e9f9e80a838ea57df9d", null ],
    [ "AddCorpsesToGrid", "class_object_accessor.html#aebcd4d8be49f32b3d3e0a7e3f62d02a2", null ],
    [ "AddObject", "class_object_accessor.html#a4bf5d224da16e7206088b051c8947f59", null ],
    [ "AddObject", "class_object_accessor.html#ae7115a1530d89e14d26bc941348c4876", null ],
    [ "ConvertCorpseForPlayer", "class_object_accessor.html#ac87f10b9bd7126b40542350bce723cc5", null ],
    [ "GetCorpseForPlayerGUID", "class_object_accessor.html#a7e3a3ab9ed01b62c7d3ab0547b8c95f1", null ],
    [ "GetPlayers", "class_object_accessor.html#a8bb3e0f9822cec485848284dbf0bddee", null ],
    [ "RemoveCorpse", "class_object_accessor.html#a692029affdf4a6c5424fc21e6f983c06", null ],
    [ "RemoveObject", "class_object_accessor.html#ab911ccb9e7bb232536d14ecce800f18d", null ],
    [ "RemoveObject", "class_object_accessor.html#a9f5ad3f030115d081b466f683b2ff46c", null ],
    [ "RemoveOldCorpses", "class_object_accessor.html#aae68acaf4837dc38d6c1ff442ec4c2d6", null ],
    [ "SaveAllPlayers", "class_object_accessor.html#a0fba0222d2e46026727c6e898a7f35bc", null ],
    [ "MaNGOS::OperatorNew< ObjectAccessor >", "class_object_accessor.html#aefdea1542be23975c8aa786dbdec8d2b", null ]
];