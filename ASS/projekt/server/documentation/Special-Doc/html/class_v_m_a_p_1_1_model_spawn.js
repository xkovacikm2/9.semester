var class_v_m_a_p_1_1_model_spawn =
[
    [ "getBounds", "class_v_m_a_p_1_1_model_spawn.html#a8a36861d3b0b94692a3a40c9179d02a6", null ],
    [ "operator==", "class_v_m_a_p_1_1_model_spawn.html#ab9713afd57c0bdf9f5ff8d5d12cd567f", null ],
    [ "adtId", "class_v_m_a_p_1_1_model_spawn.html#a4ebcfe8326eff028e722c9972a16200b", null ],
    [ "flags", "class_v_m_a_p_1_1_model_spawn.html#acfc7a76db09013abfad8e42aaae59f52", null ],
    [ "iBound", "class_v_m_a_p_1_1_model_spawn.html#ae1dac1aa360c4d49b61a0cd8af92399c", null ],
    [ "ID", "class_v_m_a_p_1_1_model_spawn.html#ab2fdb441870d6ac7ddc05640ec1d0598", null ],
    [ "iPos", "class_v_m_a_p_1_1_model_spawn.html#abb5966f87710bc6a1f5da766c99b3e12", null ],
    [ "iRot", "class_v_m_a_p_1_1_model_spawn.html#a6d0b4f6bde1a20b246e4b22cc43e25af", null ],
    [ "iScale", "class_v_m_a_p_1_1_model_spawn.html#aad355d0fb09ff0360ed19610bef92d8e", null ],
    [ "name", "class_v_m_a_p_1_1_model_spawn.html#a66e19a4a28f8fc0c537f1315d9bddcfb", null ]
];