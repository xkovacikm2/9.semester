var defines_8h =
[
    [ "sField", "structs_field.html", "structs_field" ],
    [ "T_Table", "defines_8h.html#a833ba44d2eff37383b728c727a5cd862", null ],
    [ "T_TableList", "defines_8h.html#a2475405d0383f1c526d8c416b590068a", null ],
    [ "TDataBase", "defines_8h.html#a5118695d81f6a516b65a4fba2eb3c73a", null ],
    [ "uint32", "defines_8h.html#a1134b580f8da4de94ca6b1de4d37975e", null ],
    [ "uint64", "defines_8h.html#abc0f5bc07737e498f287334775dff2b6", null ],
    [ "ConvertNativeType", "defines_8h.html#a2946e62edfa1f1db3378ba92adf69949", null ],
    [ "IsNeeedEscapeString", "defines_8h.html#a4b5c1c35ef84fe813041932e813793b2", null ],
    [ "PG_Escape_Str", "defines_8h.html#a6556ee7e6a49a1a8c75b9a188f8a5536", null ],
    [ "PG_Exec_str", "defines_8h.html#a7b9e5dfd0c12d15de67f987fa26bdd5b", null ]
];