var namespace_movement =
[
    [ "CommonInitializer", "struct_movement_1_1_common_initializer.html", "struct_movement_1_1_common_initializer" ],
    [ "counter", "class_movement_1_1counter.html", "class_movement_1_1counter" ],
    [ "FacingInfo", "union_movement_1_1_facing_info.html", "union_movement_1_1_facing_info" ],
    [ "FallInitializer", "struct_movement_1_1_fall_initializer.html", "struct_movement_1_1_fall_initializer" ],
    [ "Location", "struct_movement_1_1_location.html", "struct_movement_1_1_location" ],
    [ "MoveSpline", "class_movement_1_1_move_spline.html", "class_movement_1_1_move_spline" ],
    [ "MoveSplineFlag", "class_movement_1_1_move_spline_flag.html", "class_movement_1_1_move_spline_flag" ],
    [ "MoveSplineInit", "class_movement_1_1_move_spline_init.html", "class_movement_1_1_move_spline_init" ],
    [ "MoveSplineInitArgs", "struct_movement_1_1_move_spline_init_args.html", "struct_movement_1_1_move_spline_init_args" ],
    [ "PacketBuilder", "class_movement_1_1_packet_builder.html", null ],
    [ "Spline", "class_movement_1_1_spline.html", "class_movement_1_1_spline" ],
    [ "SplineBase", "class_movement_1_1_spline_base.html", "class_movement_1_1_spline_base" ]
];