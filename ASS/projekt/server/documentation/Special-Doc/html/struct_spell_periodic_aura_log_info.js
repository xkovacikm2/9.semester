var struct_spell_periodic_aura_log_info =
[
    [ "SpellPeriodicAuraLogInfo", "struct_spell_periodic_aura_log_info.html#a2b9815b4b62e69394bdfc639053081db", null ],
    [ "absorb", "struct_spell_periodic_aura_log_info.html#af69ed83578024b979dc9d65ff5ddc83b", null ],
    [ "aura", "struct_spell_periodic_aura_log_info.html#a878abb87f3a4aa5cff97b95fd2119935", null ],
    [ "damage", "struct_spell_periodic_aura_log_info.html#aa3839fed2e3de0f7a01ad209eec2bdcd", null ],
    [ "multiplier", "struct_spell_periodic_aura_log_info.html#a26b18592b0aebbdc6a00e8f91fd2a75a", null ],
    [ "resist", "struct_spell_periodic_aura_log_info.html#a5cb7dbeeb9d28037771a2f235a95e0fe", null ]
];