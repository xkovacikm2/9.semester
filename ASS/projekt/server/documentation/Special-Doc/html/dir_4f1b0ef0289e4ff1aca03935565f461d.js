var dir_4f1b0ef0289e4ff1aca03935565f461d =
[
    [ "BearTankDruidStrategy.cpp", "_bear_tank_druid_strategy_8cpp.html", [
      [ "BearTankDruidStrategyActionNodeFactory", "class_bear_tank_druid_strategy_action_node_factory.html", "class_bear_tank_druid_strategy_action_node_factory" ]
    ] ],
    [ "BearTankDruidStrategy.h", "_bear_tank_druid_strategy_8h.html", [
      [ "BearTankDruidStrategy", "classai_1_1_bear_tank_druid_strategy.html", "classai_1_1_bear_tank_druid_strategy" ]
    ] ],
    [ "CasterDruidStrategy.cpp", "_caster_druid_strategy_8cpp.html", [
      [ "CasterDruidStrategyActionNodeFactory", "class_caster_druid_strategy_action_node_factory.html", "class_caster_druid_strategy_action_node_factory" ]
    ] ],
    [ "CasterDruidStrategy.h", "_caster_druid_strategy_8h.html", [
      [ "CasterDruidStrategy", "classai_1_1_caster_druid_strategy.html", "classai_1_1_caster_druid_strategy" ],
      [ "CasterDruidAoeStrategy", "classai_1_1_caster_druid_aoe_strategy.html", "classai_1_1_caster_druid_aoe_strategy" ],
      [ "CasterDruidDebuffStrategy", "classai_1_1_caster_druid_debuff_strategy.html", "classai_1_1_caster_druid_debuff_strategy" ]
    ] ],
    [ "CatDpsDruidStrategy.cpp", "_cat_dps_druid_strategy_8cpp.html", [
      [ "CatDpsDruidStrategyActionNodeFactory", "class_cat_dps_druid_strategy_action_node_factory.html", "class_cat_dps_druid_strategy_action_node_factory" ]
    ] ],
    [ "CatDpsDruidStrategy.h", "_cat_dps_druid_strategy_8h.html", [
      [ "CatDpsDruidStrategy", "classai_1_1_cat_dps_druid_strategy.html", "classai_1_1_cat_dps_druid_strategy" ],
      [ "CatAoeDruidStrategy", "classai_1_1_cat_aoe_druid_strategy.html", "classai_1_1_cat_aoe_druid_strategy" ]
    ] ],
    [ "DruidActions.cpp", "_druid_actions_8cpp.html", null ],
    [ "DruidActions.h", "_druid_actions_8h.html", [
      [ "CastFaerieFireAction", "classai_1_1_cast_faerie_fire_action.html", "classai_1_1_cast_faerie_fire_action" ],
      [ "CastFaerieFireFeralAction", "classai_1_1_cast_faerie_fire_feral_action.html", "classai_1_1_cast_faerie_fire_feral_action" ],
      [ "CastRejuvenationAction", "classai_1_1_cast_rejuvenation_action.html", "classai_1_1_cast_rejuvenation_action" ],
      [ "CastRegrowthAction", "classai_1_1_cast_regrowth_action.html", "classai_1_1_cast_regrowth_action" ],
      [ "CastHealingTouchAction", "classai_1_1_cast_healing_touch_action.html", "classai_1_1_cast_healing_touch_action" ],
      [ "CastRejuvenationOnPartyAction", "classai_1_1_cast_rejuvenation_on_party_action.html", "classai_1_1_cast_rejuvenation_on_party_action" ],
      [ "CastRegrowthOnPartyAction", "classai_1_1_cast_regrowth_on_party_action.html", "classai_1_1_cast_regrowth_on_party_action" ],
      [ "CastHealingTouchOnPartyAction", "classai_1_1_cast_healing_touch_on_party_action.html", "classai_1_1_cast_healing_touch_on_party_action" ],
      [ "CastRebirthAction", "classai_1_1_cast_rebirth_action.html", "classai_1_1_cast_rebirth_action" ],
      [ "CastMarkOfTheWildAction", "classai_1_1_cast_mark_of_the_wild_action.html", "classai_1_1_cast_mark_of_the_wild_action" ],
      [ "CastMarkOfTheWildOnPartyAction", "classai_1_1_cast_mark_of_the_wild_on_party_action.html", "classai_1_1_cast_mark_of_the_wild_on_party_action" ],
      [ "CastThornsAction", "classai_1_1_cast_thorns_action.html", "classai_1_1_cast_thorns_action" ],
      [ "CastWrathAction", "classai_1_1_cast_wrath_action.html", "classai_1_1_cast_wrath_action" ],
      [ "CastHurricaneAction", "classai_1_1_cast_hurricane_action.html", "classai_1_1_cast_hurricane_action" ],
      [ "CastMoonfireAction", "classai_1_1_cast_moonfire_action.html", "classai_1_1_cast_moonfire_action" ],
      [ "CastInsectSwarmAction", "classai_1_1_cast_insect_swarm_action.html", "classai_1_1_cast_insect_swarm_action" ],
      [ "CastStarfireAction", "classai_1_1_cast_starfire_action.html", "classai_1_1_cast_starfire_action" ],
      [ "CastEntanglingRootsAction", "classai_1_1_cast_entangling_roots_action.html", "classai_1_1_cast_entangling_roots_action" ],
      [ "CastEntanglingRootsCcAction", "classai_1_1_cast_entangling_roots_cc_action.html", "classai_1_1_cast_entangling_roots_cc_action" ],
      [ "CastNaturesGraspAction", "classai_1_1_cast_natures_grasp_action.html", "classai_1_1_cast_natures_grasp_action" ],
      [ "CastHibernateAction", "classai_1_1_cast_hibernate_action.html", "classai_1_1_cast_hibernate_action" ],
      [ "CastCurePoisonAction", "classai_1_1_cast_cure_poison_action.html", "classai_1_1_cast_cure_poison_action" ],
      [ "CastCurePoisonOnPartyAction", "classai_1_1_cast_cure_poison_on_party_action.html", "classai_1_1_cast_cure_poison_on_party_action" ],
      [ "CastAbolishPoisonAction", "classai_1_1_cast_abolish_poison_action.html", "classai_1_1_cast_abolish_poison_action" ],
      [ "CastAbolishPoisonOnPartyAction", "classai_1_1_cast_abolish_poison_on_party_action.html", "classai_1_1_cast_abolish_poison_on_party_action" ],
      [ "CastBarskinAction", "classai_1_1_cast_barskin_action.html", "classai_1_1_cast_barskin_action" ],
      [ "CastInnervateAction", "classai_1_1_cast_innervate_action.html", "classai_1_1_cast_innervate_action" ],
      [ "CastTranquilityAction", "classai_1_1_cast_tranquility_action.html", "classai_1_1_cast_tranquility_action" ]
    ] ],
    [ "DruidAiObjectContext.cpp", "_druid_ai_object_context_8cpp.html", [
      [ "StrategyFactoryInternal", "classai_1_1druid_1_1_strategy_factory_internal.html", "classai_1_1druid_1_1_strategy_factory_internal" ],
      [ "DruidStrategyFactoryInternal", "classai_1_1druid_1_1_druid_strategy_factory_internal.html", "classai_1_1druid_1_1_druid_strategy_factory_internal" ],
      [ "TriggerFactoryInternal", "classai_1_1druid_1_1_trigger_factory_internal.html", "classai_1_1druid_1_1_trigger_factory_internal" ],
      [ "AiObjectContextInternal", "classai_1_1druid_1_1_ai_object_context_internal.html", "classai_1_1druid_1_1_ai_object_context_internal" ]
    ] ],
    [ "DruidAiObjectContext.h", "_druid_ai_object_context_8h.html", [
      [ "DruidAiObjectContext", "classai_1_1_druid_ai_object_context.html", "classai_1_1_druid_ai_object_context" ]
    ] ],
    [ "DruidBearActions.h", "_druid_bear_actions_8h.html", [
      [ "CastFeralChargeBearAction", "classai_1_1_cast_feral_charge_bear_action.html", "classai_1_1_cast_feral_charge_bear_action" ],
      [ "CastGrowlAction", "classai_1_1_cast_growl_action.html", "classai_1_1_cast_growl_action" ],
      [ "CastMaulAction", "classai_1_1_cast_maul_action.html", "classai_1_1_cast_maul_action" ],
      [ "CastBashAction", "classai_1_1_cast_bash_action.html", "classai_1_1_cast_bash_action" ],
      [ "CastSwipeAction", "classai_1_1_cast_swipe_action.html", "classai_1_1_cast_swipe_action" ],
      [ "CastDemoralizingRoarAction", "classai_1_1_cast_demoralizing_roar_action.html", "classai_1_1_cast_demoralizing_roar_action" ],
      [ "CastMangleBearAction", "classai_1_1_cast_mangle_bear_action.html", "classai_1_1_cast_mangle_bear_action" ],
      [ "CastSwipeBearAction", "classai_1_1_cast_swipe_bear_action.html", "classai_1_1_cast_swipe_bear_action" ],
      [ "CastLacerateAction", "classai_1_1_cast_lacerate_action.html", "classai_1_1_cast_lacerate_action" ],
      [ "CastBashOnEnemyHealerAction", "classai_1_1_cast_bash_on_enemy_healer_action.html", "classai_1_1_cast_bash_on_enemy_healer_action" ]
    ] ],
    [ "DruidCatActions.h", "_druid_cat_actions_8h.html", [
      [ "CastFeralChargeCatAction", "classai_1_1_cast_feral_charge_cat_action.html", "classai_1_1_cast_feral_charge_cat_action" ],
      [ "CastCowerAction", "classai_1_1_cast_cower_action.html", "classai_1_1_cast_cower_action" ],
      [ "CastBerserkAction", "classai_1_1_cast_berserk_action.html", "classai_1_1_cast_berserk_action" ],
      [ "CastTigersFuryAction", "classai_1_1_cast_tigers_fury_action.html", "classai_1_1_cast_tigers_fury_action" ],
      [ "CastRakeAction", "classai_1_1_cast_rake_action.html", "classai_1_1_cast_rake_action" ],
      [ "CastClawAction", "classai_1_1_cast_claw_action.html", "classai_1_1_cast_claw_action" ],
      [ "CastMangleCatAction", "classai_1_1_cast_mangle_cat_action.html", "classai_1_1_cast_mangle_cat_action" ],
      [ "CastSwipeCatAction", "classai_1_1_cast_swipe_cat_action.html", "classai_1_1_cast_swipe_cat_action" ],
      [ "CastFerociousBiteAction", "classai_1_1_cast_ferocious_bite_action.html", "classai_1_1_cast_ferocious_bite_action" ],
      [ "CastRipAction", "classai_1_1_cast_rip_action.html", "classai_1_1_cast_rip_action" ]
    ] ],
    [ "DruidMultipliers.cpp", "_druid_multipliers_8cpp.html", null ],
    [ "DruidMultipliers.h", "_druid_multipliers_8h.html", null ],
    [ "DruidShapeshiftActions.h", "_druid_shapeshift_actions_8h.html", [
      [ "CastBearFormAction", "classai_1_1_cast_bear_form_action.html", "classai_1_1_cast_bear_form_action" ],
      [ "CastDireBearFormAction", "classai_1_1_cast_dire_bear_form_action.html", "classai_1_1_cast_dire_bear_form_action" ],
      [ "CastCatFormAction", "classai_1_1_cast_cat_form_action.html", "classai_1_1_cast_cat_form_action" ],
      [ "CastTreeFormAction", "classai_1_1_cast_tree_form_action.html", "classai_1_1_cast_tree_form_action" ],
      [ "CastMoonkinFormAction", "classai_1_1_cast_moonkin_form_action.html", "classai_1_1_cast_moonkin_form_action" ],
      [ "CastCasterFormAction", "classai_1_1_cast_caster_form_action.html", "classai_1_1_cast_caster_form_action" ]
    ] ],
    [ "DruidTriggers.cpp", "_druid_triggers_8cpp.html", null ],
    [ "DruidTriggers.h", "_druid_triggers_8h.html", [
      [ "MarkOfTheWildOnPartyTrigger", "classai_1_1_mark_of_the_wild_on_party_trigger.html", "classai_1_1_mark_of_the_wild_on_party_trigger" ],
      [ "MarkOfTheWildTrigger", "classai_1_1_mark_of_the_wild_trigger.html", "classai_1_1_mark_of_the_wild_trigger" ],
      [ "ThornsTrigger", "classai_1_1_thorns_trigger.html", "classai_1_1_thorns_trigger" ],
      [ "RakeTrigger", "classai_1_1_rake_trigger.html", "classai_1_1_rake_trigger" ],
      [ "InsectSwarmTrigger", "classai_1_1_insect_swarm_trigger.html", "classai_1_1_insect_swarm_trigger" ],
      [ "MoonfireTrigger", "classai_1_1_moonfire_trigger.html", "classai_1_1_moonfire_trigger" ],
      [ "FaerieFireTrigger", "classai_1_1_faerie_fire_trigger.html", "classai_1_1_faerie_fire_trigger" ],
      [ "FaerieFireFeralTrigger", "classai_1_1_faerie_fire_feral_trigger.html", "classai_1_1_faerie_fire_feral_trigger" ],
      [ "BashInterruptSpellTrigger", "classai_1_1_bash_interrupt_spell_trigger.html", "classai_1_1_bash_interrupt_spell_trigger" ],
      [ "TigersFuryTrigger", "classai_1_1_tigers_fury_trigger.html", "classai_1_1_tigers_fury_trigger" ],
      [ "NaturesGraspTrigger", "classai_1_1_natures_grasp_trigger.html", "classai_1_1_natures_grasp_trigger" ],
      [ "EntanglingRootsTrigger", "classai_1_1_entangling_roots_trigger.html", "classai_1_1_entangling_roots_trigger" ],
      [ "CurePoisonTrigger", "classai_1_1_cure_poison_trigger.html", "classai_1_1_cure_poison_trigger" ],
      [ "PartyMemberCurePoisonTrigger", "classai_1_1_party_member_cure_poison_trigger.html", "classai_1_1_party_member_cure_poison_trigger" ],
      [ "BearFormTrigger", "classai_1_1_bear_form_trigger.html", "classai_1_1_bear_form_trigger" ],
      [ "TreeFormTrigger", "classai_1_1_tree_form_trigger.html", "classai_1_1_tree_form_trigger" ],
      [ "CatFormTrigger", "classai_1_1_cat_form_trigger.html", "classai_1_1_cat_form_trigger" ],
      [ "EclipseSolarTrigger", "classai_1_1_eclipse_solar_trigger.html", "classai_1_1_eclipse_solar_trigger" ],
      [ "EclipseLunarTrigger", "classai_1_1_eclipse_lunar_trigger.html", "classai_1_1_eclipse_lunar_trigger" ],
      [ "BashInterruptEnemyHealerSpellTrigger", "classai_1_1_bash_interrupt_enemy_healer_spell_trigger.html", "classai_1_1_bash_interrupt_enemy_healer_spell_trigger" ]
    ] ],
    [ "FeralDruidStrategy.cpp", "_feral_druid_strategy_8cpp.html", [
      [ "FeralDruidStrategyActionNodeFactory", "class_feral_druid_strategy_action_node_factory.html", "class_feral_druid_strategy_action_node_factory" ]
    ] ],
    [ "FeralDruidStrategy.h", "_feral_druid_strategy_8h.html", [
      [ "ShapeshiftDruidStrategyActionNodeFactory", "classai_1_1_shapeshift_druid_strategy_action_node_factory.html", "classai_1_1_shapeshift_druid_strategy_action_node_factory" ],
      [ "FeralDruidStrategy", "classai_1_1_feral_druid_strategy.html", "classai_1_1_feral_druid_strategy" ]
    ] ],
    [ "GenericDruidNonCombatStrategy.cpp", "_generic_druid_non_combat_strategy_8cpp.html", [
      [ "GenericDruidNonCombatStrategyActionNodeFactory", "class_generic_druid_non_combat_strategy_action_node_factory.html", "class_generic_druid_non_combat_strategy_action_node_factory" ]
    ] ],
    [ "GenericDruidNonCombatStrategy.h", "_generic_druid_non_combat_strategy_8h.html", [
      [ "GenericDruidNonCombatStrategy", "classai_1_1_generic_druid_non_combat_strategy.html", "classai_1_1_generic_druid_non_combat_strategy" ]
    ] ],
    [ "GenericDruidStrategy.cpp", "_generic_druid_strategy_8cpp.html", [
      [ "GenericDruidStrategyActionNodeFactory", "class_generic_druid_strategy_action_node_factory.html", "class_generic_druid_strategy_action_node_factory" ]
    ] ],
    [ "GenericDruidStrategy.h", "_generic_druid_strategy_8h.html", [
      [ "GenericDruidStrategy", "classai_1_1_generic_druid_strategy.html", "classai_1_1_generic_druid_strategy" ]
    ] ],
    [ "HealDruidStrategy.cpp", "_heal_druid_strategy_8cpp.html", [
      [ "HealDruidStrategyActionNodeFactory", "class_heal_druid_strategy_action_node_factory.html", "class_heal_druid_strategy_action_node_factory" ]
    ] ],
    [ "HealDruidStrategy.h", "_heal_druid_strategy_8h.html", [
      [ "HealDruidStrategy", "classai_1_1_heal_druid_strategy.html", "classai_1_1_heal_druid_strategy" ]
    ] ]
];