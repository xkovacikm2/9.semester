var class_query_named_result =
[
    [ "QueryNamedResult", "class_query_named_result.html#ade35d990d8df8256d1f93c07c44904d7", null ],
    [ "~QueryNamedResult", "class_query_named_result.html#a8cc46304015b77a4baeedd27a27b340a", null ],
    [ "Fetch", "class_query_named_result.html#a808e4defb84932387c3ab83a6ce556fd", null ],
    [ "GetField_idx", "class_query_named_result.html#a4226689781a503c719e1953a21a95151", null ],
    [ "GetFieldCount", "class_query_named_result.html#a5b433e0a9196cce6fbfb0de023a1d69d", null ],
    [ "GetFieldNames", "class_query_named_result.html#a2986c641920c01e1231449357e4a3c94", null ],
    [ "GetRowCount", "class_query_named_result.html#a67492a90069d272302b403082f8431c6", null ],
    [ "NextRow", "class_query_named_result.html#a15da48b2a74457394e08a7745d5d942c", null ],
    [ "operator[]", "class_query_named_result.html#a30e25996121c01711d733125ca5a3905", null ],
    [ "operator[]", "class_query_named_result.html#af6b32346c83f9408e7fa6cddd54adc30", null ],
    [ "mFieldNames", "class_query_named_result.html#a5fceaa1396f629e9e0ca620b71be1bc5", null ],
    [ "mQuery", "class_query_named_result.html#ad752fc5c84c5bdf5cf456789e14eb76d", null ]
];