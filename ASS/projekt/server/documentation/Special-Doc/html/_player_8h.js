var _player_8h =
[
    [ "PlayerSpell", "struct_player_spell.html", "struct_player_spell" ],
    [ "SpellModifier", "struct_spell_modifier.html", "struct_spell_modifier" ],
    [ "SpellCooldown", "struct_spell_cooldown.html", "struct_spell_cooldown" ],
    [ "ActionButton", "struct_action_button.html", "struct_action_button" ],
    [ "PlayerCreateInfoItem", "struct_player_create_info_item.html", "struct_player_create_info_item" ],
    [ "PlayerClassLevelInfo", "struct_player_class_level_info.html", "struct_player_class_level_info" ],
    [ "PlayerClassInfo", "struct_player_class_info.html", "struct_player_class_info" ],
    [ "PlayerLevelInfo", "struct_player_level_info.html", "struct_player_level_info" ],
    [ "PlayerCreateInfoAction", "struct_player_create_info_action.html", "struct_player_create_info_action" ],
    [ "PlayerInfo", "struct_player_info.html", "struct_player_info" ],
    [ "PvPInfo", "struct_pv_p_info.html", "struct_pv_p_info" ],
    [ "DuelInfo", "struct_duel_info.html", "struct_duel_info" ],
    [ "Areas", "struct_areas.html", "struct_areas" ],
    [ "EnchantDuration", "struct_enchant_duration.html", "struct_enchant_duration" ],
    [ "HonorCP", "struct_honor_c_p.html", "struct_honor_c_p" ],
    [ "HonorRankInfo", "struct_honor_rank_info.html", "struct_honor_rank_info" ],
    [ "SkillStatusData", "struct_skill_status_data.html", "struct_skill_status_data" ],
    [ "ItemPosCount", "struct_item_pos_count.html", "struct_item_pos_count" ],
    [ "InstancePlayerBind", "struct_instance_player_bind.html", "struct_instance_player_bind" ],
    [ "PlayerTaxi", "class_player_taxi.html", "class_player_taxi" ],
    [ "BGData", "struct_b_g_data.html", "struct_b_g_data" ],
    [ "TradeStatusInfo", "struct_trade_status_info.html", "struct_trade_status_info" ],
    [ "TradeData", "class_trade_data.html", "class_trade_data" ],
    [ "Player", "class_player.html", "class_player" ],
    [ "BgBattleGroundQueueID_Rec", "struct_player_1_1_bg_battle_ground_queue_i_d___rec.html", "struct_player_1_1_bg_battle_ground_queue_i_d___rec" ],
    [ "ACTION_BUTTON_ACTION", "_player_8h.html#ab6a9fef336444d917d4caa5a59acf600", null ],
    [ "ACTION_BUTTON_TYPE", "_player_8h.html#a7cd994ea695a2fdb343be5ef0bdb03a8", null ],
    [ "DISABLED_MIRROR_TIMER", "_player_8h.html#a8b548b9b50daac698d7c2900ce310aa4", null ],
    [ "HONOR_RANK_COUNT", "_player_8h.html#a0c4c1e6ed477f465185b1ef29616f0d9", null ],
    [ "INVENTORY_SLOT_BAG_0", "_player_8h.html#ad3dd8ed856e16139eaa67147d40cf24e", null ],
    [ "MAX_ACTION_BUTTON_ACTION_VALUE", "_player_8h.html#aea4cac3aceaad70ea9b8397854b0128b", null ],
    [ "MAX_ACTION_BUTTONS", "_player_8h.html#aa44542ab1037b2da52095a5c8e923bb2", null ],
    [ "MAX_DRUNKEN", "_player_8h.html#aa54329cb1b1a81d0eeed65b6b11f7465", null ],
    [ "MAX_MONEY_AMOUNT", "_player_8h.html#aabbccefded1f0620ac64736f1c8224f9", null ],
    [ "MAX_PLAYED_TIME_INDEX", "_player_8h.html#a95dadd52b33da56e706760fd70e83681", null ],
    [ "MAX_PLAYER_SUMMON_DELAY", "_player_8h.html#ad061a6c6874af01a0dba9f5fe5ad9909", null ],
    [ "MAX_QUEST_OFFSET", "_player_8h.html#ab5c23006e50d6e278b50ca5a57e478d6", null ],
    [ "MAX_TIMERS", "_player_8h.html#a79fd3eac646964332ed33ec7dd40f208", null ],
    [ "NEGATIVE_HONOR_RANK_COUNT", "_player_8h.html#a38b6a9977ec9150309ed74e965d8d0f2", null ],
    [ "PLAYER_EXPLORED_ZONES_SIZE", "_player_8h.html#adc637ac719fdefb939a974c01dc7c838", null ],
    [ "PLAYER_MAX_SKILLS", "_player_8h.html#abcb5e93310258612a5598eecf2801dce", null ],
    [ "POSITIVE_HONOR_RANK_COUNT", "_player_8h.html#a0b90db92a59d94811db4dccb33cbc8c9", null ],
    [ "ActionButtonList", "_player_8h.html#a732fdd0c458850a5f5358b561f98bd86", null ],
    [ "EnchantDurationList", "_player_8h.html#ab7da463fdcf48285d8c4a18db9fceb3d", null ],
    [ "HonorCPMap", "_player_8h.html#a37ee200a3fd5f4a5fc2db5112d186c94", null ],
    [ "ItemDurationList", "_player_8h.html#afd821be77e7efb1d6b6515cacc42b993", null ],
    [ "ItemPosCountVec", "_player_8h.html#a06f492a0a20f5264c8395684220fec84", null ],
    [ "PlayerCreateInfoActions", "_player_8h.html#a845b4a4f1fbd851dbce7bb2890187eb8", null ],
    [ "PlayerCreateInfoItems", "_player_8h.html#a5a5358007f5f17ad3fa9afed3b6379ee", null ],
    [ "PlayerCreateInfoSpells", "_player_8h.html#a50742342a33da3ecb5a8ff89f00031cb", null ],
    [ "PlayerMails", "_player_8h.html#a05aa9870aaa54b93e1cded06d75e9c01", null ],
    [ "PlayerSpellMap", "_player_8h.html#a752cc80ea5c2eb262ea50ba02dc2f23a", null ],
    [ "QuestStatusMap", "_player_8h.html#ae5397f2a53a6b2bbb11c9274300fbe2c", null ],
    [ "SkillStatusMap", "_player_8h.html#a284b042b0a4534474a6cd9d540c6903f", null ],
    [ "SpellCooldowns", "_player_8h.html#a4e67032f6ccb88a3ffc6fb32196c31ce", null ],
    [ "SpellModList", "_player_8h.html#a20396fe24b032cd78afb6f5c81568b26", null ],
    [ "ActionButtonType", "_player_8h.html#a6ae74597b52b61e76f80036480d4a9f9", [
      [ "ACTION_BUTTON_SPELL", "_player_8h.html#a6ae74597b52b61e76f80036480d4a9f9ab22c805ed49708b22344f1b60d04c70a", null ],
      [ "ACTION_BUTTON_C", "_player_8h.html#a6ae74597b52b61e76f80036480d4a9f9a65d30d092bec5abb9be54061921d3720", null ],
      [ "ACTION_BUTTON_MACRO", "_player_8h.html#a6ae74597b52b61e76f80036480d4a9f9a86d89b819e5bc7ba55d745eec88b7de2", null ],
      [ "ACTION_BUTTON_CMACRO", "_player_8h.html#a6ae74597b52b61e76f80036480d4a9f9ab1ea2d0d6c47379631332aa4289f1023", null ],
      [ "ACTION_BUTTON_ITEM", "_player_8h.html#a6ae74597b52b61e76f80036480d4a9f9a9d3647edb340128cc90e9167f44fb84b", null ]
    ] ],
    [ "ActionButtonUpdateState", "_player_8h.html#a4b43e90481f5e261e3d229f0335ce559", [
      [ "ACTIONBUTTON_UNCHANGED", "_player_8h.html#a4b43e90481f5e261e3d229f0335ce559a75e31cc7b92c9defc20409340fd2655e", null ],
      [ "ACTIONBUTTON_CHANGED", "_player_8h.html#a4b43e90481f5e261e3d229f0335ce559aacd2b7c05c5d95df742900f01552b6a3", null ],
      [ "ACTIONBUTTON_NEW", "_player_8h.html#a4b43e90481f5e261e3d229f0335ce559a624ca83cfd4e23edb581bc3ed9cca3ba", null ],
      [ "ACTIONBUTTON_DELETED", "_player_8h.html#a4b43e90481f5e261e3d229f0335ce559a95a40a8a9ddb1b7e24c112af3473be21", null ]
    ] ],
    [ "AtLoginFlags", "_player_8h.html#aa6b2dcfd7e0c4f62b238241afe792fcb", [
      [ "AT_LOGIN_NONE", "_player_8h.html#aa6b2dcfd7e0c4f62b238241afe792fcba3c65bd9b9ddfef144eee2270448f9326", null ],
      [ "AT_LOGIN_RENAME", "_player_8h.html#aa6b2dcfd7e0c4f62b238241afe792fcba8785c6f27dffe91968bf6635024169eb", null ],
      [ "AT_LOGIN_RESET_SPELLS", "_player_8h.html#aa6b2dcfd7e0c4f62b238241afe792fcba0e52bd573c6218d6fd8b712f4453f8b9", null ],
      [ "AT_LOGIN_RESET_TALENTS", "_player_8h.html#aa6b2dcfd7e0c4f62b238241afe792fcba06050e392f96594aa17d23cb85ec5a22", null ],
      [ "AT_LOGIN_FIRST", "_player_8h.html#aa6b2dcfd7e0c4f62b238241afe792fcba603335736eef48aeea31b0efd812373d", null ]
    ] ],
    [ "BankBagSlots", "_player_8h.html#a7fcff0151a9fe9259e042a52c3c37b0c", [
      [ "BANK_SLOT_BAG_START", "_player_8h.html#a7fcff0151a9fe9259e042a52c3c37b0cade633953c0847a2e477be4083dd037bc", null ],
      [ "BANK_SLOT_BAG_END", "_player_8h.html#a7fcff0151a9fe9259e042a52c3c37b0cad76e6ac10062f15e697857d39056cbec", null ]
    ] ],
    [ "BankItemSlots", "_player_8h.html#a8db93a42b48c6f627796a8ef38bbd2f2", [
      [ "BANK_SLOT_ITEM_START", "_player_8h.html#a8db93a42b48c6f627796a8ef38bbd2f2aa69f68ed66e52b3fb5f86cb25dee7546", null ],
      [ "BANK_SLOT_ITEM_END", "_player_8h.html#a8db93a42b48c6f627796a8ef38bbd2f2ae712bb7256b4aa6cced2a2a05fbe1fbe", null ]
    ] ],
    [ "BuyBackSlots", "_player_8h.html#a6d1182dfc036dc286011d767c384ee5e", [
      [ "BUYBACK_SLOT_START", "_player_8h.html#a6d1182dfc036dc286011d767c384ee5ea6d5756709c97422e8f21e84315606235", null ],
      [ "BUYBACK_SLOT_END", "_player_8h.html#a6d1182dfc036dc286011d767c384ee5ea0e09f1c1c4bae726eb6fdfa99e667e0c", null ]
    ] ],
    [ "BuyBankSlotResult", "_player_8h.html#a8d589ad9d1711f4ebe892105f6a1c167", [
      [ "ERR_BANKSLOT_FAILED_TOO_MANY", "_player_8h.html#a8d589ad9d1711f4ebe892105f6a1c167a4ce27fd088caf2063c85464cace1dd4d", null ],
      [ "ERR_BANKSLOT_INSUFFICIENT_FUNDS", "_player_8h.html#a8d589ad9d1711f4ebe892105f6a1c167ab287d75d988f8f669f28195600800d13", null ],
      [ "ERR_BANKSLOT_NOTBANKER", "_player_8h.html#a8d589ad9d1711f4ebe892105f6a1c167a484b0fa66e46fb264b6373b6bf5e9e20", null ],
      [ "ERR_BANKSLOT_OK", "_player_8h.html#a8d589ad9d1711f4ebe892105f6a1c167a8b2e56a615bc771de8c15d9d17d2867d", null ]
    ] ],
    [ "DrunkenState", "_player_8h.html#adc8fb9638bd9c8f972163d22b9d870ed", [
      [ "DRUNKEN_SOBER", "_player_8h.html#adc8fb9638bd9c8f972163d22b9d870eda8b35d1194abf0f952fc5fb6450bea654", null ],
      [ "DRUNKEN_TIPSY", "_player_8h.html#adc8fb9638bd9c8f972163d22b9d870eda01028419e0fdd37426276b1b6c6a9784", null ],
      [ "DRUNKEN_DRUNK", "_player_8h.html#adc8fb9638bd9c8f972163d22b9d870eda6123ab27dcbbe6042bec1e3230fe3c2d", null ],
      [ "DRUNKEN_SMASHED", "_player_8h.html#adc8fb9638bd9c8f972163d22b9d870edabf93e12c2036f95b171f99c8ce23baef", null ]
    ] ],
    [ "DuelCompleteType", "_player_8h.html#a7847fd3865d67c4dbaa18b6110b7c395", [
      [ "DUEL_INTERRUPTED", "_player_8h.html#a7847fd3865d67c4dbaa18b6110b7c395a9cb0160ce51487ba7d07e2717ffb5f45", null ],
      [ "DUEL_WON", "_player_8h.html#a7847fd3865d67c4dbaa18b6110b7c395a8936c8bfc123e34f0bb94756cfc5d9c2", null ],
      [ "DUEL_FLED", "_player_8h.html#a7847fd3865d67c4dbaa18b6110b7c395a68b4bc8291cc5e26e991056f8dc63d64", null ]
    ] ],
    [ "EnvironmentalDamageType", "_player_8h.html#a5e5c8c3a572ab9cbf427e6f56aa0069d", [
      [ "DAMAGE_EXHAUSTED", "_player_8h.html#a5e5c8c3a572ab9cbf427e6f56aa0069daf2f82d203447677b0e6fd61c9f6805f6", null ],
      [ "DAMAGE_DROWNING", "_player_8h.html#a5e5c8c3a572ab9cbf427e6f56aa0069dab526385fff812ce58ad0263baa7bbb09", null ],
      [ "DAMAGE_FALL", "_player_8h.html#a5e5c8c3a572ab9cbf427e6f56aa0069da4e07f767d39ffd8e48e5138ec45710f8", null ],
      [ "DAMAGE_LAVA", "_player_8h.html#a5e5c8c3a572ab9cbf427e6f56aa0069daa5a0269734b9add5ce6f09cccfcf0aef", null ],
      [ "DAMAGE_SLIME", "_player_8h.html#a5e5c8c3a572ab9cbf427e6f56aa0069da462f686f2a4bc0002bbb334607d746cd", null ],
      [ "DAMAGE_FIRE", "_player_8h.html#a5e5c8c3a572ab9cbf427e6f56aa0069dac50b14cf6946cd1d0767f4601733fca8", null ],
      [ "DAMAGE_FALL_TO_VOID", "_player_8h.html#a5e5c8c3a572ab9cbf427e6f56aa0069dab46a1fed925746a6a0fd964191f4fa82", null ]
    ] ],
    [ "EquipmentSlots", "_player_8h.html#a93b8041f948d575ef2d23746324a37af", [
      [ "EQUIPMENT_SLOT_START", "_player_8h.html#a93b8041f948d575ef2d23746324a37afaefb28ca135419e98ec3929a81f96465c", null ],
      [ "EQUIPMENT_SLOT_HEAD", "_player_8h.html#a93b8041f948d575ef2d23746324a37afaa67c5ceeab0d56ffed28f3e533535c32", null ],
      [ "EQUIPMENT_SLOT_NECK", "_player_8h.html#a93b8041f948d575ef2d23746324a37afa3f92c4537375758b0b7c28c2843f352c", null ],
      [ "EQUIPMENT_SLOT_SHOULDERS", "_player_8h.html#a93b8041f948d575ef2d23746324a37afa15b6dc080176095e3268c1dc500ef809", null ],
      [ "EQUIPMENT_SLOT_BODY", "_player_8h.html#a93b8041f948d575ef2d23746324a37afa1b522aed12afec5f683edf72963e9a33", null ],
      [ "EQUIPMENT_SLOT_CHEST", "_player_8h.html#a93b8041f948d575ef2d23746324a37afac5910f0d7f72991e7e549b95258bd2b1", null ],
      [ "EQUIPMENT_SLOT_WAIST", "_player_8h.html#a93b8041f948d575ef2d23746324a37afaba58fe4be610f86a7c1d3f7abdb0a4e4", null ],
      [ "EQUIPMENT_SLOT_LEGS", "_player_8h.html#a93b8041f948d575ef2d23746324a37afaa86a656730a67ce3f152430033686884", null ],
      [ "EQUIPMENT_SLOT_FEET", "_player_8h.html#a93b8041f948d575ef2d23746324a37afac57a21fd5f2fbcfc1806a40deeef537c", null ],
      [ "EQUIPMENT_SLOT_WRISTS", "_player_8h.html#a93b8041f948d575ef2d23746324a37afada5f2dccee1a2378e00605914f745f6a", null ],
      [ "EQUIPMENT_SLOT_HANDS", "_player_8h.html#a93b8041f948d575ef2d23746324a37afa1fd368f8ce7dad72d75fd6840bdb058b", null ],
      [ "EQUIPMENT_SLOT_FINGER1", "_player_8h.html#a93b8041f948d575ef2d23746324a37afa39712e341d5a043a281b504c7e2262b9", null ],
      [ "EQUIPMENT_SLOT_FINGER2", "_player_8h.html#a93b8041f948d575ef2d23746324a37afa452d18cf712c55651e9573af053e7b74", null ],
      [ "EQUIPMENT_SLOT_TRINKET1", "_player_8h.html#a93b8041f948d575ef2d23746324a37afa6c59392fb13158757074daa4831b9733", null ],
      [ "EQUIPMENT_SLOT_TRINKET2", "_player_8h.html#a93b8041f948d575ef2d23746324a37afaef8af329fc694fb7b105354d4b40fb02", null ],
      [ "EQUIPMENT_SLOT_BACK", "_player_8h.html#a93b8041f948d575ef2d23746324a37afab12fc5acedadf8b93d001debf14ad63a", null ],
      [ "EQUIPMENT_SLOT_MAINHAND", "_player_8h.html#a93b8041f948d575ef2d23746324a37afa559e9d51b0cff8ac6448c07e005a13bd", null ],
      [ "EQUIPMENT_SLOT_OFFHAND", "_player_8h.html#a93b8041f948d575ef2d23746324a37afab44cb19f2d7a24d14ef74748716cd731", null ],
      [ "EQUIPMENT_SLOT_RANGED", "_player_8h.html#a93b8041f948d575ef2d23746324a37afafc73f33a54a6e48f474648a2ad5bd72a", null ],
      [ "EQUIPMENT_SLOT_TABARD", "_player_8h.html#a93b8041f948d575ef2d23746324a37afa591f131493398617246860f52b2999b8", null ],
      [ "EQUIPMENT_SLOT_END", "_player_8h.html#a93b8041f948d575ef2d23746324a37afa730f8fcebbadad4d0b2dadc86d8431d9", null ]
    ] ],
    [ "HonorKillState", "_player_8h.html#a4fd68d6404d3d0cdd9944f4403cc5aaa", [
      [ "HK_NEW", "_player_8h.html#a4fd68d6404d3d0cdd9944f4403cc5aaaaa1ce8e0e13f1afe803a68968d7faf353", null ],
      [ "HK_OLD", "_player_8h.html#a4fd68d6404d3d0cdd9944f4403cc5aaaaac298a00b8a9cd6d324c8bcf891a747a", null ],
      [ "HK_DELETED", "_player_8h.html#a4fd68d6404d3d0cdd9944f4403cc5aaaa153097f3716d5ab50ada94325e42bb4e", null ],
      [ "HK_UNCHANGED", "_player_8h.html#a4fd68d6404d3d0cdd9944f4403cc5aaaa585aedf20625030adf544dd83d3dc290", null ]
    ] ],
    [ "InstanceResetWarningType", "_player_8h.html#a96ff8529b2e55f6def4fe3c9dc1b5a4a", [
      [ "RAID_INSTANCE_WARNING_HOURS", "_player_8h.html#a96ff8529b2e55f6def4fe3c9dc1b5a4aa997298918391902d13f306ff08b35468", null ],
      [ "RAID_INSTANCE_WARNING_MIN", "_player_8h.html#a96ff8529b2e55f6def4fe3c9dc1b5a4aa2a33c9bcc1f691f568c1abbdabe196a9", null ],
      [ "RAID_INSTANCE_WARNING_MIN_SOON", "_player_8h.html#a96ff8529b2e55f6def4fe3c9dc1b5a4aa92b66b96dffff8aadaf5c68b506f4679", null ],
      [ "RAID_INSTANCE_WELCOME", "_player_8h.html#a96ff8529b2e55f6def4fe3c9dc1b5a4aadbecbf56496afcc6f2cc0abe2a838be0", null ]
    ] ],
    [ "InventoryPackSlots", "_player_8h.html#a3f01594f5f05965df25a0f1ebba196f4", [
      [ "INVENTORY_SLOT_ITEM_START", "_player_8h.html#a3f01594f5f05965df25a0f1ebba196f4a26ae565f8a97797a81412a9f148f28bb", null ],
      [ "INVENTORY_SLOT_ITEM_END", "_player_8h.html#a3f01594f5f05965df25a0f1ebba196f4a8adf93fe675626b28fcba2ffce8d4e62", null ]
    ] ],
    [ "InventorySlots", "_player_8h.html#a7adbf9c1ce11a5e5f7661abba7c544f6", [
      [ "INVENTORY_SLOT_BAG_START", "_player_8h.html#a7adbf9c1ce11a5e5f7661abba7c544f6a409cf3d00f62b16d53633aed142536dc", null ],
      [ "INVENTORY_SLOT_BAG_END", "_player_8h.html#a7adbf9c1ce11a5e5f7661abba7c544f6a522331bc28cdac31f9a89bb65afdb32a", null ]
    ] ],
    [ "KeyRingSlots", "_player_8h.html#a3d6156ea4383f298702ee6c20c616536", [
      [ "KEYRING_SLOT_START", "_player_8h.html#a3d6156ea4383f298702ee6c20c616536a14ef668000e8d99734cd26b5fdd3f558", null ],
      [ "KEYRING_SLOT_END", "_player_8h.html#a3d6156ea4383f298702ee6c20c616536a035d2e4073a4495bc3691b8ae22395e7", null ]
    ] ],
    [ "MirrorTimerType", "_player_8h.html#ace7965b06d468750afb8d5bd36ca6103", [
      [ "FATIGUE_TIMER", "_player_8h.html#ace7965b06d468750afb8d5bd36ca6103ac77fdbe212fe2d09fd2f8dea5cec42cf", null ],
      [ "BREATH_TIMER", "_player_8h.html#ace7965b06d468750afb8d5bd36ca6103af02b0882bd022d38de23cdc52e5cd534", null ],
      [ "FIRE_TIMER", "_player_8h.html#ace7965b06d468750afb8d5bd36ca6103a91c9a62241f28714002f84dbbddf768f", null ]
    ] ],
    [ "PlayedTimeIndex", "_player_8h.html#a93382b39331f08de3cbbd53229d1a966", [
      [ "PLAYED_TIME_TOTAL", "_player_8h.html#a93382b39331f08de3cbbd53229d1a966a9bd2564ef499c6918f26493b7b7b88f1", null ],
      [ "PLAYED_TIME_LEVEL", "_player_8h.html#a93382b39331f08de3cbbd53229d1a966afe747cb7121ea94f1ff5377dd28ea631", null ]
    ] ],
    [ "PlayerDelayedOperations", "_player_8h.html#a624afc8e329c958087cc3bb6d83d5362", [
      [ "DELAYED_SAVE_PLAYER", "_player_8h.html#a624afc8e329c958087cc3bb6d83d5362a4e85f13a93421ba527a2804bccb20707", null ],
      [ "DELAYED_RESURRECT_PLAYER", "_player_8h.html#a624afc8e329c958087cc3bb6d83d5362aee275bab03f9a6d2b3896764a56d7a93", null ],
      [ "DELAYED_SPELL_CAST_DESERTER", "_player_8h.html#a624afc8e329c958087cc3bb6d83d5362ab976aa8d517fc2ff8e89f51b5fb0149d", null ],
      [ "DELAYED_END", "_player_8h.html#a624afc8e329c958087cc3bb6d83d5362a4accb9039dcb370e1d8fca7f8c3bdeb3", null ]
    ] ],
    [ "PlayerDismountResult", "_player_8h.html#a4a73ab089be517b0f3102861fd7e16c7", [
      [ "DISMOUNTRESULT_NOPET", "_player_8h.html#a4a73ab089be517b0f3102861fd7e16c7a01db76730ff1d0e789d9b1e3c5470cab", null ],
      [ "DISMOUNTRESULT_NOTMOUNTED", "_player_8h.html#a4a73ab089be517b0f3102861fd7e16c7aa5f4fdd98b77b072d127930edc3a5d81", null ],
      [ "DISMOUNTRESULT_NOTYOURPET", "_player_8h.html#a4a73ab089be517b0f3102861fd7e16c7aa19cc36b9803dd28a95d07ba980cf997", null ],
      [ "DISMOUNTRESULT_OK", "_player_8h.html#a4a73ab089be517b0f3102861fd7e16c7aae6a6293349cccb60df49eb7647a96b8", null ]
    ] ],
    [ "PlayerExtraFlags", "_player_8h.html#a3ab88b258808534e161f229800cfa0ec", [
      [ "PLAYER_EXTRA_GM_ON", "_player_8h.html#a3ab88b258808534e161f229800cfa0eca2a49f9c4e06bb3d4f3b8415e42329aea", null ],
      [ "PLAYER_EXTRA_GM_ACCEPT_TICKETS", "_player_8h.html#a3ab88b258808534e161f229800cfa0eca3148a0a006ce318eb8b52e516cec1434", null ],
      [ "PLAYER_EXTRA_ACCEPT_WHISPERS", "_player_8h.html#a3ab88b258808534e161f229800cfa0eca436ab7736cb36870980253692f9b68d0", null ],
      [ "PLAYER_EXTRA_TAXICHEAT", "_player_8h.html#a3ab88b258808534e161f229800cfa0eca938ea85001baaf2073af57ded0411a02", null ],
      [ "PLAYER_EXTRA_GM_INVISIBLE", "_player_8h.html#a3ab88b258808534e161f229800cfa0eca50fd4c0e9d997797b467986f29d0988a", null ],
      [ "PLAYER_EXTRA_GM_CHAT", "_player_8h.html#a3ab88b258808534e161f229800cfa0ecaf0285b1075903e68a12c5496fa0db458", null ],
      [ "PLAYER_EXTRA_AUCTION_NEUTRAL", "_player_8h.html#a3ab88b258808534e161f229800cfa0ecaf919f71a9379fca893c42092215b966f", null ],
      [ "PLAYER_EXTRA_AUCTION_ENEMY", "_player_8h.html#a3ab88b258808534e161f229800cfa0eca7eae7813191984434f9e1b8d07d39a98", null ],
      [ "PLAYER_EXTRA_PVP_DEATH", "_player_8h.html#a3ab88b258808534e161f229800cfa0ecad9fc309580a3e8ac23871eab1c8ce586", null ]
    ] ],
    [ "PlayerFieldByte2Flags", "_player_8h.html#a29b0496d910c02e5cc98d349c4427474", [
      [ "PLAYER_FIELD_BYTE2_NONE", "_player_8h.html#a29b0496d910c02e5cc98d349c4427474abe76b28df20592b4328b9d9ea04da780", null ],
      [ "PLAYER_FIELD_BYTE2_DETECT_AMORE_0", "_player_8h.html#a29b0496d910c02e5cc98d349c4427474a8aec211ee90d48f5750a9b59705e8e78", null ],
      [ "PLAYER_FIELD_BYTE2_DETECT_AMORE_1", "_player_8h.html#a29b0496d910c02e5cc98d349c4427474a2efbb29cf2831148aefe2be7c6ccdbf9", null ],
      [ "PLAYER_FIELD_BYTE2_DETECT_AMORE_2", "_player_8h.html#a29b0496d910c02e5cc98d349c4427474a720078e3a623c95f643333207502a0f1", null ],
      [ "PLAYER_FIELD_BYTE2_DETECT_AMORE_3", "_player_8h.html#a29b0496d910c02e5cc98d349c4427474a51b2ffee9845c421f02b1c726ec95241", null ],
      [ "PLAYER_FIELD_BYTE2_STEALTH", "_player_8h.html#a29b0496d910c02e5cc98d349c4427474a1c01d4f3e3f54b6e479482ce3f3fc9dd", null ],
      [ "PLAYER_FIELD_BYTE2_INVISIBILITY_GLOW", "_player_8h.html#a29b0496d910c02e5cc98d349c4427474a512b317a67d45505043fcae61dcca393", null ]
    ] ],
    [ "PlayerFieldByteFlags", "_player_8h.html#a31ae2504a623eb1746ff5da8a1902333", [
      [ "PLAYER_FIELD_BYTE_TRACK_STEALTHED", "_player_8h.html#a31ae2504a623eb1746ff5da8a1902333aaf1cbce95c600ce8b27067d9c8f47da8", null ],
      [ "PLAYER_FIELD_BYTE_RELEASE_TIMER", "_player_8h.html#a31ae2504a623eb1746ff5da8a1902333adf79443adab6dafc6a7ec2546b3db2b6", null ],
      [ "PLAYER_FIELD_BYTE_NO_RELEASE_WINDOW", "_player_8h.html#a31ae2504a623eb1746ff5da8a1902333a72cfc6f89a9aaadecf8342b412a3ba56", null ]
    ] ],
    [ "PlayerFlags", "_player_8h.html#a486dd000e9a9532459d9dd9010570621", [
      [ "PLAYER_FLAGS_NONE", "_player_8h.html#a486dd000e9a9532459d9dd9010570621a0c29e4b0413330c9a34138a1858e2f00", null ],
      [ "PLAYER_FLAGS_GROUP_LEADER", "_player_8h.html#a486dd000e9a9532459d9dd9010570621a68aeeecc2703ca6b7aa4185b8aa41548", null ],
      [ "PLAYER_FLAGS_AFK", "_player_8h.html#a486dd000e9a9532459d9dd9010570621a8de0423ce1a8d2ebbbb183eadd16395c", null ],
      [ "PLAYER_FLAGS_DND", "_player_8h.html#a486dd000e9a9532459d9dd9010570621a1922f0681c50a674bc3c66a3b6c1c74c", null ],
      [ "PLAYER_FLAGS_GM", "_player_8h.html#a486dd000e9a9532459d9dd9010570621a9a591d06e9f3ca54de79e7b643d468f9", null ],
      [ "PLAYER_FLAGS_GHOST", "_player_8h.html#a486dd000e9a9532459d9dd9010570621a8972cf61a8fe94379d50f9b737894863", null ],
      [ "PLAYER_FLAGS_RESTING", "_player_8h.html#a486dd000e9a9532459d9dd9010570621a73d7f97ed3311ab8d4adc27637e12c74", null ],
      [ "PLAYER_FLAGS_UNK7", "_player_8h.html#a486dd000e9a9532459d9dd9010570621ace90ec83a930ddb21a9913d5ff5cd406", null ],
      [ "PLAYER_FLAGS_FFA_PVP", "_player_8h.html#a486dd000e9a9532459d9dd9010570621af57ddef9be6726832aadb423b71c81c4", null ],
      [ "PLAYER_FLAGS_CONTESTED_PVP", "_player_8h.html#a486dd000e9a9532459d9dd9010570621a41798f78d62382fc517da86534ad7191", null ],
      [ "PLAYER_FLAGS_IN_PVP", "_player_8h.html#a486dd000e9a9532459d9dd9010570621a3545e357a3349ee42fad9ec871660880", null ],
      [ "PLAYER_FLAGS_HIDE_HELM", "_player_8h.html#a486dd000e9a9532459d9dd9010570621a9e11b33b95bd42aca29ab5133c3a1458", null ],
      [ "PLAYER_FLAGS_HIDE_CLOAK", "_player_8h.html#a486dd000e9a9532459d9dd9010570621a3bc0762eb887939b2925dc398760df88", null ],
      [ "PLAYER_FLAGS_PARTIAL_PLAY_TIME", "_player_8h.html#a486dd000e9a9532459d9dd9010570621a50a32e5861a2313e3377fa1501483aca", null ],
      [ "PLAYER_FLAGS_NO_PLAY_TIME", "_player_8h.html#a486dd000e9a9532459d9dd9010570621abf7073734869b912c7a34ad21ff43157", null ],
      [ "PLAYER_FLAGS_UNK15", "_player_8h.html#a486dd000e9a9532459d9dd9010570621a88d452e584fdd1570178315ca82424eb", null ],
      [ "PLAYER_FLAGS_UNK16", "_player_8h.html#a486dd000e9a9532459d9dd9010570621a945e4b3576964b0f0d216c1c7f5184f5", null ],
      [ "PLAYER_FLAGS_SANCTUARY", "_player_8h.html#a486dd000e9a9532459d9dd9010570621acdc5acc87352eafb677112090bc4cc6e", null ],
      [ "PLAYER_FLAGS_TAXI_BENCHMARK", "_player_8h.html#a486dd000e9a9532459d9dd9010570621a9cf3750d7552ebb5c2801370a2d8cae6", null ],
      [ "PLAYER_FLAGS_PVP_TIMER", "_player_8h.html#a486dd000e9a9532459d9dd9010570621adc8433300089ff8f6753b673ca882555", null ]
    ] ],
    [ "PlayerLoginQueryIndex", "_player_8h.html#af771ff47a13b9f47e9729c8194ff7642", [
      [ "PLAYER_LOGIN_QUERY_LOADFROM", "_player_8h.html#af771ff47a13b9f47e9729c8194ff7642aa864417297e6e075121ff8dd2757ef60", null ],
      [ "PLAYER_LOGIN_QUERY_LOADGROUP", "_player_8h.html#af771ff47a13b9f47e9729c8194ff7642ac1eb349d009f9d7a7e7a3ef7f18a8f63", null ],
      [ "PLAYER_LOGIN_QUERY_LOADBOUNDINSTANCES", "_player_8h.html#af771ff47a13b9f47e9729c8194ff7642aa23f10e5d515a25800dd4acb52d08d0a", null ],
      [ "PLAYER_LOGIN_QUERY_LOADAURAS", "_player_8h.html#af771ff47a13b9f47e9729c8194ff7642a06cc5da29aab7d209f1b8da694db8edd", null ],
      [ "PLAYER_LOGIN_QUERY_LOADSPELLS", "_player_8h.html#af771ff47a13b9f47e9729c8194ff7642aeb879c41577dc7be5186acdf474d6574", null ],
      [ "PLAYER_LOGIN_QUERY_LOADQUESTSTATUS", "_player_8h.html#af771ff47a13b9f47e9729c8194ff7642a00dd65bcb8f3f76b7d1bb9a73dc17b23", null ],
      [ "PLAYER_LOGIN_QUERY_LOADHONORCP", "_player_8h.html#af771ff47a13b9f47e9729c8194ff7642a3c01874d0aecce303941a4f53c882c94", null ],
      [ "PLAYER_LOGIN_QUERY_LOADREPUTATION", "_player_8h.html#af771ff47a13b9f47e9729c8194ff7642abea8ddb688cf3387fd3fc35022a3204e", null ],
      [ "PLAYER_LOGIN_QUERY_LOADINVENTORY", "_player_8h.html#af771ff47a13b9f47e9729c8194ff7642abaf70704b4b0e70eaed15e4896b731da", null ],
      [ "PLAYER_LOGIN_QUERY_LOADITEMLOOT", "_player_8h.html#af771ff47a13b9f47e9729c8194ff7642ad052d533f5378a0e60db33d78c60e0a8", null ],
      [ "PLAYER_LOGIN_QUERY_LOADACTIONS", "_player_8h.html#af771ff47a13b9f47e9729c8194ff7642ad3c8d171252da69387457c5dddce714f", null ],
      [ "PLAYER_LOGIN_QUERY_LOADSOCIALLIST", "_player_8h.html#af771ff47a13b9f47e9729c8194ff7642aba118cadfad173228ea66b69e739c711", null ],
      [ "PLAYER_LOGIN_QUERY_LOADHOMEBIND", "_player_8h.html#af771ff47a13b9f47e9729c8194ff7642ae9216998b69fa858b6780e203241d117", null ],
      [ "PLAYER_LOGIN_QUERY_LOADSPELLCOOLDOWNS", "_player_8h.html#af771ff47a13b9f47e9729c8194ff7642a8c575e6dc940a1a4d1277a656fcdfe60", null ],
      [ "PLAYER_LOGIN_QUERY_LOADGUILD", "_player_8h.html#af771ff47a13b9f47e9729c8194ff7642aee7d6602e43e3970bf2b24db6e97154e", null ],
      [ "PLAYER_LOGIN_QUERY_LOADBGDATA", "_player_8h.html#af771ff47a13b9f47e9729c8194ff7642a97bce1dc64bf2059df5a173255e8c5e7", null ],
      [ "PLAYER_LOGIN_QUERY_LOADSKILLS", "_player_8h.html#af771ff47a13b9f47e9729c8194ff7642af144f19fc5f4e92cd7415470735b2921", null ],
      [ "PLAYER_LOGIN_QUERY_LOADMAILS", "_player_8h.html#af771ff47a13b9f47e9729c8194ff7642a5c3bcef63025a700f7ec01c154ac4218", null ],
      [ "PLAYER_LOGIN_QUERY_LOADMAILEDITEMS", "_player_8h.html#af771ff47a13b9f47e9729c8194ff7642aa7cea6898bd5dd2528f16a4f7a396f72", null ],
      [ "MAX_PLAYER_LOGIN_QUERY", "_player_8h.html#af771ff47a13b9f47e9729c8194ff7642a13e3b1b23530ab3ae516dd6b39df0164", null ]
    ] ],
    [ "PlayerMountResult", "_player_8h.html#aff047fb8a5d8b30962c606ca826a570c", [
      [ "MOUNTRESULT_INVALIDMOUNTEE", "_player_8h.html#aff047fb8a5d8b30962c606ca826a570ca7cd73d2af3664bd4944657414d729fb1", null ],
      [ "MOUNTRESULT_TOOFARAWAY", "_player_8h.html#aff047fb8a5d8b30962c606ca826a570cacdb7830508bb76e6cda3c153a8ee0146", null ],
      [ "MOUNTRESULT_ALREADYMOUNTED", "_player_8h.html#aff047fb8a5d8b30962c606ca826a570ca16c5c06e3e18cf4784cf0aa83c967d37", null ],
      [ "MOUNTRESULT_NOTMOUNTABLE", "_player_8h.html#aff047fb8a5d8b30962c606ca826a570caf2fe43768938b3df87c1dc5b108de456", null ],
      [ "MOUNTRESULT_NOTYOURPET", "_player_8h.html#aff047fb8a5d8b30962c606ca826a570cac1e0e3298f70acd27f299c6730fe19d9", null ],
      [ "MOUNTRESULT_OTHER", "_player_8h.html#aff047fb8a5d8b30962c606ca826a570ca1cfc35adf97ccc94df8ba7747d01fad8", null ],
      [ "MOUNTRESULT_LOOTING", "_player_8h.html#aff047fb8a5d8b30962c606ca826a570caf3ac3b3a02925575d36939edd6688b28", null ],
      [ "MOUNTRESULT_RACECANTMOUNT", "_player_8h.html#aff047fb8a5d8b30962c606ca826a570cac0069d22960647a44d20543a0dc31f01", null ],
      [ "MOUNTRESULT_SHAPESHIFTED", "_player_8h.html#aff047fb8a5d8b30962c606ca826a570caa9e0119f3748f3f3ee8afa93a95fcc88", null ],
      [ "MOUNTRESULT_FORCEDDISMOUNT", "_player_8h.html#aff047fb8a5d8b30962c606ca826a570caf8f305ac860fb388b2f6e7b27179ffc8", null ],
      [ "MOUNTRESULT_OK", "_player_8h.html#aff047fb8a5d8b30962c606ca826a570ca7d733d30c766edc8bcfa0c35c9493400", null ]
    ] ],
    [ "PlayerRestState", "_player_8h.html#ab6046cc6e043004b533c669be1dc95a8", [
      [ "REST_STATE_RESTED", "_player_8h.html#ab6046cc6e043004b533c669be1dc95a8a43e609d4309527a1344f62f72ac6a861", null ],
      [ "REST_STATE_NORMAL", "_player_8h.html#ab6046cc6e043004b533c669be1dc95a8a1f34b88af1180daa0afd33967f997658", null ],
      [ "REST_STATE_RAF_LINKED", "_player_8h.html#ab6046cc6e043004b533c669be1dc95a8a756758e20568fa43014daae863cf07d1", null ]
    ] ],
    [ "PlayerSlots", "_player_8h.html#af59383be59ca1d77ff918e40494cc38b", [
      [ "PLAYER_SLOT_START", "_player_8h.html#af59383be59ca1d77ff918e40494cc38baba6e6d37f461ac68b007f7cbb77513ec", null ],
      [ "PLAYER_SLOT_END", "_player_8h.html#af59383be59ca1d77ff918e40494cc38bac237277423fec6615d1e7edae681f63b", null ],
      [ "PLAYER_SLOTS_COUNT", "_player_8h.html#af59383be59ca1d77ff918e40494cc38ba6f014774c7004f9e99c4457446df0ff1", null ]
    ] ],
    [ "PlayerSpellState", "_player_8h.html#aeba4f8d5fc0fa3fd00538df69077ba55", [
      [ "PLAYERSPELL_UNCHANGED", "_player_8h.html#aeba4f8d5fc0fa3fd00538df69077ba55a3d895d03802b444cce65a8021262a638", null ],
      [ "PLAYERSPELL_CHANGED", "_player_8h.html#aeba4f8d5fc0fa3fd00538df69077ba55a598b808ff4ad6c853b4a3b0474944bfc", null ],
      [ "PLAYERSPELL_NEW", "_player_8h.html#aeba4f8d5fc0fa3fd00538df69077ba55ab6092cbafa41540e9b6318f88eed9285", null ],
      [ "PLAYERSPELL_REMOVED", "_player_8h.html#aeba4f8d5fc0fa3fd00538df69077ba55a1a8411f74fdb6a5715ed8cfca6b96a50", null ]
    ] ],
    [ "PlayerUnderwaterState", "_player_8h.html#a95f398bf1c0950ba72e11f4a395aba29", [
      [ "UNDERWATER_NONE", "_player_8h.html#a95f398bf1c0950ba72e11f4a395aba29a204b03b1b48f61ad7570fd091374ff39", null ],
      [ "UNDERWATER_INWATER", "_player_8h.html#a95f398bf1c0950ba72e11f4a395aba29a8571e08d528628519afbdf44b5628360", null ],
      [ "UNDERWATER_INLAVA", "_player_8h.html#a95f398bf1c0950ba72e11f4a395aba29aba8380ee5a74eee5ae932cb7f270a800", null ],
      [ "UNDERWATER_INSLIME", "_player_8h.html#a95f398bf1c0950ba72e11f4a395aba29a25f92c2b2ab3c1d261f1c79eb54be9b0", null ],
      [ "UNDERWATER_INDARKWATER", "_player_8h.html#a95f398bf1c0950ba72e11f4a395aba29a1409f730f8ce0935d0987666ec35724b", null ],
      [ "UNDERWATER_EXIST_TIMERS", "_player_8h.html#a95f398bf1c0950ba72e11f4a395aba29a6f0108f107fd1e87a1e4dfd13b5208b1", null ]
    ] ],
    [ "QuestSlotOffsets", "_player_8h.html#a7c33ee51eabd76272609a0ca6e26f315", [
      [ "QUEST_ID_OFFSET", "_player_8h.html#a7c33ee51eabd76272609a0ca6e26f315a4cf928a289d9c1cdfffc50383ce347e9", null ],
      [ "QUEST_COUNT_STATE_OFFSET", "_player_8h.html#a7c33ee51eabd76272609a0ca6e26f315af74367476b3e064da1b7cc3729b4b116", null ],
      [ "QUEST_TIME_OFFSET", "_player_8h.html#a7c33ee51eabd76272609a0ca6e26f315a12d05f2c63b809813d6d54c174f93598", null ]
    ] ],
    [ "QuestSlotStateMask", "_player_8h.html#a2d079af58ae5bf704ea2b958307a5196", [
      [ "QUEST_STATE_NONE", "_player_8h.html#a2d079af58ae5bf704ea2b958307a5196aa0fcb1405df6995fae9176032524522a", null ],
      [ "QUEST_STATE_COMPLETE", "_player_8h.html#a2d079af58ae5bf704ea2b958307a5196a61b0594f1dd162d5a1dcff7103cecfc2", null ],
      [ "QUEST_STATE_FAIL", "_player_8h.html#a2d079af58ae5bf704ea2b958307a5196a94e8ba97d7ae5f33b87bab4d23ffb39f", null ]
    ] ],
    [ "RaidGroupError", "_player_8h.html#aa3f619ca1bdd92d58591fb70ef54d3ec", [
      [ "ERR_RAID_GROUP_REQUIRED", "_player_8h.html#aa3f619ca1bdd92d58591fb70ef54d3eca9b6c51406a261b22d0b20285ecc52653", null ],
      [ "ERR_RAID_GROUP_FULL", "_player_8h.html#aa3f619ca1bdd92d58591fb70ef54d3eca898da720ac95e0b5ce7d2d8837fde90b", null ]
    ] ],
    [ "ReputationSource", "_player_8h.html#add0e486b240be9a2d1a8d50d397b917c", [
      [ "REPUTATION_SOURCE_KILL", "_player_8h.html#add0e486b240be9a2d1a8d50d397b917ca6b17403b4310c7f0550c6734e3d2834b", null ],
      [ "REPUTATION_SOURCE_QUEST", "_player_8h.html#add0e486b240be9a2d1a8d50d397b917caaee4856eda6944c68ce6d462d848f1a9", null ],
      [ "REPUTATION_SOURCE_SPELL", "_player_8h.html#add0e486b240be9a2d1a8d50d397b917ca81f9c093fa4f90ca6584d34915f00dc9", null ]
    ] ],
    [ "RestType", "_player_8h.html#aff0fc472b81b176add891fc1d3552e3e", [
      [ "REST_TYPE_NO", "_player_8h.html#aff0fc472b81b176add891fc1d3552e3ea6c16cd16eeffe95ed40e20ad03d3ae5e", null ],
      [ "REST_TYPE_IN_TAVERN", "_player_8h.html#aff0fc472b81b176add891fc1d3552e3ead673bb204f26bd4356023e30e28b76db", null ],
      [ "REST_TYPE_IN_CITY", "_player_8h.html#aff0fc472b81b176add891fc1d3552e3ea941e4f9bf591b1c8b77fbb4372754869", null ]
    ] ],
    [ "SkillUpdateState", "_player_8h.html#ad7133f5242269df996858d8167cea631", [
      [ "SKILL_UNCHANGED", "_player_8h.html#ad7133f5242269df996858d8167cea631a5a813f231f33d394c1c2f54e1665b281", null ],
      [ "SKILL_CHANGED", "_player_8h.html#ad7133f5242269df996858d8167cea631a484f2cf1987c9512855c222040c9a5e6", null ],
      [ "SKILL_NEW", "_player_8h.html#ad7133f5242269df996858d8167cea631ac6d3121ccb6ec61d5c598fd9eac0f855", null ],
      [ "SKILL_DELETED", "_player_8h.html#ad7133f5242269df996858d8167cea631a14f05147e756e9d928f6759e43541968", null ]
    ] ],
    [ "SpellModType", "_player_8h.html#a946f1b98541e5df18b4b45f6fb144ce8", [
      [ "SPELLMOD_FLAT", "_player_8h.html#a946f1b98541e5df18b4b45f6fb144ce8a8ebc303e6990a35340cd39cef55f4dd7", null ],
      [ "SPELLMOD_PCT", "_player_8h.html#a946f1b98541e5df18b4b45f6fb144ce8ac1724959d706b685bccc6fdd33e5dcd7", null ]
    ] ],
    [ "TeleportToOptions", "_player_8h.html#a19f261ab504f8e06e9488bd5a3c23075", [
      [ "TELE_TO_GM_MODE", "_player_8h.html#a19f261ab504f8e06e9488bd5a3c23075ac848cb12b6d8ee4208900a15b9c7b3bc", null ],
      [ "TELE_TO_NOT_LEAVE_TRANSPORT", "_player_8h.html#a19f261ab504f8e06e9488bd5a3c23075af901d71505d50d133cdf4933f8279aa8", null ],
      [ "TELE_TO_NOT_LEAVE_COMBAT", "_player_8h.html#a19f261ab504f8e06e9488bd5a3c23075ab2944787df830211a346e9ceae0def7f", null ],
      [ "TELE_TO_NOT_UNSUMMON_PET", "_player_8h.html#a19f261ab504f8e06e9488bd5a3c23075ab4ccbaee4377b41a3e3bf0f2890186f9", null ],
      [ "TELE_TO_SPELL", "_player_8h.html#a19f261ab504f8e06e9488bd5a3c23075a8487b61066dab8a7ed85701246847da0", null ]
    ] ],
    [ "TradeSlots", "_player_8h.html#ad8fd978c4198825d89a1f02643d20c62", [
      [ "TRADE_SLOT_COUNT", "_player_8h.html#ad8fd978c4198825d89a1f02643d20c62a1e8d56a5a22d784c5d388fd8bd206488", null ],
      [ "TRADE_SLOT_TRADED_COUNT", "_player_8h.html#ad8fd978c4198825d89a1f02643d20c62ab52f8b8c3be8445987c622e4dfacaa79", null ],
      [ "TRADE_SLOT_NONTRADED", "_player_8h.html#ad8fd978c4198825d89a1f02643d20c62abdab7c30065c4b145dd2e6ce0e27c45f", null ]
    ] ],
    [ "TrainerSpellState", "_player_8h.html#a103ca8d3e1b78335f77c37f5bf8e5966", [
      [ "TRAINER_SPELL_GREEN", "_player_8h.html#a103ca8d3e1b78335f77c37f5bf8e5966a1a7825291a283accb01c0819ce4df4bc", null ],
      [ "TRAINER_SPELL_RED", "_player_8h.html#a103ca8d3e1b78335f77c37f5bf8e5966a776f4797486a8c97784084045c88dd5c", null ],
      [ "TRAINER_SPELL_GRAY", "_player_8h.html#a103ca8d3e1b78335f77c37f5bf8e5966a615c839a694ba15638907808472b572d", null ],
      [ "TRAINER_SPELL_GREEN_DISABLED", "_player_8h.html#a103ca8d3e1b78335f77c37f5bf8e5966a733b3dafcdb9636839d52b20c8e34dde", null ]
    ] ],
    [ "TransferAbortReason", "_player_8h.html#a34b87c411e3511dc8bde293dc2b25210", [
      [ "TRANSFER_ABORT_MAX_PLAYERS", "_player_8h.html#a34b87c411e3511dc8bde293dc2b25210a568c3ebfc68db0f9c268c5d54ff6c05b", null ],
      [ "TRANSFER_ABORT_NOT_FOUND", "_player_8h.html#a34b87c411e3511dc8bde293dc2b25210aac27b37ec6cb6f81c05eb7a2c6c786f4", null ],
      [ "TRANSFER_ABORT_TOO_MANY_INSTANCES", "_player_8h.html#a34b87c411e3511dc8bde293dc2b25210a00f567b73392d8208b8a37e6a60dc7c7", null ],
      [ "TRANSFER_ABORT_SILENTLY", "_player_8h.html#a34b87c411e3511dc8bde293dc2b25210a0e94f8dfa34c474faca790154bd73fd4", null ],
      [ "TRANSFER_ABORT_ZONE_IN_COMBAT", "_player_8h.html#a34b87c411e3511dc8bde293dc2b25210a76584b704ef89518ee151826d5fbc268", null ]
    ] ],
    [ "TYPE_OF_HONOR", "_player_8h.html#a6479ff9b3a963a7987fed7ab910146ff", [
      [ "HONORABLE", "_player_8h.html#a6479ff9b3a963a7987fed7ab910146ffaeb3400d0f0a1ffd0831f39e622cfc947", null ],
      [ "DISHONORABLE", "_player_8h.html#a6479ff9b3a963a7987fed7ab910146ffaf9e65871e863f67b8dd23ac085101ffa", null ]
    ] ],
    [ "AddItemsSetItem", "_player_8h.html#a02944420353f8ba883b80737f8e63137", null ],
    [ "operator<<", "_player_8h.html#ac1c70635005fb1c1d786519d4020d26f", null ],
    [ "RemoveItemsSetItem", "_player_8h.html#a788477be54316bcc39d11d1bf033ce6f", null ]
];