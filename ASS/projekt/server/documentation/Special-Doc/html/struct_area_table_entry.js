var struct_area_table_entry =
[
    [ "area_level", "struct_area_table_entry.html#a23434848cf8b2eab4de1b6530b8d5f8d", null ],
    [ "area_name", "struct_area_table_entry.html#acd6ecb99a76f55ad0e7368db228d7297", null ],
    [ "exploreFlag", "struct_area_table_entry.html#a78cf3009cd0fe49dc7870c7f65c79251", null ],
    [ "flags", "struct_area_table_entry.html#a5854e3f565d827dbad3d9a36f99e9a25", null ],
    [ "ID", "struct_area_table_entry.html#ad02bb9fce21f464e38223581074a9e21", null ],
    [ "LiquidTypeOverride", "struct_area_table_entry.html#a9c786bd771e7fcb2a73d625a7a01843d", null ],
    [ "mapid", "struct_area_table_entry.html#a048ac249318efd9aa35bb705bfb92dc8", null ],
    [ "team", "struct_area_table_entry.html#ae18e0efef83e663452715f6be15ecaef", null ],
    [ "zone", "struct_area_table_entry.html#a3b4f9e187629db5ced9a06c601fc7f78", null ]
];