var struct_spell_area =
[
    [ "ApplyOrRemoveSpellIfCan", "struct_spell_area.html#a20856c511116464eb17561e8a755b4b5", null ],
    [ "IsFitToRequirements", "struct_spell_area.html#af1585273701356a591bf9d9483e996a3", null ],
    [ "areaId", "struct_spell_area.html#af7e6f7b027388447a1f99e759975ca09", null ],
    [ "auraSpell", "struct_spell_area.html#adaf2f6f7c56701fe42a2674aca74fba0", null ],
    [ "autocast", "struct_spell_area.html#a38b42b82e4a588e102eb393cc907cd27", null ],
    [ "conditionId", "struct_spell_area.html#a93643300218ab509f087d66219ec6e9e", null ],
    [ "gender", "struct_spell_area.html#af4314cba9c91fd1e2c09905c17c67d22", null ],
    [ "questEnd", "struct_spell_area.html#a075cc2bc4d8c9663b2bdb2cf8ba77f5f", null ],
    [ "questStart", "struct_spell_area.html#afc99b14a3724fef7c5dd70e976a479a8", null ],
    [ "questStartCanActive", "struct_spell_area.html#a9a67beb6acb7555c982564437aa95089", null ],
    [ "raceMask", "struct_spell_area.html#a402373ff841bd2aa47b4b36a581624cc", null ],
    [ "spellId", "struct_spell_area.html#a8a299d901e2c13cdc3c00d6cf0a856d3", null ]
];