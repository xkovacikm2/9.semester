var class_loot_store =
[
    [ "LootStore", "class_loot_store.html#a99896ddfc04c20e2f7088a2ad8403e35", null ],
    [ "~LootStore", "class_loot_store.html#a5ed3ef2d00fdcb8edc3c4ddb9cb5a9ca", null ],
    [ "CheckLootRefs", "class_loot_store.html#a771b684a8d8a2aef38a819ea3d95fe75", null ],
    [ "Clear", "class_loot_store.html#abd9b51ff3aa4452ff5f5cc82267bac48", null ],
    [ "GetEntryName", "class_loot_store.html#a1640bb7cc3abe6a85fbd563c29e6c770", null ],
    [ "GetLootFor", "class_loot_store.html#a541edad9042f950ba2545c6e1126c419", null ],
    [ "GetName", "class_loot_store.html#a1f4973d6034f3bda7c3bbeeeb8ef37bd", null ],
    [ "HaveLootFor", "class_loot_store.html#abd45358d22bbb44f54743792a7fd0039", null ],
    [ "HaveQuestLootFor", "class_loot_store.html#a82a84ddb5051cf3e9ee508c2f0642d1e", null ],
    [ "HaveQuestLootForPlayer", "class_loot_store.html#a6c5068823be565c6f59efaabc0301652", null ],
    [ "HaveSharedQuestLootForPlayer", "class_loot_store.html#ac70a1d95e376fbbff582982740d5ed8f", null ],
    [ "HaveStartingQuestLootForPlayer", "class_loot_store.html#adf8cde68fc25f5f06fc13e4a59647c06", null ],
    [ "IsRatesAllowed", "class_loot_store.html#aa64da1c4756d94e30c552b17795b1da0", null ],
    [ "LoadAndCollectLootIds", "class_loot_store.html#af477b36f426855ac9a9901b21eae4de6", null ],
    [ "LoadLootTable", "class_loot_store.html#a52c0a1f41853e85c97a5d9628547551f", null ],
    [ "ReportNotExistedId", "class_loot_store.html#adcf8f3baea4d6ababdae1ecf532b551e", null ],
    [ "ReportUnusedIds", "class_loot_store.html#af0fe6608310a123d70cc8221e65e4556", null ],
    [ "Verify", "class_loot_store.html#afd0762a7391f530009b44bf6b4a3a3be", null ]
];