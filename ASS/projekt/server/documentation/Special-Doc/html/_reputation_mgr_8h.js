var _reputation_mgr_8h =
[
    [ "FactionState", "struct_faction_state.html", "struct_faction_state" ],
    [ "ReputationMgr", "class_reputation_mgr.html", "class_reputation_mgr" ],
    [ "FactionStateList", "_reputation_mgr_8h.html#ad6532434415723ae38cfd01814db70c4", null ],
    [ "FactionStateListPair", "_reputation_mgr_8h.html#a67bd86198dc3404c933e563b244e7b4f", null ],
    [ "ForcedReactions", "_reputation_mgr_8h.html#a7f488a06b2477791f620e975ea444f4e", null ],
    [ "RepListID", "_reputation_mgr_8h.html#a5d6355bb0de767f549aeb8f1e6481624", null ],
    [ "FactionFlags", "_reputation_mgr_8h.html#a7610697e1175bfdb17c7d746e244164c", [
      [ "FACTION_FLAG_VISIBLE", "_reputation_mgr_8h.html#a7610697e1175bfdb17c7d746e244164caad5d4e0bf07462c5bd4c2b560a38a681", null ],
      [ "FACTION_FLAG_AT_WAR", "_reputation_mgr_8h.html#a7610697e1175bfdb17c7d746e244164cab533fbeed7847b02f72e23b47c22904a", null ],
      [ "FACTION_FLAG_HIDDEN", "_reputation_mgr_8h.html#a7610697e1175bfdb17c7d746e244164ca9c232bf8f0a7262392568f28b30b1266", null ],
      [ "FACTION_FLAG_INVISIBLE_FORCED", "_reputation_mgr_8h.html#a7610697e1175bfdb17c7d746e244164ca6c06b920a3bf968aa5ad6974c7d32f34", null ],
      [ "FACTION_FLAG_PEACE_FORCED", "_reputation_mgr_8h.html#a7610697e1175bfdb17c7d746e244164ca25a9227a2852b61f8f6977458a8901e9", null ],
      [ "FACTION_FLAG_INACTIVE", "_reputation_mgr_8h.html#a7610697e1175bfdb17c7d746e244164ca130d2ccab5717b7d75fc58e6267e047f", null ],
      [ "FACTION_FLAG_RIVAL", "_reputation_mgr_8h.html#a7610697e1175bfdb17c7d746e244164cac157a1fa3b9e2c1ebca0716ca7d1d3fc", null ]
    ] ]
];