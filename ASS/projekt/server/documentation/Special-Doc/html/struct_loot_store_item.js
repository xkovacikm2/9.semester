var struct_loot_store_item =
[
    [ "LootStoreItem", "struct_loot_store_item.html#a3df457078681fc6de9b346d0402ecd74", null ],
    [ "IsValid", "struct_loot_store_item.html#ae5fc113c31d96bc7ca31a5bc36ab2fcf", null ],
    [ "Roll", "struct_loot_store_item.html#a41bdbe34a3815d6bcc6284fa57dcdb27", null ],
    [ "chance", "struct_loot_store_item.html#a20411103b449a9176b3bc665f909f06f", null ],
    [ "conditionId", "struct_loot_store_item.html#a891ac969e43fa31f8b717be350611d71", null ],
    [ "group", "struct_loot_store_item.html#a406cf8abff285ec5bef612f3988c6edb", null ],
    [ "itemid", "struct_loot_store_item.html#a5df1d4fde5f3358470bd16f7ad9154c1", null ],
    [ "maxcount", "struct_loot_store_item.html#a96012059cc6affa8120ef544682781d2", null ],
    [ "mincountOrRef", "struct_loot_store_item.html#a2b9a70a1293109adc191698a94797203", null ],
    [ "needs_quest", "struct_loot_store_item.html#a5a2fe7ae48508b88bbdfc70fc4035f31", null ]
];