var class_world_object =
[
    [ "UpdateHelper", "class_world_object_1_1_update_helper.html", "class_world_object_1_1_update_helper" ],
    [ "~WorldObject", "class_world_object.html#a287e5b87a26133cd35f3015268d2a5a3", null ],
    [ "WorldObject", "class_world_object.html#a632d2e78acdcbc0aef09951134340d4e", null ],
    [ "_Create", "class_world_object.html#ac6db5313900addc0e99f9343bb1366f4", null ],
    [ "_IsWithinDist", "class_world_object.html#aa6e6ee0d5e385309cbb0647d3e3730fb", null ],
    [ "AddObjectToRemoveList", "class_world_object.html#a8b5bf67b1f64fee748e20a147455c141", null ],
    [ "AddToClientUpdateList", "class_world_object.html#a917dc79cdc3899cdabb6b712e4056bde", null ],
    [ "BuildUpdateData", "class_world_object.html#a9a59cd9b29192feef1af5fd1537a1eab", null ],
    [ "CleanupsBeforeDelete", "class_world_object.html#a26b2e0100b7be0a93ab8a525c3ad0d7c", null ],
    [ "GetAngle", "class_world_object.html#a65210c718c1485e22ffa04b42a388a96", null ],
    [ "GetAngle", "class_world_object.html#a2d973b4321092f9e843f5bb4bf218f17", null ],
    [ "GetAreaId", "class_world_object.html#acdd585ef58eb876c506389ff96a4fbd9", null ],
    [ "GetClosePoint", "class_world_object.html#a2ae47b18d07bbd9f4af45a0fa6104e43", null ],
    [ "GetContactPoint", "class_world_object.html#a5b88f310861271005f4b0c16768a8725", null ],
    [ "GetDistance", "class_world_object.html#a7a3393e4f621e8c348436e6d85c270ee", null ],
    [ "GetDistance", "class_world_object.html#ab441a45efda6bc187ff4070b897cd548", null ],
    [ "GetDistance2d", "class_world_object.html#aee80d1e726b3f0f629ec65f93a42da74", null ],
    [ "GetDistance2d", "class_world_object.html#a52f51272b6fb3e5790a65d47406cd75f", null ],
    [ "GetDistanceOrder", "class_world_object.html#a64beb0950f5e5207749ac5bba180ed31", null ],
    [ "GetDistanceZ", "class_world_object.html#a574c627fcf61fb205e7348171c022341", null ],
    [ "GetInstanceData", "class_world_object.html#a9ae950f261bdb6c242a074dbe8e7bdba", null ],
    [ "GetInstanceId", "class_world_object.html#a664e99edd35c98e8eb7f67476aa8082d", null ],
    [ "GetMap", "class_world_object.html#a76c507a1b3adcebf2732572bc6b67fe0", null ],
    [ "GetMapId", "class_world_object.html#a485ded18163bda1b789be2283781780e", null ],
    [ "GetName", "class_world_object.html#a2bd4a4a5a7c308efcf9c52e8ea32fd46", null ],
    [ "GetNameForLocaleIdx", "class_world_object.html#af9739bf0457b2f048628806ecb179f62", null ],
    [ "GetNearPoint", "class_world_object.html#a28ab37149c939f7b036530e9a8515255", null ],
    [ "GetNearPoint2D", "class_world_object.html#aaf97c5762542ce92f769a457de53376b", null ],
    [ "GetObjectBoundingRadius", "class_world_object.html#aefb894f950c193c66d802e6f3c7313d3", null ],
    [ "GetOrientation", "class_world_object.html#ab6ca0a11a0d2bb8a7ec0110adb732857", null ],
    [ "GetPosition", "class_world_object.html#a2443ccaece359deb5376a4296a9a8217", null ],
    [ "GetPosition", "class_world_object.html#af9b4cc8fd94e85953ca5f83c095a7c43", null ],
    [ "GetPositionX", "class_world_object.html#aa66c526ae67a978c3c8abfef4e45df50", null ],
    [ "GetPositionY", "class_world_object.html#a950bb10c341d39dff61c394009860ac3", null ],
    [ "GetPositionZ", "class_world_object.html#a9c900e733e0e4b039682234dd9514a7e", null ],
    [ "GetRandomPoint", "class_world_object.html#a368eb2bbbdd14cb9c8c2ca49414a0eee", null ],
    [ "GetViewPoint", "class_world_object.html#a906283af2fd8f022aa19283ecffa94b9", null ],
    [ "GetZoneAndAreaId", "class_world_object.html#ae1a8efe701e885f0ed1eaeb93bbece8e", null ],
    [ "GetZoneId", "class_world_object.html#ad498d690fbe69bb99a8a2ffdcb2f5169", null ],
    [ "HasInArc", "class_world_object.html#a57ac646041055ee549f150d7aca432f9", null ],
    [ "IsActiveObject", "class_world_object.html#a8c45d0dbb68cae082adfb4c347510d29", null ],
    [ "isActiveObject", "class_world_object.html#a5cfdcda6e5530175c824a7f9e2d5c50d", null ],
    [ "IsControlledByPlayer", "class_world_object.html#a654ebfe6ac1e7c3ad6ea440854508824", null ],
    [ "IsFriendlyTo", "class_world_object.html#ab4e894638027998113b4ce6127ecb804", null ],
    [ "IsHostileTo", "class_world_object.html#ad9303bc86de5160b5f1b5a883a833f58", null ],
    [ "IsInBack", "class_world_object.html#aae463cebed511f695ad45da8aa8df53d", null ],
    [ "IsInBackInMap", "class_world_object.html#ae480f1ffdd135436179214e21cfcde7f", null ],
    [ "IsInFront", "class_world_object.html#a400f70db7dc37ebd850d476c994434ba", null ],
    [ "IsInFrontInMap", "class_world_object.html#aa6ce9119abf1bfde5ae3c7cf532e2f2a", null ],
    [ "IsInMap", "class_world_object.html#ada978f0324fd25b91689bbf0d0808e3e", null ],
    [ "IsInRange", "class_world_object.html#ab79ebfcc3ba4247ccfeb95eee63dc01a", null ],
    [ "IsInRange2d", "class_world_object.html#a31d7a5673827b882fdfbe177c5aecbbc", null ],
    [ "IsInRange3d", "class_world_object.html#a0b4cc2758ff4a939539d0adbac6aa53c", null ],
    [ "IsPositionValid", "class_world_object.html#a1cde45b48f735b88dbffb81355694693", null ],
    [ "IsVisibleFor", "class_world_object.html#ad3ad8a865d407e0a93afbb7d7b476112", null ],
    [ "IsVisibleForInState", "class_world_object.html#a6ac4e565cd212696d654c7b0022c6563", null ],
    [ "IsWithinDist", "class_world_object.html#ad31c70778088c09b056d7c12ecb61e2a", null ],
    [ "IsWithinDist2d", "class_world_object.html#acf1cbacd9452a1567720d99a95ccdcbd", null ],
    [ "IsWithinDist3d", "class_world_object.html#a138b2dc44ad7b39f7d0721d00583f7f9", null ],
    [ "IsWithinDistInMap", "class_world_object.html#a2f5eb3453402863dcd638027b69ed384", null ],
    [ "IsWithinLOS", "class_world_object.html#a9c5cae24555bfaefee5fe805acb4ed7b", null ],
    [ "IsWithinLOSInMap", "class_world_object.html#a32af7b92b204045a0296a257d019a742", null ],
    [ "MonsterSay", "class_world_object.html#aacbd452087fdaccd322c9a03e30d6a80", null ],
    [ "MonsterText", "class_world_object.html#ad8962ca2fe26d083295eac71ab1c8ce9", null ],
    [ "MonsterTextEmote", "class_world_object.html#a200aebedc5cf266fe7d97d390dec425c", null ],
    [ "MonsterWhisper", "class_world_object.html#a7e55f1c1fdb3bc5737d920ac8fcacfbd", null ],
    [ "MonsterYell", "class_world_object.html#a94614eddaebc2e47ace1b3e8f6e82948", null ],
    [ "PlayDirectSound", "class_world_object.html#a9a69cbddff62e7e6054f8c77400e63b6", null ],
    [ "PlayDistanceSound", "class_world_object.html#a3c4c4a5b82df0cb766869cba43d6fac9", null ],
    [ "PlayMusic", "class_world_object.html#a2bd0e70d718c2eae22282da4de775889", null ],
    [ "PrintCoordinatesError", "class_world_object.html#a9a68ce84715982302cfc9326739402ec", null ],
    [ "Relocate", "class_world_object.html#a0c939338d714517b7a9598c98031138e", null ],
    [ "Relocate", "class_world_object.html#a738799e930254d105176d807c9c190ee", null ],
    [ "RemoveFromClientUpdateList", "class_world_object.html#a14cb8374cdc94210350757138af62ba8", null ],
    [ "ResetMap", "class_world_object.html#a1087ef3824cf3d798c53ae070894b526", null ],
    [ "SaveRespawnTime", "class_world_object.html#af5615d14f10bdc7546e4cbc0557370dd", null ],
    [ "SendMessageToSet", "class_world_object.html#a96cf75d18220bffe609e2496a4fe4382", null ],
    [ "SendMessageToSetExcept", "class_world_object.html#a47809c7d1a01a529f573d8b814571cd8", null ],
    [ "SendMessageToSetInRange", "class_world_object.html#ad9f4545193fd2b2795d5a2e116a3ea7c", null ],
    [ "SendObjectDeSpawnAnim", "class_world_object.html#ab5a418c979525ebc39647471b1fb84ca", null ],
    [ "SetActiveObjectState", "class_world_object.html#a6cf43119ed3248e9b9af167e3659543b", null ],
    [ "SetLocationInstanceId", "class_world_object.html#a2fa068bdfada15525672399270537e12", null ],
    [ "SetLocationMapId", "class_world_object.html#a577756281e727788e2095a147a524753", null ],
    [ "SetMap", "class_world_object.html#a7ef77a055df228f1e6580eb52d66cba8", null ],
    [ "SetName", "class_world_object.html#ab0e25e8aff1c21bb10a1e2d2319363d1", null ],
    [ "SetOrientation", "class_world_object.html#a14343f33471333537df2c5ad4815d880", null ],
    [ "StartGroupLoot", "class_world_object.html#a92d5d1e30ca38eceb3b06762eef4622e", null ],
    [ "StopGroupLoot", "class_world_object.html#a457687ec6d06ffce35c43964666528ad", null ],
    [ "SummonCreature", "class_world_object.html#aafdeb9e44afd8d8f8f10f98f6e653612", null ],
    [ "SummonGameObject", "class_world_object.html#a02c8d120f83835113094e8027dafedd1", null ],
    [ "Update", "class_world_object.html#a108517ff0e03fb5e3c19ad576754921a", null ],
    [ "UpdateAllowedPositionZ", "class_world_object.html#aa35cc89ab7bbaca230ebf15a07a1901f", null ],
    [ "UpdateGroundPositionZ", "class_world_object.html#aaac0221148a65e0aec0cc92663f55c9b", null ],
    [ "UpdateObjectVisibility", "class_world_object.html#a632d406c51bc49cf5a688928018ff5bd", null ],
    [ "UpdateVisibilityAndView", "class_world_object.html#acd129b4d5151ee01ef5578a392cac442", null ],
    [ "WorldObjectChangeAccumulator", "class_world_object.html#a9439c654378229266671e5503c51149e", null ],
    [ "m_name", "class_world_object.html#a185b15e6aa5be8ab5f914f8a27cfea06", null ]
];