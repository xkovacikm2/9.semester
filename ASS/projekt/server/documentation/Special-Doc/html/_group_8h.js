var _group_8h =
[
    [ "Roll", "class_roll.html", "class_roll" ],
    [ "InstanceGroupBind", "struct_instance_group_bind.html", "struct_instance_group_bind" ],
    [ "Group", "class_group.html", "class_group" ],
    [ "MemberSlot", "struct_group_1_1_member_slot.html", "struct_group_1_1_member_slot" ],
    [ "GROUP_UPDATE_FLAGS_COUNT", "_group_8h.html#ad409013260cb0aea80c911d24d7b5d00", null ],
    [ "MAX_GROUP_SIZE", "_group_8h.html#a6ce981cf860637abfb692b61c5af0d3b", null ],
    [ "MAX_RAID_SIZE", "_group_8h.html#a768d942fbb5e677e72365dd37c94551c", null ],
    [ "MAX_RAID_SUBGROUPS", "_group_8h.html#afb58b37bfbd0e4d75c26cc40d4066a43", null ],
    [ "TARGET_ICON_COUNT", "_group_8h.html#a32f8049979f884ed8fd7ea6c43971f18", null ],
    [ "GroupMemberOnlineStatus", "_group_8h.html#a330da2ab05c167f677e54f82a222ef9d", [
      [ "MEMBER_STATUS_OFFLINE", "_group_8h.html#a330da2ab05c167f677e54f82a222ef9da3d85c38ccfdc94eda5fe36f186dcb75c", null ],
      [ "MEMBER_STATUS_ONLINE", "_group_8h.html#a330da2ab05c167f677e54f82a222ef9da5b80950bd3340d7de2b5a769001b9fec", null ],
      [ "MEMBER_STATUS_PVP", "_group_8h.html#a330da2ab05c167f677e54f82a222ef9da56b0705f4b91684e2bc6536d45c628bd", null ],
      [ "MEMBER_STATUS_DEAD", "_group_8h.html#a330da2ab05c167f677e54f82a222ef9da68c1ec7408566a34bc264bd9eca1cb53", null ],
      [ "MEMBER_STATUS_GHOST", "_group_8h.html#a330da2ab05c167f677e54f82a222ef9daab8058da6edb35b7c3c459a000e9dd0c", null ],
      [ "MEMBER_STATUS_PVP_FFA", "_group_8h.html#a330da2ab05c167f677e54f82a222ef9daff365b501c0206847eac2c3d02b21961", null ],
      [ "MEMBER_STATUS_UNK3", "_group_8h.html#a330da2ab05c167f677e54f82a222ef9da795eafeca5cb33a35458e5d646975039", null ],
      [ "MEMBER_STATUS_AFK", "_group_8h.html#a330da2ab05c167f677e54f82a222ef9da0fd180911ad96ed8d444b6dfc6fd8777", null ],
      [ "MEMBER_STATUS_DND", "_group_8h.html#a330da2ab05c167f677e54f82a222ef9dacd9a792ab0f7517ea210ea164e3f8b9f", null ]
    ] ],
    [ "GroupType", "_group_8h.html#a3de1267c3b87dda21863fd739439d918", [
      [ "GROUPTYPE_NORMAL", "_group_8h.html#a3de1267c3b87dda21863fd739439d918a06d2671866631d30309707bd4a233b71", null ],
      [ "GROUPTYPE_RAID", "_group_8h.html#a3de1267c3b87dda21863fd739439d918a1d64f52216490e026252bf5be0ce72ea", null ]
    ] ],
    [ "GroupUpdateFlags", "_group_8h.html#a914819bad74fbd5e30a14b5250e00e22", [
      [ "GROUP_UPDATE_FLAG_NONE", "_group_8h.html#a914819bad74fbd5e30a14b5250e00e22aabf53171e29489386fb5f82c83b8291c", null ],
      [ "GROUP_UPDATE_FLAG_STATUS", "_group_8h.html#a914819bad74fbd5e30a14b5250e00e22a84be5c25600d56528690fa6c4f693c39", null ],
      [ "GROUP_UPDATE_FLAG_CUR_HP", "_group_8h.html#a914819bad74fbd5e30a14b5250e00e22a053bf2b40e15c99b4d87212b6224edf5", null ],
      [ "GROUP_UPDATE_FLAG_MAX_HP", "_group_8h.html#a914819bad74fbd5e30a14b5250e00e22a51f854cd0a192b6a26f2ad93cb04f290", null ],
      [ "GROUP_UPDATE_FLAG_POWER_TYPE", "_group_8h.html#a914819bad74fbd5e30a14b5250e00e22a18568d1a780d4ee8bd1261f04789ddec", null ],
      [ "GROUP_UPDATE_FLAG_CUR_POWER", "_group_8h.html#a914819bad74fbd5e30a14b5250e00e22aff7d60d1b0cadfb6faf0a9cc0fb4c647", null ],
      [ "GROUP_UPDATE_FLAG_MAX_POWER", "_group_8h.html#a914819bad74fbd5e30a14b5250e00e22a8e989903a18a03c2c1b04c93a6f7b806", null ],
      [ "GROUP_UPDATE_FLAG_LEVEL", "_group_8h.html#a914819bad74fbd5e30a14b5250e00e22ac3d0e3f073f9bd3b10dfe6815879d721", null ],
      [ "GROUP_UPDATE_FLAG_ZONE", "_group_8h.html#a914819bad74fbd5e30a14b5250e00e22a8ff7f1354cb854bfd7acc1a15572e1ec", null ],
      [ "GROUP_UPDATE_FLAG_POSITION", "_group_8h.html#a914819bad74fbd5e30a14b5250e00e22ab08e141bb44f5282877077ceef93f4db", null ],
      [ "GROUP_UPDATE_FLAG_AURAS", "_group_8h.html#a914819bad74fbd5e30a14b5250e00e22abfc7c4cf05e71e2c6a263d14c50c300e", null ],
      [ "GROUP_UPDATE_FLAG_AURAS_2", "_group_8h.html#a914819bad74fbd5e30a14b5250e00e22ac1eff6a536db93ae43f227a11f740285", null ],
      [ "GROUP_UPDATE_FLAG_PET_GUID", "_group_8h.html#a914819bad74fbd5e30a14b5250e00e22ae059ba28d829c215698fb53fb9c46b81", null ],
      [ "GROUP_UPDATE_FLAG_PET_NAME", "_group_8h.html#a914819bad74fbd5e30a14b5250e00e22ab34868446bbc6c7214746ae0ee07298e", null ],
      [ "GROUP_UPDATE_FLAG_PET_MODEL_ID", "_group_8h.html#a914819bad74fbd5e30a14b5250e00e22ab29f0639aa4364f2937eafa3cf3c9bfd", null ],
      [ "GROUP_UPDATE_FLAG_PET_CUR_HP", "_group_8h.html#a914819bad74fbd5e30a14b5250e00e22a0828c5fe18a98dcec2e52be5a6faf62a", null ],
      [ "GROUP_UPDATE_FLAG_PET_MAX_HP", "_group_8h.html#a914819bad74fbd5e30a14b5250e00e22afc364631361127107eede68c44672067", null ],
      [ "GROUP_UPDATE_FLAG_PET_POWER_TYPE", "_group_8h.html#a914819bad74fbd5e30a14b5250e00e22a8654981d5a32b737e43bb9f799891ab8", null ],
      [ "GROUP_UPDATE_FLAG_PET_CUR_POWER", "_group_8h.html#a914819bad74fbd5e30a14b5250e00e22a356f93308e4e05d59544bb68adeff09e", null ],
      [ "GROUP_UPDATE_FLAG_PET_MAX_POWER", "_group_8h.html#a914819bad74fbd5e30a14b5250e00e22a4110160e7fd5360f13bf4527effcf436", null ],
      [ "GROUP_UPDATE_FLAG_PET_AURAS", "_group_8h.html#a914819bad74fbd5e30a14b5250e00e22abb4365d738698af44649fda4435825af", null ],
      [ "GROUP_UPDATE_FLAG_PET_AURAS_2", "_group_8h.html#a914819bad74fbd5e30a14b5250e00e22a6350be76e26e217278426361395e7089", null ],
      [ "GROUP_UPDATE_MODE_OFFLINE", "_group_8h.html#a914819bad74fbd5e30a14b5250e00e22a0b519aee46cff839048aa0524732712f", null ],
      [ "GROUP_UPDATE_PLAYER", "_group_8h.html#a914819bad74fbd5e30a14b5250e00e22acd82ed94f21636d6216c9773417af97c", null ],
      [ "GROUP_UPDATE_PET", "_group_8h.html#a914819bad74fbd5e30a14b5250e00e22ac4eec7ebcdd88fe72703af3fbcdeef65", null ],
      [ "GROUP_UPDATE_FULL", "_group_8h.html#a914819bad74fbd5e30a14b5250e00e22a23d5a8a1b2b3ff72c3fc74d801590ef6", null ]
    ] ],
    [ "InviteMethod", "_group_8h.html#a1d56a35a4ca5708824b4aee26a909c98", [
      [ "GROUP_JOIN", "_group_8h.html#a1d56a35a4ca5708824b4aee26a909c98a018b9dba3b21bfb7a56e40b3014baa2b", null ],
      [ "GROUP_LFG", "_group_8h.html#a1d56a35a4ca5708824b4aee26a909c98a71d8b196316cf8fd4f70127a7492c2a5", null ]
    ] ],
    [ "LootMethod", "_group_8h.html#ab10ff4858d8b83699f25198a47ad628b", [
      [ "FREE_FOR_ALL", "_group_8h.html#ab10ff4858d8b83699f25198a47ad628baed81cb07716a14961fd4de42df7faf81", null ],
      [ "ROUND_ROBIN", "_group_8h.html#ab10ff4858d8b83699f25198a47ad628ba9e805af736d9be20caf33f5ec0dc1d47", null ],
      [ "MASTER_LOOT", "_group_8h.html#ab10ff4858d8b83699f25198a47ad628ba8d79cbace44f9ce8c9f1d4b2a970837e", null ],
      [ "GROUP_LOOT", "_group_8h.html#ab10ff4858d8b83699f25198a47ad628ba73564f8582e2a666fb3c4f4b4bc96bff", null ],
      [ "NEED_BEFORE_GREED", "_group_8h.html#ab10ff4858d8b83699f25198a47ad628ba5d87501f88aede6518f687729359edb2", null ]
    ] ],
    [ "RemoveMethod", "_group_8h.html#a6759c8870b5ea25efb6bd8492338e6be", [
      [ "GROUP_LEAVE", "_group_8h.html#a6759c8870b5ea25efb6bd8492338e6beac0e0f0bdbead301b53fb3d2aeba2a1da", null ],
      [ "GROUP_KICK", "_group_8h.html#a6759c8870b5ea25efb6bd8492338e6beaebc3f9b4be02acacf612d4f2718587cc", null ]
    ] ],
    [ "RollVote", "_group_8h.html#aacc4f2efff134a1d80260fd50b4feb1f", [
      [ "ROLL_PASS", "_group_8h.html#aacc4f2efff134a1d80260fd50b4feb1fa38fed431ec0ae02ffdd70ed6014f03a5", null ],
      [ "ROLL_NEED", "_group_8h.html#aacc4f2efff134a1d80260fd50b4feb1fa5ccd70d5799413c4175e4278fa6f8995", null ],
      [ "ROLL_GREED", "_group_8h.html#aacc4f2efff134a1d80260fd50b4feb1fa537ec62994db268ea075329b2a8506fa", null ],
      [ "MAX_ROLL_FROM_CLIENT", "_group_8h.html#aacc4f2efff134a1d80260fd50b4feb1fa5bc8cf63fecd6437dd58faa51d0475da", null ],
      [ "ROLL_NOT_EMITED_YET", "_group_8h.html#aacc4f2efff134a1d80260fd50b4feb1fa7bf1ed0f38c3bf4ce45206e26d9af92f", null ],
      [ "ROLL_NOT_VALID", "_group_8h.html#aacc4f2efff134a1d80260fd50b4feb1fa04a758843f4e5424e3a5908d1dd5a02f", null ]
    ] ]
];