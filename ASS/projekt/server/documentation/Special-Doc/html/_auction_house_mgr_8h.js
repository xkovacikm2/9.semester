var _auction_house_mgr_8h =
[
    [ "MAX_AUCTION_HOUSE_TYPE", "group__auctionhouse.html#gaca6f3e6cf68f39827039e665a84c8f63", null ],
    [ "MIN_AUCTION_TIME", "group__auctionhouse.html#ga65e0ff6290b1d068de3d9be0aa323d27", null ],
    [ "sAuctionMgr", "group__auctionhouse.html#gac3e168154709326e99ab24a7d1395906", null ],
    [ "AuctionAction", "group__auctionhouse.html#ga6f0f1b824cc940197c2bc0cd6cb1dd18", [
      [ "AUCTION_STARTED", "group__auctionhouse.html#gga6f0f1b824cc940197c2bc0cd6cb1dd18a5de57764c787512c30d3d8f913c299e4", null ],
      [ "AUCTION_REMOVED", "group__auctionhouse.html#gga6f0f1b824cc940197c2bc0cd6cb1dd18a91c65a21aee0a0c64208ecbe8d9b20f2", null ],
      [ "AUCTION_BID_PLACED", "group__auctionhouse.html#gga6f0f1b824cc940197c2bc0cd6cb1dd18a6744093b19b0b66c8a16ed4d3cf8bbd2", null ]
    ] ],
    [ "AuctionError", "group__auctionhouse.html#gacd1082bcecaa4487e9f32ad1d4cd5225", [
      [ "AUCTION_OK", "group__auctionhouse.html#ggacd1082bcecaa4487e9f32ad1d4cd5225a5e21918df126147d7ca3b5d5b371af5b", null ],
      [ "AUCTION_ERR_INVENTORY", "group__auctionhouse.html#ggacd1082bcecaa4487e9f32ad1d4cd5225a3cb971082e96555c4dc38224c529e898", null ],
      [ "AUCTION_ERR_DATABASE", "group__auctionhouse.html#ggacd1082bcecaa4487e9f32ad1d4cd5225afa44015ad23126c1421a8d8b64e83bca", null ],
      [ "AUCTION_ERR_NOT_ENOUGH_MONEY", "group__auctionhouse.html#ggacd1082bcecaa4487e9f32ad1d4cd5225adb1fc0e63920a9e29fdb63302368a350", null ],
      [ "AUCTION_ERR_ITEM_NOT_FOUND", "group__auctionhouse.html#ggacd1082bcecaa4487e9f32ad1d4cd5225ac00dd0f9752426324bf6c062c9214000", null ],
      [ "AUCTION_ERR_HIGHER_BID", "group__auctionhouse.html#ggacd1082bcecaa4487e9f32ad1d4cd5225a01a2e4fda7e90ed7daa2d5121966a461", null ],
      [ "AUCTION_ERR_BID_INCREMENT", "group__auctionhouse.html#ggacd1082bcecaa4487e9f32ad1d4cd5225a81d7f8f686c02bd6912ab18b26a65fba", null ],
      [ "AUCTION_ERR_BID_OWN", "group__auctionhouse.html#ggacd1082bcecaa4487e9f32ad1d4cd5225ab9860914beb50ccf5540a1a15e1bc488", null ],
      [ "AUCTION_ERR_RESTRICTED_ACCOUNT", "group__auctionhouse.html#ggacd1082bcecaa4487e9f32ad1d4cd5225a89b5ba62f2bef773ae4587e6c3577e70", null ]
    ] ],
    [ "AuctionHouseType", "group__auctionhouse.html#gae063f5b729ba5ff5bbc5bf45d2429063", [
      [ "AUCTION_HOUSE_ALLIANCE", "group__auctionhouse.html#ggae063f5b729ba5ff5bbc5bf45d2429063ae56ac8afffad43ac817634a045684311", null ],
      [ "AUCTION_HOUSE_HORDE", "group__auctionhouse.html#ggae063f5b729ba5ff5bbc5bf45d2429063a80182d928aab531f712da114de84823c", null ],
      [ "AUCTION_HOUSE_NEUTRAL", "group__auctionhouse.html#ggae063f5b729ba5ff5bbc5bf45d2429063a45389d540d5731e050f52454b8190136", null ]
    ] ]
];