var struct_spell_1_1_target_info =
[
    [ "damage", "struct_spell_1_1_target_info.html#a1813d779c7b7efea9cd44d8458252418", null ],
    [ "effectMask", "struct_spell_1_1_target_info.html#aa8f8bf8d4b9d9cc597becabea53e37b0", null ],
    [ "HitInfo", "struct_spell_1_1_target_info.html#a1c4c7b936702260fd43837d92419a6c6", null ],
    [ "missCondition", "struct_spell_1_1_target_info.html#a4987528818ccf19954e185a828eabe60", null ],
    [ "processed", "struct_spell_1_1_target_info.html#ae1661c7a29cbc96d5fc7136f23209ad0", null ],
    [ "reflectResult", "struct_spell_1_1_target_info.html#a8c21d234f8add978f80d9da1a0002657", null ],
    [ "targetGUID", "struct_spell_1_1_target_info.html#ac264ddc84aa6e2370c9735cb2f7049a4", null ],
    [ "timeDelay", "struct_spell_1_1_target_info.html#a37089a7b59855626751b9148d8a6a87c", null ]
];