var namespaceai_1_1mage =
[
    [ "AiObjectContextInternal", "classai_1_1mage_1_1_ai_object_context_internal.html", "classai_1_1mage_1_1_ai_object_context_internal" ],
    [ "MageBuffStrategyFactoryInternal", "classai_1_1mage_1_1_mage_buff_strategy_factory_internal.html", "classai_1_1mage_1_1_mage_buff_strategy_factory_internal" ],
    [ "MageStrategyFactoryInternal", "classai_1_1mage_1_1_mage_strategy_factory_internal.html", "classai_1_1mage_1_1_mage_strategy_factory_internal" ],
    [ "StrategyFactoryInternal", "classai_1_1mage_1_1_strategy_factory_internal.html", "classai_1_1mage_1_1_strategy_factory_internal" ],
    [ "TriggerFactoryInternal", "classai_1_1mage_1_1_trigger_factory_internal.html", "classai_1_1mage_1_1_trigger_factory_internal" ]
];