var class_playerbot_mgr =
[
    [ "PlayerbotMgr", "class_playerbot_mgr.html#a7ecb8c28081674313c9105eb0050df88", null ],
    [ "~PlayerbotMgr", "class_playerbot_mgr.html#a728305dca95c41c19862339440b24c0d", null ],
    [ "GetMaster", "class_playerbot_mgr.html#a5cd7b6fd4554126e76e239dc11fb7c91", null ],
    [ "HandleCommand", "class_playerbot_mgr.html#a47706399dd725a8376ca05f99654857e", null ],
    [ "HandleMasterIncomingPacket", "class_playerbot_mgr.html#aa9e61de123872889410032d8c0fbdcb2", null ],
    [ "HandleMasterOutgoingPacket", "class_playerbot_mgr.html#ae4d7f73646739f956ece51bd723663a9", null ],
    [ "OnBotLoginInternal", "class_playerbot_mgr.html#ae61ec48ee11cce1e6e22e5bf59a49365", null ],
    [ "SaveToDB", "class_playerbot_mgr.html#a3b6042286737121d895e4bda5fde725f", null ],
    [ "UpdateAIInternal", "class_playerbot_mgr.html#a7a3c7e224a5b5db45b71dc685ff80e16", null ]
];