var struct_mangos_string_locale =
[
    [ "MangosStringLocale", "struct_mangos_string_locale.html#a08c19ac4cf66e66b87b36f49322834ab", null ],
    [ "Content", "struct_mangos_string_locale.html#ac22fa3f2bd66c95d59d3f3b1770961c6", null ],
    [ "Emote", "struct_mangos_string_locale.html#afe34b985dd29fb128c09e809847a5974", null ],
    [ "LanguageId", "struct_mangos_string_locale.html#a0987f46da6207b44b61dd65530d73b4b", null ],
    [ "SoundId", "struct_mangos_string_locale.html#a2aa9467eb94858f44a639f886785e009", null ],
    [ "Type", "struct_mangos_string_locale.html#a48c7f04e690b49d3887f28707358a1a0", null ]
];