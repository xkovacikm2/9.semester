var _spell_mgr_8cpp =
[
    [ "SpellRankHelper", "struct_spell_rank_helper.html", "struct_spell_rank_helper" ],
    [ "DoSpellProcEvent", "struct_do_spell_proc_event.html", "struct_do_spell_proc_event" ],
    [ "DoSpellProcItemEnchant", "struct_do_spell_proc_item_enchant.html", "struct_do_spell_proc_item_enchant" ],
    [ "DoSpellBonuses", "struct_do_spell_bonuses.html", "struct_do_spell_bonuses" ],
    [ "DoSpellThreat", "struct_do_spell_threat.html", "struct_do_spell_threat" ],
    [ "AbilitySpellPrevMap", "_spell_mgr_8cpp.html#a8be465528cccd966da90f14bdffd562a", null ],
    [ "CalculateDefaultCoefficient", "_spell_mgr_8cpp.html#a73f3482c49fef8f71f4b44103ba0374c", null ],
    [ "CalculateSpellDuration", "_spell_mgr_8cpp.html#a645e807222d5781802586fdefb8a97a8", null ],
    [ "CompareAuraRanks", "_spell_mgr_8cpp.html#ae072797cf3522e5841b1eaf2a45e2c84", null ],
    [ "GetDiminishingReturnsGroupForSpell", "_spell_mgr_8cpp.html#a46823309ec434fd0636f6f06099b8919", null ],
    [ "GetDiminishingReturnsGroupType", "_spell_mgr_8cpp.html#ab0fdc6c7b1e9317d6f6c0480d734b4f1", null ],
    [ "GetErrorAtShapeshiftedCast", "_spell_mgr_8cpp.html#ab4874b247401539ddc5f6838fb5abf6b", null ],
    [ "GetSpellAuraMaxTicks", "_spell_mgr_8cpp.html#a55c2fdb0b375a3f9757d0a0c44f7bc3e", null ],
    [ "GetSpellAuraMaxTicks", "_spell_mgr_8cpp.html#aee2fd68013d926425e4373bd0280c83b", null ],
    [ "GetSpellCastTime", "_spell_mgr_8cpp.html#ad9b7a1e5b8fb66d9e19ff4913f4fb9a5", null ],
    [ "GetSpellCastTimeForBonus", "_spell_mgr_8cpp.html#a3d7b8a061c4a21e4bcbfeac982122f8b", null ],
    [ "GetSpellDuration", "_spell_mgr_8cpp.html#ac8cbcaf933a719c961ea28ae0be73c10", null ],
    [ "GetSpellMaxDuration", "_spell_mgr_8cpp.html#a32fca1eb003a195410671781213da255", null ],
    [ "GetSpellSpecific", "_spell_mgr_8cpp.html#a417c87a24aaff623d2f2fcda960a85a4", null ],
    [ "GetWeaponAttackType", "_spell_mgr_8cpp.html#a8a6c654559ee1eedaa1c460dd60f1823", null ],
    [ "IsDiminishingReturnsGroupDurationLimited", "_spell_mgr_8cpp.html#a423a80c889a3456a2b55e0e9c22ad060", null ],
    [ "IsExplicitNegativeTarget", "_spell_mgr_8cpp.html#a6ca453d3ff552518d1ae5793b3a7c607", null ],
    [ "IsExplicitPositiveTarget", "_spell_mgr_8cpp.html#a4b1d5260f309c0f6d4df3420b369c33e", null ],
    [ "IsNoStackAuraDueToAura", "_spell_mgr_8cpp.html#a65e3116010b740feb376336e42cdbac3", null ],
    [ "IsPassiveSpell", "_spell_mgr_8cpp.html#a28b958e9db34bdad035df1f81ed2a280", null ],
    [ "IsPassiveSpell", "_spell_mgr_8cpp.html#a4de26724a13236356425783200ebc863", null ],
    [ "IsPositiveEffect", "_spell_mgr_8cpp.html#a64839b6b59c2d67639371e895c55d375", null ],
    [ "IsPositiveSpell", "_spell_mgr_8cpp.html#a32f217c383a78e6c51e7265e546b869d", null ],
    [ "IsPositiveSpell", "_spell_mgr_8cpp.html#a4afb42807a67d79e18ef7b0eefd8c593", null ],
    [ "IsPositiveTarget", "_spell_mgr_8cpp.html#a9640cf0b520c8120cc8048ac846db839", null ],
    [ "IsPrimaryProfessionSkill", "_spell_mgr_8cpp.html#a09c4e23bd85fab812b67beafbbf1b7b3", null ],
    [ "IsSingleFromSpellSpecificPerTarget", "_spell_mgr_8cpp.html#a17f362aa623cfcdbd44fa7db41b7077b", null ],
    [ "IsSingleFromSpellSpecificPerTargetPerCaster", "_spell_mgr_8cpp.html#a19e8eb231b6372a32f6949b233f4a439", null ],
    [ "IsSingleFromSpellSpecificSpellRanksPerTarget", "_spell_mgr_8cpp.html#aef2a9cf0dec13bfdaec18f7a5186ba8d", null ],
    [ "IsSingleTargetSpell", "_spell_mgr_8cpp.html#aed7b91459a4bd42d0aa48f420338f41f", null ],
    [ "IsSingleTargetSpells", "_spell_mgr_8cpp.html#a07825bea62b9f0a964433432b33d0f88", null ]
];