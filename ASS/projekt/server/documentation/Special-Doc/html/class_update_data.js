var class_update_data =
[
    [ "UpdateData", "class_update_data.html#a7164bb9338d6362f427d75025c9026b6", null ],
    [ "AddOutOfRangeGUID", "class_update_data.html#a82baab8cf85aa7934f274b72ad137701", null ],
    [ "AddOutOfRangeGUID", "class_update_data.html#afffbe6560b958724b7608d9f4f85b402", null ],
    [ "AddUpdateBlock", "class_update_data.html#a620f0036078833a3b5ce1245274595f8", null ],
    [ "BuildPacket", "class_update_data.html#a105e0cfcee2ca9a89ee4c7d1d0af2475", null ],
    [ "Clear", "class_update_data.html#adc0750db190d94fed3bfef72da75066c", null ],
    [ "Compress", "class_update_data.html#af5476907b5ca746388d116311d72b809", null ],
    [ "GetOutOfRangeGUIDs", "class_update_data.html#a629ac60685bb97abd8af18316c6091a1", null ],
    [ "HasData", "class_update_data.html#a5f0a63840a8913b4c552ff70032c6e96", null ],
    [ "m_blockCount", "class_update_data.html#a791f9606867a40d018f67600e338a08e", null ],
    [ "m_data", "class_update_data.html#ac21a3bda51f8d1d2b71be6d00d760e38", null ],
    [ "m_outOfRangeGUIDs", "class_update_data.html#ae65b63b314fdb41541aaebfe891e6dcb", null ]
];