var class_loot_template =
[
    [ "LootGroup", "class_loot_template_1_1_loot_group.html", "class_loot_template_1_1_loot_group" ],
    [ "AddEntry", "class_loot_template.html#a38f01b97807db5efe42dea0ee54a0562", null ],
    [ "CheckLootRefs", "class_loot_template.html#a5520dece9f294c61719e9d3beb36fbf2", null ],
    [ "HasQuestDrop", "class_loot_template.html#a1cc0e6e0bad555fe5f6aad6df7bacf24", null ],
    [ "HasQuestDropForPlayer", "class_loot_template.html#adee35bb1f0810a48ca102a81a7ff0ea7", null ],
    [ "HasSharedQuestDropForPlayer", "class_loot_template.html#a8fd5dc2baed53088882fc6c9247f3acc", null ],
    [ "HasStartingQuestDropForPlayer", "class_loot_template.html#affd8665c2760a211efce506212344b45", null ],
    [ "Process", "class_loot_template.html#abf31fb53ec30bf32b980ec93267fdf47", null ],
    [ "Verify", "class_loot_template.html#adedb1f03278aaeac63deb3dd32e07329", null ]
];