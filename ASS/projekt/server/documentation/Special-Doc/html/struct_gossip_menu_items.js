var struct_gossip_menu_items =
[
    [ "action_menu_id", "struct_gossip_menu_items.html#a649ec6c84b2bba66254279621cd23cf1", null ],
    [ "action_poi_id", "struct_gossip_menu_items.html#aa5aa3db7a52b7552dbcb761159086f73", null ],
    [ "action_script_id", "struct_gossip_menu_items.html#aa06769b6d702f84514191a550eeae211", null ],
    [ "box_coded", "struct_gossip_menu_items.html#aaf431bfe122752facdd312cc078b96b1", null ],
    [ "box_text", "struct_gossip_menu_items.html#aaf06321de6bfbf1e05948ccb6a6ae2fd", null ],
    [ "conditionId", "struct_gossip_menu_items.html#a599c55d66bac17dbff2166e5cf7abfae", null ],
    [ "id", "struct_gossip_menu_items.html#a618c98d71a721e2529df5f3b62beb341", null ],
    [ "menu_id", "struct_gossip_menu_items.html#afc5f71cc4afd396a279b1d0c76f52ee1", null ],
    [ "npc_option_npcflag", "struct_gossip_menu_items.html#a0e80201c0958f2acd809d84148c496c6", null ],
    [ "option_icon", "struct_gossip_menu_items.html#a740a04db2f1d237627d2613a607eb552", null ],
    [ "option_id", "struct_gossip_menu_items.html#ab0aed43f9922090a4ee075b1c35676d8", null ],
    [ "option_text", "struct_gossip_menu_items.html#a15455ccf743084d94192406866353acc", null ]
];