var struct_do_spell_threat =
[
    [ "DoSpellThreat", "struct_do_spell_threat.html#aeb7e70d3b64432427f9d24951e1e2a2b", null ],
    [ "AddEntry", "struct_do_spell_threat.html#a2145a2dec5c89030441c7ec38fb1d1d1", null ],
    [ "HasEntry", "struct_do_spell_threat.html#a9ea7556a9aa23f32d70a429219695b5f", null ],
    [ "IsValidCustomRank", "struct_do_spell_threat.html#a5cfaffbd6266d3ca7ec0e4e766f2c731", null ],
    [ "operator()", "struct_do_spell_threat.html#a0c3ab10da507bd9da5628a00f7fe121d", null ],
    [ "SetStateToEntry", "struct_do_spell_threat.html#a83e5915b372fd66d71e36b777bdf9eb3", null ],
    [ "TableName", "struct_do_spell_threat.html#a641f9aaaafc24702e40ee530d44b77e6", null ],
    [ "count", "struct_do_spell_threat.html#aa073ab1340ed3c8d138d30e6da3bec54", null ],
    [ "state", "struct_do_spell_threat.html#a852bef4f6639594fc7f83a4a01b9033d", null ],
    [ "threatMap", "struct_do_spell_threat.html#a2c94c4bf48f757bb274aff434233f579", null ]
];