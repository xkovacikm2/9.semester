var struct_grid_map_liquid_header =
[
    [ "flags", "struct_grid_map_liquid_header.html#ab7d2c47a7cb962ceb952233cdb13d234", null ],
    [ "fourcc", "struct_grid_map_liquid_header.html#a8c3b38142f21ad32feda33aa20a45f1c", null ],
    [ "height", "struct_grid_map_liquid_header.html#abe69fca59ded6cceeeee24c51ea77a77", null ],
    [ "liquidLevel", "struct_grid_map_liquid_header.html#a535a62a6172c7d9878980eade7e282a2", null ],
    [ "liquidType", "struct_grid_map_liquid_header.html#a9a066da72ce0eabbc307e23620807153", null ],
    [ "offsetX", "struct_grid_map_liquid_header.html#a301ec12e827eabfe4cc9b3f109f9eb61", null ],
    [ "offsetY", "struct_grid_map_liquid_header.html#ab34f742bd0003cdade8554e245fc8b54", null ],
    [ "width", "struct_grid_map_liquid_header.html#a7ac826b60fb9ec98fe81487623d3e4b0", null ]
];