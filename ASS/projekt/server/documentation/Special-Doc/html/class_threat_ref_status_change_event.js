var class_threat_ref_status_change_event =
[
    [ "ThreatRefStatusChangeEvent", "class_threat_ref_status_change_event.html#a97d1b7a1ff9f2c6090e6dfa6d33db2c7", null ],
    [ "ThreatRefStatusChangeEvent", "class_threat_ref_status_change_event.html#a5941cfd844f89dbc371a9ef69e53f6ef", null ],
    [ "ThreatRefStatusChangeEvent", "class_threat_ref_status_change_event.html#ab7736e7438ab0741a66aee7cfe72b308", null ],
    [ "ThreatRefStatusChangeEvent", "class_threat_ref_status_change_event.html#a45c52c850dc6b07ea913e8e83cc32958", null ],
    [ "getBValue", "class_threat_ref_status_change_event.html#a4bd7c3ddca2aaa6a1418390b6df8ff67", null ],
    [ "getFValue", "class_threat_ref_status_change_event.html#a660c6f8258ebb7c6abd81d51c7ff3399", null ],
    [ "getIValue", "class_threat_ref_status_change_event.html#ab9b40ed1c0448ee5de158c26642cacc2", null ],
    [ "getReference", "class_threat_ref_status_change_event.html#abee0e0ebf55554342f55191cf20a726a", null ],
    [ "GetThreatManager", "class_threat_ref_status_change_event.html#a6eb7aeb8c941f98ef6f7ec6d57b4375b", null ],
    [ "setBValue", "class_threat_ref_status_change_event.html#a41735bb86d49572843cd488deb79e631", null ],
    [ "setThreatManager", "class_threat_ref_status_change_event.html#a2d7418c9e89c764a1f9345c2c2645b25", null ],
    [ "iBValue", "class_threat_ref_status_change_event.html#a24f3b0ec126bc8d432c9662854065964", null ],
    [ "iFValue", "class_threat_ref_status_change_event.html#a057aab2519feac28e980e53c364f4358", null ],
    [ "iIValue", "class_threat_ref_status_change_event.html#a10f8d82c037d0e742b3c9742489d1da9", null ]
];