var namespaces =
[
    [ "ACE_Based", "namespace_a_c_e___based.html", null ],
    [ "ahbot", "namespaceahbot.html", null ],
    [ "ai", "namespaceai.html", "namespaceai" ],
    [ "AIRegistry", "namespace_a_i_registry.html", null ],
    [ "ByteConverter", "namespace_byte_converter.html", null ],
    [ "CharacterDatabaseCleaner", "namespace_character_database_cleaner.html", null ],
    [ "ConvertConditions", "namespace_convert_conditions.html", null ],
    [ "DisableMgr", "namespace_disable_mgr.html", null ],
    [ "FactorySelector", "namespace_factory_selector.html", null ],
    [ "G3D", "namespace_g3_d.html", null ],
    [ "MaNGOS", "namespace_ma_n_g_o_s.html", "namespace_ma_n_g_o_s" ],
    [ "MMAP", "namespace_m_m_a_p.html", null ],
    [ "mmap_extract", "namespacemmap__extract.html", null ],
    [ "Movement", "namespace_movement.html", null ],
    [ "VMAP", "namespace_v_m_a_p.html", null ],
    [ "WardenState", "namespace_warden_state.html", null ]
];