var struct_spell_proc_event_entry =
[
    [ "cooldown", "struct_spell_proc_event_entry.html#a0cd88c27979ccc5284aa46846db32a7c", null ],
    [ "customChance", "struct_spell_proc_event_entry.html#adf6742b70f0b00abddf1da6f45da3d16", null ],
    [ "ppmRate", "struct_spell_proc_event_entry.html#a931b2df4a187482e32ef252c3320080a", null ],
    [ "procEx", "struct_spell_proc_event_entry.html#a911770f28fe36243593b12fe92d6fbec", null ],
    [ "procFlags", "struct_spell_proc_event_entry.html#af253ef32b9de49fe85d0fe6f06855a60", null ],
    [ "schoolMask", "struct_spell_proc_event_entry.html#a9a6d38ee4e4a35066780ef924c63f92f", null ],
    [ "spellFamilyMask", "struct_spell_proc_event_entry.html#a37f0f77b0edac897c68ff113b366dea8", null ],
    [ "spellFamilyName", "struct_spell_proc_event_entry.html#a5ffa91b3f66ebb0ac34f30e1c532fb3a", null ]
];