var struct_ma_n_g_o_s_1_1_spell_notifier_creature_and_player =
[
    [ "SpellNotifierCreatureAndPlayer", "struct_ma_n_g_o_s_1_1_spell_notifier_creature_and_player.html#a8b1dbc60c9a6229f196c506e1f764d7d", null ],
    [ "GetCenterX", "struct_ma_n_g_o_s_1_1_spell_notifier_creature_and_player.html#a8c674f90c3d5c58499705ea1843873dc", null ],
    [ "GetCenterY", "struct_ma_n_g_o_s_1_1_spell_notifier_creature_and_player.html#ad0294a1224109c2aa306575e5105aece", null ],
    [ "Visit", "struct_ma_n_g_o_s_1_1_spell_notifier_creature_and_player.html#aa0e642ba7987096537a1244cab8d095b", null ],
    [ "Visit", "struct_ma_n_g_o_s_1_1_spell_notifier_creature_and_player.html#ad8fd598c21dca9a91bcf75ace098677e", null ],
    [ "Visit", "struct_ma_n_g_o_s_1_1_spell_notifier_creature_and_player.html#a2702139ba8a764117a73749126c6fb72", null ],
    [ "Visit", "struct_ma_n_g_o_s_1_1_spell_notifier_creature_and_player.html#ada6cb2b0781f02aae85121d13887983d", null ],
    [ "Visit", "struct_ma_n_g_o_s_1_1_spell_notifier_creature_and_player.html#a7077068ee72c8cbddf74671df57085b1", null ],
    [ "i_castingObject", "struct_ma_n_g_o_s_1_1_spell_notifier_creature_and_player.html#a6fd4a620c8d56c4d975286ea066503b2", null ],
    [ "i_centerX", "struct_ma_n_g_o_s_1_1_spell_notifier_creature_and_player.html#a122981fbf671175fc5fbc88dec934fad", null ],
    [ "i_centerY", "struct_ma_n_g_o_s_1_1_spell_notifier_creature_and_player.html#a23243b919cbecc684027d9a77e4e0126", null ],
    [ "i_centerZ", "struct_ma_n_g_o_s_1_1_spell_notifier_creature_and_player.html#abc555d6013806015f9b5ba9bc01fe130", null ],
    [ "i_data", "struct_ma_n_g_o_s_1_1_spell_notifier_creature_and_player.html#a5eb239e0da8633ed08c9f5eb838f592b", null ],
    [ "i_originalCaster", "struct_ma_n_g_o_s_1_1_spell_notifier_creature_and_player.html#a0b059a68a070a9fe553b6386df22aa91", null ],
    [ "i_playerControlled", "struct_ma_n_g_o_s_1_1_spell_notifier_creature_and_player.html#a733253ccb8b4450a98594de09fc239c1", null ],
    [ "i_push_type", "struct_ma_n_g_o_s_1_1_spell_notifier_creature_and_player.html#ad25dcc3f8afebc63a259b08f09b43318", null ],
    [ "i_radius", "struct_ma_n_g_o_s_1_1_spell_notifier_creature_and_player.html#a4c10f7423e253174873bd4b424b0dac4", null ],
    [ "i_spell", "struct_ma_n_g_o_s_1_1_spell_notifier_creature_and_player.html#a2c29732409cba8b51077ce3940dcf133", null ],
    [ "i_TargetType", "struct_ma_n_g_o_s_1_1_spell_notifier_creature_and_player.html#a64e2bf75a8bb0d5ce53ee4539e0c393c", null ]
];