var class_hostile_ref_manager =
[
    [ "HostileRefManager", "class_hostile_ref_manager.html#af18221714cf34c11867e7de7b92cdcbb", null ],
    [ "~HostileRefManager", "class_hostile_ref_manager.html#a78c715326ed4d9d4f9d4be25576706a2", null ],
    [ "addThreatPercent", "class_hostile_ref_manager.html#a4e577dc44aea40471e596e494a6ae40f", null ],
    [ "deleteReference", "class_hostile_ref_manager.html#abc219c69f8e2eef12335b19de16bf214", null ],
    [ "deleteReferences", "class_hostile_ref_manager.html#ab994687d3500d51b1195f395efaf7704", null ],
    [ "deleteReferencesForFaction", "class_hostile_ref_manager.html#a3f9d42b6565f62b24207cd8f04e28325", null ],
    [ "getFirst", "class_hostile_ref_manager.html#a1453ae219377cd6805357054ce0a186c", null ],
    [ "getOwner", "class_hostile_ref_manager.html#acf8ef7f71f764fe57101310b561e0bc0", null ],
    [ "setOnlineOfflineState", "class_hostile_ref_manager.html#af58dd436c3e017bca4b309fc19b8bf62", null ],
    [ "setOnlineOfflineState", "class_hostile_ref_manager.html#a9c2c8ddcc39f427cdbd29f10ef59a648", null ],
    [ "threatAssist", "class_hostile_ref_manager.html#a9bb6db840486d7605a41a28690ecef93", null ],
    [ "updateThreatTables", "class_hostile_ref_manager.html#affb3ae9e4bf6881573d48e46cda177f7", null ]
];