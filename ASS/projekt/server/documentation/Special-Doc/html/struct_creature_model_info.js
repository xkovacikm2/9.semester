var struct_creature_model_info =
[
    [ "bounding_radius", "struct_creature_model_info.html#a9f4a794b06bcbe31fc0a104cc97b91a4", null ],
    [ "combat_reach", "struct_creature_model_info.html#ac271edb10093db19e4df1396053f820d", null ],
    [ "gender", "struct_creature_model_info.html#aa5c8134851ff4f7ab6ad2af3b6e1dc1e", null ],
    [ "modelid", "struct_creature_model_info.html#a0f69c477e8a6db93803a6dcb3f21cfa9", null ],
    [ "modelid_other_gender", "struct_creature_model_info.html#a222b3c513a64b385954ea549e67f4255", null ],
    [ "modelid_other_team", "struct_creature_model_info.html#a855ba8d37ddf5d8e32a66ee1dcce9630", null ]
];