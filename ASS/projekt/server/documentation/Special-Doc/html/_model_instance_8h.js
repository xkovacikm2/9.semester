var _model_instance_8h =
[
    [ "ModelSpawn", "class_v_m_a_p_1_1_model_spawn.html", "class_v_m_a_p_1_1_model_spawn" ],
    [ "ModelInstance", "class_v_m_a_p_1_1_model_instance.html", "class_v_m_a_p_1_1_model_instance" ],
    [ "ModelFlags", "_model_instance_8h.html#ac540ff16efadcc07eeb9d8511b94ccce", [
      [ "MOD_M2", "_model_instance_8h.html#ac540ff16efadcc07eeb9d8511b94ccceadfad2b6f6278e720b00314cd5117b673", null ],
      [ "MOD_WORLDSPAWN", "_model_instance_8h.html#ac540ff16efadcc07eeb9d8511b94cccea3a6165ebc7b838e28ef59670dc84dc46", null ],
      [ "MOD_HAS_BOUND", "_model_instance_8h.html#ac540ff16efadcc07eeb9d8511b94ccceaa47940cd802c5225d2babb37d24c7f09", null ]
    ] ]
];