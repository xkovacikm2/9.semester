var _player_dump_8h =
[
    [ "PlayerDump", "class_player_dump.html", "class_player_dump" ],
    [ "PlayerDumpWriter", "class_player_dump_writer.html", "class_player_dump_writer" ],
    [ "PlayerDumpReader", "class_player_dump_reader.html", "class_player_dump_reader" ],
    [ "DumpReturn", "_player_dump_8h.html#a4471667403db9bc7532f371d28743866", [
      [ "DUMP_SUCCESS", "_player_dump_8h.html#a4471667403db9bc7532f371d28743866a6516d3543697022dbf635913b248e48e", null ],
      [ "DUMP_FILE_OPEN_ERROR", "_player_dump_8h.html#a4471667403db9bc7532f371d28743866a01a5aec7fbe2bac9208d139b46a23656", null ],
      [ "DUMP_TOO_MANY_CHARS", "_player_dump_8h.html#a4471667403db9bc7532f371d28743866a73b27eff0adff2ab7bfd73e34aeb307d", null ],
      [ "DUMP_UNEXPECTED_END", "_player_dump_8h.html#a4471667403db9bc7532f371d28743866ab47c1d5d34bb544e84a3dc02c235a873", null ],
      [ "DUMP_FILE_BROKEN", "_player_dump_8h.html#a4471667403db9bc7532f371d28743866a86acf22616e301a2ffef9ba80e227ed6", null ]
    ] ],
    [ "DumpTableType", "_player_dump_8h.html#ad4739d865080bb5590831b606365e7ef", [
      [ "DTT_CHARACTER", "_player_dump_8h.html#ad4739d865080bb5590831b606365e7efaea3cb16043024e5fdfc4f3301d2df323", null ],
      [ "DTT_CHAR_TABLE", "_player_dump_8h.html#ad4739d865080bb5590831b606365e7efa3c784c38ccd07be78692e3e03bb53d94", null ],
      [ "DTT_INVENTORY", "_player_dump_8h.html#ad4739d865080bb5590831b606365e7efa137a1112df8b9582403fec12c50fea7f", null ],
      [ "DTT_MAIL", "_player_dump_8h.html#ad4739d865080bb5590831b606365e7efa581ecbcaf9623d952b28fd4e964ae67d", null ],
      [ "DTT_MAIL_ITEM", "_player_dump_8h.html#ad4739d865080bb5590831b606365e7efa917099d0dd625e7aa46b6358fb4c036b", null ],
      [ "DTT_ITEM", "_player_dump_8h.html#ad4739d865080bb5590831b606365e7efad5be0dba7076e09532f3eb8760b8501d", null ],
      [ "DTT_ITEM_GIFT", "_player_dump_8h.html#ad4739d865080bb5590831b606365e7efadead833bce15df38e80b9eee85b1d847", null ],
      [ "DTT_ITEM_LOOT", "_player_dump_8h.html#ad4739d865080bb5590831b606365e7efa7ae60daf917b0ff39db876d52aa66f0c", null ],
      [ "DTT_PET", "_player_dump_8h.html#ad4739d865080bb5590831b606365e7efacb52988d860dbf41c2e2a5e2650d95c8", null ],
      [ "DTT_PET_TABLE", "_player_dump_8h.html#ad4739d865080bb5590831b606365e7efa4b50591bf81a27c7f6300f7733b14ac4", null ],
      [ "DTT_ITEM_TEXT", "_player_dump_8h.html#ad4739d865080bb5590831b606365e7efa2386ce04eeb0315a9387cd5d3ace7a07", null ]
    ] ]
];