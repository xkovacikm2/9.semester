var class_auction_bot_seller =
[
    [ "ItemPool", "class_auction_bot_seller.html#a0b28b6d467375e6cb060eb87214ebafc", null ],
    [ "AuctionBotSeller", "group__auctionbot.html#gacf6de0d2f7a80b5557317a4118353b0e", null ],
    [ "~AuctionBotSeller", "group__auctionbot.html#ga3fd57dc9d76e46e3ff168ccc5e04affa", null ],
    [ "addNewAuctions", "group__auctionbot.html#gaa77a1850e64c804419c8ec2d64a029a8", null ],
    [ "Initialize", "group__auctionbot.html#ga027006df953f7c6bdc3aadeb8081c6ca", null ],
    [ "LoadConfig", "group__auctionbot.html#gab911944b4319a03857a3fd36073651e6", null ],
    [ "SetItemsAmount", "group__auctionbot.html#gaf9f5bd4265ffc999ab429fc1390f832b", null ],
    [ "SetItemsAmountForQuality", "group__auctionbot.html#ga0a97d5b0227ddd2438ef7c72a4485777", null ],
    [ "SetItemsRatio", "group__auctionbot.html#gaef025a67bd8cb34294a7ce723198bd38", null ],
    [ "SetItemsRatioForHouse", "group__auctionbot.html#gaec18797d78422a33adc96016245846d9", null ],
    [ "Update", "group__auctionbot.html#ga8480eccd2b1a18a1f16ae6c230051195", null ]
];