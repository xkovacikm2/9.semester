var _warden_check_mgr_8h =
[
    [ "WardenCheck", "struct_warden_check.html", "struct_warden_check" ],
    [ "WardenCheckResult", "struct_warden_check_result.html", "struct_warden_check_result" ],
    [ "WardenCheckMgr", "class_warden_check_mgr.html", "class_warden_check_mgr" ],
    [ "sWardenCheckMgr", "_warden_check_mgr_8h.html#a367963ecc82479f8bea3aea9e235a85c", null ],
    [ "WardenActions", "_warden_check_mgr_8h.html#acdd5041a8c9aa2f418b3cd0eaa552a1a", [
      [ "WARDEN_ACTION_LOG", "_warden_check_mgr_8h.html#acdd5041a8c9aa2f418b3cd0eaa552a1aa886d1d7fc710f19b1f98a28dd6652d81", null ],
      [ "WARDEN_ACTION_KICK", "_warden_check_mgr_8h.html#acdd5041a8c9aa2f418b3cd0eaa552a1aa3e6856d34f7e97f6cafcce4a86315d13", null ],
      [ "WARDEN_ACTION_BAN", "_warden_check_mgr_8h.html#acdd5041a8c9aa2f418b3cd0eaa552a1aa420c50c42a2e76090d4a4ab690885ae0", null ]
    ] ]
];