var struct_skill_line_ability_entry =
[
    [ "classmask", "struct_skill_line_ability_entry.html#a9f508ac9eb6f904153fd89d2953c33f2", null ],
    [ "forward_spellid", "struct_skill_line_ability_entry.html#afec443857e629a9972ec26b91a1a89cc", null ],
    [ "id", "struct_skill_line_ability_entry.html#af3f16baafc71e93656f3b9175f702bb3", null ],
    [ "learnOnGetSkill", "struct_skill_line_ability_entry.html#abeb24e41b110faba4a3dc75b92254256", null ],
    [ "max_value", "struct_skill_line_ability_entry.html#aa45c99f2ba0f9202ee77deb8bed3948d", null ],
    [ "min_value", "struct_skill_line_ability_entry.html#a4ea1e0e0968f5dd3a4406786d1ea15da", null ],
    [ "racemask", "struct_skill_line_ability_entry.html#ac94097fbd4dc4315ceba020af568ff4d", null ],
    [ "req_skill_value", "struct_skill_line_ability_entry.html#a0c5b56f324739465969e24afade8c430", null ],
    [ "reqtrainpoints", "struct_skill_line_ability_entry.html#abe1d2496c07b3f3986a22499339f7ab3", null ],
    [ "skillId", "struct_skill_line_ability_entry.html#adff1e5a7840ae7a2337e10e67a189c60", null ],
    [ "spellId", "struct_skill_line_ability_entry.html#a11e4e90943ac22ffa4481fd8fc303cfa", null ]
];