var class_battle_ground_queue =
[
    [ "BattleGroundQueue", "class_battle_ground_queue.html#a8eb51f824e6d65b6567388317038db67", null ],
    [ "~BattleGroundQueue", "class_battle_ground_queue.html#ace4e82d788dc6ba44b32804a006b6e2c", null ],
    [ "AddGroup", "class_battle_ground_queue.html#aa3b744703268b140558eaf93cf6e37fe", null ],
    [ "CheckNormalMatch", "class_battle_ground_queue.html#aa14751adbb21af0b9564587379676d4b", null ],
    [ "CheckPremadeMatch", "class_battle_ground_queue.html#ac2789d0837be2c27a9ef0abbd104c40f", null ],
    [ "FillPlayersToBG", "class_battle_ground_queue.html#a308c9ae6e697529ff333773db88dbe4e", null ],
    [ "GetAverageQueueWaitTime", "class_battle_ground_queue.html#af5692c071843a07ae15768db4eb49894", null ],
    [ "GetPlayerGroupInfoData", "class_battle_ground_queue.html#adbae04653890b5cd8d8103cc3df0c2ff", null ],
    [ "IsPlayerInvited", "class_battle_ground_queue.html#a963fa4453528ae9410156aa4d35bb835", null ],
    [ "PlayerInvitedToBGUpdateAverageWaitTime", "class_battle_ground_queue.html#a78919d55c7762a36958ca40f087e2fb6", null ],
    [ "RemovePlayer", "class_battle_ground_queue.html#ac8a3d079a4fa1d692c647dec9e6ded27", null ],
    [ "Update", "class_battle_ground_queue.html#a33cf5edea9e6de411ff39aae7e740bd3", null ]
];