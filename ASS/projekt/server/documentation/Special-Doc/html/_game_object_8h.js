var _game_object_8h =
[
    [ "GameObjectInfo", "struct_game_object_info.html", "struct_game_object_info" ],
    [ "GameObjectLocale", "struct_game_object_locale.html", "struct_game_object_locale" ],
    [ "GameObjectData", "struct_game_object_data.html", "struct_game_object_data" ],
    [ "GameObject", "class_game_object.html", "class_game_object" ],
    [ "FISHING_BOBBER_READY_TIME", "_game_object_8h.html#aa7242bc5664b993aa0dfe458bf845b13", null ],
    [ "GO_ANIMPROGRESS_DEFAULT", "_game_object_8h.html#a82e3ff3b8f0dff7dc9c5301b1e2f526b", null ],
    [ "MAX_GO_STATE", "_game_object_8h.html#ac148792c2982ecdddc593bf3871aa520", null ],
    [ "CapturePointSliderValue", "_game_object_8h.html#a2bbef7231812d3025d485bf569292bf3", [
      [ "CAPTURE_SLIDER_ALLIANCE", "_game_object_8h.html#a2bbef7231812d3025d485bf569292bf3a0992c2b6d05ba725503ab698aef68b23", null ],
      [ "CAPTURE_SLIDER_HORDE", "_game_object_8h.html#a2bbef7231812d3025d485bf569292bf3a39064221e0e50d024137ccbb8622505d", null ],
      [ "CAPTURE_SLIDER_MIDDLE", "_game_object_8h.html#a2bbef7231812d3025d485bf569292bf3a65bf65e48aa57a32efc7a9d20c4703a7", null ]
    ] ],
    [ "CapturePointState", "_game_object_8h.html#a1f69a7a6a24985de062d09298d0850eb", [
      [ "CAPTURE_STATE_NEUTRAL", "_game_object_8h.html#a1f69a7a6a24985de062d09298d0850eba162f24756b0362c2344b7f2c73e653d7", null ],
      [ "CAPTURE_STATE_PROGRESS_ALLIANCE", "_game_object_8h.html#a1f69a7a6a24985de062d09298d0850eba44a20a5d152fa9fd05b052dba182c0e8", null ],
      [ "CAPTURE_STATE_PROGRESS_HORDE", "_game_object_8h.html#a1f69a7a6a24985de062d09298d0850eba98aa72473ce7fc8c66c84f8027b2efc3", null ],
      [ "CAPTURE_STATE_CONTEST_ALLIANCE", "_game_object_8h.html#a1f69a7a6a24985de062d09298d0850eba7bdb29db5c39d896b87e4d02f23b8864", null ],
      [ "CAPTURE_STATE_CONTEST_HORDE", "_game_object_8h.html#a1f69a7a6a24985de062d09298d0850eba816f6b8bb2664ccbf9c9b6513069d7a0", null ],
      [ "CAPTURE_STATE_WIN_ALLIANCE", "_game_object_8h.html#a1f69a7a6a24985de062d09298d0850eba8a6ff43e9ce17d5f129bfbe68d87706f", null ],
      [ "CAPTURE_STATE_WIN_HORDE", "_game_object_8h.html#a1f69a7a6a24985de062d09298d0850eba9dab22d58bca37fcd976fcce42a78e98", null ]
    ] ],
    [ "GOState", "_game_object_8h.html#ac17cd142af80da019367bb796b15e5ab", [
      [ "GO_STATE_ACTIVE", "_game_object_8h.html#ac17cd142af80da019367bb796b15e5abaf4f299c1cfc830c90418c4487c567c1e", null ],
      [ "GO_STATE_READY", "_game_object_8h.html#ac17cd142af80da019367bb796b15e5aba8dd2dd7e5c1dda64ff6d0d06d623b650", null ],
      [ "GO_STATE_ACTIVE_ALTERNATIVE", "_game_object_8h.html#ac17cd142af80da019367bb796b15e5abaf8410e5f8e696adf15ecdd441dee7e0e", null ]
    ] ],
    [ "LootState", "_game_object_8h.html#a08733f20cea1f77cc9f1a549f8ce9f07", [
      [ "GO_NOT_READY", "_game_object_8h.html#a08733f20cea1f77cc9f1a549f8ce9f07aa7f58536a66a38d2bb42502424d69a79", null ],
      [ "GO_READY", "_game_object_8h.html#a08733f20cea1f77cc9f1a549f8ce9f07a8708de424ecaae41eb7271d399f1d5fc", null ],
      [ "GO_ACTIVATED", "_game_object_8h.html#a08733f20cea1f77cc9f1a549f8ce9f07a3e50ac5f6829b981fdff39a85a48959e", null ],
      [ "GO_JUST_DEACTIVATED", "_game_object_8h.html#a08733f20cea1f77cc9f1a549f8ce9f07af69d13b3580b3c24d26f66bcf0db43d5", null ]
    ] ]
];