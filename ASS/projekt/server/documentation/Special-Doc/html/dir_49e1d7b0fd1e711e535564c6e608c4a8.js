var dir_49e1d7b0fd1e711e535564c6e608c4a8 =
[
    [ "DpsHunterStrategy.cpp", "_dps_hunter_strategy_8cpp.html", [
      [ "DpsHunterStrategyActionNodeFactory", "class_dps_hunter_strategy_action_node_factory.html", "class_dps_hunter_strategy_action_node_factory" ]
    ] ],
    [ "DpsHunterStrategy.h", "_dps_hunter_strategy_8h.html", [
      [ "DpsHunterStrategy", "classai_1_1_dps_hunter_strategy.html", "classai_1_1_dps_hunter_strategy" ],
      [ "DpsAoeHunterStrategy", "classai_1_1_dps_aoe_hunter_strategy.html", "classai_1_1_dps_aoe_hunter_strategy" ],
      [ "DpsHunterDebuffStrategy", "classai_1_1_dps_hunter_debuff_strategy.html", "classai_1_1_dps_hunter_debuff_strategy" ]
    ] ],
    [ "GenericHunterNonCombatStrategy.cpp", "_generic_hunter_non_combat_strategy_8cpp.html", [
      [ "GenericHunterNonCombatStrategyActionNodeFactory", "class_generic_hunter_non_combat_strategy_action_node_factory.html", "class_generic_hunter_non_combat_strategy_action_node_factory" ]
    ] ],
    [ "GenericHunterNonCombatStrategy.h", "_generic_hunter_non_combat_strategy_8h.html", [
      [ "GenericHunterNonCombatStrategy", "classai_1_1_generic_hunter_non_combat_strategy.html", "classai_1_1_generic_hunter_non_combat_strategy" ]
    ] ],
    [ "GenericHunterStrategy.cpp", "_generic_hunter_strategy_8cpp.html", [
      [ "GenericHunterStrategyActionNodeFactory", "class_generic_hunter_strategy_action_node_factory.html", "class_generic_hunter_strategy_action_node_factory" ]
    ] ],
    [ "GenericHunterStrategy.h", "_generic_hunter_strategy_8h.html", [
      [ "GenericHunterStrategy", "classai_1_1_generic_hunter_strategy.html", "classai_1_1_generic_hunter_strategy" ]
    ] ],
    [ "HunterActions.cpp", "_hunter_actions_8cpp.html", null ],
    [ "HunterActions.h", "_hunter_actions_8h.html", "_hunter_actions_8h" ],
    [ "HunterAiObjectContext.cpp", "_hunter_ai_object_context_8cpp.html", [
      [ "StrategyFactoryInternal", "classai_1_1hunter_1_1_strategy_factory_internal.html", "classai_1_1hunter_1_1_strategy_factory_internal" ],
      [ "BuffStrategyFactoryInternal", "classai_1_1hunter_1_1_buff_strategy_factory_internal.html", "classai_1_1hunter_1_1_buff_strategy_factory_internal" ],
      [ "TriggerFactoryInternal", "classai_1_1hunter_1_1_trigger_factory_internal.html", "classai_1_1hunter_1_1_trigger_factory_internal" ],
      [ "AiObjectContextInternal", "classai_1_1hunter_1_1_ai_object_context_internal.html", "classai_1_1hunter_1_1_ai_object_context_internal" ]
    ] ],
    [ "HunterAiObjectContext.h", "_hunter_ai_object_context_8h.html", [
      [ "HunterAiObjectContext", "classai_1_1_hunter_ai_object_context.html", "classai_1_1_hunter_ai_object_context" ]
    ] ],
    [ "HunterBuffStrategies.cpp", "_hunter_buff_strategies_8cpp.html", null ],
    [ "HunterBuffStrategies.h", "_hunter_buff_strategies_8h.html", [
      [ "HunterBuffSpeedStrategy", "classai_1_1_hunter_buff_speed_strategy.html", "classai_1_1_hunter_buff_speed_strategy" ],
      [ "HunterBuffManaStrategy", "classai_1_1_hunter_buff_mana_strategy.html", "classai_1_1_hunter_buff_mana_strategy" ],
      [ "HunterBuffDpsStrategy", "classai_1_1_hunter_buff_dps_strategy.html", "classai_1_1_hunter_buff_dps_strategy" ],
      [ "HunterNatureResistanceStrategy", "classai_1_1_hunter_nature_resistance_strategy.html", "classai_1_1_hunter_nature_resistance_strategy" ]
    ] ],
    [ "HunterMultipliers.cpp", "_hunter_multipliers_8cpp.html", null ],
    [ "HunterMultipliers.h", "_hunter_multipliers_8h.html", null ],
    [ "HunterTriggers.cpp", "_hunter_triggers_8cpp.html", null ],
    [ "HunterTriggers.h", "_hunter_triggers_8h.html", [
      [ "HunterAspectOfTheHawkTrigger", "classai_1_1_hunter_aspect_of_the_hawk_trigger.html", "classai_1_1_hunter_aspect_of_the_hawk_trigger" ],
      [ "HunterAspectOfTheWildTrigger", "classai_1_1_hunter_aspect_of_the_wild_trigger.html", "classai_1_1_hunter_aspect_of_the_wild_trigger" ],
      [ "HunterAspectOfTheViperTrigger", "classai_1_1_hunter_aspect_of_the_viper_trigger.html", "classai_1_1_hunter_aspect_of_the_viper_trigger" ],
      [ "HunterAspectOfThePackTrigger", "classai_1_1_hunter_aspect_of_the_pack_trigger.html", "classai_1_1_hunter_aspect_of_the_pack_trigger" ],
      [ "BlackArrowTrigger", "classai_1_1_black_arrow_trigger.html", "classai_1_1_black_arrow_trigger" ],
      [ "HuntersMarkTrigger", "classai_1_1_hunters_mark_trigger.html", "classai_1_1_hunters_mark_trigger" ],
      [ "FreezingTrapTrigger", "classai_1_1_freezing_trap_trigger.html", "classai_1_1_freezing_trap_trigger" ],
      [ "RapidFireTrigger", "classai_1_1_rapid_fire_trigger.html", "classai_1_1_rapid_fire_trigger" ],
      [ "TrueshotAuraTrigger", "classai_1_1_trueshot_aura_trigger.html", "classai_1_1_trueshot_aura_trigger" ],
      [ "SerpentStingOnAttackerTrigger", "classai_1_1_serpent_sting_on_attacker_trigger.html", "classai_1_1_serpent_sting_on_attacker_trigger" ]
    ] ]
];