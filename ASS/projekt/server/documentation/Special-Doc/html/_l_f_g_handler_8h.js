var _l_f_g_handler_8h =
[
    [ "MeetingstoneFailedStatus", "_l_f_g_handler_8h.html#a240f9fecd416f635b080b12098a25bf9", [
      [ "MEETINGSTONE_FAIL_NONE", "_l_f_g_handler_8h.html#a240f9fecd416f635b080b12098a25bf9af55061162ee2c1b827bfd1e673f4dacc", null ],
      [ "MEETINGSTONE_FAIL_PARTYLEADER", "_l_f_g_handler_8h.html#a240f9fecd416f635b080b12098a25bf9aa79216d9b9a82c332511016595fbb549", null ],
      [ "MEETINGSTONE_FAIL_FULL_GROUP", "_l_f_g_handler_8h.html#a240f9fecd416f635b080b12098a25bf9a51699e54f0b2a553e6b012566fc6917a", null ],
      [ "MEETINGSTONE_FAIL_RAID_GROUP", "_l_f_g_handler_8h.html#a240f9fecd416f635b080b12098a25bf9a67bac26222512a39b606bb3df0e2d06a", null ]
    ] ],
    [ "MeetingstoneQueueStatus", "_l_f_g_handler_8h.html#a825724e8705d5c469c7bbea91270e093", [
      [ "MEETINGSTONE_STATUS_LEAVE_QUEUE", "_l_f_g_handler_8h.html#a825724e8705d5c469c7bbea91270e093a577e9c9ac6a84ec81570a560da9c2dbc", null ],
      [ "MEETINGSTONE_STATUS_JOINED_QUEUE", "_l_f_g_handler_8h.html#a825724e8705d5c469c7bbea91270e093aafd966cb2067ff310f5421862b64c7c6", null ],
      [ "MEETINGSTONE_STATUS_PARTY_MEMBER_LEFT_LFG", "_l_f_g_handler_8h.html#a825724e8705d5c469c7bbea91270e093a0f69c8d3941a23843f9c6e437c250282", null ],
      [ "MEETINGSTONE_STATUS_PARTY_MEMBER_REMOVED_PARTY_REMOVED", "_l_f_g_handler_8h.html#a825724e8705d5c469c7bbea91270e093a13c5c68eca24d6e546935498a6b8cdc4", null ],
      [ "MEETINGSTONE_STATUS_LOOKING_FOR_NEW_PARTY_IN_QUEUE", "_l_f_g_handler_8h.html#a825724e8705d5c469c7bbea91270e093ab69e07e7b28b115dfc83681988f7f222", null ],
      [ "MEETINGSTONE_STATUS_NONE", "_l_f_g_handler_8h.html#a825724e8705d5c469c7bbea91270e093a5228c8f4e120d87e7898cac5240354d1", null ]
    ] ]
];