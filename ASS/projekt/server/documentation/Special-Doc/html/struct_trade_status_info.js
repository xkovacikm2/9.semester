var struct_trade_status_info =
[
    [ "TradeStatusInfo", "struct_trade_status_info.html#a4a343bd89d5a871144a4959cbef87e42", null ],
    [ "IsTargetResult", "struct_trade_status_info.html#a10427d72e5bafd643648638a1fe7e854", null ],
    [ "ItemLimitCategoryId", "struct_trade_status_info.html#aa1e0003d046c213ddb1d4460c33b7b57", null ],
    [ "Result", "struct_trade_status_info.html#ac90552353a0fb8ff175fec99bf573981", null ],
    [ "Slot", "struct_trade_status_info.html#a3fcb84e08ecdfccaef6bc396c8c4eb2a", null ],
    [ "Status", "struct_trade_status_info.html#a265172d89cdc4c6fc4df20ad5df45c64", null ],
    [ "TraderGuid", "struct_trade_status_info.html#afc3926b9ff7dda3eb3505b225b74409d", null ]
];