var group__npc__linking =
[
    [ "CreatureLinkingMgr.cpp", "_creature_linking_mgr_8cpp.html", null ],
    [ "CreatureLinkingMgr.h", "_creature_linking_mgr_8h.html", null ],
    [ "CreatureLinkingInfo", "struct_creature_linking_info.html", [
      [ "linkingFlag", "struct_creature_linking_info.html#ae4ffc2fc7752c2742aca283b7130e701", null ],
      [ "mapId", "struct_creature_linking_info.html#ae7ab1ecdb5e71e48f7c0bc718fabf181", null ],
      [ "masterDBGuid", "struct_creature_linking_info.html#a235b44c975c958fee42a039ca18b15ce", null ],
      [ "masterId", "struct_creature_linking_info.html#a97ecd27d7562e671af6ecd08bb3de5e6", null ],
      [ "searchRange", "struct_creature_linking_info.html#a4d0695a4bd281092b7f73fa1f9e1e881", null ]
    ] ],
    [ "CreatureLinkingMgr", "class_creature_linking_mgr.html", [
      [ "CreatureLinkingMgr", "class_creature_linking_mgr.html#a4633792956a9887136b7dca46241069f", null ],
      [ "GetLinkedTriggerInformation", "group__npc__linking.html#ga7cb0f254d8c920bb3e252cf677ca79fc", null ],
      [ "GetLinkedTriggerInformation", "group__npc__linking.html#ga4bb9ace172e4c7d2f6a69e0781922fae", null ],
      [ "IsLinkedEventTrigger", "group__npc__linking.html#ga1bead7202530122a77e15c6d4e2761b3", null ],
      [ "IsLinkedMaster", "group__npc__linking.html#ga53abec816bb2e3ec2acc045d96603e44", null ],
      [ "IsSpawnedByLinkedMob", "group__npc__linking.html#gab2299e33cab49edca14c7c3a785ceb76", null ],
      [ "IsSpawnedByLinkedMob", "group__npc__linking.html#ga5fe947bdc3d31a49749913614705be33", null ],
      [ "LoadFromDB", "group__npc__linking.html#ga788201f3dbdde9ef6c7863c6b454e525", null ]
    ] ],
    [ "CreatureLinkingHolder", "class_creature_linking_holder.html", [
      [ "CreatureLinkingHolder", "class_creature_linking_holder.html#ab47434c6dd0ed1a1beeb3c6dd01dde9b", null ],
      [ "AddMasterToHolder", "group__npc__linking.html#gaf2e71ccc0b034bf18650f4e717080bcd", null ],
      [ "AddSlaveToHolder", "group__npc__linking.html#ga45b861cb9fe7bbe6b3ad0e69c922b583", null ],
      [ "CanSpawn", "group__npc__linking.html#ga406a6e53729376d8e6e8f62cf725a077", null ],
      [ "DoCreatureLinkingEvent", "group__npc__linking.html#gada413117cde80c71974670f2f42b523a", null ],
      [ "TryFollowMaster", "group__npc__linking.html#gaa7e9bb62200dbd4838a9411dfe5f0572", null ]
    ] ],
    [ "INVALID_MAP_ID", "group__npc__linking.html#gaff8d6e0668b3313e331f38e69f165c70", null ],
    [ "sCreatureLinkingMgr", "group__npc__linking.html#gac5d7abb28e265fd4e849bfe846b07530", null ],
    [ "CreatureLinkingEvent", "group__npc__linking.html#gae3b4a05c8337afd5382c98a2439590e6", [
      [ "LINKING_EVENT_AGGRO", "group__npc__linking.html#ggae3b4a05c8337afd5382c98a2439590e6a4730f701c7baa33681fb59c5d924bedf", null ],
      [ "LINKING_EVENT_EVADE", "group__npc__linking.html#ggae3b4a05c8337afd5382c98a2439590e6afe505a78427711c61051105f2a526d0c", null ],
      [ "LINKING_EVENT_DIE", "group__npc__linking.html#ggae3b4a05c8337afd5382c98a2439590e6a0bec8cc3ade4cbda6e195bafdde3fc74", null ],
      [ "LINKING_EVENT_RESPAWN", "group__npc__linking.html#ggae3b4a05c8337afd5382c98a2439590e6a92211e24ae76d9e91c4edd2e853b8759", null ],
      [ "LINKING_EVENT_DESPAWN", "group__npc__linking.html#ggae3b4a05c8337afd5382c98a2439590e6aca8a70a7c1b6bc92de445329ad7326eb", null ]
    ] ],
    [ "CreatureLinkingFlags", "group__npc__linking.html#ga85fdb5b3ef451e4df8cc021780fad4b5", [
      [ "FLAG_AGGRO_ON_AGGRO", "group__npc__linking.html#gga85fdb5b3ef451e4df8cc021780fad4b5ae1e664ef7418039124b5af9f6e4e2500", null ],
      [ "FLAG_TO_AGGRO_ON_AGGRO", "group__npc__linking.html#gga85fdb5b3ef451e4df8cc021780fad4b5ab616f5ebb69d3957ec4755932c248c14", null ],
      [ "FLAG_RESPAWN_ON_EVADE", "group__npc__linking.html#gga85fdb5b3ef451e4df8cc021780fad4b5a2155275aaa0fd98260717905f1cf5746", null ],
      [ "FLAG_TO_RESPAWN_ON_EVADE", "group__npc__linking.html#gga85fdb5b3ef451e4df8cc021780fad4b5aa01cb6260aedc246458b759aefda4488", null ],
      [ "FLAG_DESPAWN_ON_EVADE", "group__npc__linking.html#gga85fdb5b3ef451e4df8cc021780fad4b5abdd1918830ad64cc4d4bfc5e23711841", null ],
      [ "FLAG_DESPAWN_ON_DEATH", "group__npc__linking.html#gga85fdb5b3ef451e4df8cc021780fad4b5a127fe34b06a61de447481dc8ba311b9c", null ],
      [ "FLAG_SELFKILL_ON_DEATH", "group__npc__linking.html#gga85fdb5b3ef451e4df8cc021780fad4b5a7bfffd268fcef3f9226c392b4c62f483", null ],
      [ "FLAG_RESPAWN_ON_DEATH", "group__npc__linking.html#gga85fdb5b3ef451e4df8cc021780fad4b5a0352d5fff43ee36fedf55bd43ff50716", null ],
      [ "FLAG_RESPAWN_ON_RESPAWN", "group__npc__linking.html#gga85fdb5b3ef451e4df8cc021780fad4b5a255ff64596ccc8c06abfc7ebe3438945", null ],
      [ "FLAG_DESPAWN_ON_RESPAWN", "group__npc__linking.html#gga85fdb5b3ef451e4df8cc021780fad4b5aede0368505c770ce171e35be2f5aa8d6", null ],
      [ "FLAG_FOLLOW", "group__npc__linking.html#gga85fdb5b3ef451e4df8cc021780fad4b5ac2da92a8766c35abc80307c84098db41", null ],
      [ "FLAG_DESPAWN_ON_DESPAWN", "group__npc__linking.html#gga85fdb5b3ef451e4df8cc021780fad4b5a56eb667fab17dc3a67b82dfb22f341cf", null ],
      [ "FLAG_CANT_SPAWN_IF_BOSS_DEAD", "group__npc__linking.html#gga85fdb5b3ef451e4df8cc021780fad4b5a11adfe7e9ef4a1a9a48cdd3b8cd4a640", null ],
      [ "FLAG_CANT_SPAWN_IF_BOSS_ALIVE", "group__npc__linking.html#gga85fdb5b3ef451e4df8cc021780fad4b5a30f08121037ca7ba4361a614109aa3d3", null ],
      [ "LINKING_FLAG_INVALID", "group__npc__linking.html#gga85fdb5b3ef451e4df8cc021780fad4b5a3c65f8a2d308d3e1791dac9690f99d8a", null ]
    ] ],
    [ "EventMask", "group__npc__linking.html#gad89c5d6c463a04476707258d0c1eb591", [
      [ "EVENT_MASK_ON_AGGRO", "group__npc__linking.html#ggad89c5d6c463a04476707258d0c1eb591a6bb39165455de2884fbee10650b0e7b4", null ],
      [ "EVENT_MASK_ON_EVADE", "group__npc__linking.html#ggad89c5d6c463a04476707258d0c1eb591a251b6defe6c50b92fd32fe1e15b1d7bd", null ],
      [ "EVENT_MASK_ON_DIE", "group__npc__linking.html#ggad89c5d6c463a04476707258d0c1eb591a103877f94fa7f4945af0af1e3194a8a5", null ],
      [ "EVENT_MASK_ON_RESPAWN", "group__npc__linking.html#ggad89c5d6c463a04476707258d0c1eb591a19b40cb557c20bdedcb567c2e3e46360", null ],
      [ "EVENT_MASK_TRIGGER_TO", "group__npc__linking.html#ggad89c5d6c463a04476707258d0c1eb591a26ca5ec1fed7f18f0cdf519cb31d508f", null ],
      [ "EVENT_MASK_ON_DESPAWN", "group__npc__linking.html#ggad89c5d6c463a04476707258d0c1eb591ae7d460f986d82416fa1995d0d116ee75", null ]
    ] ],
    [ "AddMasterToHolder", "group__npc__linking.html#gaf2e71ccc0b034bf18650f4e717080bcd", null ],
    [ "AddSlaveToHolder", "group__npc__linking.html#ga45b861cb9fe7bbe6b3ad0e69c922b583", null ],
    [ "CanSpawn", "group__npc__linking.html#ga406a6e53729376d8e6e8f62cf725a077", null ],
    [ "DoCreatureLinkingEvent", "group__npc__linking.html#gada413117cde80c71974670f2f42b523a", null ],
    [ "GetLinkedTriggerInformation", "group__npc__linking.html#ga7cb0f254d8c920bb3e252cf677ca79fc", null ],
    [ "GetLinkedTriggerInformation", "group__npc__linking.html#ga4bb9ace172e4c7d2f6a69e0781922fae", null ],
    [ "INSTANTIATE_SINGLETON_1", "group__npc__linking.html#ga9f3c2a3c27fcbcca9d4c72225cd917f5", null ],
    [ "IsLinkedEventTrigger", "group__npc__linking.html#ga1bead7202530122a77e15c6d4e2761b3", null ],
    [ "IsLinkedMaster", "group__npc__linking.html#ga53abec816bb2e3ec2acc045d96603e44", null ],
    [ "IsSpawnedByLinkedMob", "group__npc__linking.html#gab2299e33cab49edca14c7c3a785ceb76", null ],
    [ "IsSpawnedByLinkedMob", "group__npc__linking.html#ga5fe947bdc3d31a49749913614705be33", null ],
    [ "LoadFromDB", "group__npc__linking.html#ga788201f3dbdde9ef6c7863c6b454e525", null ],
    [ "TryFollowMaster", "group__npc__linking.html#gaa7e9bb62200dbd4838a9411dfe5f0572", null ]
];