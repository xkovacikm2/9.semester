var dir_6507d6625ccbf45a3c5bce07b4888c2b =
[
    [ "CreationPolicy.h", "_creation_policy_8h.html", [
      [ "OperatorNew", "class_ma_n_g_o_s_1_1_operator_new.html", null ],
      [ "LocalStaticCreation", "class_ma_n_g_o_s_1_1_local_static_creation.html", null ],
      [ "CreateUsingMalloc", "class_ma_n_g_o_s_1_1_create_using_malloc.html", null ],
      [ "CreateOnCallBack", "class_ma_n_g_o_s_1_1_create_on_call_back.html", null ]
    ] ],
    [ "ObjectLifeTime.cpp", "_object_life_time_8cpp.html", "_object_life_time_8cpp" ],
    [ "ObjectLifeTime.h", "_object_life_time_8h.html", "_object_life_time_8h" ],
    [ "Singleton.h", "_singleton_8h.html", "_singleton_8h" ],
    [ "ThreadingModel.h", "_threading_model_8h.html", "_threading_model_8h" ]
];