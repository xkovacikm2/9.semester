var _chat_8h =
[
    [ "ChatCommand", "class_chat_command.html", "class_chat_command" ],
    [ "ChatHandler", "class_chat_handler.html", "class_chat_handler" ],
    [ "DeletedInfo", "struct_chat_handler_1_1_deleted_info.html", "struct_chat_handler_1_1_deleted_info" ],
    [ "CliHandler", "class_cli_handler.html", "class_cli_handler" ],
    [ "ChatTagFlags", "_chat_8h.html#ae151821a0d5020a2f944a5e6711bad62", null ],
    [ "ChatCommandSearchResult", "_chat_8h.html#a8e9ef28674ee96e3aef49e7940bdc874", [
      [ "CHAT_COMMAND_OK", "_chat_8h.html#a8e9ef28674ee96e3aef49e7940bdc874a30405867b29d57b81c578d1555a63bed", null ],
      [ "CHAT_COMMAND_UNKNOWN", "_chat_8h.html#a8e9ef28674ee96e3aef49e7940bdc874a2ecfd5580bcbfa21ddb09756423791c3", null ],
      [ "CHAT_COMMAND_UNKNOWN_SUBCOMMAND", "_chat_8h.html#a8e9ef28674ee96e3aef49e7940bdc874aa4d51347c750f739610d6d3c21687f6d", null ]
    ] ],
    [ "PlayerChatTag", "_chat_8h.html#ae1f5dfd68177d7823343e24d6f44ad53", [
      [ "CHAT_TAG_NONE", "_chat_8h.html#ae1f5dfd68177d7823343e24d6f44ad53afe80129742a819c84d6da0a747ba1b36", null ],
      [ "CHAT_TAG_AFK", "_chat_8h.html#ae1f5dfd68177d7823343e24d6f44ad53ac8b97a1b06be376b49dd64694c3663f7", null ],
      [ "CHAT_TAG_DND", "_chat_8h.html#ae1f5dfd68177d7823343e24d6f44ad53ab4f87786eeaf27915252193213b51e5c", null ],
      [ "CHAT_TAG_GM", "_chat_8h.html#ae1f5dfd68177d7823343e24d6f44ad53ad5474de19aaafacb575a7d6fbd298378", null ]
    ] ]
];