# From: http://www.cmake.org/Wiki/CMake_FAQ
#

IF(NOT EXISTS "/home/kovko/Documents/prace/STU/9.semester/ASS/projekt/server/cmake-build-debug/install_manifest.txt")
  MESSAGE(FATAL_ERROR "Cannot find install manifest: \"/home/kovko/Documents/prace/STU/9.semester/ASS/projekt/server/cmake-build-debug/install_manifest.txt\"")
ENDIF(NOT EXISTS "/home/kovko/Documents/prace/STU/9.semester/ASS/projekt/server/cmake-build-debug/install_manifest.txt")

FILE(READ "/home/kovko/Documents/prace/STU/9.semester/ASS/projekt/server/cmake-build-debug/install_manifest.txt" files)
STRING(REGEX REPLACE "\n" ";" files "${files}")
FOREACH(file ${files})
  MESSAGE(STATUS "Uninstalling \"${file}\"")
  IF(EXISTS "${file}")
    EXEC_PROGRAM(
      "/home/kovko/utility/clion-2017.2.2/bin/cmake/bin/cmake" ARGS "-E remove \"${file}\""
      OUTPUT_VARIABLE rm_out
      RETURN_VALUE rm_retval
      )
    IF("${rm_retval}" STREQUAL 0)
    ELSE("${rm_retval}" STREQUAL 0)
      MESSAGE(FATAL_ERROR "Problem when removing \"${file}\"")
    ENDIF("${rm_retval}" STREQUAL 0)
  ELSE(EXISTS "${file}")
    MESSAGE(STATUS "File \"${file}\" does not exist.")
  ENDIF(EXISTS "${file}")
ENDFOREACH(file)
