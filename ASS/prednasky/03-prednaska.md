# Architektonicke styly
## Interpreter
![intepreter](diagrams/interpreter.png)
- **interpretacny engine**
- **program**
- **stav**
- **vstup**

### Interpreter, na pravidlach zalozeny system
![intepreter](diagrams/interpreter_odvodzovanie.png)
- **interpreter pravidiel**
- **znalostna baza**
- **pamat**
- **pravidla a vyber prvok**

## Tracny model kompilator
$text\rightarrow Lex\rightarrow Syn\rightarrow Sem\rightarrow Opt\rightarrow Code$

## Moderny kanonicky kompilator
![intepreter](diagrams/kompilator.png)

## Centralizovanie riadenia
riadiaci podsystem zodpoveda za manazovanie spustania ostatnych 
podsystemov

### Riadenie Volanie-Navrat

## Systemy ovladane udalostami
### Modely s vysielanim
### Modely s ovladanim preruseniami
existuju typy preruseni, ktore maju urceny typ ovladaca preruseni (HW).

# Distribuovane systemy
- **Charakteristiky**
  - Zdielanie prostriedkov
    - zdielanie hardverovych a softverovych prostriedkov
  - otvorenost
    - pouzivanie nastrojov od inych predajcov
  - subeznost
    - paralelne spracovanie pre zvysenie vykonu
  - skalovatelnost
  - tolerovanie chyb
- **Nevyhody**
  - zlozitost
  - bezpecnost
  - manazovatelnost
  - nepredvidalnest
