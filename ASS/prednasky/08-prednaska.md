## Page controller
zo stranky pouzivatelskeho rozhrania vyvolam funkcie kontrollera, vzdy jeden pre UI

## Front controller
transformuje pouzivatelske poziadavky do specifickych requestov/metod. UI ma ine rozhranie ako controller

## Aplication controller
pouzivatel interaguje s UI -> aplikacny riadiaci element -> vykonaj prikaz -> ziada sa o sluzbu -> navrat do aplikacneho riadiaceho elementu ->
vytvorenie pohladu -> zobrazenie pohladu s vysledkami

## Command processor
- Poziadavky od roznych klientov
- Handluje planovanie, logovanie, undo/redo
- Client -> vytvori service request -> pride do command processora -> ten ho odosle do componentu na spracovanie
- umoznuje asynchronne spracovanie

## Template view
- preddefinuva strukturu pohladu s placeholdermi pre dynamicke aplikacne data
- nalievanie dat do sablony

## Transform view
- Transformuje data do specifickeho formatu
- UI chce nieco zobrazit a sluzba vytvori celu stranku.

## Firewall Proxy
- Client -> Firewall proxy dostane request, ktory osetri podla access rule base -> dispatche request na component

## Authorizacia
Client -> kontrola access rigts pre volanu metodu -> komponent

## Observer
subject poskytuje funkcie register a unregister, na ktore sa prihlasuju observery -> pride zmena stavu, tak vola notify -> ten pre kazdy observer vola update

## Mediator
klient vola od mediatora servisu, ktory rozdeluje ulohy medzi komponenty (podobne ako master, slave, ale nie je paralelne)

## Command
klient -> dynamicky sa vytvori command, ktory moze byt z dispatchu, alebo strategy -> potom sa vyvola execute, ktory uz vie aky komponent ma volat

## Memento
Klient si requestne snapshot stavu od originatora, ktory sa v case meni. Nasledne si stav mozem ukladat v objekte memento a z neho ho nacitat

## Data transfer object
podobne ako memento, ale nerequestujem si stav, ale data, ktore sa zabaluju do transfer objectu

## Context object
Kontextovy objekt vytvara klient a musi dodat context do objektu, ktoru si sluzba vytahuje. Podobne ako DTO, ale pre vstupne data

## Message
posielanie dat medzi komponentami, pricom data sa enkapsuluju do messageov, ktore maju hlavicky a telo

## Bridge
- Rozne implementacie (plaformovo specificke, runtime rozhodovanie)
- obsahuje paralelne hierarchie dedicnosti

## Object adapter
- podobne ako proxy, ale ponuka ine rozhranie a vyvolava povodnu metodu v riadnom zneni
- vyrovnava napriklad volania na rozne verzie API

## Chain of responsibility
- klient -> vola metodu -> vola inu metodu -> vola este inu metodu (delegovanie)
- podoba sa pipes and filter -> retaz objektov si posiela ulohu do ktorej mozu este doplnat vlastne riesenia