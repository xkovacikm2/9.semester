(TeX-add-style-hook
 "09-prednaska"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref")
   (LaTeX-add-labels
    "sec:orgbd2a833"
    "sec:orgb5bfdbc"
    "sec:orgff18f43"
    "sec:org4855c72"
    "sec:org33902d4"
    "sec:orge0c252d"
    "sec:org47e4717"
    "sec:orga676a27"
    "sec:orgd81d862"
    "sec:org2fd043f"
    "sec:org4620ced"
    "sec:org09e23c9"
    "sec:orgf49d124"
    "sec:orgfd761d2"
    "sec:org04f5ed9"
    "sec:org1acd2ab"
    "sec:orgc80bee2"
    "sec:org4b4e244"
    "sec:orgca6bda4"
    "sec:org4734c3c"
    "sec:orge1126b6"
    "sec:orgbc9eb66"
    "sec:org818a9d9"))
 :latex)

