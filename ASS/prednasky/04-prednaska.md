## Aspektovo orientovany vyvoj
- oddelenie koncernov(zalezitosti), aby neboli prepletene medzi modulmi (bezpecnost, logovanie)
- body spajania su urcene _pointcuts_ - bodovymi prierezmi
  - before, around, after
- do pointcutov mozeme vkladat _advices_

```Java
public aspect AccessMonitoring {
  pointcut monitoringSetPointcut(){
    execution(Point.set*(...));
  }

  before():monitoringSetPointcut(){
    log.('setting point');
  }
}
```

## Architektonicke vzory

### Model-View-Kontroler
- ramec pre systemovu infrastrukturu pre GUI aplikacii
- viacero prezentacii objektu a separatne interakcie
- zahrna instancovanie roznych vzorov

### Product instance development
- napriklad SAP, vyberiem si iba co potrebujem a z roznych kociek si vyskladam
produkt

### Distribuovana objektova architektura
CORBA - medzinarodny standard pre Object Request Broker

#### Corba common facilities
zabezpecuju vseobecne veci ako interface, manazment informacii, systemovy manazment 
a manazment uloh

#### Corba services
- object life cycle
- naming
- events
- relationships
- externalization
- transactions
- concurrency control
- property

Corba objekty su porovnatelne s objektami v C++, alebo Jave

Pouzitim ORB, volajuci objekt naviaze IDL stub, ktory definuje rozhranie volaneho 
objektu, volanie vyusti do volani ORB, ktore neskor volaju potrebny objekt, cez IDL 
skeleton

