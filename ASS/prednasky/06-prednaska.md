# POSA
- Bushmanova notacia
- prezentacna vrstva
- business vrstva
- vrstva perzistencie

## Model-View-Controller
- view moze byt akykolvek (cela funkcionalita vizualizacie dat), teda HTC Vice, monitor, etc.
- controller moze byt zasa cokolvek (varechy, joisticky, kinect) 
  - volanimi funkcii modelu meni jeho stav
- model obsahuje data modifikovane controllerom
  - notifikuje view o zmenach modelu (observer)

## Microkernel
- su v nom spolocne funkcie pre vsetky moduly a elementy
- pripomina vrstvy (najnizsiu vrstvu)
- k nemu mozeme pridat supervrstvu (plug-in komponenty), obsahujucu specialnejsie funkcie
- vytvaranie novych verzii extendovanim spolocneho minimalneho jadra cez plug-and-play infrastrukturu
- mikrokernel potom routuje requesty do neho pripojenych pluginov

## Reflexia
- oddelenie metaobjektov od aplikacnej logiky

## Messaging
- komponenty navzajom potrebuju komunikovat
- komunikuju cez zbernicu (message bus)
- sluzby asynchronne posielaju a prijimaju spravy

### Message translator
- kovertuje spracuj z jedneho formatu na iny

### Message router
- rozhoduje do ktoreho kanala ma poslat spravu

## Publisher - Subscriber
- elementy publikuju svoju funkcionalitu a su subscriberi, ktori chcu data prijimat
- bezi to cez messaging infrastrukturu

## Broker
1. klient pozaduje metodu od client-proxy (tvari sa, ze ponuka metody)
1. cez client broker proxyna vyziada implementovanu metodu
1. server-side broker vyvola na aplikacnom komponente vracia cez server-brokera odpoved

## Client Proxy
- rychlejsi ako broker, ale neobsahuje samotny broker, teda to nie je rake robusne

# Distribuovana infrastruktura

## Requestor
zaobaluje vytvaranie a zasielanie poziadavkovych sprav na vzdialene komponenty

## Invoker
Zaobaluje prijimanie a preposielanie poziadavkocych sprav od vzdialenych klientov

## Client Request Handler
Nielen vytvara a zasiela spravy, ale aj prijima odpovede od servera a preposiela ich klientovi

## Server Request Handler
Nielen prijima a preposiela spravy, ale aj zasiela odpovede

# Event demultiplexing and dispatching

## Reactor
- demultiplexuje a prepaja niekolko simultannych udalosti
1. nastartovanie reaktora (event_loop)
1. demux_events v event_loope - caka na udalost
1. pre prijaty event sa identifikuje handler (zo zoznamu)
1. zavola sa identifikovany handler pre udalost

## Proaktor
- maximalizuje event driven performance 
1. event loop nastartuje proaktor
1. demuxuje eventy od OS
1. pouziva asynchronny zapis a citanie

##Acceptor-Connector
- peer-to-peer spojenie
- vyziadam si od konektora spojenie s acceptorom (connect) 
- akceptor aj konektor passnu spojenie do service handlera (init)
- spustenie samotnej prace

## Asynchronous completion token
- klient posle poziadavkud aj spolu s tokenom na sluzvu
- operacia sa asynchronne spracuje (naplni token)
- vysledok sa dispatchne cez dispatcher
- a potom sa spracuje