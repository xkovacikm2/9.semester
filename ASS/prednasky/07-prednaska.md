# Navrhove vzory rozhrani

## Explicit interface
Klient vola interface, ten definuje len vstupy a vystupy a ktory polymorfnym dispatchom vola implementaciu v komponente niekde za tym.

## Extension interface
Jeden interface pre kazdu rolu.
- rozhrania su oddelene do viacero rozhrani a tie su potom implementovane v jednej komponente

## Introspective interface
Oddelenie operacnych rozhrani komponentu do meta-rozhrania. Sme nezavisli od komponentu aj od jeho rozhrani. Volania sa robia s pouzitim reflexie.

## Dynamic invocation interface
- umoznuje klientovi skladat volania v komponente dynamicky
- metody sa identifikuju za behu podla retazov
- argumenty sa posuvaju ako genericky typovane kolekcie

## Proxy
client -> proxy, ktory ponuka metody -> vykonanie preprocessingu v proxyne (vytiahne komponent z DB) -> vykonanie impolementacie metody v komponente -> postprocessing v proxy

## Business Delegate
Pre kazdy remote komponent, ktory moze byt vytvoreny, pouzity a upratany ako koalokovany komponent s identickym rozhranim
Klient vyvola metodu na delegatovi -> delegat lokalizuje komponent -> vyvola na nom metodu -> osetrenie chyb komponentu delegatom -> navrat klientovi

## Facade
Jednotny pristup k celemu zvazku komponentov/tried (napr. kniznice).
- jednotne rozhranie

## Combined method
- rozhranie s metodami
- kombinovana metoda, ktora vyuziva metody z rozhrania v sekvencii

## Iterator
- Umoznuje pracovat uplne oddelene s datami a s iteratormi.
- poskytuje abstraktne rozhranie pre iteraciu a pre zoskupenia
- klient pracuje s abstraktnymi rozhraniami zoskupeni a iteratora
- abstraktne rozhrania dispatchuju na konkretne implementacie

## Enumeration method
- agreguje komponenty ako su grafy, alebo stromy
- lepsie ako iterator
- agregat vyzaduje pred a za iteracny kod
- synchronizacia threadov v distribuovanych systemov
- klient posle akciu s metodou execute do enumeratora a tam sa vykona s kazdym prvkom

## Batch method
Vezmi vsetky argumenty pre kazde vykonanie akcie do komponentu s akciou.
- cez put poslem z klienta kolekciu na komponent
- cez get vyberiem z komponentu celu spracovanu kolekciu
- rozdiel je:
  - v enumeratione posielam akciu, ktora ma vykonat nad kolekciou
  - v batchi posielam akcii kolekciu prvkov

# Navehove vzory partionovania komponentov

## Encapsulated implementation
- implementacne detaily ostavaju skryte za rozhranim.
- zhodne s explicitnym rozhranim

## Whole part interface
- existuje navonok ako celok posobiaci monolit vo vnutri sa skladajuci z casti
- vyzera ako fasade

## Composite
- Klient vola metody z rozhrania komponentov, ktora moze mat kompozitny objekt, alebo listovym objektom.
- implementacia stromu, kde kazdy child kompozit elementu tvori dalsi kompletny strom
- rekurzivne invokuje metodu az kym nespadne do listov

## Master-Slave
Divide et impera
- nezavisle paralelne podulohy
- klient vola sluzbu na masterovi
- funkcia moze napriklad rozdelit data na n-casti a posle ich na vypocet slave-om
- vysledky spoji a vrati

## Half-Object plus protocol
- minimalizuje prenosy dat v sieti replikaciou objektov a rozdelenim metod: nech zostanu len lokalne potrebne,
aj s lokalnymi datami, zvysok, cez siet
- vzor pre distribuovane pocitanie
- na jednej strane siete su implementovane len iste casti metod objektu a na druhej strane ine, vzdy len lokalne potrebne
- pokial potrebuje na jednej strane siete nejaku sluzbu, ktora nie je lokalne implementovana tak ju musi vyvolat cez sietovy protokol

## Replicated component group
Klient vola sluzbu na rozhrani komponentu, ktore su implementovane na roznych miestach v sieti a caka sa, ktory odpovie prvy
