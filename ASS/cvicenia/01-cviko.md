## O cviceniach
- projekt z githubu nejaky opensource
- 1 styl a 9 vzorov pre dvojicu 13 vzorov pre trojicu
- prezentacia s diagramami identifikovanych stylov/vzorov
- vybrat si projekt v nejakom normalnom jazyku
- nemusi byt nutne objektovy
- odhadovane vzory a styly

### prve odovzdanie
- zapisat sa do tabulky, ze kto a aky projekt bude riesit
- odporucanie na zvolenie projektu
  - nieco dostatocne velke
- pred druhym cvikom sa odovzdava, ktory projekt sme si zvolili, nejake metriky (pocet riadkov kodu, tried, etc.)
  - output je pdfko
  - jedna stranka s popisom odkial je ten projekt, co je za to projekt

### druhe odovzdanie
- pred tretim cvikom sa nieco odovzdava, este nevieme co

### dalsie cvicenia
- bude sa skusat, co bolo na prednaske, treba chodit pripraveny

### stvrte cviko
- na prednaske pisomka, cviko teda nebude

# Diskusia

### Style
cely system sa mu podriaduje. Napriklad, pri zmene databazy staci vymenit iba driver (v railse).
- _klient-server_
- _peer-to-peer_

### Pattern
riesi iba jeden konkretny problem
- *_batch_* davkove spracovanie dat - architektonicky pattern
- *_invoker_*, ktory vyvola nejaky job

### Dizajnove vs. architektonicke patterny
Dizajnovy pattern neriesi velke problemy,
ale napriklad len komunikaciu medzi lokalnymi triedami,
zatialco architektonicke riesia problemy na inej urovni.

Aj ked maju spolocne nazvy, mozu riesit uplne ine urovne problemov. 
