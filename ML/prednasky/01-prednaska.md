# Prednaska 1
slajdy nebudu

## Hodnotenie
- 30% projekt
- 30% domace ulohy (3x)
- 40% skuska (bude sa pytat dopnujuce otazky k projektu)
- bonusove body (5% na cvicenie)
- aktivita, poznamky

## Projekt
Vybrat si aplikaciu strojoveho ucenia (termin v strede semestra)
- Implementacia
- Evaluacia (pokial mozno na skutocnych datach)
- Zhrnutie v style konferencneho clanku 5-15 stran (implementaciu a
 zhrnutie odovzdat najneskor tyzden pred skuskou)

## Triedenie posty
- **PSC**
  - system na rozpoznavanie cisel postoveho smeroveho cisla
  - nejaka heuristika sa snazi zredukovat a precitat jednolive cisla
    - *ziskavanie atributov* matica 32x32 {0,1}
    - **_int h(matica[32][32])_** -> z matice urci ako je to cislo
    - mnzina hypotez H -> trenovaci algoritmus(velmi velka mnozina),
     z nich _h_ vyberie tu najblizsiu.

## Slovnicek

#### Strojove ucenie s ucitelom
Supervised learning
  - rozpoznavanie obrazkov
  - samojazdiace auta
  - akciovy trh
  - je nador zhubny?

#### Strojove ucenie bez ucitela

Unsupervised learning
  - cielom nie je odpovedat na urcitu konkretnu otazku
  - detekcia outlayerov (namerane data z N leteckych motorov a snazim
  sa zistit potencialne vadne)
  - redukcia dimenzionality (geneticke data ako 1000 rozmerne vektory 
  si viem zobrazit v 2D priestore)

#### Strojove ucenie s posilnovanim

S odmenou a trestom (Reinforced learning)
  - algorimus sa postupne modifikuje a spatnu vazbu rozdistribuujem 
  medzi akcie, ktore k udalosti viedli

# Linearna regresia
- predpovedame cenu bytu v zavislosti od podlahovej plochy

$$ x \epsilon R -> y \epsilon R^+ $$

x - podlahova plocha

y - cena bytu

### Trenovacie data
$$ (x_1, y_1), (x_2, y_2) ... (x_n, y_n) $$

### Mnozina hypotez
$$ H=\{h: h_{\theta_0 + \theta_1}(x)\} $$

$$ h_{\theta_0\theta_1}(x) = \Theta_0 + \Theta_1x $$

Potrebujem chybovu funkciu, ktora ohodnoti vystup

$$ err(h_{\theta_0\theta_1}(x),y) = (\Theta_0 + \Theta_1x - y)^2 $$

najdi funkciu h z H

$$ J = \sum^{t}_{i=1} err(h^*(x_i), y_i) $$

potrebujem teda najst parametre: 

$$ \theta_0 \theta_1 $$ 

take, aby derivacia bola nulova, teda

$$ \partial(\Theta_0\Theta_1) $$

ked spravim parcialne derivacie

$$ \Theta_0 = \frac{\partial J(\Theta_0,\Theta_1)}{\partial\Theta_0} $$

<br/>

$$ \Theta_1 = \frac{\partial J(\Theta_0,\Theta_1)}{\partial\Theta_1} $$

tak dostanem

$$ \Theta_0 = \frac{\sum u_i \sum x_i - \sum x_i \sum x_i y_i}{t \sum x^2 - (\sum x_i)^2} $$

<br/>

$$ \Theta_1 = \frac{t\sum(x_i y_i) - \sum(x_iy_i)}{t\sum x_i^2 - (\sum x_i)^2} $$
