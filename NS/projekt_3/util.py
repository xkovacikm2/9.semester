import atexit
import numpy as np
import matplotlib
matplotlib.use('TkAgg') # todo: remove or change if not working
import matplotlib.pyplot as plt
import os
import time


## utility



## plotting

def plot_state(s, index=None, width=None, height=None, block=True):
    plt.figure(1).canvas.mpl_connect('key_press_event', keypress)
    plt.clf()

    plt.imshow(s.reshape((height, width)), cmap='gray', interpolation='nearest', vmin=-1, vmax=+1)
    plt.xticks([])
    plt.yticks([])

    if index:
        plt.scatter(index%width, index//width, s=150)

    plt.tight_layout()
    plt.gcf().canvas.set_window_title('State')
    plt.show(block=block)


def plot_states(S, width=None, height=None, hits='', block=False):
    plt.figure(2).canvas.mpl_connect('key_press_event', keypress)
    plt.clf()

    for i, s in enumerate(S):
        plt.subplot(1, len(S), i+1)
        plt.imshow(s.reshape((height, width)), cmap='gray', interpolation='nearest', vmin=-1, vmax=+1)
        plt.title('Hits: ' + str(hits))
        plt.xticks([])
        plt.yticks([])

    plt.tight_layout()
    plt.gcf().canvas.set_window_title('States')
    plt.gcf().savefig('plots/states_' + str(hits) + '.png')


## interactive drawing, very fragile....

wait = 0.005

def clear():
    plt.clf()


def ion():
    plt.ion()
    time.sleep(wait)


def ioff():
    plt.ioff()


def redraw():
    plt.gcf().canvas.draw()
    plt.waitforbuttonpress(timeout=0.001)
    time.sleep(wait)

def keypress(e):
    if e.key in {'q', 'escape'}:
        os._exit(0) # unclean exit, but exit() or sys.exit() won't work


## non-blocking figures still block at end

def finish():
    plt.show(block=True) # block until all figures are closed

def calculate_similarity(state, pattern):
  diff = np.equal(state, pattern.flatten())
  total = pattern.shape[0] * pattern.shape[1]
  return sum(diff)/total

def plot_similarity(S, patterns, iteration, noise, block=False):
  plt.figure()
  plt.clf()

  hs = []
  for idx, pattern in enumerate(patterns):
    similarities = []

    for state in S:
      similarities.append(calculate_similarity(state, pattern))

    h, = plt.plot(similarities, label='Pattern ' + str(idx))
    hs.append(h)

  plt.legend(handles=hs)
  plt.gcf().savefig('plots/similarity_' + str(iteration) + '_' + str(noise)+ '.png')

  # plt.show(block=block)

def plot_error(E, iteration, noise, block=False):
  plt.figure()
  plt.clf()

  h1, = plt.plot(E, label='Energy decay')
  plt.legend(handles=[h1])

  plt.gcf().savefig('plots/energy_' + str(iteration) + '_' + str(noise)+ '.png')

atexit.register(finish)
