import numpy as np

from util import *


class Hopfield():
  max_steps = 10

  def __init__(self, dim):
    self.dim = dim

  def train(self, patterns):
    self.W = np.zeros((self.dim, self.dim))

    indices = len(patterns)

    for p in range(0, indices):
      for i, x_i in enumerate(patterns[p].flatten()):
        for j, x_j in enumerate(patterns[p].flatten()):
          self.W[i,j] += (x_i * x_j) if j != i else 0
    self.W /= indices

  def energy(self, state):
    e = self.W @ state @ state.T
    e /= -2
    return e

  def signum(self, x):
    return 1 if x >= 0 else -1

  def run_sync(self, x):
    s = x.flatten()
    e = self.energy(s)
    S = [s,s]
    E = [e,e]
    signum = np.vectorize(self.signum)

    for _ in range(Hopfield.max_steps):
      s = signum(self.W @ s)
      e = self.energy(s)

      if np.all(s == S[-1]):
        break  # fixed point

      S.append(s)
      E.append(e)

      if len(S) >= 3 and np.all(s == S[-3]):
        break  # length-2 cycle

    return S, E

  def run_async(self, x):
    height, width = x.shape
    s = x.flatten()
    signum = np.vectorize(self.signum)

    ion()

    while(True):
      for i in np.random.permutation(self.dim):
        s[i] = signum(sum([self.W[i,j]*s[j] for j in range(0, self.dim)]))

        plot_state(s, index=i, width=width, height=height, block=False)
        redraw()
