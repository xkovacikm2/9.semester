import numpy as np

from hopfield import Hopfield
from util import *

## load data

dataset = 'patterns'

patterns = []

# with open(dataset + '.dat') as f:
#   count, width, height = [int(x) for x in f.readline().split()]
#
#   for _ in range(count):
#     f.readline()
#     x = np.empty((height, width))
#     for r in range(height):
#       x[r, :] = np.array(list(f.readline().strip())) == '#'
#
#     patterns.append(2 * x - 1)
#
# ## select patterns
#
# patterns = patterns[:]
# count = len(patterns)
# dim = width * height
#
# ## train and run model
#
# model = Hopfield(dim)
# model.train(patterns)

## task 1
# indices = [(i,j) for i in range(0, height) for j in range(0, width)]
#
# for idx, pattern in enumerate(patterns):
#   for k in [0,7,14,21]:
#     noiced = pattern.copy()
#     np.random.shuffle(indices)
#
#     for index in indices[0:k]:
#       noiced[index] *= -1
#
#     S, E = model.run_sync(noiced)
#     plot_similarity(S, patterns, idx, k)
#     plot_error(E, idx, k)
#
#     plot_states(S, width=width, height=height)

## task 2
# for idx in range(0,5):
#   noiced = ((np.random.rand(width, height) < 0.5) * 2) - 1
#
#   S, E = model.run_sync(noiced)
#   plot_similarity(S, patterns, idx, 1)
#   plot_error(E, idx, 1)
#
#   plot_states(S, width=width, height=height)

## task 3
# dictionary = {}
#
# for idx in range(0, 5000):
#   noiced = ((np.random.rand(width, height) < np.random.rand()) * 2) - 1
#
#   S, E = model.run_sync(noiced)
#
#   last_s = S[-1].tostring()
#   inv_last_s = ((-1) * S[-1]).tostring()
#
#   # if cycle
#   if len(S) > 2 and np.array_equal(S[-3], S[-1]):
#     prev = S[-2].tostring()
#     inv_prev = ((-1) * S[-2]).tostring()
#
#     if (last_s) in dictionary:
#       dictionary[last_s]['value'] += 1
#     elif (prev) in dictionary:
#       dictionary[prev]['value'] += 1
#
#     #handle inverse cycles
#     elif (inv_last_s) in dictionary:
#       dictionary[inv_last_s]['value'] += 1
#     elif (inv_prev) in dictionary:
#       dictionary[inv_prev]['value'] += 1
#     else:
#       dictionary[last_s] = {
#         'value': 1,
#         'is_cycle': True,
#         'prev': S[-2]
#       }
#   elif (last_s) in dictionary:
#     dictionary[last_s]['value'] += 1
#   elif (inv_last_s) in dictionary:
#     dictionary[inv_last_s]['value'] += 1
#   else:
#     dictionary[last_s] = {
#       'value': 1,
#       'is_cycle': False,
#     }
#
# for item in sorted(dictionary.items(), key=lambda x: x[1]['value'], reverse=True)[0:20]:
#   states = [np.fromstring(item[0])]
#   if item[1]['is_cycle']:
#     states.append(item[1]['prev'])
#   plot_states(states, width=width, height=height, hits=item[1]['value'])

## bonus
with open(dataset + '.dat') as f:
  count, width, height = [int(x) for x in f.readline().split()]

  for _ in range(count):
    f.readline()
    x = np.empty((height, width))
    for r in range(height):
      x[r, :] = np.array(list(f.readline().strip())) == '#'

    patterns.append(2 * x - 1)

    ## select patterns

    patterns = patterns[:]
    dim = width * height

    ## train and run model

    model = Hopfield(dim)
    model.train(patterns)

    indices = [(i,j) for i in range(0, height) for j in range(0, width)]

    for k in [0,7,14,21]:
      for idx, pattern in enumerate(patterns):
        similarities = []

        for iterate in range(0,100):
          noiced = pattern.copy()
          np.random.shuffle(indices)

          for index in indices[0:k]:
            noiced[index] *= -1

          S, E = model.run_sync(noiced)
          similarities.append(calculate_similarity(S[-1], pattern) == 1)

        print('| ' + str(len(patterns)) + ' | ' + str(idx) + ' | ' + str(k) + ' | ' + str(sum(similarities)/len(similarities)*100) + ' |')
        print('|---+---+---+---|')

