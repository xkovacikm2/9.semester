import numpy as np

from regressor import *
from util import *


## load data

data = np.loadtxt('mhd-easy.dat')

# data = data[:, data[-1] != 0] # prune empty cells

inputs  = data[0:2]
targets = data[2]

# plot_reg_density('Density', inputs, targets)

## normalize inputs
inputs[0] = normalize(inputs[0])
inputs[1] = normalize(inputs[1])

targets = normalize(targets) 

## train & visualize
model = MLPRegressor(inputs.shape[0], 20, targets.shape[0])
trainREs = model.train(inputs, targets, alpha=0.05, eps=300, trace=False)

outputs = model.predict(inputs)

plot_reg_density('Density', inputs, targets, outputs, block=False)
plot_errors('Model loss', trainREs, block=False)
