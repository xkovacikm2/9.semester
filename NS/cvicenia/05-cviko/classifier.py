import numpy as np

from mlp import *
from util import *
from func import *
import math


class MLPClassifier(MLP):

    def __init__(self, dim_in, dim_hid, n_classes):
        self.n_classes = n_classes
        super().__init__(dim_in, dim_hid, dim_out=n_classes)

    ## functions

    def cost(self, targets, outputs): # new
        return np.sum((targets - outputs)**2, axis=0)

    def f_hid(self, x): # override
        return tanh(x)

    def df_hid(self, x): # override
        return dtanh(x)

    def f_out(self, x): # override
        return logsig(x)

    def df_out(self, x): # override
        return dlogsig(x)


    ## prediction pass

    def predict(self, x):
        count = x.shape[1]
        y = []
        for i in range(count):
            x_i = x[:,i]
            q, *_ = self.forward(x_i)
            y.append(onehot_decode(q))
        return np.array(y)

    ## testing pass

    def test(self, inputs, labels):
        (_, count) = inputs.shape
        targets = onehot_encode(labels, self.n_classes)

        CE = 0
        RE = 0

        for i in range(0, count):
            x = inputs[:,i]
            d = targets[:,i]

            y, *_ = self.forward(x)

            CE += labels[i] != onehot_decode(y)
            RE += self.cost(d,y)

        CE /= count
        RE /= count

        return CE, RE


    ## training

    def train(self, inputs, labels, alpha=0.1, eps=100, trace=False, trace_interval=10):
        (_, count) = inputs.shape
        targets = onehot_encode(labels, self.n_classes)

        if trace:
            ion()

        CEs = []
        REs = []

        for ep in range(eps):
            print('Ep {:3d}/{}: '.format(ep+1, eps), end='')
            CE = 0
            RE = 0

            for i in np.random.permutation(count):
                x = inputs[:,i]
                d = targets[:,i]

                y, dW_hid, dW_out = self.backward(x, d)

                CE += labels[i] != onehot_decode(y)
                RE += self.cost(d,y)

                self.W_hid += alpha * dW_hid
                self.W_out += alpha * dW_out

            CE /= count
            RE /= count

            CEs.append(CE)
            REs.append(RE)

            print('CE = {:6.2%}, RE = {:.5f}'.format(CE, RE))

            if trace and ((ep+1) % trace_interval == 0):
                clear()
                predicted = self.predict(inputs)
                plot_dots(inputs, labels, predicted, block=False)
                plot_both_errors(CEs, REs, block=False)
                redraw()

        if trace:
            ioff()

        print()

        return CEs, REs
