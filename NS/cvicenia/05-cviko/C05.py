import numpy as np
import random

from classifier import *
from util import *


## load data

data = np.loadtxt('iris.dat').T

inputs = data[:-1]
labels = data[-1].astype(int) - 1 # last column is class, starts from 1

(dim, count) = inputs.shape


## split training & test set

# maybe:
ind = [i for i in range(0, count)]
random.shuffle(ind)
split = len(ind)//5
test_ind  = ind[:split]
train_ind = ind[split:]
# /maybe

train_inputs = inputs[:, train_ind]
train_labels = labels[train_ind]

test_inputs = inputs[:, test_ind]
test_labels = labels[test_ind]

# plot_dots(inputs)
# plot_dots(inputs, labels)

## train & test model

model = MLPClassifier(dim, 20, np.max(labels)+1)
trainCEs, trainREs = model.train(train_inputs, train_labels, alpha=0.1, eps=500, trace=True, trace_interval=100)

testCE, testRE = model.test(test_inputs, test_labels)
print('Final testing error: CE = {:6.2%}, RE = {:.5f}'.format(testCE, testRE))

plot_both_errors(trainCEs, trainREs, testCE, testRE, block=False)
