import numpy as np
import random

from som import *
from util import *



def L_1(win_idx, idx):
    return np.linalg.norm(np.asarray(idx) - np.asarray(win_idx))

def L_2(win_idx, idx):
    return np.sqrt(((idx[0] - win_idx[0])**2) + ((idx[1] - win_idx[1])**2))

def L_max(win_idx, idx):
    return max((idx[0] - win_idx[0]), (idx[1] - win_idx[1]))

## load data

# skewed square
# inputs = np.random.rand(2, 500)
# inputs[1,:] += 0.5 * inputs[0,:]

# # circle
# inputs = 2*np.random.rand(2, 500) - 1
# inputs = inputs[:,np.abs(np.linalg.norm(inputs, axis=0)) < 1]

# # first two features of iris
# inputs = np.loadtxt('iris.dat').T[:2]

# # first three features of iris
inputs = np.loadtxt('iris.dat').T[:3]

# # all features of iris
# inputs = np.loadtxt('iris.dat').T

(dim, count) = inputs.shape


## train model

rows = 5
cols = 9

metric = L_1

lambda_s = metric((0,0), (rows, cols)) / 2

model = SOM(dim, rows, cols, inputs)
model.train(inputs, discrete=False, metric=metric, alpha_s=0.5, alpha_f=0.01, lambda_s=lambda_s,
            lambda_f=1, eps=100, in3d=dim>2, trace=True, trace_interval=1)
