import numpy as np
import random

from util import *


## Radial-Basis Function Network
# (abstract base class)

class RBF():
  def __init__(self, dim_in, dim_rbf, dim_out):
    self.dim_in = dim_in
    self.dim_rbf = dim_rbf
    self.dim_out = dim_out
    self.W_out = np.random.randn(dim_rbf + 1, dim_out)

  ## radial-basis function

  def f_rbf(self, d):
    return np.exp(-(d ** 2) / (self.width ** 2))

  ## output activation function & derivation
  # (not implemented, to be overriden in derived classes)

  def f_out(self, x):
    pass

  def df_out(self, x):
    pass


    ## organize centers

  def adjust_width(self):
    win_dist = 0

    for ci in self.centres:
      for cj in self.centres:
        dist = np.linalg.norm(ci - cj)

        if dist > win_dist:
          win_dist = dist

    self.width = 1 / np.sqrt(2 * self.centres.shape[0]) * win_dist

  def init_random(self, inputs, extend=1.0):
    self.centres = np.random.uniform(np.min(inputs), np.max(inputs), size=(self.dim_rbf, self.dim_in))
    self.adjust_width()

  def init_sample(self, inputs):
    arr = inputs.T
    self.centres = arr[np.random.randint(0, arr.shape[0], self.dim_rbf)]

    self.adjust_width()

  def init_kmeans(self, inputs, beta=0.1, eps=20, trace=True, trace_interval=1):
    self.init_random(inputs, extend=1.5)

    if trace:
      ion()

    for ep in range(eps):
      for i in np.random.permutation(inputs.shape[1]):
        x = inputs[:, i]

        j = self.find_winner(x)

        self.centres[j] += beta * (x - self.centres[j])

      if trace and ((ep + 1) % trace_interval == 0):
        plot_kmeans(inputs, self.centres, block=False)
        redraw()

    if trace:
      ioff()

    self.adjust_width()

  def find_winner(self, input):
    win_idx = -1
    win_dist = np.inf

    for i in range(0, self.centres.shape[0]):
      center = self.centres[i]
      dist = np.linalg.norm(center - input)

      if dist < win_dist:
        win_dist = dist
        win_idx = i

    return win_idx

  ## forward pass (RBF only)
  # (single input vector)

  def radial(self, x):
    return [self.f_rbf(np.linalg.norm(x - weight)) for weight in self.centres]

  ## forward pass (full)
  # (single input vector)

  def forward(self, x):
    h = np.array(self.radial(x))

    h = augment(h).reshape(1, -1)
    b = h @ self.W_out
    y = self.f_out(b)

    return y, b, h

  ## forward & "backprop" pass (only trains output weights)
  # (single input and target vector)

  def backward(self, x, d):
    y, b, h = self.forward(x)

    g_out = (d - y) * self.df_out(b)

    if g_out.shape[1] != h.shape[0]:
      g_out = g_out.T

    dW_out = g_out @ h

    return y, dW_out
