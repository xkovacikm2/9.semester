import numpy as np

from regressor import *
from util import *


## load data

data = np.loadtxt('mhd-easy.dat')

data = data[:, data[-1] != 0] # prune empty cells

inputs  = data[0:2]
targets = data[2:]  # keep the regression targets as a 2D-matrix with 1 column

# plot_reg_density('Density', inputs, targets)


## normalize inputs

inputs -= np.mean(inputs, axis=1, keepdims=True)
inputs /= np.std(inputs,  axis=1, keepdims=True)

targets -= np.mean(targets)
targets /= np.std(targets)


## init

model = RBFRegressor(inputs.shape[0], 50, targets.shape[0])


## init centers (unsupervised)

relevant_inputs = inputs[:,(targets>0.1).flatten()]

# model.init_random(relevant_inputs)
model.init_sample(relevant_inputs)
# model.init_kmeans(relevant_inputs)


## train outputs (supervised)

trainREs = model.train_sgd(inputs, targets, alpha=0.05, eps=100)
plot_errors('Model loss', trainREs, block=False)

# model.train_pinv(inputs, targets)


## visualize

outputs = model.predict(inputs)
plot_reg_density('Density', inputs, targets, outputs, centres=model.centres, block=False)
