import numpy as np
import random

from classifier import *
from util import *


## load data

data = np.loadtxt('iris.dat').T

inputs = data[:-1]
labels = data[-1].astype(int) - 1 # last column is class, starts from 1

(dim, count) = inputs.shape


## split training & test set

ind = list(range(count))
random.shuffle(ind)

split = int(0.8 * count)
train_ind = ind[:split]
test_ind  = ind[split:]

train_inputs = inputs[:,train_ind]
train_labels = labels[train_ind]

test_inputs = inputs[:,test_ind]
test_labels = labels[test_ind]

# plot_dots(train_inputs, test_inputs=test_inputs)
# plot_dots(train_inputs, train_labels, None, test_inputs, test_labels, None)


## train & test model

model = RBFClassifier(dim, 10, np.max(labels)+1)


## init centers (unsupervised)

# model.init_random(inputs)
model.init_sample(inputs)
# model.init_kmeans(inputs)


## train outputs (supervised)

trainCEs, trainREs = model.train(train_inputs, train_labels, alpha=0.1, eps=100, trace=True, trace_interval=10)


## evaluate

testCE, testRE = model.test(test_inputs, test_labels)
print('Final testing error: CE = {:6.2%}, RE = {:.5f}'.format(testCE, testRE))

train_predicted = model.predict(train_inputs)
test_predicted  = model.predict(test_inputs)

# plot_dots(train_inputs, train_labels, train_predicted, test_inputs, test_labels, test_predicted, block=False)
plot_dots(train_inputs, None, None, test_inputs, test_labels, test_predicted, centres=model.centres, block=False)
plot_both_errors(trainCEs, trainREs, testCE, testRE, block=False)
# plot_areas(model, inputs, labels, block=False)
