import numpy as np

from rbf  import *
from util import *


class RBFClassifier(RBF):

    def __init__(self, dim_in, dim_hid, n_classes):
        self.n_classes = n_classes
        super().__init__(dim_in, dim_hid, dim_out=n_classes)

    
    ## functions

    def cost(self, targets, outputs): # new
        return np.sum((targets - outputs)**2, axis=0)

    def f_out(self, x): # override
        return 1 / (1 + np.exp(-x))

    def df_out(self, x): # override
        return self.f_out(x) * (1 - self.f_out(x))


    ## prediction pass

    def predict(self, inputs):
        # outputs, *_ = self.forward(inputs)  # if self.forward() can take a whole batch
        outputs = ([onehot_decode(self.forward(x)[0][0]) for x in inputs.T]) # otherwise
        return outputs


    ## testing pass

    def test(self, inputs, labels):
        outputs = np.array([(self.forward(x)[0][0]) for x in inputs.T])
        targets = onehot_encode(labels, self.n_classes)
        predicted = self.predict(inputs)
        CE = np.mean(predicted != labels)
        RE = np.mean(self.cost(targets, outputs.T))
        return CE, RE


    ## training

    def train(self, inputs, labels, alpha=0.1, eps=100, trace=False, trace_interval=10):
        (_, count) = inputs.shape
        targets = onehot_encode(labels, self.n_classes)

        if trace:
            ion()

        CEs = []
        REs = []

        for ep in range(eps):
            print('Ep {:3d}/{}: '.format(ep+1, eps), end='')
            CE = 0
            RE = 0

            for i in np.random.permutation(count):
                x = inputs[:,i]
                d = targets[:,i]

                y, dW_out = self.backward(x, d)

                CE += labels[i] != onehot_decode(y[0])
                RE += self.cost(d,y[0])

                self.W_out += alpha * (dW_out.T)

            CE /= count
            RE /= count

            CEs.append(CE)
            REs.append(RE)

            print('CE = {:6.2%}, RE = {:.5f}'.format(CE, RE))

            if trace and ((ep+1) % trace_interval == 0):
                predicted = self.predict(inputs)
                plot_dots(inputs, labels, predicted, centres=self.centres, block=False)
                plot_both_errors(CEs, REs, block=False)
                # plot_areas(self, inputs, block=False)
                redraw()

        if trace:
            ioff()

        print()

        return CEs, REs
