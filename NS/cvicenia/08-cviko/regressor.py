import numpy as np

from rbf import *
from util import *


class RBFRegressor(RBF):
  def __init__(self, dim_in, dim_hid, dim_out):
    super().__init__(dim_in, dim_hid, dim_out)

  ## functions

  def cost(self, targets, outputs):  # new
    return np.sum((targets - outputs) ** 2, axis=0)

  def f_out(self, x):  # override
    return x

  def df_out(self, x):  # override
    return np.ones(x.shape)

  ## prediction pass

  def predict(self, inputs):
    # outputs, *_ = self.forward(inputs)  # if self.forward() can take a whole batch
    outputs = np.stack([self.forward(x)[0] for x in inputs.T])  # otherwise
    return outputs

  ## training

  def train_sgd(self, inputs, targets, alpha=0.1, eps=100, trace=True, trace_interval=10):
    (_, count) = inputs.shape

    errors = []

    if trace:
      ion()

    for ep in range(eps):
      print('Ep {:3d}/{}: '.format(ep + 1, eps), end='')
      E = 0

      for i in np.random.permutation(count):
        x = inputs[:, i]
        d = targets[:, i]

        y, dW_out = self.backward(x, d)

        E += self.cost(d, y)

        self.W_out += alpha * dW_out.T

      E /= count
      errors.append(E)
      print('E = {:.3f}'.format(E[0]))

      if trace and ((ep + 1) % trace_interval == 0):
        outputs = self.predict(inputs)
        plot_reg_density('Density', inputs, targets, outputs, centres=self.centres, block=False)
        redraw()

    if trace:
      ioff()

    return errors

  def train_pinv(self, inputs, targets):
    hidden = np.array([(self.forward(x))[2][0] for x in inputs.T])
    inverse = self.pseudo_inverse_matrix(hidden, hidden.shape[0], hidden.shape[1])

    if inverse == None:
      print('Inverse matrix not computable')
      exit()

    self.W_out = targets @ inverse

  def pseudo_inverse_matrix(self, inputs, n, k):
    rank = np.linalg.matrix_rank(inputs)
    if n == k and rank == n:
      return np.linalg.inv(inputs)

    inputs_t = inputs.T

    if n < k and rank == n:
      return inputs_t @ np.linalg.inv(inputs @ inputs_t)

    if k < n and rank == k:
      return np.linalg.inv(inputs_t @ inputs) @ inputs_t
