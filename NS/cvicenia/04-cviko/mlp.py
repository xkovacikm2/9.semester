import numpy as np

from util import *

class MLP():

    def __init__(self, dim_in, dim_hid, dim_out):
        self.dim_in     = dim_in
        self.dim_hid    = dim_hid
        self.dim_out    = dim_out

        self.W_hid = np.random.rand(dim_in+1, dim_hid)
        self.W_out = np.random.rand(dim_hid+1, 1)

    def predict(self, x):
        count = x.shape[1]
        y = []
        for i in range(count):
            x_i = x[:,i]
            q, *_ = self.forward(x_i)
            y.append(q[0,0])
        return np.array(y)

    def forward(self, x):
        x = np.matrix(x)
        x = augment(x.T).T

        a = x @ self.W_hid
        f_hid = np.vectorize(self.f_hid)

        h = f_hid(a)
        h = augment(h.T).T

        b = h @ self.W_out
        y = self.f_out(b)

        return y, b, h, a


    def backward(self, x, d):
        y, b, h, a = self.forward(x)

        x = np.matrix(x)
        x = augment(x.T).T

        g_out = (d-y) * self.df_out(b)

        W_out = self.W_out[:-1]
        df_hid = np.vectorize(self.df_hid)

        g_hid = ((W_out @ g_out)[0] @ df_hid(a)[0]).T

        dW_out = (g_out @ h).T
        dW_hid = (g_hid @ x).T
        return y, dW_hid, dW_out
