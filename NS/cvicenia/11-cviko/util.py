import atexit
import numpy as np
import matplotlib

matplotlib.use('TkAgg')  # todo: remove or change if not working
import matplotlib.pyplot as plt


## plotting
def augment(X):
  if X.ndim == 1:
    return np.concatenate((X, [1]))
  else:
    pad = np.ones((1, X.shape[1]))
    return np.concatenate((X, pad), axis=0)


def plot_prediction(targets, outputs, split, block=True):
  plt.figure()

  plt.plot(targets, lw=5, alpha=0.3)
  plt.plot(outputs)

  plt.tight_layout()
  plt.gcf().canvas.set_window_title('Prediction')
  plt.show(block=block)


def mackey_glass(n, a=0.2, b=0.8, c=0.9, d=23, e=10, initial=0.1):
  # Mackey-Glass discrete equation.
  x = np.zeros(n)
  x[0] = initial
  d = int(d)
  for k in range(0, n - 1):
    x[k + 1] = c * x[k] + ((a * x[k - d]) / (b + (x[k - d] ** e)))
  return x
