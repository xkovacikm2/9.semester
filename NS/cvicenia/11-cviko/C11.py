import numpy as np

from esn import ESN
from util import *


## generate data

count    = 50
overhang = 1.0  # additional length for generalization

X = np.linspace(0, 2*np.pi*(1+overhang), num=count*(1+overhang))

# data = 0*X
# data = 0*X + 4
# data = 0.5*X
# data = 50.0*X
# data = X**2
# data = (2*X-4)**2
# data = np.sin(X*3)
# data = np.sin(X*3) + 5
# data = np.sin(X) * np.sin(0.5*X)
# data = np.sin(X*8) + 0.4 * np.cos(X*2)
# data = np.sin(3*X)**3
# data = np.sin(X) * np.sin(2*X) # last that should definitely work
# data = np.sin(X) * np.sin(3*X)
# data = np.sin(X*3) * X
# data = np.sin(X**2*2)
data = np.sin(X**2*9) + np.cos(X*3)
# data = np.tan(X*3)

input  = data[:-1]
target = data[1:]


## train and run model

import matplotlib.pyplot as plt

model = ESN(dim_res=20, radius=2.1, sparsity=1)
model.use(input, target)
