import numpy as np

from util import *


class ESN():
  def __init__(self, dim_res, radius, scale_in=0.01, scale_res=0.01, scale_out=0.01, sparsity=None):
    self.dim_in = 1
    self.dim_res = dim_res
    self.dim_out = 1

    W_in = np.random.randn(self.dim_in, self.dim_res)
    W_res = np.random.randn(self.dim_res, self.dim_res)
    W_out = np.random.rand(self.dim_res+1, self.dim_out)

    self.W_in = W_in * scale_in
    self.W_res = W_res * scale_res
    self.W_out = W_out * scale_out

    if sparsity:
      for i in range(0,self.W_res.shape[0]):
        for j in range(0,self.W_res.shape[1]):
          if np.random.randn() < sparsity:
            self.W_res[i,j] = 0

    eigs, _ = np.linalg.eig(self.W_res)
    self.W_res *= radius/np.abs(np.max(eigs))

    self.res = self.f_res(np.matrix([[0]]) @ self.W_in)

  def f_res(self, X):
    return np.tanh(X)

  def feed(self, x):
    self.res = self.W_in * x + self.res @ self.W_res
    return self.res

  def use(self, input, target):

    full_count = len(input)
    train_count = int(0.5 * full_count)
    test_count = full_count - train_count

    # 1. compute reservoir states for the training inputs

    R = np.zeros((self.dim_res+1, full_count))

    for i in range(train_count):
      tmp = self.f_res(self.feed(input[i]))
      R[:, i] = augment(tmp.T).T

    # 2. compute output weights analytically

    self.W_out = np.matrix(input[0:train_count]) * np.linalg.pinv(R[:, 0:train_count])

    output = np.zeros((full_count))

    output[:train_count] = self.W_out * R[:, 0:train_count]

    # 3. generalize, using previous output as input

    for i in range(train_count, full_count):
      tmp = self.f_res(self.feed(input[i]))
      R[:, i] = augment(tmp.T).T
      output[i] = self.W_out @ R[:, i]

    plot_prediction(target, output, train_count)
