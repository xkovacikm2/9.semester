import atexit
import numpy as np
import matplotlib
matplotlib.use('TkAgg') # todo: remove or change if not working
import matplotlib.pyplot as plt
import os
import time


## utility



## plotting

def plot_state(s, index=None, width=None, height=None, block=True):
    plt.figure(1).canvas.mpl_connect('key_press_event', keypress)
    plt.clf()

    plt.imshow(s.reshape((height, width)), cmap='gray', interpolation='nearest', vmin=-1, vmax=+1)
    plt.xticks([])
    plt.yticks([])

    if index:
        plt.scatter(index%width, index//width, s=150)

    plt.tight_layout()
    plt.gcf().canvas.set_window_title('State')
    plt.show(block=block)


def plot_states(S, width=None, height=None, block=True):
    plt.figure(2).canvas.mpl_connect('key_press_event', keypress)
    plt.clf()

    for i, s in enumerate(S):
        plt.subplot(1, len(S), i+1)
        plt.imshow(s.reshape((height, width)), cmap='gray', interpolation='nearest', vmin=-1, vmax=+1)
        plt.xticks([])
        plt.yticks([])

    plt.tight_layout()
    plt.gcf().canvas.set_window_title('States')
    plt.show(block=block)



## interactive drawing, very fragile....

wait = 0.005

def clear():
    plt.clf()


def ion():
    plt.ion()
    time.sleep(wait)


def ioff():
    plt.ioff()


def redraw():
    plt.gcf().canvas.draw()
    plt.waitforbuttonpress(timeout=0.001)
    time.sleep(wait)

def keypress(e):
    if e.key in {'q', 'escape'}:
        os._exit(0) # unclean exit, but exit() or sys.exit() won't work


## non-blocking figures still block at end

def finish():
    plt.show(block=True) # block until all figures are closed


atexit.register(finish)
