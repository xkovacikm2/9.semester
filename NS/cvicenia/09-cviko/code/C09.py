import numpy as np

from hopfield import Hopfield
from util import *

## load data

dataset = 'small'

patterns = []

with open(dataset + '.txt') as f:
  count, width, height = [int(x) for x in f.readline().split()]

  for _ in range(count):
    f.readline()
    x = np.empty((height, width))
    for r in range(height):
      x[r, :] = np.array(list(f.readline().strip())) == '#'

    patterns.append(2 * x - 1)

## select patterns

patterns = patterns[:]
count = len(patterns)
dim = width * height

## train and run model

model = Hopfield(dim)
model.train(patterns)

noise = np.random.rand(height, width)
for i in range(0,height):
  for j in range(0,width):
    noise[i,j] = 1 if noise[i,j] > 0.5 else -1

# synchronous deterministic

S, E = model.run_sync(noise)
print(E)
plot_states(S, width=width, height=height)

# asynchronous deterministic

# model.run_async(noise)
