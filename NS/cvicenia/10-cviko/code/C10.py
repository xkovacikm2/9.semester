import numpy as np

from hopfield import Hopfield
from util import *


## load data

dataset = 'small'
# dataset = 'medium'

patterns = []

with open(dataset+'.txt') as f:
    count, width, height = [int(x) for x in f.readline().split()]
    
    for _ in range(count):
        f.readline()
        x = np.empty((height, width))
        for r in range(height):
            x[r,:] = np.array(list(f.readline().strip())) == '#'

        patterns.append(2*x-1)


## select patterns

patterns = patterns[:]
count = len(patterns)
dim = width*height


## train and run model

model = Hopfield(width, height)
model.train(patterns)

# a) from before: random binary pattern

input = 2*(np.random.rand(height, width) > 0.5)-1

# b) corrupted input

input_2 = patterns[np.random.randint(0, len(patterns))]
for i in range(0, len(input_2)):
    for j in range(0, len(input_2[i])):
        if np.random.random() < 0.2:
            input_2[i, j] *= -1

# asynchronous stochastic vs. deterministic

model.run_async(input_2, eps=5, rows=2, row=1)
model.run_async(input_2, eps=20, rows=2, row=2, beta_s=0.1, beta_f=10)
