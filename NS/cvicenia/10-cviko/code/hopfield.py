import numpy as np

from util import *


class Hopfield():
  max_steps = 10

  def __init__(self, width, height):
    self.width = width
    self.height = height
    self.dim = width * height

  def train(self, patterns):
    self.W = np.zeros((self.dim, self.dim))

    for x in (p.flatten() for p in patterns):
      self.W += np.outer(x, x) - np.eye(self.dim)  # `eye` only works with ±1 in x

    self.W /= len(patterns)

  def energy(self, s):
    return -np.inner(s, self.W @ s)

  def forward(self, s, neuron=None):
    W = self.W if neuron is None else self.W[neuron]

    net = (W @ s)

    if self.beta is not None:  # stochastic
      p = 1 / (1 + np.exp(-net * self.beta))
      return 1 if np.random.random() < p else -1
    else:  # deterministic
      return 2 * (net >= 0) - 1

  def run_async(self, x, eps=None, beta_s=None, beta_f=None, row=1, rows=1):
    s = x.flatten()
    e = self.energy(s)
    E = [e]

    for ep in range(eps):
      if beta_s:
        self.beta = beta_s * ((beta_f/beta_s)**(ep/(eps-1)))
        print('Ep {:2d}/{:2d}:  stochastic, beta = 1/T = {:7.4f}'.format(ep + 1, eps, self.beta))
      else:
        self.beta = None
        print('Ep {:2d}/{:2d}:  deterministic'.format(ep + 1, eps))

      for i in np.random.permutation(self.dim):
        s[i] = self.forward(s, neuron=i)
        e = self.energy(s)
        E.append(e)

      plot_state(s, errors=E, index=None, max_eps=eps * self.dim, width=self.width, height=self.height, row=row,
                 rows=rows, block=False)
      redraw()

      # terminate deterministic when stuck in a local/global minimum

      if self.beta is None:
        if np.all(self.forward(s) == s):
          break
