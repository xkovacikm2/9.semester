import atexit
import numpy as np
import matplotlib
matplotlib.use('TkAgg') # todo: remove or change if not working
import matplotlib.pyplot as plt
import time


## plotting

palette = ['#e41a1c','#377eb8','#4daf4a','#984ea3','#ff7f00','#ffff33','#a65628','#f781bf','#999999']


def limits(values, gap=0.05):
    x0 = np.min(values)
    x1 = np.max(values)
    xg = (x1 - x0) * gap
    return np.array((x0-xg, x1+xg))


def plot_pca(inputs, transformed=None, weights=None, i_x=0, i_y=1, s=100, block=True):
    plt.figure(1, figsize=(10,5))
    plt.clf()

    if transformed is not None:
        plt.subplot(1,2,1)
        plt.axis('equal')

    plt.scatter(inputs[i_x,:], inputs[i_y,:], s=s, c=palette[-1], edgecolors=[0.4]*3, alpha=0.5)

    if weights is not None:
        X = np.outer(weights[0], [-4,+4])
        plt.plot(X[0], X[1], c=palette[0])

        if weights.shape[0]>1:
            Y = np.outer(weights[1], [-2,+2])
            plt.plot(Y[0], Y[1], c=palette[1])

    if transformed is not None:
        plt.subplot(1,2,2)
        plt.axis('equal')
        plt.scatter(transformed[0,:], transformed[1,:], s=s, c=palette[-1], edgecolors=[0.4]*3, alpha=0.5)

    plt.tight_layout()
    plt.show(block=block)


## interactive drawing, very fragile....

wait = 0.0

def clear():
    plt.clf()


def ion():
    plt.ion()
    time.sleep(wait)


def ioff():
    plt.ioff()


def redraw():
    plt.gcf().canvas.draw()
    time.sleep(wait)

## non-blocking figures still block at end

def finish():
    plt.show(block=True) # block until all figures are closed


atexit.register(finish)
