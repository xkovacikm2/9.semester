import numpy as np
import random

from pca import *
from util import *


## constants

dim_in  = 3
dim_out = 2
count   = 500
skew    = 0.5


## generate data

data  = np.random.randn(dim_in, count)         # multi-variate gaussian

cov   = np.random.randn(dim_in) * skew         # some axis correlations
cov  += cov.T                                  # make symmetric
corr  = np.eye(dim_in) + cov                   # perturb
corr /= np.linalg.norm(corr) / np.sqrt(dim_in) # normalize

inputs = corr @ data

# plot_pca(inputs)

## train & test model

model = PCA(dim_in, dim_out)
model.train('GHA', inputs, alpha=0.04, eps=500, trace=False, trace_interval=10)
