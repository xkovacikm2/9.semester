import numpy as np

from util import *
import pdb;


class PCA():

    def __init__(self, dim_in, dim_out):
        self.dim_in  = dim_in
        self.dim_out = dim_out

        self.weights = np.random.rand(dim_out, dim_in) # no bias -- inputs must be centered!


    def transform(self, inputs):
        return self.weights @ inputs

    def train(self, algo, inputs, alpha=0.1, eps=100, trace=True, trace_interval=10):
        assert(algo in {'Hebb', 'Oja', 'GHA'})

        (_, count) = inputs.shape

        if trace:
            ion()

        for ep in range(eps):

            for i in np.random.permutation(count):
                x = inputs[:,i]
                y = self.weights @ x


                if algo == 'Hebb':
                    assert(self.dim_out == 1) # only for a single output unit
                    self.weights += alpha*y*x
                    self.weights /= np.max(self.weights)

                if algo == 'Oja':
                    y = np.expand_dims(y, axis=1)
                    self.weights += alpha*y*(x-(y.T)@self.weights)

                if algo == 'GHA':
                    gha_mem =  sum(y[t] * self.weights[t] for t in range(self.dim_out))
                    y = np.expand_dims(y, axis=1)
                    self.weights += alpha * y * (x - gha_mem).T

            if trace and ((ep+1) % trace_interval == 0):
                transformed = self.transform(inputs) if self.dim_out == 2 else None
                plot_pca(inputs, transformed, self.weights, block=False)
                redraw()

        transformed = self.transform(inputs) if self.dim_out == 2 else None
        plot_pca(inputs, transformed, self.weights, block=False)

        if trace:
            ioff()
