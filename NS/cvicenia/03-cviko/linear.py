import numpy as np
from util import *

class LinearAutoassociator():

    def __init__(self, dim):
        self.dim     = dim
        self.weights = np.zeros([dim, dim])

    def reconstruct(self, input):
        # return (self.weights - np.eye(self.dim)) @ input
        return self.weights @ input

    def analytical(self, inputs):
        shape = inputs.shape
        n = shape[0]
        k = shape[1]

        self.weights = inputs @ self.pseudo_inverse_matrix(inputs, n, k)

    def iterative(self, inputs):
        (_, count) = inputs.shape

        for i in range(count):
            x = inputs[:,i]
            x = x.reshape((x).size, 1)
            z = x - (self.weights @ x)

            self.weights = self.weights + ((z @ z.T)/(np.linalg.norm(z)**2))

    def pseudo_inverse_matrix(self, inputs, n, k):
        rank = np.linalg.matrix_rank(inputs)
        if n == k and rank == n:
            return np.linalg.inv(inputs)

        inputs_t = inputs.T

        if n < k and rank == n:
            return inputs_t @ np.linalg.inv( inputs @ inputs_t )

        if k < n and rank == k:
            return np.linalg.inv(inputs_t @ inputs) @ inputs_t
