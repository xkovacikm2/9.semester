import numpy as np

from util import *
import math


class Perceptron2():

    def __init__(self, dim):
        self.dim     = dim
        self.weights = np.random.rand(dim+1)

    def train(self, inputs, targets, alpha=0.1, eps=20, trace=False):
        (count, _) = inputs.shape

        if trace:
            plot_decision(self.weights, inputs, targets, show=False)
            ion()

        errors = []

        for ep in range(eps):
            print('Ep {:3d}/{}:'.format(ep+1, eps), end='')
            E = 0

            for i in np.random.permutation(count):
                x = augment(inputs[i])
                d = targets[i]

                net = sum(x*self.weights)
                y = 1/(1+math.exp(-net))

                print(y-d)
                e = 1/2*(y - d)**2
                E += e**2 / 2.0 # Python 2 has C-like / semantics, Python 3 has // for integer division, / for float

                self.weights -= alpha*(d-y)*(-1)*(math.exp(-net)/((1+math.exp(-net))**2))*x

            errors.append(E)

            print('E = {:.3f}'.format(E))

            if trace:
                clear()
                plot_decision(self.weights, inputs, targets, show=False)
                redraw()

        if trace:
            ioff()

        return errors
