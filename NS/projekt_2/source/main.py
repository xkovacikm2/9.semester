import numpy as np
import random

from som import *
from util import *


def L_1(win_idx, idx):
  return np.linalg.norm(np.asarray(idx) - np.asarray(win_idx))

def L_2(win_idx, idx):
  return np.sqrt(sum([(idx[i] - win_idx[i]) ** 2 for i in range(0, len(win_idx))]))

def L_max(win_idx, idx):
  return max([abs(idx[i] - win_idx[i]) for i in range(0, len(win_idx))])

## load data
data = np.loadtxt('seeds.dat').T
inputs = data[:-1]
labels = data[-1]
(dim, count) = inputs.shape

## train model
rows = 10
cols = 10

metric = L_1
lambda_s = metric([0, 0], [rows, cols]) / 2

model = SOM(dim, rows, cols, inputs)
avg_dist, avg_adjust, alpha_dec, lamda_dec = model.train(inputs, discrete=True, metric=metric, alpha_s=1, alpha_f=0.05, lambda_s=lambda_s,
            lambda_f=1, eps=200, in3d=dim > 2, trace=True, trace_interval=1)

plot_dists(model.weights)
plot_activations(inputs=inputs, labels=labels, model=model)
plot_heatmap(model=model)
plot_report(avg_dist, avg_adjust, alpha_dec, lamda_dec)
