import numpy as np

from util import *


class SOM():
  def __init__(self, dim_in, n_rows, n_cols, inputs=None):
    self.dim_in = dim_in
    self.n_rows = n_rows
    self.n_cols = n_cols
    self.weights = np.random.uniform(np.min(inputs), np.max(inputs), size=(n_rows, n_cols, dim_in))

  def winner(self, x):
    win_r, win_c, win_d = -1, -1, float('inf')

    for r in range(0, self.n_rows):
      for c in range(0, self.n_cols):
        distance = np.linalg.norm(self.weights[r, c] - x)

        if distance < win_d:
          win_d = distance
          win_r = r
          win_c = c

    return win_r, win_c, win_d

  def train(self, inputs, discrete=True, metric=lambda x, y: 0, alpha_s=0.01, alpha_f=0.001, lambda_s=None,
            lambda_f=1, eps=100, in3d=True, trace=True, trace_interval=10):
    (_, count) = inputs.shape
    avg_dist = []
    avg_adjust = []
    alpha_dec = []
    lamda_dec = []

    if trace:
      ion()
      (plot_grid_3d if in3d else plot_grid_2d)(inputs, self.weights, block=False)
      redraw()

    for ep in range(eps):
      alpha_t = alpha_s * ((alpha_f / alpha_s) ** (ep / (eps - 1)))
      lambda_t = lambda_s * ((lambda_f / lambda_s) ** (ep / (eps - 1)))

      alpha_dec.append(alpha_t)
      lamda_dec.append(lambda_t/lambda_s)

      print()
      print('Ep {:3d}/{:3d}:'.format(ep + 1, eps))
      print('  alpha_t = {:.3f}, lambda_t = {:.3f}'.format(alpha_t, lambda_t))

      distances = []
      adjustments = 0
      for i in np.random.permutation(count):
        x = inputs[:, i]

        win_r, win_c, win_d = self.winner(x)
        distances.append(win_d)

        for r in range(self.n_rows):
          for c in range(self.n_cols):
            d = metric((win_r, win_c), (r, c))
            h = (1 if d < lambda_t else 0) if discrete else (np.exp(-(d ** 2) / (lambda_t ** 2)))
            adjustment = (alpha_t * (x - self.weights[r, c]) * h)
            self.weights[r, c] += adjustment
            adjustments += sum(abs(adjustment))

      avg_dist.append(sum([abs(d) for d in distances])/len(distances))
      avg_adjust.append(adjustments/count/self.n_cols/self.n_rows)

      if trace and ((ep + 1) % trace_interval == 0):
        (plot_grid_3d if in3d else plot_grid_2d)(inputs, self.weights, block=False)
        redraw()

    if trace:
      ioff()

    return avg_dist, avg_adjust, alpha_dec, lamda_dec
