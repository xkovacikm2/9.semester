import atexit
import numpy as np
import matplotlib

matplotlib.use('TkAgg')  # todo: remove or change if not working
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import time

## plotting

palette = ['#e41a1c', '#377eb8', '#4daf4a', '#984ea3', '#ff7f00', '#ffff33', '#a65628', '#f781bf', '#999999']


def limits(values, gap=0.05):
  x0 = np.min(values)
  x1 = np.max(values)
  xg = (x1 - x0) * gap
  return np.array((x0 - xg, x1 + xg))


def plot_grid_2d(inputs, weights, i_x=0, i_y=1, s=60, block=True):
  plt.figure(1)
  plt.clf()

  # plt.gcf().canvas.set_window_title('SOM neurons and inputs (2D)')
  plt.scatter(inputs[i_x, :], inputs[i_y, :], s=s, c=palette[-1], edgecolors=[0.4] * 3, alpha=0.5)

  n_rows, n_cols, _ = weights.shape

  for r in range(n_rows):
    plt.plot(weights[r, :, i_x], weights[r, :, i_y], c=palette[0])

  for c in range(n_cols):
    plt.plot(weights[:, c, i_x], weights[:, c, i_y], c=palette[0])

  plt.xlim(limits(inputs[i_x, :]))
  plt.ylim(limits(inputs[i_y, :]))
  plt.tight_layout()
  plt.show(block=block)


def plot_grid_3d(inputs, weights, i_x=0, i_y=1, i_z=2, s=60, block=True):
  fig = plt.figure(2)
  # plt.gcf().canvas.set_window_title('SOM neurons and inputs (3D)')

  if plot_grid_3d.ax is None:
    plot_grid_3d.ax = Axes3D(fig)

  ax = plot_grid_3d.ax
  ax.cla()

  ax.scatter(inputs[i_x, :], inputs[i_y, :], inputs[i_z, :], s=s, c=palette[-1], edgecolors=[0.4] * 3, alpha=0.5)

  n_rows, n_cols, _ = weights.shape

  for r in range(n_rows):
    ax.plot(weights[r, :, i_x], weights[r, :, i_y], weights[r, :, i_z], c=palette[0])

  for c in range(n_cols):
    ax.plot(weights[:, c, i_x], weights[:, c, i_y], weights[:, c, i_z], c=palette[0])

  ax.set_xlim(limits(inputs[i_x, :]))
  ax.set_ylim(limits(inputs[i_y, :]))
  ax.set_zlim(limits(inputs[i_z, :]))
  plt.show(block=block)


plot_grid_3d.ax = None

## interactive drawing, very fragile....

wait = 0.0


def clear():
  plt.clf()


def ion():
  plt.ion()
  # time.sleep(wait)


def plot_activations(inputs, labels=None, model=None, s=100):
  plt.figure(4)
  plt.clf()

  neurons = model.weights

  compartments = np.zeros((neurons.shape[0], neurons.shape[1], int(np.max(labels))), dtype=int)
  max_labels = 0

  for ci, x in enumerate(inputs.T):
    win_r, win_c, win_d = model.winner(x)

    compartments[win_r, win_c, int(labels[ci]) - 1] += 1
    max_labels = max(np.sum(compartments[win_r, win_c]), max_labels)

  for r in range(neurons.shape[0]):
    for c in range(neurons.shape[1]):
      if any(compartments[r, c] > 0):
        cl = np.argmax(compartments[r, c])
        scale = (sum(compartments[r, c]) / max_labels) * s
        plt.scatter(r, c, s=scale, c=palette[cl])

  plt.show(block=False)


def ioff():
  plt.ioff()


def plot_heatmap(model):
  for i in range(0, model.dim_in):
    plt.figure(6 + i)
    plt.imshow(model.weights[:, :, i], cmap='hot', interpolation='nearest')


def plot_dists(weights, filename=None):
  horizontal_weights = weights[1:]
  vertical_weights = weights[:, 1:]

  plt.figure(5)
  plt.subplot(2, 1, 1)
  ax = plt.subplot(2, 1, 1)
  ax.axis('off')
  plt.imshow(np.linalg.norm(weights[:-1] - horizontal_weights, axis=2), cmap='gray')
  ax = plt.subplot(2, 1, 2)
  plt.imshow(np.linalg.norm(weights[:, :-1] - vertical_weights, axis=2), cmap='gray')
  ax.axis('off')

  plt.tight_layout()

  plt.show(block=False)


def plot_report(avg_distance, avg_adjust, alpha_decay, lambda_decay, block=True):
  plt.figure()
  plt.clf()

  h1, = plt.plot(avg_distance, label='average distance')
  h2, = plt.plot(avg_adjust, label='average adjust')
  h3, = plt.plot(alpha_decay, label='alpha decay')
  h4, = plt.plot(lambda_decay, label='lambda decay')
  plt.legend(handles=[h1, h2, h3, h4])

  plt.show(block=block)


def redraw():
  plt.gcf().canvas.draw()
  # plt.gcf().canvas.draw_idle()
  # fig.canvas.draw_idle()
  plt.waitforbuttonpress(timeout=0.001)
  time.sleep(wait)


## non-blocking figures still block at end

def finish():
  plt.show(block=True)  # block until all figures are closed


atexit.register(finish)
