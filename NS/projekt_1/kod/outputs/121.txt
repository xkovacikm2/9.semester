hidden function: LOGSIG
output function: LOGSIG
number of epizodes: 20
number of neurons: 100
weight init function: Gaussovská distribúcia od -1 do 1
alpha value: 0.1
Ep   1/20: CE = 21.88%, RE = 0.33175
Ep   2/20: CE = 10.59%, RE = 0.18264
Ep   3/20: CE =  7.86%, RE = 0.14670
Ep   4/20: CE =  6.58%, RE = 0.12574
Ep   5/20: CE =  5.98%, RE = 0.11347
Ep   6/20: CE =  5.34%, RE = 0.10243
Ep   7/20: CE =  4.84%, RE = 0.09468
Ep   8/20: CE =  4.39%, RE = 0.08748
Ep   9/20: CE =  3.81%, RE = 0.08171
Ep  10/20: CE =  4.00%, RE = 0.07799
Ep  11/20: CE =  3.59%, RE = 0.07344
Ep  12/20: CE =  3.55%, RE = 0.07186
Ep  13/20: CE =  3.64%, RE = 0.06915
Ep  14/20: CE =  3.48%, RE = 0.06772
Ep  15/20: CE =  3.45%, RE = 0.06504
Ep  16/20: CE =  3.00%, RE = 0.06151
Ep  17/20: CE =  3.00%, RE = 0.06167
Ep  18/20: CE =  2.92%, RE = 0.05892
Ep  19/20: CE =  2.95%, RE = 0.05766
Ep  20/20: CE =  2.95%, RE = 0.05674
Final testing error: CE =  2.56%, RE = 0.05424