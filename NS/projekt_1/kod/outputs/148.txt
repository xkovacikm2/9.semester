hidden function: TANH
output function: LOGSIG
number of epizodes: 20
number of neurons: 100
weight init function: Gaussovská distribúcia od -1 do 1
alpha value: 0.1
Ep   1/20: CE = 44.30%, RE = 0.53275
Ep   2/20: CE = 32.75%, RE = 0.37413
Ep   3/20: CE =  8.91%, RE = 0.15950
Ep   4/20: CE =  5.19%, RE = 0.07985
Ep   5/20: CE =  4.83%, RE = 0.07344
Ep   6/20: CE =  4.06%, RE = 0.06557
Ep   7/20: CE =  3.94%, RE = 0.06200
Ep   8/20: CE =  3.64%, RE = 0.05723
Ep   9/20: CE =  3.45%, RE = 0.05460
Ep  10/20: CE =  3.31%, RE = 0.05246
Ep  11/20: CE =  3.25%, RE = 0.04925
Ep  12/20: CE =  3.00%, RE = 0.04787
Ep  13/20: CE =  3.22%, RE = 0.04910
Ep  14/20: CE =  3.08%, RE = 0.04794
Ep  15/20: CE =  2.95%, RE = 0.04679
Ep  16/20: CE =  2.75%, RE = 0.04321
Ep  17/20: CE =  2.86%, RE = 0.04511
Ep  18/20: CE =  2.69%, RE = 0.04245
Ep  19/20: CE =  2.77%, RE = 0.04264
Ep  20/20: CE =  2.61%, RE = 0.04170
Final testing error: CE =  2.50%, RE = 0.03806