hidden function: TANH
output function: LOGSIG
number of epizodes: 20
number of neurons: 40
weight init function: Gaussovská distribúcia od -1 do 1
alpha value: 0.1
Ep   1/20: CE = 13.50%, RE = 0.21891
Ep   2/20: CE =  6.08%, RE = 0.11117
Ep   3/20: CE =  4.83%, RE = 0.08901
Ep   4/20: CE =  4.44%, RE = 0.07704
Ep   5/20: CE =  3.94%, RE = 0.07120
Ep   6/20: CE =  3.83%, RE = 0.06679
Ep   7/20: CE =  3.50%, RE = 0.06092
Ep   8/20: CE =  3.36%, RE = 0.05900
Ep   9/20: CE =  3.36%, RE = 0.05696
Ep  10/20: CE =  3.33%, RE = 0.05332
Ep  11/20: CE =  3.09%, RE = 0.05134
Ep  12/20: CE =  2.89%, RE = 0.04851
Ep  13/20: CE =  2.80%, RE = 0.04726
Ep  14/20: CE =  2.78%, RE = 0.04636
Ep  15/20: CE =  2.66%, RE = 0.04597
Ep  16/20: CE =  2.81%, RE = 0.04399
Ep  17/20: CE =  2.72%, RE = 0.04363
Ep  18/20: CE =  2.83%, RE = 0.04387
Ep  19/20: CE =  2.64%, RE = 0.04209
Ep  20/20: CE =  2.62%, RE = 0.04270
Final testing error: CE =  1.94%, RE = 0.03626