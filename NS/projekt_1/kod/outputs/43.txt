hidden function: TANH
output function: LOGSIG
number of epizodes: 20
number of neurons: 10
weight init function: Gaussovská distribúcia od -1 do 1
alpha value: 0.2
Ep   1/20: CE = 16.39%, RE = 0.25408
Ep   2/20: CE =  9.33%, RE = 0.14647
Ep   3/20: CE =  7.92%, RE = 0.12205
Ep   4/20: CE =  7.36%, RE = 0.11005
Ep   5/20: CE =  6.53%, RE = 0.10268
Ep   6/20: CE =  6.64%, RE = 0.10156
Ep   7/20: CE =  6.45%, RE = 0.09774
Ep   8/20: CE =  6.09%, RE = 0.09284
Ep   9/20: CE =  5.97%, RE = 0.08966
Ep  10/20: CE =  5.50%, RE = 0.08255
Ep  11/20: CE =  5.03%, RE = 0.07650
Ep  12/20: CE =  5.20%, RE = 0.07723
Ep  13/20: CE =  4.59%, RE = 0.07114
Ep  14/20: CE =  4.64%, RE = 0.07129
Ep  15/20: CE =  4.70%, RE = 0.06952
Ep  16/20: CE =  4.33%, RE = 0.06578
Ep  17/20: CE =  4.55%, RE = 0.06777
Ep  18/20: CE =  4.38%, RE = 0.06807
Ep  19/20: CE =  4.31%, RE = 0.06621
Ep  20/20: CE =  4.38%, RE = 0.06667
Final testing error: CE =  5.56%, RE = 0.07936