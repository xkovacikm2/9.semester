hidden function: LOGSIG
output function: LOGSIG
number of epizodes: 20
number of neurons: 10
weight init function: Gaussovská distribúcia od -1 do 1
alpha value: 0.1
Ep   1/20: CE = 35.33%, RE = 0.46961
Ep   2/20: CE = 18.34%, RE = 0.30282
Ep   3/20: CE = 12.22%, RE = 0.22083
Ep   4/20: CE =  9.05%, RE = 0.17686
Ep   5/20: CE =  7.06%, RE = 0.14870
Ep   6/20: CE =  6.41%, RE = 0.13238
Ep   7/20: CE =  5.97%, RE = 0.12160
Ep   8/20: CE =  5.56%, RE = 0.11391
Ep   9/20: CE =  5.62%, RE = 0.10882
Ep  10/20: CE =  5.31%, RE = 0.10369
Ep  11/20: CE =  4.77%, RE = 0.09908
Ep  12/20: CE =  4.89%, RE = 0.09616
Ep  13/20: CE =  4.67%, RE = 0.09223
Ep  14/20: CE =  4.75%, RE = 0.09001
Ep  15/20: CE =  4.55%, RE = 0.08772
Ep  16/20: CE =  4.59%, RE = 0.08491
Ep  17/20: CE =  4.34%, RE = 0.08345
Ep  18/20: CE =  4.27%, RE = 0.08232
Ep  19/20: CE =  4.33%, RE = 0.08025
Ep  20/20: CE =  4.39%, RE = 0.07938
Final testing error: CE =  4.56%, RE = 0.08419