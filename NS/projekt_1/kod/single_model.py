import numpy as np
import random

from classifier import *
from util import *
from func import *

## load data

inputs = np.loadtxt('data/2d.trn.dat', dtype=float, skiprows=1, usecols=[0,1]).T
classes = np.loadtxt('data/2d.trn.dat', dtype=str, skiprows=1, usecols=2)

tst_inputs = np.loadtxt('data/2d.tst.dat', dtype=float, skiprows=1, usecols=[0,1]).T
tst_classes = np.loadtxt('data/2d.tst.dat', dtype=str, skiprows=1, usecols=2)

labels = np.zeros(classes.shape, dtype=int)
tst_labels = np.zeros(classes.shape, dtype=int)

for i in range(0, classes.size):
    labels[i] = ord(classes[i]) - ord('A')

for i in range(0, tst_classes.size):
    tst_labels[i] = ord(tst_classes[i]) - ord('A')

(dim, count) = inputs.shape

## normalize
for i in range(0, dim):
    inputs[i] = normalize(inputs[i])

for i in range(0, dim):
    tst_inputs[i] = normalize(tst_inputs[i])

train_inputs = inputs
train_labels = labels

test_inputs = tst_inputs
test_labels = tst_labels

# plot_dots(inputs, labels)

## train & test model

np.random.seed(1)
random.seed(1)

model = MLPClassifier(dim, 100, np.max(labels)+1, hid_func={'f': tanh, 'df': dtanh, 'desc': 'TANH'}, out_func={'f': logsig, 'df': dlogsig, 'desc': 'LOGSIG'}, weight_func=np.random.randn, file=None)
trainCEs, trainREs = model.train(train_inputs, train_labels, alpha=0.05, eps=200, trace=True, trace_interval=20)

testCE, testRE = model.test(test_inputs, test_labels)
print('Final testing error: CE = {:6.2%}, RE = {:.5f}'.format(testCE, testRE))

plot_both_errors(trainCEs, trainREs, -1, testCE, testRE, block=False)
