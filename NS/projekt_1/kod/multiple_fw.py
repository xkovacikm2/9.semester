import numpy as np
import random

from classifier import *
from util import *
from func import *

## load data

inputs = np.loadtxt('data/2d.trn.dat', dtype=float, skiprows=1, usecols=[0,1]).T
classes = np.loadtxt('data/2d.trn.dat', dtype=str, skiprows=1, usecols=2)

labels = np.zeros(classes.shape, dtype=int)

for i in range(0, classes.size):
    labels[i] = ord(classes[i]) - ord('A')

(dim, count) = inputs.shape

## normalize
for i in range(0, dim):
    inputs[i] = normalize(inputs[i])

## split data
ind = [i for i in range(0, count)]
random.shuffle(ind)
split = len(ind)//5
test_ind  = ind[:split]
train_ind = ind[split:]

train_inputs = inputs[:, train_ind]
train_labels = labels[train_ind]

test_inputs = inputs[:, test_ind]
test_labels = labels[test_ind]

# plot_dots(inputs, labels)

## automatize hyperparameters shuffling

neuron_sizes = [10, 40, 100]

functions_out = [{'f': logsig, 'df': dlogsig, 'desc': 'LOGSIG'}]

functions_hid = [{'f': logsig, 'df': dlogsig, 'desc': 'LOGSIG'}, {'f': tanh, 'df': dtanh, 'desc': 'TANH'}]

weight_functions = [{'f': np.random.rand, 'desc': 'Náhodne od 0 do 1'}, {'f': np.random.randn, 'desc': 'Gaussovská distribúcia od -1 do 1'},
                    {'f': np.ones, 'desc': 'Samé 1'}]

episode_sizes = [20, 50, 100]

alphas = [0.05, 0.1, 0.2]

minimum = {
    'iteration': 0,
    'min_ce': 100
}

iteration = 0
for neurons in neuron_sizes:
    for function_1 in functions_hid:
        for function_2 in functions_out:
            for weight_function in weight_functions:
                for alpha in alphas:
                    for episodes in episode_sizes:
                        iteration += 1
                        file = open('outputs/' + str(iteration) + '.txt', 'w')
                        print(str(iteration))

                        file.write('hidden function: ' + function_1['desc'])
                        file.write("\n")
                        file.write('output function: ' + function_2['desc'])
                        file.write("\n")
                        file.write('number of epizodes: ' + str(episodes))
                        file.write("\n")
                        file.write('number of neurons: ' + str(neurons))
                        file.write("\n")
                        file.write('weight init function: ' + weight_function['desc'])
                        file.write("\n")
                        file.write('alpha value: ' + str(alpha))
                        file.write("\n")

                        print('hidden function: ' + function_1['desc'])
                        print('output function: ' + function_2['desc'])
                        print('number of epizodes: ' + str(episodes))
                        print('number of neurons: ' + str(neurons))
                        print('weight init function: ' + weight_function['desc'])
                        print('alpha value: ' + str(alpha))
                        ## train & test model

                        np.random.seed(1)
                        random.seed(1)
                        model = MLPClassifier(dim, neurons, np.max(labels)+1, hid_func=function_1, out_func=function_2, weight_func=weight_function['f'], file=file)
                        trainCEs, trainREs = model.train(train_inputs, train_labels, alpha=alpha, eps=episodes, trace=False, trace_interval=20)

                        testCE, testRE = model.test(test_inputs, test_labels)
                        file.write('Final testing error: CE = {:6.2%}, RE = {:.5f}'.format(testCE, testRE))
                        print('Final testing error: CE = {:6.2%}, RE = {:.5f}'.format(testCE, testRE))

                        table = open('outputs/tabulka.tex', 'a')
                        table.write(str(neurons))
                        table.write(' & ')
                        table.write(function_2['desc'])
                        table.write(' & ')
                        table.write(function_1['desc'])
                        table.write(' & ')
                        table.write(weight_function['desc'])
                        table.write(' & ')
                        table.write(str(episodes))
                        table.write(' & ')
                        table.write(str(alpha))
                        table.write(' & ')
                        table.write('{:6.2%}'.format(testCE))
                        table.write(' & ')
                        table.write('{:.5f}'.format(testRE))
                        table.write(' \\\\ ')
                        table.write("\n")
                        table.close()

                        if testCE < minimum['min_ce']:
                            minimum['iteration'] = iteration
                            minimum['min_ce'] = testCE
                            print('New minimum found!')

                        plot_both_errors(trainCEs, trainREs, iteration, testCE, testRE, block=False)

                        file.close()
