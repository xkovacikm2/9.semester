import numpy as np

from util import *


class MLP():

    def __init__(self, dim_in, dim_hid, dim_out, w_init_func):
        self.dim_in     = dim_in
        self.dim_hid    = dim_hid
        self.dim_out    = dim_out

        if w_init_func == np.ones:
            self.W_hid = w_init_func([dim_in+1, dim_hid])
            self.W_out = w_init_func([dim_hid+1, dim_out])
        else:
            self.W_hid = w_init_func(dim_in+1, dim_hid)
            self.W_out = w_init_func(dim_hid+1, dim_out)

    def predict(self, x):
        count = x.shape[1]
        y = []
        for i in range(count):
            x_i = x[:,i]
            q, *_ = self.forward(x_i)
            y.append(q[i])
        return np.array(y)

    def forward(self, x):
        x = np.array(x)
        x = augment(x.T).T

        a = x @ self.W_hid
        f_hid = np.vectorize(self.f_hid)

        h = f_hid(a)
        h = augment(h.T).T

        b = h @ self.W_out
        f_out = np.vectorize(self.f_out)
        y = f_out(b)

        return y, b, h, a


    def backward(self, x, d):
        y, b, h, a = self.forward(x)

        y = y.T
        b = b.T

        x = np.matrix(x)
        x = augment(x.T).T

        df_out = np.vectorize(self.df_out)
        g_out = (d-y) * df_out(b)

        W_out = self.W_out[:-1]
        df_hid = np.vectorize(self.df_hid)

        g_hid = np.matrix((W_out @ g_out) * df_hid(a)).T

        g_out = np.matrix(g_out).T
        h = np.matrix(h)

        dW_out = (g_out @ h).T
        dW_hid = (g_hid @ x).T
        return y, dW_hid, dW_out
