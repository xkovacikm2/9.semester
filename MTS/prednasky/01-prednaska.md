## Zlyhavanie softverovych projektov
- nepresne odhady
- zle vyjadrene ciele
- zle definovane poziadavky, hlavne s klientami, ktori tomu nerozumeju
- slaba komunikacia medzi vyvojarmi a pouzivatelmi
- slaby manazment projektu

### Manazment
- proces koordinovania cinnosti skupiny ludi (entit) za ucelom efektivneho dosiahnutia stanovenych cielov.

### Projektovy manazment
- Zivotny cyklus softveroveho projektu
- planovanie a odhadovanie v softverovom projekte
- manazment rizik
- riadenie a vykonovanie projektu
- specifika manazmentu vybranych typov softverovych projektov
- manazment rozsahu projektu, nakladov, casu, kvality

### Strategie a politiky manazmentu IT
- zlepsovanie procesov

### Projekt
docasne, vopred casovo ohranicene usilie s cielom vytvorenia jedinecneho vyrobku, alebo sluzby
- **docasne**
  - kazdy projekt ma jednoznacny zaciatok a koniec
- **podprojekt**
  - cast projektu, samostatne a spravidla lahsie manazovatelna
  
#### Cinnosti v projekt
- **_manazment projektu_** - opis/specifikacia/organizacia prace
  - pouzitie vhodnuch znalosti, zrucnosti, prostriedkov
  - ciele manazmentu (cas/rozsah/naklady) -> rychlo/lacno/vsetko
- **_tvorba produktu ako vystupu_** - specifikacia a tvorba, vyvoj

### Zivotny cyklus projektu
- inicializacia
- planovanie
- vykonavanie
- riadenie
- ukoncenie

## Softverove projekty

#### Specifika softveru
- abstraktnost (neexistencia v realnom svete)
- kognitivne naroky (vysoka narocnost tvorby)
- lahka duplikovatelnost
- moznost distribuovanej tvorby

#### Problemy tvorby softveru
Nedaju sa odstranit, daju sa len zmiernit
- zlozitost
- podriadenost
- premenlivost
- neuchopitelnost

### Startup
- spolocnost s cielom rychleho rastu
- spolocnost s vysokou mierou rizika a neistoty
- spolocnost, alebo docasna organizacia vytvorena s cielom najdenia opakovatelneho a skalovatelneho biznis modelu

# Zadanie 1
Navrh startup projektu
22.10 - 5. tyzden sa odovzdava do aisu