(TeX-add-style-hook
 "metodika"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "12pt" "oneside" "slovak" "a4paper")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem") ("babel" "slovak" "english") ("placeins" "section") ("geometry" "dvips" "dvipdfm" "a4paper" "centering" "textwidth=14cm" "top=4.6cm" "headsep=.6cm" "footnotesep=1cm" "footskip=0.6cm" "bottom=3.8cm")))
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art12"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref"
    "babel"
    "url"
    "cite"
    "times"
    "placeins"
    "listings"
    "geometry")
   (LaTeX-add-labels
    "sec:orgf5f6ba1"
    "sec:org8927edd"
    "sec:orgc649833"
    "sec:org746de60"
    "sec:org4c7c529"
    "sec:org885aeff"
    "sec:org6e92e27"
    "sec:orgfbb1636"
    "orgb4b5010"
    "orgeca56c7"
    "sec:org334dc98"
    "sec:orgdc49103"
    "sec:orga88923c"
    "sec:org06720ba"
    "org62a13a5"
    "sec:orgd0f857d"
    "sec:org3e63534"
    "sec:orgeeedcff"
    "sec:orgcdbc57e"
    "fig:org367a368"))
 :latex)

