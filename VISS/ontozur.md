# Ako citat clanok
1. nadpis
1. abstrakt
1. skim introduction
1. look at the structure
1. previous/related work section
1. read conclusions
1. read the body

## citanie na 3 behy
1. priblizne 10 minut a prebehnut len 
1. priblizne hodinu na druhy beh si popozeras obrazky, nemusis citat dokazy etc.
1. asi 4 hodiny, pomaly citat, reimplementovat clanok, identifikovat a zamysliet sa nad zavermi

## manazment clankov
**Hacky na Mendeleya**
- related work tlacitko pri clanku
- search operators, klasicky ako google. publication: "Behaviour research"
- ku clanku sa daju pridavat dalsie files

### reading report
- o com to bolo
- metoda vyskumu
- vysledky
- comu som nerozumel

# Funkcia kapitoly analyzy

## preco analyzujeme
- ucime sa
- robim si nazor na problem
- formujeme riesenie

## analyza ako kapitola

### co nerobit
- nic by nemalo by _samoucelne_
- nema to byt plytke
- prilis vseobecne
- zmes poznamok k clankom
- zmes zakladnych pojmov

### co robit
- hovorime o akte pisania
- zacinam pisanim zhrnutia
  - predstavit si mnozinu vyrokov (mozu byt motivaciou k metode)
  - nie viac ako strana
- potom premyslat nad tym ako ma vyzerat kapitola

- Existuje kolekcia 3D volumetrickych snimkov tela a existuje kolekcia anotovanych patologickych zmien organov.
- Lekar sa pri skene zameriava na oblast, ktoru indikuju priznaky a velka cast snimky ostane neprezreta, pretoze sken sa robil kvoli castiam, na ktore sa zameriava.
- Metoda si kladie za ciel prezriet snimky strojovo, ci sa na nich nepodari indikovat mozne patologicke zmeny aj mimo primarneho regionu zaujmu lekara.
- V prvom kroku potrebujeme vysegmentovat organy a potom pre kazdy organ spustit specializovanu metodu na hladanie konkretnych patologickych zmien.

```
## Metody segmentacie obrazkov
- klasicke algoritmy, metody umelej inteligencie
## Sucasne pristupy k segmentacii organov
### State of the art sucasne metody
```

- v zhrnuti su teda nejake claimy, ktore potom v samotnej analyze mozem rozobrat
- ked este neviem metodu, mozem aspon napisat poziadavky na svoju metodu
- vyrokova osnova je ovela lepsia ako klucove slova, pre osnovu je teda vyhodne pisat cele vety
- kazdy odstavec by mal mat svoj vyrok

