(TeX-add-style-hook
 "zamer"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref"
    "pdfpages")
   (TeX-add-symbols
    "Author"
    "Title"
    "Subtitle"
    "Examiner"
    "Year"
    "Month")
   (LaTeX-add-labels
    "sec:orgdc96dec"
    "sec:org9669687"
    "sec:org9bcd951"
    "sec:orgddd9fea"
    "sec:orge11316a"
    "sec:orgba87790"
    "sec:org6626b21"
    "sec:org248b01c"
    "sec:orgfcf73a2"
    "sec:orgfc3a9ea"
    "sec:org8a14850"
    "sec:orgb3aa95a"
    "sec:orgd2713e2"
    "sec:orga68b40d"
    "sec:orgb1b30a1"
    "sec:org35bdece"
    "org36b1347"
    "sec:org16ae165")
   (LaTeX-add-bibliographies
    "literatura"))
 :latex)

