(TeX-add-style-hook
 "literatura"
 (lambda ()
   (LaTeX-add-bibitems
    "Hu2017"
    "Moghbel2017"
    "Goceri2016"
    "Babalola2008"
    "babalola2007automatic"
    "aljabar2007classifier"
    "patenaude2007bayesian"
    "Litjens2017"
    "kamnitsas2017efficient"
    "stollenga2015parallel"))
 :bibtex)

