(TeX-add-style-hook
 "11-prednaska"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref")
   (LaTeX-add-labels
    "sec:orgacd7f74"
    "sec:orgb5453b3"
    "sec:orgf10e517"
    "sec:org9607b4c"
    "sec:org1cc5425"
    "sec:org10e07fd"
    "sec:orgbb36138"
    "sec:org9c3aca2"
    "sec:orgc47fed5"
    "sec:orgb04c505"
    "sec:orgb729b0d"
    "sec:org8b87c5d"
    "sec:org8354b37"
    "sec:org87c20a2"
    "sec:orgea8d372"
    "sec:org350223d"
    "sec:orgfc65690"
    "sec:org3a0f91d"
    "sec:org4d3ea56"
    "sec:org7ee58a3"
    "sec:orgb73d2dd"))
 :latex)

