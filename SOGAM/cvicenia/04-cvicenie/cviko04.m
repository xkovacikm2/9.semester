%% Kompresia
raw_sound = importdata("Fado1.wav");

sampling_f = raw_sound.fs;
signal = raw_sound.data(:,1);

figure
plot(1/sampling_f:1/sampling_f:length(signal)/sampling_f, signal);

window_size = 512;
window = signal(25000:25000 + window_size -1);

figure
plot(window)

figure
stem(dct(window));

loops = length(signal)/window_size - 1;
new_signal = [];

for i=0:loops
  window = signal((i*window_size+1):(i*window_size+window_size));

  dct_rest = dct(window);
  dct_rest = dct_rest .* (abs(dct_rest) > 0.02);

  new_window = idct(dct_rest);
  new_signal = [new_signal; new_window];
end

figure 
spectrogram(signal, hamming(window_size), window_size/4*3, window_size, sampling_f, 'yaxis');
figure
spectrogram(new_signal, hamming(window_size), window_size/4*3, window_size, sampling_f, 'yaxis');

%% Pass filtre
raw_sound = importdata("prostitutky.aifc");

sampling_f = raw_sound.fs;
signal = raw_sound.data(:,1);
window_size = 4096;

loops = length(signal)/window_size - 1;
new_signal = [];

for i=1:loops
  window = signal((i*window_size+1):(i*window_size+window_size));

  dct_rest = dct(window);
  % LP
  dct_rest(120:window_size) = 0;
  % HP
  dct_rest(1:80) = 0;

  new_window = idct(dct_rest);
  new_signal = [new_signal; new_window];
end

playblocking(audioplayer(new_signal, sampling_f));
spectrogram(new_signal, hamming(window_size), window_size/4*3, window_size, sampling_f, 'yaxis');