QL = [16 11 10 16 24 40 51 61 
12 12 14 19 26 58 60 55 
14 13 16 24 40 57 69 56 
14 17 22 29 51 87 80 62 
18 22 37 56 68 109 103 77 
24 35 55 64 81 104 113 92 
49 64 78 87 103 121 120 101 
72 92 95 98 112 100 103 99];

QC = [17 18 24 47 99 99 99 99 
18 21 26 66 99 99 99 99 
24 26 56 99 99 99 99 99 
47 66 99 99 99 99 99 99 
99 99 99 99 99 99 99 99 
99 99 99 99 99 99 99 99 
99 99 99 99 99 99 99 99 
99 99 99 99 99 99 99 99];

pic = imread('Lenna.png');

result = pic;
pic_size = size(pic);

q = input('Enter quality: ');

if q < 50
  alpha = 50/q;
else
  alpha = 2 - q/50;
end

aQL = alpha*QL;
aQC = alpha*QC;

total = 0;
non_zeros = 0;
block_size = 8;

for x = 1:block_size:(pic_size(1))
  for y = 1:block_size:(pic_size(2))
    block_rgb = pic(x:x+7, y:y+7, :);
    block_ycbcr = rgb2ycbcr(block_rgb);

    block_y = block_ycbcr(:,:,1); 
    block_cb = block_ycbcr(:,:,2); 
    block_cr = block_ycbcr(:,:,3); 

    dct_y = dct2(block_y);
    dct_cb = dct2(block_cb);
    dct_cr = dct2(block_cr);

    reduced_block = block_ycbcr;

    reduced_y = round(dct_y./(aQL));
    reduced_cb = round(dct_cb./(aQC));
    reduced_cr = round(dct_cr./(aQC));

    % calculate metrics
    non_zeros = non_zeros + sum(sum(reduced_y ~= 0));
    non_zeros = non_zeros + sum(sum(reduced_cb ~= 0));
    non_zeros = non_zeros + sum(sum(reduced_cr ~= 0));
    total = total + 3*block_size*block_size;

    reduced_block(:,:,1) = idct2(reduced_y .* (aQL));
    reduced_block(:,:,2) = idct2(reduced_cb .* (aQC));
    reduced_block(:,:,3) = idct2(reduced_cr .* (aQC));

    reduced_rgb_block = ycbcr2rgb(reduced_block);
    result(x:x+7, y:y+7, :) = reduced_rgb_block(:,:,:);
  end
end

figure
imshow(result);

(1-non_zeros/total)*100