function [ signal ] = signal_generator( signal_f, time_interval )
  signal = sin(2 * pi * signal_f .* time_interval);
end

