%% Nasnimanie signalu ladicky
raw_sound = importdata("ladicka.aifc");

sampling_f = raw_sound.fs;
signal = raw_sound.data(:,1);

plot(1/sampling_f:1/sampling_f:length(signal)/sampling_f, signal);

window_size = 512;
window = signal((sampling_f*2):(sampling_f*2)+window_size-1);
hamm_res = window .* hamming(window_size);

figure
plot(hamm_res);

figure
sq_magnitude = abs(fft(window));
stem(sq_magnitude);

figure
hamm_magnitude = abs(fft(hamm_res));
stem(hamm_magnitude);

bin_f = (sampling_f/2)/(window_size/2);
[val, idx] = max(hamm_magnitude);
freq = bin_f * (idx-1)

figure
spectrogram(signal, hamming(window_size), window_size/4*3, window_size, sampling_f, 'yaxis');


%% Vypocet spektra
sampling_f = 44100;
time_interval = 0:(1/sampling_f):1;

sum_signal = zeros(1,sampling_f + 1);

for i=1:5
  sum_signal = sum_signal + (rand * signal_generator(i*100, time_interval));
end

figure
plot(time_interval, sum_signal);

window_size = 1000;
sq_window = sum_signal(1:window_size);

figure
plot(sq_window);

hamm_window = sum_signal(1:window_size)' .* hamming(window_size);

figure
plot(hamm_window);

magnitude_hamm = abs(fft(padarray(hamm_window, 1024 - length(hamm_window), 'post')))
magnitude_sq = abs(fft(padarray(sq_window', 1024 - length(sq_window), 'post')))

figure
stem(fftshift(magnitude_hamm))
figure
stem(fftshift(magnitude_sq))
