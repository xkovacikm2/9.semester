%% Linearna filtracia
pic = imread('rsz_reichstag.jpg');

gauss_filter = fspecial('gaussian', 5, 4);
sobel_filter = fspecial('sobel');
laplace_filter = fspecial('laplacian');

figure
subplot(2,2,1)
imshow(pic);

gauss_res = uint8(conv2(pic, gauss_filter, 'same'));
subplot(2,2,2)
imshow(gauss_res);

sobel_res = uint8(conv2(pic, sobel_filter, 'same'));
subplot(2,2,3)
imshow(sobel_res);

laplace_res = uint8(conv2(pic, laplace_filter, 'same'));
subplot(2,2,4)
imshow(laplace_res);

%%Filtracia obrazu v spektralnej domene
pic = imread('rsz_reichstag.jpg');

pic_spectrum = fftshift(fft2(pic));
figure
imagesc(log(abs(pic_spectrum)));

kernel_size = size(pic_spectrum);
kernel_lp= ones(kernel_size);

%LP
for x = 1:kernel_size(1)
  for y = 1:kernel_size(2)
    if sqrt((x - kernel_size(1)/2)^2 + (y - kernel_size(2)/2)^2) > 20
      kernel_lp(x,y) = 0;
    end
  end
end

figure
subplot(2,2,1)
imshow(kernel_lp);

result_ideal_lp = uint8(abs(ifft2(ifftshift(pic_spectrum .* kernel_lp))));

subplot(2,2,3)
imshow(result_ideal_lp);

gauss_kernel_lp= conv2(kernel_lp, fspecial('gaussian', 15, 20), 'same');

subplot(2,2,2)
imshow(gauss_kernel_lp);

result_gauss_lp = uint8(abs(ifft2(ifftshift(pic_spectrum .* gauss_kernel_lp))));

subplot(2,2,4)
imshow(result_gauss_lp);

% HP
kernel_hp = 1-kernel_lp;

figure
subplot(2,2,1)
imshow(kernel_hp);

result_ideal_hp = uint8(abs(ifft2(ifftshift(pic_spectrum .* kernel_hp))));

subplot(2,2,3)
imshow(result_ideal_hp);

gauss_kernel_hp= 1-gauss_kernel_lp;

subplot(2,2,2)
imshow(gauss_kernel_hp);

result_gauss_hp = uint8(abs(ifft2(ifftshift(pic_spectrum .* gauss_kernel_hp))));

subplot(2,2,4)
imshow(result_gauss_hp);