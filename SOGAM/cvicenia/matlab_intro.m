%% MATLAB, The MATrix LABoratory

A=['a':'z' 'A':'Z' '0':'9';];
Data=zeros(length(A),20);
for j=1:3001
    for k=1:100
        Data(:,k)=randperm(length(A));
    end
    disp(A(Data))
end

%% clc
clear % clear workspace
clc % CLear Console output
disp('Knock, knock, knock, Neo.'); % print();

%% IDE

% filesystem
% editor
% REPL console
% Workspace (choose columns)

% ctrl + enter (block)
% F9 (selection)
% tabs (ctrl + PgUp/PgDwn)
% windows (ctrol + tab)

%% ops
add = 4 + 5
sub = 4 - 5 
mul = 4 * 5 
div = 4 / 5 
modulo = mod(4, 5) % comment

power = 2 ^ 8
infinite_power = 2 / 0
not_a_number = 0 / 0


%% surpass output (;) vs. disp() func
'show this'
'hide this';
disp('display this');

%% vectors
V1 = [1   2 3] % row vector
V2 = [1 ; 2 ; 3] % column vector

vector_long = 1:20
vector_custom_step = 1:0.1:2

% sin, cos, tan
sin(vector_custom_step)

%% matrices
A = [1 2 ; 3 4]

%% matrix ops

V2_transp = V2' % transpose
A_transp = A'

V2' == V1 % compare element-wise

mat_add = A + A
mat_add = A - A
mat_prod = A * A % matrix multiplipaction
mat_dot_prod = A .* A % dot product
mat_slash_prod = A ./ A

more_power = [2 .^ [0:5]]
more_power = [[0:5] .^ 2]

%% 0s and 1s
vec_zeros = zeros(1, 5)
mat_ones = ones(5, 5)
three_d_ones = ones(5, 5, 5);
%% indexing
vector_long(7)
vector_long(1:4) % index from 1
vector_long([1 3 5:7 10:3:19])

matrix_big = reshape([1:100], [10,10])'
matrix_big([1 2 3],:)
matrix_big(:,3:5)
matrix_big(4:end,3:5)
matrix_big(1:4,[3 5])

% more fun here: https://www.mathworks.com/company/newsletters/articles/matrix-indexing-in-matlab.html

%% conditions
if 1 > 2
	% do something;
    'A'
elseif 1 == 2
    % do something else
    'B'
elseif 1 >= 2
    % do something even more else
    'C'
elseif 2 ~= 2 % not equals
    % do something even more more else
    'D'
else 1 <= 2
	% do the most else thing there is
    'E'
end

~true % negation by tilda (~) character
~false

%% loops
for index = [1 2 3] % 1:length(signal)
	% do something with index
end

i = 1
while i < 10
    % no i++
    % no i += 1
    % no inc(1)
    i = i + 1;
end
i

%% type cast
vec = [1 2 3]
isfloat(vec)

int_vec = uint8(vec)
isinteger(int_vec)

islogical([true false])

%% functions
% !!! encapsulate repeatedly used stuff !!!
% matlab -> new -> function
% save as file func_name.m
function [ a b c ] = func_name(arg1, arg2, arg3)
%FUNC_NAME Summary of this function goes here
%   Detailed explanation goes here
    a = arg1
    b = 7
    c = arg3 - arg2 / arg1
end
% existence of func_name.m in workdir grants function visibility in MATLAB

%% workspace
whos
clear
clc

%% TASK 1 – generate signal (220 Hz, 22500Hz sampling)
sampling_freq = 22500;
signal_freq = 440;

time_step = 1 / sampling_freq;
sampling_times = 0:time_step:1;
signal = sin(2 * pi * signal_freq * sampling_times);

%% plot the signal - multiple plots in multiple figures
figure();
plot(sampling_times, signal);
title('Snippet (1s) of signal (440Hz)');
ylim([-1.2 1.2]); % limits Y axis to given range

figure();
% continue long statements on multiple lines (end line by ...)
plot(sampling_times(1:sampling_freq/100+1), ... 
     signal(1:sampling_freq/100+1));
title('Snippet (0.01s) of signal (440Hz)');

%% plot the signal - multiple subplots in single figure
close % closes all open figures

figure();
sp1 = subplot(3,1,1);
plot(sampling_times, signal);
title('Snippet (1s) of signal (440Hz)');
ylabel('amplitude');
xlabel('time [s]');

sp2 = subplot(3,1,2);
plot(sampling_times(1:sampling_freq/100+1), ... 
     signal(1:sampling_freq/100+1));
title('Snippet (0.01s) of signal (440Hz)');
ylabel('amplitude');
xlabel('time [s]');

sp3 = subplot(3,1,3);
plot(sampling_times(1:sampling_freq/signal_freq+1), ... 
     signal(1:sampling_freq/signal_freq+1));
title('Single sine wave snippet of signal (440Hz)');
ylabel('amplitude');
xlabel('time [s]');

linkaxes([sp1 sp2 sp3]'); % this might be useful for comparisons of
%similarly long signals

%close % closes all open figures

%% play audio signal: non-blocking command
for i = 1:2
    soundsc(signal, sampling_freq);
    pause(length(signal) / sampling_freq) % Wait For First Sound To Finish
end

%% play audio signal: blocking command (better way)
player = audioplayer(signal, sampling_freq);
% for various signals to play create separate audioplayer objects
for i = 1:2
    playblocking(player);
end
% no need to calculate pause time
