w = warning('off', 'all')
files = dir('frames/*.png');
macroblock_size = 15;

i = 10
current_pic = rgb2gray(imread(strcat('frames/', files(i).name)));
next_pic = rgb2gray(imread(strcat('frames/', files(i+2).name)));
motions_horiz = [];
motions_vert = [];
x = [];
y = [];
pic_size = size(current_pic);
next_pic_size = size(next_pic);

% loop over blocks
for vertical = 1:macroblock_size:pic_size(1)-macroblock_size
  for horizontal = 1:macroblock_size:pic_size(2)-macroblock_size
    uint32(vertical+ horizontal/macroblock_size) %delete

    % retrieve block from current frame
    block = current_pic(vertical:(vertical+macroblock_size), horizontal:(horizontal+macroblock_size));

    % set the block on the same position in next frame as default best
    winner = trace(pdist2(block, next_pic(vertical:(vertical+macroblock_size), horizontal:(horizontal+macroblock_size)), 'cosine'));
    winner_vertical = vertical;
    winner_horizontal = horizontal;

    % linear search surroundings of macroblock
    for search_vertical = max(1, vertical - macroblock_size):1:min(next_pic_size(1)-macroblock_size, vertical + macroblock_size)
      for search_horizontal = max(1, horizontal - macroblock_size):1:min(next_pic_size(2)-macroblock_size, horizontal + macroblock_size)
        next_block = next_pic(search_vertical:(search_vertical+macroblock_size), search_horizontal:(search_horizontal+macroblock_size));
        distance = trace(pdist2(block, next_block, 'cosine'));

        % set best match
        if distance < winner
          winner = distance;
          winner_vertical = search_vertical;
          winner_horizontal = search_horizontal;
        end

      end
    end

    % store motion vectors
    x = [x, vertical];
    y = [y, horizontal];
    motions_vert = [motions_vert, winner_vertical];
    motions_horiz = [motions_horiz, winner_horizontal];
  end
end

fused = imfuse(current_pic, next_pic, 'blend');
figure
imshow(fused);
hold on
quiver(uint32(y+macroblock_size/2), uint32(x+macroblock_size/2), motions_horiz-y, motions_vert-x);
