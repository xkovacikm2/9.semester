
function [ result ] = rand_interval( minimum, maximum )
  result = rand() * (maximum - minimum) + minimum
end