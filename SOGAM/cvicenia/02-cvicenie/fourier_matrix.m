function [ matrix ] = fourier_matrix( dim )
  matrix = ones(dim, dim);
  for j = 1:dim
    for k = 1:dim
      matrix(j,k) = exp((2*pi*1i*j*k)/dim);
    end
  end
end

