%% Generovanie signalu
sampling_f = 44.1*(10^3);
time_interval = 0:(1/sampling_f):3;

% Navzorkovanie signalu podla samplovacej frekvencie 
signal440 = 0.7 * signal_generator(440, time_interval);

signal880 = rand_interval(0.2,0.5) * signal_generator(880, time_interval);
signal1320 = rand_interval(0.1,0.4) * signal_generator(1320, time_interval);

playblocking(audioplayer(signal440, sampling_f));
playblocking(audioplayer(signal880, sampling_f));
playblocking(audioplayer(signal1320, sampling_f));

sum_signal = signal440 + signal880 + signal1320;

playblocking(audioplayer(sum_signal, sampling_f));

figure
plot(time_interval(1:sampling_f/100), signal440(1:1:sampling_f/100));
figure
plot(time_interval(1:sampling_f/100), signal880(1:1:sampling_f/100));
figure
plot(time_interval(1:sampling_f/100), signal1320(1:1:sampling_f/100));
figure
plot(time_interval(1:sampling_f/100), sum_signal(1:1:sampling_f/100));

%% Vypocet spektier signalu
window = 512;

% fft cez matlab
matlab_result = fft(sum_signal, window);
magnitude = abs(matlab_result);
figure
stem(magnitude(1:50));

% fft cez dvojnu sumu
loop_result = zeros(1,window);

for u = 1:window
  for k = 1:window
    loop_result(u) = loop_result(u) + sum_signal(k) * exp((-1i * 2 * pi * u * k)/window);
  end
end

magnitude = abs(loop_result);
figure
stem(magnitude(1:50));

% fft cez maticove nasobenie
fourier_m = fourier_matrix(window);

matrix_result = fourier_m * sum_signal(1:window).';
magnitude = abs(loop_result);
figure
stem(magnitude(1:50))