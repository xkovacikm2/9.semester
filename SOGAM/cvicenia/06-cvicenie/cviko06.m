%% odstranenie sumu v priestorovej domene
pic = imread('6_periodic_noise/salt_and_pepper.png');
pic = rgb2gray(pic);

gauss_filter = fspecial('gaussian', 5, 8);
mean_filter = fspecial('average', 5);

figure
subplot(2,2,1)
imshow(pic);

gauss_res = uint8(conv2(pic, gauss_filter, 'same'));
subplot(2,2,2)
imshow(gauss_res);

mean_res = uint8(conv2(pic, mean_filter, 'same'));
subplot(2,2,3)
imshow(mean_res);

median_res = medfilt2(pic);
subplot(2,2,4)
imshow(median_res);

%% odtranenie sumu v spektre
pic = imread('6_periodic_noise/per5.png');
pic = rgb2gray(pic);

pic_spectrum = fftshift(fft2(pic));
x = 10000;
y = 10000; 
btn = 0;

% q key
while btn ~= 113
  subplot(2,2,1)
  imshow(pic);

  kernel_size = size(pic_spectrum);
  kernel = zeros(kernel_size);

  click_size = sqrt((kernel_size(1)/2 - y)^2 + (kernel_size(2)/2 - x)^2);

  for i = 1:kernel_size(1)
    for j = 1:kernel_size(2)
      % left click
      if btn == 1
        if sqrt((x - j)^2 + (y - i)^2) < 10
          kernel(i,j) = 1;
          kernel(kernel_size(1)-i,kernel_size(2)-j) = 1;
        end
      % right click
      elseif btn == 3
        now_size = sqrt((kernel_size(1)/2 - i)^2 + (kernel_size(2)/2 - j)^2);
        if now_size + 10 > click_size && now_size - 10 < click_size
          kernel(i,j) = 1;
        end
      end
    end
  end

  kernel = conv2(kernel, fspecial('gaussian', 15, 20), 'same');
  kernel = 1 - kernel;
  subplot(2,2,2)
  imshow(kernel);

  pic_spectrum = pic_spectrum .* kernel;

  subplot(2,2,3)
  imshow(log(abs(pic_spectrum))./max(log(abs(pic_spectrum(:)))));

  subplot(2,2,4)
  imshow(uint8(abs(ifft2(ifftshift(pic_spectrum)))));

  [x,y,btn] = ginput(1);
end