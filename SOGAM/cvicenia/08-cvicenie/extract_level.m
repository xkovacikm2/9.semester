function [ cA, cV, cH, cD ] = extract_level(c,s,w_type,level)
  cA = wrcoef2('a', c, s, w_type, level);
  cV = wrcoef2('V', c, s, w_type, level);
  cH = wrcoef2('H', c, s, w_type, level);
  cD = wrcoef2('D', c, s, w_type, level);
end