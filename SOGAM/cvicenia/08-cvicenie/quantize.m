function [ quantized ] = quantize( pic, q_step )
  quantized = sign(pic) .* floor(abs(pic)/q_step);
end