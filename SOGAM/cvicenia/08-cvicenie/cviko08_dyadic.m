pic = imread('Lenna.png');

pic_g = rgb2gray(pic);
q_step = 10

mode = 'zpd'; % zero padding
w_type = 'haar';

max_recursions = wmaxlev(size(pic_g), w_type);
dwtmode(mode);
[c, s] = wavedec2(pic_g, max_recursions, w_type);

for i=3:1:4
  [cA, cH, cV, cD] = extract_level(c,s,w_type,i);
end

quantized_c = quantize(c, q_step);
result = waverec2(quantized_c, s, w_type);
dequantized_result = uint8(result * q_step);

figure
imshow(dequantized_result);