%% single level wavelet compression
pic = imread('Lenna.png');

pic_size = size(pic);
pic_g = rgb2gray(pic);

quantizer_step = 10;
mode = 'zpd'; %zero padding
w_type = 'haar';

[cA,cH,cV,cD] = dwt2(pic_g, w_type, 'mode', mode);

figure
subplot(2,2,1)
imshow(scale4imshow(cA));

subplot(2,2,2)
imshow(cH/20); title('Horizontal');

subplot(2,2,3)
imshow(cV/20); title('Vertical');

subplot(2,2,4)
imshow(cD/20); title('Diagonal');

quantized_cA = quantize(cA, quantizer_step);
quantized_cH = quantize(cH, quantizer_step);
quantized_cV = quantize(cV, quantizer_step);
quantized_cD = quantize(cD, quantizer_step);

figure
subplot(2,2,1)
imshow(scale4imshow(quantized_cA));

subplot(2,2,2)
imshow(quantized_cH); title('Quantized horizontal');

subplot(2,2,3)
imshow(quantized_cV); title('Quantized vertical');

subplot(2,2,4)
imshow(quantized_cD); title('Quantized diagonal');

idwt_result = idwt2(quantized_cA, quantized_cH, quantized_cV, quantized_cD, w_type, 'mode', mode);
dequantized = idwt_result * quantizer_step;

figure
imshow(uint8(dequantized));

