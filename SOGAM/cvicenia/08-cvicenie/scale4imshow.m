function [ scaled_pic ] = scale4imshow( pic )
  scaled_pic = uint8(pic/max(pic(:))*255);
end